
[Setup]
AppName=EzPlan-IT for C++ Builder (with source)
AppVerName=EzPlan-IT version $Version$
AppCopyright=Copyright (C) 1998-2011 A-Dato Scheduling Technology
DefaultDirName={pf}\A-Dato\EzPlanIT
DefaultGroupName=EzPlan-IT
OutputDir=RegSrcOutput

[Files]
Source: "Files\*.txt"; DestDir: "{app}";
Source: "Files\Bpl\*"; DestDir: "{app}\Bpl"; Flags: recursesubdirs;
Source: "Files\Demos\*"; DestDir: "{app}\Demos"; Flags: recursesubdirs;
Source: "Files\EndPoints\*"; DestDir: "{app}\EndPoints"; Flags: recursesubdirs;
Source: "Files\Help\*"; DestDir: "{app}\Help"; Flags: recursesubdirs;
Source: "Files\Include\*"; DestDir: "{app}\Include"; Flags: recursesubdirs;
Source: "Files\Lib\*"; DestDir: "{app}\Lib"; Flags: recursesubdirs;
Source: "Files\Source\*"; DestDir: "{app}\Source\"; Flags: recursesubdirs;



