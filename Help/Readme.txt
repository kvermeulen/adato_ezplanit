=------------------------------------------------------------------------------=
EzPlan-IT readme file
  E-Mail:  Info@a-dato.net
  Web:     www.a-dato.net

Copyright (C) 1998-2011, by A-Dato Scheduling Technology
=------------------------------------------------------------------------------=

Welcome to this new release of EzPlan-IT!

EzPlan-IT is the next step in resource scheduling components. Using our suite
makes building scheduling applications easy.


IMPORTANT NOTE
**************

>>> From version 6.1.00, EzPlan-IT installs into directory {install dir}\A-Dato\EzPlanIT!

Each version of Delphi supports different properties. It can therefore happen that
when running or opening a file from the EzProject demo application an exception is
raised saying that a certain property cannot be read because this property does 
not exist. 

To overcome this problem, open each file in your development environment, 
apply a small change to the form (for example: update the position of a component)
and save your changes. This will remove any conflicting property from the source code.


****************

Read Release.txt to get information about the latest changes made to this release.
Read Update.txt for information on how to convert your existing applications
to the latest version of EzPlan-IT.

=====================
CONTENTS
=====================
- Installation
- New features
- Demo application

=====================
INSTALLATION
=====================

Installing under Vista or Windows 7? Please read the note below!

We advise you to run the installation program as administrator.

The default installation directory has been set to: c:\program files\A-Dato\EzPlanIT.
This is because C++ Builder cannot handle paths including a '-' character.

From this version on, EzPlan-IT's bpl files are split into a runtime- and designtime
package. Package EzBasic_XX is therefore accompanied by EzBasicDs_XX and EzPlanit_XX
is accompanied by EzPlanitDs_XX. Only the design time packages need to be installed in
your development environment.

To actualy install the EzPlan-IT components, you need copy the Bpl files to a directory
available on your search path (for example <delphi dir>\projects\Bpl).

For a Delphi installation the Bpl files are located in <installdir>\Bpl. These file
should be copied to your Bpl directory:

  EzSpecials_XX.bpl
  EzSpecialsDs_XX.bpl --> Exists only in Delphi 2006 and above
  EzBasic_XX.bpl
  EzBasicDs_XX.bpl
  EzPlanit_XX.bpl
  EzPlanitDs_XX.bpl

  EzSpecials_XX.dcp
  EzSpecialsDs_XX.dcp --> Exists only in Delphi 2006 and above
  EzBasic_XX.dcp
  EzBasicDs_XX.dcp
  EzPlanit_XX.dcp
  EzPlanitDs_XX.dcp


(XX indicates the Delphi version: D5, D6, D7, D2005, D2006, D2007, D2009, D2010, DXE)

For a C++ Builder installation these file should be copied to this directory:

  EzSpecials_XX.bpl
  EzBasic_XXX.bpl
  EzBasicDs_XXX.bpl
  EzPlanit_XXX.bpl
  EzPlanitDs_XXX.bpl

And to your Lib directory (<bcb dir>\projects\Lib):
  EzSpecials_XX.bpi
  EzSpecialsDs_XX.bpi --> Only in BCB 2007 or above
  EzBasic_XXX.bpi
  EzBasicDs_XXX.bpi
  EzPlanit_XXX.bpi
  EzPlanitDs_XXX.bpi

  EzSpecials_XX.lib
  EzSpecialsDs_XX.lib --> Only in BCB 2007 or above
  EzBasic_XXX.lib
  EzBasicDs_XXX.lib
  EzPlanit_XXX.lib
  EzPlanitDs_XXX.lib

(XX indicates the C++ Builder version: CB5, CB6, CB2007, CB2009, CB2010, CBXE)

After copying the files, you can install the EzPlan-IT components
using the Component|Install package command. You only have to install
the design time packages, being: 

   EzSpecials_XX.bpl (or EzSpecialsDs_XX.bpl when installing under BCB 2007)
   EzBasicDs_XX.bpl
   EzPlanitDs_XX.bpl


***********************************
Special note on Vista installations
***********************************

On Vista the directory in which bpl files should be placed has changed. 
By default, bpl's should now be copied to:

	C:\Users\Public\Documents\RAD Studio\xxx\BPL

lib files to: 
	C:\Users\Public\Documents\RAD Studio\xxx\DCP
	

xxx holds the Delphi version number: 

	'5.0' for Delphi 2007
	'6.0' for Delphi 2009
	'7.0' for Delphi 2010
	'8.0' for Delphi XE

In addition, <install dir>Lib\Obj should be added to your 'project's default path' when installing under C++ Builder.
	

===============================
Other file in this distribution
===============================

This version uses a very simple installer which will copy all files to the right
place. These directories are created:

- <install dir>\Demos
  This directory contains the demonstration application which you can inspect
  to learn how to use our components. Two demos are included:
  - Demos\EzProject
    Our scheduler application based on ADO and TClientDataset components.
    This version requires an enterprise version of your Borland development
    environment to compile.
    The database is stored in demos\EzProject\EzProject.mdb (Ms Access).
  - Demos\EzProject_BDE
    Our scheduler application based on the BDE.
    This version can be compiled with any Borland development environment.
    The database is stored in demos\EzProject_BDE\DB\ (Paradox).

- <install dir>\Help
  This directory contains EzPlan-IT's help file.

- <install dir>\Bpl
  This directory holds the package files which must be installed in your
  development environment. It is normally a good idea to copy these files
  to a directory located in your search path before actually installing them.

- <install dir>\lib
  This directory is used to store the compiled Delphi files (*.dcu) for the
  components. In case of a C++ Builder installation this directory holds
  the library (*.lib) and package include files (*.bpi).

  It is neccesary that you include this dir in the library search path of your
  Delphi environment because otherwise you will get compiler errors when
  buidling applications using one of our components.

- <install dir>\lib\Obj (C++ Builder only)
  This directory is used to store the compiled units (*.dcu) in a C++ Builder environment.
  It is neccesary that you include this dir in the library search path of your
  Delphi environment because otherwise you will get compiler errors when
  buidling applications using one of our components.

- <install dir>\Source (only when you bought EzPlan-IT including the sources)
  This directory contains the source code for both the EzBasic.bpl and
  EzPlanIT.bpl files.

- <install dir>\Bin
  The bin directory contains binary files required by Plan-IT. These files
  implement some of the COM objects used.

- <install dir>\Endpoints
  This directory stores some bitmap images you can use as end points in your
  TEzGanttChart component (see TEzEndPointList).

- <install dir>\Include (bcb versions only)
  This directory holds the header files required for bcb versions.

Important:
  It is normally a good idea (especially with C++ builder versions) to copy the
  *.bpl files to a directory located in your search path before installing them
  with 'Component | Install packages'.

The compiled units accompanying these two packages are also located in the
<install dir>\lib directory and it is necessary that you either copy
these files to a directory pointed to by Delphi's library search path or you
should update the search path to include the <install dir>\lib directory.

=====================
NEW FEATURES
=====================

Please read the file Release.txt to see the updates for the
current release.

=====================
DEMO APPLICATION
=====================
A demo application is installed in the directory <install dir>\demos which you
can examine to learn how to use the components. Be aware that the demonstration
is not a full blown application and it lacks many features a real live
application should have.

An executable demo can be downloaded from our website.


*******************         End of readme file               *******************
================================================================================
