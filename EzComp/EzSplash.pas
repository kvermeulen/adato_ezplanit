unit EzSplash;

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TfrmEzSplash = class(TForm)
    Image1: TImage;
    Memo1: TMemo;
    Label1: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    btnClose: TButton;
    lbVersion: TLabel;
    mmRegistered: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    lbBuild: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  procedure ShowAboutDialog;

implementation

{$R *.DFM}

procedure GetVersionInfo(Filename: string; var Version, Build: string);
var
  Len: UINT;        // length of structs returned from API calls
  Ptr: Pointer;     // points to version info structures
  InfoSize: DWORD;  // size of info buffer
  Dummy: DWORD;     // stores 0 in call to GetFileVersionInfoSize
  FixedFileInfo: TVSFixedFileInfo;
  PInfoBuffer: PChar;

begin
  FillChar(FixedFileInfo, SizeOf(FixedFileInfo), 0);
  InfoSize := GetFileVersionInfoSize(PChar(Filename), Dummy);
  if InfoSize > 0 then
  begin
    PInfoBuffer := StrAlloc(InfoSize);
    try
      if GetFileVersionInfo(PChar(Filename), Dummy, InfoSize, PInfoBuffer) then
      begin
        // Get fixed file info & copy to own storage
        VerQueryValue(PInfoBuffer, '\', Ptr, Len);
        FixedFileInfo := PVSFixedFileInfo(Ptr)^;

        Version := Format('%d.%d.%d',
          [
            HiWord(FixedFileInfo.dwFileVersionMS),
            LoWord(FixedFileInfo.dwFileVersionMS),
            HiWord(FixedFileInfo.dwFileVersionLS)
          ]);
        Build := Format('%d', [LoWord(FixedFileInfo.dwFileVersionLS)]);
      end;
    finally
      StrDispose(PInfoBuffer);
    end;
  end;
end;

procedure ShowAboutDialog;
begin
  with TfrmEzSplash.Create(nil) do
  try
    ShowModal;
  finally
    Destroy;
  end;
end;

procedure TfrmEzSplash.FormCreate(Sender: TObject);
var
  Filename, Version, Build: string;

begin
{$IFNDEF DEMO}
  mmRegistered.Visible := true;
{$ENDIF}
{$IFDEF EZ_VD4}
  Filename:='EzPlanitDS_D4.bpl';
{$ENDIF}
{$IFDEF EZ_VD5}
  Filename:='EzPlanitDS_D5.bpl';
{$ENDIF}
{$IFDEF EZ_VD6}
  Filename:='EzPlanitDS_D6.bpl';
{$ENDIF}
{$IFDEF EZ_VD7}
  Filename:='EzPlanitDS_D7.bpl';
{$ENDIF}
{$IFDEF EZ_VD8}
  Filename:='EzPlanitDS_D8.bpl';
{$ENDIF}
{$IFDEF EZ_VD2005}
  Filename:='EzPlanitDS_D2005.bpl';
{$ENDIF}
{$IFDEF EZ_VCB4}
  Filename:='EzPlanitDS_CB4.bpl';
{$ENDIF}
{$IFDEF EZ_VCB5}
  Filename:='EzPlanitDS_CB5.bpl';
{$ENDIF}
{$IFDEF EZ_VCB6}
  Filename:='EzPlanitDS_CB6.bpl';
{$ENDIF}
  GetVersionInfo(Filename, Version, Build);
  lbVersion.Caption := Version;
  lbBuild.Caption := Build;
end;

end.
