@echo off
set SAVEPATH=%PATH%
set PATH=D:\PROGRA~1\BORLAND\CBUILDER5\BIN;%PATH%
..\UpdateRcVersion EzPlanit_CB5.rc %1
..\UpdateRcVersion EzPlanitDs_CB5.rc %1
ECHO ******************************
ECHO Using EzPlanIT_CB5.mak
ECHO ******************************

if %2 == DEMO goto :DEMO
if %2 == REG goto :REG
goto :End

:DEMO
make -B -fEzPlanit_CB5_Demo.mak
make -B -fEzPlanitDs_CB5.mak
goto :END

:REG
make -B -fEzPlanIT_CB5.mak
make -B -fEzPlanitDs_CB5.mak
goto :End

:END
set PATH=%SAVEPATH%

