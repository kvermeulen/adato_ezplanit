object EzCalendarPanel: TEzCalendarPanel
  Left = 232
  Top = 209
  ClientHeight = 312
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  IsControl = True
  object pnlOuter: TPanel
    Left = 0
    Top = 0
    Width = 686
    Height = 312
    Align = alClient
    BevelOuter = bvNone
    ParentColor = True
    TabOrder = 0
    object gbDaySelector: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 312
      Align = alLeft
      Caption = 'Day selector'
      TabOrder = 0
      object Label4: TLabel
        Left = 8
        Top = 264
        Width = 178
        Height = 13
        Alignment = taCenter
        Caption = 'Green areas indicate working periods.'
        WordWrap = True
      end
      object lbWorkinghoursLabel: TLabel
        Left = 8
        Top = 280
        Width = 150
        Height = 13
        Caption = 'Working hours for selected day:'
      end
      object lbWorkingHours: TLabel
        Left = 163
        Top = 280
        Width = 6
        Height = 13
        Caption = '0'
      end
      object CalendarGrid: TEzCalendarGrid
        Left = 8
        Top = 48
        Width = 201
        Height = 209
        OnChange = CalendarGridChange
        OnDrawCell = CalendarGridDrawCell
        WeekStart = wdMonday
      end
      object dtpDate: TDateTimePicker
        Left = 9
        Top = 16
        Width = 201
        Height = 21
        CalAlignment = dtaRight
        Date = 38695.546928159720000000
        Time = 38695.546928159720000000
        DateFormat = dfLong
        DateMode = dmUpDown
        TabOrder = 1
        OnChange = dtpDateChange
      end
    end
    object pnlFill: TPanel
      Left = 217
      Top = 0
      Width = 4
      Height = 312
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
    end
    object pnlRules: TPanel
      Left = 221
      Top = 0
      Width = 465
      Height = 312
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 2
      object pnlWorkingHours: TPanel
        Left = 0
        Top = 0
        Width = 465
        Height = 49
        Align = alTop
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object rbRulesPerDate: TRadioButton
          Left = 24
          Top = 16
          Width = 161
          Height = 17
          Caption = 'Show rules for selected date'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = rbRulesPerDateClick
        end
        object rbAllRules: TRadioButton
          Left = 192
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Show all rules'
          TabOrder = 1
          OnClick = rbAllRulesClick
        end
      end
      object lvRules: TListView
        Left = 0
        Top = 49
        Width = 465
        Height = 232
        Align = alClient
        Columns = <
          item
            AutoSize = True
            Caption = 'Rule description'
          end>
        ReadOnly = True
        RowSelect = True
        TabOrder = 1
        ViewStyle = vsReport
      end
      object pnlBottom: TPanel
        Left = 0
        Top = 281
        Width = 465
        Height = 31
        Align = alBottom
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 2
        object btnEdit: TButton
          Left = 155
          Top = 3
          Width = 99
          Height = 25
          Action = acEditRule
          Anchors = [akTop, akRight]
          TabOrder = 1
        end
        object BtnDeleteRule: TButton
          Left = 259
          Top = 3
          Width = 99
          Height = 25
          Action = acDeleteRule
          Anchors = [akTop, akRight]
          TabOrder = 2
        end
        object btnWorkingHours: TButton
          Left = 363
          Top = 3
          Width = 99
          Height = 25
          Action = acSetWorkingHours
          Anchors = [akTop, akRight]
          TabOrder = 3
        end
        object btnAdd: TButton
          Left = 51
          Top = 3
          Width = 99
          Height = 25
          Action = acAddRule
          Anchors = [akTop, akRight]
          TabOrder = 0
        end
      end
    end
  end
  object ActionList1: TActionList
    Left = 272
    Top = 56
    object acAddRule: TAction
      Caption = '&Add rule ...'
      OnExecute = acAddRuleExecute
    end
    object acDeleteRule: TAction
      Caption = '&Delete rule'
      OnExecute = acDeleteRuleExecute
      OnUpdate = acDeleteRuleUpdate
    end
    object acSetWorkingHours: TAction
      Caption = '&Working hours ...'
      OnExecute = acSetWorkingHoursExecute
    end
    object acEditRule: TAction
      Caption = '&Edit ...'
      OnExecute = acEditRuleExecute
      OnUpdate = acEditRuleUpdate
    end
  end
end
