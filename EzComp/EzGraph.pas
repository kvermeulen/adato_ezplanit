unit EzGraph;

interface

{$I Ez.inc}

uses
	Windows, Messages, SysUtils, Classes, Forms, Graphics, Controls, Dialogs,
	StdCtrls, ExtCtrls, EzTimebar, Math, ComObj, ToolWin,
  Buttons, ComCtrls, EzGantt, EzDateTime, EzCalendar, EzArrays;

type
  TEzGraph = class;
  TEzGraphLine = class;

	TSelectionKind = (
    skNone,
    skHLinePart,
    skVLinePart,
    skRangeActive,
    skSetRange,
    skShiftRange
  );

  TGraphOption = (
    goGrid,
    goHSnap,
    goVSnap
  );
  TGraphOptions = set of TGraphOption;

  TEzPointList = array of TPoint;

  TEzGraphLineNotifier = class(TEzListNotifier)
  protected
    FOwner: TEzGraphLine;
    procedure Notify(AObject: TObject; Action: TEzListNotification); override;

  public
    constructor Create(AOwner: TEzGraphLine; Profile: TEzAvailabilityProfile); virtual;
  end;

  //
  // TEzGraphLine represents a single line in a TEzGraph.
  //
  // Description:
  //  TEzGraph uses a TEzGraphLines collection to store the lines to be
  //  displayed in the graph. Each line displays the contents of an availability
  //  profile which are implemented in the TEzAvailabilityProfile class.
  //
  TEzGraphLine = class(TCollectionItem)
  private
    FDescription: WideString;
    FCanEdit: Boolean;
    FVisible: Boolean;
    FOwnsProfile: Boolean;
    FPen: TPen;
    FBrush: TBrush;
    FProfile: TEzAvailabilityProfile;
    FChangeNotifyer: TEzGraphLineNotifier;
    FGroup: Integer;
    FTag: Integer;

  protected
    function  GetDisplayName: string; override;
    function  GetVisible: Boolean;
    procedure GraphicsObjectChange(Sender: TObject);
    procedure Notify(APoint: TEzAvailabilityPoint; Event: TEzListNotification);
    procedure RestoreDefaults; virtual;
    procedure SetVisible(Value: Boolean);
    procedure SetPen(Value: TPen);
    procedure SetBrush(Value: TBrush);
    procedure SetDescription(Desc: WideString);
    procedure SetProfile(Value: TEzAvailabilityProfile);

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

    property Profile: TEzAvailabilityProfile read FProfile write SetProfile;

  published
    property Description: WideString read FDescription write SetDescription;
    property Group: Integer read FGroup write FGroup default -1;
    property Visible: Boolean read GetVisible write SetVisible default False;
    property CanEdit: Boolean read FCanEdit write FCanEdit default False;
    property Pen: TPen read FPen write SetPen;
    property Brush: TBrush read FBrush write SetBrush;
    property Tag: Integer read FTag write FTag;
  end;

  TEzGraphLines = class(TCollection)
  private
    FGraph: TEzGraph;

  protected
    function  GetItem(Index: Integer): TEzGraphLine;
    procedure Update(Item: TCollectionItem); override;

  public
    function  Add: TEzGraphLine;
    function  GetOwner: TPersistent; override;

    constructor Create(Graph: TEzGraph; ColumnClass: TCollectionItemClass);
    property Graph: TEzGraph read FGraph;
    property Items[Index: Integer]: TEzGraphLine read GetItem; default;
  end;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TGraphTimebarLink = class(TEzTimebarLink)
    FGraph: TEzGraph;

  public
    constructor Create(AGraph: TEzGraph);

  protected
    procedure LayoutChanged; override;
    procedure DateChanged(OldDate: TDateTime); override;
    procedure UpdateState; override;
  end;

  TEzGraphScrollBarOptions = class(TPersistent)
  private
    FAlwaysVisible: TScrollStyle;
    FOwner: TEzGraph;
    FScrollBars: TScrollStyle;                   // used to hide or show vertical and/or horizontal scrollbar
    procedure SetAlwaysVisible(Value: TScrollStyle);
    procedure SetScrollBars(Value: TScrollStyle);

  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(AOwner: TEzGraph);
    procedure Assign(Source: TPersistent); override;
  published
    property AlwaysVisible: TScrollStyle read FAlwaysVisible write SetAlwaysVisible default ssNone;
    property ScrollBars: TScrollStyle read FScrollbars write SetScrollBars default ssBoth;
  end;

  TOverLineEvent = procedure(Sender: TObject; Line: TEzGraphLine; PointIndex: Integer; SelectionKind: TSelectionKind) of object;
  TOnLineMovedEvent = procedure(Sender: TObject; ALine: TEzGraphLine; SelectionKind: TSelectionKind; const SelectionPoints: TEzPointList) of object;
  TStepScaling = (ssManual, ssAutomatic, ssContinues);

  TEzGraphPanel = class(TCustomPanel)
    //
    // Implements the pane at which the graph is actually drawn.
    //
  protected
    FGraph: TEzGraph;
    FYAxLabelsWidth: Integer;
    FVScrollExtendMin, FVScrollExtendMax: Integer;

    procedure CMHintShow(var Message: TCMHintShow); message CM_HINTSHOW;
    procedure CMHintShowPause(var Message: TCMHintShowPause); message CM_HINTSHOWPAUSE;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DrawLineTo(ACanvas: TCanvas; Line: TEzGraphLine; Start, Stop: Integer; ExcludeClip: Boolean);
    procedure DrawPolyLine(Points: TEzPointlist);
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure WMHScroll(var Msg: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Msg: TWMHScroll); message WM_VSCROLL;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure ScrollGraph(dst: Integer);

    property Graph: TEzGraph read FGraph;
  end;

  TYAxPanel = class(TCustomPanel)
  private
    FDrawLine: Boolean;
    FDrawTicks: Boolean;
    FGraph: TEzGraph;
    FPen: TPen;
    FSnapSize: Double;
    FSpacing: Integer;
    FStepSize: Integer;
    FStepScaling: TStepScaling;
    FDrawInside: Boolean;

  protected
    procedure Paint; override;
    procedure SetDrawInside(Value: Boolean);
    procedure SetDrawLine(Value: Boolean);
    procedure SetDrawTicks(Value: Boolean);
    procedure SetPen(Value: TPen);
    procedure SetSpacing(Value: Integer);
    procedure SetStepSize(Value: Integer);
    procedure UpdateStepSize(Value: Integer);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

    property Graph: TEzGraph read FGraph;

    property DrawInside: Boolean read FDrawInside write SetDrawInside default false;
    property DrawLine: Boolean read FDrawLine write SetDrawLine default true;
    property DrawTicks: Boolean read FDrawTicks write SetDrawTicks default true;
    property Pen: TPen read FPen write SetPen;
    property SnapSize: Double read FSnapSize write FSnapSize;
    property Spacing: Integer read FSpacing write SetSpacing default 40;
    property StepSize: Integer read FStepSize write SetStepSize default 1;
    property StepScaling: TStepScaling read FStepScaling write FStepScaling default ssManual;
  end;

  TXAxHelper = class(TPersistent)
  private
    FGraph: TEzGraph;
    FPen: TPen;
    FPosition: Integer;

  protected
    procedure SetPen(Value: TPen);
    procedure SetPosition(Value: Integer);

  public
    constructor Create(AOwner: TEzGraph);
    destructor  Destroy; override;

    procedure Assign(Source: TPersistent); override;

    property Graph: TEzGraph read FGraph;

  published
    property Pen: TPen read FPen write SetPen;
    property Position: Integer read FPosition write SetPosition;
  end;

  TYAxHelper = class(TPersistent)
  private
    FPanel: TYAxPanel;

  protected
    function  GetColor: TColor;
    function  GetFont: TFont;
    function  GetDrawInside: Boolean;
    function  GetDrawLine: Boolean;
    function  GetDrawTicks: Boolean;
    function  GetPen: TPen;
    function  GetSnapSize: Double;
    function  GetSpacing: Integer;
    function  GetStepSize: Integer;
    function  GetStepScaling: TStepScaling;
    function  GetVisible: Boolean;
    function  GetWidth: Integer;

    procedure SetColor(Value: TColor);
    procedure SetFont(Value: TFont);
    procedure SetDrawInside(Value: Boolean);
    procedure SetDrawLine(Value: Boolean);
    procedure SetDrawTicks(Value: Boolean);
    procedure SetPen(Value: TPen);
    procedure SetSnapSize(Value: Double);
    procedure SetSpacing(Value: Integer);
    procedure SetStepSize(Value: Integer);
    procedure SetStepScaling(Value: TStepScaling);
    procedure SetVisible(Value: Boolean);
    procedure SetWidth(Value: Integer);

    procedure UpdateStepSize(Value: Integer);

  public
    constructor Create(APanel: TYAxPanel);
    procedure Assign(Source: TPersistent); override;
    procedure DefineProperties(Filer: TFiler); override;
    procedure SkipInt(Reader: TReader);

    function  CalcY(Units: Double): Integer;
    function  CalcUnits(Y: Integer): Double;
    function  CalcStepSize(MinY, MaxY: Integer): Integer;
    function  Snap(Value: Integer): Integer;

  published
    property Color: TColor read GetColor write SetColor;
    property Font: TFont read GetFont write SetFont;
    property DrawInside: Boolean read GetDrawInside write SetDrawInside default false;
    property DrawLine: Boolean read GetDrawLine write SetDrawLine default true;
    property DrawTicks: Boolean read GetDrawTicks write SetDrawTicks default true;
    property SnapSize: Double read GetSnapSize write SetSnapSize;
    property Spacing: Integer read GetSpacing write SetSpacing default 40;
    property StepSize: Integer read GetStepSize write SetStepSize default 1;
    property StepScaling: TStepScaling read GetStepScaling write SetStepScaling default ssManual;
    property Pen: TPen read GetPen write SetPen;
    property Visible: Boolean read GetVisible write SetVisible default true;
    property Width: Integer read GetWidth write SetWidth default 20;
  end;

  TEzGraphChangeEvent =
      procedure(Sender: TEzGraph; Line: TEzGraphline;
                Point: TEzAvailabilityPoint; Event: TEzListNotification) of Object;

  TEzGraphGetHintEvent =  procedure ( Sender: TEzGraph;
                                      Location: TPoint;
                                      Line: Integer;
                                      PointIndex: Integer;
                                      SelectionKind: TSelectionKind;
                                      var Hint: string) of object;
  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
	TEzGraph = class(TCustomControl)
	private
    FBorderStyle: TBorderStyle;
    FColorNegative: TColor;
    FGridPen: TPen;
    FHitMargin: Integer;
    FGraphPanel: TEzGraphPanel;
    FYAxPanel: TYAxPanel;
    FYaxHelper: TYAxHelper;
    FXAxHelper: TXAxHelper;
    FSavedX: Integer;
    FOptions: TGraphOptions;
    FTimebarLink: TGraphTimebarLink;
    FSelectionPoints: TEzPointList;
		FSelectionKind: TSelectionKind;
		FSelectedLinePart: Integer;
		FSelectedLine: TEzGraphLine;
    FSelectionStartY: Integer;
    FScrollbarOptions: TEzGraphScrollBarOptions;
    FMinDate: TDateTime;                    // Left date of graph
    FMaxDate: TDateTime;                    // Right side of graph
		FLines: TEzGraphLines;
    FStacked: Boolean;

    FSnapCount: integer;                    // Holds snap count (i.e. 5)
    FSnapScale: TScale;                     // Holds snap scale (scHours)

    FIsPrinting: Boolean;
    FUpdateCount: Integer;
    FOnChanging: TEzGraphChangeEvent;
    FOnChanged: TEzGraphChangeEvent;
    FOnOverLine: TOverLineEvent;
    FOnLineMoved: TOnLineMovedEvent;
    FOnGetHint: TEzGraphGetHintEvent;

  protected
    procedure DefineProperties(Filer: TFiler); override;
    procedure ReadVScale(Reader: TReader);
    procedure ReadVSnapSize(Reader: TReader);

    procedure AddPoint(var List: TEzPointList; P: TPoint);
    procedure CMShowingChanged(var Message: TMessage); message CM_SHOWINGCHANGED;
    procedure CreateParams(var Params: TCreateParams); override;
    function  DoGetHint(  Location: TPoint;
                          Line: Integer;
                          PointIndex: Integer;
                          SelectionKind: TSelectionKind): string;

    procedure DoLineChangeNotification(ALine: TEzGraphLine; APoint: TEzAvailabilityPoint; Event: TEzListNotification); virtual;
    procedure DoOnOverLine(Line: TEzGraphLine; PointIndex: Integer; SelectionKind: TSelectionKind); virtual;
    procedure DoLineMoved(ALine: TEzGraphLine; SelectionKind: TSelectionKind; const SelectionPoints: TEzPointList);
    procedure GetLinePoints(Line: TEzGraphLine; Start, Stop: Integer; var Points: TEzPointList); overload;
    procedure GetLinePoints(Line: TEzGraphLine; Start, Stop: Integer; var Points: TEzPointList; var PointIndex: Integer); overload;
    procedure GetLinePoints(Line: TEzGraphLine; LinePart: Integer; var Points: TEzPointList); overload;
    function  GetSnapSize: string;
    function  GetTimebar: TEzTimebar;
    procedure GraphDblClick(Sender: TObject);
    procedure GraphicsObjectChange(Sender: TObject);
		function  PointOnLine(var X, Y:Integer; Line: TEzGraphLine; var LinePart: Integer): TSelectionKind;
		procedure FindLine(var X, Y:Integer; var Index: Integer;
                var PointIndex: Integer; var SelectionKind: TSelectionKind;
                IgnoreEditFlag : boolean = false);
    procedure PaintLegendBitmap(Scale: Double);
    procedure ScrollHorizontal(var Msg: TWMHScroll);
    procedure ScrollVertical(var Msg: TWMHScroll);
    procedure ScollTimerTimer(Sender: TObject);
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure SetColor(Value: TColor);
    procedure SetColorNegative(Value: TColor);
    procedure SetGridPen(Value: TPen);
    procedure SetLines(Value: TEzGraphLines);
    procedure SetMinDate(D: TDateTime);
    procedure SetMaxDate(D: TDateTime);
    procedure SetScrollbarOptions(Value: TEzGraphScrollBarOptions);
    procedure SetSnapSize(S: string);
    procedure SetStacked(Value: Boolean);
    procedure SetTimebar(Bar: TEzTimebar);
    procedure SetOptions(Value: TGraphOptions);
    procedure SetXAxHelper(Value: TXAxHelper);
    procedure SetYAxHelper(Value: TYAxHelper);
    procedure TimebarLayoutChanged;
    procedure TimebarDateChanged(OldDate: TDateTime);
    procedure UpdateTimebarState;
    procedure UpdateHorizontalScrollbar;
    procedure UpdateVerticalScrollbar;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;

	public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure BeginUpdate;
    procedure EndUpdate;
    procedure Invalidate; override;
    procedure OpenSetupDialog;
    procedure PrintTo(ACanvas: TCanvas; ARect: TRect; Dpi: Integer);
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    function  UpdateStepSize(Start, Stop: TDateTime) : Boolean;

    property SnapCount: integer read FSnapCount write FSnapCount;
    property SnapScale: TScale read FSnapScale write FSnapScale;

    property GraphPanel: TEzGraphPanel read FGraphPanel write FGraphPanel;

  published
    property Color write SetColor;
    property ColorNegative: TColor read FColorNegative write SetColorNegative default clBtnFace;
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsSingle;
    property Hint;
    property ShowHint;
    property HitMargin: Integer read FHitMargin write FHitMargin default 5;
    property Lines: TEzGraphLines read FLines write SetLines;
    property MinDate: TDateTime read FMinDate write SetMinDate;
    property MaxDate: TDateTime read FMaxDate write SetMaxDate;
    property OnChanging: TEzGraphChangeEvent read FOnChanging write FOnChanging;
    property OnChanged: TEzGraphChangeEvent read FOnChanged write FOnChanged;
    property OnOverLine : TOverLineEvent read FOnOverLine write FOnOverLine;
    property OnLineMoved: TOnLineMovedEvent read FOnLineMoved write FOnLineMoved;
    property OnGetHint: TEzGraphGetHintEvent read FOnGetHint write FOnGetHint; 

    property Options: TGraphOptions read FOptions write SetOptions
      default [goGrid, goHSnap, goVSnap];
    property ScrollbarOptions: TEzGraphScrollBarOptions read FScrollbarOptions write SetScrollbarOptions;
    property SnapSize: string read GetSnapSize write SetSnapSize;
    property Stacked: Boolean read FStacked write SetStacked default False;
    property Xax: TXAxHelper read FXAxHelper write SetXAxHelper;
    property Yax: TYAxHelper read FYAxHelper write SetYAxHelper;

    property Timebar: TEzTimebar read GetTimebar write SetTimebar;
    property GridPen: TPen read FGridPen write SetGridPen;

    property Align;
    property Font;
    property PopupMenu;
    property Visible;

//    property OnMouseDown;
//    property OnMouseUp;
//    property OnMouseMove;
	end;

var
  crSizeScale: Integer;

implementation

{$R EzGraph.RES}

uses EzPainting;

//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
procedure TEzGraphPanel.CMHintShowPause(var Message: TCMHintShowPause);
begin

end;

constructor TEzGraphPanel.Create(AOwner: TComponent);
begin
  inherited;
  FGraph := AOwner as TEzGraph;
  FYAxLabelsWidth := 0;
  FVScrollExtendMin := 0;
  FVScrollExtendMax := 0;
end;

destructor TEzGraphPanel.Destroy;
begin
  inherited;
end;

procedure TEzGraphPanel.CMHintShow(var Message: TCMHintShow);
var
  IsFocused: Boolean;
  ParentForm: TCustomForm;
  Pt: TPoint;
  Line: Integer;
  sk : TSelectionKind;
  PointIndex: Integer;

begin
  with Message do
  begin
    Result := 1;

    // Make sure a hint is only shown if the tree or at least its parent form is active. Active editing is ok too.
    ParentForm := GetParentForm(Self);
    if Assigned(ParentForm) then
      IsFocused := ParentForm.Focused or (Screen.ActiveForm = ParentForm) else
      IsFocused := False;

    if (GetCapture = 0) and Graph.ShowHint and IsFocused then
    begin
      Pt := ScreenToClient(Mouse.CursorPos);
      Graph.FindLine(Pt.X, Pt.Y, Line, PointIndex, sk, True);
      HintInfo.HintStr := Graph.DoGetHint(Pt, Line, PointIndex, sk);
      if HintInfo.HintStr <> '' then
      begin
        Result := 0;
        HintInfo.HideTimeout := 10000;
        HintInfo.CursorRect := Rect(Pt.X - 2, Pt.Y - 2, Pt.X + 2, Pt.Y + 2);
      end;
    end;
  end;
end;

procedure TEzGraphPanel.CreateParams(var Params: TCreateParams);
const
  ScrollBar: array[TScrollStyle] of Cardinal = (0, WS_HSCROLL, 0 {WS_VSCROLL}, WS_HSCROLL {or WS_VSCROLL});

begin
  inherited CreateParams(Params);
  Params.Style := Params.Style or ScrollBar[Graph.ScrollbarOptions.ScrollBars];
end;

procedure TEzGraphPanel.DrawLineTo(ACanvas: TCanvas; Line: TEzGraphLine; Start, Stop: Integer; ExcludeClip: Boolean);
var
  Points: TEzPointList;
  iPoint: Integer;
  ARect: TRect;

begin
  if Line = nil then Exit;
  with ACanvas do
  try
    Graph.GetLinePoints(Line, Start, Stop, Points);

    // Fill area beneeth graph line
    if (Line.Brush.Style <> bsClear) then
    begin
      Brush := Line.Brush;
      iPoint := 0;
      while iPoint <= High(Points) do
      begin
        if Points[iPoint].y <> Graph.XAx.Position then
        begin
          Setrect(ARect, Points[iPoint].x, Points[iPoint].y, Points[iPoint+1].x, Graph.XAx.Position);
          if not IsRectEmpty(ARect) then
          begin
            FillRect(ARect);
            if ExcludeClip then
              ExcludeClipRect(ACanvas.Handle, ARect.Left, ARect.Top, ARect.Right, ARect.Bottom);
          end;
        end;
        Inc(iPoint, 2);
      end;
    end;

    if (Line.Pen.Style <> psClear) then
    begin
      Pen := Line.Pen;
      PolyLine(Points);
    end;
  finally
  end;
end;

procedure TEzGraphPanel.DrawPolyLine(Points: TEzPointlist);
begin
  Canvas.PolyLine(Points);
end;

procedure TEzGraphPanel.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  StartX, StartY, LineIndex: Integer;
  sk : TSelectionKind;

begin
  with Graph do
  begin
    if Timebar=nil then Exit;

    if goVSnap in Options then
      Y := FYAxHelper.Snap(Y);

    StartX := X;
    StartY := Y;
    FindLine(StartX, StartY, LineIndex, FSelectedLinePart, sk);
    if (FSelectionKind = skRangeActive) then
    begin
      if (X > FSelectionPoints[0].X) and
         (X < FSelectionPoints[High(FSelectionPoints)].X) and
         (sk <> skNone)
      then
      begin
        // Start shifting the range set by the user
        FSelectionKind := skShiftRange;
        // Mark the start position
        FSelectionStartY := Y;
      end
      else
        with Self.Canvas do
        begin
          Pen.Color := clYellow;
          Pen.Mode := pmXor;
          DrawPolyLine(FSelectionPoints);;
          FSelectionPoints := nil;
          FSelectionKind := skNone;
        end; // with
    end
    else
    if sk <> skNone then
      if ssShift in Shift then
      begin
        FSelectionKind := skSetRange;
        FSelectedLine := FLines[LineIndex];
        SetLength(FSelectionPoints, 1);
        if (goHSnap in Options) and (SnapCount > 0) then
          X := Graph.Timebar.DateTime2X(RoundDate(SnapScale, SnapCount, 0, Graph.Timebar.X2DateTime(X)));
        FSelectionPoints[0] := Point(X, Y);
//        FSelectionPoints[1] := Point(X, 0);
      end
      else
      with Self.Canvas do
      begin
        // Activate a timer when moving horizontally to let the screen follow
        // the mouse cursor
//      if sk = skVLinePart then
//        ScrollTimer.Enabled := True;
        FSelectedLine := FLines[LineIndex];
        FSelectionKind := sk;
        Pen.Color := clYellow;
        Pen.Mode := pmXor;
        FSelectionPoints := nil;
        if sk = skVLinePart then
          GetLinePoints(FSelectedLine, StartX, StartX, FSelectionPoints, FSelectedLinePart) else
          GetLinePoints(FSelectedLine, FSelectedLinePart, FSelectionPoints);
        DrawPolyLine(FSelectionPoints);
      end;
  end;
end;

procedure TEzGraphPanel.MouseMove(Shift: TShiftState; X, Y: Integer);
var
	LinePart: Integer;
  IClientData, I, P: Integer;
  sk: TSelectionKind;
  NewCursor: TCursor;
  Line: TEzGraphLine;

begin
  if Graph.Timebar=nil then Exit;
  NewCursor := crDefault;

  with Graph do
    case FSelectionKind of
      skNone:
      begin
        FindLine(X, Y, IClientData, LinePart, sk, true);
        if (sk <> skNone) and (ssShift in Shift) then
          NewCursor := crCross
        else
          case sk of
            skHLinePart:
            begin
              line := self.FGraph.FLines.GetItem(IClientData);
              Graph.DoOnOverLine(Line, LinePart, sk);
              if line.FCanEdit then //vna 25/1/4
                NewCursor := crVSplit;
            end;

            skVLinePart:
            begin
              line := self.FGraph.FLines.GetItem(IClientData);   //vna 25/1/4
              //Must use next availability point for date
              Graph.DoOnOverLine(line, LinePart, sk); //vna 25/1/4
              if line.FCanEdit then  //vna 25/1/4
                NewCursor := crHSplit;
            end;
          end;
      end;

      skRangeActive:
        if (X > FSelectionPoints[0].X) and
           (X < FSelectionPoints[High(FSelectionPoints)].X) and
           (PointOnLine(X, Y, FSelectedLine, LinePart) <> skNone)
        then
          NewCursor := crSizeNS;

      skSetRange:
        with Self.Canvas do
        begin
          if (goHSnap in Options) and (SnapCount > 0) then
            X := Timebar.DateTime2X(RoundDate(SnapScale, SnapCount, 0, Timebar.X2DateTime(X)));
          Pen.Color := clYellow;
          Pen.Mode := pmXor;
          DrawPolyLine(FSelectionPoints);
          I := FSelectionPoints[0].X;
          P := FSelectionPoints[High(FSelectionPoints)].X;
          FSelectionPoints := nil;
          if X < I then
            GetLinePoints(FSelectedLine, X, P, FSelectionPoints) else
            GetLinePoints(FSelectedLine, I, X, FSelectionPoints);
          DrawPolyLine(FSelectionPoints);
          NewCursor := crCross;
        end; // With

      skShiftRange:
        with Self.Canvas do
        begin
          if goVSnap in FOptions then
            Y := FYAxHelper.Snap(Y);

          Pen.Color := clYellow;
          Pen.Mode := pmXor;
          DrawPolyLine(FSelectionPoints);
          // Shift all points vertically
          for I:=0 to High(FSelectionPoints) do
            FSelectionPoints[I] := Point(FSelectionPoints[I].X, FSelectionPoints[I].Y + Y - FSelectionStartY);
          FSelectionStartY := Y;
          Graph.DoLineMoved(FSelectedLine, FSelectionKind, FSelectionPoints);
          DrawPolyLine(FSelectionPoints);
          NewCursor := crSizeNS;
        end;

      skHLinePart:
        with Self.Canvas do
        begin
          if goVSnap in FOptions then
            Y := FYAxHelper.Snap(Y);

          Pen.Color := clYellow;
          Pen.Mode := pmXor;
          DrawPolyLine(FSelectionPoints);
          // Shift line vertically
          for I:=0 to High(FSelectionPoints) do
            FSelectionPoints[I] := Point(FSelectionPoints[I].X, Y);
          Graph.DoLineMoved(FSelectedLine, FSelectionKind, FSelectionPoints);
          DrawPolyLine(FSelectionPoints);
          NewCursor := crHSplit;
        end;

      skVLinePart:
        with Graph.Timebar, Self.Canvas do
        begin
          if (goHSnap in Options) and (SnapCount > 0) then
            X := DateTime2X(RoundDate(SnapScale, SnapCount, 0, X2DateTime(X)));
          Pen.Color := clYellow;
          Pen.Mode := pmXor;
          DrawPolyLine(FSelectionPoints);
          FSelectionPoints[0] := Point(X, FSelectionPoints[0].Y);
          FSelectionPoints[1] := Point(X, FSelectionPoints[1].Y);;
          Graph.DoLineMoved(FSelectedLine, FSelectionKind, FSelectionPoints);
          DrawPolyLine(FSelectionPoints);
          NewCursor := crVSplit;
        end;
    end; // case

  Cursor := NewCursor;
end;

procedure TEzGraphPanel.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  StartDate, StopDate: TDateTime;
  d_unit: Double;
  P: TEzAvailabilityPoint;

begin
  with Graph do
  begin
    if goVSnap in FOptions then
      Y := FYAxHelper.Snap(Y);
    if (goHSnap in Options) and (SnapCount > 0) then
      X := Timebar.DateTime2X(RoundDate(SnapScale, SnapCount, 0, Timebar.X2DateTime(X)));

    case FSelectionKind of
      skHLinePart:
        with Self.Canvas, FSelectedLine.Profile do
        try
          Pen.Color := clYellow;
          Pen.Mode := pmXor;
  	  		DrawPolyLine(FSelectionPoints);
          d_unit := FYAxHelper.CalcUnits(Y);
          if goVSnap in FOptions then
            d_unit := RoundUnits(d_unit, FYAxHelper.SnapSize);

          StartDate := Items[FSelectedLinePart].Datetime;
          Add(StartDate, d_Unit, []);
        finally
          FSelectedLine := nil;
          FSelectionKind := skNone;
        end; // With

      skVLinePart:
        with Self.Canvas, FSelectedLine.Profile do
        try
          Pen.Color := clYellow;
          Pen.Mode := pmXor;
          DrawPolyLine(FSelectionPoints);

          StartDate := Items[FSelectedLinePart].Datetime;
          if X < Timebar.DateTime2X(StartDate) then // Going left
          begin
            d_unit := Items[FSelectedLinePart].Units;
            SetInterval(Timebar.X2DateTime(X), StartDate, d_unit, []);
          end
          else
          begin
            P := Items[FSelectedLinePart-1];
            SetInterval(P.DateValue, DatetimeToEzDatetime(Timebar.X2DateTime(X)), P.Units, []);
          end;
        finally
          FSelectedLine := nil;
          FSelectionKind := skNone;
        end; // With

      skSetRange:
        FSelectionKind := skRangeActive;

      skShiftRange:
        with Self.Canvas, FSelectedLine.Profile do
        try
          Pen.Color := clYellow;
          Pen.Mode := pmXor;
  	  		DrawPolyLine(FSelectionPoints);

          StartDate := Timebar.X2DateTime(FSelectionPoints[0].X);
          if (goHSnap in Options) and (SnapCount > 0) then
            StartDate := RoundDate(SnapScale, SnapCount, 0, StartDate);

          StopDate := Timebar.X2DateTime(FSelectionPoints[High(FSelectionPoints)].X);
          if (goHSnap in Options) and (SnapCount > 0) then
            StopDate := RoundDate(SnapScale, SnapCount, 0, StopDate);

          d_unit := FYAxHelper.CalcUnits(FSelectionPoints[0].Y)-Units[StartDate];
          if goVSnap in FOptions then
            d_unit := RoundUnits(d_unit, FYAxHelper.SnapSize);

          SumInterval(
                StartDate,
                StopDate,
                d_unit,
                True // AllowNegative
            );
        finally
          FSelectedLine := nil;
          FSelectionKind := skNone;
        end; // With
    end;
  end;
end;

function TEzGraph.UpdateStepSize(Start, Stop: TDateTime) : Boolean;
var
  i: Integer;
  MaxStep: Integer;
  Points: TEzPointList;
  startPos: Integer;
  stopPos: Integer;
  ts: TEzTimespan;


  function CalcStepSize : Integer;
  var
    Y, i, MinY, MaxY: Integer;

  begin
    MinY := MaxInt;
    MaxY := -MaxInt;

    for i:=0 to High(Points) do
    begin
      Y := Points[i].Y;
      MinY := min(MinY, Y);
      MaxY := max(MaxY, Y);
    end;

    Result := FYaxHelper.CalcStepSize(MinY, MaxY);
  end;

begin
  Result := False;
  MaxStep := 0;
  for i:=0 to FLines.Count-1 do
  begin
    with FLines[i] do
      if Visible and (Profile<>nil) and (not FStacked or (Group=-1)) then
      begin
        ts.Start := DatetimeToEzDatetime(Start);
        ts.Stop := DatetimeToEzDatetime(Stop);
        Profile.PrepareDateRange(ts);
        startPos := Timebar.DateTime2X(Start);
        stopPos := Timebar.DateTime2X(Stop);
        GetLinePoints(FLines[i], startPos, stopPos, Points);
        MaxStep := Max(MaxStep, CalcStepSize);
      end;
  end;

  if MaxStep <> FYaxHelper.StepSize then
  begin
    FYaxHelper.StepSize := MaxStep;
    Result := True;
  end;
end;

procedure TEzGraphPanel.Paint;
var
	i, y, YTick: integer;
	YPos, TH: Integer;
  DrawRect, R: TRect;
  str: string;
  dtLeft, dtRight: TEzDateTime;
  ts: TEzTimespan;
  Points: TEzPointList;

  procedure DrawStacked;
  var
    StackedLine: TEzGraphLine;
//xx    EnumData: IEnumAvPoints;
    CurrentGroup, LastGroup: Integer;
//    dblDummy: Double;
    ClipRgn: HRGN;

  begin
    LastGroup := -1;
    ClipRgn := 0;
    StackedLine := nil;

    with Graph do
    repeat
      CurrentGroup := MaxInt;
      i := 0;
      while (i < Lines.Count) and (LastGroup+1 <> CurrentGroup) do
      begin
        with Lines[i] do
          if Visible and (Profile<>nil) and (Group > LastGroup) then
            CurrentGroup := min(CurrentGroup, Group);
        Inc(i);
      end;

      if CurrentGroup <> MaxInt then
      begin
        i := 0;

        // First time initialisation
        if StackedLine = nil then
        begin
          StackedLine := TEzGraphLine.Create(nil);
//xx          StackedLine.IGraph := CoPiAvailability.Create;
//xx          StackedLine.IGraph.AddPoint(0, 0.0);
          ClipRgn := CreateRectRgnIndirect(DrawRect);
        end
        else
        begin
          // Clear data stored when drawing previous group
//xx          StackedLine.IGraph.Clear;
//xx          StackedLine.IGraph.AddPoint(0, 0.0);
          // Reset cliping region to it's original size;
          SelectClipRgn(Canvas.Handle, ClipRgn);
        end;

        while (i < Lines.Count) do
        begin
          with Lines[i] do
            if Visible and (Profile<>nil) and (Group = CurrentGroup) then
            begin
//xx              IGraph.EnumPoints(dtLeft, dtRight, IUnknown(EnumData));
//xx              StackedLine.IGraph.SumRange(EnumData, dtRight, opAdd, dblDummy);
              StackedLine.Brush.Assign(Lines[i].Brush);
              StackedLine.Pen.Assign(Lines[i].Pen);
              DrawLineTo(Self.Canvas, StackedLine, DrawRect.Left, DrawRect.Right, True);
//xx              EnumData := nil;
            end;
          Inc(i);
        end;
      end;

      LastGroup := CurrentGroup;
    until CurrentGroup=MaxInt;

    if StackedLine <> nil then
    begin
      StackedLine.Destroy;
      DeleteObject(ClipRgn);
    end;
  end;

  function CalcStepSize : Integer;
  var
    Y, i, MinY, MaxY: Integer;

  begin
    MinY := MaxInt;
    MaxY := -MaxInt;

    for i:=0 to High(Points) do
    begin
      Y := Points[i].Y;
      MinY := min(MinY, Y);
      MaxY := max(MaxY, Y);
    end;

    Result := Graph.FYaxHelper.CalcStepSize(MinY, MaxY);
  end;

begin
	with Canvas do
	begin
    DrawRect := ClipRect;
    if IsRectEmpty(DrawRect) then
      Exit;

    // Update Y-ax scaling factor
    if not (csDesigning in ComponentState) and Assigned(Graph.Timebar) and
           (Graph.FYaxHelper.StepScaling in [ssContinues, ssAutomatic])
    then
    begin
      if Graph.UpdateStepSize(Graph.Timebar.X2DateTime(DrawRect.Left), Graph.Timebar.X2DateTime(DrawRect.Right)) then
        Exit;
    end;

    FYAxLabelsWidth := 0;

    if Graph.FYAxPanel.DrawInside then
      Font := Graph.FYAxPanel.Font;

    TH := TextHeight('0');

    // YTick holds the label on the Y-ax for the current line
    YTick := Trunc(TruncUnits(Graph.FYAxHelper.CalcUnits(DrawRect.Top), Graph.FYAxHelper.StepSize));

    // Rounded y-position
    YPos := Graph.FYAxHelper.CalcY(YTick);

    if DrawRect.Top < YPos then
    begin
      inc(YTick, Graph.FYAxHelper.StepSize);
      dec(YPos, Graph.FYAxHelper.Spacing);
    end;

    y := Graph.FYAxHelper.CalcY(Trunc(Graph.FYAxHelper.CalcUnits(DrawRect.Bottom)));

    if DrawRect.Bottom > y then
      DrawRect.Bottom := y + Graph.FYAxHelper.Spacing else
      DrawRect.Bottom := y;

    //
    // Fill area below x-ax
    //
    SetRect(
          R,
          DrawRect.Left,
          max(DrawRect.Top, Graph.XAx.Position),
          DrawRect.Right,
          max(DrawRect.Bottom, Graph.XAx.Position)
      );

    if not IsRectEmpty(R) then
    begin
      Brush.Color := Graph.ColorNegative;
      FillRect(R);
    end;

    //
    // Fill area above x-ax
    //
    SetRect(
          R,
          DrawRect.Left,
          min(DrawRect.Top, Graph.XAx.Position),
          DrawRect.Right,
          min(DrawRect.Bottom, Graph.XAx.Position)
      );

    if not IsRectEmpty(R) then
    begin
      Brush.Color := Graph.Color;
      FillRect(R);
    end;

    //
    // Paint grid lines
    //
    if goGrid in Graph.Options then
    begin
      Pen := Graph.GridPen;
  		while (YPos <= DrawRect.Bottom) do
  		begin
        if YTick = 0 then
          Pen := Graph.FXAxHelper.Pen;

        MoveTo(DrawRect.Left, YPos);
        LineTo(DrawRect.Right, YPos);

        if YTick = 0 then
          Pen := Graph.GridPen;

        //
        // Draw tick label
        //
        if Graph.FYAxPanel.DrawInside then
        begin
          str := IntToStr(YTick);
          SetRect(R, 4, YPos-TH, Width, YPos);
          EzRenderText(Canvas, R, str, tsNormal, 0);
          FYAxLabelsWidth := max(4 + TextWidth(str), FYAxLabelsWidth);
        end;

        dec(YTick, Graph.FYAxHelper.StepSize);
        inc(YPos, Graph.FYAxHelper.Spacing);
      end;
    end else
      //
      // draw at least the x-ax
      //
    begin
      Pen := Graph.FXAxHelper.Pen;
      MoveTo(DrawRect.Left, Graph.XAx.Position);
      LineTo(DrawRect.Right, Graph.XAx.Position);
    end;

    if not (csDesigning in ComponentState) and Assigned(Graph.Timebar)then
      with Graph do
      begin
        dtLeft := DatetimeToEzDatetime(Timebar.X2DateTime(DrawRect.Left));
        dtRight := DatetimeToEzDatetime(Timebar.X2DateTime(DrawRect.Right));

// xx        if FStacked then DrawStacked;

        for i:=0 to FLines.Count-1 do
          with FLines[i] do
            if Visible and (Profile<>nil) and (not FStacked or (Group=-1)) then
            begin
              ts.Start := dtLeft;
              ts.Stop := dtRight;
              Profile.PrepareDateRange(ts);
              DrawLineTo(Self.Canvas, FLines[i], DrawRect.Left, DrawRect.Right, False);
            end;
      end;
  end;
end;

procedure TEzGraphPanel.ScrollGraph(dst: Integer);
var
  R: TRect;

begin
  R := ClientRect;
  R.Left := FYAxLabelsWidth;
  ScrollWindowEx(Handle, dst, 0, @R, @R, 0, nil, SW_Invalidate);

  if FYAxLabelsWidth > 0 then
  begin
    R.Left := 0;
    R.Right := FYAxLabelsWidth;
    InvalidateRect(Handle, @R, True);
  end;
end;

procedure TEzGraphPanel.WMHScroll(var Msg: TWMHScroll);
begin
  Graph.ScrollHorizontal(Msg);
end;

procedure TEzGraphPanel.WMVScroll(var Msg: TWMHScroll);
begin
  Graph.ScrollVertical(Msg);
end;

//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
constructor TYAxPanel.Create(AOwner: TComponent);
begin
  inherited;

  FGraph := AOwner as TEzGraph;
  FPen := TPen.Create;
  Width := 20;
  FDrawLine := true;
  FDrawTicks := true;
  FSnapSize := 0.25;
  FSpacing := 40;
  FStepSize := 1;
  FStepScaling := ssManual;  
end;

destructor TYAxPanel.Destroy;
begin
  inherited;
  FPen.Destroy;
end;

procedure TYAxPanel.Assign(Source: TPersistent);
begin
  if Source is TYAxPanel then
  begin
    Width := TYAxPanel(Source).Width;
    FDrawLine := TYAxPanel(Source).DrawLine;
    FDrawTicks := TYAxPanel(Source).DrawTicks;
    FPen.Assign(TYAxPanel(Source).Pen);
    FSnapSize := TYAxPanel(Source).SnapSize;
    FSpacing := TYAxPanel(Source).Spacing;
    FStepSize := TYAxPanel(Source).StepSize;
    FStepScaling := TYAxPanel(Source).StepScaling;
    FDrawInside := TYAxPanel(Source).DrawInside;
  end else
    inherited;
end;

procedure TYAxPanel.Paint;
var
  TH, YTick, YPos, y, W, MaxHeight: Integer;
  DrawRect: TRect;
  str: string;

begin
	with Canvas do
	begin
    Pen := Self.Pen;
    Font := Self.Font;
    Brush.Color := Color;

    TH := TextHeight('0');
    W := ClientWidth;

    DrawRect := ClipRect;
    FillRect(DrawRect);

    MaxHeight := Graph.GraphPanel.ClientRect.Bottom;
    if DrawRect.Bottom > MaxHeight then
      //
      // do not extend y-ax below bottom of graph
      //
      DrawRect.Bottom := MaxHeight;

    // YTick holds the label on the Y-ax for the current line
    YTick := Trunc(TruncUnits(Graph.FYAxHelper.CalcUnits(DrawRect.Top), Graph.FYAxHelper.StepSize));

    // Rounded y-position
    YPos := Graph.FYAxHelper.CalcY(YTick);

    if DrawRect.Top < YPos then
    begin
      inc(YTick, Graph.FYAxHelper.StepSize);
      dec(YPos, Graph.FYAxHelper.Spacing);
    end;

    // Align bottom of update rect with grid line
    y := Graph.FYAxHelper.CalcY(Trunc(Graph.FYAxHelper.CalcUnits(DrawRect.Bottom)));

    if DrawRect.Bottom > y then
      DrawRect.Bottom := y + Graph.FYAxHelper.Spacing else
      DrawRect.Bottom := y;

    // Draw y-ax line
    if DrawLine then
    begin
      MoveTo(W-Pen.Width, DrawRect.Top);
      LineTo(W-Pen.Width, math.min(y, MaxHeight));
    end;

//    if not DrawInside then
      while (YPos <= DrawRect.Bottom) do
    	begin
        // Draw tick
        if DrawTicks then
        begin
          MoveTo(W-3, YPos);
          LineTo(W, YPos);
        end;
        str := IntToStr(YTick);
        TextOut(W-Pen.Width-TextWidth(str)-4, YPos-(TH div 2), str);
        dec(YTick, Graph.FYAxHelper.StepSize);
        inc(YPos, Graph.FYAxHelper.Spacing);
      end;
    end;
end;

procedure TYAxPanel.SetDrawInside(Value: Boolean);
begin
  if DrawInside <> Value then
  begin
    FDrawInside := Value;
    Invalidate;
  end;
end;

procedure TYAxPanel.SetDrawLine(Value: Boolean);
begin
  if FDrawLine <> Value then
  begin
    FDrawLine := Value;
    Invalidate;
  end;
end;

procedure TYAxPanel.SetDrawTicks(Value: Boolean);
begin
  if FDrawTicks <> Value then
  begin
    FDrawTicks := Value;
    Invalidate;
  end;
end;

procedure TYAxPanel.SetPen(Value: TPen);
begin
  FPen.Assign(Value);
  if not (csLoading in Graph.ComponentState) and HandleAllocated then
    Invalidate;
end;

procedure TYAxPanel.SetSpacing(Value: Integer);
begin
  if Value<>FSpacing then
  begin
    FSpacing := Value;
    Invalidate;
    Graph.Invalidate;
  end;
end;

procedure TYAxPanel.SetStepSize(Value: Integer);
begin
  Value := Math.max(Value, 1);

  if Value<>FStepSize then
  begin
    FStepSize := Value;
    Invalidate;
    Graph.Invalidate;
  end;
end;

procedure TYAxPanel.UpdateStepSize(Value: Integer);
begin
  FStepSize := Value;
end;


//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
constructor TXAxHelper.Create(AOwner: TEzGraph);
begin
  FGraph := AOwner;
  FPen := TPen.Create;
  FPosition := FGraph.Height - 30;
end;

destructor TXAxHelper.Destroy;
begin
  inherited;
  FPen.Destroy;
end;

procedure TXAxHelper.Assign(Source: TPersistent);
begin
  if Source is TXAxHelper then
  begin
    FPen.Assign(TXAxHelper(Source).Pen);
  end else
    inherited;
end;

procedure TXAxHelper.SetPen(Value: TPen);
begin
  FPen.Assign(Value);
  Graph.GraphPanel.Invalidate;
end;

procedure TXAxHelper.SetPosition(Value: Integer);
begin
  if Value <> FPosition then
  begin
    FPosition := Value;
    with Graph do
    begin
      UpdateVerticalScrollbar;
      FYaxPanel.Invalidate;
      GraphPanel.Invalidate;
    end;
  end;
end;

//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
constructor TYAxHelper.Create(APanel: TYAxPanel);
begin
  FPanel := APanel;
end;

procedure TYAxHelper.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('Max', SkipInt, nil, false);
  Filer.DefineProperty('Min', SkipInt, nil, false);
end;

procedure TYAxHelper.SkipInt(Reader: TReader);
begin
  Reader.ReadInteger;
end;

procedure TYAxHelper.Assign(Source: TPersistent);
begin
  if Source is TYAxHelper then
  begin
    FPanel.FDrawInside := TYAxHelper(Source).DrawInside;
    FPanel.FDrawLine := TYAxHelper(Source).DrawLine;
    FPanel.FDrawTicks := TYAxHelper(Source).DrawTicks;
    FPanel.FPen.Assign(TYAxHelper(Source).Pen);
    FPanel.FSnapSize := TYAxHelper(Source).SnapSize;
    FPanel.FSpacing := TYAxHelper(Source).Spacing;
    FPanel.FStepSize := TYAxHelper(Source).StepSize;
    FPanel.FStepScaling := TYAxHelper(Source).StepScaling;
  end else
    inherited;
end;

function TYAxHelper.CalcY(Units: Double): Integer;
begin
  Result := FPanel.Graph.XAx.Position-Round(Units*Spacing/StepSize);
end;

function TYAxHelper.CalcUnits(Y: Integer): Double;
begin
  Result := -(Y-FPanel.Graph.XAx.Position)*StepSize/Spacing;
end;

function TYAxHelper.CalcStepSize(MinY, MaxY: Integer): Integer;
var
  R1, R2, L: Integer;

begin
  if MinY<FPanel.Graph.XAx.Position then
  begin
    if FPanel.Graph.XAx.Position>0 then
      // Calc value for upper limit
      R1 := Ceil(CalcUnits(MinY)/(FPanel.Graph.XAx.Position/Spacing)) else
      R1 := StepSize;
  end else
    R1 := 1;

  if MaxY>FPanel.Graph.XAx.Position then
    R2 := Ceil(CalcUnits(MaxY)/((FPanel.Graph.Height-FPanel.Graph.XAx.Position)/Spacing)) else
    R2 := 1;
    
  // Calc value for lower limit
  Result := Math.max(R1, R2);

  if Result>=10 then
  begin
    R1:=Result;
    L := Trunc(Log10(Result));
    Result := Trunc(RoundTo(Result, L));
    if Result<R1 then
      inc(Result, Trunc(Power(10, L)));
  end
  else if Result>=5 then
    Result := 10
  else if Result>2 then
    Result:=5;
end;

function TYAxHelper.Snap(Value: Integer): Integer;
var
  X: Integer;

begin
  X := FPanel.Graph.XAx.Position;
  Result := Round(Round((Value-X)/(Spacing*SnapSize))*SnapSize*Spacing)+X;
end;

function TYAxHelper.GetColor: TColor;
begin
  Result := FPanel.Color;
end;

function TYAxHelper.GetFont: TFont;
begin
  Result := FPanel.Font;
end;

function TYAxHelper.GetDrawInside: Boolean;
begin
  Result := FPanel.DrawInside;
end;

function TYAxHelper.GetDrawLine: Boolean;
begin
  Result := FPanel.DrawLine;
end;

function TYAxHelper.GetDrawTicks: Boolean;
begin
  Result := FPanel.DrawTicks;
end;

function TYAxHelper.GetPen: TPen;
begin
  Result := FPanel.Pen;
end;

function TYAxHelper.GetSnapSize: Double;
begin
  Result := FPanel.FSnapSize;
end;

function TYAxHelper.GetSpacing: Integer;
begin
  Result := FPanel.FSpacing;
end;

function TYAxHelper.GetStepSize: Integer;
begin
  Result := FPanel.FStepSize;
end;

function TYAxHelper.GetStepScaling: TStepScaling;
begin
  Result := FPanel.StepScaling;
end;

function TYAxHelper.GetVisible: Boolean;
begin
  Result := FPanel.Visible;
end;

function TYAxHelper.GetWidth: Integer;
begin
  Result := FPanel.Width;
end;

procedure TYAxHelper.SetColor(Value: TColor);
begin
  FPanel.Color := Value;
end;

procedure TYAxHelper.SetFont(Value: TFont);
begin
  FPanel.Font := Value;
end;

procedure TYAxHelper.SetDrawInside(Value: Boolean);
begin
  FPanel.DrawInside := Value;
end;

procedure TYAxHelper.SetDrawLine(Value: Boolean);
begin
  FPanel.DrawLine := Value;
end;

procedure TYAxHelper.SetDrawTicks(Value: Boolean);
begin
  FPanel.DrawTicks := Value;
end;

procedure TYAxHelper.SetPen(Value: TPen);
begin
  FPanel.Pen := Value;
end;

procedure TYAxHelper.SetSnapSize(Value: Double);
begin
  FPanel.FSnapSize := Value;
end;

procedure TYAxHelper.SetSpacing(Value: Integer);
begin
  FPanel.Spacing := Value;
end;

procedure TYAxHelper.SetStepSize(Value: Integer);
begin
  FPanel.StepSize := Value;
end;

procedure TYAxHelper.UpdateStepSize(Value: Integer);
begin
  FPanel.UpdateStepSize(Value);
end;

procedure TYAxHelper.SetStepScaling(Value: TStepScaling);
begin
  FPanel.StepScaling := Value;
end;

procedure TYAxHelper.SetVisible(Value: Boolean);
begin
  FPanel.Visible := Value;
end;

procedure TYAxHelper.SetWidth(Value: Integer);
begin
  FPanel.Width := Value;
end;

//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
constructor TEzGraphLine.Create(Collection: TCollection);
begin
  inherited;
  FOwnsProfile := True;
  FProfile := TEzAvailabilityProfile.Create(0);
  FChangeNotifyer := TEzGraphLineNotifier.Create(Self, FProfile);
  FVisible := False;
  FGroup := -1;
  FPen := TPen.Create;
  FPen.OnChange := GraphicsObjectChange;
  FBrush := TBrush.Create;
  FBrush.Style := bsClear;
  FBrush.OnChange := GraphicsObjectChange;
end;

destructor TEzGraphLine.Destroy;
begin
  FPen.Free;
  FBrush.Free;
  FChangeNotifyer.Free;
  if FOwnsProfile then
    FProfile.Free;
  inherited;
end;

procedure TEzGraphLine.Assign(Source: TPersistent);
begin
  if Assigned(Collection) then Collection.BeginUpdate;

  if Source is TEzGraphLine then
  try
    FDescription := TEzGraphLine(Source).Description;
    FPen.Assign(TEzGraphLine(Source).Pen);
    FBrush.Assign(TEzGraphLine(Source).Brush);
    Visible := TEzGraphLine(Source).Visible;
    Tag := TEzGraphLine(Source).Tag;
    FCanEdit := TEzGraphLine(Source).FCanEdit;
    FGroup := TEzGraphLine(Source).FGroup;
    FProfile.Assign(TEzGraphLine(Source).Profile);
  finally
    if Assigned(Collection) then Collection.EndUpdate;
  end else
    inherited;
end;

procedure TEzGraphLine.RestoreDefaults;
begin
  FDescription := '';
  FVisible := False;
  FPen.Color := clBlack;
  FPen.Style := psSolid;
  FPen.Width := 1;
  FBrush.Color := clBlack;
  FBrush.Style := bsClear;
end;

procedure TEzGraphLine.SetVisible(Value: Boolean);
begin
  if Value <> FVisible then
  begin
    FVisible := Value;
    Changed(False);
  end;
end;

procedure TEzGraphLine.SetBrush(Value: TBrush);
begin
  FBrush.Assign(Value);
  Changed(False);
end;

procedure TEzGraphLine.SetDescription(Desc: WideString);
begin
  FDescription := Desc;
  Changed(False);
end;

procedure TEzGraphLine.SetProfile(Value: TEzAvailabilityProfile);
begin
  if FProfile <> Value then
  begin
    if Value <> nil then
    begin
      FOwnsProfile := False;
      FProfile := Value;
    end
    else
    begin
      FProfile := TEzAvailabilityProfile.Create(0);
      FOwnsProfile := True;
    end;
    FChangeNotifyer.ObjectList := FProfile;
    Changed(False);
  end;
end;

procedure TEzGraphLine.SetPen(Value: TPen);
begin
  FPen.Assign(Value);
  Changed(False);
end;

function TEzGraphLine.GetDisplayName: string;
begin
  if FDescription <> '' then
    Result := FDescription
  else
    Result := inherited GetDisplayName;
end;

function TEzGraphLine.GetVisible: Boolean;
begin
  if (Collection <> nil) and
     (csDesigning in (Collection as TEzGraphLines).Graph.ComponentState)
  then
    Result := FVisible
  else
    Result := FVisible and (Profile <> nil);
end;

procedure TEzGraphLine.GraphicsObjectChange(Sender: TObject);
begin
  Changed(False);
end;

procedure TEzGraphLine.Notify(APoint: TEzAvailabilityPoint; Event: TEzListNotification);
begin
  if (Collection <> nil) then
    (Collection as TEzGraphLines).Graph.DoLineChangeNotification(Self, APoint, Event);
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzGraphLineNotifier.Create(AOwner: TEzGraphLine; Profile: TEzAvailabilityProfile);
begin
  inherited Create(Profile);
  FOwner := AOwner;
end;

procedure TEzGraphLineNotifier.Notify(AObject: TObject; Action: TEzListNotification);
begin
  FOwner.Notify(TEzAvailabilityPoint(AObject), Action);
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzGraphLines.Create(Graph: TEzGraph; ColumnClass: TCollectionItemClass);
begin
  inherited Create(ColumnClass);
  FGraph := Graph;
end;

function TEzGraphLines.GetOwner: TPersistent;
begin
  Result := FGraph;
end;

function TEzGraphLines.GetItem(Index: Integer): TEzGraphLine;
begin
  Result := TEzGraphLine(inherited Items[Index]);
end;

function TEzGraphLines.Add: TEzGraphLine;
begin
  Result := TEzGraphLine(inherited Add);
end;

procedure TEzGraphLines.Update(Item: TCollectionItem);
begin
  inherited;
  if (FGraph <> nil) then
    FGraph.GraphPanel.Invalidate;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGraphScrollBarOptions
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=

constructor TEzGraphScrollBarOptions.Create(AOwner: TEzGraph);

begin
  inherited Create;
  FOwner := AOwner;
  FScrollBars := ssBoth;
  FAlwaysVisible := ssNone;
end;

procedure TEzGraphScrollBarOptions.SetAlwaysVisible(Value: TScrollStyle);

begin
  if FAlwaysVisible <> Value then
  begin
    FAlwaysVisible := Value;
    if not (csLoading in FOwner.ComponentState) and FOwner.HandleAllocated then
      FOwner.RecreateWnd;
  end;
end;

procedure TEzGraphScrollBarOptions.SetScrollBars(Value: TScrollStyle);
begin
  if FScrollbars <> Value then
  begin
    FScrollBars := Value;
    if not (csLoading in FOwner.ComponentState) and FOwner.HandleAllocated then
      FOwner.RecreateWnd;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
function TEzGraphScrollBarOptions.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TEzGraphScrollBarOptions.Assign(Source: TPersistent);
begin
  if Source is TEzGanttScrollBarOptions then
  begin
    FScrollBars := TEzGanttScrollBarOptions(Source).ScrollBars;
    FAlwaysVisible := TEzGanttScrollBarOptions(Source).AlwaysVisible;
  end
  else
    inherited;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TGraphTimebarLink
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TGraphTimebarLink.Create(AGraph: TEzGraph);
begin
  inherited Create;
  FGraph := AGraph;
end;

procedure TGraphTimebarLink.LayoutChanged;
begin
  FGraph.TimebarLayoutChanged;
end;

procedure TGraphTimebarLink.DateChanged(OldDate: TDateTime);
begin
  FGraph.TimebarDateChanged(OldDate);
end;

procedure TGraphTimebarLink.UpdateState;
begin
  FGraph.UpdateTimebarState;
end;

//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
procedure TEzGraph.AddPoint(var List: TEzPointList; P: TPoint);
begin
  SetLength(List, High(List)+2);
  List[High(List)] := P;
end;

procedure TEzGraph.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

constructor TEzGraph.Create(AOwner: TComponent);

  function AddCustomCursor(aHCursor: HCursor): Integer;
  var
    i: Integer;
  begin
     Result:=0;

     for i:=1 to High(Integer) do
     begin
        if Screen.Cursors[i]=Screen.Cursors[crDefault] then
        begin
           Screen.Cursors[i]:=aHCursor;
           Result:=i;
           Break;
        end;
     end;
  end;

begin
  inherited;
  Width := 200;
  Height := 200;
  FBorderStyle := bsSingle;
  FColorNegative := clBtnFace;
  FHitMargin := 5;
  FStacked := False;
  FIsPrinting := False;
  FSavedX := -1;
	FSelectionKind := skNone;
  FOptions := [goGrid, goHSnap, goVSnap];

  FScrollbarOptions := TEzGraphScrollBarOptions.Create(Self);
  FLines := TEzGraphLines.Create(Self, TEzGraphLine);
  FTimebarLink := TGraphTimebarLink.Create(Self);
  FGridPen := TPen.Create;
  FGridPen.OnChange := GraphicsObjectChange;

  FXAxHelper := TXAxHelper.Create(Self);

  FYAxPanel := TYAxPanel.Create(Self);
  with FYAxPanel do
  begin
    Align := alLeft;
    Parent := Self;
  end;

  FYAxHelper := TYAxHelper.Create(FYaxPanel);

  FGraphPanel := TEzGraphPanel.Create(Self);
  with FGraphPanel do
  begin
    Align := alClient;
    Parent := Self;
  end;

  crSizeScale := AddCustomCursor(LoadCursor(hInstance, 'SIZESCALE'));
end;

destructor TEzGraph.Destroy;
begin
  // If we are under control of a ganttchart, break connection
  inherited;
  FXAxHelper.Free;
  FYAxHelper.Free;
  FScrollbarOptions.Free;
  FLines.Free;
  FGridPen.Free;
  FTimebarLink.Free;
end;

procedure TEzGraph.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('VScale', ReadVScale, nil, false);
  Filer.DefineProperty('VSnapSize', ReadVSnapSize, nil, false);
end;

procedure TEzGraph.ReadVScale(Reader: TReader);
begin
  FYAxHelper.Spacing := Reader.ReadInteger;
end;

procedure TEzGraph.ReadVSnapSize(Reader: TReader);
begin
  FYAxHelper.SnapSize := Reader.ReadFloat;
end;

procedure TEzGraph.CMShowingChanged(var Message: TMessage);
var
  Saved: TScrollStyle;

begin
  inherited;
  if Showing then
  begin
    Saved := ScrollbarOptions.FAlwaysVisible;
    ScrollbarOptions.FAlwaysVisible := ssNone;
    UpdateHorizontalScrollbar;
    UpdateVerticalScrollbar;
    ScrollbarOptions.FAlwaysVisible := Saved;
  end;
end;

procedure TEzGraph.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or WS_CLIPCHILDREN or WS_CLIPSIBLINGS;
    WindowClass.style := CS_DBLCLKS;
    if FBorderStyle = bsSingle then
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
        Style := Style or WS_BORDER;
  end;
end;

procedure TEzGraph.DoLineChangeNotification(ALine: TEzGraphLine; APoint: TEzAvailabilityPoint; Event: TEzListNotification);
begin
  if csDesigning in ComponentState then Exit;
  if Event in [elnAdding, elnChanging, elnExtracting, elnDeleting] then
  begin
    if Assigned(FOnChanging) then FOnChanging(Self, ALine, APoint, Event);
  end
  else
    if Assigned(FOnChanged) then FOnChanged(Self, ALine, APoint, Event);
  Invalidate;
end;

procedure TEzGraph.DoLineMoved(ALine: TEzGraphLine; SelectionKind: TSelectionKind; const SelectionPoints: TEzPointList);
begin
  if Assigned(FOnLineMoved) then FOnLineMoved(Self, ALine, SelectionKind, SelectionPoints);
end;

procedure TEzGraph.DoOnOverLine(Line: TEzGraphLine; PointIndex: Integer; SelectionKind: TSelectionKind);
begin
  if Assigned(FOnOverLine) then FOnOverLine(Self, Line, PointIndex, SelectionKind);
end;

procedure TEzGraph.EndUpdate;
begin
  if FUpdateCount > 0 then
    Dec(FUpdateCount);
end;

function TEzGraph.DoGetHint(
  Location: TPoint;
  Line: Integer;
  PointIndex: Integer;
  SelectionKind: TSelectionKind): string;
begin
  Result := GetShortHint(Hint);
  if Assigned(FOnGetHint) then
    FOnGetHint(Self, Location, Line, PointIndex, SelectionKind, Result);
end;

procedure TEzGraph.FindLine(var X, Y:Integer; var Index: Integer;
                var PointIndex: Integer; var SelectionKind: TSelectionKind;
                IgnoreEditFlag: boolean = false);
begin
  SelectionKind := skNone;
  Index := FLines.Count-1;
  while (SelectionKind = skNone) and (Index >= 0) do
  begin
    if FLines[Index].Visible and (IgnoreEditFlag or FLines[Index].CanEdit) then
      SelectionKind := PointOnLine(X, Y, FLines[Index], PointIndex);
    if SelectionKind = skNone then
      Dec(Index);
  end;
end;

procedure TEzGraph.PaintLegendBitmap(Scale: Double);
begin
end;
{
var
  yPos, iGraph, iRow, TotalWidth, iCol, ExmplW, ExmplS, RowS: Integer;
  CW: array[0..10] of integer;
  Go, GoCal: Boolean;
  Line: TEzGraphLine;
  ARect: TRect;
begin
  with btmLegend do
  begin
    Canvas.Font.Assign(Font);

    // Width of line example to be drawn
    ExmplW := Round(20 * Scale);
    // Space arround example line
    ExmplS := Round(2 * Scale);
    // Space between rows
    RowS := Round(4 * Scale);

    for iCol := 0 to 10 do
      CW[iCol] := 0;

    GoCal := True;

    if FLines.Count > 0 then
    begin
      Go := True;
      if FCalendarGraphs.Count = 0 then
        GoCal := False;
    end
    else
      Go := False;

    iCol := 0;
    iRow := 0;
    iGraph := 0;

    while go do
    begin
      if GoCal and FCalendarGraphs[iGraph].Visible then
        Line := FCalendarGraphs[iGraph]
      else
      if not GoCal and FResourceGraphs[IGraph].Visible then
        Line := FResourceGraphs[IGraph]
      else
        Line := nil;

      if Line <> nil then
      begin
        CW[iCol] := max(CW[iCol], Canvas.TextWidth(Line.Key));
        if iRow = 2 then
        begin
          Inc(iCol);
          iRow := 0;
        end
        else
          Inc(iRow);
      end;

      if GoCal and (iGraph = FCalendarGraphs.Count-1) then
      begin
        GoCal := False;
        iGraph := 0;
        if FResourceGraphs.Count = 0 then
          Go := False;
      end
      else
      if not GoCal and (iGraph = FResourceGraphs.Count-1) then
        Go := False
      else
        Inc(iGraph);
    end;

    TotalWidth := 0;
    iGraph := 0;
    while CW[iGraph] > 0 do
    begin
      Inc(TotalWidth, CW[iGraph] + ExmplW);
      Inc(iGraph);
    end;

    // Anything to display ?
    if (TotalWidth > 0) then
    begin
      btmLegend.Width := TotalWidth;
      if iCol > 0 then
        btmLegend.Height := 3 * Canvas.TextHeight('1') + 2 * RowS
      else
        btmLegend.Height := iRow * Canvas.TextHeight('1') + (iRow-1) * RowS;

      SetRect(ARect, TotalWidth - CW[0], 0, TotalWidth, Canvas.TextHeight('1'));
      Canvas.Brush.Color := Color;
      Canvas.FillRect(RECT(0, 0, btmLegend.Width, btmLegend.Height));

      iCol := 0;
      iRow := 0;
      iGraph := 0;

      Go := True;
      if FCalendarGraphs.Count > 0 then
        GoCal := True
      else
        GoCal := False;
    end
    else
      Go := False;

    while Go do
    begin
      if GoCal and FCalendarGraphs[iGraph].Visible then
        Line := FCalendarGraphs[iGraph]
      else
      if not GoCal and FResourceGraphs[IGraph].Visible then
        Line := FResourceGraphs[iGraph]
      else
        Line := nil;

      if Line <> nil then
      begin
        Canvas.Font.Color := Line.Pen.Color;
        Canvas.TextRect(ARect, ARect.Left, ARect.Top, Line.Key);

        yPos := ARect.Top + Round((ARect.Bottom - ARect.Top) / 2);
        if dtArea in Line.Draw then
        begin
          Canvas.Brush.Assign(Line.Brush);
          Canvas.Pen.Style := psClear;
          Canvas.Rectangle(RECT
          (
            ARect.Left - (ExmplW - 2 * ExmplS),
            yPos-ExmplS,
            ARect.Left-ExmplS + 1,
            yPos+ExmplS + 1
          ));
          Dec(yPos, ExmplS+1);
          Canvas.Brush.Color := Color;
          Canvas.Brush.Style := bsSolid;
        end;

        if dtLine in Line.Draw then
        begin
          Canvas.Pen.Assign(Line.Pen);
          Canvas.MoveTo(ARect.Left - (ExmplW - 2 * ExmplS), yPos);
          Canvas.LineTo(ARect.Left - ExmplS, yPos);
        end;

        if iRow < 2 then
        begin
          OffsetRect(ARect, 0, Canvas.TextHeight('1') + RowS);
          Inc(iRow);
        end
        else
        begin
          Inc(iCol);
          OffsetRect(ARect, -CW[iCol] - ExmplW, -ARect.Top);
          ARect.Right := ARect.Left + CW[iCol];
          iRow := 0;
        end;

      end;

      if GoCal and (iGraph = FCalendarGraphs.Count-1) then
      begin
        GoCal := False;
        iGraph := 0;
        if FResourceGraphs.Count = 0 then
          Go := False;
      end
      else
      if not GoCal and (iGraph = FResourceGraphs.Count-1) then
        Go := False
      else
        Inc(iGraph);
    end;
  end;
end;
}

function TEzGraph.PointOnLine(var X, Y:Integer; Line: TEzGraphLine; var LinePart: Integer): TSelectionKind;
var
  Points: TEzPointList;
  Margin, i_point: Integer;
  p1, p2: TPoint;

begin
  Result := skNone;
  // Get the line near the cursor
  Margin := FHitMargin div 2;
  GetLinePoints(Line, X-Margin, X+Margin, Points, LinePart);
  i_point := 0;
  while (Result = skNone) and (i_point < High(Points)) do
  begin
    p1 := Points[i_point];
    p2 := Points[i_point+1];
    if (p1.Y = p2.Y) then
    begin
      if PtInRect(Rect(p1.X, p1.Y-Margin, p2.X+1, p2.Y+Margin+1), Point(x,y)) then
        Result := skHLinePart;
    end
    else if PtInRect(Rect(p1.X-Margin, min(p1.Y, p2.Y), p2.X+Margin+1, max(p1.Y, p2.Y)+1), Point(x,y)) then
      Result := skVLinePart;
    inc(i_point);
  end;
end;

procedure TEzGraph.PrintTo(ACanvas: TCanvas; ARect: TRect; Dpi: Integer);
var
  iSaved : array[0..4] of integer;

  function Scale(P: Integer) : Integer;
  begin
    Result := P * Dpi div Screen.PixelsPerInch;
  end;

  procedure Save;
  begin
    iSaved[0] := Width;
    iSaved[1] := Height;
    iSaved[2] := Font.Size;
    iSaved[3] := XAx.Position;
    iSaved[4] := FYAxHelper.Spacing;
  end;

  procedure Restore;
  begin
    Width := iSaved[0];
    Height := iSaved[1];
    Font.Size := iSaved[2];
    XAx.Position := iSaved[3];
    FYAxHelper.Spacing := iSaved[4];
  end;

begin
  FIsPrinting := True;
  Save;
  Width := ARect.Right - ARect.Left;
  Height := ARect.Bottom - ARect.Top;
  Font.Size := Scale(Font.Size);
  XAx.Position := Height - Scale(10);
  FYAxHelper.Spacing := Scale(FYAxHelper.Spacing);
  PaintLegendBitmap(Dpi / Screen.PixelsPerInch);
  PaintTo(ACanvas.Handle, ARect.Left, ARect.Top);
  Restore;
end;

procedure TEzGraph.GetLinePoints(Line: TEzGraphLine; Start, Stop: Integer; var Points: TEzPointList);
var
  Dummy: Integer;
begin
  GetLinePoints(Line, Start, Stop, Points, Dummy);
end;

procedure TEzGraph.GetLinePoints(Line: TEzGraphLine; Start, Stop: Integer; var Points: TEzPointList; var PointIndex: Integer);
var
  dtStart, dtStop: TDateTime;
  P1, P2: TEzAvailabilityPoint;
  u_prev: Double;
  i_point, x: Integer;

  function CalcX(ADate: TDateTime) : Integer;
  begin
    Result := Timebar.DateTime2X(ADate);
  end;

begin
  if not Assigned(Line) then Exit;

  dtStart := Timebar.X2DateTime(Start);

  if (Start=Stop) {special case, a single vertical linepart is requested} and (Line.Profile.Count>1) then
  begin
    Line.Profile.IndexOf(i_point, dtStart);

    if (i_point = Line.Profile.Count) then
      dec(i_point);

    Assert(i_point>=1);

    P1 := Line.Profile[i_point-1];
    P2 := Line.Profile[i_point];
    SetLength(Points, 2);

    if abs(P1.DateTime-dtStart) < abs(P2.DateTime-dtStart) then
    begin
      Assert(i_point>1);
      PointIndex := i_point-1;
      Points[0] := Point(Start, FYAxHelper.CalcY(Line.Profile[i_point-2].Units));
      Points[1] := Point(Start, FYAxHelper.CalcY(P1.Units));
    end
    else
    begin
      PointIndex := i_point;
      Points[0] := Point(Start, FYAxHelper.CalcY(P1.Units));
      Points[1] := Point(Start, FYAxHelper.CalcY(P2.Units));
    end;
    Exit;
  end;

  dtStop := Timebar.X2DateTime(Stop);
  Line.Profile.IndexOf(i_point, dtStart);
  dec(i_point);

  PointIndex := i_point;

  P1 := Line.Profile[i_point];
  u_prev := P1.Units;

  if P1.Datetime < dtStart then
    //
    // Add starting point, that is the point before the first vertical
    // line part.
    //
  begin
    AddPoint(Points, Point(Start, FYAxHelper.CalcY(u_prev)));
    inc(i_point);
    if i_point < Line.Profile.Count then
      P1 := Line.Profile[i_point];
  end;

  //
  // Add line parts between Start and Stop. These are all vertical parts and
  // therefore require two points.
  //
  while (i_point < Line.Profile.Count) and (P1.DateTime <= dtStop) do
  begin
    x := CalcX(P1.Datetime);
    AddPoint(Points, Point(x, FYAxHelper.CalcY(u_prev)));
    AddPoint(Points, Point(x, FYAxHelper.CalcY(P1.Units)));

    u_prev := P1.Units;
    inc(i_point);
    if i_point < Line.Profile.Count then
      P1 := Line.Profile[i_point];
  end;

  if Points[High(Points)].x < Stop then
    //
    // Add final point, that is the point after the last vertical
    // line part.
    //
    AddPoint(Points, Point(Stop, FYAxHelper.CalcY(u_prev)));
end;

procedure TEzGraph.GetLinePoints(Line: TEzGraphLine; LinePart: Integer; var Points: TEzPointList);
var
  AvPoint: TEzAvailabilityPoint;

begin
  SetLength(Points, 2);

  AvPoint := Line.Profile[LinePart];
  Points[0] :=
    Point(max(0, min(ClientWidth, Timebar.DateTime2X(AvPoint.Datetime))),
          XAx.Position - Round(AvPoint.Units*FYAxHelper.Spacing));

  if LinePart+1 < Line.Profile.Count then
  begin
    AvPoint := Line.Profile[LinePart+1];
    Points[1] :=
      Point(max(0, min(ClientWidth, Timebar.DateTime2X(AvPoint.Datetime))), Points[0].y);
  end
  else
    Points[1] := Point(ClientWidth, Points[0].y);
end;

function TEzGraph.GetSnapSize: string;
begin
  Result := '';
  if FSnapCount <> 0 then
  begin
    Result := IntToStr(FSnapCount);
    case FSnapScale of
      msYear: Result := Result + ' year(s)';
      msQuarter: Result := Result + ' quarter(s)';
      msMonth: Result := Result + ' month(s)';
      msWeek: Result := Result + ' week(s)';
      msDay: Result := Result + ' day(s)';
      msHour: Result := Result + ' hour(s)';
      msMinute: Result := Result + ' minute(s)';
    end;
  end;
end;

function TEzGraph.GetTimebar: TEzTimebar;
begin
  Result := FTimebarLink.Timebar;
end;

{
    // Draw gridlines
    if goGrid in FOptions then
    begin
      // Integer holding (units) label to paint
      iLabel := FXAx.Position div FYAxHelper.Spacing;

    	YPos:=FXAx.Position mod FYAxHelper.Spacing;

      // x-ax is drawn using a solid line
      if iLabel = 0 then
      begin
        Pen.Style := psSolid;
	  		MoveTo(DrawRect.Left, YPos);
  			LineTo(DrawRect.Right,YPos);
	  		YPos := YPos + FYAxHelper.Spacing;
        Pen.Style := CurrentStyle;
      end;

  		while YPos < Height do
  		begin
	  		MoveTo(DrawRect.Left, YPos);
  			LineTo(DrawRect.Right,YPos);

        // Paint unit label
        if goInsideScale in FOptions then
          TextOut(4, YPos - th, IntToStr(iLabel));

	  		YPos := YPos + FYAxHelper.Spacing;
		  	Dec(iLabel);
  		end;
  	end
    else
    // Draw X-ax only
    begin
      Pen.Style := psSolid;
      MoveTo(DrawRect.Left, FXAx.Position);
      LineTo(DrawRect.Right,FXAx.Position);
      Pen.Style := CurrentStyle;
    end;

    if not (csDesigning in ComponentState) and Assigned(Timebar)then
    begin
      dtLeft := Timebar.X2DateTime(DrawRect.Left-FYAxPos);
      dtRight := Timebar.X2DateTime(DrawRect.Right);

      if FStacked then
        DrawStacked;

      for i:=0 to FLines.Count-1 do
        with FLines[i] do
          if Visible and (IGraph<>nil) and (not FStacked or (Group=-1)) then
          begin
            if IGraph.RequiresPrepare then
              IGraph.PrepareDateRange(dtLeft, dtRight);
            DrawLineTo(Canvas, FLines[i], DrawRect.Left-FYAxPos, DrawRect.Right-FYAxPos, False);
          end;
    end;
	end;
}

procedure TEzGraph.GraphDblClick(Sender: TObject);
begin
  OpenSetupDialog;
end;

procedure TEzGraph.GraphicsObjectChange(Sender: TObject);
begin
  GraphPanel.Invalidate;
end;

procedure TEzGraph.Invalidate;
begin
  FYAxPanel.Invalidate;
  GraphPanel.Invalidate;
end;

procedure TEzGraph.ScollTimerTimer(Sender: TObject);
var
  ClientPos: TPoint;

begin
  if (FSelectionKind = skVLinePart) then
  begin
    ClientPos := ScreenToClient(Mouse.CursorPos);
    if ClientPos.X > ClientRect.Right then
    begin
      with Timebar do
        Date := AddTicks(MajorScale, MajorCount, Date, 1);
      Invalidate;
    end
    else
    if ClientPos.X < ClientRect.Left then
    begin
      with Timebar do
        Date := AddTicks(MajorScale, MajorCount, Date, 1);
      Invalidate;
    end
  end;
end;

procedure TEzGraph.SetBorderStyle(Value: TBorderStyle);
begin
  if FBorderStyle <> Value then
  begin
    FBorderStyle := Value;
    RecreateWnd;
  end;
end;

procedure TEzGraph.SetColor(Value: TColor);
begin
  if Color <> Value then
  begin
    inherited Color := Value;
    GraphPanel.Invalidate;
  end;
end;

procedure TEzGraph.SetColorNegative(Value: TColor);
begin
  if FColorNegative <> Value then
  begin
    FColorNegative := Value;
    GraphPanel.Invalidate;
  end;
end;

procedure TEzGraph.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  if not (csLoading in ComponentState) and HandleAllocated and (Height <> AHeight) then
    inc(XAx.FPosition, AHeight-Height);
  inherited;
end;

procedure TEzGraph.SetOptions(Value: TGraphOptions);
begin
  if Value <> FOptions then
  begin
    FOptions := Value;
    GraphPanel.Invalidate;
  end;
end;

procedure TEzGraph.SetXAxHelper(Value: TXAxHelper);
begin
  FXAxHelper.Assign(Value);
end;

procedure TEzGraph.SetYAxHelper(Value: TYAxHelper);
begin
  FYAxHelper.Assign(Value);
end;

procedure TEzGraph.SetGridPen(Value: TPen);
begin
  FGridPen.Assign(Value);
  GraphPanel.Invalidate;
end;

procedure TEzGraph.SetLines(Value: TEzGraphLines);
begin
  FLines.Assign(Value);
end;

procedure TEzGraph.SetMinDate(D: TDateTime);
begin
  if D <> FMinDate then
  begin
    FMinDate := D;
    UpdateHorizontalScrollbar;
  end;
end;

procedure TEzGraph.SetMaxDate(D: TDateTime);
begin
  if D <> FMaxDate then
  begin
    FMaxDate := D;
    UpdateHorizontalScrollbar;
  end;
end;

procedure TEzGraph.SetScrollBarOptions(Value: TEzGraphScrollBarOptions);
begin
  FScrollBarOptions.Assign(Value);
end;

procedure TEzGraph.SetSnapSize(S: string);
var
  i: integer;
begin
  i:=1;

  while (i <= length(S)) and {$IFDEF EZ_D2009}CharInSet(S[i], ['0'..'9']){$ELSE}(S[i] in ['0'..'9']){$ENDIF} do
    inc(i);

  if (i > 1) and (i <= length(S)) then
    FSnapCount := StrToInt(Copy(S, 1, i-1)) else
    FSnapCount := 0;

  while (i <= length(S)) and (Pos(S[i], 'mMhHdDwWqQyY') = 0) do
    inc(i);

  if (i <= length(S)) then
    case S[i] of
      'm': FSnapScale := msMinute;
      'M': FSnapScale := msMonth;
      'h', 'H': FSnapScale := msHour;
      'd', 'D': FSnapScale := msDay;
      'w', 'W': FSnapScale := msWeek;
      'q', 'Q': FSnapScale := msQuarter;
      'y', 'Y': FSnapScale := msYear;
    else
      FSnapCount := 0;
    end;
end;

procedure TEzGraph.SetStacked(Value: Boolean);
begin
  if Value <> FStacked then
  begin
    FStacked := Value;
    GraphPanel.Invalidate;
  end;
end;

procedure TEzGraph.SetTimebar(Bar: TEzTimebar);
begin
  FTimebarLink.Timebar := Bar;
end;

procedure TEzGraph.TimebarLayoutChanged;
begin
  if Timebar.ColSizing or (FUpdateCount > 0) then Exit;
  try
    BeginUpdate;
    UpdateHorizontalScrollbar;
    GraphPanel.Invalidate;
  finally
    EndUpdate;
  end;
end;

procedure TEzGraph.TimebarDateChanged(OldDate: TDateTime);
var
  dst: Integer;

begin
  if FUpdateCount > 0 then Exit;
  try
    BeginUpdate;

    with Timebar do
      dst := MajorWidth * TickCount(MajorScale, MajorCount, Date, OldDate);

    GraphPanel.ScrollGraph(dst);
    UpdateHorizontalScrollbar;
  finally
    EndUpdate;
  end;
end;

procedure TEzGraph.OpenSetupDialog;
begin
end;
{
var
  Dlg: TEzGraphDlg;
  Hour, Min, Sec, MSec: Word;
  NewOptions: TGraphOptions;
begin
  Dlg := TEzGraphDlg.Create(Self);
  with Dlg do
  begin
    CalendarGraphs.Assign(FCalendarGraphs);
    ResourceGraphs.Assign(FResourceGraphs);
    chkHSnap.Checked := goHSnap in FOptions;
    chkVSnap.Checked := goVSnap in FOptions;
    edVSnapSize.EditText := FormatFloat('00.00', FYAxHelper.SnapSize);
    chkShowPosition.Checked := goShowPosition in FOptions;
    chkShowGrid.Checked := goGrid in FOptions;
    chkShowLegend.Checked := goShowLegend in FOptions;
    if FYAxHelper.SnapSize >= 1 then
    begin
      cbSnapUnit.ItemIndex := 0;
      udSnapSize.Position := Floor(FHSnapSize);
    end
    else
    if FYAxHelper.SnapSize = 0 then
    begin
      cbSnapUnit.ItemIndex := 2;
      udSnapSize.Position := 0;
    end
    else
    begin
      DecodeTime(FHSnapSize, Hour, Min, Sec, MSec);
      if Min = 0 then
      begin
        cbSnapUnit.ItemIndex := 1;
        udSnapSize.Position := Hour;
      end
      else
      begin
        cbSnapUnit.ItemIndex := 2;
        udSnapSize.Position := Hour*60 + Min;
      end;
    end;
    if Dlg.ShowModal = mrOK then
    begin
      FCalendarGraphs.CopySettings(CalendarGraphs);
      FResourceGraphs.CopySettings(ResourceGraphs);

      FYAxHelper.SnapSize := StrToFloat(edVSnapSize.EditText);

      case cbSnapUnit.ItemIndex of
        0: FHSnapSize := udSnapSize.Position;
        1: FHSnapSize := udSnapSize.Position/24;
        2: FHSnapSize := udSnapSize.Position/(24*60);
      end;

      NewOptions := [];
      if chkHSnap.Checked then Include(NewOptions, goHSnap);
      if chkVSnap.Checked then Include(NewOptions, goVSnap);
      if chkShowPosition.Checked then Include(NewOptions, goShowPosition);
      if chkShowGrid.Checked then Include(NewOptions, goGrid);
      if chkShowLegend.Checked then Include(NewOptions, goShowLegend);
      if goDockedView in FOptions then Include(NewOptions, goDockedView);
      Options := NewOptions;
    end;
  end;
end;
}

procedure TEzGraph.UpdateTimebarState;
begin
  if Timebar <> nil then
    Enabled := True
  else
    Enabled := False;
end;

procedure TEzGraph.UpdateHorizontalScrollbar;
var
  ScrollInfo: TScrollInfo;

begin
  if not (ScrollbarOptions.ScrollBars in [ssBoth, ssHorizontal]) or not HandleAllocated or
     not Showing or not Enabled or not Assigned(Timebar) then
    Exit;

  with Timebar do
  begin
    FillChar(ScrollInfo, SizeOf(ScrollInfo), 0);
    ScrollInfo.cbSize := SizeOf(ScrollInfo);

    if Date < FMinDate then FMinDate := Date;
    if Date > FMaxDate then FMaxDate := Date;

    if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssHorizontal]) then
      ScrollInfo.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
      ScrollInfo.fMask := SIF_ALL;
    ScrollInfo.nMin  := 0;
    ScrollInfo.nPage := Timebar.Width div MajorWidth;
    ScrollInfo.nPos := max(1, TickCount(MajorScale, MajorCount, FMinDate, Date));
    ScrollInfo.nMax := TickCount(MajorScale, MajorCount, FMinDate, FMaxDate) + integer(ScrollInfo.nPage) + 1;
    SetScrollInfo(GraphPanel.Handle, SB_HORZ, ScrollInfo, true);
  end;
end;

procedure TEzGraph.UpdateVerticalScrollbar;
var
  ScrollInfo: TScrollInfo;

begin
  if not (ScrollbarOptions.ScrollBars in [ssBoth, ssVertical]) or not HandleAllocated or
     not Showing or not Enabled
  then
    Exit;

  FillChar(ScrollInfo, SizeOf(ScrollInfo), 0);
  ScrollInfo.cbSize := SizeOf(ScrollInfo);

  if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssVertical]) then
    ScrollInfo.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
    ScrollInfo.fMask := SIF_ALL;

  ScrollInfo.nPos := XAx.Position;

  ScrollInfo.nMin  := ScrollInfo.nPos+min(0, ScrollInfo.nPos-FGraphPanel.FVScrollExtendMax*FYAxHelper.Spacing);
  ScrollInfo.nMax  := ScrollInfo.nPos+max(ClientHeight-1, ScrollInfo.nPos-FGraphPanel.FVScrollExtendMin*FYAxHelper.Spacing);
  ScrollInfo.nPage := ClientHeight;
  SetScrollInfo(FGraphPanel.Handle, SB_VERT, ScrollInfo, true);
end;

procedure TEzGraph.ScrollHorizontal(var Msg: TWMHScroll);
var
  Delta: Integer;

begin
//  if FUpdateCount > 0 then Exit;
  if not Assigned(Timebar) then Exit;

  BeginUpdate;
  try
    Delta := 0;
    case Msg.ScrollCode of
      SB_LINEUP: Delta := -1;
      SB_LINEDOWN: Delta := 1;
      SB_PAGEUP: Delta := -Timebar.Width div Timebar.MajorWidth;
      SB_PAGEDOWN: Delta := Timebar.Width div Timebar.MajorWidth;
      SB_THUMBTRACK:
        with Timebar do
          Date := Ticks2Date(FMinDate, Msg.Pos);
      SB_THUMBPOSITION:
        begin
          with Timebar do
            Date := Ticks2Date(FMinDate, Msg.Pos);
          UpdateHorizontalScrollbar;
          GraphPanel.Invalidate;
        end;
      SB_BOTTOM:
      begin
        Timebar.Date := FMaxDate;
        UpdateHorizontalScrollbar;
        GraphPanel.Invalidate;
      end;
      SB_TOP:
      begin
        Timebar.Date := FMinDate;
        UpdateHorizontalScrollbar;
        GraphPanel.Invalidate;
      end;
    end;

    if Delta <> 0 then
      with Timebar do
      begin
        Date := AddTicks(MajorScale, MajorCount, Date, Delta);
        GraphPanel.ScrollGraph(-Delta * Timebar.MajorWidth);
        UpdateHorizontalScrollbar;
      end;

  finally
    EndUpdate;
  end;
end;

procedure TEzGraph.ScrollVertical(var Msg: TWMHScroll);
var
  Delta: Integer;

  procedure ScrollClientRect(Distance: Integer);
  var
    SRect: TRect;
  begin
    SRect := GraphPanel.ClientRect;
    ScrollWindowEx(GraphPanel.Handle, 0, Distance, @SRect, @SRect, 0, nil, SW_Invalidate);
    FYAxPanel.Invalidate;
  end;

begin
  if FUpdateCount > 0 then Exit;

  BeginUpdate;
  try
    Delta := 0;
    case Msg.ScrollCode of
      SB_LINEUP: Delta := -FYAxHelper.Spacing;
      SB_LINEDOWN: Delta := FYAxHelper.Spacing;
      SB_PAGEUP: Delta := -2*FYAxHelper.Spacing;
      SB_PAGEDOWN: Delta := 2*FYAxHelper.Spacing;
      SB_THUMBTRACK:
      begin
        if FSavedX = -1 then
          FSavedX := XAx.Position;
        XAx.FPosition := FSavedX+(FSavedX-Msg.Pos);
        Invalidate;
      end;
      SB_THUMBPOSITION:
      begin
        FSavedX := -1;
        UpdateVerticalScrollbar;
        Invalidate;
      end;
    end;

    if Delta <> 0 then
    begin
      dec(XAx.FPosition, Delta);
      UpdateVerticalScrollbar;
      ScrollClientRect(-Delta);
    end;

  finally
    EndUpdate;
  end;
end;

procedure TEzGraph.WMSize(var Msg: TWMSize);
begin
  inherited;
  UpdateHorizontalScrollbar;
  UpdateVerticalScrollbar;
end;

end.
