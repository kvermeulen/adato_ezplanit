//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("EzPlanitDs_CB5.res");
USEPACKAGE("vcl50.bpi");
USEUNIT("EzReg.pas");
USEPACKAGE("Vcldb50.bpi");
USEPACKAGE("EzBasic_CB5.bpi");
USEFORMNS("EzDsDsgn.pas", Ezdsdsgn, EzDatasetDesigner);
USEPACKAGE("dcldb50.bpi");
USEPACKAGE("dsnide50.bpi");
USEFORMNS("EzEpDsgn.pas", Ezepdsgn, EzEndPointDesigner);
USEFORMNS("EzScmDsg.pas", Ezscmdsg, EzSchemeDesigner);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------

#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void*)
{
        return 1;
}
//---------------------------------------------------------------------------
