unit EzRuleEd;

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, EzCalendar;

type
  TfrmRule = class(TForm)
    Label10: TLabel;
    lbDayDate: TLabel;
    GroupBox2: TGroupBox;
    rbWorking: TRadioButton;
    rbNonWorking: TRadioButton;
    gbPeriod: TGroupBox;
    dtpFromTime: TDateTimePicker;
    dtpToTime: TDateTimePicker;
    rbEntireDay: TRadioButton;
    rbInterval: TRadioButton;
    btnOK: TButton;
    btnCancel: TButton;
    GroupBox1: TGroupBox;
    gbRangeOfRecurrance: TGroupBox;
    Bevel1: TBevel;
    rbNoRecur: TRadioButton;
    rbRecurWeekly: TRadioButton;
    rbRecurMonthly: TRadioButton;
    rbRecurYearly: TRadioButton;
    pnlMonth: TPanel;
    rbMonthPerDate: TRadioButton;
    rbMonthPerDay: TRadioButton;
    pnlWeek: TPanel;
    lbEvery: TLabel;
    cbWeekDays: TComboBox;
    Label1: TLabel;
    edWeekCount: TEdit;
    udWeekCount: TUpDown;
    Label4: TLabel;
    pnlYear: TPanel;
    rbYearPerDate: TRadioButton;
    rbYearPerDay: TRadioButton;
    pnlMonthPerdayFields: TPanel;
    cbMonthNthDay: TComboBox;
    cbMonthPerDayDaylist: TComboBox;
    lbOfTheMonth: TLabel;
    edMonthDayCount: TEdit;
    udMonthPerDayCount: TUpDown;
    Label5: TLabel;
    pnlYearPerdayFields: TPanel;
    cbYearNthDay: TComboBox;
    cbYearPerDayDaylist: TComboBox;
    cbYearPerDayMonthlist: TComboBox;
    pnlMonthPerdateFields: TPanel;
    cbMonthPerDateDaynum: TComboBox;
    lbDayOfTheMonth: TLabel;
    edMonthDateCount: TEdit;
    udMonthPerDateCount: TUpDown;
    Label7: TLabel;
    pnlYearPerdateFields: TPanel;
    cbYearPerDateDaynum: TComboBox;
    cbYearPerDateMonthlist: TComboBox;
    Panel1: TPanel;
    rbStartBy: TRadioButton;
    RangeStartDate: TDateTimePicker;
    rbNoStartDate: TRadioButton;
    Panel2: TPanel;
    rbNoEndDate: TRadioButton;
    rbEndAfter: TRadioButton;
    edOcurrences: TEdit;
    udOcurrences: TUpDown;
    Label6: TLabel;
    rbEndBy: TRadioButton;
    RangeEndDate: TDateTimePicker;
    pnlNoRecurrence: TPanel;
    Label2: TLabel;
    dtpNoRecurranceFromDate: TDateTimePicker;
    Label3: TLabel;
    dtpNoRecurranceToDate: TDateTimePicker;
    Label8: TLabel;
    dtpNoRecurranceFromTime: TDateTimePicker;
    dtpNoRecurranceToTime: TDateTimePicker;
    procedure rbNoRecurClick(Sender: TObject);
    procedure rbRecurWeeklyClick(Sender: TObject);
    procedure rbRecurMonthlyClick(Sender: TObject);
    procedure rbMonthPerDateClick(Sender: TObject);
    procedure rbEntireDayClick(Sender: TObject);
    procedure rbIntervalClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rbRecurYearlyClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure rbYearPerDateClick(Sender: TObject);
    procedure rbNoStartDateClick(Sender: TObject);
    procedure rbEndAfterClick(Sender: TObject);
    procedure edWeekCountChange(Sender: TObject);
  protected
    FADate: TDateTime;

    function  GetRuleStart: TDateTime;
    function  GetRuleStop: TDateTime;
    function  GetIntervalType: TEzIntervalType;
    function  GetRuleType: TEzCalendarRuleType;
    procedure SetDate(Value: TDateTime);
    procedure SetIntervalType(Value: TEzIntervalType);
    procedure SetRuleStart(Value: TDateTime);
    procedure SetRuleStop(Value: TDateTime);
    procedure SetRuleType(Value: TEzCalendarRuleType);
    procedure UpdateEnabled;

  public
    procedure GetRule(Rule: TEzCalendarRule);
    procedure SetRule(Rule: TEzCalendarRule);

    property TheDate: TDateTime read FADate write SetDate;
    property IntervalType: TEzIntervalType read GetIntervalType write SetIntervalType;
    property RuleStart: TDateTime read GetRuleStart write SetRuleStart;
    property RuleStop: TDateTime read GetRuleStop write SetRuleStop;
    property RuleType: TEzCalendarRuleType read GetRuleType write SetRuleType;
  end;

var
  frmRule: TfrmRule;

implementation

{$R *.DFM}

uses EzStrings_3, EzDateTime, Math;

procedure TfrmRule.FormCreate(Sender: TObject);
var
  x: integer;

begin
  cbYearPerDayMonthlist.Items.Clear;
  cbYearPerDateMonthlist.Items.Clear;
  for x:=1 to 12 do
  begin
    cbYearPerDayMonthlist.Items.Add({$IFDEF XE3}FormatSettings.{$ENDIF}LongMonthNames[x]);
    cbYearPerDateMonthlist.Items.Add({$IFDEF XE3}FormatSettings.{$ENDIF}LongMonthNames[x]);
  end;

  cbWeekDays.Items.Clear;
  cbMonthPerDayDaylist.Items.Clear;
  cbYearPerDayDaylist.Items.Clear;
  for x:=1 to 7 do
  begin
    cbWeekDays.Items.Add({$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[x]);
    cbMonthPerDayDaylist.Items.Add({$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[x]);
    cbYearPerDayDaylist.Items.Add({$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[x]);
  end;

  cbMonthPerDateDaynum.Items.Clear;
  cbYearPerDateDaynum.Items.Clear;
  for x:=1 to 31 do
  begin
    cbMonthPerDateDaynum.Items.Add(DayNumNames[x]);
    cbYearPerDateDaynum.Items.Add(DayNumNames[x]);
  end;
end;

procedure TfrmRule.FormDestroy(Sender: TObject);
begin
  frmRule := nil;
end;

procedure TfrmRule.SetDate(Value: TDateTime);
var
  AYear, AMonth, ADay: Word;
  bIsLast: Boolean;

begin
  FADate := Value;
  dtpNoRecurranceFromDate.Date := FADate;
  dtpNoRecurranceFromTime.Time := Frac(FADate);

  lbDayDate.Caption := FormatDateTime('dddddd', FADate);
  DecodeDate(FADate, AYear, AMonth, ADay);
  cbMonthNthDay.ItemIndex := NthDay(AYear, AMonth, ADay, bIsLast) - 1;
  cbYearNthDay.ItemIndex := cbMonthNthDay.ItemIndex;

  cbMonthPerDateDaynum.ItemIndex := ADay - 1;
  cbYearPerDateDaynum.ItemIndex := cbMonthPerDateDaynum.ItemIndex;

  // Because LongDayNames starts with Sunday, we need to use
  // DayOfWeek here instead of DayOfTheWeek
  cbMonthPerDayDayList.ItemIndex := ADay-1;
  cbYearPerDayDayList.ItemIndex := ADay-1;
  CbWeekDays.ItemIndex := ADay-1;
  cbYearPerDayMonthlist.ItemIndex := AMonth - 1;
  cbYearPerDateMonthlist.ItemIndex := AMonth - 1;

  RangeStartDate.Date := FADate;
  RangeEndDate.Date := FADate;
end;

procedure TfrmRule.SetIntervalType(Value: TEzIntervalType);
begin
  rbWorking.Checked := Value = itWorking;
end;

procedure TfrmRule.SetRuleStart(Value: TDateTime);
begin
  TheDate := Value;
end;

procedure TfrmRule.SetRuleStop(Value: TDateTime);
begin
  dtpNoRecurranceToDate.Date := Trunc(Value);
  dtpNoRecurranceToTime.Time := Frac(Value);
end;

procedure TfrmRule.SetRuleType(Value: TEzCalendarRuleType);
begin
  case Value of
    crtFixedPeriod:
      rbNoRecur.Checked := True;

    crtRecurringDayOfWeek:
      rbrecurWeekly.Checked := True;

    crtRecurringDayPerMonth:
    begin
      rbRecurMonthly.Checked := True;
      rbMonthPerDay.Checked := True;
    end;

    crtRecurringDayOfMonth:
    begin
      rbRecurMonthly.Checked := True;
      rbMonthPerDate.Checked := True;
    end;

    crtRecurringDayPerYear:
    begin
      rbRecurYearly.Checked := True;
      rbYearPerDay.Checked := True;
    end;

    crtRecurringDayOfYear:
    begin
      rbRecurYearly.Checked := True;
      rbYearPerDate.Checked := True;
    end;
  end;
end;

procedure TfrmRule.GetRule(Rule: TEzCalendarRule);
begin
  Rule.Start := RuleStart;
  Rule.Stop := RuleStop;
  Rule.RuleType := RuleType;
  Rule.IntervalType := IntervalType;

  Rule.RangeStart := 0;
  Rule.RangeEnd := 0;
  Rule.Occurrences := 0;
  Rule.Count := 1;

  if rbStartBy.Checked then
    Rule.RangeStart := Trunc(RangeStartDate.Date);

  if rbEndAfter.Checked then
    Rule.Occurrences := udOcurrences.Position

  else if rbEndBy.Checked then
    Rule.RangeEnd := Trunc(RangeEndDate.Date);

  if rbRecurWeekly.Checked then
    Rule.Count := udWeekCount.Position
  else if rbRecurMonthly.Checked and rbMonthPerDate.Checked then
    Rule.Count := udMonthPerDateCount.Position
  else if rbRecurMonthly.Checked and rbMonthPerDay.Checked then
    Rule.Count := udMonthPerDayCount.Position;
end;

procedure TfrmRule.SetRule(Rule: TEzCalendarRule);
begin
  RuleType := Rule.RuleType;
  IntervalType := Rule.IntervalType;
  RuleStart := Rule.Start;
  RuleStop := Rule.Stop;  
  if (Rule.RuleType<>crtFixedPeriod) and ((Frac(Rule.Start)<>0) or (Frac(Rule.Stop)<>0)) then
  begin
    rbInterval.Checked := True;
    dtpFromTime.Time := Frac(Rule.Start);
    dtpToTime.Time := Frac(Rule.Stop);
  end
  else
  begin
    rbEntireDay.Checked := True;
    dtpFromTime.Time := 0;
    dtpToTime.Time := 0;
  end;

  if Rule.RangeStart<>0 then
  begin
    rbStartBy.Checked := True;
    RangeStartDate.Date := Rule.RangeStart;
  end
  else
  begin
    rbNoStartDate.Checked := True;
    RangeStartDate.Date := Trunc(Now);
  end;

  if Rule.RangeEnd<>0 then
  begin
    rbEndBy.Checked := True;
    RangeEndDate.Date := Rule.RangeEnd;
  end
  else if Rule.Occurrences<>0 then
  begin
    rbEndAfter.Checked := True;
    udOcurrences.Position := Rule.Occurrences;
  end
  else
  begin
    rbNoEndDate.Checked := True;
    RangeEndDate.Date := Trunc(Now);
  end;


  if rbRecurWeekly.Checked then
    udWeekCount.Position := Rule.Count
  else if rbRecurMonthly.Checked and rbMonthPerDate.Checked then
    udMonthPerDateCount.Position := Rule.Count
  else if rbRecurMonthly.Checked and rbMonthPerDay.Checked then
    udMonthPerDayCount.Position := Rule.Count;
end;

function TfrmRule.GetRuleStart: TDateTime;
var
  ATime: TDateTime;

begin
  if rbInterval.Checked then
    ATime := Frac(dtpFromTime.Time) else
    ATime := 0;

  case RuleType of
    crtRecurringDayOfWeek:
      Result := EncodeRecurringDayOfWeek(CbWeekDays.ItemIndex+1) + ATime;

    crtRecurringDayPerMonth:
      Result := EncodeRecurringDayPerMonth(cbMonthPerDayDayList.ItemIndex+1, TEzDayIndex(cbMonthNthDay.ItemIndex)) + ATime;

    crtRecurringDayOfMonth:
      Result := EncodeRecurringDayOfMonth(cbMonthPerDateDaynum.ItemIndex+1) + ATime;

    crtRecurringDayPerYear:
      Result := EncodeRecurringDayPerYear(cbYearPerDayDayList.ItemIndex+1, cbYearPerDayMonthlist.ItemIndex+1, TEzDayIndex(cbYearNthDay.ItemIndex)) + ATime;

    crtRecurringDayOfYear:
      Result := EncodeRecurringDayOfYear(cbYearPerDateDaynum.ItemIndex+1, cbYearPerDateMonthlist.ItemIndex+1) + ATime;

  else {crtFixedPeriod}
    Result := Floor(dtpNoRecurranceFromDate.Date) + Frac(dtpNoRecurranceFromTime.Time);
  end;
end;

function TfrmRule.GetRuleStop: TDateTime;
begin
  if rbNoRecur.Checked then
    Result := Floor(dtpNoRecurranceToDate.Date) + Frac(dtpNoRecurranceToTime.Time)

  else if rbInterval.Checked then
    Result := Floor(RuleStart) + Frac(dtpToTime.Time)

  else
    Result := Floor(RuleStart);
end;

function TfrmRule.GetIntervalType: TEzIntervalType;
begin
  if rbWorking.Checked then
    Result := itWorking else
    Result := itNonWorking;
end;

function TfrmRule.GetRuleType: TEzCalendarRuleType;
begin
  Result := crtFixedPeriod;

  if rbrecurWeekly.Checked then
    Result := crtRecurringDayOfWeek

  else if rbRecurMonthly.Checked then
  begin
    if rbMonthPerDay.Checked then
      Result := crtRecurringDayPerMonth
    else if rbMonthPerDate.Checked then
      Result := crtRecurringDayOfMonth;
  end

  else if rbRecurYearly.Checked then
  begin
    if rbYearPerDay.Checked then
      Result := crtRecurringDayPerYear
    else if rbYearPerDate.Checked then
      Result := crtRecurringDayOfYear;
  end;
end;

procedure TfrmRule.rbNoRecurClick(Sender: TObject);
begin
  pnlNoRecurrence.Visible := True;
  pnlMonth.Visible := False;
  pnlYear.Visible := False;
  pnlWeek.Visible := False;
  gbRangeOfRecurrance.Enabled := False;
  gbPeriod.Enabled := False;
  UpdateEnabled;
end;

procedure TfrmRule.rbRecurWeeklyClick(Sender: TObject);
begin
  pnlNoRecurrence.Visible := False;
  pnlMonth.Visible := False;
  pnlYear.Visible := False;
  pnlWeek.Visible := True;
  gbRangeOfRecurrance.Enabled := True;
  gbPeriod.Enabled := True;
  UpdateEnabled;
end;

procedure TfrmRule.rbRecurMonthlyClick(Sender: TObject);
begin
  pnlNoRecurrence.Visible := False;
  pnlMonth.Visible := True;
  pnlYear.Visible := False;
  pnlWeek.Visible := False;
  gbRangeOfRecurrance.Enabled := True;
  gbPeriod.Enabled := True;
  rbMonthPerDateClick(Self);

  UpdateEnabled;
end;

procedure TfrmRule.rbRecurYearlyClick(Sender: TObject);
begin
  pnlNoRecurrence.Visible := False;
  pnlMonth.Visible := False;
  pnlYear.Visible := True;
  pnlWeek.Visible := False;
  gbRangeOfRecurrance.Enabled := True;
  gbPeriod.Enabled := True;
  rbYearPerDateClick(Self);

  UpdateEnabled;
end;

procedure TfrmRule.rbMonthPerDateClick(Sender: TObject);
begin
  pnlMonthPerdateFields.Visible := rbMonthPerDate.Checked;
  pnlMonthPerdayFields.Visible := rbMonthPerDay.Checked;
end;

procedure TfrmRule.rbEntireDayClick(Sender: TObject);
begin
  UpdateEnabled;
end;

procedure TfrmRule.rbIntervalClick(Sender: TObject);
begin
  UpdateEnabled;
end;

procedure TfrmRule.UpdateEnabled;
begin
  dtpFromTime.Enabled := rbInterval.Checked;
  dtpToTime.Enabled := rbInterval.Checked;
end;

procedure TfrmRule.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (ModalResult = idOK) then
  begin
    if not rbStartBy.Checked and
      (
        (rbRecurWeekly.Checked and (udWeekCount.Position>1)) or
        (rbRecurMonthly.Checked and rbMonthPerDate.Checked and (udMonthPerDateCount.Position>1)) or
        (rbRecurMonthly.Checked and rbMonthPerDay.Checked and (udMonthPerDayCount.Position>1))
      )
    then
    begin
      CanClose := False;
      MessageDlg(SStartDateRequired, mtError, [mbOK], 0);
    end;

    if not rbStartBy.Checked and rbEndAfter.Checked then
    begin
      CanClose := False;
      MessageDlg(SStartDateRequiredWithEndAfter, mtError, [mbOK], 0);
    end;

    if rbInterval.Checked and (Frac(dtpFromTime.Time)>=Frac(dtpToTime.Time)) then
    begin
      CanClose := False;
      MessageDlg(SInvalidTimes, mtError, [mbOK], 0);
    end;
  end;
end;

procedure TfrmRule.rbYearPerDateClick(Sender: TObject);
begin
  pnlYearPerdateFields.Visible := rbYearPerDate.Checked;
  pnlYearPerdayFields.Visible := rbYearPerDay.Checked;
end;

procedure TfrmRule.rbNoStartDateClick(Sender: TObject);
begin
  RangeStartDate.Enabled := rbStartBy.Checked;
end;

procedure TfrmRule.rbEndAfterClick(Sender: TObject);
begin
  udOcurrences.Enabled := rbEndAfter.Checked;
  edOcurrences.Enabled := rbEndAfter.Checked;
  RangeEndDate.Enabled := rbEndBy.Checked;
  rbStartBy.Checked := rbStartBy.Checked or rbEndAfter.Checked;
end;

procedure TfrmRule.edWeekCountChange(Sender: TObject);
begin
  rbStartBy.Checked :=
        (rbRecurWeekly.Checked and (udWeekCount.Position>1)) or
        (rbRecurMonthly.Checked and rbMonthPerDate.Checked and (udMonthPerDateCount.Position>1)) or
        (rbRecurMonthly.Checked and rbMonthPerDay.Checked and (udMonthPerDayCount.Position>1));

  rbNoStartDate.Checked := not rbStartBy.Checked;
end;

end.
