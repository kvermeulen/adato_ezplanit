object frmPrintPreview: TfrmPrintPreview
  Left = 291
  Top = 188
  Caption = 'Print preview'
  ClientHeight = 604
  ClientWidth = 849
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 137
    Top = 31
    Height = 573
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 849
    Height = 31
    BorderWidth = 2
    ButtonHeight = 21
    ButtonWidth = 33
    Caption = 'ToolBar1'
    EdgeBorders = [ebBottom]
    Indent = 20
    ShowCaptions = True
    TabOrder = 0
    object ToolButton3: TToolButton
      Left = 20
      Top = 0
      Caption = 'Close'
      ImageIndex = 2
      OnClick = ToolButton3Click
    end
    object Panel4: TPanel
      Left = 53
      Top = 0
      Width = 140
      Height = 21
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Zoom'
      end
      object cbZoomFactor: TComboBox
        Left = 40
        Top = 0
        Width = 89
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = cbZoomFactorChange
        Items.Strings = (
          'Scale to fit'
          '25%'
          '50%'
          '100%'
          '200%'
          '300%')
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 31
    Width = 137
    Height = 573
    Align = alLeft
    TabOrder = 1
    object pnlPageMap: TPanel
      Left = 1
      Top = 17
      Width = 135
      Height = 128
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 5
      TabOrder = 0
      OnResize = pnlPageMapResize
      object grPageMap: TStringGrid
        Left = 5
        Top = 5
        Width = 125
        Height = 118
        Align = alClient
        BorderStyle = bsNone
        DefaultColWidth = 10
        DefaultRowHeight = 10
        FixedCols = 0
        FixedRows = 0
        ScrollBars = ssNone
        TabOrder = 0
        OnDrawCell = grPageMapDrawCell
        OnSelectCell = grPageMapSelectCell
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 135
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Page map'
      TabOrder = 1
    end
    object Panel2: TPanel
      Left = 1
      Top = 145
      Width = 135
      Height = 427
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
    end
  end
end
