unit EzScmDsg;

{ =============================================================================== }
{                            TEzDataset designer object                           }
{                                                                                 }
{ For compiling this file do the following:                                       }
{   BCB 4.0:                                                                      }
{       Add '-LUvcl40 -LUdcldb40' to the PFLAGS compiler options in               }
{       the *.bpk file.                                                           }
{   BCB 5.0:                                                                      }
{       Add '-LUvcl50 -LUdcldb50' to the PFLAGS compiler options in               }
{       the *.bpk file.                                                           }
{   BCB 6.0:                                                                      }
{       Add '-LUvcl -LUdcldb' to the PFLAGS compiler options in                   }
{       the *.bpk file.                                                           }
{ =============================================================================== }

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Grids, StdCtrls, Db, EzDBGrid, EzDataset,
  ExtCtrls, ImgList, ActnList, Menus, Buttons, math,
  ToolWin, EzColumnGrid, TypInfo

{$IFDEF EZ_D6}
  , DesignIntf, DesignWindows;
{$ELSE}
  , LibIntf, Dsgnintf, DsgnWnds;
{$ENDIF}

resourcestring
  SGridNotAssigened = 'Column has no grid assigned.';
  SDataSourceNotAssigened = 'Tree grid is not connected to any datasource.';
  SDataSetNotAssigened = 'Tree grid is not connected to any dataset.';
  SSchemeCollectionNotAssigened = 'Tree grid is not connected to a scheme collection.';

type
  ESchemeDesignerError = class(Exception);

  TEzSchemeDesigner = class(TDesignWindow)
    ImageList1: TImageList;
    tmSetHelptext: TTimer;
    pnlFieldmap: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure grSchemeGridGetValue(Sender: TObject; ACol, ARow: Integer;
      var Value: Variant);
    procedure grSchemeGridSetValue(Sender: TObject; ACol, ARow: LongInt;
      const Value: Variant);
    procedure grSchemeGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure grSchemeGridSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);

    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    UpdateCount: integer;
{$IFDEF EZ_D6}
    FDesigner: IDesigner;
{$ELSE}
    FDesigner: IFormDesigner;
{$ENDIF}
    FColumn: TEzTreeGridColumn;
    FModified: Boolean;
    grSchemeGrid: TEzColumnGrid;

    function  GetCellValue(ACol, ARow: Integer) : string;
    function  GetDataset: TCustomEzDataset;
    function  GetGrid: TCustomEzDBTreeGrid;
    function  GetNodeType(ARow: Integer) : TNodeType;
    function  GetSchemeCollection: TEzTreeGridSchemes;
    procedure SetColumn(Value: TEzTreeGridColumn);
    procedure SetModified(Value: Boolean);

  protected

{$IFNDEF DSGNDEV}
    function  UniqueName(Component: TComponent): string; override;
{$ENDIF}

  public
    destructor Destroy; override;

    function  GetEditState: TEditState; override;

{$IFDEF EZ_D6}
    function EditAction(Action: TEditAction) : Boolean; override;
{$ELSE}
    procedure EditAction(Action: TEditAction); override;
{$ENDIF}

    procedure InternalItemDeleted(AItem: TPersistent);

{$IFDEF EZ_D6}
    procedure ItemDeleted(const ADesigner: IDesigner; AItem: TPersistent); override;
{$ELSE}
  {$IFNDEF DSGNDEV}
    procedure ComponentDeleted(Component: IPersistent); override;
  {$ENDIF}
{$ENDIF}

{$IFDEF EZ_D6}
    property Designer: IDesigner read FDesigner write FDesigner;
{$ELSE}
    property Designer: IFormDesigner read FDesigner write FDesigner;
{$ENDIF}
    property EzDataset: TCustomEzDataset read GetDataset;
    property Grid: TCustomEzDBTreeGrid read GetGrid;
    property Column: TEzTreeGridColumn read FColumn write SetColumn;
    property SchemeCollection: TEzTreeGridSchemes read GetSchemeCollection;
    property Modified: Boolean read FModified write SetModified;
  end;

{$IFDEF EZ_D6}
  procedure ShowEzSchemeDesigner(Designer: IDesigner; Column: TEzTreeGridColumn);
{$ELSE}
  procedure ShowEzSchemeDesigner(Designer: IFormDesigner; Column: TEzTreeGridColumn);
{$ENDIF}

implementation

uses {DSDesign, }EzPainting

{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
  ;
{$ENDIF}

{$R *.DFM}

procedure EzSchemeDesignerError(const Message: string; Component: TComponent = nil);
begin
  if Assigned(Component) then
    raise ESchemeDesignerError.Create(Format('%s: %s', [Component.Name, Message])) else
    raise ESchemeDesignerError.Create(Message);
end;

{$IFDEF EZ_D6}
  procedure ShowEzSchemeDesigner(Designer: IDesigner; Column: TEzTreeGridColumn);
{$ELSE}
  procedure ShowEzSchemeDesigner(Designer: IFormDesigner; Column: TEzTreeGridColumn);
{$ENDIF}
var
  Editor: TEzSchemeDesigner;

begin
  Editor := TEzSchemeDesigner.Create(Application);
  Editor.Designer := Designer;
  Editor.Column := Column;
  Editor.Show;
end;

//============================================================================
// General functions
//============================================================================
function TEzSchemeDesigner.GetEditState: TEditState;
begin
  if Assigned(ActiveControl) then
    Result := [esCanCopy, esCanCut, esCanPaste] else
    Result := [];
end;

{$IFDEF EZ_D6}
function TEzSchemeDesigner.EditAction(Action: TEditAction) : Boolean;
{$ELSE}
procedure TEzSchemeDesigner.EditAction(Action: TEditAction);
{$ENDIF}
begin
{$IFDEF EZ_D6}
  Result := True;
{$ENDIF}
  if not Assigned(ActiveControl) then Exit;
  case Action of
    eaCut: SendMessage(ActiveControl.Handle, WM_CUT, 0, 0);
    eaCopy: SendMessage(ActiveControl.Handle, WM_COPY, 0, 0);
    eaPaste: SendMessage(ActiveControl.Handle, WM_PASTE, 0, 0);
  end;
end;

procedure TEzSchemeDesigner.InternalItemDeleted(AItem: TPersistent);
begin
  if AItem = FColumn then
  begin
    FColumn := nil;
    Close;
  end;
end;

{$IFDEF EZ_D6}
  procedure TEzSchemeDesigner.ItemDeleted(const ADesigner: IDesigner; AItem: TPersistent);
  begin
    InternalItemDeleted(AItem);
  end;
{$ELSE}
  {$IFNDEF DSGNDEV}
    procedure TEzSchemeDesigner.ComponentDeleted(Component: IPersistent);
    begin
      InternalItemDeleted(ExtractPersistent(Component));
    end;
  {$ENDIF}
{$ENDIF}

procedure TEzSchemeDesigner.FormCreate(Sender: TObject);
begin
  // Hide helpstring memo (Visible = false does not work!!!)
  UpdateCount := 0;
  FModified := false;
  FColumn := nil;

  grSchemeGrid := TEzColumnGrid.Create(Self);
  with grSchemeGrid do begin
    Parent := pnlFieldmap;
    Align := alClient;
    BorderStyle := bsNone;

    OnDrawCell := grSchemeGridDrawCell;
    OnGetValue := grSchemeGridGetValue;
    OnSetValue := grSchemeGridSetValue;
    OnSelectCell := grSchemeGridSelectCell;

    DefaultRowHeight := 18;
    Flags := [igfShowWarnings, igfUseInsertCache];

    with Columns.Add do begin
      Title := 'Dataset';
      Width := 130;
    end;

    with Columns.Add do begin
      Title := 'Scheme';
      Width := 130;
      EditStyle := esPickList;
    end;
  end;
end;

procedure TEzSchemeDesigner.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Release;
end;

destructor TEzSchemeDesigner.Destroy;
begin
  FColumn := nil;
  inherited;
end;

procedure TEzSchemeDesigner.SetColumn(Value: TEzTreeGridColumn);
var
  i: Integer;

begin
  if Value <> FColumn then
  begin
    FColumn := Value;
    if FColumn <> nil then
    begin
      Caption := FColumn.GetNamePath + '.Schemes';
      if EzDataset.SelfReferencing then
      begin
        grSchemeGrid.RowCount := 5;
        grSchemeGrid.Empty := False;
      end
      else
      begin
        grSchemeGrid.RowCount := EzDataset.EzDataLinks.Count + 1;
        grSchemeGrid.Empty := not EzDataset.EzDataLinks.Count>0;
      end;

      with SchemeCollection do
        for i:=0 to Schemes.Count-1 do
          grSchemeGrid.Columns[1].PickList.Add(Schemes[i].DisplayName);
    end else
    begin
      Caption := '';
      grSchemeGrid.RowCount := 2;
      grSchemeGrid.Empty := True;
      grSchemeGrid.Columns[1].PickList.Clear;
    end;
  end;
end;

procedure TEzSchemeDesigner.SetModified(Value: Boolean);
begin
  FModified := Value;
  if FModified and Assigned(Designer) then
    Designer.Modified;
end;

{$IFNDEF DSGNDEV}
function TEzSchemeDesigner.UniqueName(Component: TComponent): string;
begin
  Result := 'NoName'
end;
{$ENDIF}

function TEzSchemeDesigner.GetDataset: TCustomEzDataset;
begin
  if not Assigned(Grid.DataSource) then
    EzSchemeDesignerError(SDataSourceNotAssigened, Column.Grid);

  if not Assigned(Grid.DataSource.Dataset) then
    EzSchemeDesignerError(SDataSetNotAssigened, Column.Grid);

  Result := Grid.DataSource.Dataset as TCustomEzDataset;
end;

function TEzSchemeDesigner.GetGrid: TCustomEzDBTreeGrid;
begin
  if not Assigned(Column.Grid) then
    EzSchemeDesignerError(SGridNotAssigened);

  Result := Column.Grid as TCustomEzDBTreeGrid;
end;

function TEzSchemeDesigner.GetSchemeCollection: TEzTreeGridSchemes;
begin
  if not Assigned((Grid as TCustomEzDBTreeGrid).SchemeCollection) then
    EzSchemeDesignerError(SSchemeCollectionNotAssigened, Grid);

  Result := Grid.SchemeCollection;
end;

// ==============================================================
// functions related to fieldmap page
// ==============================================================
function TEzSchemeDesigner.GetNodeType(ARow: Integer) : TNodeType;
begin
  Result := TNodeType(ARow-1);
end;

function TEzSchemeDesigner.GetCellValue(ACol, ARow: Integer) : string;
var
  Index: Integer;

begin
  try
    Result := '';

    if ACol = 1 then
    begin
      if EzDataset.SelfReferencing then
        case GetNodeType(ARow) of
          ntSingle: Result := 'Single nodes';
          ntRoot: Result := 'Root nodes';
          ntParent: Result := 'Parent nodes';
          ntChild: Result := 'Child nodes';
        end
      else if Assigned(EzDataset.EzDatalinks[ARow-1].Datasource) then
        Result := EzDataset.EzDatalinks[ARow-1].Datasource.Name;
    end
    else if ACol > 1 then
    begin
      if EzDataset.SelfReferencing then
        Index := Column.GetSchemeIndex(GetNodeType(ARow))
      else
        Index := Column.GetSchemeIndex(ARow-1);

      if Index <> -1 then
        Result := SchemeCollection.Schemes[Index].DisplayName;
    end;

  except
    grSchemeGrid.RowCount := 2;
    grSchemeGrid.State := [igsEmpty];
    raise;
  end;
end;

procedure TEzSchemeDesigner.grSchemeGridGetValue(Sender: TObject; ACol,
  ARow: Integer; var Value: Variant);
begin
  Value := GetCellValue(ACol, ARow);
end;

procedure TEzSchemeDesigner.grSchemeGridSetValue(Sender: TObject; ACol, ARow: LongInt;
      const Value: Variant);
var
  i: Integer;

begin
  Modified := True;
  
  if VarIsNull(Value) or (Value = '') then
    i := -1 else
    i := grSchemeGrid.Columns[1].PickList.IndexOf(Value);

  if EzDataset.SelfReferencing then
    Column.SetSchemeIndex(GetNodeType(ARow), i) else
    Column.SetSchemeIndex(ARow-1, i);
end;

procedure TEzSchemeDesigner.grSchemeGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol = 1) and (ARow > 0) then
  begin
    grSchemeGrid.Canvas.Brush.Color := $00FDE1E1;
    grSchemeGrid.Canvas.FillRect(Rect);

    if EzDataset.SelfReferencing then
    begin
      if ARow > 2 then
      begin
        inc(Rect.Left, (ARow-3)*8 + 3);
        ImageList1.Draw(grSchemeGrid.Canvas, Rect.Left, Rect.Top+2, 4);
        inc(Rect.Left, 8);
      end;
    end
    else if ARow > 1 then
    begin
      inc(Rect.Left, (ARow-2)*8 + 3);
      ImageList1.Draw(grSchemeGrid.Canvas, Rect.Left, Rect.Top+3, 4);
      inc(Rect.Left, 8);
    end;

    EzRenderTextRect(grSchemeGrid.Canvas, Rect, GetCellValue(ACol, ARow), btsFlatButton, bstNormal, 0, DT_LEFT or DT_VCENTER or DT_SINGLELINE);
  end;
end;

procedure TEzSchemeDesigner.grSchemeGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ACol = 1 then
    CanSelect := false else
    CanSelect := true;
end;

end.
