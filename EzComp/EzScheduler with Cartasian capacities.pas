unit EzScheduler;

{$I Ez.inc}

interface

uses Classes, SysUtils, EzDataset, EzArrays, EzCalendar, ContNrs;

type
  EEzScheduleError = class(Exception);
  TEzResourceAssignments = class;
  TEzScheduler = class;
  TEzScheduleTask = class;

  TScheduleIntervalFlag = (
    // Do not schedule this interval
    ifDoNotSchedule,
    // Schedule this interval against the project calendar
    ifProjectCalendar,
    // Schedule this interval against the resource default availability profile.
    // That's the profile before allocating any scheduled task.
    ifResourceDefaultAvailability,
    // Schedule this interval against the resource actual availability profile.
    // That's the profile after allocating scheduled task.
    ifResourceActualAvailability
  );

  TEzWorkInterval = class
    TimeSpan: TEzTimespan;
    ResourceAssigments: TEzResourceAssignments;

  public
    destructor Destroy; override;
  end;

  TEzWorkIntervals = class(TEzObjectList)
  protected
    function GetItem(Index: Integer): TEzWorkInterval;
    procedure SetItem(Index: Integer; Value: TEzWorkInterval);
  public
    property Items[Index: Integer]: TEzWorkInterval read GetItem write SetItem; default;
  end;

  TEzScheduleInterval = class(TPersistent)
  public
    Start: TDateTime;
    Stop: TDateTime;
    ScheduleFlag: TScheduleIntervalFlag;
    Duration: Double;
    Tag: Integer;
    IsSlackInterval: Boolean;

    constructor Create; virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  end;

  TEzScheduleIntervals = class(TObjectList)
  protected
    function GetItem(Index: Integer): TEzScheduleInterval;
    procedure SetItem(Index: Integer; Value: TEzScheduleInterval);
  public
    property Items[Index: Integer]: TEzScheduleInterval read GetItem write SetItem; default;
  end;

  TEzCapacityKey = Variant;
  TEzTaskAssignment = class(TPersistent)
  protected
    FKey: Variant;
    FAssigned: Double;
    FAffectsDuration: Boolean;
    ScheduleSucceeded: Boolean;

  public
    constructor Create; overload; virtual;
    constructor Create(CopyFrom: TEzTaskAssignment); overload; virtual;
    procedure Assign(Source: TPersistent); override;

    property AffectsDuration: Boolean read FAffectsDuration write FAffectsDuration;
    property Assigned: Double read FAssigned write FAssigned;
    property Key: Variant read FKey write FKey;
  end;

  TEzTaskAssignments = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzTaskAssignment;
    function  GetText: string;
    procedure SetItem(Index: Integer; Value: TEzTaskAssignment);
    procedure SetText(Value: string);

  public
    property Text: string read GetText write SetText;
    property Items[Index: Integer]: TEzTaskAssignment read GetItem write SetItem; default;
  end;

  TEzResourceAssignment = class(TEzTaskAssignment)
  protected
    // CapacityKey holds the capacity for which this resource was selected.
    // If we're scheduling against capacity then all resources supporting
    // the required capacity are added to the profile. In order to keep
    // track of which resource was selected because of which capacity
    // this property is used.
    FCapacityKey: TEzCapacityKey;
    property CapacityKey: TEzCapacityKey read FCapacityKey write FCapacityKey;

  public
    constructor Create; override;
    constructor Create(CopyFrom: TEzResourceAssignment); overload;
  end;

  TEzResourceAssignments = class(TEzTaskAssignments)
  protected
    function  GetItem(Index: Integer): TEzResourceAssignment;
    procedure SetItem(Index: Integer; Value: TEzResourceAssignment);

    function IndexOfKey(Key: Variant): Integer;

  public
    property Items[Index: Integer]: TEzResourceAssignment read GetItem write SetItem; default;
  end;

  TEzSortedResourceAssignments = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzResourceAssignment;

    function IndexOfKey(Key: Variant): Integer;

  public
    property Items[Index: Integer]: TEzResourceAssignment read GetItem; default;
  end;

  TEzCapacityListItem = class
  protected
    FKey: TEzCapacityKey;

  public
    constructor Create(AKey: TEzCapacityKey);
    property Key: TEzCapacityKey read FKey write FKey;
  end;

  TEzCapacityList = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzCapacityListItem;
    procedure SetItem(Index: Integer; Value: TEzCapacityListItem);

  public
    property Items[Index: Integer]: TEzCapacityListItem read GetItem write SetItem; default;
  end;

  TEzCapacityAssignment = class(TEzTaskAssignment)
  protected
    FMergeProfiles: Boolean;

  public
    constructor Create; override;
    constructor Create(CopyFrom: TEzCapacityAssignment); overload;

    property MergeProfiles: Boolean read FMergeProfiles write FMergeProfiles;
  end;

  TEzCapacityAssignments = class(TEzTaskAssignments)
  protected
    function  GetItem(Index: Integer): TEzCapacityAssignment;
    procedure SetItem(Index: Integer; Value: TEzCapacityAssignment);

  public
    property Items[Index: Integer]: TEzCapacityAssignment read GetItem write SetItem; default;
  end;

  TEzSortedCapacityAssignments = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzCapacityAssignment;

    function IndexOfKey(Key: Variant): Integer;

  public
    property Items[Index: Integer]: TEzCapacityAssignment read GetItem; default;
  end;

  TEzResourcePath = class
    protected
      FResources: TEzResourceAssignments;
      FCapacities: TEzCapacityAssignments;

    public
      constructor Create(
          _Resources: TEzResourceAssignments;
          _Capacities: TEzCapacityAssignments);
      destructor Destroy; override;

      property Resources: TEzResourceAssignments read FResources;
      property Capacities: TEzCapacityAssignments read FCapacities;
  end;

  // A list of TEzResourceAssignments lists
  TEzResourcePathList = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzResourcePath;

  public

    property Items[Index: Integer]: TEzResourcePath read GetItem; default;
  end;
  
  TEzResource = class(TPersistent)
  private
    // Key value which uniquely identifies this resource
    FKey: Variant;

    // Descriptive name for this resource
    FName: string;

    // Indicates whether thios resource may switch to
    // a different task and back.
    FAllowTaskSwitching: Boolean;

  	// Pointer to the original availability graph for this resource.
	  // ActualAvailability will use Availability as a source to copy
  	// data from. When a task is scheduled, then the allocated intervals
	  // are substracted from ActualAvailability.
  	// Availability allways contains the original data.
	  FAvailability: TEzAvailabilityProfile;

  	// Pointer to the actual availability graph for this resource. This graph
    // keep track of the allocated and unallocated intervals.
  	FActualAvailability: TEzAvailabilityProfile;

    // Boolean value which indicates if the profile refrenced by property
    // Availability is owned by this instance. If OwnsAvailability is true, then
    // the object referenced by Availability will be destroyed when this object
    // will be destroyed.
    FOwnsAvailability: Boolean;

    FCapacities: TEzCapacityList;

    // A list of resources or capacities required for this resource
    // to function. Whenever this resource is scheduled, the resources
    // from this list should be allocated as well.
    FOperators: TEzTaskAssignments;

    public
      constructor Create(Scheduler: TEzScheduler; Resource: TEzResourceAssignment); reintroduce; overload;
      constructor Create(AKey: Variant); reintroduce; overload;
      destructor  Destroy; override;
      procedure   Assign(Source: TPersistent); override;
      procedure   ProfileRequired;

    published
      property Key: Variant read FKey;
      property Name: string read FName write FName;
      property AllowTaskSwitching: Boolean read FAllowTaskSwitching write FAllowTaskSwitching;
	    property Availability: TEzAvailabilityProfile read FAvailability write FAvailability;
	    property ActualAvailability: TEzAvailabilityProfile read FActualAvailability write FActualAvailability;
      property Capacities: TEzCapacityList read FCapacities;
      property Operators: TEzTaskAssignments read FOperators;
      property OwnsAvailability: Boolean read FOwnsAvailability write FOwnsAvailability;
  end;
  PEzScheduleResource = ^TEzResource;

  TEzResources = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzResource;

  public
    function  ResourceByKey(Key: Variant): TEzResource;
    function  ResourceExists(Key: Variant): Boolean;

    property Items[Index: Integer]: TEzResource read GetItem; default;
  end;

  TEzPredecessorRelation = (
    // Schedule successor task after the finish date of predecessor
    prFinishStart,
    // Schedule the end date of the successor task at the start date
    // of predecessor
    prStartFinish,
    // Schedule the start date of the successor after the start date
    // of predecessor
    prStartStart,
    // Schedule the end date of the successor before the end date
    // of predecessor
    prFinishFinish,
    // There is no relation between the successor and the predecessor.
    prNoRelation
  );

  TEzConstraint = (
    // Do Not Level,
    // Do not schedule this task but update the resource availability using
    // the start and stop dates from the task.
    cnDNL,
    // Must Start On,
    // The start date of this task must be equal to the constraint date.
    cnMSO,
    // Must Finish On
    cnMFO,
    // Start No Earlier Than,
    cnSNET,
    // Finish At or Before,
    cnFAB,
    // Start No Later Than
    cnSNLT,
    // Finish No Later Than,
    cnFNLT,
    // As Soon As Possible
    cnASAP,
    // As Late As Possible
    cnALAP,
    // Ignore task
    // Do not schedule this task and do not allocate the task interval
    // from the resources.
    cnIgnore
  );

  TEzCapacityResourceListItem = class
  protected
    FKey: TEzCapacityKey;
    FResources: TEzResources;
  public
    constructor Create(AKey: TEzCapacityKey); reintroduce;
    destructor  Destroy; override;
    property Key: TEzCapacityKey read FKey write FKey;
    property Resources: TEzResources read FResources;
  end;

  TEzCapacityResourceList = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzCapacityResourceListItem;
    procedure SetItem(Index: Integer; Value: TEzCapacityResourceListItem);

    function IndexOf(AKey: TEzCapacityKey): Integer; overload;

  public
    property Items[Index: Integer]: TEzCapacityResourceListItem read GetItem write SetItem; default;
  end;

  TEzPredecessor = class(TPersistent)
  public
    Key: TNodeKey;
    Relation: TEzPredecessorRelation;
    Lag: Double;

    constructor Create;
    procedure Assign(Source: TPersistent); override;
  end;

  TEzPredecessors = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzPredecessor;
    function  GetText: string;
    procedure SetItem(Index: Integer; Value: TEzPredecessor);
    procedure SetText(Value: string);
  public
    function IndexOf(Key: TNodeKey): integer;
    property Items[Index: Integer]: TEzPredecessor read GetItem write SetItem; default;
    property Text: string read GetTExt write SetText;
  end;

  TEzPredecessorSuccessor = class(TObject)
  public
    Predecessor, Successor: TEzScheduleTask;
    constructor Create(APred, ASucc: TEzScheduleTask);
  end;

  TEzPredecessorSuccessors = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzPredecessorSuccessor;
    procedure SetItem(Index: Integer; Value: TEzPredecessorSuccessor);
  public
    procedure AddRelation(APred, ASucc: TEzScheduleTask);
    function  RelationExists(APred, ASucc: TEzScheduleTask): Boolean;

    property Items[Index: Integer]: TEzPredecessorSuccessor read GetItem write SetItem; default;
  end;

  TEzScheduleSuccessor = class(TPersistent)
  public
    Task: TEzScheduleTask;
    Relation: TEzPredecessorRelation;
    Lag: Double;

    procedure Assign(Source: TPersistent); override;
  end;

  TEzScheduleSuccessors = class(TObjectList)
  protected
    function GetItem(Index: Integer): TEzScheduleSuccessor;
    procedure SetItem(Index: Integer; Value: TEzScheduleSuccessor);
  public
    property Items[Index: Integer]: TEzScheduleSuccessor read GetItem write SetItem; default;
  end;

  TScheduleWindowBoundType = (
    // Date boundary is not set, schedule window still empty
    btEmpty,

    // Date boundary is set from project start date
    btProjectStartDate,
    // Date boundary is set from project end date
    btProjectEndDate,
    // Date boundary is set from start date of indicated task
    btTaskStartDate,
    // Date boundary is set from end date of indicated task
    btTaskEndDate,
    // Date boundary is set from constraint date of indicated task
    btConstraintDate,
    // Date boundary is set from the start date of the schedule window
    btTaskWindowStartDate,
    // Date boundary is set from the end date of the schedule window
    btTaskWindowEndDate,
    // Date boundary is set from the start date of the slack window
    btSlackWindowStartDate,
    // Date boundary is set from the end date of the slack window
    btSlackWindowEndDate
    );

  // Holds internal data used by the scheduler for storing intermediate results.
  TEzInternalData = record
    // Reserved for internal use by the scheduler;
    // Stores the maximum resource assignment for this task. Tasks with
    // higher assignments will be scheduled first.
    MaxAssignement: Double;

    // Reserved for internal use by the scheduler;
    // Successors holds a list of successor tasks.
    Successors: TEzScheduleSuccessors;

    // Reference to the Task which marks the left side of the schedule window.
    // Most probably a predecessor.
    LeftTask: TEzScheduleTask;
    LeftBoundaryType: TScheduleWindowBoundType;

    // Reference to the Task which marks the right side of the schedule window.
    // Most probably a successor.
    RightTask: TEzScheduleTask;
    RightBoundaryType: TScheduleWindowBoundType;

    // Window holds the interval in which the task should be scheduled
    // (also called a schedule window). Window.Start marks the left side
    // of the window and Window.Stop marks the right side.
    //
    // The window is either determined by the project schedule window or by
    // tasks to the left or right side of the current task (successors and
    // predecessors).
    //
    // The scheduler will try to schedule a task between these boundaries.
    Window: TEzTimeSpan;

    // Direction in which the scheduler should try to find a suitable interval
    // for this task.
    Direction: TEzScheduleDirection;

    // Profile holds the combined availability profile to schedule against.
    Profile: TEzAvailabilityProfile;
    OwnsProfile: Boolean;
  end;

  TEzScheduleTask = class(TPersistent)
  public
    // The unique key of the task
    Key: TNodeKey;

    // Key value of the parent if this task is a child
    Parent: TNodeKey;

    // Constraint to comply with when scheduling.
    Constraint: TEzConstraint;

    // DateTime value in case coConstraint requires one.
    ConstraintDate: TDateTime;

    // Priority of task.
    // Lower values indicate higher priorities.
    Priority: Integer;
    Flags: TEzScheduleResults;
    Intervals: TEzScheduleIntervals;

    // A list of resources or capacities required to complete this task
    Resources: TEzTaskAssignments;

    // A list of resources assigned to this task.
//    Resources: TEzResourceAssignments;
    ResourceAllocation: TAllocationType;

    // A capacity required to complete this task.
    // A task can either have resources or a capacity assigned (not both)
//    Capacity: TEzCapacityAssignment;

    // A list of predecessors assigned to this task.
    Predecessors: TEzPredecessors;

    // Stores an integer value as part of a schedule task.
    // Description:
    //  Tag has no predefined meaning. The Tag property is provided for the
    //  convenience of developers. It can be used for storing an additional
    //  integer value or it can be typecast to any 32-bit value such as a
    //  component reference or a pointer.
    Tag: Integer;

    // ScheduleData holds data required by the schedule engine.
    ScheduleData: TEzInternalData;

    // Indicates the the schedulewindow for this task.
    // The task will not be scheduled outside this window.
    WindowStart: TDateTime;
    WindowStop: TDateTime;

    // Holds a list of work intervals allocated for this task.
    WorkIntervals: TEzWorkIntervals;

    // The amount of freetime required between this task and any of it's
    // successors
    Slack: Double;
    SlackWindowStart: TDateTime;
    SlackWindowStop: TDateTime;

    constructor Create; reintroduce; virtual;
    destructor  Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset;
  end;

  TEzSortedTaskList = class(TEzSortedObjectList)
  protected
    function  CompareItems(Obj1, Obj2: TObject): Integer; override;
    procedure SetTasks(Index: Integer; Task: TEzScheduleTask);
    function  GetTasks(Index: Integer): TEzScheduleTask;

  public
    constructor Create; reintroduce;
    property Tasks[Index: Integer]: TEzScheduleTask read GetTasks write SetTasks; default;
  end;

  // Extends TObjectQueue with a PushBack method.
  TEzObjectQueue = class(TObjectQueue)
  public
    // Empty queue
    procedure Clear;
    // Pushes an item at the back of the queue instead of at the beginning.
    procedure PushBack(AItem: TObject);
  end;

  TMessageSeverity = (
    msFatal,
    msError,
    msWarning,
    msNote
  );

  TEzScheduleMessage = class(TObject)
  public
    // Holds the key of the task to which this message belongs
    TaskKey: TNodeKey;
    // Holds the key of the resource which caused the scheduler to fail, if any.
    ResourceKey: Variant;
    // Text of message
    MessageText: string;
    // Left date boundary of the schedule window used during scheduling
    ScheduleWindowStart: TDateTime;
    // Right date boundary of the schedule window used during scheduling
    ScheduleWindowEnd: TDateTime;
    // Key of the (predecessor) task that marks the left boundary, if any.
    ScheduleWindowStartTaskKey: TNodeKey;
    // Key of the (successor) task that marks the right boundary, if any.
    ScheduleWindowEndTaskKey: TNodeKey;
    // Flag indicating how the left boundary was set
    StartBoundaryType: TScheduleWindowBoundType;
    // Flag indicating how the right boundary was set
    EndBoundaryType: TScheduleWindowBoundType;
    // Flag indicating severity of this schedule error
    Severity: TMessageSeverity;

    constructor Create; overload; virtual;
    constructor Create(Key: TNodeKey; AMessage: string; ASeverity: TMessageSeverity=msWarning); overload; virtual;

    procedure Assign(Source: TEzScheduleMessage);
  end;

  TEzScheduleMessages = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzScheduleMessage;

  public
    constructor Create; reintroduce;

    property Items[Index: Integer]: TEzScheduleMessage read GetItem; default;
  end;

  TEzScheduleThread = class(TThread)
  protected
    Scheduler: TEzScheduler;
    procedure Execute(); override;

  public
    constructor Create(AScheduler: TEzScheduler);
  end;

  TLoadResourceData = procedure (Scheduler: TEzScheduler; Resource: TEzResource) of object;
  TTestResourceCapacityEvent = procedure(
                                    Task: TEzScheduleTask;
                                    Capacity: TEzCapacityAssignment;
                                    Resource: TEzResource;
                                    var AllowResource: Boolean) of object;

  TEzScheduler = class(TComponent)
  private
    procedure UpdateTaskScheduleDirection(Task: TEzScheduleTask);
    procedure CrossJoinProfiles(Task: TEzScheduleTask; Paths: TEzResourcePathList);
  protected
    FUseWorkingPeriods: Boolean;
    FReturnWorkIntervals: Boolean;
    FSchedulebeyondScheduleWindow: Boolean;
    FScheduleTasks: TEzSortedTaskList;
    FPriorityStack: TEzObjectStack;
    FPredecessorStack: TEzObjectStack;
    FSuccessorStack: TEzObjectStack;
    FTrackPath: TEzPredecessorSuccessors;
    FScheduleMessages: TEzScheduleMessages;
    FTaskCursor: TEzDBCursor;
    FProjectProfile: TEzAvailabilityProfile;
    FProjectWindowAbsoluteStart: TDateTime;
    FProjectWindowAbsoluteStop: TDateTime;
    FProjectWindowStart: TDateTime;
    FProjectWindowStop: TDateTime;
    FProjectMakeSpanStart: TDateTime;
    FProjectMakeSpanStop: TDateTime;
    FResources: TEzResources;
    FCapacityResources: TEzCapacityResourceList;
    FScheduleIntervals: TEzTimespanArray;
    FScheduleResults: TEzScheduleResults;

    FOnLoadResourceData: TLoadResourceData;
    FOnTestResourceCapacity: TTestResourceCapacityEvent;

    procedure AllocateResources(  Task: TEzScheduleTask;
                                  Intervals: TEzTimespanArray;
                                  ResourcePath: TEzResourcePath); virtual;

		procedure CopyParentsResources(Task, Parent: TEzScheduleTask);
		procedure CopyParentPredecessors(Task, Parent: TEzScheduleTask);
    procedure DoLoadResourceData(Resource: TEzResource);
    function  DoTestResourceCapacity(Task: TEzScheduleTask;
                                     Capacity: TEzCapacityAssignment;
                                     Resource: TEzResource): Boolean;

    procedure AddScheduleMessage(Task: TEzScheduleTask; AMessage: string); overload;
    procedure AddScheduleMessage(Task: TEzScheduleTask; ResourceKey: Variant; AMessage: string); overload;
    procedure BuildResourceCapacityList;
    procedure BuildScheduleList;
    procedure CalculateSlackWindow(Task: TEzScheduleTask; TaskWindow: TEzTimeSpan);
    procedure CheckScheduleResult(Task: TEzScheduleTask; Window: TEzTimeSpan); virtual;
    procedure FillSuccessors;
    function  GetProjectProfile: TEzAvailabilityProfile; virtual;
    function  GetTaskScheduleData(Task: TEzScheduleTask; ScheduleWithPriority: Boolean) : Boolean;
    function  GetTaskWindow(Task: TEzScheduleTask) : TEzTimeSpan; virtual;
    function  IsFixedPoint(Task: TEzScheduleTask; Direction: TEzScheduleDirection) : Boolean; virtual;
    procedure LoadResourceData(Resource: TEzResourceAssignment); virtual;
    procedure NormalizePredecessors(Task: TEzScheduleTask);
    procedure ProcessResources(Task: TEzScheduleTask);
    procedure Run;
    procedure ScheduleTasks; virtual;
    procedure ScheduleTask(TaskKey: TNodeKey; Task: TEzScheduleTask; ScheduleWithPriority: Boolean);
    procedure ScheduleTask_DoNotLevel(Task: TEzScheduleTask);
    procedure ScheduleToProjectCalendar(Key: Variant; Task: TEzScheduleTask; Interval: TEzScheduleInterval; var ResultInterval: TEzTimeSpan);
//    procedure ScheduleToOriginalProfile(Key: Variant; Task: TEzScheduleTask; Interval: TEzScheduleInterval; var ResultInterval: TEzTimeSpan);
    procedure ScheduleToResourceProfile(const Key: Variant; Task: TEzScheduleTask; Interval: TEzScheduleInterval; TestSchedule: Boolean; var ResultInterval: TEzTimeSpan);
    procedure StoreScheduleTask(Node: TRecordNode); virtual;
//    function  TrackFailingResource(Task: TEzScheduleTask; Interval: TEzScheduleInterval): TEzResource;
    procedure UpdateWindowWithTask(var ScheduleData: TEzInternalData; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithPredecessor(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithPredecessorTaskWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
    procedure UpdateWindowWithPredecessorSlackWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
    procedure UpdateWindowWithSuccessor(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithSuccessorTaskWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithSuccessorSlackWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask); virtual;    
    function  VerifyTaskWindow(Task: TEzScheduleTask): Boolean; virtual;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure AddTask(Task: TEzScheduleTask);
    function  FindTask(Key: TNodeKey): TEzScheduleTask;
    procedure RemoveTask(Task: TEzScheduleTask);
    function  GetNextScheduledTask(var Task: TEzScheduleTask): Boolean;
    procedure Reset(ResetAll: Boolean = True);
    procedure Reschedule;

    property Messages: TEzScheduleMessages read FScheduleMessages;
    property ProjectMakeSpanStart: TDateTime read FProjectMakeSpanStart;
    property ProjectMakeSpanStop: TDateTime read FProjectMakeSpanStop;

    property ProjectProfile: TEzAvailabilityProfile
                  read FProjectProfile
                  write FProjectProfile;

    property Resources: TEzResources read FResources;
    property ScheduleResults: TEzScheduleResults read FScheduleResults;
    property UseWorkingPeriods: Boolean read FUseWorkingPeriods write FUseWorkingPeriods default False;

  published
    property SchedulebeyondScheduleWindow: Boolean
              read FSchedulebeyondScheduleWindow
              write FSchedulebeyondScheduleWindow
              default False;

    property ProjectWindowAbsoluteStart: TDateTime read FProjectWindowAbsoluteStart write FProjectWindowAbsoluteStart;
    property ProjectWindowStart: TDateTime read FProjectWindowStart write FProjectWindowStart;
    property ProjectWindowAbsoluteStop: TDateTime read FProjectWindowAbsoluteStop write FProjectWindowAbsoluteStop;
    property ProjectWindowStop: TDateTime read FProjectWindowStop write FProjectWindowStop;
    property ReturnWorkIntervals: Boolean read FReturnWorkIntervals write FReturnWorkIntervals default false;
    property OnLoadResourceData: TLoadResourceData
                read FOnLoadResourceData write FOnLoadResourceData;
    property OnTestResourceCapacity: TTestResourceCapacityEvent
                read FOnTestResourceCapacity write FOnTestResourceCapacity;
  end;

  function IsMileStone(Task: TEzScheduleTask): Boolean;

implementation

uses EzStrings_3, Math, EzDatetime
{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
;
{$ENDIF}

procedure EzScheduleError(const Message: string; Scheduler: TEzScheduler = nil);
begin
  if Assigned(Scheduler) then
    raise EEzScheduleError.Create(Format('%s: %s', [Scheduler.Name, Message])) else
    raise EEzScheduleError.Create(Message);
end;

function PosEx(const SubStr, S: string; Offset: Cardinal = 1): Integer;
var
  I,X: Integer;
  Len, LenSubStr: Integer;
begin
  if Offset = 1 then
    Result := Pos(SubStr, S)
  else
  begin
    I := Offset;
    LenSubStr := Length(SubStr);
    Len := Length(S) - LenSubStr + 1;
    while I <= Len do
    begin
      if S[I] = SubStr[1] then
      begin
        X := 1;
        while (X < LenSubStr) and (S[I + X] = SubStr[X + 1]) do
          Inc(X);
        if (X = LenSubStr) then
        begin
          Result := I;
          exit;
        end;
      end;
      Inc(I);
    end;
    Result := 0;
  end;
end;

procedure SplitString(Value: string; sl: TStringList);
var
  i, l, p: Integer;
  q: Boolean;

begin
  i:=1;
  p:=1;
  q:=False;
  l:=length(Value);
  if l=0 then Exit;
  while i<=l do
  begin
    if (Value[i] = '''') and (i<l) and (Value[i+1] <> '''') then
      q := not q;

    if (Value[i] = ',') and not q then
    begin
      sl.Add(Copy(Value, p, i-p));
      p:=i+1;
    end;
    inc(i);
  end;
  sl.Add(Copy(Value, p, i-p));
end;

function SaveStrToFloat(Text: string): Double;
var
  C: Char;

begin
  C := DecimalSeparator;
  DecimalSeparator := '.';
  try
    Result := StrToFloat(StringReplace(Text, ',', '.', [rfReplaceAll]));
  finally
    DecimalSeparator := C;
  end;
end;

function SafeString(Value: Variant): String;
begin
  if not VarIsNull(Value) then
    Result := Value else
    Result := SNoString;
end;

function EstimateTaskduration(Task: TEzScheduleTask): Double;
var
  i: Integer;

begin
  Result := 0;
  for i:=0 to Task.Intervals.Count-1 do
    Result := Result+Task.Intervals[i].Duration;
end;

function GetExtendsFromIntervals(Intervals: TEzTimespanArray): TEzTimeSpan; overload;
var
  i: Integer;
  ts: TEzTimespan;

begin
  if Intervals.Count=0 then
    EzScheduleError(SIntevalsAreEmpty);

  Result.Start := MaxEzDatetime;
  Result.Stop := 0;
  for i:=0 to Intervals.Count-1 do
  begin
    ts := Intervals[i];
    Result.Start := min(Result.Start, ts.Start);
    Result.Stop := max(Result.Stop, ts.Stop);
  end;
end;

function GetExtendsFromIntervals(Intervals: TEzScheduleIntervals): TEzTimeSpan; overload;
var
  i: Integer;
  ts: TEzScheduleInterval;
begin
  if Intervals.Count=0 then
    EzScheduleError(SIntevalsAreEmpty);

  Result.Start := MaxEzDatetime;
  Result.Stop := 0;
  for i:=0 to Intervals.Count-1 do
  begin
    ts := Intervals[i];
    Result.Start := min(Result.Start, DatetimeToEzDatetime(ts.Start));
    Result.Stop := max(Result.Stop, DatetimeToEzDatetime(ts.Stop));
  end;
end;

function IsMileStone(Task: TEzScheduleTask): Boolean;
begin
  Result := (Task.Intervals.Count=0) and (Task.ConstraintDate>0);
end;
//=----------------------------------------------------------------------------=
// TEzTaskAssignment
//=----------------------------------------------------------------------------=
constructor TEzTaskAssignment.Create;
begin
  inherited;
  Assigned := 1.0;
  ScheduleSucceeded := False;
  FAffectsDuration := False;
end;

procedure TEzTaskAssignment.Assign(Source: TPersistent);
begin
  if Source is TEzTaskAssignment then
  begin
    FKey := TEzTaskAssignment(Source).Key;
    FAssigned := TEzTaskAssignment(Source).Assigned;
    FAffectsDuration := TEzTaskAssignment(Source).AffectsDuration;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzTaskAssignments
//=----------------------------------------------------------------------------=
function TEzTaskAssignments.GetItem(Index: Integer): TEzTaskAssignment;
begin
  Result := TEzTaskAssignment(inherited GetItem(Index));
end;

function TEzTaskAssignments.GetText: string;
var
  i: Integer;
  r: TEzTaskAssignment;

begin
  Result := '';
  for i:=0 to Count-1 do
  begin
    r := Items[i];
    if Result <> '' then
      Result := result + ',';

    if r.Assigned <> 1.0 then
      Result := r.Key + ':' + FloatToStr(r.Assigned) else
      Result := r.Key;
  end;
end;

procedure TEzTaskAssignments.SetItem(Index: Integer; Value: TEzTaskAssignment);
begin
  inherited SetItem(Index, Value);
end;

//
// Handle Text property
// Strings can be assigned like: John,Henk or John:2,0,Piet
//
procedure TEzTaskAssignments.SetText(Value: string);
var
  sl: TStringList;
  i, p1: Integer;
  R: TEzTaskAssignment;
  val: string;

begin
  Clear;
  sl := TStringList.Create;
  try
    SplitString(Value, sl);

    for i:=0 to sl.count-1 do
    begin
      R := TEzTaskAssignment.Create;
      val := sl[i];
      p1 := Pos(':', val);

      if p1 = 0 then
      begin
        R.Key := val;
        R.Assigned := 1.0;
      end
      else
      begin
        R.Key := Copy(val, 1, p1-1);
        R.Assigned := SaveStrToFloat(Copy(val,p1+1,MaxInt));
      end;
      Add(R);
    end;
  finally
    sl.Destroy;
  end;
end;

constructor TEzCapacityListItem.Create(AKey: Variant);
begin
  FKey := AKey;
end;

function TEzCapacityList.GetItem(Index: Integer): TEzCapacityListItem;
begin
  Result := TEzCapacityListItem(inherited GetItem(Index));
end;

procedure TEzCapacityList.SetItem(Index: Integer; Value: TEzCapacityListItem);
begin
  inherited SetItem(Index, Value);
end;

function TEzResourceAssignments.GetItem(Index: Integer): TEzResourceAssignment;
begin
  Result := TEzResourceAssignment(inherited GetItem(Index));
end;

function TEzResourceAssignments.IndexOfKey(Key: Variant): Integer;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    if Items[i].Key = Key then
    begin
      Result := i;
      Exit;
    end;
  end;
  Result := -1;
end;

procedure TEzResourceAssignments.SetItem(Index: Integer; Value: TEzResourceAssignment);
begin
  inherited SetItem(Index, Value);
end;

constructor TEzCapacityAssignment.Create(CopyFrom: TEzCapacityAssignment);
begin
  inherited Create(CopyFrom);
  FMergeProfiles := CopyFrom.MergeProfiles;
end;

constructor TEzCapacityAssignment.Create;
begin
  inherited;
end;

function TEzCapacityAssignments.GetItem(Index: Integer): TEzCapacityAssignment;
begin
  Result := TEzCapacityAssignment(inherited GetItem(Index));
end;

procedure TEzCapacityAssignments.SetItem(Index: Integer; Value: TEzCapacityAssignment);
begin
  inherited SetItem(Index, Value);
end;

//=----------------------------------------------------------------------------=
// TEzScheduleInterval
//=----------------------------------------------------------------------------=
constructor TEzScheduleInterval.Create;
begin
  inherited;
  ScheduleFlag := ifResourceActualAvailability;
end;

destructor TEzScheduleInterval.Destroy;
begin
  inherited;
end;

procedure TEzScheduleInterval.Assign(Source: TPersistent);
begin
  if Source is TEzScheduleInterval then
  begin
    Start := TEzScheduleInterval(Source).Start;
    Stop := TEzScheduleInterval(Source).Stop;
    ScheduleFlag := TEzScheduleInterval(Source).ScheduleFlag;
    Duration := TEzScheduleInterval(Source).Duration;
    Tag := TEzScheduleInterval(Source).Tag;
    IsSlackInterval := TEzScheduleInterval(Source).IsSlackInterval;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleIntervals
//=----------------------------------------------------------------------------=
function TEzScheduleIntervals.GetItem(Index: Integer): TEzScheduleInterval;
begin
  Result := TEzScheduleInterval(inherited GetItem(Index));
end;

procedure TEzScheduleIntervals.SetItem(Index: Integer; Value: TEzScheduleInterval);
begin
  inherited SetItem(Index, Value);
end;

//=----------------------------------------------------------------------------=
// TEzResource
//=----------------------------------------------------------------------------=
constructor TEzResource.Create(AKey: Variant);
begin
  inherited Create;
  FKey := AKey;
  FAllowTaskSwitching := False;
  FAvailability := nil;
  FName := '';
  FOwnsAvailability := True;
  FCapacities := TEzCapacityList.Create(True);
  FOperators := TEzTaskAssignments.Create(True);
end;

constructor TEzResource.Create(Scheduler: TEzScheduler; Resource: TEzResourceAssignment);
begin
  Create(Resource.Key);
  Scheduler.DoLoadResourceData(Self);
end;

destructor TEzResource.Destroy;
begin
  inherited;
  FActualAvailability.Free;
  FCapacities.Free;
  if FOwnsAvailability then
    FAvailability.Free;
  FOperators.Free;
end;

procedure TEzResource.Assign(Source: TPersistent);
begin
  if Source is TEzResource then
  begin
    FKey := TEzResource(Source).Key;
    FName := TEzResource(Source).Name;
	  FAvailability := TEzResource(Source).Availability;
    ActualAvailability.Assign(TEzResource(Source).ActualAvailability);
    FCapacities.Assign(TEzResource(Source).Capacities);
  end else
    inherited;
end;

procedure TEzResource.ProfileRequired;
begin
  if FActualAvailability=nil then
  begin
    FActualAvailability := TEzAvailabilityProfile.Create(0);
    FActualAvailability.AddSource(FAvailability, False);
  end;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleTask
//=----------------------------------------------------------------------------=
constructor TEzScheduleTask.Create;
begin
  inherited;
  Constraint := cnAsap;
  Flags := [];
  Intervals := TEzScheduleIntervals.Create;
  Resources := TEzTaskAssignments.Create(True);
  ResourceAllocation:= alFixed;
  Predecessors := TEzPredecessors.Create(True);
  ScheduleData.Direction := sdUndefined;
  ScheduleData.Successors := TEzScheduleSuccessors.Create;
  ScheduleData.LeftTask := nil;
  ScheduleData.RightTask := nil;
  ScheduleData.Profile := nil;
  WindowStart := 0;
  WindowStop := 0;
  Slack := 0;
  SlackWindowStart := 0;
  SlackWindowStop := 0;
  WorkIntervals := nil;
end;

destructor TEzScheduleTask.Destroy;
begin
  inherited;
  Intervals.Free;
  Resources.Free;
  Predecessors.Free;
  ScheduleData.Successors.Free;
  if ScheduleData.OwnsProfile then
    ScheduleData.Profile.Free;

  WorkIntervals.Free;
end;

procedure TEzScheduleTask.Reset;
begin
  Flags := [];
  ScheduleData.Direction := sdUndefined;
  ScheduleData.Successors.Free;
  ScheduleData.Successors := TEzScheduleSuccessors.Create;
  ScheduleData.LeftTask := nil;
  ScheduleData.RightTask := nil;
  ScheduleData.Profile := nil;
end;

procedure TEzScheduleTask.Assign(Source: TPersistent);
begin
end;

constructor TEzSortedTaskList.Create;
begin
  inherited Create(False {object are not automatically freed});
  Duplicates := dupAccept;
end;

function TEzSortedTaskList.CompareItems(Obj1, Obj2: TObject): Integer;
var
  i1: TEzScheduleTask;
  i2: TEzScheduleTask;

begin
  i1 := TEzScheduleTask(Obj1);
  i2 := TEzScheduleTask(Obj2);

	if i1.Constraint <> i2.Constraint then
  begin
    if i1.Constraint < i2.Constraint then
      Result := -1 else
      Result := 1;
    Exit;
  end;

	if i1.Priority <> i2.Priority then
  begin
    if i1.Priority < i2.Priority then
      Result := -1 else
      Result := 1;
    Exit;
  end;

  if (i1.Constraint < cnASAP) and (i1.ConstraintDate <> i2.ConstraintDate) then
  begin
    if i1.ConstraintDate < i2.ConstraintDate then
      Result := -1 else
      Result := 1;
    Exit;
  end;

  if i1.ScheduleData.MaxAssignement = i1.ScheduleData.MaxAssignement then
    Result := 0
  else if i1.ScheduleData.MaxAssignement < i1.ScheduleData.MaxAssignement then
    Result := -1
  else
    Result := 1;
end;

procedure TEzSortedTaskList.SetTasks(Index: Integer; Task: TEzScheduleTask);
begin
  inherited Items[index] := Task;
end;

function TEzSortedTaskList.GetTasks(Index: Integer): TEzScheduleTask;
begin
  result := TEzScheduleTask(inherited Items[index]);
end;

//=----------------------------------------------------------------------------=
// TEzObjectQueue
//=----------------------------------------------------------------------------=
procedure TEzObjectQueue.Clear;
begin
  List.Clear;
end;

procedure TEzObjectQueue.PushBack(AItem: TObject);
begin
  List.Insert(List.Count, AItem);
end;

//=----------------------------------------------------------------------------=
// TEzCapacityResourceListItem
//=----------------------------------------------------------------------------=
constructor TEzCapacityResourceListItem.Create(AKey: TEzCapacityKey);
begin
  FKey := AKey;
  FResources := TEzResources.Create(False);
end;

destructor TEzCapacityResourceListItem.Destroy;
begin
  inherited;
  FResources.Free;
end;

//=----------------------------------------------------------------------------=
// TEzCapacityResourceList
//=----------------------------------------------------------------------------=
function TEzCapacityResourceList.CompareItems(Item1, Item2: TObject): Integer;
var
  i1: TEzCapacityResourceListItem;
  i2: TEzCapacityResourceListItem;

begin
  i1 := TEzCapacityResourceListItem(Item1);
  i2 := TEzCapacityResourceListItem(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzCapacityResourceList.GetItem(Index: Integer): TEzCapacityResourceListItem;
begin
  Result := TEzCapacityResourceListItem(inherited GetItem(Index));
end;

procedure TEzCapacityResourceList.SetItem(Index: Integer; Value: TEzCapacityResourceListItem);
begin
  inherited SetItem(Index, Value);
end;

function TEzCapacityResourceList.IndexOf(AKey: TEzCapacityKey): Integer;
var
  Find: TEzCapacityResourceListItem;

begin
  Find := TEzCapacityResourceListItem.Create(AKey);
  try
    Result := IndexOf(Find);
  finally
    Find.Destroy;
  end;
end;

//=----------------------------------------------------------------------------=
// TEzPredecessor
//=----------------------------------------------------------------------------=
constructor TEzPredecessor.Create;
begin
  Relation := prFinishStart;
  Lag := 0.0;
end;

procedure TEzPredecessor.Assign(Source: TPersistent);
begin
  if Source is TEzPredecessor then
  begin
    Key := TEzPredecessor(Source).Key;
    Relation := TEzPredecessor(Source).Relation;
    Lag := TEzPredecessor(Source).Lag;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzPredecessors
//=----------------------------------------------------------------------------=
function TEzPredecessors.GetItem(Index: Integer): TEzPredecessor;
begin
  Result := TEzPredecessor(inherited GetItem(Index));
end;

function TEzPredecessors.GetText: string;
var
  i: Integer;
  P: TEzPredecessor;

begin
  Result := '';
  for i:=0 to Count-1 do
  begin
    P := Items[i];
    if Result <> '' then
      Result := Result + ',';
    Result := Result + IntToStr(P.Key.Value);
    if P.Relation <> prFinishStart then
    begin
      case P.Relation of
        prStartStart:
          Result := Result + ':SS';
        prStartFinish:
          Result := Result + ':SF';
        prFinishFinish:
          Result := Result + ':FF';
      end;
    end;

    if P.Lag <> 0.0 then
      Result := Result + ':' + FloatToStr(P.Lag)
  end;
end;

function TEzPredecessors.IndexOf(Key: TNodeKey): integer;
begin
  Result := 0;
  while (Result<Count) and (CompareNodeKeys(Items[Result].Key, Key) <> 0) do
    inc(Result);
  if Result = Count then
    Result := -1;
end;

procedure TEzPredecessors.SetItem(Index: Integer; Value: TEzPredecessor);
begin
  inherited SetItem(Index, Value);
end;

procedure TEzPredecessors.SetText(Value: string);
const
  RelationChars = ['f', 'F', 's', 'S'];
var
  sl: TStringList;
  i, p1, p2: Integer;
  P: TEzPredecessor;
  val: string;

  function TranslateRelation(Value: string): TEzPredecessorRelation;
  begin
    Value := UpperCase(Value);
    if Value = 'FS' then
      Result := prFinishStart
    else if Value = 'SS' then
      Result := prStartStart
    else if Value = 'SF' then
      Result := prStartFinish
    else if Value = 'FF' then
      Result := prFinishFinish
    else
    begin
      Result := prFinishStart;
      EzScheduleError(SInvalidPredecessorRelation);
    end;
  end;

begin
  Clear;
  if Value = '' then Exit;
  sl := TStringList.Create;
  try
    SplitString(Value, sl);
    for i:=0 to sl.count-1 do
    begin
      P := TEzPredecessor.Create;
      val := sl[i];
      p1 := Pos(':', val);
      if p1 <> 0 then
        p2 := PosEx(':', val, p1+1) else
        p2 := 0;

      if p1 = 0 then
      begin
        P.Key.Value := StrToInt(val);
        P.Key.Level := -1;
        P.Relation := prFinishStart;
        P.Lag := 0.0;
      end
      else if p2 = 0 then
      begin
        P.Key.Value := StrToInt(Copy(val, 1, p1-1));
        P.Key.Level := -1;

        if (length(val)-p1 = 2) and
        {$IFDEF EZ_D2009}
          CharInSet(val[p1+1], RelationChars) and CharInSet(val[p1+2], RelationChars)
        {$ELSE}
          (val[p1+1] in RelationChars) and (val[p1+2] in RelationChars)
        {$ENDIF}
        then
        begin
          P.Relation := TranslateRelation(Copy(Val, p1+1,MaxInt));
          P.Lag := 0.0;
        end
        else
        begin
          P.Relation := prFinishStart;
          P.Lag := SaveStrToFloat(Copy(val,p1+1,MaxInt));
        end;
      end
      else
      begin
        P.Key.Value := StrToInt(Copy(val, 1, p1-1));
        P.Key.Level := -1;
        P.Relation := TranslateRelation(Copy(Val, p1+1,p2-p1-1));
        P.Lag := SaveStrToFloat(Copy(val,p2+1,MaxInt));
      end;
      Add(P);
    end;
  finally
    sl.Destroy;
  end;
end;

//=----------------------------------------------------------------------------=
// TEzPredecessorSuccessors
//=----------------------------------------------------------------------------=
constructor TEzPredecessorSuccessor.Create(APred, ASucc: TEzScheduleTask);
begin
  inherited Create;
  Predecessor := APred;
  Successor := ASucc;
end;

procedure TEzPredecessorSuccessors.AddRelation(APred, ASucc: TEzScheduleTask);
begin
  Add(TEzPredecessorSuccessor.Create(APred, ASucc));
end;

function TEzPredecessorSuccessors.CompareItems(Item1, Item2: TObject): Integer;
var
  R1, R2: TEzPredecessorSuccessor;
begin
  R1 := TEzPredecessorSuccessor(Item1);
  R2 := TEzPredecessorSuccessor(Item2);
  Result := CompareNodeKeys(R1.Predecessor.Key, R2.Predecessor.Key);
  if Result = 0 then
    Result := CompareNodeKeys(R1.Successor.Key, R2.Successor.Key);
end;

function TEzPredecessorSuccessors.GetItem(Index: Integer): TEzPredecessorSuccessor;
begin
  Result := TEzPredecessorSuccessor(inherited Items[Index]);
end;

procedure TEzPredecessorSuccessors.SetItem(Index: Integer; Value: TEzPredecessorSuccessor);
begin
  inherited Items[Index] := Value;
end;

function TEzPredecessorSuccessors.RelationExists(APred, ASucc: TEzScheduleTask): Boolean;
var
  Search: TEzPredecessorSuccessor;
begin
  Search := TEzPredecessorSuccessor.Create(APred, ASucc);
  try
    Result := IndexOf(Search)<>-1;
  finally
    Search.Destroy;
  end;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleSuccessor
//=----------------------------------------------------------------------------=
procedure TEzScheduleSuccessor.Assign(Source: TPersistent);
begin
  if Source is TEzScheduleSuccessor then
  begin
    Task := TEzScheduleSuccessor(Source).Task;
    Relation := TEzScheduleSuccessor(Source).Relation;
    Lag := TEzScheduleSuccessor(Source).Lag;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleSuccessors
//=----------------------------------------------------------------------------=
function TEzScheduleSuccessors.GetItem(Index: Integer): TEzScheduleSuccessor;
begin
  Result := TEzScheduleSuccessor(inherited GetItem(Index));
end;

procedure TEzScheduleSuccessors.SetItem(Index: Integer; Value: TEzScheduleSuccessor);
begin
  inherited SetItem(Index, Value);
end;

constructor TEzScheduleMessage.Create;
begin
  TaskKey.Value := Null;
  MessageText := '';
  Severity := msWarning;
  ResourceKey := Null;
  ScheduleWindowStart := 0;
  ScheduleWindowEnd := 0;
  ScheduleWindowStartTaskKey.Level := -1;
  ScheduleWindowStartTaskKey.Value := Null;
  ScheduleWindowEndTaskKey.Level := -1;
  ScheduleWindowEndTaskKey.Value := Null;
end;

constructor TEzScheduleMessage.Create(Key: TNodeKey; AMessage: string; ASeverity: TMessageSeverity=msWarning);
begin
  Create;

  TaskKey := Key;
  MessageText := AMessage;
  Severity := ASeverity;
end;

procedure TEzScheduleMessage.Assign(Source: TEzScheduleMessage);
begin
  TaskKey := Source.TaskKey;
  MessageText := Source.MessageText;
  Severity := Source.Severity;
  ResourceKey := Source.ResourceKey;
  ScheduleWindowStart := Source.ScheduleWindowStart;
  ScheduleWindowEnd := Source.ScheduleWindowEnd;
  ScheduleWindowStartTaskKey := Source.ScheduleWindowStartTaskKey;
  ScheduleWindowEndTaskKey := Source.ScheduleWindowEndTaskKey;
  StartBoundaryType := Source.StartBoundaryType;
  EndBoundaryType := Source.EndBoundaryType;  
end;

//=----------------------------------------------------------------------------=
// TEzMessageList
//=----------------------------------------------------------------------------=
constructor TEzScheduleMessages.Create;
begin
  inherited Create(True);
  OwnsObjects := True;
end;

function TEzScheduleMessages.GetItem(Index: Integer): TEzScheduleMessage;
begin
  Result := TEzScheduleMessage(inherited Items[Index]);
end;

function TEzResources.CompareItems(Item1, Item2: TObject): Integer;
var
  i1: TEzResource;
  i2: TEzResource;

begin
  i1 := TEzResource(Item1);
  i2 := TEzResource(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzResources.GetItem(Index: Integer): TEzResource;
begin
  Result := TEzResource(inherited Items[Index]);
end;

function TEzResources.ResourceExists(Key: Variant): Boolean;
var
  Find: TEzResource;

begin
  Find := TEzResource.Create;
  try
    Find.FKey := Key;
    if IndexOf(Find) = -1 then
      Result := False else
      Result := True;
  finally
    Find.Destroy;
  end;
end;

function TEzResources.ResourceByKey(Key: Variant): TEzResource;
var
  Find: TEzResource;
  i: Integer;

begin
  Find := TEzResource.Create;
  try
    Find.FKey := Key;
    if not IndexOf(i, Find) then
      EzScheduleError(Format(SResourceNotFound, [SafeString(Key)]));
    Result := Items[i];
  finally
    Find.Destroy;
  end;
end;

constructor TEzScheduleThread.Create(AScheduler: TEzScheduler);
begin
  Scheduler := AScheduler;
  inherited Create(False {Run immediately});
end;

procedure TEzScheduleThread.Execute();
begin
  Scheduler.Run;
end;

constructor TEzScheduler.Create(AOwner: TComponent);
begin
  inherited;
  FTaskCursor := TEzDBCursor.Create(nil);
  FTaskCursor.Options := FTaskCursor.Options + [cpAutoCreateParents, cpShowAll];
  FScheduleTasks := TEzSortedTaskList.Create;
  FPriorityStack := TEzObjectStack.Create(False);
  FTrackPath := TEzPredecessorSuccessors.Create(True);
  FPredecessorStack := TEzObjectStack.Create(False);
  FSuccessorStack := TEzObjectStack.Create(False);
  FScheduleMessages := TEzScheduleMessages.Create;
  FResources := TEzResources.Create(True);
  FCapacityResources := TEzCapacityResourceList.Create(True);
  FScheduleIntervals := TEzTimespanArray.Create;
  FUseWorkingPeriods := False;
  FReturnWorkIntervals := False;
end;

destructor TEzScheduler.Destroy;
begin
  // Reset will free-up memory associated with tasks etc.
  Reset;

  inherited;
  FTaskCursor.Free;
  FScheduleTasks.Free;
  FPriorityStack.Free;
  FTrackPath.Free;
  FPredecessorStack.Free;
  FSuccessorStack.Free;
  FScheduleMessages.Free;
  FResources.Free;
  FCapacityResources.Free;
  FScheduleIntervals.Free;
end;

procedure TEzScheduler.AddScheduleMessage(Task: TEzScheduleTask; AMessage: string);
begin
  AddScheduleMessage(Task, Null, AMessage);
end;

procedure TEzScheduler.AddScheduleMessage(Task: TEzScheduleTask; ResourceKey: Variant; AMessage: string);
var
  SMessage: TEzScheduleMessage;
begin
  SMessage := TEzScheduleMessage.Create(Task.Key, AMessage);

  SMessage.ResourceKey := ResourceKey;
  SMessage.ScheduleWindowStart := EzDateTimeToDateTime(Task.ScheduleData.Window.Start);
  SMessage.ScheduleWindowEnd := EzDateTimeToDateTime(Task.ScheduleData.Window.Stop);
  if Task.ScheduleData.LeftTask<>nil then
    SMessage.ScheduleWindowStartTaskKey := Task.ScheduleData.LeftTask.Key;
  if Task.ScheduleData.RightTask<>nil then
    SMessage.ScheduleWindowEndTaskKey := Task.ScheduleData.RightTask.Key;
  SMessage.StartBoundaryType := Task.ScheduleData.LeftBoundaryType;
  SMessage.EndBoundaryType := Task.ScheduleData.RightBoundaryType;

  FScheduleMessages.Add(SMessage);
end;

procedure TEzScheduler.AddTask(Task: TEzScheduleTask);
var
  Node: TRecordNode;

begin
  if FTaskCursor.FindNode(Task.Key) <> FTaskCursor.EndNode then
    EzScheduleError(Format(SDuplicateTask, [IntToStr(Task.Key.Level) + ':' + SafeString(Task.Key.Value)]));

  Node := FTaskCursor.Add(Task.Parent, Task.Key, [], True);
  Node.Data := Task;
end;

function TEzScheduler.FindTask(Key: TNodeKey): TEzScheduleTask;
var
  Node: TRecordNode;
begin
  Node := FTaskCursor.FindNode(Key);
  if Node = FTaskCursor.EndNode then
    Result := nil else
    Result := Node.Data;
end;

procedure TEzScheduler.RemoveTask(Task: TEzScheduleTask);
var
  Node: TRecordNode;
begin
  Node := FTaskCursor.FindNode(Task.Key);
  if Node<>FTaskCursor.EndNode then
  begin
    FTaskCursor.SortedKeys.DeleteKey(Node.Key);
    FTaskCursor.CutOffNode(Node);
  end;
end;

procedure TEzScheduler.BuildResourceCapacityList;
var
  Find: TEzCapacityResourceListItem;
  i, n, AIndex: Integer;
begin
  Find := TEzCapacityResourceListItem.Create(Null);
  try
    for i:=0 to Resources.Count-1 do
    begin
      for n:=0 to Resources[i].Capacities.Count-1 do
      begin
        // Check if this capacity key already exists
        Find.Key := Resources[i].Capacities[n].Key;
        AIndex := FCapacityResources.IndexOf(Find);
        if AIndex=-1 then
          AIndex := FCapacityResources.Add(TEzCapacityResourceListItem.Create(Find.Key));

        // Add resource to this capacity
        FCapacityResources[AIndex].Resources.Add(Resources[i]);
      end;
    end;
  finally
    Find.Free;
  end;
end;

procedure TEzScheduler.BuildScheduleList;
  //
  // Add all tasks in reverse order.
  //
begin
  if FTaskCursor.IsEmpty then Exit;
  FTaskCursor.MoveLast;

  while not FTaskCursor.Bof do
  begin
    if not FTaskCursor.HasChildren then
      StoreScheduleTask(FTaskCursor.CurrentNode);
    FTaskCursor.MovePrevious;
  end;
end;

procedure TEzScheduler.CalculateSlackWindow(Task: TEzScheduleTask; TaskWindow: TEzTimeSpan);
var
  Int: TEzScheduleInterval;
  Extends: TEzTimeSpan;

begin
  Int := TEzScheduleInterval.Create;
  try
    Int.Assign(Task.Intervals[0]);
    Int.Duration := Task.Slack;
    Int.IsSlackInterval := True;

    // Update schedule window
    Extends := GetExtendsFromIntervals(Task.Intervals);
    if Task.ScheduleData.Direction=sdForwards then
      Task.ScheduleData.Window.Start := Extends.Stop else
      Task.ScheduleData.Window.Stop := Extends.Start;

    // If no resources assigned or when indicated by the user,
    // schedule task against projects calendar
    if (Task.Resources.Count=0) or (int.ScheduleFlag = ifProjectCalendar) then
      ScheduleToProjectCalendar(Task.Key.Value, Task, Int, TaskWindow) else
      ScheduleToResourceProfile(Task.Key.Value, Task, Int, False, TaskWindow);

    // Slack window runs from star/stop of task to scheduled end/start
    TaskWindow.Start := min(Extends.Start, TaskWindow.Start);
    TaskWindow.Stop := max(Extends.Stop, TaskWindow.Stop);

    if not (srSlackScheduleFailed in Task.Flags) and
      (
           (DatetimeToEzDatetime(Task.SlackWindowStart)<>TaskWindow.Start) or
           (DatetimeToEzDatetime(Task.SlackWindowStop)<>TaskWindow.Stop)
      )
    then
    begin
      Task.SlackWindowStart := EzDatetimeToDatetime(TaskWindow.Start);
      Task.SlackWindowStop := EzDatetimeToDatetime(TaskWindow.Stop);
      Include(Task.Flags, srSlackWindowChanged);
    end;
  finally
    Int.Free;
  end;
end;

procedure TEzScheduler.CheckScheduleResult(Task: TEzScheduleTask; Window: TEzTimeSpan);
begin

end;

procedure TEzScheduler.FillSuccessors;
var
  i, p: Integer;
  Task, Predecessor: TEzScheduleTask;
  Node: TRecordNode;
  PredecessorData: TEzPredecessor;
  Successor: TEzScheduleSuccessor;


begin
  for i:=0 to FScheduleTasks.Count-1 do
  begin
    Task := FScheduleTasks[i];
    for p:=0 to Task.Predecessors.Count-1 do
    begin
      PredecessorData := Task.Predecessors[p];
      Node := FTaskCursor.FindNode(PredecessorData.Key);
      if Node = FTaskCursor.EndNode then
      begin
        AddScheduleMessage(Task, Format(SPredecessorTaskNotFound, [SafeString(PredecessorData.Key.Value)]));
        continue;
      end;
      Predecessor := Node.Data;
      Successor := TEzScheduleSuccessor.Create;
      Successor.Task := Task;
      Successor.Relation := PredecessorData.Relation;
      Successor.Lag := PredecessorData.Lag;
      Predecessor.ScheduleData.Successors.Add(Successor);
    end;
  end;
end;

function TEzScheduler.GetNextScheduledTask(var Task: TEzScheduleTask): Boolean;
begin
  Result := False;
  if FTaskCursor.IsEmpty then Exit;

  if (Task = nil) then
    FTaskCursor.MoveFirst else
    FTaskCursor.FindNode(Task.Key);

  if FTaskCursor.Eof then
    EzScheduleError(STaskNotFound, Self);

  repeat
    if Task <> nil then
    begin
      FTaskCursor.MoveNext;
      if FTaskCursor.Eof then Exit;
    end;
    Task := FTaskCursor.CurrentNode.Data;
  until ((Task.Flags*[srIntervaldataChanged, srSlackWindowChanged])<>[]);

  Result := True;
end;

function TEzScheduler.GetProjectProfile: TEzAvailabilityProfile;
begin
  if FProjectProfile=nil then
    EzScheduleError(SProjectAvailabilityNotSet, Self);
  Result := FProjectProfile;
end;

function TEzScheduler.GetTaskWindow(Task: TEzScheduleTask) : TEzTimeSpan;
var
  i: Integer;

begin
  if Task.WindowStart<>0 then
  begin
    Result.Start := DateTimeToEzDatetime(Task.WindowStart);
    Result.Stop := DateTimeToEzDatetime(Task.WindowStop);
  end

  else
  begin
    Result.Start := END_OF_TIME;
    Result.Stop := 0;

    for i:=0 to Task.Intervals.Count-1 do
    begin
      Result.Start := min(Result.Start, DatetimeToEzDatetime(Task.Intervals[i].Start));
      Result.Stop := max(Result.Stop, DatetimeToEzDatetime(Task.Intervals[i].Stop));
    end;
  end;
end;

procedure TEzScheduler.UpdateTaskScheduleDirection(Task: TEzScheduleTask);

  procedure InvestigatePredecessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Preds: TEzPredecessors;
    Pred: TEzScheduleTask;
    i_pred: Integer;

  begin
    Preds := CurrentTask.Predecessors;
    i_pred := Preds.Count-1;

    while i_pred>=0 do
    begin
      Pred := FTaskCursor.FindNode(Preds[i_pred].Key).Data;

      //
      // Check for recursive predecessor relations
      //
      if FTrackPath.RelationExists(Pred, CurrentTask) then
      begin
        dec(i_pred);
        continue;
        // EzScheduleError(Format(SCircularRelation, [SafeString(Pred.Key.Value), SafeString(CurrentTask.Key.Value)]));
      end;
      FTrackPath.AddRelation(Pred, CurrentTask);

      if IsFixedPoint(Pred, sdBackwards) then
      begin
        Task.ScheduleData.Direction := sdForwards;
        Exit;
      end;

      InvestigatePredecessors(Pred, True);

      dec(i_pred);
    end;
  end;

  //
  // Investigate successors
  //
  procedure InvestigateSuccessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Succs: TEzScheduleSuccessors;
    Successor: TEzScheduleSuccessor;
    i_succ: Integer;

  begin
    i_succ := CurrentTask.ScheduleData.Successors.Count-1;
    Succs := CurrentTask.ScheduleData.Successors;

    while i_succ>=0 do
    begin
      Successor := Succs[i_succ];

      if IsFixedPoint(Successor.Task, sdForwards) then
      begin
        Task.ScheduleData.Direction := sdBackwards;
        Exit;
      end;

      InvestigateSuccessors(Successor.Task, True);

      dec(i_succ);
    end;
  end;

begin
  if Task.ScheduleData.Direction in [sdForwards, sdBackwards] then Exit;
  FTrackPath.Clear;
  InvestigatePredecessors(Task, False);
  if Task.ScheduleData.Direction in [sdForwards, sdBackwards] then Exit;
  InvestigateSuccessors(Task, False);
  if not (Task.ScheduleData.Direction in [sdForwards, sdBackwards]) then
    Task.ScheduleData.Direction := sdForwards;
end;

function TEzScheduler.GetTaskScheduleData(Task: TEzScheduleTask; ScheduleWithPriority: Boolean) : Boolean;
var
  HasUnscheduledSuccessors,
  HasUnscheduledPredecessors, HasScheduledPredecessors: Boolean;
  HasSuccessorFixedPoint: Boolean;
  TaskIsFixedPoint: Boolean;
  i: Integer;


  procedure InvestigatePredecessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Preds: TEzPredecessors;
    Pred: TEzScheduleTask;
    i_pred: Integer;

  begin
    Preds := CurrentTask.Predecessors;
    i_pred := Preds.Count-1;

    while i_pred>=0 do
    begin
      Pred := FTaskCursor.FindNode(Preds[i_pred].Key).Data;

      //
      // Check for recursive predecessor relations
      //
      if FTrackPath.RelationExists(Pred, CurrentTask) then
        EzScheduleError(Format(SCircularRelation, [SafeString(Pred.Key.Value), SafeString(CurrentTask.Key.Value)]));
      FTrackPath.AddRelation(Pred, CurrentTask);

      // Handle taskwindow of predecessor
      if Pred.Slack>0 then
        updateWindowWithPredecessorSlackWindow(Pred, Preds[i_pred], Task) else
        updateWindowWithPredecessorTaskWindow(Pred, Preds[i_pred], Task);

      if not (srScheduledFailed in Pred.Flags) then
      begin
        if srScheduledOK in Pred.Flags then
        begin
          //
          // If there is a scheduled predecessor, then there's no need
          // to investigate successors of task. The task can simply
          // be scheduled against it's predecessor(s).
          //
          HasScheduledPredecessors := True;

          //
          // no need to update schedule window when there still are
          // unscheduled predecessors (these need scheduling first anyway)
          // or when this procedure is recursively called
          //
          if not HasUnscheduledPredecessors and not IsNested then
            updateWindowWithPredecessor(Pred, Preds[i_pred], Task);
        end

        else if not TaskIsFixedPoint then
          //
          // An unscheduled predecessor exists and this needs scheduling first
          //
        begin
          HasUnscheduledPredecessors := true;

          if FPredecessorStack.IndexOf(Pred) = -1 {Prevent double checking of the same predecessor} then
          begin
            //
            //  push predecessor into queue so it will be scheduled before current task.
            //
            FPredecessorStack.Push(Pred);

            //
            // Load predecessors of predecessor
            //
            InvestigatePredecessors(Pred, True);
          end;
        end;
      end; // if not (srScheduledFailed in Pred.Flags) then

      dec(i_pred);
    end;
  end;

  //
  // Investigate successors
  //
  procedure InvestigateSuccessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Succs: TEzScheduleSuccessors;
    Successor: TEzScheduleSuccessor;
    i_succ: Integer;

  begin
    i_succ := CurrentTask.ScheduleData.Successors.Count-1;
    Succs := CurrentTask.ScheduleData.Successors;

    while i_succ>=0 do
    begin
      Successor := Succs[i_succ];

      // Handle taskwindow of successor
      if Successor.Task.Slack>0 then
        updateWindowWithSuccessorSlackWindow(Successor, CurrentTask) else
        updateWindowWithSuccessorTaskWindow(Successor, CurrentTask);

      if not (srScheduledFailed in Successor.Task.Flags) then
      begin
        if srScheduledOK in Successor.Task.Flags then
        begin
          //
          // no need to update schedule window when there still are
          // unscheduled successors (these need scheduling first anyway)
          // or when this procedure is recursively called
          //
          // KV: 11 feb 2008
          // Check for HasUnscheduledSuccessors removed
          if {not HasUnscheduledSuccessors and} not IsNested then
            updateWindowWithSuccessor(Successor, CurrentTask);

          if Successor.Task.ScheduleData.Direction = sdBackwards then
          begin
            CurrentTask.ScheduleData.Direction := sdBackwards;
            HasSuccessorFixedPoint := True;
          end;
        end
        else if not TaskIsFixedPoint

        // KV: 12 June 2008
        // Check removed. We allways look at the successors regardless
        // of any scheduled predecessor.
        // This solves the problem if in a group of tasks one predecessor
        // has a MSO constraint and a successor has a FAB constraint.
        {and not HasScheduledPredecessors}


        then
          //
          // if a task is a fixed point itself, or when scheduled predecessors
          // exist, then we don't care about unscheduled successors. The task can
          // simply be scheduled against it's predecessor(s) or the fixed point.
          //
          // Unscheduled successor exists and need handling first.
          //
        begin
          HasUnscheduledSuccessors := True;

          if Successor.Task.Constraint in [cnMFO, cnFAB] then
          begin
            HasSuccessorFixedPoint := True;
            // KV: 23 may 2008
            // Next line added:
            // If current chain ends with a fixed successors, we
            // keep track of all successors in between.
            // These task need scheduling before current task.
            FSuccessorStack.Push(Successor.Task);
          end

          else if FSuccessorStack.IndexOf(Successor.Task) = -1 {Prevent double checking of successor} then
          begin
            InvestigateSuccessors(Successor.Task, True);

            // KV: 11 feb 2008
            // If current chain ends with a fixed successors, we
            // keep track of all successors in between.
            // These task need scheduling before current task.
            if HasSuccessorFixedPoint then
              FSuccessorStack.Push(Successor.Task);

            if Successor.Task.ScheduleData.Direction = sdBackwards then
              CurrentTask.ScheduleData.Direction := sdBackwards;
          end;
        end;
      end;
      dec(i_succ);
    end;
  end;

begin
  Result := True;
	HasUnscheduledPredecessors := false;
	HasUnscheduledSuccessors := false;
  HasSuccessorFixedPoint := False;
  HasScheduledPredecessors := False;

  FTrackPath.Clear;
  FSuccessorStack.Clear;
  FPredecessorStack.Clear;
  TaskIsFixedPoint := IsFixedPoint(Task, sdForwards);
  InvestigatePredecessors(Task, False);
//  if not HasScheduledPredecessors then
    //
    // If a task has scheduled predecessors then there is no need to
    // investigate the successors since this task can simply be scheduled
    // against it's predecessors.
    //
  begin
    TaskIsFixedPoint := IsFixedPoint(Task, sdBackwards);
    InvestigateSuccessors(Task, False);
  end;

  if HasSuccessorFixedPoint then
    //
    // There is a fixed point successor ==> we need to schedule successor first
    // and then schedule this task backwards against these successors.
    //
  begin
    Task.ScheduleData.Direction := sdBackwards;

    if FSuccessorStack.Count>0 then
    begin
      FPriorityStack.Add(Task);
      for i:=FSuccessorStack.Count-1 downto 0 do
        FPriorityStack.Add(FSuccessorStack[i]);

      // Cannot continue scheduling, schedule tasks from priority queue
      // first
      Result := False;
    end;
  end

  else if HasUnscheduledPredecessors then
  begin
    if FPredecessorStack.Count>0 then
    begin
      FPriorityStack.Add(Task);
      for i:=0 to FPredecessorStack.Count-1 do
        FPriorityStack.Add(FPredecessorStack[i]);
      // Cannot continue scheduling, schedule tasks from priority queue
      // first
      Result := False;
    end;
  end;
end;

{
function TEzScheduler.GetTaskScheduleData(Task: TEzScheduleTask; ScheduleWithPriority: Boolean) : Boolean;
var
  PredecessorsRequired, hasUnscheduledPredecessor: Boolean;
	i_succ, i_pred, Stop: Integer;
  Succs: TEzScheduleSuccessors;
  Preds: TEzPredecessors;
  Pred: TEzScheduleTask;
  Successor: TEzScheduleSuccessor;
  TrackPath: TEzPredecessorSuccessors;
  SuccessorTask: TEzScheduleTask;
  TaskIsPushed: Boolean;

begin
  Result := False;
  TaskIsPushed := False;

  // Controls whether predecessors can be fixed points to schedule against
  PredecessorsRequired := true;
	hasUnscheduledPredecessor := false;

  TrackPath := TEzPredecessorSuccessors.Create(True);
  try
    //
    // Investigate successors
    //
    i_succ := 0;
    Succs := Task.ScheduleData.Successors;
    Stop := Succs.Count;

    while not TaskIsPushed and (i_succ < Stop) do
    begin
      Successor := Succs[i_succ];
      if srScheduledOK in Successor.Task.Flags then
      begin
        // Successors act as fixed point.
        // predecessors may therefore be ignored.
        PredecessorsRequired := false;
        updateWindowWithSuccessor(Successor, Task);
      end

    	//
  		//	if this task, or one of it's successors, is a fixed point then push this task
	 		//  as a unscheduled successor.
  		//
	 		else if not ScheduleWithPriority and
             (IsFixedPoint(Successor.Task, sdBackwards) or locateFixedPoint(Successor.Task)) then
      begin
        // By adding successor to the route queue before this task, we ensure
        // that the successor will be scheduled first.
        if not TaskIsPushed then
        begin
          TaskIsPushed := True;
          FPriorityStack.Push(Task);
        end;
  			FPriorityStack.Push(Successor.Task);
      end;
      inc(i_succ);
    end;

    // If any successor was added, schedule them first before continueing
  	if TaskIsPushed then Exit;

	  //
    //	update schedulewindow if current task is a fixed point itself.
    //
  	if IsFixedPoint(Task, sdForwards) then
    begin
      // This task is a fixed point itself.
      // predecessors may therefore be ignored.
	    PredecessorsRequired := false;
  		updateWindowWithTask(Task.ScheduleData, Task);
    end;

    //
    // Investigate predecessors
    //
    Preds := Task.Predecessors;
    SuccessorTask := Task;
    i_pred := 0;
    Stop := Preds.Count;

    while i_pred<Stop do
    begin
      Pred := FTaskCursor.FindNode(Preds[i_pred].Key).Data;

      if TrackPath.RelationExists(Pred, SuccessorTask) then
        EzScheduleError(Format(SCircularRelation, [string(Pred.Key.Value), string(SuccessorTask.Key.Value)]));
      TrackPath.AddRelation(pred, SuccessorTask);

      if srScheduledFailed in Pred.Flags then
        inc(i_pred)

      else if srScheduledOK in Pred.Flags then
      begin
        if not hasUnscheduledPredecessor then //no need to update window when there are unscheduled predecessors
          updateWindowWithPredecessor(Pred, Preds[i_pred], Task);
        inc(i_pred);
      end
      else if PredecessorsRequired then
        //
        // Predecessor needs scheduling first
        //
      begin
        hasUnscheduledPredecessor := true;

				//
				//  push current task into queue to ensure rescheduling after predecessors.
				//
        if not TaskIsPushed then
        begin
          TaskIsPushed := True;
          FPriorityStack.Push(Task);
        end;

				//
				//  push predecessor into queue so it will be scheduled before current task.
				//
				FPriorityStack.Push(Pred);

				//
				//  go to predecessor of predecessor
				//
        SuccessorTask := Pred;
        Preds := Pred.Predecessors;
        i_pred := 0;
        Stop := Preds.Count;
      end else
        inc(i_pred);
    end;

  	if hasUnscheduledPredecessor and PredecessorsRequired then
      Result := False else
	    Result := True;
  finally
    TrackPath.Destroy;
  end;
end;
}

function TEzScheduler.IsFixedPoint(Task: TEzScheduleTask; Direction: TEzScheduleDirection) : Boolean;
begin
  case direction of
    sdForwards:
	    Result := Task.Constraint in [cnDNL, cnMSO, cnMFO, cnFAB];
    sdBackwards:
      Result := Task.Constraint in [cnDNL, cnMSO, cnMFO, cnSNET];
  else
    Result := False;
  end;

end;

procedure TEzScheduler.AllocateResources( Task: TEzScheduleTask;
                                          Intervals: TEzTimespanArray;
                                          ResourcePath: TEzResourcePath);
var
  CapacityKey: TEzCapacityKey;
  LocalWorkInterval: TEzWorkInterval;
  WorkIntervals: TEzWorkIntervals;
  MessageAdded: Boolean;

  procedure AllocateInterval(
      Assignment: TEzResourceAssignment;
      Profile: TEzAvailabilityProfile;
      Resource: TEzResource;
      Interval: TEzTimeSpan);
  var
    OverAllocated: Boolean;

  begin
    if (Interval.Start<Interval.Stop) then
    begin
      if Task.ResourceAllocation=alProfile then
      begin
        // When scheduling to a resource profile,
        // all availability is consumed.
        Profile.SetInterval(Interval.Start, Interval.Stop, 0, [afIsAllocated])
      end
      else
      begin
        OverAllocated := Profile.AllocateInterval(Interval.Start, Interval.Stop, Assignment.Assigned, True);
        if OverAllocated and not MessageAdded then
        begin
          MessageAdded := True;
          AddScheduleMessage(Task, Format(SResourceOverallocated, [Resource.Name]));
        end;
      end;
    end;
  end;

  procedure AllocateWorkInterval(WorkInterval: TEzWorkInterval; ResourceAssignment: TEzResourceAssignment);
  var
    Profile: TEzAvailabilityProfile;
    Resource: TEzResource;

  begin
    // ignore resource when assignement equals 0
    if (Task.ResourceAllocation=alFixed) and (ResourceAssignment.Assigned<=0.0) then
      Exit;

    Resource := FResources.ResourceByKey(ResourceAssignment.Key);
    Profile := Resource.ActualAvailability;

    // Because of the MSO constraint, the task has been scheduled against
    // the availability profile of the resource instead of against the
    // actual availability profile. Therefore the actual availability profile
    // has not been prepared.
    if (Task.Constraint in [cnMSO, cnMFO]) then
      Profile.PrepareDateRange(WorkInterval.TimeSpan);

    AllocateInterval(ResourceAssignment, Profile, Resource, WorkInterval.TimeSpan);
  end;

  function SelectBestResource(ResourceIndex: Integer; WorkInterval: TEzWorkInterval): Boolean;
  var
    av_point, av_next: TEzAvailabilityPoint;
    i_point: Integer;
    bestIndex: Integer;
    Resource: TEzResource;
    ResourceAssignment: TEzResourceAssignment;
    Stop: TEzDateTime;
    Profile: TEzAvailabilityProfile;

  begin
    // Result indicates whether interval has been split
    Result := False;
    Stop := WorkInterval.TimeSpan.Start;
    bestIndex := -1;

    repeat
      ResourceAssignment := ResourcePath.Resources[ResourceIndex];
      Resource := FResources.ResourceByKey(ResourceAssignment.Key);

      // Because of the MSO constraint, the task has been scheduled against
      // the availability profile of the resource instead of against the
      // actual availability profile. Therefore the actual availability profile
      // has not been prepared.
      if (Task.Constraint in [cnMSO, cnMFO]) then
        Profile := Resource.Availability else
        Profile := Resource.ActualAvailability;

      if not Profile.IndexOf(i_point, WorkInterval.TimeSpan.Start) then
        dec(i_point);

      av_point := Profile[i_point];
      if (av_point.Units >= ResourceAssignment.Assigned) then
      begin
        if i_point < Profile.Count - 1 then
          av_next := Profile[i_point + 1] else
          av_next := nil;

        if (av_next = nil) or (av_next.DateValue >= WorkInterval.TimeSpan.Stop) then
        begin
          if ReturnWorkIntervals then
            // This resource can work the whole interval.
            // No need to look any further.
            WorkInterval.ResourceAssigments.Add(TEzResourceAssignment.Create(ResourceAssignment));

          AllocateWorkInterval(WorkInterval, ResourceAssignment);

          Exit;
        end;

        if av_next.DateValue > Stop then
        begin
          bestIndex := ResourceIndex;
          Stop := av_next.DateValue;
        end;
      end;

      inc(ResourceIndex);
    until (ResourceIndex = ResourcePath.Resources.Count) or
          (CapacityKey <> ResourcePath.Resources[ResourceIndex].CapacityKey);

    Assert(
        (bestIndex >= 0) and
        (bestIndex < ResourcePath.Resources.Count) and
        (WorkInterval.TimeSpan.Start<Stop));

    // Interval has been split
    Result := True;
    ResourceAssignment := ResourcePath.Resources[bestIndex];

    if ReturnWorkIntervals then
      WorkInterval.ResourceAssigments.Add(TEzResourceAssignment.Create(ResourceAssignment));

    WorkInterval.TimeSpan.Stop := Stop;
    AllocateWorkInterval(WorkInterval, ResourceAssignment);
  end;

  function SetupWorkInterval(ts: TEzTimeSpan; AllocResources: Boolean): TEzWorkInterval;
  begin
    if ReturnWorkIntervals then
    begin
      Result := TEzWorkInterval.Create;
      Result.TimeSpan := ts;
      if AllocResources then
        Result.ResourceAssigments := TEzResourceAssignments.Create(True);
      WorkIntervals.Add(Result);
    end
    else
    begin
      if LocalWorkInterval=nil then
      begin
        LocalWorkInterval := TEzWorkInterval.Create;
        if AllocResources then
          LocalWorkInterval.ResourceAssigments := TEzResourceAssignments.Create(True);
      end;

      Result := LocalWorkInterval;
      Result.TimeSpan := ts;
    end;
  end;

  procedure BuildWorkIntervalList;
  var
    i_int, i_res: Integer;
    IntervalHasBeenSplit: Boolean;
    Stop: TEzDateTime;
    ResourceAssignment: TEzResourceAssignment;
    WorkInterval: TEzWorkInterval;

  begin
    i_int := 0;
    while i_int < Intervals.Count do
    begin
      WorkInterval := SetupWorkInterval(Intervals[i_int], True);

      i_res := 0;
      while i_res < ResourcePath.Resources.Count do
      begin
        ResourceAssignment := ResourcePath.Resources[i_res];
        CapacityKey := ResourceAssignment.CapacityKey;

        // Merged profile?
        if (i_res < ResourcePath.Resources.Count - 1) and
           (CapacityKey = ResourcePath.Resources[i_res + 1].CapacityKey)
        then
        begin
          IntervalHasBeenSplit := SelectBestResource(i_res, WorkInterval);
          if IntervalHasBeenSplit then
          begin
            Stop := WorkInterval.TimeSpan.Stop;
            WorkInterval := SetupWorkInterval(Intervals[i_int], True);
            WorkInterval.TimeSpan.Start := Stop;
          end else
            // Skip other resources supporting the same capacity
            while (i_res < ResourcePath.Resources.Count) and (CapacityKey = ResourcePath.Resources[i_res].CapacityKey) do
              inc(i_res);
        end
        else
        begin
          if ReturnWorkIntervals then
            WorkInterval.ResourceAssigments.Add(TEzResourceAssignment.Create(ResourceAssignment));

          AllocateWorkInterval(WorkInterval, ResourceAssignment);
          inc(i_res);
        end;
      end;

      inc(i_int);
    end;
  end;

  procedure AllocateIntervals;
  var
    i_int, i_res: Integer;
    Interval: TEzTimespan;
    Profile: TEzAvailabilityProfile;
    Resource: TEzResource;
    ResourceAssignment: TEzResourceAssignment;

  begin
      for i_res:=0 to ResourcePath.Resources.Count-1 do
      begin
        ResourceAssignment := ResourcePath.Resources[i_res];

        // ignore resource when assignement equals 0
        if (Task.ResourceAllocation=alFixed) and (ResourceAssignment.Assigned<=0.0) then
          break;

        Resource := FResources.ResourceByKey(ResourceAssignment.Key);
        Profile := Resource.ActualAvailability;

        for i_int:=0 to Intervals.Count-1 do
        begin
          Interval := Intervals[i_int];
          if ReturnWorkIntervals then
            SetupWorkInterval(Intervals[i_int], False);
          AllocateInterval(ResourceAssignment, Profile, Resource, Interval);
        end;
      end;
  end;

var
  i_ass: Integer;

begin
  MessageAdded := False;
  LocalWorkInterval := nil;

  if ReturnWorkIntervals then
  begin
    WorkIntervals := TEzWorkIntervals.Create(True);
    Task.WorkIntervals := WorkIntervals;
  end;

  try
    if srTaskHasMergedProfiles in Task.Flags then
      BuildWorkIntervalList
    else
    begin
      AllocateIntervals;
      if ReturnWorkIntervals then
      begin
        WorkIntervals[0].ResourceAssigments := TEzResourceAssignments.Create(True);
        for i_ass := 0 to ResourcePath.Resources.Count - 1 do
          WorkIntervals[0].ResourceAssigments.Add(TEzResourceAssignment.Create(ResourcePath.Resources[i_ass]));
      end;
    end;
  finally
    LocalWorkInterval.Free;
  end;
end;

procedure TEzScheduler.CopyParentsResources(Task, Parent: TEzScheduleTask);
var
  i, Current: Integer;
  Res: TEzResourceAssignment;

  function CheckHasResource(Resource: TEzTaskAssignment): Boolean;
  var
    n: Integer;

  begin
    Result := False;
    for n:=0 to Current-1 do
      if (Task.Resources[n].Key = Resource.Key) and (Resource is TEzResourceAssignment) then
      begin
        Result := True;
        LoadResourceData(Resource as TEzResourceAssignment);
        AddScheduleMessage(Task, Format(SDuplicateResources, [Resources.ResourceByKey(Resource.Key).Name]));
      end;
  end;

begin
  if Parent.Resources.Count = 0 then Exit;

  Current := Task.Resources.Count;

  for i:=0 to Parent.Resources.Count-1 do
  begin
    if not CheckHasResource(Parent.Resources[i]) then
    begin
      Res := TEzResourceAssignment.Create;
      Res.Assign(Parent.Resources[i]);
      Task.Resources.Add(Res);
    end;
  end;
end;

procedure TEzScheduler.CopyParentPredecessors(Task, Parent: TEzScheduleTask);
var
  i: Integer;
  p: TEzPredecessor;

begin
  if Parent.Predecessors.Count = 0 then Exit;

  for i:=0 to Parent.Predecessors.Count-1 do
  begin
    if Task.Predecessors.IndexOf(Parent.Predecessors[i].Key) = -1 then
    begin
      p := TEzPredecessor.Create;
      p.Assign(Parent.Predecessors[i]);
      Task.Predecessors.Add(p);
    end;
  end;
end;

procedure TEzScheduler.DoLoadResourceData(Resource: TEzResource);
begin
  if Assigned(FOnLoadResourceData) then
    FOnLoadResourceData(Self, Resource);
  if not Assigned(Resource.Availability) then
    EzScheduleError(Format(SCouldNotLoadAvailability, [SafeString(Resource.Key)]));
end;

function TEzScheduler.DoTestResourceCapacity(
  Task: TEzScheduleTask;
  Capacity: TEzCapacityAssignment;
  Resource: TEzResource): Boolean;
begin
  Result := True;
  if Assigned(FOnTestResourceCapacity) then
    FOnTestResourceCapacity(Task, Capacity, Resource, Result);
end;

procedure TEzScheduler.LoadResourceData(Resource: TEzResourceAssignment);
var
  res: TEzResource;

begin
  if not FResources.ResourceExists(Resource.Key) then
  begin
    res := TEzResource.Create(Self, Resource);
    FResources.Add(res);
  end;
end;

procedure TEzScheduler.NormalizePredecessors(Task: TEzScheduleTask);
  //
  // Convert predecessor relations to a parent task into predecessor relations
  // to all child tasks of that parent.
  //
var
  p, childs: Integer;
  PredecessorData: TEzPredecessor;
  PredecessorTask: TEzScheduleTask;
  TaskNode, SavedPos, Node: TRecordNode;
  NewPredecessor: TEzPredecessor;
  NewPredecessors: TEzPredecessors;

begin
  if Task.Predecessors.Count=0 then Exit;

  SavedPos := FTaskCursor.CurrentNode;
  NewPredecessors := TEzPredecessors.Create(true {will be reset later!!!});
  try
    p:=0;
    while p<Task.Predecessors.Count do
    begin
      PredecessorData := Task.Predecessors[p];
      Node := FTaskCursor.FindNode(PredecessorData.Key);

      if Node = FTaskCursor.EndNode then
      begin
        AddScheduleMessage(Task, Format(SPredecessorTaskNotFound, [SafeString(PredecessorData.Key.Value)]));
        Task.Predecessors.Delete(p);
//        inc(p);
        continue;
      end;

      TaskNode := FTaskCursor.FindNode(Task.Key);
      if FTaskCursor.NodeHasParent(TaskNode, Node) then
      begin
        AddScheduleMessage(Task, Format(SPredecessorRelationInvalid, [SafeString(PredecessorData.Key.Value), SafeString(Task.Key.Value)]));
        Task.Predecessors.Delete(p);
        continue;
      end;

      if FTaskCursor.HasChildren(Node) then
        //
        // Predecessor is a parent ==> convert parent links into child links
        //
      begin
        FTaskCursor.CurrentNode := Node.Child;
        for childs:=1 to FTaskCursor.Childs(Node) do
        begin
          if not FTaskCursor.HasChildren then
          begin
            PredecessorTask := FTaskCursor.CurrentNode.Data;
            NewPredecessor := TEzPredecessor.Create;
            NewPredecessor.Key := PredecessorTask.Key;
            NewPredecessor.Relation := PredecessorData.Relation;
            NewPredecessor.Lag := PredecessorData.Lag;
            NewPredecessors.Add(NewPredecessor);
          end;
          FTaskCursor.MoveNext;
        end;

        // Remove predecessor relation to parent record.
        Task.Predecessors.Delete(p);
      end else
        inc(p);
    end;

    //
    // insert new relations to predecessor list of task
    //
    NewPredecessors.OwnsObjects := False;
    p:=0;
    while p<NewPredecessors.Count do
    begin
      if Task.Predecessors.IndexOf(NewPredecessors[p].Key) = -1 then
        Task.Predecessors.Add(NewPredecessors[p]);
      inc(p);
    end;
  finally
    FTaskCursor.CurrentNode := SavedPos;
    NewPredecessors.Destroy;
  end;
end;

procedure TEzScheduler.ProcessResources(Task: TEzScheduleTask);
var
  i: Integer;

begin
  Task.ScheduleData.MaxAssignement := 0;
  for i:=0 to Task.Resources.Count-1 do
  begin
    if (Task.Resources[i] is TEzResourceAssignment) then
      LoadResourceData(Task.Resources[i] as TEzResourceAssignment);
    Task.ScheduleData.MaxAssignement := max(Task.ScheduleData.MaxAssignement, Task.Resources[i].Assigned);
  end;
end;

procedure TEzScheduler.Reset(ResetAll: Boolean = True);
var
  i: Integer;
  Task: TEzScheduleTask;

begin
  FScheduleResults := [];

  // empty task list (FScheduleTasks does not own objects and therefore memory must be freed manually)
  FScheduleTasks.Clear;
  FScheduleMessages.Clear;
  FProjectWindowStart := 0;
  FProjectWindowStop := 0;
  FProjectWindowAbsoluteStart := MaxDouble;
  FProjectWindowAbsoluteStop := 0;
  FProjectMakeSpanStart := 0;
  FProjectMakeSpanStop := 0;

  FCapacityResources.Clear;

  if ResetAll then
  begin
    // Free memory allocated for tasks
    FTaskCursor.MoveFirst;
    while not FTaskCursor.Eof do
    begin
      TEzScheduleTask(FTaskCursor.CurrentNode.Data).Free;
      FTaskCursor.MoveNext;
    end;

    // Empty hierarchical cursor
    FTaskCursor.Clear;
    FResources.Clear;
  end
  else
  begin
    FTaskCursor.MoveFirst;
    while not FTaskCursor.Eof do
    begin
      Task := TEzScheduleTask(FTaskCursor.CurrentNode.Data);
      if Task<>nil then
        Task.Reset;
      FTaskCursor.MoveNext;
    end;

    for i:=0 to FResources.Count-1 do
    begin
      if FResources[i].ActualAvailability<>nil then
      begin
        FResources[i].ActualAvailability.Free;
        FResources[i].ActualAvailability := nil;
      end;
    end;
  end;
end;

procedure TEzScheduler.Reschedule;
begin
  Run;
end;

procedure TEzScheduler.Run;
begin
  if (ProjectWindowStart = 0) or (ProjectWindowStart >= ProjectWindowStop) then
    EzScheduleError(SInvalidProjectDates, Self);

  FProjectWindowAbsoluteStart := min(FProjectWindowAbsoluteStart, FProjectWindowStart);
  FProjectWindowAbsoluteStop := max(FProjectWindowAbsoluteStop, FProjectWindowStop);

  BuildResourceCapacityList;
  BuildScheduleList;
  FillSuccessors;
  ScheduleTasks;

  if Messages.Count>0 then
    Include(FScheduleResults, srScheduledWithErrors);
end;

procedure TEzScheduler.ScheduleTasks;
var
  i: Integer;
  Task: TEzScheduleTask;

begin
  for i:=0 to FScheduleTasks.Count-1 do
  begin
    Task := FScheduleTasks[i];

    ScheduleTask(Task.Key, Task, False);

    // Empty priority queue before scheduling next task
    while FPriorityStack.Count>0 do
    begin
      Task := TEzScheduleTask(FPriorityStack.Pop);
      ScheduleTask(Task.Key, Task, True);
    end;
  end;

  Include(FScheduleResults, srScheduledOK);
end;

procedure TEzScheduler.ScheduleTask(TaskKey: TNodeKey; Task: TEzScheduleTask; ScheduleWithPriority: Boolean);
const
  HalfASecond = 1/24/60/60/2;

var
  TaskWindow: TEzTimeSpan;
  i_int: Integer;
  Int: TEzScheduleInterval;
  IntervaldataChanged: Boolean;

begin
  // Debugging code
  // This code is useless, however it prevents Delphi from removing
  // this parameter from code.
  if VarIsNull(TaskKey.Value) then;

  if [srScheduledOK, srScheduledFailed]*Task.Flags<>[] {Task has already been scheduled} then
    Exit;

  Task.ScheduleData.Window.Start := DatetimeToEzDatetime(ProjectWindowStart);
  Task.ScheduleData.LeftBoundaryType := btProjectStartDate;
  Task.ScheduleData.Window.Stop := DatetimeToEzDatetime(ProjectWindowStop);
  Task.ScheduleData.RightBoundaryType := btProjectEndDate;

	//
	//	no need for scheduling a task having a Ignore constraint
	//  no need to allocate rescources too
	//
	if (Task.Constraint=cnIgnore) or IsMileStone(Task) then
  begin
    if Task.Constraint in [cnMFO, cnFAB] then
      Task.ScheduleData.Direction := sdBackwards else
      Task.ScheduleData.Direction := sdForwards;
    Include(Task.Flags, srScheduledOK);
		Exit;
  end

  else if Task.Intervals.Count=0 then
    //
    // Task without intervals cannot be scheduled.
    //
  begin
    Include(Task.Flags, srScheduledFailed);
    AddScheduleMessage(Task, SNoIntervals);
    Exit;
  end

	//
	//	no need for scheduling a task having a DNL constraint
	//
	else if Task.Constraint = cnDNL then
  begin
		UpdateTaskScheduleDirection(Task);
    Assert(Task.ScheduleData.Direction in [sdForwards, sdBackwards]);
    TaskWindow := GetTaskWindow(Task);
    ScheduleTask_DoNotLevel(Task);
		CheckScheduleResult(Task, TaskWindow);
		Exit;
  end

	else if Task.Constraint <= cnMFO then
    UpdateWindowWithTask(Task.ScheduleData, Task)

  else
  begin
		if not GetTaskScheduleData(Task, ScheduleWithPriority) then
      //
      // Task cannot be scheduled because a predecessor or successor needs
      // scheduling first.
      //
     Exit;

		if(Task.ScheduleData.Direction in [sdUndefined, sdDontCare]) then
      {
        KV: 8 april 2005, this line replaced by next 3 lines
            Task.ScheduleData.Direction := sdForwards;
      }
      if Task.Constraint in [cnMFO, cnFAB] then
        Task.ScheduleData.Direction := sdBackwards else
        Task.ScheduleData.Direction := sdForwards;

    UpdateWindowWithTask(Task.ScheduleData, Task)
  end;

  {
  KV: 6 sept 2006:
    Skip check altogether
    Let scheduler try to schedule task, if this fails an error will still be
    reported.
  if not VerifyTaskWindow(Task) then
  begin
    include(Task.Flags, srScheduledFailed);
    Exit;
  end;
  }

  //
  // Schedule window has been determined, continue with scheduling this task.
  //

  try
    // Schedule each interval of task seperately
    i_int:=0;
    IntervaldataChanged := False;
    while not (srScheduledFailed in Task.Flags) and (i_int<Task.Intervals.Count) do
    begin
      int := Task.Intervals[i_int];

      // If no resources assigned or when indicated by the user,
      // schedule task against projects calendar
      if (Task.Resources.Count=0) or (int.ScheduleFlag = ifProjectCalendar) then
        ScheduleToProjectCalendar(Task.Key.Value, Task, Int, TaskWindow) else
        ScheduleToResourceProfile(Task.Key.Value, Task, Int, False, TaskWindow);

      if (not (srScheduledFailed in Task.Flags)) and
         (
           (DatetimeToEzDatetime(Int.Start)<>TaskWindow.Start) or
           (DatetimeToEzDatetime(Int.Stop)<>TaskWindow.Stop)
         )
      then
      begin
        Int.Start := EzDatetimeToDatetime(TaskWindow.Start);
        Int.Stop := EzDatetimeToDatetime(TaskWindow.Stop);
        IntervaldataChanged := True;
        if FProjectMakeSpanStart = 0 then
          FProjectMakeSpanStart := Int.Start else
          FProjectMakeSpanStart := min(FProjectMakeSpanStart, Int.Start);

        if FProjectMakeSpanStop = 0 then
          FProjectMakeSpanStop := Int.Stop else
          FProjectMakeSpanStop := max(FProjectMakeSpanStop, Int.Stop);
      end;

      if not (srScheduledFailed in Task.Flags) and (i_int<Task.Intervals.Count-1) then
        //
        // If another interval requires scheduling, then update schedule window
        // with results. This prevents multiple intervals from overlapping.
        //
      begin
        if Task.ScheduleData.Direction = sdForwards then
          Task.ScheduleData.Window.Start := TaskWindow.Stop else
          Task.ScheduleData.Window.Stop := TaskWindow.Start;
      end;

      // Next interval
      inc(i_int);
    end;

    // Reserve slack
    if not (srScheduledFailed in Task.Flags) and (Task.Slack>0) then
      CalculateSlackWindow(Task, TaskWindow);

    if IntervaldataChanged then
      include(Task.Flags, srIntervaldataChanged);

  finally
    if Task.ScheduleData.OwnsProfile then
      FreeAndNil(Task.ScheduleData.Profile);
  end;
end;

procedure TEzScheduler.ScheduleToProjectCalendar(Key: Variant; Task: TEzScheduleTask; Interval: TEzScheduleInterval; var ResultInterval: TEzTimeSpan);
var
  i_int: Integer;
  Profile: TEzAvailabilityProfile;
  Data: TEzCalendarScheduleData;
  Intervals: TEzTimeSpanArray;
  WorkInterval: TEzWorkInterval;

begin
  if VarIsNull(Key) then; // Prevent key from removing by optimiser

  Profile := GetProjectProfile;

  Data.Start := Task.ScheduleData.Window.Start;
  Data.Stop := Task.ScheduleData.Window.Stop;
  Data.AbsoluteStart := DatetimeToEzDatetime(FProjectWindowAbsoluteStart);
  Data.AbsoluteStop := DatetimeToEzDatetime(FProjectWindowAbsoluteStop);
  Data.Duration := DoubleToEzDuration(Interval.Duration);
  Data.UnitsRequired := 1; // Assigned = 1
  Data.Direction := Task.ScheduleData.Direction;
  Data.AllocationType := Task.ResourceAllocation;
  Data.SchedulebeyondScheduleWindow := FScheduleBeyondScheduleWindow;
  Data.FailingProfile := nil;
  Data.ScheduleResults := [];

  FScheduleIntervals.Clear;
  Intervals := FScheduleIntervals;

  Profile.ScheduleTask(Data, Intervals);
  if srScheduledOK in Data.ScheduleResults then
  begin
    if not Interval.IsSlackInterval then
      Task.Flags := Data.ScheduleResults;

    // Return result
    ResultInterval := GetExtendsFromIntervals(Intervals);

    if ReturnWorkIntervals then
    begin
      Task.WorkIntervals := TEzWorkIntervals.Create(True);
      for i_int := 0 to Intervals.Count - 1 do
      begin
        WorkInterval := TEzWorkInterval.Create;
        WorkInterval.TimeSpan := Intervals[i_int];
        Task.WorkIntervals.Add(WorkInterval);
      end;
    end;

    if srScheduledOutsideWindow in Task.Flags then
    begin
      if Interval.IsSlackInterval then
        AddScheduleMessage(Task, SSlackScheduledOutsideWindow) else
        AddScheduleMessage(Task, SScheduledOutsideWindow);
    end;
  end
  else
  begin
    if Interval.IsSlackInterval then
    begin
      Include(Task.Flags, srSlackScheduleFailed);
      AddScheduleMessage(Task, SSlackScheduleFailed);
    end
    else
    begin
      Task.Flags := Data.ScheduleResults;
      AddScheduleMessage(Task, STaskScheduleFailed);
    end;
  end;
end;

procedure TEzScheduler.ScheduleTask_DoNotLevel(Task: TEzScheduleTask);
var
  i_res, i_interval, i_point: Integer;
  Timespan: TEzTimespan;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource;
  OverAllocated, MessageAdded: Boolean;
  ResourceAssigments: TEzTaskAssignments;
  WorkInterval: TEzWorkInterval;

  Profile: TEzAvailabilityProfile;
  avpoint: TEzAvailabilityPoint;

begin
  if (Task.WorkIntervals = nil) or (Task.WorkIntervals.Count=0) then
    //
    // There is no workinterval data available.
    // Create a dummy resource path so that we can call AllocateResources
    // which will determine the workintervals for us.
    //
  begin
    Task.WorkIntervals := TEzWorkIntervals.Create(True);

    Profile := TEzAvailabilityProfile.Create;
    try
      Profile.Operator := opScheduleProfile;

      i_res := 0;
      while i_res < Task.Resources.Count do
      begin
        if Task.Resources[i_res] is TEzCapacityAssignment then
        begin
          AddScheduleMessage(Task, SCannotUseCapacityWithDNL);
          Exit;
        end;

        Assignment := Task.Resources[i_res] as TEzResourceAssignment;

        // Use the resource's original availability profile
        Resource := FResources.ResourceByKey(Assignment.Key);
        Profile.AddSource(Resource.Availability, False);
        inc(i_res);
      end;

      Timespan := GetTaskWindow(Task);
      Profile.PrepareDateRange(Timespan);

      i_point := Profile.DateToIndex(Timespan.Start);
      avPoint := Profile[i_point];
      repeat
        if avPoint.Units > 0.0 then
        begin
          WorkInterval := TEzWorkInterval.Create;
          WorkInterval.TimeSpan.Start := max(Timespan.Start, avPoint.DateValue);
          if i_point < Profile.Count - 1  then
          begin
            inc(i_point);
            avPoint := Profile[i_point];
            WorkInterval.TimeSpan.Stop := min(Timespan.Stop, avPoint.DateValue);
          end else
            WorkInterval.TimeSpan.Stop := Timespan.Stop;

          Task.WorkIntervals.Add(WorkInterval);
        end
        else
        begin
          inc(i_point);
          if i_point < Profile.Count then
            avPoint := Profile[i_point];
        end;
      until (i_point = Profile.Count) or (avPoint.DateValue >= Timespan.Stop);
    finally
      Profile.Free;
    end;
  end;

  //
  // Workintervals loaded now.
  //

  MessageAdded := False;

  for i_interval:=0 to Task.WorkIntervals.Count-1 do
  begin
    WorkInterval := Task.WorkIntervals[i_interval];

    if WorkInterval.ResourceAssigments <> nil then
      ResourceAssigments := WorkInterval.ResourceAssigments else
      ResourceAssigments := Task.Resources;

    Timespan := WorkInterval.TimeSpan;

    i_res:=0;
    while i_res < ResourceAssigments.Count do
    begin
      Assignment := (ResourceAssigments[i_res] as TEzResourceAssignment);
      Resource := FResources.ResourceByKey(Assignment.Key);

      Resource.ProfileRequired;
      OverAllocated := Resource.ActualAvailability.AllocateInterval(Timespan.Start, Timespan.Stop, Assignment.Assigned, True);

      if OverAllocated and not MessageAdded then
      begin
        MessageAdded := True;
        AddScheduleMessage(Task, Format(SResourceOverallocated, [Resource.Name]));
      end;

      inc(i_res);
    end;
  end;

  Include(Task.Flags, srScheduledOK);
end;

{$IFDEF XXX}
procedure TEzScheduler.CrossJoinProfiles(Task: TEzScheduleTask; Paths: TEzResourcePathList);
var
  BaseResources: TEzResourceAssignments;
  BaseCapacities: TEzCapacityAssignments;

  TaskAssignment: TEzTaskAssignment;
  ResourceAssignment: TEzResourceAssignment;

  i_ass: Integer;
  Resource: TEzResource;

  procedure AddResourceAssignment(
      Assignment: TEzResourceAssignment;
      AList: TEzResourceAssignments);
  var
    i_index: Integer;

  begin
    i_index := AList.IndexOf(Assignment);

    if i_index = -1 then
      // New resource
      AList.Add(TEzResourceAssignment.Create(Assignment))
    else
      // If resource is already added to this task,
      // update the assigned units to the maximum.
      AList[i_index].Assigned :=
        Max(Assignment.Assigned, AList[i_index].Assigned);
  end;

  procedure AddCapacityAssignment(
      Assignment: TEzCapacityAssignment;
      AList: TEzCapacityAssignments);
  var
    i_index: Integer;

  begin
    i_index := AList.IndexOf(Assignment);

    if i_index = -1 then
      // New Capacity
      AList.Add(TEzCapacityAssignment.Create(Assignment))
    else
      // If Capacity is already added to this task,
      // update the assigned units to the maximum.
      AList[i_index].Assigned :=
        Max(Assignment.Assigned, AList[i_index].Assigned);
  end;

  procedure AddResourceOperators(
    Resource: TEzResource;
    ResourceList: TEzResourceAssignments;
    CapacityList: TEzCapacityAssignments);
  var
    Operators: TEzTaskAssignments;
    i_opp: Integer;

  begin
    Operators := Resource.Operators;

    i_opp := 0;
    while i_opp < Operators.Count do
    begin
      if Operators[i_opp] is TEzResourceAssignment then
        AddResourceAssignment(Operators[i_opp] as TEzResourceAssignment, ResourceList)
      else
        AddCapacityAssignment(Operators[i_opp] as TEzCapacityAssignment, CapacityList);
      inc(i_opp);
    end;
  end;

  function CloneRPath(Path: TEzResourceAssignments): TEzResourceAssignments;
  var
    i: Integer;

  begin
    Result := TEzResourceAssignments.Create(True);
    for i := 0 to Path.Count - 1 do
      Result.Add(TEzResourceAssignment.Create(Path[i]));
  end;

  function CloneCPath(Path: TEzCapacityAssignments): TEzCapacityAssignments;
  var
    i: Integer;

  begin
    Result := TEzCapacityAssignments.Create(True);
    for i := 0 to Path.Count - 1 do
      Result.Add(TEzCapacityAssignment.Create(Path[i]));
  end;

  procedure CrossJoinCapacities(
      ListIndex: Integer;
      RPath: TEzResourceAssignments;
      CPath: TEzCapacityAssignments);
  var
    CapacityAssignment: TEzCapacityAssignment;
    CapacityResources: TEzResources;
    i_cap, i_res: Integer;
    ResourceAssignment: TEzResourceAssignment;
    ResourceWasAdded: Boolean;
    RClone: TEzResourceAssignments;
    CClone: TEzCapacityAssignments;

  begin
    CapacityAssignment := CPath[ListIndex];

    i_cap := FCapacityResources.IndexOf(CapacityAssignment.Key);
    if i_cap=-1 then
    begin
      AddScheduleMessage(Task, Format(SCapacityNotSupported, [SafeString(CapacityAssignment.Key)]));
      include(Task.Flags, srScheduledFailed);
      Exit;
    end;

    ResourceWasAdded := False;
    CapacityResources := FCapacityResources[i_cap].Resources;

    for i_res := 0 to CapacityResources.Count-1 do
    begin
      Resource := FCapacityResources[i_cap].Resources[i_res];

      // Add this resource as a possible resource to schedule for this task
      // However make sure this resource was not selected as a
      // fixed resource and not as a resource for another capacity.
      if (RPath.IndexOfKey(Resource.Key) = -1) then
      begin
        if not DoTestResourceCapacity(Task, CapacityAssignment, Resource) then
          continue;

        if not CapacityAssignment.MergeProfiles or (ResourceWasAdded = False)  then
          // if the ResourceWasAdded variable is still false then
          // it is the first resource being added for the current capacity
          //
          // Clone current path if we are not merging
          // or
          // when this is the first resource being added for the current capacity
          //
        begin
          RClone := CloneRPath(RPath);
          CClone := CloneCpath(CPath);
        end;

        ResourceWasAdded := True;

        ResourceAssignment := TEzResourceAssignment.Create;
        ResourceAssignment.Key := Resource.Key;
        ResourceAssignment.Assigned := CapacityAssignment.Assigned;
        ResourceAssignment.AffectsDuration := CapacityAssignment.AffectsDuration;
        ResourceAssignment.CapacityKey := CapacityAssignment.Key;

        // Add assignment to current path
        RClone.Add(ResourceAssignment);

        AddResourceOperators(Resource, RClone, CClone);

        if not CapacityAssignment.MergeProfiles then
        begin
          if ListIndex < CClone.Count - 1 then
            // Go down the chain, add resources for other capacities as well
            CrossJoinCapacities(ListIndex + 1, RClone, CClone) else
            Paths.Add(TEzResourcePath.Create(RClone, CClone));
        end;
      end;
    end;

    if not ResourceWasAdded then
    begin
      AddScheduleMessage(Task, Format(SCapacityNotSupported, [SafeString(CapacityAssignment.Key)]));
      include(Task.Flags, srScheduledFailed);
      Exit;
    end;

    if CapacityAssignment.MergeProfiles then
    begin
      if ListIndex < CClone.Count - 1 then
        // Go down the chain, add resources for other capacities as well
        CrossJoinCapacities(ListIndex + 1, RClone, CClone) else
        Paths.Add(TEzResourcePath.Create(RClone, CClone));
    end;
 end;

begin
  BaseResources := TEzResourceAssignments.Create(True);
  BaseCapacities := TEzCapacityAssignments.Create(True);

  i_ass := 0;
  while i_ass < Task.Resources.Count do
  begin
    TaskAssignment := Task.Resources[i_ass];

    if TaskAssignment is TEzResourceAssignment then
    begin
      ResourceAssignment := TaskAssignment as TEzResourceAssignment;
      AddResourceAssignment(ResourceAssignment, BaseResources);

      //
      // Add operators of resource to the list as well
      //
      Resource := FResources.ResourceByKey(ResourceAssignment.Key);
      AddResourceOperators(Resource, BaseResources, BaseCapacities);
    end
    else
    begin
      AddCapacityAssignment(TaskAssignment as TEzCapacityAssignment, BaseCapacities);
    end;

    inc(i_ass);
  end;

  // Does this task use any 'resource by capacity'?
  if BaseCapacities.Count > 0 then
  begin
    CrossJoinCapacities(0, BaseResources, BaseCapacities);
    BaseResources.Free;
    BaseCapacities.Free;
  end else
    Paths.Add(TEzResourcePath.Create(BaseResources, BaseCapacities));
end;
{$ENDIF}

procedure TEzScheduler.CrossJoinProfiles(Task: TEzScheduleTask; Paths: TEzResourcePathList);
var
  BaseResources: TEzResourceAssignments;
  BaseCapacities: TEzCapacityAssignments;

  TaskAssignment: TEzTaskAssignment;
  ResourceAssignment: TEzResourceAssignment;

  i_ass: Integer;
  Resource: TEzResource;

  procedure AddResourceAssignment(
      Assignment: TEzResourceAssignment;
      AList: TEzResourceAssignments);
  var
    i_index: Integer;

  begin
    i_index := AList.IndexOf(Assignment);

    if i_index = -1 then
      // New resource
      AList.Add(TEzResourceAssignment.Create(Assignment))
    else
      // If resource is already added to this task,
      // update the assigned units to the maximum.
      AList[i_index].Assigned :=
        Max(Assignment.Assigned, AList[i_index].Assigned);
  end;

  procedure AddCapacityAssignment(
      Assignment: TEzCapacityAssignment;
      AList: TEzCapacityAssignments);
  var
    i_index: Integer;

  begin
    i_index := AList.IndexOf(Assignment);

    if i_index = -1 then
      // New Capacity
      AList.Add(TEzCapacityAssignment.Create(Assignment))
    else
      // If Capacity is already added to this task,
      // update the assigned units to the maximum.
      AList[i_index].Assigned :=
        Max(Assignment.Assigned, AList[i_index].Assigned);
  end;

  procedure AddResourceOperators(
    Resource: TEzResource;
    ResourceList: TEzResourceAssignments;
    CapacityList: TEzCapacityAssignments);
  var
    Operators: TEzTaskAssignments;
    i_opp: Integer;

  begin
    Operators := Resource.Operators;

    i_opp := 0;
    while i_opp < Operators.Count do
    begin
      if Operators[i_opp] is TEzResourceAssignment then
        AddResourceAssignment(Operators[i_opp] as TEzResourceAssignment, ResourceList)
      else
        AddCapacityAssignment(Operators[i_opp] as TEzCapacityAssignment, CapacityList);
      inc(i_opp);
    end;
  end;

  procedure AppendGroupToPath(
    Group: TObjectList;
    ResourceList: TEzResourceAssignments;
    CapacityList: TEzCapacityAssignments);
  begin

  end;


  function CloneRPath(Path: TEzResourceAssignments): TEzResourceAssignments;
  var
    i: Integer;

  begin
    Result := TEzResourceAssignments.Create(True);
    for i := 0 to Path.Count - 1 do
      Result.Add(TEzResourceAssignment.Create(Path[i]));
  end;

  function CloneCPath(Path: TEzCapacityAssignments): TEzCapacityAssignments;
  var
    i: Integer;

  begin
    Result := TEzCapacityAssignments.Create(True);
    for i := 0 to Path.Count - 1 do
      Result.Add(TEzCapacityAssignment.Create(Path[i]));
  end;

  procedure Trail(  Source: TEzResources;
                    var Path: TObjectList;
                    Destination: TObjectList;
                    Index: Integer;
                    IgnoreList: TEzResourceAssignments);
  var
    I: Integer;

  begin
    if IgnoreList.IndexOfKey(Source[Index].Key) <> -1 then
      Exit;

    if Index >= Source.Count then
    begin
      Destination.Add(Path);
      Path := TObjectList.Create(False);
      Exit;
    end;

    Path.Add(Source[Index]);

    for I := Index + 1 to Source.Count do
      Trail(Source, Path, Destination, I, IgnoreList);
  end;

  //
  // Returns a list of TEzResources containing all possible combinations
  // of resources.
  //
  function CartasianProductResources(Resources: TEzResources; IgnoreList: TEzResourceAssignments): TObjectList;
  var
    I: Integer;
    Path: TObjectList;
  begin
    // Result owns resource lists
    Result := TObjectList.Create(True);

    // Trail does not own resources
    Path := TObjectList.Create(False);

    for I := 0 to Resources.Count do
      Trail(Resources, Path, Result, 0, IgnoreList);
  end;

  procedure CrossJoinCapacities(
      ListIndex: Integer;
      RPath: TEzResourceAssignments;
      CPath: TEzCapacityAssignments);
  var
    CapacityAssignment: TEzCapacityAssignment;
    CapacityResources: TEzResources;
    i_cap, i_res: Integer;
    ResourceAssignment: TEzResourceAssignment;
    ResourceWasAdded: Boolean;
    RClone: TEzResourceAssignments;
    CClone: TEzCapacityAssignments;
    i_group: Integer;
    ResourceGroups: TObjectList;

  begin
    CapacityAssignment := CPath[ListIndex];

    i_cap := FCapacityResources.IndexOf(CapacityAssignment.Key);
    if i_cap=-1 then
    begin
      AddScheduleMessage(Task, Format(SCapacityNotSupported, [SafeString(CapacityAssignment.Key)]));
      include(Task.Flags, srScheduledFailed);
      Exit;
    end;

    ResourceWasAdded := False;
    CapacityResources := FCapacityResources[i_cap].Resources;

    if CapacityAssignment.MergeProfiles then
      //
      // When merge profile is set, we do a cartasian product of all resources.
      // This returns a list of TEzResources (also a list) of all possible combinations.
      //
    begin
      ResourceGroups := CartasianProductResources(CapacityResources, RClone);

      for i_group := 0 to ResourceGroups.Count - 1 do
      begin
        if i_group > 0 then
        begin
          RClone := CloneRPath(RPath);
          CClone := CloneCpath(CPath);
        end;

        AppendGroupToPath(ResourceGroups[i_group] as TObjectList, RClone, CClone);
      end;
    end;



    for i_res := 0 to CapacityResources.Count-1 do
    begin
      Resource := FCapacityResources[i_cap].Resources[i_res];

      // Add this resource as a possible resource to schedule for this task
      // However make sure this resource was not selected as a
      // fixed resource and not as a resource for another capacity.
      if (RPath.IndexOfKey(Resource.Key) = -1) then
      begin
        if not DoTestResourceCapacity(Task, CapacityAssignment, Resource) then
          continue;

        if not CapacityAssignment.MergeProfiles or (ResourceWasAdded = False)  then
          // if the ResourceWasAdded variable is still false then
          // it is the first resource being added for the current capacity
          //
          // Clone current path if we are not merging
          // or
          // when this is the first resource being added for the current capacity
          //
        begin
          RClone := CloneRPath(RPath);
          CClone := CloneCpath(CPath);
        end;

        ResourceWasAdded := True;

        ResourceAssignment := TEzResourceAssignment.Create;
        ResourceAssignment.Key := Resource.Key;
        ResourceAssignment.Assigned := CapacityAssignment.Assigned;
        ResourceAssignment.AffectsDuration := CapacityAssignment.AffectsDuration;
        ResourceAssignment.CapacityKey := CapacityAssignment.Key;

        // Add assignment to current path
        RClone.Add(ResourceAssignment);

        AddResourceOperators(Resource, RClone, CClone);

        if not CapacityAssignment.MergeProfiles then
        begin
          if ListIndex < CClone.Count - 1 then
            // Go down the chain, add resources for other capacities as well
            CrossJoinCapacities(ListIndex + 1, RClone, CClone) else
            Paths.Add(TEzResourcePath.Create(RClone, CClone));
        end;
      end;
    end;

    if not ResourceWasAdded then
    begin
      AddScheduleMessage(Task, Format(SCapacityNotSupported, [SafeString(CapacityAssignment.Key)]));
      include(Task.Flags, srScheduledFailed);
      Exit;
    end;

    if CapacityAssignment.MergeProfiles then
    begin
      if ListIndex < CClone.Count - 1 then
        // Go down the chain, add resources for other capacities as well
        CrossJoinCapacities(ListIndex + 1, RClone, CClone) else
        Paths.Add(TEzResourcePath.Create(RClone, CClone));
    end;
 end;

begin
  BaseResources := TEzResourceAssignments.Create(True);
  BaseCapacities := TEzCapacityAssignments.Create(True);

  i_ass := 0;
  while i_ass < Task.Resources.Count do
  begin
    TaskAssignment := Task.Resources[i_ass];

    if TaskAssignment is TEzResourceAssignment then
    begin
      ResourceAssignment := TaskAssignment as TEzResourceAssignment;
      AddResourceAssignment(ResourceAssignment, BaseResources);

      //
      // Add operators of resource to the list as well
      //
      Resource := FResources.ResourceByKey(ResourceAssignment.Key);
      AddResourceOperators(Resource, BaseResources, BaseCapacities);
    end
    else
    begin
      AddCapacityAssignment(TaskAssignment as TEzCapacityAssignment, BaseCapacities);
    end;

    inc(i_ass);
  end;

  // Does this task use any 'resource by capacity'?
  if BaseCapacities.Count > 0 then
  begin
    CrossJoinCapacities(0, BaseResources, BaseCapacities);
    BaseResources.Free;
    BaseCapacities.Free;
  end else
    Paths.Add(TEzResourcePath.Create(BaseResources, BaseCapacities));
end;

procedure TEzScheduler.ScheduleToResourceProfile(
    const Key: Variant;
    Task: TEzScheduleTask;
    Interval: TEzScheduleInterval;
    TestSchedule: Boolean;
    var ResultInterval: TEzTimeSpan);
var
  i_path: Integer;
  Resource: TEzResource;
  Assignment: TEzResourceAssignment;
  Data: TEzCalendarScheduleData;
  Intervals: TEzTimeSpanArray;
  Profile: TEzAvailabilityProfile;
  BestResult: TEzTimeSpan;
  BestIntervals: TEzTimeSpanArray;
  BestPath: TEzResourcePath;
  BestFlags: TEzScheduleResults;
  PathHasMergedProfile: Boolean;
  Paths: TEzResourcePathList;
  ProfilesToDestroy: array of TObject;

  procedure Reset;
  begin
    PathHasMergedProfile := False;
    Intervals.Clear;
    Data.AllResourcesAllowSwithing := True;
    Profile.Free;
  end;

  procedure AddAssigmentToProfile(AProfile: TEzAvailabilityProfile; Assignment: TEzResourceAssignment);
  begin
    Resource := FResources.ResourceByKey(Assignment.Key);

    if (Task.ResourceAllocation=alFixed) and (Assignment.Assigned<=0.0) then
      // Assignment should be greater than 0
      AddScheduleMessage(Task, Format(SResourceAssignmentIsNull, [Resource.Name]))

    else
    begin
      Resource.ProfileRequired;
      Resource.ActualAvailability.AllowTaskSwitching := Resource.AllowTaskSwitching;

      // if schedule type == calendar or if constraint == MFO or MSO
      // then we ignore any task already scheduled and schedule
      // this task against the original resource availability.
      if (Interval.ScheduleFlag=ifResourceDefaultAvailability) or (Task.Constraint in [cnMSO, cnMFO]) then
      begin
        Resource.Availability.UnitsRequired := Assignment.Assigned;
        Resource.Availability.AffectsDuration := Assignment.AffectsDuration;
        AProfile.AddSource(Resource.Availability, False)
      end
      else
      begin
        Resource.ActualAvailability.UnitsRequired := Assignment.Assigned;
        Resource.ActualAvailability.AffectsDuration := Assignment.AffectsDuration;
        AProfile.AddSource(Resource.ActualAvailability, False);
      end;
    end;
  end;

  procedure AddMergedProfilesToProfile(Path: TEzResourcePath; var i_res: Integer);
  var
    MergedProfile: TEzAvailabilityProfile;
    ResourceAssignment: TEzResourceAssignment;
  begin
    PathHasMergedProfile := True;
    SetLength(ProfilesToDestroy, length(ProfilesToDestroy) + 1);
    MergedProfile := TEzAvailabilityProfile.Create;
    ProfilesToDestroy[High(ProfilesToDestroy)] := MergedProfile;

    MergedProfile.ScheduleProfileBaseUnits := 0.0;
    MergedProfile.Operator := opMergeScheduleProfile;
    MergedProfile.UnitsRequired := 1.0;

    repeat
      ResourceAssignment := Path.Resources[i_res];
      AddAssigmentToProfile(MergedProfile, ResourceAssignment);
      inc(i_res);
    until (i_res = Path.Resources.Count) or (Path.Resources[i_res].CapacityKey <> Assignment.CapacityKey);

    MergedProfile.AffectsDuration := ResourceAssignment.AffectsDuration;
    Profile.AddSource(MergedProfile, False);
  end;

  procedure PrepareProfile(Path: TEzResourcePath);
  var
    i_res: Integer;

  begin
    Profile := TEzAvailabilityProfile.Create;
    Profile.Operator := opScheduleProfile;

    i_res := 0;
    while i_res < Path.Resources.Count do
    begin
      Assignment := Path.Resources[i_res];

      if (i_res < Path.Resources.Count - 1) and
         (not VarIsEmpty(Assignment.CapacityKey)) and
         (Path.Resources[i_res + 1].CapacityKey = Assignment.CapacityKey)
      then
        AddMergedProfilesToProfile(Path, i_res)
      else
      begin
        AddAssigmentToProfile(Profile, Assignment);
        inc(i_res);
      end;
    end;
  end;

begin
  Assert(Interval.IsSlackInterval=False);

  if VarIsNull(Key) then; // Prevent key from removing by optimiser

  Data.Start := Task.ScheduleData.Window.Start;
  Data.Stop := Task.ScheduleData.Window.Stop;
  Data.AbsoluteStart := DatetimeToEzDatetime(FProjectWindowAbsoluteStart);
  Data.AbsoluteStop := DatetimeToEzDatetime(FProjectWindowAbsoluteStop);

  Intervals := FScheduleIntervals;

  BestFlags := [];
  BestResult.Start := END_OF_TIME;
  BestResult.Stop := 0;
  BestPath := nil;
  Profile := nil;

  Paths := TEzResourcePathList.Create(True);
  BestIntervals := TEzTimeSpanArray.Create;

  try
    CrossJoinProfiles(Task, Paths);

    Data.Direction := Task.ScheduleData.Direction;
    Data.Duration := DoubleToEzDuration(Interval.Duration);
    Data.UnitsRequired := 1;                // Must be 1 when scheduling against
                                            // a profile with opScheduleProfile
    Data.AllResourcesAllowSwithing := False;// Must be false when scheduling against
                                            // a profile with opScheduleProfile
    Data.ResourceAllowsSwithing := False;   // Must be false when scheduling against
                                            // a profile with opScheduleProfile
    Data.SchedulebeyondScheduleWindow := FScheduleBeyondScheduleWindow;
    Data.AllocationType := Task.ResourceAllocation;

    for i_path := 0 to Paths.Count - 1 do
    begin
      Reset;

      PrepareProfile(Paths[i_path]);

      Data.FailingProfile := nil;
      Data.ScheduleResults := [];
      Profile.ScheduleTask(Data, Intervals);

      if srScheduledOK in Data.ScheduleResults then
      begin
        ResultInterval := GetExtendsFromIntervals(Intervals);
        if ((Data.Direction=sdForwards) and (ResultInterval.Start<BestResult.Start)) or
           ((Data.Direction=sdBackwards) and (ResultInterval.Stop>BestResult.Stop))
        then
        begin
          BestResult := ResultInterval;
          BestPath := Paths[i_path];
          BestIntervals.Assign(Intervals);
          BestFlags := Data.ScheduleResults;
          if PathHasMergedProfile then
            BestFlags := BestFlags + [srTaskHasMergedProfiles];
        end;
      end;
    end;

    if srScheduledOK in BestFlags then
    begin
      Task.Flags := BestFlags;

      ResultInterval := BestResult;

      AllocateResources(Task, BestIntervals, BestPath);

      if srScheduledOutsideWindow in Task.Flags then
        AddScheduleMessage(Task, SScheduledOutsideWindow);
    end;

    if srScheduledFailed in Task.Flags then
      AddScheduleMessage(Task, STaskScheduleFailed);
  finally
    Paths.Free;
    BestIntervals.Free;
    Profile.Free;
    for i_path := 0 to High(ProfilesToDestroy) do
      ProfilesToDestroy[i_path].Free;
  end;
end;

//procedure TEzScheduler.ScheduleToResourceProfile(
//    const Key: Variant;
//    Task: TEzScheduleTask;
//    Interval: TEzScheduleInterval;
//    TestSchedule: Boolean;
//    var ResultInterval: TEzTimeSpan);
//var
//  Resource: TEzResource;
//  Assignment: TEzResourceAssignment;
//  Data: TEzCalendarScheduleData;
//  Intervals: TEzTimeSpanArray;
//  Joins: TEzResourceAssignmentsCrossJoins;
//
//  procedure PrepareAvailabilityProfile;
//  var
//    i_res: Integer;
//
//  begin
//    Task.ScheduleData.Profile := TEzAvailabilityProfile.Create;
//    Task.ScheduleData.OwnsProfile := True;
//
//    Task.ScheduleData.Profile.Operator := opScheduleProfile;
//    i_res := 0;
//    while i_res < Task.Resources.Count do
//    begin
//      Assignment := Task.Resources[i_res];
//      Resource := FResources.ResourceByKey(Assignment.Key);
//
//      if (Task.ResourceAllocation=alFixed) and (Assignment.Assigned<=0.0) then
//        // Assignment should be greater than 0
//        AddScheduleMessage(Task, Format(SResourceAssignmentIsNull, [Resource.Name]))
//
//      else
//      begin
//        Resource.ProfileRequired;
//        Resource.ActualAvailability.AllowTaskSwitching := Resource.AllowTaskSwitching;
//
//        // if schedule type == calendar or if constraint == MFO or MSO
//    		// then we ignore any task already scheduled and schedule
//        // this task against the original resource availability.
//        if (Interval.ScheduleFlag=ifResourceDefaultAvailability) or (Task.Constraint in [cnMSO, cnMFO]) then
//          Task.ScheduleData.Profile.AddSource(Resource.Availability, Assignment.Assigned, False) else
//          Task.ScheduleData.Profile.AddSource(Resource.ActualAvailability, Assignment.Assigned, False);
//      end;
//
//      inc(i_res);
//    end;
//  end;
//
//  procedure SaveScheduleResult(ScheduleResults: TEzScheduleResults);
//  begin
//    if not Interval.IsSlackInterval then
//    begin
//      Task.Flags := ScheduleResults;
//
//      if srScheduledOK in Task.Flags then
//      begin
//        ResultInterval := GetExtendsFromIntervals(Intervals);
//        AllocateResources(Task, Intervals);
//        if srScheduledOutsideWindow in Task.Flags then
//          AddScheduleMessage(Task, SScheduledOutsideWindow);
//      end
//
//      else if srScheduledFailed in Task.Flags then
//      begin
//        Resource := TrackFailingResource(Task, Interval);
//        if Resource<>nil then
//          AddScheduleMessage(Task, Resource.Key, SResourceScheduleFailed) else
//          AddScheduleMessage(Task, STaskScheduleFailed);
//      end;
//    end
//    else
//      //
//      // Slack interval scheduled
//      //
//    begin
//      if srScheduledOK in ScheduleResults then
//      begin
//        ResultInterval := GetExtendsFromIntervals(Intervals);
//        if srScheduledOutsideWindow in ScheduleResults then
//          AddScheduleMessage(Task, SSlackScheduledOutsideWindow);
//      end
//
//      else if srScheduledFailed in ScheduleResults then
//      begin
//        Include(Task.Flags, srSlackScheduleFailed);
//        AddScheduleMessage(Task, SSlackScheduleFailed);
//      end;
//    end;
//  end;
//
//begin
//  Joins := TEzResourceAssignmentsCrossJoins.Create;
//  CrossJoinProfiles(Task, Joins);
//
//  if VarIsNull(Key) then; // Prevent debugger from removing this variable
//
//  Data.Start := Task.ScheduleData.Window.Start;
//  Data.Stop := Task.ScheduleData.Window.Stop;
//  Data.AbsoluteStart := DatetimeToEzDatetime(FProjectWindowAbsoluteStart);
//  Data.AbsoluteStop := DatetimeToEzDatetime(FProjectWindowAbsoluteStop);
//
//  if FUseWorkingPeriods then
//    Intervals := Interval.GetWorkingPeriods else
//    Intervals := FScheduleIntervals;
//
//  Intervals.Clear;
//
//  if Task.ScheduleData.Profile = nil then
//    PrepareAvailabilityProfile;
//
//  if Task.ScheduleData.Profile.Sources.Count=0 then
//  begin
//    include(Task.Flags, srScheduledOK);
//    Exit;
//  end;
//
//  Data.Direction := Task.ScheduleData.Direction;
//  Data.Duration := DoubleToEzDuration(Interval.Duration);
//  Data.UnitsRequired := 1;                // Must be 1 when scheduling against
//                                          // a profile with opScheduleProfile
//  Data.AllResourcesAllowSwithing := False;// Must be false when scheduling against
//                                          // a profile with opScheduleProfile
//  Data.ResourceAllowsSwithing := False;   // Must be false when scheduling against
//                                          // a profile with opScheduleProfile
//  Data.SchedulebeyondScheduleWindow := FScheduleBeyondScheduleWindow;
//  Data.AllocationType := Task.ResourceAllocation;
//  Data.FailingProfile := nil;
//  Data.ScheduleResults := [];
//  Task.ScheduleData.Profile.ScheduleTask(Data, Intervals);
//  if TestSchedule then
//    Task.Flags := Data.ScheduleResults else
//    SaveScheduleResult(Data.ScheduleResults);
//end;

procedure TEzScheduler.StoreScheduleTask(Node: TRecordNode);
var
  ParentNode: TRecordNode;
  Task, Parent: TEzScheduleTask;

begin
  Task := Node.Data;

	//
  // A task with constraint cnIgnore does not require any scheduling
  //
  if Task.Constraint = cnIgnore then
  begin
    Include(Task.Flags, srScheduledOK);
    Exit;
  end;

  ParentNode := Node.Parent;
  while ParentNode <> nil do
  begin
    Parent := ParentNode.Data;

 		// Add resources of parent to this task
		CopyParentsResources(Task, Parent);

 		// Add predecessors of parent to this task
		CopyParentPredecessors(Task, Parent);

  	// If parents constraint is more restrictive than childs constraint, then
		// use parents constraint
		if Parent.Constraint < Task.Constraint then
    begin
			Task.Constraint := Parent.Constraint;
      Task.ConstraintDate := Parent.ConstraintDate;
    end;

    // if parents priority is higher then childs priority, then
    // overwrite childs priority
    Task.Priority := min(Task.Priority, Parent.Priority);

    ParentNode := ParentNode.Parent;
  end;

	// Check if we end up with a valid constraint and constraint date
	if (Task.Constraint > cnDNL) and (Task.Constraint < cnASAP) then
  begin
		if Task.ConstraintDate = 0 then
    begin
      AddScheduleMessage(Task, SNoConstraintDate);
      Task.Constraint := cnASAP;
    end
		else if Task.ConstraintDate < ProjectWindowAbsoluteStart then
    begin
      AddScheduleMessage(Task, SConstraintBeforeProjectStart);
      Task.Constraint := cnASAP;
    end
		else if Task.ConstraintDate > ProjectWindowAbsoluteStop then
    begin
      AddScheduleMessage(Task, SConstraintAfterProjectEnd);
      Task.Constraint := cnASAP;
    end;
  end;

  ProcessResources(Task);
	NormalizePredecessors(Task);
  FScheduleTasks.Add(Task);
end;

//function TEzScheduler.TrackFailingResource(Task: TEzScheduleTask; Interval: TEzScheduleInterval): TEzResource;
//var
//  SavedFlags: TEzScheduleResults;
//  SavedResources: TEzResourceAssignments;
//  i_res: Integer;
//  ResultInterval: TEzTimeSpan;
//
//begin
//  Result := nil;
//
//  if Task.Resources.Count=1 then
//    Result := FResources.ResourceByKey(Task.Resources[0].Key)
//
//  else
//  begin
//    i_res := 0;
//    SavedFlags := Task.Flags;
//    SavedResources := Task.Resources;
//
//    Task.Resources := TEzResourceAssignments.Create(False {does not own objects});
//    Task.Flags := [];
//    try
//      repeat
//        Task.Resources.Clear;
//        Task.Resources.Add(SavedResources[i_res]);
//        ScheduleToResourceProfile(Task.key.Value, Task, Interval, True, ResultInterval);
//        if (srScheduledOK in Task.Flags) then
//          inc(i_res);
//      until (i_res>=SavedResources.Count) or not (srScheduledOK in Task.Flags);
//
//      if i_res<SavedResources.Count then
//        Result := FResources.ResourceByKey(SavedResources[i_res].Key);
//    finally
//      Task.Resources.Free;
//      Task.Resources := SavedResources;
//      Task.Flags := SavedFlags;
//    end;
//  end;
//end;

procedure TEzScheduler.UpdateWindowWithTask(var ScheduleData: TEzInternalData; Task: TEzScheduleTask);
var
  Estimate: TEzDuration;

begin
	case Task.Constraint of
		cnMSO:
    begin
      ScheduleData.LeftTask := Task;
      ScheduleData.LeftBoundaryType := btConstraintDate;
      ScheduleData.Window.Start := DatetimeToEzDatetime(Task.ConstraintDate);
      ScheduleData.Direction := sdForwards;
    end;

		cnMFO:
    begin
      ScheduleData.RightTask := Task;
      ScheduleData.RightBoundaryType := btConstraintDate;
      ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate);
      ScheduleData.Direction := sdBackwards;
		end;

		cnSNET:
      if DatetimeToEzDatetime(Task.ConstraintDate)>ScheduleData.Window.Start then
      begin
        ScheduleData.LeftTask := Task;
        ScheduleData.LeftBoundaryType := btConstraintDate;
        ScheduleData.Window.Start := DatetimeToEzDatetime(Task.ConstraintDate);
        if ScheduleData.Direction = sdUndefined then
          ScheduleData.Direction := sdForwards;
      end;

		cnSNLT:
    begin
      Estimate := DoubleToEzDuration(EstimateTaskDuration(Task));
      if DatetimeToEzDatetime(Task.ConstraintDate)<ScheduleData.Window.Stop+Estimate then
      begin
        ScheduleData.RightTask := Task;
        ScheduleData.RightBoundaryType := btConstraintDate;
        ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate)+Estimate;
        if ScheduleData.Direction = sdUndefined then
          ScheduleData.Direction := sdForwards;
      end;
    end;

		cnFNLT:
    begin
      if DatetimeToEzDatetime(Task.ConstraintDate)<ScheduleData.Window.Stop then
      begin
        ScheduleData.RightTask := Task;
        ScheduleData.RightBoundaryType := btConstraintDate;
        ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate);
        if ScheduleData.Direction = sdUndefined then
          ScheduleData.Direction := sdForwards;
      end;
    end;

		cnFAB:
			if DatetimeToEzDatetime(Task.ConstraintDate)<ScheduleData.Window.Stop then
      begin
				ScheduleData.RightTask := Task;
        ScheduleData.RightBoundaryType := btConstraintDate;
				ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate);
				ScheduleData.Direction := sdBackwards;
      end;
  end;

  if Task.WindowStart<>0 then
    ScheduleData.Window.Start :=
      max(ScheduleData.Window.Start, DateTimeToEzDateTime(Task.WindowStart));

  if Task.WindowStop<>0 then
    ScheduleData.Window.Stop :=
      min(ScheduleData.Window.Stop, DateTimeToEzDateTime(Task.WindowStop));
end;

procedure TEzScheduler.UpdateWindowWithPredecessorTaskWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Relation.Relation of
    prFinishStart, prStartStart:
      if Predecessor.WindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.WindowStop+Relation.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Predecessor;
          Task.ScheduleData.LeftBoundaryType := btTaskWindowEndDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Predecessor.WindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.WindowStart-Relation.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Predecessor;
          Task.ScheduleData.RightBoundaryType := btTaskWindowStartDate;
        end;
      end;
  end;
end;

procedure TEzScheduler.UpdateWindowWithPredecessorSlackWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Relation.Relation of
    prFinishStart, prStartStart:
      if Predecessor.SlackWindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.SlackWindowStop+Relation.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Predecessor;
          Task.ScheduleData.LeftBoundaryType := btSlackWindowEndDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Predecessor.SlackWindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.SlackWindowStart-Relation.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Predecessor;
          Task.ScheduleData.RightBoundaryType := btSlackWindowStartDate;
        end;
      end;
  end;
end;

procedure TEzScheduler.UpdateWindowWithPredecessor(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
var
  sd: TEzScheduleDirection;
	ADate: TEzDateTime;
  BoundaryType: TScheduleWindowBoundType;

begin
  sd := sdForwards;
  case Relation.Relation of
    prFinishStart:
    begin
			sd := sdForwards;
      if IsMileStone(Predecessor) then
      begin
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate+Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
        ADate := GetExtendsFromIntervals(Predecessor.Intervals).Stop+DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskEndDate;
      end;

			if(ADate > Task.ScheduleData.Window.Start) then
      begin
        Task.ScheduleData.Window.Start := ADate;
        Task.ScheduleData.LeftTask := Predecessor;
        Task.ScheduleData.LeftBoundaryType := BoundaryType;
      end;
    end;

		prStartFinish:
    begin
      if IsMileStone(Predecessor) then
      begin
  			sd := sdForwards;
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate-Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Predecessor.Intervals).Start-DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskStartDate;
      end;

			if(ADate<Task.ScheduleData.Window.Stop) then
      begin
        Task.ScheduleData.Window.Stop := ADate;
        Task.ScheduleData.RightTask := Predecessor;
        Task.ScheduleData.RightBoundaryType := BoundaryType;
      end;
    end;

		prStartStart:
    begin
			sd := sdForwards;
      if IsMileStone(Predecessor) then
      begin
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate+Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			ADate := GetExtendsFromIntervals(Predecessor.Intervals).Start+
                 DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskStartDate;
      end;

			if(ADate>Task.ScheduleData.Window.Start) then
      begin
        Task.ScheduleData.Window.Start := ADate;
        Task.ScheduleData.LeftTask := Predecessor;
        Task.ScheduleData.LeftBoundaryType := BoundaryType;
      end;
    end;

		prFinishFinish:
    begin
      if IsMileStone(Predecessor) then
      begin
  			sd := sdForwards;
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate-Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
        sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Predecessor.Intervals).Stop-
                 DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskEndDate;
      end;

			if(ADate<Task.ScheduleData.Window.Stop) then
      begin
        Task.ScheduleData.Window.Stop := ADate;
        Task.ScheduleData.RightTask := Predecessor;
        Task.ScheduleData.RightBoundaryType := BoundaryType;
      end;
    end;
  end;

  case Task.ScheduleData.Direction of
		sdUndefined:
      Task.ScheduleData.Direction := sd;

		sdForwards:
			if sd = sdBackwards then
				Task.ScheduleData.Direction := sdDontCare;

		sdBackwards:
			if sd = sdForwards then
				Task.ScheduleData.Direction := sdDontCare;
	end;
end;

procedure TEzScheduler.UpdateWindowWithSuccessor(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask);
var
  sd: TEzScheduleDirection;
	ADate: TEzDateTime;
  BoundaryType: TScheduleWindowBoundType;
  
begin
  sd := sdBackwards;
  case Successor.Relation of
    prFinishStart:
    begin
      sd := sdForwards;

      if IsMileStone(Successor.Task) then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate-Successor.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Start-
                 DoubleToEzDuration(Successor.Lag);
        BoundaryType := btTaskStartDate;
      end;

			if(ADate<Task.ScheduleData.Window.Stop) then
      begin
        Task.ScheduleData.Window.Stop := ADate;
        Task.ScheduleData.RightTask := Successor.Task;
        Task.ScheduleData.RightBoundaryType := BoundaryType;
      end;
    end;

		prStartFinish:
    begin
			sd := sdForwards;
      if IsMileStone(Successor.Task) then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate+Successor.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Stop+
                 DoubleToEzDuration(Successor.Lag);
        BoundaryType := btTaskEndDate;
      end;

			if(ADate>Task.ScheduleData.Window.Start) then
      begin
        Task.ScheduleData.Window.Start := ADate;
        Task.ScheduleData.LeftTask := Successor.Task;
        Task.ScheduleData.LeftBoundaryType := BoundaryType;
      end;
    end;

		prStartStart:
    begin
      if IsMileStone(Successor.Task) then
      begin
  			sd := sdForwards;
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate-Successor.Lag);
  			if(ADate>Task.ScheduleData.Window.Start) then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btConstraintDate;
        end;
      end
      else
      begin
        sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Start+
                 DoubleToEzDuration(estimateTaskduration(Task))-
                 DoubleToEzDuration(Successor.Lag);
  			if(ADate<Task.ScheduleData.Window.Stop) then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btTaskStartDate;
        end;
      end;
    end;

		prFinishFinish:
    begin
      if IsMileStone(Successor.Task) then
      begin
        sd := sdForwards;
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate+Successor.Lag);
  			if(ADate>Task.ScheduleData.Window.Start) then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btConstraintDate;
        end;
      end
      else
      begin
        sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Stop-
                 DoubleToEzDuration(estimateTaskduration(Task))+
                 DoubleToEzDuration(Successor.Lag);
  			if(ADate<Task.ScheduleData.Window.Stop) then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btTaskEndDate;
        end;
      end;
    end;
  end;

  case Task.ScheduleData.Direction of
		sdUndefined:
      Task.ScheduleData.Direction := sd;

		sdForwards:
			if sd = sdBackwards then
				Task.ScheduleData.Direction := sdDontCare;

		sdBackwards:
			if sd = sdForwards then
				Task.ScheduleData.Direction := sdDontCare;
	end;
end;

procedure TEzScheduler.UpdateWindowWithSuccessorTaskWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Successor.Relation of
    prFinishStart, prStartStart:
      if Successor.Task.WindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.WindowStart-Successor.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btTaskWindowStartDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Successor.Task.WindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.WindowStop+Successor.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btTaskWindowEndDate;
        end;
      end;
  end;
end;

procedure TEzScheduler.UpdateWindowWithSuccessorSlackWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Successor.Relation of
    prFinishStart, prStartStart:
      if Successor.Task.SlackWindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.SlackWindowStart-Successor.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btSlackWindowStartDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Successor.Task.SlackWindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.SlackWindowStop+Successor.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btSlackWindowEndDate;
        end;
      end;
  end;
end;

function TEzScheduler.VerifyTaskWindow(Task: TEzScheduleTask): Boolean;
begin
  if Task.ScheduleData.Window.Stop<Task.ScheduleData.Window.Start then
  begin
    Result := False;
    AddScheduleMessage(Task, SEmptyScheduleWindow);
  end else
    Result := True;
end;

{ TEzSortedResourceAssignments }

function TEzSortedResourceAssignments.CompareItems(Item1,
  Item2: TObject): Integer;
var
  i1: TEzResourceAssignment;
  i2: TEzResourceAssignment;

begin
  i1 := TEzResourceAssignment(Item1);
  i2 := TEzResourceAssignment(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzSortedResourceAssignments.GetItem(
  Index: Integer): TEzResourceAssignment;
begin
  Result := TEzResourceAssignment(inherited GetItem(Index));
end;

function TEzSortedResourceAssignments.IndexOfKey(Key: Variant): Integer;
var
  Find: TEzResourceAssignment;

begin
  Find := TEzResourceAssignment.Create;
  try
    Find.FKey := Key;
    if not IndexOf(Result, Find) then
      Result := -1;
  finally
    Find.Free;
  end;
end;

{ TEzSortedCapacityAssignments }

function TEzSortedCapacityAssignments.CompareItems(Item1,
  Item2: TObject): Integer;
var
  i1: TEzCapacityAssignment;
  i2: TEzCapacityAssignment;

begin
  i1 := TEzCapacityAssignment(Item1);
  i2 := TEzCapacityAssignment(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzSortedCapacityAssignments.GetItem(
  Index: Integer): TEzCapacityAssignment;
begin
  Result := TEzCapacityAssignment(inherited GetItem(Index));
end;

function TEzSortedCapacityAssignments.IndexOfKey(Key: Variant): Integer;
var
  Find: TEzCapacityAssignment;

begin
  Find := TEzCapacityAssignment.Create;
  try
    Find.FKey := Key;
    IndexOf(Result, Find);
  finally
    Find.Free;
  end;
end;

{ TEzResourceAssignment }
constructor TEzResourceAssignment.Create;
begin
  inherited;

end;

constructor TEzResourceAssignment.Create(CopyFrom: TEzResourceAssignment);
begin
  inherited Create(CopyFrom);
  FCapacityKey := CopyFrom.CapacityKey;
end;

constructor TEzTaskAssignment.Create(CopyFrom: TEzTaskAssignment);
begin
  FKey := CopyFrom.Key;
  FAssigned := CopyFrom.Assigned;
  FAffectsDuration := CopyFrom.AffectsDuration;
  ScheduleSucceeded := CopyFrom.ScheduleSucceeded;
end;

{ TEzResourcePath }

constructor TEzResourcePath.Create(
    _Resources: TEzResourceAssignments;
    _Capacities: TEzCapacityAssignments);

begin
  FResources := _Resources;
  FCapacities := _Capacities;
end;

destructor TEzResourcePath.Destroy;
begin
  inherited;
  FResources.Free;
  FCapacities.Free;
end;

{ TEzResourcePathList }

function TEzResourcePathList.GetItem(Index: Integer): TEzResourcePath;
begin
  Result := TEzResourcePath(inherited GetItem(Index));
end;

{ TEzWorkingPeriods }

function TEzWorkIntervals.GetItem(Index: Integer): TEzWorkInterval;
begin
  Result := TEzWorkInterval(inherited GetItem(Index));
end;

procedure TEzWorkIntervals.SetItem(Index: Integer; Value: TEzWorkInterval);
begin
  inherited SetItem(Index, Value);
end;

{ TEzWorkInterval }

destructor TEzWorkInterval.Destroy;
begin
  inherited;
  ResourceAssigments.Free;
end;

end.
