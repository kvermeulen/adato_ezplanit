unit EzStrings_3;

interface

resourcestring

  // EzScheduler
  SNoString = 'empty';

  // EzScheduler
  SResourceScheduleFailed = 'Failed to schedule this task because a resource has insufficient availability during the ''schedule window''.';

  // EzScheduler
  SInvalidTask = 'Task is invalid';
  SInvalidPredecessorRelation = 'Invalid predecessor relation.';
  SCapacityNotSupported = 'None of the resources supports capacity with id %s';
  SCircularRelation = 'Circular predecessor relation encountered between predecessor task %s and successor task %s.';
  SDuplicateTask = 'A task with key ''%s'' is already loaded into the scheduler.';
  SDuplicateResources = 'Resource ''%s'' is assigned to this task and to one of its parents. Therefore the resource assignment to the parent is ignored.';
  SDuplicatePredecessor = 'Predecessor ''%s'' is assigned to this task and to one of its parents. Therefore the predecessor assignment to the parent is ignored.';
  SNoProfileForInterval = 'There is no schedulprofile set within Flags property of Interval.';
  SNoConstraintDate = 'The constraint set for this task requires a constraint date, such a date was not set and therefore the constraint is ignored.';
  SConstraintBeforeProjectStart = 'The constraint date given lies before the project start date, therefore this constraint will be ignored.';
  SConstraintAfterProjectEnd = 'The constraint date given lies after the project end date, therefore this constraint will be ignored.';
  SCouldNotLoadAvailability = 'Failed to load availability profile for resource having key %s. Please check your implementation of OnLoadResourceData.';
  SPredecessorTaskNotFound = 'Predecessor ''%s'' could not be located for this task.';
  SPredecessorRelationInvalid = 'Predecessor relation ignored: cannot link a parent (task %s) to one of it''s childs (task %s).';
  SResourceNotFound = 'Schedule resource ''%s'' could not be located.';
  SResourceProfilesDoNotMatch = 'This task could not be scheduled because the resource profiles do not match.';
  SResourceCantSwitch = 'Resource ''%s'' could not be scheduled for this task because this resource does not allow task switching.';
  SResourceOverallocated = 'Resource ''%s'' is overallocated due to scheduling for this task.';
  SCannotUseCapacityWithDNL = 'Resources cannot be assigned by capacity when scheduling a ''do not level'' constraint.';
  SResourceAssignmentIsNull = 'Assignment of resource ''%s'' to task cannot be 0.';
  SInvalidProjectDates = 'Project start date must be smaller than project end date.';
  SProjectAvailabilityNotSet = 'Default availability profile is not set, therefore we cannot schedule tasks without resources.';
  SNoIntervals = 'A task without intervals cannot be scheduled unless constraint date is set (i.e. a milestone).';
  SEmptyScheduleWindow = 'This task could not be scheduled because the schedule window is empty.';
  STaskScheduleFailed = 'Scheduling this task failed.';
  STaskNotFound = 'Task not found.';
  SIntevalsAreEmpty = 'No intervals present, getting extends is not possible.';
  SScheduledOutsideWindow = 'This task is scheduled outside it''s schedule window.';
  SSlackScheduledOutsideWindow = 'Schedule window too small to allocate the requested amount of slack.';
  SSlackScheduleFailed = 'Scheduler failed to allocate the requested amount of slack.';

  // End of EzScheduler

  // EzGantt
  SNoDataSet = 'No dataset.';
  SWrongDatasetType = 'Gantt chart can only be used with TCustomEzDatasets.';
  SDuplicateName = 'Duplicate ganttbar name.';
  SSuccessorNotFound = 'Could not locate successor node when painting predecessor line.';
  SPredecessorNotFound  = 'Could not locate predecessor node when painting predecessor line.';
  SInvalidPredecessorLine = 'Invalid predecessorline data.';
  SDatalinkNameNotUsed = 'Property EzDatalinkName is not used for self-referencing datasets. Use NodeTypes instead.';
  SNodeTypesNotUsed = 'Property NodeTypes is not used for master/detail datasets. Use EzDatalinkName instead.';
  SConfirmDeleteBars = 'Delete records for selected bar(s)?';
  // End of EzGantt

  // EzRuleEd
  SInvalidTimes = '''from'' hours must lie before ''to'' hours.';
  SStartDateRequired = 'A start date is required when using a count value.';
  SStartDateRequiredWithEndAfter = 'A start date is required when setting a number of occurrences.';
  // End of EzRuleEd

  // EzDataset
  SUnsupportedFieldType = 'Unsupported field type (%s) in field %s.';
  SCantUseFormulaWithFieldType = 'This field type (%s) doesn''t support formula''s.';
  SNoKeyFields = 'Key fields are not set.';
  SInvalidFormula = 'Error in formula: %s.'#13#10'Error message: %s.';
  SNoValue = 'Unable to retrieve value for variable: ''%s''.';
  SNoActiveCursor = 'No active cursor set.';
  SCannotGenerate = 'Cannot generate key value for this fieldtype.';
  SConnotMove = 'Cannot move record between different datasets.';
  SConnotMoveNoLink = 'Cannot move record because field ''%s'' is not linked to any field in this dataset.';
  SDefaultCursorName = 'DefaultCursor';
  SInvalidNode = 'Action cannot be performed on this node.';
  SInvalidCursorIndex = 'Cursor index out of range.';
  SInvalidCursorPosition = 'Invalid assignment to CurrentNode.';
  SInvalidFieldLink = 'Could not resolve link: ''%s''';
  SNoKey = 'Key cannot be null.';
  SKeyNotFound = 'Key not found.';
  SKeyValueRequired = 'Inserting a record in the detail dataset did not return a value for the key field.'#10#13'You might want to set the index field to AutoIncrement or turn on eoGenerateKeys if you want TEzDataset to generate key values.';
  SKeyCannotChange = 'Key value(s) may not be updated.';
  SParentNotFound = 'Parent not found';
  SNodeNotFound = 'Node not found.';
  SLocateFailed = 'Failed to locate a record using Locate on datalink ''%s''.';
  SBookmarkRequired = 'A bookmark is required when calling GenerateKey.';
  SBookmarkNotFound = 'Bookmark could not be located.';
  SIncompatibleKeys = 'Incompatible keys: main key not compatible with parent reference key.';
  SCircularParent = 'Circular parent reference found!';
  SParentCantChange = 'Parent value cannot be changed for this record.';
  SCreateCalculatorFailed = 'Failed to instantiate calculator object, probably because EzPi.dll is not registered.';
  SCantDeleteDefCursor = 'Default cursor may not be deleted.';
  SDefCursorCantSort = 'Default cursor cannot be sorted.';
  SDefCursorCantFilter = 'Default cursor cannot be filtered.';
  SNoRecnumbers = 'Record numbers cannot be used with filtered cursors.';
  SMappingFailed = 'Could not map field ''%s''';
  SReverseMapFailed = 'Could not reverse-map field ''%s''';
  SParentRequired = 'Failed to load cursor: record ''%s'' has no parent.';
  SRecursiveDataset = 'Failed to open detail dataset: circular datalink encountered.';
  SRecursiveFormula = 'Recursion in formula: ''%s''';
  SLinkindexOutOfRange = 'Cannot access this link with master/detail dataset';
  SInvalidStreamFormat = 'Invalid stream format while reading TEzFieldLinks';
  SCannotInsertChild = 'A child cannot be inserted in this position.';
  SInvalidDatalinkIndex = 'Index (%d) out of range for when accessing datalinks collection.';
  SCannotMoveToItself = 'Cannot move Node to itself.';
  SCalcfieldUpdated = 'Cannot assign value to calculated field ''%s''.';
  SUsupportedFieldType = 'Unsupported field type (%s) in field %s';
  // End of EzDataset

  // EzDBGrid
  SInvalidDataset = 'Dataset must be of type TEzDataSet';
  // End of EzDBGrid

  // EzCalendar
  SThe = 'the ';
  SWorkingText = 'working';
  SNonWorkingText = 'not working';
  SEveryText = 'every %s ';
  SStartAtText = ', starting at %s';
  SEndAtText = 'ending at %s';
  SBetweenText = ' between %s and %s';
  SOccurrencesText = ' for %d occurrences';
  SWorkingHourText = 'Resources are normally working between %s and %s.';
  SRecurDayWeek = 'Resources are %s on %s%s%s%s.';
  SRecurDateMonth = 'Resources are %s on the %s day of %smonth%s%s.';
  SRecurDayMonth = 'Resources are %s on every %s %s of %smonth%s%s.';
  SRecurDateYear = 'Resources are %s on every %s %s%s%s.';
  SRecurDayYear = 'Resources are %s on every %s %s in %s%s%s.';
  SPeriodDay = 'Resources are %s on %s.';
  SPeriodDayTime = 'Resources are %s on %s between %s and %s.';
  SPeriodDayDay = 'Resources are %s between %s, %s and %s, %s.';

  SInvalidDayValue = 'Invalid day value for this rule type.';
  SInvalidMonthValue = 'Invalid month value for this rule type.';
  SInvalidYearValue = 'Invalid year value for this rule type.';

  SRuleOverlaps = 'This rule overlaps with a rule already stored in this set.';
  SNullInterval = 'Invalid interval, duration equals to 0.';
  SInvalidDates = 'Date values are invalid.';
  SInvalidScheduleDirection = 'Invalid schedule direction, should be sdForwards or sdBackwards.';
  SNoDataToCopy = 'Source profile does not contain any data.';
  SDuplicateSource = 'Source already exists.';
  SCantAddSelf = 'Source can''t be the same as ''Self''.';
  SMaximumSourcesExceeded = 'Maximimum number of sources exceeded.';
  SDateRangeRequired = 'Date range required when Rules is set.';
  SRulesRequired = 'Rules object required for this function.';
  // End of EzCalendar

  // EzPrinter
  SInvalidPageNum = 'Page number must be 1 or greater.';
  SNoDateBounds = 'Could not calculate date bounds.';
  SDefaultTitle = 'EzPlan-IT document';
  SPageToSmall = 'Page size to small.';
  SPrintComponent = 'No component to print; neither PrintGrid or PrintGantt are set.';
  SCantFit = 'Output cannot be fitted to %d by %d pages.';
  // End of EzPrinter

  // EzPreview
  SInvalidZoomFactor = 'Invalid zoom factor.';
  // End of EzPreview

implementation

end.

