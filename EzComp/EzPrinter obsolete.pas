unit EzPrinter;

{$I Ez.inc}

interface

uses Classes,
     Controls,
     Graphics,
     Messages,
     EzMasks,
     EzDbGrid,
     DbGrids,
     EzGantt,
     SysUtils,
     EzDataset,
     EzTimebar,
     Windows,
     Forms,
     StdCtrls;

type
  EEzPrinterError = class(Exception);
  TEzPrinter = class;

  TEzUnit = (euMM, euInches, euPixels, euNative);
  TEdgeBorder = (ebLeft, ebTop, ebRight, ebBottom);
  TEdgeBorders = set of TEdgeBorder;

  TPageCoord = record
    HPage, VPage: integer;
  end;

  TPrintPages = record
    HPages, VPages: integer;
  end;

  TPrintRange = class(TObject)
  public
    PageSettingsOK: Boolean;
    DateBoundsOK: Boolean;

    Pages: TPrintPages;

    PageCoord: TPageCoord;    // Coordinate of requested page
                              // if HPage >= 0 then horizontal range
                              //  information is cached.
                              // if VPage >= 0 then vertical range
                              //  information is cached.

    //
    // Horizontal range information
    //
    LeftColumn,
    RightColumn,
    GridWidth: integer;
    PageStartDate,
    PageEndDate,
    StartDate,
    EndDate: TDateTime;
    TimebarWidth: integer;
    ShowSeperator: Boolean;
    PrintLegend: Boolean;
    ExtraLegendPage: Boolean;
    //
    // Vertical range information
    //
    TopNode: TRecordNode;
    RecordCount: integer;

    //
    // Page band information
    //
    PagePhysicalWidth: Int64;
    PagePhysicalHeight: Int64;

    PrinterBounds,
    PageBounds,
    HeaderRect,
    PrintRect,
    FooterRect,
    EdgeRect,
    LegendRect: TRect;

    constructor Create;
    procedure   Reset(PageSettings, DateBounds: Boolean);
    function    GetPrintHeight: integer;
    function    GetPrintWidth: integer;

    public
      property PrintHeight: integer read GetPrintHeight;
      property PrintWidth: integer read GetPrintWidth;
  end;

  TEzScaleToFitSettings = class(TPersistent)
  protected
    FPrinter: TEzPrinter;
    FHPages: Integer;
    FVPages: Integer;
    FScaleStep: Integer;
    FScaleMin: Integer;
    FScaleHoriz: Integer;
    FScaleVert: Integer;

    function  GetScale: Integer;
    procedure SetScaleStep(Value: Integer);
    procedure SetScaleMin(Value: Integer);
    procedure SetScale(Value: Integer);
    procedure SetScaleHoriz(Value: Integer);
    procedure SetScaleVert(Value: Integer);
    procedure SetHPages(Value: Integer);
    procedure SetVPages(Value: Integer);

  public
    constructor Create(P: TEzPrinter);
    procedure Assign(Source: TPersistent); override;

  published
    property HPages: Integer read FHPages write SetHPages default -1;
    property VPages: Integer read FVPages write SetVPages default -1;
    property ScaleStep: Integer read FScaleStep write SetScaleStep default 5;
    property ScaleMin: Integer read FScaleMin write SetScaleMin default 50;
    property ScaleHoriz: Integer read FScaleHoriz write SetScaleHoriz default 100;
    property ScaleVert: Integer read FScaleVert write SetScaleVert default 100;
    property Scale: Integer read GetScale write SetScale;
  end;

  TEzPageSettings = class(TPersistent)
  private
    FPrinter: TEzPrinter;
    FMargin: TRect;
    FSize: TSize;

  protected
    function  GetMargin(Index: integer) : integer;
    procedure SetMargin(Index: integer; m: integer);
    function  GetSize(Index: integer) : integer;
    procedure SetSize(Index: integer; s: integer);

  public
    constructor Create(P: TEzPrinter);
    procedure Assign(Source: TPersistent); override;

    property Margins: TRect read FMargin write FMargin;
    property PageSize: TSize read FSize write FSize;

  published
    property LeftMargin: integer index 0 read GetMargin write SetMargin default 1250;
    property TopMargin: integer index 1 read GetMargin write SetMargin default 1250;
    property RightMargin: integer index 2 read GetMargin write SetMargin default 1250;
    property BottomMargin: integer index 3 read GetMargin write SetMargin default 1250;
    property PageWidth: integer index 0 read GetSize write SetSize default 21000;
    property PageLength: integer index 1 read GetSize write SetSize default 29700;
  end;

  TEzLegendLocation = (lcNone, lcFirstPage, lcLastPage, lcEveryPage);
  TEzLegendPosition = (lpLeft, lpTop, lpRight, lpBottom);
  TEzPrintProgressStep = (psInitialization, psPrinting);

  TEzLegendSettings = class(TPersistent)
  private
    FControl: TWinControl;
    FLocation: TEzLegendLocation;
    FPosition: TEzLegendPosition;
    FSpace: Integer;

  protected

  public
    constructor Create;

    procedure Assign(Source: TPersistent); override;

  published
    property Control: TWinControl read FControl write FControl;
    property Location: TEzLegendLocation read FLocation write FLocation default lcNone;
    property Position: TEzLegendPosition read FPosition write FPosition default lpBottom;
    property Space: Integer read FSpace write FSpace default 2;
  end;

  TEzPrintProgressEvent = procedure(Sender: TEzPrinter; Step: TEzPrintProgressStep; Progress, MaxProgress: Integer; var Abort: Boolean) of object;

  TEzPrinter = class(TComponent)
  protected
    FPrintDate: TDateTime;
    FPrintGrid: TCustomEzDBCoolGrid;
    FPrintGantt: TEzGanttchart;
    FLegend: TEzLegendSettings;
    FEdgeBorders: TEdgeBorders;
    FEdgePen: TPen;
    FBorderSize: integer;
    FEzDataset: TEzDataset;
    FEzTimebar: TEzTimebar;
    FScaleToFit: TEzScaleToFitSettings;
    FSeperatorWidth: integer;
    FPrintRange: TPrintRange;
    FTruncDateBoundaries: Boolean;
    FUnits: TEzUnit;
    FPageSettings: TEzPageSettings;
    FShowMargins, FShowPrinterBounds: Boolean;
    FPreviewForm: TComponent;
    FCollate: Boolean;
    FCopies: Integer;
    FPages: string;
    FHeaderFont: TFont;
    FFooterFont: TFont;
    FHeaderHeight: integer;
    FFooterHeight: integer;
    FTitle,
    FHeaderLeft, FHeaderCenter, FHeaderRight,
    FFooterLeft, FFooterCenter, FFooterRight: WideString;
    FOnPrintProgress: TEzPrintProgressEvent;
    FScrn_DpiX: DWord;
    FScrn_DpiY: DWord;
    FPrinter_DpiX: DWord;
    FPrinter_DpiY: DWord;

    FDebugInfo: TStringList;
    FDebugDir: string;

  protected
    procedure CalcDateBounds(Range: TPrintRange);
    procedure DoProgress(Step: TEzPrintProgressStep; Progress, MaxProgress: Integer);
    function  GetEzDataset: TEzDataset;
    function  GetFooterHeight: integer;
    function  GetHeaderHeight: integer;
    function  GetTitle : WideString;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    function  ScaleValueHoriz(V: Integer): Integer;
    function  ScaleValueVert(V: Integer): Integer;
    function  StoreTitle: Boolean;
    procedure SetEdgePen(P: TPen);
    procedure SetScaleToFit(V: TEzScaleToFitSettings);
    procedure SetFooterFont(F: TFont);
    procedure SetFooterHeight(H: integer);
    procedure SetHeaderHeight(H: integer);
    procedure SetHeaderFont(F: TFont);
    procedure SetLegend(Value: TEzLegendSettings);
    procedure SetPageSettings(P: TEzPageSettings);
    procedure SetPrintGrid(Value: TCustomEzDBCoolGrid);
    procedure UpdatePrintRange(Range: TPrintRange; Page: TPageCoord);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    function  CalcBoundRect(B: TRect; var OutputScale: Integer) : TRect;
    procedure CalcPrintSize(Range: TPrintRange);
    function  HasDataset: Boolean;
    procedure Print;
    procedure Preview;
    procedure PrintPageTo(ACanvas: TCanvas; Range: TPrintRange; Page: TPageCoord; X, Y: Integer);
    function  ToNative(val: Double; UnitIn: TEzUnit) : integer;
    function  ToUnit(val: integer; UnitOut: TEzUnit) : Double;

    function  TransForm(val: Double; UnitIn, UnitOut: TEzUnit) : Double;

    property DebugDir: string read FDebugDir write FDebugDir;
    property EzDataset: TEzDataset read GetEzDataset;
    property PreviewForm: TComponent read FPreviewForm;
    property PrintRange: TPrintRange read FPrintRange write FPrintRange;

  published
    property Collate: Boolean read FCollate write FCollate default True;
    property Copies: Integer read FCopies write FCopies default 1;
    property EdgeBorders: TEdgeBorders read FEdgeBorders write FEdgeBorders
        default [ebLeft, ebTop, ebRight, ebBottom];
    property EdgePen: TPen read FEdgePen write SetEdgePen;
    property FooterFont: TFont read FFooterFont write SetFooterFont;
    property FooterHeight: integer read GetFooterHeight write SetFooterHeight default 1000;
    property FooterLeft: WideString read FFooterLeft write FFooterLeft;
    property FooterCenter: WideString read FFooterCenter write FFooterCenter;
    property FooterRight: WideString read FFooterRight write FFooterRight;

    property HeaderFont: TFont read FHeaderFont write SetHeaderFont;
    property HeaderHeight: integer read GetHeaderHeight write SetHeaderHeight default 0;
    property HeaderLeft: WideString read FHeaderLeft write FHeaderLeft;
    property HeaderCenter: WideString read FHeaderCenter write FHeaderCenter;
    property HeaderRight: WideString read FHeaderRight write FHeaderRight;
    property ScaleToFit: TEzScaleToFitSettings read FScaleToFit write SetScaleToFit;
    property Legend: TEzLegendSettings read FLegend write SetLegend;
    property Pages: string read FPages write FPages;
    property PrintDate: TDateTime read FPrintDate write FPrintDate;
    property PrintGrid: TCustomEzDBCoolGrid read FPrintGrid write SetPrintGrid;
    property PrintGantt: TEzGanttchart read FPrintGantt write FPrintGantt;
    property TruncDateBoundaries: Boolean read FTruncDateBoundaries write FTruncDateBoundaries default True;
    property ShowMargins: Boolean read FShowMargins write FShowMargins default true;
    property ShowPrinterBounds: Boolean read FShowPrinterBounds write FShowPrinterBounds default true;
    property SeperatorWidth: integer read FSeperatorWidth write FSeperatorWidth default 5;
    property Title: WideString read GetTitle write FTitle Stored StoreTitle;
    property Units: TEzUnit read FUnits write FUnits;
    property PageSettings: TEzPageSettings read FPageSettings write SetPageSettings;
    property OnPrintProgress: TEzPrintProgressEvent read FOnPrintProgress write FOnPrintProgress;
  end;

  TEzPrintPreview = class(TCustomControl)
  protected
    FEzPrinter: TEzPrinter;
    FCurrentPage: TPageCoord;
    FScrollBars: TScrollStyle;
    FOutputScale: integer;
    FZoomFactor: integer;
    FPreviewPageRect: TRect;
    FShowShadow: Boolean;
    FShadowColor: TColor;
    FPaperColor: TColor;
    FScrollPos: TPoint;

    procedure CreateParams(var Params: TCreateParams); override;
    procedure Resize; override;
    procedure CalculatePageBounds;
    procedure SetEzPrinter(P: TEzPrinter);
    procedure SetPaperColor(Value: TColor);
    procedure SetScrollBars(Value: TScrollStyle);
    procedure SetShadowColor(Value: TColor);
    procedure SetShowShadow(Value: Boolean);
    procedure SetHorizontalPage(Value: Integer);
    procedure SetVerticalPage(Value: Integer);
    procedure SetZoomFactor(Value: Integer);
    procedure UpdateScrollRange;
    procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;

  public
    constructor Create(AOwner: TComponent); override;
    procedure Paint; override;

    property OutputScale: integer read FOutputScale;

  published
    property EzPrinter: TEzPrinter read FEzPrinter write SetEzPrinter;
    property ScrollBars: TScrollStyle read FScrollbars write SetScrollBars default ssBoth;
    property ShowShadow: Boolean read FShowShadow write SetShowShadow default true;
    property ShadowColor: TColor read FShadowColor write SetShadowColor default clBlack;
    property HorizontalPage: Integer read FCurrentPage.HPage write SetHorizontalPage;
    property PaperColor: TColor read FPaperColor write SetPaperColor default clWhite;
    property VerticalPage: Integer read FCurrentPage.VPage write SetVerticalPage;
    property ZoomFactor: integer read FZoomFactor write SetZoomFactor default 100;
  end;

implementation

uses EzStrings_3,
     math,
     Printers,
     EzPreview,
     EzPainting
{$IFDEF WIDESTRINGSUPPORTED}
     , WideStrUtils
{$ENDIF}
     ;

procedure EzPrinterError(const Message: string; EzPrinter: TEzPrinter = nil);
begin
  if Assigned(EzPrinter) then
    raise EEzPrinterError.Create(Format('%s: %s', [EzPrinter.Name, Message])) else
    raise EEzPrinterError.Create(Message);
end;

constructor TPrintRange.Create;
begin
  Reset(true, true);
end;

procedure TPrintRange.Reset(PageSettings, DateBounds: Boolean);
begin
  if PageSettings then
  begin
    PageSettingsOK := false;
    TopNode := nil;
    PageCoord.HPage := -1;
    PageCoord.VPage := -1;

    SetRect(HeaderRect,0,0,0,0);
    SetRect(PrintRect,0,0,0,0);
    SetRect(FooterRect,0,0,0,0);
  end;

  if DateBounds then
  begin
    DateBoundsOK := false;
    StartDate := 0;
    EndDate := 0;
  end;
end;

function TPrintRange.GetPrintHeight: integer;
begin
  Result := PrintRect.Bottom - PrintRect.Top;
end;

function TPrintRange.GetPrintWidth: integer;
begin
  Result := PrintRect.Right - PrintRect.Left;
end;

constructor TEzPageSettings.Create(P: TEzPrinter);
begin
  FPrinter := P;
  SetRect(FMargin, 1250, 1250, 1250, 1250);
  FSize.cx := 21000;
  FSize.cy := 29700;
end;

procedure TEzPageSettings.Assign(Source: TPersistent);
begin
  if Source is TEzPageSettings then
  begin
    FPrinter := (Source as TEzPageSettings).FPrinter;
    FMargin := (Source as TEzPageSettings).FMargin;
    FSize := (Source as TEzPageSettings).FSize;
  end else
    inherited Assign(Source);
end;

function TEzPageSettings.GetMargin(Index: integer) : integer;
begin
  case index of
    0: Result := Floor(FPrinter.ToUnit(FMargin.Left, FPrinter.Units));
    1: Result := Floor(FPrinter.ToUnit(FMargin.Top, FPrinter.Units));
    2: Result := Floor(FPrinter.ToUnit(FMargin.Right, FPrinter.Units));
    3: Result := Floor(FPrinter.ToUnit(FMargin.Bottom, FPrinter.Units));
  else
    Result := 0;
  end;
end;

procedure TEzPageSettings.SetMargin(Index: integer; m: integer);
begin
  case index of
    0: FMargin.Left := FPrinter.ToNative(m, FPrinter.Units);
    1: FMargin.Top := FPrinter.ToNative(m, FPrinter.Units);
    2: FMargin.Right := FPrinter.ToNative(m, FPrinter.Units);
    3: FMargin.Bottom := FPrinter.ToNative(m, FPrinter.Units);
  end;
end;

function TEzPageSettings.GetSize(Index: integer) : integer;
begin
  case index of
    0: Result := Floor(FPrinter.ToUnit(FSize.cx, FPrinter.Units));
    1: Result := Floor(FPrinter.ToUnit(FSize.cy, FPrinter.Units));
  else
    Result := 0;
  end;
end;

procedure TEzPageSettings.SetSize(Index: integer; s: integer);
begin
  case index of
    0: FSize.cx := FPrinter.ToNative(s, FPrinter.Units);
    1: FSize.cy := FPrinter.ToNative(s, FPrinter.Units);
  end;
end;

constructor TEzScaleToFitSettings.Create(P: TEzPrinter);
begin
  inherited Create;
  FPrinter := P;
  FHPages := -1;
  FVPages := -1;
  FScaleStep := 5;
  FScaleMin := 50;
  FScaleHoriz := 100;
  FScaleVert := 100;
end;

procedure TEzScaleToFitSettings.Assign(Source: TPersistent);
begin
  if Source is TEzScaleToFitSettings then
  begin
    FPrinter := (Source as TEzScaleToFitSettings).FPrinter;
    FHPages := (Source as TEzScaleToFitSettings).FHPages;
    FVPages := (Source as TEzScaleToFitSettings).FVPages;
    FScaleStep := (Source as TEzScaleToFitSettings).FScaleStep;
    FScaleMin := (Source as TEzScaleToFitSettings).FScaleMin;
    FScaleHoriz := (Source as TEzScaleToFitSettings).FScaleHoriz;
    FScaleVert := (Source as TEzScaleToFitSettings).FScaleVert;
  end else
    inherited Assign(Source);
end;

procedure TEzScaleToFitSettings.SetScaleStep(Value: Integer);
begin
  FScaleStep := min(max(5, Value), 50);
end;

procedure TEzScaleToFitSettings.SetScaleMin(Value: Integer);
begin
  FScaleMin := min(max(5, Value), 100);
  if FScaleMin>FScaleHoriz then
    ScaleHoriz := FScaleMin;
  if FScaleMin>FScaleVert then
    ScaleVert := FScaleMin;
end;

function TEzScaleToFitSettings.GetScale: Integer;
begin
  Result := min(FScaleHoriz, FScaleVert);
end;

procedure TEzScaleToFitSettings.SetScale(Value: Integer);
begin
  Self.SetScaleVert(Value);
  Self.SetScaleHoriz(Value);
end;

procedure TEzScaleToFitSettings.SetScaleHoriz(Value: Integer);
begin
  Value := max(5, Value);
  if Value<>FScaleHoriz then
  begin
    FScaleHoriz:=Value;
    FHPages := -1;
    FVPages := -1;
    FPrinter.PrintRange.Reset(true, true);
  end;
end;

procedure TEzScaleToFitSettings.SetScaleVert(Value: Integer);
begin
  Value := max(5, Value);
  if Value<>FScaleVert then
  begin
    FScaleVert := Value;
    FHPages := -1;
    FVPages := -1;
    FPrinter.PrintRange.Reset(true, true);
  end;
end;

procedure TEzScaleToFitSettings.SetHPages(Value: Integer);
begin
  if Value<>FHPages then
  begin
    FHPages:=Value;
    FScaleHoriz := 100;
    FScaleVert := 100;
    FPrinter.PrintRange.Reset(true, true);
  end;
end;

procedure TEzScaleToFitSettings.SetVPages(Value: Integer);
begin
  if Value<>FVPages then
  begin
    FVPages:=Value;
    FScaleHoriz := 100;
    FScaleVert := 100;
    FPrinter.PrintRange.Reset(true, true);
  end;
end;

constructor TEzLegendSettings.Create;
begin
  inherited;
  FControl := nil;
  FLocation := lcNone;
  FPosition := lpBottom;
  FSpace := 2;
end;

procedure TEzLegendSettings.Assign(Source: TPersistent);
begin
  if Source is TEzLegendSettings then
  begin
    FControl := TEzLegendSettings(Source).Control;
    FLocation := TEzLegendSettings(Source).Location;
    FPosition := TEzLegendSettings(Source).Position;
    FSpace := TEzLegendSettings(Source).Space;
  end else
    inherited;
end;

constructor TEzPrinter.Create(AOwner: TComponent);
begin
  inherited;
  FBorderSize := 2;
  FEdgeBorders := [ebLeft, ebTop, ebRight, ebBottom];
  FEdgePen := TPen.Create;
  FFooterFont := TFont.Create;
  FFooterHeight := 1000;
  FHeaderFont := TFont.Create;
  FHeaderHeight := 0;
  FScaleToFit := TEzScaleToFitSettings.Create(Self);
  FSeperatorWidth := 5;
  FPageSettings := TEzPageSettings.Create(Self);
  FLegend := TEzLegendSettings.Create;
  FPrintDate := 0;
  FPrintRange := TPrintRange.Create;
  FTruncDateBoundaries := True;
  FUnits := euMM;
  FShowMargins := true;
  FShowPrinterBounds := true;
  FCollate := true;
  FCopies := 1;
  FScrn_DpiX := 0;
  FScrn_DpiY := 0;

  FDebugInfo := TSTringList.Create;
end;

procedure TEzPrinter.CalcDateBounds(Range: TPrintRange);
const
  BigDate = MaxInt;

var
  i: integer;
  dtStart, dtStop: TDateTime;

begin
  if not Assigned(FPrintGantt) or (FPrintGantt.Timebar=nil) or not HasDataset then Exit;
  FEzTimebar := FPrintGantt.Timebar;

  Range.StartDate := BigDate;

  EzDataset.StartDirectAccess([cpShowAll]);
  try
    with FPrintGantt, EzDataset.ActiveCursor do
    begin
      // Direct Db access;
      // control db cursor through EzDataset cursor.
      MoveFirst;

      while not Eof do
      begin
        for i:=0 to Ganttbars.Count-1 do
        begin
          if CheckGanttbarVisibility(Ganttbars[i], False) and
             GetGanttbarDates(Ganttbars[i], dtStart, dtStop)
          then
            if (dtStart <> 0) and (dtStop <> 0) then
            begin
              Range.StartDate := min(Range.StartDate, dtStart);
              Range.EndDate := max(Range.EndDate, dtStop);
            end;
        end;

        MoveNext;
      end;
    end;
  finally
    EzDataset.EndDirectAccess;
  end;

  if Range.StartDate <> BigDate then
  begin
    if TruncDateBoundaries then
    begin
      // round dates
      with FEzTimebar do
      begin
        Range.StartDate := TruncDate(MajorScale, MajorCount, MajorOffset, Range.StartDate, MajorWeekStart);
        Range.StartDate := AddTicks(MajorScale, MajorCount, Range.StartDate, -1);
        if TruncDate(MajorScale, MajorCount, MajorOffset, Range.EndDate, MajorWeekStart) <> Range.EndDate then
          Range.EndDate := TruncDate(MajorScale, MajorCount, MajorOffset, AddTicks(MajorScale, MajorCount, Range.EndDate, 1), MajorWeekStart);
      end;
    end;
    Range.DateBoundsOK := true;
  end else
  begin
    with FEzTimebar do
      Range.StartDate := TruncDate(MajorScale, MajorCount, MajorOffset, Now);
    Range.EndDate := Range.StartDate;
    Range.DateBoundsOK := true;
//    EzPrinterError(SNoDateBounds, Self);
  end;
end;

procedure TEzPrinter.DoProgress(Step: TEzPrintProgressStep; Progress, MaxProgress: Integer);
var
  DoAbort: Boolean;

begin
  DoAbort := False; // Abort
  if Assigned(FOnPrintProgress) then
  begin
    FOnPrintProgress(Self, Step, Progress, MaxProgress, DoAbort);
    if DoAbort then
      Abort;
  end;
end;

destructor TEzPrinter.Destroy;
begin
  inherited;
  FEdgePen.Free;
  FFooterFont.Free;
  FHeaderFont.Free;
  FScaleToFit.Free;
  FPageSettings.Free;
  FPrintRange.Free;
  FLegend.Free;
  FDebugInfo.Free;
end;

function TEzPrinter.GetEzDataset: TEzDataset;
begin
  if not Assigned(FEzDataset) then
  begin
    if Assigned(PrintGrid) then
      FEzDataset := PrintGrid.DataSource.Dataset as TEzDataset
    else if Assigned(PrintGantt) then
      FEzDataset := PrintGantt.DataSource.Dataset as TEzDataset
    else
      EzPrinterError(SPrintComponent, Self);
  end;
  if not Assigned(FEzDataset) then
    EzPrinterError(SNoDataSet, Self);
  Result := FEzDataset;
end;

function TEzPrinter.GetFooterHeight: integer;
begin
  Result := Floor(ToUnit(FFooterHeight, Units));
end;

function TEzPrinter.GetHeaderHeight: integer;
begin
  Result := Floor(ToUnit(FHeaderHeight, Units));
end;

function TEzPrinter.GetTitle : WideString;
begin
  if FTitle = '' then
    Result := SDefaultTitle else
    Result := FTitle;
end;

function TEzPrinter.HasDataset: Boolean;
begin
  Result :=
      (FEzDataset<>nil) or
      ((PrintGrid<>nil) and (PrintGrid.DataSource.Dataset<>nil)) or
      ((PrintGantt<>nil) and (PrintGantt.DataSource.Dataset<>nil));
end;

procedure TEzPrinter.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);

  if (Operation = opRemove) then
  begin
    if AComponent = FPrintGrid then
      FPrintGrid := nil;
    if AComponent=FPrintGantt then
      FPrintGantt:=nil;
    if AComponent=Legend.FControl then
      Legend.Control:=nil;
  end;
end;

function TEzPrinter.ScaleValueHoriz(V: Integer): Integer;
begin
  Result := MulDiv(V, 100, ScaleToFit.ScaleHoriz);
end;

function TEzPrinter.ScaleValueVert(V: Integer): Integer;
begin
  Result := MulDiv(V, 100, ScaleToFit.ScaleVert);
end;

function TEzPrinter.StoreTitle: Boolean;
begin
  Result := Title <> SDefaultTitle;
end;

function TEzPrinter.CalcBoundRect(B: TRect; var OutputScale: Integer) : TRect;
var
  RectSize: TSize;

begin
  with PageSettings do
  begin
    RectSize.cx := ToNative(B.Right - B.Left, Units);
    RectSize.cy := ToNative(B.Bottom - B.Top, Units);

    if (PageSize.cx / RectSize.cx) > (PageSize.cy / RectSize.cy) then

      // Width is critical

    begin
      OutputScale := Floor((RectSize.cx * 100) / PageSize.cx);
      RectSize.cx := Floor(PageSize.cx * (OutputScale / 100));
      RectSize.cy := Floor(RectSize.cx * (PageSize.cy / PageSize.cx));
    end
    else

      // Height is critical

    begin
      OutputScale := Floor((RectSize.cy * 100) / PageSize.cy);
      RectSize.cy := Floor(PageSize.cy * (OutputScale / 100));
      RectSize.cx := Floor(RectSize.cy * (PageSize.cx / PageSize.cy));
    end;

    Result.Top := B.Top;
    Result.Left := B.Left;
    Result.Right := Result.Left + Floor(ToUnit(RectSize.cx, Units));
    Result.Bottom := Result.Top + Floor(ToUnit(RectSize.cy, Units));
  end;
end;

procedure TEzPrinter.CalcPrintSize(Range: TPrintRange);
var
  Page: TPageCoord;
  OutputFits: Boolean;
  DirectionHoriz, DirectionVert : string;

begin
  repeat
    DirectionHoriz := '';
    DirectionVert := '';

    Range.Pages.HPages := 0;
    Range.Pages.VPages := 0;
    Page.HPage := 0;
    Page.VPage := 0;
    Range.ExtraLegendPage := False;
    UpdatePrintRange(Range, Page);

    // Calculate number of horizontal pages
    while (Assigned(PrintGrid) and (Range.LeftColumn <> -1) and (Range.RightColumn < PrintGrid.Columns.Count-1))
           or
          (Assigned(PrintGantt) and (Range.PageEndDate < Range.EndDate)) do
    begin
      DoProgress(psInitialization, Page.HPage, -1);
      inc(Page.HPage);
      UpdatePrintRange(Range, Page);
    end;
    Range.Pages.HPages := Page.HPage+1;

    // Calculate number of vertical pages
    Page.HPage := 0;
    while Assigned(Range.TopNode) do
    begin
      DoProgress(psInitialization, Page.VPage, -1);
      inc(Page.VPage);
      UpdatePrintRange(Range, Page);
    end;

    if Range.ExtraLegendPage then
      inc(Page.VPage);

    Range.Pages.VPages := Page.VPage;

    if (ScaleToFit.VPages>0) and (ScaleToFit.HPages>0) then
    begin
      if DirectionHoriz = '' then
      begin
       if Range.Pages.HPages < ScaleToFit.HPages then
         DirectionHoriz := 'up'
       else
       if Range.Pages.HPages > ScaleToFit.HPages then
         DirectionHoriz := 'down';
      end;
      if DirectionVert = '' then
      begin
       if Range.Pages.VPages < ScaleToFit.VPages then
         DirectionVert := 'up'
       else
       if Range.Pages.VPages > ScaleToFit.VPages then
         DirectionVert := 'down';
      end;

      OutputFits := true;
      if DirectionHoriz = 'down' then
      begin
       if DirectionVert = 'down' then
         OutputFits := (Range.Pages.VPages=ScaleToFit.VPages) and (Range.Pages.HPages=ScaleToFit.HPages)
       else
         OutputFits := (Range.Pages.VPages>=ScaleToFit.VPages) and (Range.Pages.HPages=ScaleToFit.HPages);
      end
      else
      begin
       if DirectionVert = 'down' then
         OutputFits := (Range.Pages.VPages=ScaleToFit.VPages) and (Range.Pages.HPages>=ScaleToFit.HPages)
       else
         OutputFits := (Range.Pages.VPages>=ScaleToFit.VPages) and (Range.Pages.HPages>=ScaleToFit.HPages);
      end;

      //OutputFits := (Range.Pages.VPages=ScaleToFit.VPages) and (Range.Pages.HPages=ScaleToFit.HPages);
      if not OutputFits then
      begin
        DoProgress(psInitialization, max(ScaleToFit.ScaleHoriz, ScaleToFit.ScaleVert), -1);

        if (ScaleToFit.ScaleHoriz<=ScaleToFit.ScaleMin) or (ScaleToFit.ScaleVert<=ScaleToFit.ScaleMin) then
          EzPrinterError(Format(SCantFit, [ScaleToFit.HPages, ScaleToFit.VPages]), Self);

        if DirectionHoriz = 'down' then
        begin
         if Range.Pages.HPages > ScaleToFit.HPages then
           // Use FScale here to prevent resetting VPages and HPages
           ScaleToFit.FScaleHoriz := ScaleToFit.ScaleHoriz-ScaleToFit.ScaleStep;
        end
        else
        begin
         if Range.Pages.HPages < ScaleToFit.HPages then
           ScaleToFit.FScaleHoriz := ScaleToFit.ScaleHoriz+ScaleToFit.ScaleStep;
        end;

        if DirectionVert = 'down' then
        begin
         if Range.Pages.VPages > ScaleToFit.VPages then
           ScaleToFit.FScaleVert := ScaleToFit.ScaleVert-ScaleToFit.ScaleStep;
        end
        else
        begin
         if Range.Pages.VPages < ScaleToFit.VPages then
           ScaleToFit.FScaleVert := ScaleToFit.ScaleVert+ScaleToFit.ScaleStep;
        end;
      end;
    end else
      OutputFits := True;
  until OutputFits;
end;

procedure TEzPrinter.SetEdgePen(P: TPen);
begin
  FEdgePen.Assign(P);
end;

procedure TEzPrinter.SetScaleToFit(V: TEzScaleToFitSettings);
begin
  FScaleToFit.Assign(V);
end;

procedure TEzPrinter.SetFooterFont(F: TFont);
begin
  FFooterFont.Assign(F);
end;

procedure TEzPrinter.SetHeaderFont(F: TFont);
begin
  FHeaderFont.Assign(F);
end;

procedure TEzPrinter.SetLegend(Value: TEzLegendSettings);
begin
  FLegend.Assign(Value);
end;

procedure TEzPrinter.SetPageSettings(P: TEzPageSettings);
begin
  FPageSettings.Assign(P);
end;

procedure TEzPrinter.SetPrintGrid(Value: TCustomEzDBCoolGrid);
begin
  if Value = FPrintGrid then Exit;
{$IFDEF EZ_D5}
  if FPrintGrid <> nil then FPrintGrid.RemoveFreeNotification(Self);
{$ENDIF}
  FPrintGrid := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

procedure TEzPrinter.SetFooterHeight(H: integer);
begin
  FFooterHeight := ToNative(H, Units);
end;

procedure TEzPrinter.SetHeaderHeight(H: integer);
begin
  FHeaderHeight := ToNative(H, Units);
end;

function TEzPrinter.ToNative(val: Double; UnitIn: TEzUnit) : integer;
begin
  case UnitIn of
    euMM:
      Result := Round(val*100);
    euInches:
      Result := Round(val*2540);
    euPixels:
      Result := MulDiv(Round(val), 2540, Screen.PixelsPerInch);
    euNative:
      Result := Round(val);
    else
      Result := 0;
  end;
end;

function TEzPrinter.ToUnit(val: integer; UnitOut: TEzUnit) : Double;
begin
  case UnitOut of
    euMM:
      Result := val/100;
    euInches:
      Result := val/2540;
    euPixels:
      Result := MulDiv(val, Screen.PixelsPerInch, 2540);
    euNative:
      Result := val;
    else
      Result := 0;
  end;
end;

function TEzPrinter.TransForm(val: Double; UnitIn, UnitOut: TEzUnit) : Double;
begin
  Result := ToUnit(ToNative(val, UnitIn), UnitOut);
end;

procedure TEzPrinter.UpdatePrintRange(Range: TPrintRange; Page: TPageCoord);

  procedure CalcPrintRect(ThePage: TPageCoord);
  var
    ReserveLegend: Boolean;

  begin
    with Range do
    begin
      EdgeRect := PageBounds;

      if HeaderRect.Bottom<>HeaderRect.Top then
        EdgeRect.Top := HeaderRect.Bottom + 1;

      if FooterRect.Top<>FooterRect.Bottom then
        EdgeRect.Bottom := FooterRect.Top - 1;

      PrintRect := EdgeRect;

      if ebLeft in EdgeBorders then inc(PrintRect.Left, FBorderSize);
      if ebTop in EdgeBorders then inc(PrintRect.Top, FBorderSize);
      if ebRight in EdgeBorders then dec(PrintRect.Right, FBorderSize);
      if ebBottom in EdgeBorders then dec(PrintRect.Bottom, FBorderSize);

      if (Legend.Control<>nil) and (Legend.Location<>lcNone) then
      begin
        PrintLegend :=
          ((Legend.Location=lcFirstPage) and (ThePage.HPage=0) and (ThePage.VPage=0))
          or
          (Legend.Location=lcEveryPage)
          or
          ((Legend.Location=lcLastPage) and (Range.Pages.HPages<>0) and (ThePage.HPage=Range.Pages.HPages-1) and (ThePage.VPage=Range.Pages.VPages-1));

        if not PrintLegend then
        begin
          if (Legend.Position in [lpBottom, lpTop]) then
            ReserveLegend :=
             ((Legend.Location=lcFirstPage) and (ThePage.VPage=0))
             or
             ((Legend.Location=lcLastPage) and (Range.Pages.VPages<>0) and (ThePage.VPage=Range.Pages.VPages-1))
          else {(Legend.Position in [lpLeft, lpRight])}
            ReserveLegend :=
             ((Legend.Location=lcFirstPage) and (ThePage.HPage=0))
             or
             ((Legend.Location=lcLastPage) and (Range.Pages.HPages<>0) and (ThePage.HPage=Range.Pages.HPages-1));
        end else
          ReserveLegend := True;

        //
        // Update legend rectangle
        //
        if ReserveLegend then
        begin
          LegendRect := EdgeRect;

          case Legend.Position of
            lpTop:
            begin
              inc(EdgeRect.Top, Legend.Control.Height+Legend.Space);
              inc(PrintRect.Top, Legend.Control.Height+Legend.Space);
              LegendRect.Bottom := EdgeRect.Top-Legend.Space;
            end;
            lpBottom:
            begin
              dec(EdgeRect.Bottom, Legend.Control.Height+Legend.Space);
              dec(PrintRect.Bottom, Legend.Control.Height+Legend.Space);
              LegendRect.Top := EdgeRect.Bottom+Legend.Space;
            end;
            lpLeft:
            begin
              inc(EdgeRect.Left, Legend.Control.Width+Legend.Space);
              inc(PrintRect.Left, Legend.Control.Width+Legend.Space);
              LegendRect.Right := EdgeRect.Left-Legend.Space;
            end;
            lpRight:
            begin
              dec(EdgeRect.Right, Legend.Control.Width+Legend.Space);
              dec(PrintRect.Right, Legend.Control.Width+Legend.Space);
              LegendRect.Left := EdgeRect.Right+Legend.Space;
            end;
          end;
        end;
      end;
    end; {with Range do}
  end;

  procedure CalcHorizontalRange;
  var
    cnt, pos, w, col, pg, GridLineWidth, ScaledPageWidth: Integer;
    SepAdded, cont: Boolean;
    LeftDate: TDateTime;

  begin
    CalcPrintRect(Page);

    // Does Range have cached information stored ?
    if (Range.PageCoord.HPage >= 0) and (Range.PageCoord.HPage < Page.HPage) then
    begin
      pg := Range.PageCoord.HPage+1;

      col := 0;
      if Range.RightColumn <> -1 then
        col := Range.RightColumn+1
      else if Assigned(PrintGrid) then
        col := PrintGrid.Columns.Count;

      if (Range.PageEndDate <> 0) and Assigned(FEzTimebar) then
        LeftDate := AddTicks(FEzTimebar.MajorScale, FEzTimebar.MajorCount, Range.PageEndDate, 1) else
        LeftDate := Range.StartDate;
    end else
    begin
      pg:=0;
      Range.PageCoord.HPage := 0;
      col:=0;
      LeftDate := Range.StartDate;
    end;

    SepAdded := false;
    Range.LeftColumn := -1;
    Range.RightColumn := -1;
    Range.ShowSeperator := false;
    Range.GridWidth := 0;
    Range.TimebarWidth := 0;
    Range.PageStartDate := 0;
    Range.PageEndDate := 0;
    ScaledPageWidth := ScaleValueHoriz(Range.PrintWidth);

    if Assigned(PrintGrid) and (dgColLines in PrintGrid.Options) then
      GridLineWidth := PrintGrid.GridLineWidth else
      GridLineWidth := 0;

    // Calculate columns and timebar cells that fit on pages left of HPage
    while pg <= Page.HPage do
    begin
      pos:=0;
      cont := true;

      if Assigned(PrintGrid) and (col < PrintGrid.Columns.Count) then
      begin
        Range.LeftColumn := col;

        if (col = 0) and (dgIndicator in PrintGrid.Options) then
          inc(pos, IndicatorWidth + GridLineWidth);

        while (col < PrintGrid.Columns.Count) and cont do
          if PrintGrid.Columns[col].Visible then
          begin
            w := PrintGrid.Columns[col].Width + GridLineWidth;

            if w>ScaledPageWidth then
              // a column is wider than the page, trim the widht to the page width
              w:=Range.PrintWidth-1;

            if (pos+w)<ScaledPageWidth then
            begin
              inc(pos, w);
              inc(col);
            end else
              cont := false;
          end else
            inc(col);

        Range.RightColumn := col-1;
        Range.GridWidth := pos;
      end
      else
      begin
        Range.LeftColumn := -1;
        Range.RightColumn := -1;
        Range.GridWidth := 0;
      end;

      // All columns fit on this page, try to fit seperator
      if (Range.RightColumn <> -1) and cont and not SepAdded then
      begin
        w := FSeperatorWidth;
        if (pos+w)<ScaledPageWidth then
        begin
          Range.ShowSeperator := true;
          inc(pos, w);
          SepAdded := true;
        end else
          cont := false;
      end else
        Range.ShowSeperator := false;

      // fill rest of the page with timebar cells
      if cont and Assigned(FEzTimebar) then
      begin
        cnt := (ScaledPageWidth-pos) div FEzTimebar.MajorWidth;
        if cnt>0 then
        begin
          Range.TimebarWidth := cnt*FEzTimebar.MajorWidth;
          Range.PageStartDate := LeftDate;
          Range.PageEndDate := AddTicks(
                  FEzTimebar.MajorScale,
                  FEzTimebar.MajorCount,
                  LeftDate,
                  cnt-1);
          LeftDate := AddTicks(FEzTimebar.MajorScale, FEzTimebar.MajorCount, Range.PageEndDate, 1);
        end;
      end;

      inc(pg);
    end;
  end;

  procedure CalcLinesPerPage(var LineHeight, LinesPerPage: Integer);
  begin
    if Assigned(PrintGrid) then
    begin
      if dgRowLines in PrintGrid.Options then
        LineHeight := PrintGrid.DefaultRowHeight+PrintGrid.GridLineWidth else
        LineHeight := PrintGrid.DefaultRowHeight;
      LinesPerPage := (ScaleValueVert(Range.PrintHeight)-PrintGrid.TitlebarHeight) div LineHeight;
    end
    else if Assigned(PrintGantt) then
    begin
      if gaoHorzLines in PrintGantt.Options then
        LineHeight := PrintGantt.DefaultRowHeight+PrintGantt.GridlineWidth else
        LineHeight := PrintGantt.DefaultRowHeight;
      LinesPerPage := (ScaleValueVert(Range.PrintHeight)-FEzTimebar.Height) div LineHeight;
    end;
  end;

  procedure CalcVerticalRange;
  var
    TitlebarHeight, LineHeight, LinesPerPage, pg: Integer;
    pc: TPageCoord;

  begin
    if (Range.PageCoord.VPage >= 0) and (Range.PageCoord.VPage = Page.VPage) then
      Exit;

    // Does Range have cached information stored ?
    if (Range.PageCoord.VPage >= 0) and (Range.PageCoord.VPage <= Page.VPage) then
    begin
      pg := Range.PageCoord.VPage;
    end
    else
    begin
      pg:=0;
      Range.PageCoord.VPage := 0;
      EzDataset.ActiveCursor.MoveFirst;
      Range.TopNode := EzDataset.ActiveCursor.CurrentNode;
    end;

    LinesPerPage := 0;

    // Loop to run through all pages, before the page to be printed
    while (pg<Page.VPage) do
    begin
      // Init printing rectangles
      pc.HPage := Page.HPage;
      pc.VPage := pg;
      CalcPrintRect(pc);
      CalcLinesPerPage(LineHeight, LinesPerPage);
      if (Range.TopNode<>EzDataset.ActiveCursor.EndNode) then
        Range.TopNode := EzDataset.ActiveCursor.Advance(Range.TopNode, LinesPerPage);
      inc(pg);
    end;

    // Init actual printing page
    CalcPrintRect(Page);
    CalcLinesPerPage(LineHeight, LinesPerPage);

    if Range.TopNode = EzDataset.ActiveCursor.EndNode then
    begin
      Range.TopNode := nil;
      Range.RecordCount := 0;
    end
    else if Assigned(Range.TopNode) then
    begin
      Range.RecordCount :=
        min(EzDataset.ActiveCursor.RecordCount-(Range.TopNode.RecNo-1), LinesPerPage);

      // Check if we need to add an extra page to provide space for the legend
      if
          // only do this when we are calculating printsize
          (Range.Pages.HPages=0) and
          // All records fit ont his page
          ((Range.TopNode.RecNo-1)+Range.RecordCount=EzDataset.ActiveCursor.RecordCount)
      then
      begin
        if (Legend.Control<>nil) and (Legend.Location=lcLastPage) and (Legend.Position in [lpBottom, lpTop]) then
        begin
          if PrintGrid<>nil then
             TitlebarHeight := PrintGrid.TitlebarHeight else
             TitlebarHeight := 0;

          // Does legend fit in empty space?
          if ScaleValueVert(Range.PrintHeight)-
             ScaleValueVert(Legend.Space)-
             TitlebarHeight-
             (Range.RecordCount*LineHeight) < ScaleValueVert(Legend.Control.Height)
          then
            Range.ExtraLegendPage := True;
        end;
      end;
    end;
  end;

  procedure GetPrinterBounds;
  begin
    with PageSettings, Range do
    begin
      SetRect(Range.PrinterBounds, 0, 0, 0, 0);

      if EzIsPrinting or ShowPrinterBounds then
        //
        // Calculate printer bounds
        //
      begin
        FPrinter_DpiX := GetDeviceCaps(Printer.Handle, LOGPIXELSX);
        FPrinter_DpiY := GetDeviceCaps(Printer.Handle, LOGPIXELSY);
        PagePhysicalWidth := GetDeviceCaps(Printer.Handle, PHYSICALWIDTH);
        PrinterBounds.Left := MulDiv(GetDeviceCaps(Printer.Handle, PHYSICALOFFSETX), FScrn_DpiX, FPrinter_DpiX);
        PrinterBounds.Right := PrinterBounds.Left + MulDiv(GetDeviceCaps(Printer.Handle, HORZRES), FScrn_DpiX, FPrinter_DpiX);

        PagePhysicalHeight := GetDeviceCaps(Printer.Handle, PHYSICALHEIGHT);
        PrinterBounds.Top := MulDiv(GetDeviceCaps(Printer.Handle, PHYSICALOFFSETY), FScrn_DpiY, FPrinter_DpiY);
        PrinterBounds.Bottom := PrinterBounds.Top + MulDiv(GetDeviceCaps(Printer.Handle, VERTRES), FScrn_DpiY, FPrinter_DpiY);
      end;

      if EzIsPrinting then
      begin
        PageBounds.Left := max(PrinterBounds.Left, MulDiv(Margins.Left, Screen.PixelsPerInch, 2540) - PrinterBounds.Left);
        PageBounds.Top := max(PrinterBounds.Top, MulDiv(Margins.Top, Screen.PixelsPerInch, 2540) - PrinterBounds.Top);
        PageBounds.Right := min(PrinterBounds.Right, MulDiv(PageSize.cx - Margins.Right, Screen.PixelsPerInch, 2540) - PrinterBounds.Left);
        PageBounds.Bottom := min(PrinterBounds.Bottom, MulDiv(PageSize.cy - Margins.Bottom, Screen.PixelsPerInch, 2540) - PrinterBounds.Top);
      end
      else
      begin
        PageBounds.Left := MulDiv(Margins.Left, Screen.PixelsPerInch, 2540);
        PageBounds.Top := MulDiv(Margins.Top, Screen.PixelsPerInch, 2540);
        PageBounds.Right := min(PrinterBounds.Right, MulDiv(PageSize.cx - Margins.Right, Screen.PixelsPerInch, 2540));
        PageBounds.Bottom := min(PrinterBounds.Bottom, MulDiv(PageSize.cy - Margins.Bottom, Screen.PixelsPerInch, 2540));
      end;

      HeaderRect := PageBounds;
      FooterRect := PageBounds;

      if FHeaderHeight > 0 then
        HeaderRect.Bottom := PageBounds.Top + MulDiv(FHeaderHeight, Screen.PixelsPerInch, 2540) else
        HeaderRect.Bottom := HeaderRect.Top;

      if FFooterHeight > 0 then
        FooterRect.Top := PageBounds.Bottom - MulDiv(FFooterHeight, Screen.PixelsPerInch, 2540) else
        FooterRect.Top := FooterRect.Bottom;
    end;
  end;

var
  hScreenContext: HDC;

begin
  if not HasDataset then Exit;
  if FScrn_DpiX=0 then
  begin
    hScreenContext := GetDC(0);
    try
      FScrn_DpiX := GetDeviceCaps(hScreenContext, LOGPIXELSX);
      FScrn_DpiY := GetDeviceCaps(hScreenContext, LOGPIXELSY);
    finally
      ReleaseDC(0, hScreenContext);
    end;
  end;

  if not Range.PageSettingsOK then
    with PageSettings do
    begin
      GetPrinterBounds;
      CalcPrintRect(Page);

      if (Range.PrintWidth < 100) or (Range.PrintHeight < 100) then
        EzPrinterError(SPageToSmall, Self);

      Range.PageSettingsOK := true;
    end;

  if not Range.DateBoundsOK then
    CalcDateBounds(Range);

  CalcHorizontalRange;
  CalcVerticalRange;

  Range.PageCoord := Page;
end;

procedure TEzPrinter.Print;
var
  Page: TPageCoord;
  cpy: Integer;
  Printed, CancelJob: Boolean;

  function PageInRange : Boolean;
  var
    i, s, p, start, stop, pgnum: integer;
    part: string;

  begin
    if FPages = '' then
      Result := true
    else
    begin
      Result := false;
      s := 1;
      i := 0;
      pgnum := (Page.HPage+1) + (Page.VPage*PrintRange.Pages.HPages);

      while not Result and (i <= length(FPages)) do
      begin
        while (i <= Length(FPages)) and not IsDelimiter(';,', FPages, i) do
          inc(i);

        part := Copy(FPages, s, i-s);
        inc(i);
        s := i;
        p := Pos('-', part);
        if p <> 0 then
        begin
          start := StrToInt(trim(copy(part, 1, p-1)));
          stop := StrToInt(trim(copy(part, p+1, 1000)));
        end
        else
        begin
          start := StrToInt(trim(part));
          stop := start;
        end;

        Result := (pgnum >= start) and (pgnum <= stop);
      end;
    end;
  end;

  procedure NewPage(B: Boolean);
  begin
    if B then
    begin
      Printer.NewPage;
      Printed := false;
    end;
  end;

  procedure PrintThisPage;
  var
    MFile: TMetafile;
    MCanvas: TMetafileCanvas;

  begin
    DoProgress(psPrinting, (Page.HPage+1) * (Page.VPage+1),
        PrintRange.Pages.HPages * PrintRange.Pages.VPages);

    if DebugDir <> '' then
      FDebugInfo.Add('Printing page: ' + IntToStr((Page.HPage+1) * (Page.VPage+1)));

    NewPage(Printed);
    MFile := TMetafile.Create;

    try
      MFile.Enhanced := True;
      MFile.Width := PrintRange.PagePhysicalWidth;
      MFile.Height := PrintRange.PagePhysicalHeight;

      MCanvas := TMetafileCanvas.Create(MFile, 0); //Printer.Canvas.Handle);
      try
        SetMapMode(MCanvas.Handle, MM_ANISOTROPIC);
        SetWindowExtEx(MCanvas.Handle, FScrn_DpiX, FScrn_DpiY, nil);
        SetViewportExtEx(MCanvas.Handle, FPrinter_DpiX, FPrinter_DpiY, nil);

        PrintPageTo(MCanvas, PrintRange, Page, 0, 0);

        // Destroying the canvas will transfer the contents of the canvas
        // into the TMetaFile object.
        MCanvas.Destroy;
        MCanvas := nil;

        if FDebugDir <> '' then
          MFile.SaveToFile(IncludeTrailingBackslash(FDebugDir) + 'Page_' + IntToStr((Page.HPage+1) * (Page.VPage+1)) + '.emf');

        Printer.Canvas.Draw(0, 0, MFile);

        Printed := true;
      finally
        MCanvas.Free;
      end;
    finally
      MFile.Destroy;
    end;

    if DebugDir <> '' then
      FDebugInfo.Add('Page printed');
  end;

begin
  FDebugInfo.Clear;

  FPrinter_DpiX := GetDeviceCaps(Printer.Handle, LOGPIXELSX);
  FPrinter_DpiY := GetDeviceCaps(Printer.Handle, LOGPIXELSY);

  Printer.Title := Title;
  Printer.BeginDoc;

  try
    EzBeginPrint;

    // Recalculate page settings
    PrintRange.PageSettingsOK := false;
    if not PrintRange.DateBoundsOK then
      CalcPrintSize(PrintRange);

    try
      Printed := false;
      CancelJob := false;

      if Collate then
      begin
        cpy := 0;
        while cpy < Copies do
        begin
          Page.VPage := 0;
          while Page.VPage < PrintRange.Pages.VPages do
          begin
            Page.HPage := 0;
            while Page.HPage < PrintRange.Pages.HPages do
            begin
              if PageInRange then PrintThisPage;
              inc(Page.HPage);
            end;
            inc(Page.VPage);
          end;
          inc(cpy);
        end;
      end

      else
        //
        // Not Collating
        //
      begin
        Page.VPage := 0;
        while Page.VPage < PrintRange.Pages.VPages do
        begin
          Page.HPage := 0;
          while Page.HPage < PrintRange.Pages.HPages do
          begin
            cpy := 0;
            while cpy < Copies do
            begin
              if PageInRange then PrintThisPage;
              inc(cpy);
            end;
            inc(Page.HPage);
          end;
          inc(Page.VPage);
        end;
      end;

      if Printed and not CancelJob then
        Printer.EndDoc else
        Printer.Abort;

    except
      Printer.Abort;
      raise;
    end;
  finally
    PrintRange.PageSettingsOK := false;
    EzEndPrint;
    EzDataset.First;
    if FDebugDir <> '' then
      FDebugInfo.SaveToFile(IncludeTrailingBackslash(FDebugDir) + 'DebugInfo.txt');
  end;
end;

procedure TEzPrinter.Preview;
begin
  if not Assigned(FPreviewForm) then
    FPreviewForm := TfrmPrintPreview.Create(Self);

  with FPreviewForm as TfrmPrintPreview do
  begin
    EzPrinter := Self; // this will reset preview window
    ShowModal;
  end;
end;

procedure TEzPrinter.PrintPageTo(ACanvas: TCanvas; Range: TPrintRange; Page: TPageCoord; X,Y: Integer);
const
  TextFlags = DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX;

var
  HiddenCols: TList;
  ResetIndicator: Boolean;
  Flags: LParam;
  Pos, Offset: TPoint;
  WindowSize, ViewSize: TSize;
  MapMode: Integer;
  P1, P2: TPoint;

  function WasHidden(Index: integer) : Boolean;
  var
    i: integer;
  begin
    Result := False;
    i:=0;
    while not Result and (i < HiddenCols.Count) do
    begin
      if integer(HiddenCols[i]) = Index then
        Result := true;
      inc(i);
    end;
  end;

  procedure PrintMargins;
  begin
    with ACanvas, Range do
    begin
      Pen.Style := psDash;
      Pen.Color := clBlue;
      // Left
      MoveTo(PageBounds.Left-1, PrinterBounds.Top);
      LineTo(PageBounds.Left-1, PrinterBounds.Bottom);
      // top
      MoveTo(PrinterBounds.Left, PageBounds.Top-1);
      LineTo(PrinterBounds.Right, PageBounds.Top-1);
      // right
      MoveTo(PageBounds.Right+1, PrinterBounds.Top);
      LineTo(PageBounds.Right+1, PrinterBounds.Bottom);
      // bottom
      MoveTo(PrinterBounds.Left, PageBounds.Bottom+1);
      LineTo(PrinterBounds.Right, PageBounds.Bottom+1);
    end;
  end;

  procedure PrintEdges;
  var
    rCpy: TRect;
  begin
    with ACanvas, Range do
    begin
      Pen := EdgePen;

      rCpy := EdgeRect;
      OffsetRect(rCpy, Offset.X, Offset.Y);

      if ebLeft in EdgeBorders then
      begin
        MoveTo(rCpy.Left, rCpy.Top);
        LineTo(rCpy.Left, rCpy.Bottom);
      end;

      if ebTop in EdgeBorders then
      begin
        MoveTo(rCpy.Left, rCpy.Top);
        LineTo(rCpy.Right, rCpy.Top);
      end;

      if ebRight in EdgeBorders then
      begin
        MoveTo(rCpy.Right, rCpy.Top);
        LineTo(rCpy.Right, rCpy.Bottom);
      end;

      if ebBottom in EdgeBorders then
      begin
        MoveTo(rCpy.Left, rCpy.Bottom);
        LineTo(rCpy.Right, rCpy.Bottom);
      end;
    end;
  end;

  function FormatText(const Text: WideString) : WideString;
  begin
    Result := WideStringReplace(Text, '%t', Title, [rfReplaceAll, rfIgnoreCase]);
    Result := WideStringReplace(Result, '%#', IntToStr((Page.HPage+1) + (Page.VPage*Range.Pages.HPages)), [rfReplaceAll, rfIgnoreCase]);
    Result := WideStringReplace(Result, '%c', IntToStr(Range.Pages.HPages * Range.Pages.VPages), [rfReplaceAll, rfIgnoreCase]);
    Result := WideStringReplace(Result, '%h#', IntToStr(Page.HPage+1), [rfReplaceAll, rfIgnoreCase]);
    Result := WideStringReplace(Result, '%v#', IntToStr(Page.VPage+1), [rfReplaceAll, rfIgnoreCase]);
    Result := WideStringReplace(Result, '%d', DateTimeToStr(FPrintDate), [rfReplaceAll, rfIgnoreCase]);
  end;

  procedure PrintFooter;
  var
    rCpy: TRect;

  begin
    with Range do
    begin
      ACanvas.Font := FFooterFont;

      if Length(FooterLeft)>0 then
      begin
        rCpy := FooterRect;
        OffsetRect(rCpy, Offset.X, Offset.Y);
        EzRenderText(ACanvas, rCpy, FormatText(FooterLeft), tsNormal, TextFlags or DT_LEFT, false);
      end;

      if Length(FooterCenter)>0 then
      begin
        rCpy := FooterRect;
        OffsetRect(rCpy, Offset.X, Offset.Y);
        EzRenderText(ACanvas, rCpy, FormatText(FooterCenter), tsNormal, TextFlags or DT_CENTER, false);
      end;

      if Length(FooterRight)>0 then
      begin
        rCpy := FooterRect;
        OffsetRect(rCpy, Offset.X, Offset.Y);
        EzRenderText(ACanvas, rCpy, FormatText(FooterRight), tsNormal, TextFlags or DT_RIGHT, false);
      end;
    end;
  end;

  procedure PrintHeader;
  var
    rCpy: TRect;

  begin
    with Range do
    begin
      ACanvas.Font := FHeaderFont;

      if Length(HeaderLeft)>0 then
      begin
        rCpy := HeaderRect;
        OffsetRect(rCpy, Offset.X, Offset.Y);
        EzRenderText(ACanvas, rCpy, FormatText(HeaderLeft), tsNormal, TextFlags or DT_LEFT, false);
      end;

      if Length(HeaderCenter)>0 then
      begin
        rCpy := HeaderRect;
        OffsetRect(rCpy, Offset.X, Offset.Y);
        EzRenderText(ACanvas, rCpy, FormatText(HeaderCenter), tsNormal, TextFlags or DT_CENTER, false);
      end;

      if Length(HeaderRight)>0 then
      begin
        rCpy := HeaderRect;
        OffsetRect(rCpy, Offset.X, Offset.Y);
        EzRenderText(ACanvas, rCpy, FormatText(HeaderRight), tsNormal, TextFlags or DT_RIGHT, false);
      end;
    end;
  end;

  procedure PrintGridPanel;
  var
    i: integer;

  begin
    //
    // Prepare grid for printing by hiding all invisible columns
    //
    PrintGrid.Columns.BeginUpdate;

    //
    // Resize grid
    //
    PrintGrid.Width := Range.GridWidth;
    PrintGrid.Height := ScaleValueVert(Range.PrintHeight);

    //
    // Hide invisible columns
    //
    if (Range.LeftColumn > 0) and (dgIndicator in PrintGrid.Options) then
    begin
      ResetIndicator := true;
      PrintGrid.Options := PrintGrid.Options - [dgIndicator]
    end else
      ResetIndicator := false;

    HiddenCols := TList.Create;
    for i:=0 to PrintGrid.Columns.Count-1 do
      with PrintGrid.Columns[i] do
        if not Visible then
          HiddenCols.Add(Pointer(i))
        else if (i < Range.LeftColumn) or (i > Range.RightColumn) then
          Visible := false;

    PrintGrid.Columns.EndUpdate;

    //
    // Print the grid
    //
    if DebugDir <> '' then
      FDebugInfo.Add('Relocating Grid TopNode to: ' + string(Range.TopNode.Key.Value));

    if Range.TopNode <> nil then
      FEzDataset.TopNode := Range.TopNode else
      FEzDataset.TopNode := FEzDataset.ActiveCursor.EndNode;

    if DebugDir <> '' then
      FDebugInfo.Add('TopNode located at: ' + string(FEzDataset.TopNode.Key.Value));

    Flags := PRF_CLIENT;

    PrintGrid.PaintTo(ACanvas.Handle, Pos.X+Offset.X, Pos.Y+Offset.Y);

    inc(Pos.x, Range.GridWidth);

    //
    // Restore grid to it's original state
    //
    PrintGrid.Columns.BeginUpdate;
    for i:=0 to PrintGrid.Columns.Count-1 do
      if not WasHidden(i) then
        PrintGrid.Columns[i].Visible := true;

    HiddenCols.Destroy;

    if ResetIndicator then
      PrintGrid.Options := PrintGrid.Options + [dgIndicator];
    PrintGrid.Columns.EndUpdate;
  end;

  procedure PrintSeperatorLine;
  var
    R: TRect;
  begin
    with ACanvas do
    begin
      Brush.Style := bsSolid;
      Brush.Color := clBlack;

      R.Left := Pos.x+Offset.X;
      R.Top := Pos.y+Offset.Y;
      R.Right := R.Left+SeperatorWidth;
      R.Bottom := R.Top+ScaleValueVert(Range.PrintHeight);
      FillRect(R);
      inc(Pos.x, SeperatorWidth);
    end;
  end;

  procedure PrintGantt;
  var
    // Use of bitmap is required to prevent black
    // background when printing to PDF's
    bmp: Graphics.TBitmap;
    H: Integer;

  begin
    FEzTimebar.Width := Range.TimebarWidth;
    FEzTimebar.Date := Range.PageStartDate;
    FEzTimebar.PaintTo(ACanvas.Handle, Pos.X+Offset.X, Pos.Y+Offset.Y);

    H := FEzTimebar.Height;
    FEzTimebar.Height := ScaleValueVert(H);
    inc(Pos.y, H);
    FPrintGantt.Width := Range.TimebarWidth;
    FPrintGantt.Height := ScaleValueVert(Range.PrintHeight-H);

    if DebugDir <> '' then
      FDebugInfo.Add('Relocating Gantt TopNode to: ' + string(Range.TopNode.Key.Value));

    if Range.TopNode <> nil then
      FEzDataset.TopNode := Range.TopNode else
      FEzDataset.TopNode := FEzDataset.ActiveCursor.EndNode;

    if DebugDir <> '' then
      FDebugInfo.Add('TopNode located at: ' + string(FEzDataset.TopNode.Key.Value));

    // The ganttchart can only be printed to a bitmap first.
    // Painting the ganttchart on a printer canvas won't work because this is
    // write only. Also using a bitmap prevents black rects on PDF's
    bmp := Graphics.TBitmap.Create;
    try
      bmp.Width := FPrintGantt.Width;
      bmp.Height := FPrintGantt.Height;
      FPrintGantt.PaintTo(bmp.Canvas.Handle, 0, 0);
      PrintBitmap(ACanvas, Rect(Pos.X+Offset.X, Pos.Y+Offset.Y, bmp.Width+Pos.X+Offset.X, bmp.Height+Pos.Y+Offset.Y), bmp);
    finally
      bmp.Destroy;
    end;

    FEzTimebar.Height := H;
  end;

  procedure PrintLegend;
  begin
    if Legend.Position in [lpTop, lpBottom] then
      Legend.Control.Width := Range.LegendRect.Right-Range.LegendRect.Left else
      Legend.Control.Height := Range.LegendRect.Bottom-Range.LegendRect.Top;

    Legend.Control.PaintTo(ACanvas.Handle, Offset.X+Range.LegendRect.Left, Offset.Y+Range.LegendRect.Top);
  end;

  procedure UpdateViewportScale;
  var
    P: TPoint;

  begin
    if (ScaleToFit.ScaleVert<>100) or (ScaleToFit.ScaleHoriz<>100) then
    begin
      GetWindowExtEx(ACanvas.Handle, WindowSize);
      GetViewportExtEx(ACanvas.Handle, ViewSize);
      MapMode := SetMapMode(ACanvas.Handle, MM_ANISOTROPIC);
      SetWindowExtEx(ACanvas.Handle, WindowSize.cx*100, WindowSize.cy*100, nil);
      SetViewportExtEx(ACanvas.Handle, ViewSize.cx*ScaleToFit.ScaleHoriz, ViewSize.cy*ScaleToFit.ScaleVert, nil);
      GetViewportOrgEx(ACanvas.Handle, P);
      if P.X=P.y then;

      Offset := Point(X,Y);
      DPtoLP(ACanvas.Handle, Offset, 1);
      P1 := Pos;
      Pos.X := ScaleValueHoriz(Pos.x);
      Pos.Y := ScaleValueVert(Pos.y);
      P2 := Pos;
    end;
  end;

  procedure RestoreViewport;
  var
    P: TPoint;
  begin
    if (ScaleToFit.ScaleVert<>100) or (ScaleToFit.ScaleHoriz<>100) then
    begin
      SetMapMode(ACanvas.Handle, MapMode);
      SetWindowExtEx(ACanvas.Handle, WindowSize.cx, WindowSize.cy, nil);
      SetViewportExtEx(ACanvas.Handle, ViewSize.cx, ViewSize.cy, nil);
      GetViewportOrgEx(ACanvas.Handle, P);
      if P.X=P.y then;
      Offset := Point(X,Y);
      DPtoLP(ACanvas.Handle, Offset, 1);
      Pos.X := P1.X+P2.X-Pos.X;
      Pos.Y := P1.Y+P2.Y-Pos.Y;
    end;
  end;

begin
  UpdatePrintRange(Range, Page);

  Pos := Range.PrintRect.TopLeft;

  Offset := Point(X,Y);
  DPtoLP(ACanvas.Handle, Offset, 1);

  if not EzIsPrinting and ShowPrinterBounds then
    with ACanvas do
    begin
      Pen.Style := psDashDot;
      Pen.Color := clBlack;
      with Range.PrinterBounds do
        Rectangle(Left+Offset.X, Top+Offset.Y, Right+Offset.X, Bottom+Offset.Y);
    end;

  if not EzIsPrinting and ShowMargins then
    PrintMargins;

  if EdgeBorders <> [] then
    PrintEdges;

  if (HeaderHeight <> 0) then
    PrintHeader;

  if (FooterHeight <> 0) then
    PrintFooter;

  UpdateViewportScale;
  try
    if Assigned(PrintGrid) and (Range.LeftColumn <> -1) then
      PrintGridPanel;

    if Range.ShowSeperator then
      PrintSeperatorLine;

    if Range.PageStartDate <> 0 then
      PrintGantt;
  finally
    RestoreViewPort;
  end;

  if Range.PrintLegend then
    PrintLegend;
end;

constructor TEzPrintPreview.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Width := 210;
  Height := 297;
  FEzPrinter := nil;
  FZoomFactor:=100;
  FShowShadow := True;
  FShadowColor:=clBlack;
  FPaperColor:=clWhite;
  FScrollbars := ssBoth;
end;

procedure TEzPrintPreview.CreateParams(var Params: TCreateParams);
const
  ScrollBar: array[TScrollStyle] of Cardinal = (0, WS_HSCROLL, WS_VSCROLL, WS_HSCROLL or WS_VSCROLL);

begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or WS_CLIPCHILDREN or WS_CLIPSIBLINGS or ScrollBar[FScrollBars];
    WindowClass.style := CS_DBLCLKS;
  end;
end;

procedure TEzPrintPreview.Paint;
var
  CopyRect, ShadowRect: TRect;

begin
  Canvas.Pen.Color := clBlack;
  Canvas.Pen.Style := psSolid;
  Canvas.Brush.COlor := clWhite;

  CopyRect := FPreviewPageRect;
  OffsetRect(CopyRect, FScrollPos.X, FScrollPos.Y);

  if ShowShadow then
  begin
    ShadowRect:=CopyRect;
    if (ShadowRect.Right-ShadowRect.Left<50) or (ShadowRect.Bottom-ShadowRect.Top<50) then
      OffsetRect(ShadowRect, 2, 2) else
      OffsetRect(ShadowRect, 10, 10);
    Canvas.Brush.Color := ShadowColor;
    Canvas.FillRect(ShadowRect);
  end;

  Canvas.Brush.Color := PaperColor;
  Canvas.Rectangle(CopyRect);

  if FEzPrinter=nil then Exit;

  if SetMapMode(Canvas.Handle, MM_ANISOTROPIC) = 0 then
{$IFNDEF EZ_D6}
    RaiseLastWin32Error;
{$ELSE}
    RaiseLastOSError;
{$ENDIF}

  if not SetWindowExtEx(Canvas.Handle, 100, 100, nil) then
{$IFNDEF EZ_D6}
    RaiseLastWin32Error;
{$ELSE}
    RaiseLastOSError;
{$ENDIF}

  if not SetViewportExtEx(Canvas.Handle, OutputScale, OutputScale, nil) then
{$IFNDEF EZ_D6}
    RaiseLastWin32Error;
{$ELSE}
    RaiseLastOSError;
{$ENDIF}

  EzBeginPrint;
  try
    FEzPrinter.PrintPageTo(Canvas, EzPrinter.PrintRange, FCurrentPage, CopyRect.Left, CopyRect.Top);
  finally
    EzEndPrint;
  end;
end;

procedure TEzPrintPreview.Resize;
begin
  CalculatePageBounds;
  Invalidate;
end;

procedure TEzPrintPreview.CalculatePageBounds;
const
  Factor: array[1..5] of integer = (25, 50, 100, 200, 300);

var
  PageRect: TRect;
  VScrollInfo, HScrollInfo: TScrollInfo;
  W, H: Integer;

begin
  if FEzPrinter=nil then
  begin
    H := Height-20;
    W := Floor(H*200/297);
    FPreviewPageRect.Left := (Width div 2)-(W div 2);
    FPreviewPageRect.Right := FPreviewPageRect.Left+W;
    FPreviewPageRect.Top := 10;
    FPreviewPageRect.Bottom := H-10;
    Exit;
  end;

  FEzPrinter.Units := euPixels;

  FScrollPos.X := 0;
  FScrollPos.Y := 0;

  FillChar(HScrollInfo, SizeOf(TScrollInfo), 0);
  HScrollInfo.cbSize := SizeOf(TScrollInfo);
  FillChar(VScrollInfo, SizeOf(TScrollInfo), 0);
  VScrollInfo.cbSize := SizeOf(TScrollInfo);

  // Hide scrollbar
  HScrollInfo.fMask := SIF_ALL;
  HScrollInfo.nMax := 1;
  HScrollInfo.nPage := 10;
  HScrollInfo.nPos := 1;
  VScrollInfo.fMask := SIF_ALL;
  VScrollInfo.nMax := 1;
  VScrollInfo.nPage := 10;
  VScrollInfo.nPos := 1;

  if ZoomFactor=0 {Sacle to fit} then
  begin
    PageRect := ClientRect;
    InflateRect(PageRect, -20, -20);
    PageRect := FEzPrinter.CalcBoundRect(PageRect, FOutputScale);
    W:=PageRect.Right-PageRect.Left;
    H:=PageRect.Bottom-PageRect.Top;
    FPreviewPageRect.Left := Width div 2-W div 2;
    FPreviewPageRect.Right := FPreviewPageRect.Left+W;
    FPreviewPageRect.Top := Height div 2 - H div 2;
    FPreviewPageRect.Bottom := FPreviewPageRect.Top+H;
  end else
  begin
    PageRect.Left := 0;
    PageRect.Top := 0;
    PageRect.Right := MulDiv(EzPrinter.PageSettings.PageWidth, ZoomFactor, 100);
    PageRect.Bottom := MulDiv(EzPrinter.PageSettings.PageLength, ZoomFactor, 100);

    W:=PageRect.Right-PageRect.Left;
    H:=PageRect.Bottom-PageRect.Top;

    FPreviewPageRect.Left := max(20, Width div 2-W div 2);
    FPreviewPageRect.Right := FPreviewPageRect.Left+W;

    FPreviewPageRect.Top := max(20, Height div 2 - H div 2);
    FPreviewPageRect.Bottom := FPreviewPageRect.Top+H;

    FOutputScale := ZoomFactor;

    // Do we need a scrollbar?
    if (FPreviewPageRect.Left = 20) or (FPreviewPageRect.Top = 20) then
    begin
      if FScrollBars in [ssHorizontal,ssBoth] then
      begin
        HScrollInfo.nMax := FPreviewPageRect.Right+20;
        HScrollInfo.nPos := 1;
        HScrollInfo.nPage := Width;
      end;

      if FScrollBars in [ssVertical,ssBoth] then
      begin
        VScrollInfo.nMax := FPreviewPageRect.Bottom+20;
        VScrollInfo.nPos := 1;
        VScrollInfo.nPage := Height;
      end;
    end;
  end;

  SetScrollInfo(Self.Handle, SB_HORZ, HScrollInfo, true);
  SetScrollInfo(Self.Handle, SB_VERT, VScrollInfo, true);
end;

procedure TEzPrintPreview.SetEzPrinter(P: TEzPrinter);
begin
  FEzPrinter := P;
  CalculatePageBounds;
end;

procedure TEzPrintPreview.SetPaperColor(Value: TColor);
begin
  FPaperColor := Value;
  Invalidate;
end;

procedure TEzPrintPreview.SetScrollBars(Value: TScrollStyle);
begin
  if FScrollbars <> Value then
  begin
    FScrollBars := Value;
    with (Owner as TWinControl) do
      if not (csLoading in ComponentState) and HandleAllocated then
        RecreateWnd;
  end;
end;

procedure TEzPrintPreview.SetShowShadow(Value: Boolean);
begin
  FShowShadow := Value;
end;

procedure TEzPrintPreview.SetShadowColor(Value: TColor);
begin
  FShadowColor := Value;
  Invalidate;
end;

procedure TEzPrintPreview.SetHorizontalPage(Value: Integer);
begin
  if Value <> FCurrentPage.HPage then
  begin
    FCurrentPage.HPage := Value;
    Invalidate;
  end;
end;

procedure TEzPrintPreview.SetVerticalPage(Value: Integer);
begin
  if Value <> FCurrentPage.VPage then
  begin
    FCurrentPage.VPage := Value;
    Invalidate;
  end;
end;

procedure TEzPrintPreview.SetZoomFactor(Value: Integer);
begin
  Value := max(0, Value);
  if FZoomFactor<>Value then
  begin
    FZoomFactor:=Value;
    CalculatePageBounds;
//    UpdateScrollRange;
    Invalidate;
  end;
end;

procedure TEzPrintPreview.UpdateScrollRange;
var
  ScrollInfo: TScrollInfo;

begin
  FillChar(ScrollInfo, SizeOf(TScrollInfo), 0);
  ScrollInfo.cbSize := SizeOf(TScrollInfo);
  ScrollInfo.nMin := 0;
  ScrollInfo.nMax := FPreviewPageRect.Right+20;
  ScrollInfo.nPage := Width;
  ScrollInfo.fMask := SIF_PAGE or SIF_RANGE;
  ScrollInfo.nPos := 0;
  SetScrollInfo(Self.Handle, SB_HORZ, ScrollInfo, true);
end;

procedure TEzPrintPreview.WMHScroll(var Message: TWMHScroll);
var
  ScrollInfo: TScrollInfo;

begin
  case Message.ScrollCode of
    SB_LINEUP: FScrollPos.X := min(0, FScrollPos.X+10);
    SB_LINEDOWN: dec(FScrollPos.X, 10);
    SB_PAGEUP: FScrollPos.X := min(0, FScrollPos.X+100);
    SB_PAGEDOWN: dec(FScrollPos.X, 100);
    SB_THUMBTRACK, SB_THUMBPOSITION:
      FScrollPos.X := -Message.Pos;
    SB_BOTTOM:
      FScrollPos.X := FPreviewPageRect.Right+20;
    SB_TOP:
      FScrollPos.X := 0;
  end;

  FillChar(ScrollInfo, SizeOf(TScrollInfo), 0);
  ScrollInfo.cbSize := SizeOf(TScrollInfo);
  ScrollInfo.fMask := SIF_POS;
  ScrollInfo.nPos := -FScrollPos.X;
  SetScrollInfo(Self.Handle, SB_HORZ, ScrollInfo, true);
  Invalidate;
end;

procedure TEzPrintPreview.WMVScroll(var Message: TWMVScroll);
var
  ScrollInfo: TScrollInfo;

begin
  case Message.ScrollCode of
    SB_LINEUP: FScrollPos.Y := min(0, FScrollPos.Y+10);
    SB_LINEDOWN: dec(FScrollPos.Y, 10);
    SB_PAGEUP: FScrollPos.Y := min(0, FScrollPos.Y+100);
    SB_PAGEDOWN: dec(FScrollPos.Y, 100);
    SB_THUMBTRACK, SB_THUMBPOSITION:
      FScrollPos.Y := -Message.Pos;
    SB_BOTTOM:
      FScrollPos.Y := FPreviewPageRect.Bottom+20;
    SB_TOP:
      FScrollPos.Y := 0;
  end;

  FillChar(ScrollInfo, SizeOf(TScrollInfo), 0);
  ScrollInfo.cbSize := SizeOf(TScrollInfo);
  ScrollInfo.fMask := SIF_POS;
  ScrollInfo.nPos := -FScrollPos.Y;
  SetScrollInfo(Self.Handle, SB_VERT, ScrollInfo, true);

  Invalidate;
end;

end.

