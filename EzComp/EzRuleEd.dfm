object frmRule: TfrmRule
  Left = 438
  Top = 227
  Caption = 'Add rule'
  ClientHeight = 515
  ClientWidth = 547
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 8
    Top = 8
    Width = 30
    Height = 13
    Caption = 'Date: '
  end
  object lbDayDate: TLabel
    Left = 40
    Top = 8
    Width = 34
    Height = 13
    Caption = 'LbDate'
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 24
    Width = 521
    Height = 41
    Caption = 'Rule type'
    TabOrder = 0
    object rbWorking: TRadioButton
      Left = 8
      Top = 16
      Width = 105
      Height = 17
      Caption = '&Working period'
      TabOrder = 0
    end
    object rbNonWorking: TRadioButton
      Left = 160
      Top = 16
      Width = 137
      Height = 17
      Caption = '&Non working period'
      Checked = True
      TabOrder = 1
      TabStop = True
    end
  end
  object gbPeriod: TGroupBox
    Left = 8
    Top = 408
    Width = 521
    Height = 71
    Caption = 'Period'
    Enabled = False
    TabOrder = 1
    object Label8: TLabel
      Left = 266
      Top = 44
      Width = 10
      Height = 13
      Caption = 'till'
      FocusControl = dtpToTime
    end
    object dtpFromTime: TDateTimePicker
      Left = 176
      Top = 40
      Width = 81
      Height = 21
      Date = 36482.000000000000000000
      Time = 36482.000000000000000000
      Enabled = False
      Kind = dtkTime
      TabOrder = 0
    end
    object dtpToTime: TDateTimePicker
      Left = 288
      Top = 40
      Width = 81
      Height = 21
      Date = 36482.000000000000000000
      Time = 36482.000000000000000000
      Enabled = False
      Kind = dtkTime
      TabOrder = 1
    end
    object rbEntireDay: TRadioButton
      Left = 8
      Top = 16
      Width = 89
      Height = 17
      Caption = 'Entire day'
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = rbEntireDayClick
    end
    object rbInterval: TRadioButton
      Left = 8
      Top = 40
      Width = 161
      Height = 17
      Caption = 'During these hours of the day'
      TabOrder = 3
      OnClick = rbIntervalClick
    end
  end
  object btnOK: TButton
    Left = 275
    Top = 487
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object btnCancel: TButton
    Left = 194
    Top = 487
    Width = 75
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 72
    Width = 521
    Height = 225
    Caption = 'Recurrance pattern'
    TabOrder = 4
    object Bevel1: TBevel
      Left = 120
      Top = 16
      Width = 9
      Height = 201
      Shape = bsLeftLine
    end
    object rbNoRecur: TRadioButton
      Left = 8
      Top = 16
      Width = 110
      Height = 17
      Caption = 'No recurrance'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rbNoRecurClick
    end
    object rbRecurWeekly: TRadioButton
      Left = 8
      Top = 75
      Width = 110
      Height = 17
      Caption = 'Recurs weekly'
      TabOrder = 1
      OnClick = rbRecurWeeklyClick
    end
    object rbRecurMonthly: TRadioButton
      Left = 8
      Top = 108
      Width = 110
      Height = 17
      Caption = 'Recurs monthly'
      TabOrder = 2
      OnClick = rbRecurMonthlyClick
    end
    object rbRecurYearly: TRadioButton
      Left = 8
      Top = 168
      Width = 110
      Height = 17
      Caption = 'Recurs yearly'
      TabOrder = 3
      OnClick = rbRecurYearlyClick
    end
    object pnlMonth: TPanel
      Left = 136
      Top = 108
      Width = 376
      Height = 48
      AutoSize = True
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 4
      Visible = False
      object rbMonthPerDate: TRadioButton
        Left = 0
        Top = 3
        Width = 65
        Height = 17
        Caption = 'per date'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rbMonthPerDateClick
      end
      object rbMonthPerDay: TRadioButton
        Left = 0
        Top = 31
        Width = 65
        Height = 17
        Caption = 'per day'
        TabOrder = 1
        OnClick = rbMonthPerDateClick
      end
      object pnlMonthPerdayFields: TPanel
        Left = 64
        Top = 27
        Width = 312
        Height = 21
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object lbOfTheMonth: TLabel
          Left = 168
          Top = 4
          Width = 41
          Height = 13
          Caption = 'of every'
        end
        object Label5: TLabel
          Left = 272
          Top = 4
          Width = 40
          Height = 13
          Caption = 'month(s)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object cbMonthNthDay: TComboBox
          Left = 0
          Top = 0
          Width = 65
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          Items.Strings = (
            '1st'
            '2nd'
            '3rd'
            '4th'
            'last')
        end
        object cbMonthPerDayDaylist: TComboBox
          Left = 66
          Top = 0
          Width = 97
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          Items.Strings = (
            'Monday'
            'Tuesday'
            'Wednesday'
            'Thursday'
            'Friday'
            'Saturday'
            'Sunday')
        end
        object edMonthDayCount: TEdit
          Left = 216
          Top = 0
          Width = 33
          Height = 21
          TabOrder = 2
          Text = '1'
          OnChange = edWeekCountChange
        end
        object udMonthPerDayCount: TUpDown
          Left = 249
          Top = 0
          Width = 16
          Height = 21
          Associate = edMonthDayCount
          Min = 1
          Max = 51
          Position = 1
          TabOrder = 3
        end
      end
      object pnlMonthPerdateFields: TPanel
        Left = 64
        Top = 0
        Width = 233
        Height = 23
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 3
        object lbDayOfTheMonth: TLabel
          Left = 68
          Top = 4
          Width = 58
          Height = 13
          Caption = 'day of every'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 193
          Top = 4
          Width = 40
          Height = 13
          Caption = 'month(s)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object cbMonthPerDateDaynum: TComboBox
          Left = 0
          Top = 0
          Width = 65
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          Items.Strings = (
            '1st'
            '2nd'
            '3rd'
            '4th'
            '5th'
            '6th'
            '7th'
            '8th'
            '9th'
            '10th'
            '11th'
            '12th'
            '13th'
            '14th'
            '15th'
            '16th'
            '17th'
            '18th'
            '19th'
            '20th'
            '21st'
            '22nd'
            '23rd'
            '24th'
            '25th'
            '26th'
            '27th'
            '28th'
            '29th'
            '30th'
            '31th')
        end
        object edMonthDateCount: TEdit
          Left = 136
          Top = 2
          Width = 33
          Height = 21
          TabOrder = 1
          Text = '1'
          OnChange = edWeekCountChange
        end
        object udMonthPerDateCount: TUpDown
          Left = 169
          Top = 2
          Width = 16
          Height = 21
          Associate = edMonthDateCount
          Min = 1
          Max = 51
          Position = 1
          TabOrder = 2
        end
      end
    end
    object pnlWeek: TPanel
      Left = 136
      Top = 73
      Width = 285
      Height = 21
      AutoSize = True
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 5
      Visible = False
      object lbEvery: TLabel
        Left = 0
        Top = 4
        Width = 26
        Height = 13
        Caption = 'every'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 144
        Top = 4
        Width = 38
        Height = 13
        Caption = 'of every'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 248
        Top = 4
        Width = 37
        Height = 13
        Caption = 'week(s)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cbWeekDays: TComboBox
        Left = 39
        Top = 0
        Width = 97
        Height = 21
        Style = csDropDownList
        TabOrder = 0
        Items.Strings = (
          'Monday'
          'Tuesday'
          'Wednesday'
          'Thursday'
          'Friday'
          'Saturday'
          'Sunday')
      end
      object edWeekCount: TEdit
        Left = 191
        Top = 0
        Width = 33
        Height = 21
        TabOrder = 1
        Text = '1'
        OnChange = edWeekCountChange
      end
      object udWeekCount: TUpDown
        Left = 224
        Top = 0
        Width = 16
        Height = 21
        Associate = edWeekCount
        Min = 1
        Max = 51
        Position = 1
        TabOrder = 2
      end
    end
    object pnlYear: TPanel
      Left = 136
      Top = 168
      Width = 345
      Height = 49
      AutoSize = True
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 6
      Visible = False
      object rbYearPerDate: TRadioButton
        Left = 0
        Top = 2
        Width = 65
        Height = 17
        Caption = 'per date'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rbYearPerDateClick
      end
      object rbYearPerDay: TRadioButton
        Left = 0
        Top = 30
        Width = 57
        Height = 17
        Caption = 'per day'
        TabOrder = 1
        OnClick = rbYearPerDateClick
      end
      object pnlYearPerdayFields: TPanel
        Left = 64
        Top = 28
        Width = 281
        Height = 21
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 2
        Visible = False
        object cbYearNthDay: TComboBox
          Left = 0
          Top = 0
          Width = 65
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          Items.Strings = (
            '1st'
            '2nd'
            '3rd'
            '4th'
            'last')
        end
        object cbYearPerDayDaylist: TComboBox
          Left = 68
          Top = 0
          Width = 97
          Height = 21
          Style = csDropDownList
          TabOrder = 1
          Items.Strings = (
            'Monday'
            'Tuesday'
            'Wednesday'
            'Thursday'
            'Friday'
            'Saturday'
            'Sunday')
        end
        object cbYearPerDayMonthlist: TComboBox
          Left = 168
          Top = 0
          Width = 113
          Height = 21
          Style = csDropDownList
          TabOrder = 2
        end
      end
      object pnlYearPerdateFields: TPanel
        Left = 64
        Top = 0
        Width = 181
        Height = 21
        AutoSize = True
        BevelOuter = bvNone
        TabOrder = 3
        object cbYearPerDateDaynum: TComboBox
          Left = 0
          Top = 0
          Width = 65
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          Items.Strings = (
            '1st'
            '2nd'
            '3rd'
            '4th'
            '5th'
            '6th'
            '7th'
            '8th'
            '9th'
            '10th'
            '11th'
            '12th'
            '13th'
            '14th'
            '15th'
            '16th'
            '17th'
            '18th'
            '19th'
            '20th'
            '21st'
            '22nd'
            '23rd'
            '24th'
            '25th'
            '26th'
            '27th'
            '28th'
            '29th'
            '30th'
            '31th')
        end
        object cbYearPerDateMonthlist: TComboBox
          Left = 68
          Top = 0
          Width = 113
          Height = 21
          Style = csDropDownList
          TabOrder = 1
        end
      end
    end
    object pnlNoRecurrence: TPanel
      Left = 136
      Top = 14
      Width = 241
      Height = 45
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 7
      object Label2: TLabel
        Left = 0
        Top = 4
        Width = 22
        Height = 13
        Caption = 'from'
        FocusControl = dtpFromTime
      end
      object Label3: TLabel
        Left = 10
        Top = 28
        Width = 10
        Height = 13
        Caption = 'to'
        FocusControl = dtpToTime
      end
      object dtpNoRecurranceFromDate: TDateTimePicker
        Left = 32
        Top = 0
        Width = 121
        Height = 21
        Date = 37471.418747407400000000
        Time = 37471.418747407400000000
        TabOrder = 0
      end
      object dtpNoRecurranceToDate: TDateTimePicker
        Left = 32
        Top = 24
        Width = 121
        Height = 21
        Date = 37471.418747407400000000
        Time = 37471.418747407400000000
        TabOrder = 1
      end
      object dtpNoRecurranceFromTime: TDateTimePicker
        Left = 160
        Top = 0
        Width = 81
        Height = 21
        Date = 36482.000000000000000000
        Time = 36482.000000000000000000
        Kind = dtkTime
        TabOrder = 2
      end
      object dtpNoRecurranceToTime: TDateTimePicker
        Left = 160
        Top = 24
        Width = 81
        Height = 21
        Date = 36482.000000000000000000
        Time = 36482.000000000000000000
        Kind = dtkTime
        TabOrder = 3
      end
    end
  end
  object gbRangeOfRecurrance: TGroupBox
    Left = 8
    Top = 304
    Width = 521
    Height = 97
    Caption = 'Range of recurrance'
    Enabled = False
    TabOrder = 5
    object Panel1: TPanel
      Left = 8
      Top = 16
      Width = 217
      Height = 43
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 0
      object rbStartBy: TRadioButton
        Left = 0
        Top = 24
        Width = 57
        Height = 17
        Caption = 'Start by'
        TabOrder = 0
        OnClick = rbNoStartDateClick
      end
      object RangeStartDate: TDateTimePicker
        Left = 64
        Top = 22
        Width = 153
        Height = 21
        Date = 38695.405175057880000000
        Time = 38695.405175057880000000
        Enabled = False
        TabOrder = 1
      end
      object rbNoStartDate: TRadioButton
        Left = 0
        Top = 0
        Width = 89
        Height = 17
        Caption = 'No start date'
        Checked = True
        TabOrder = 2
        TabStop = True
        OnClick = rbNoStartDateClick
      end
    end
    object Panel2: TPanel
      Left = 248
      Top = 16
      Width = 225
      Height = 67
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 1
      object Label6: TLabel
        Left = 129
        Top = 26
        Width = 59
        Height = 13
        Caption = 'occurrences'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object rbNoEndDate: TRadioButton
        Left = 0
        Top = 0
        Width = 113
        Height = 17
        Caption = 'No end date'
        Checked = True
        TabOrder = 0
        TabStop = True
      end
      object rbEndAfter: TRadioButton
        Left = 0
        Top = 24
        Width = 65
        Height = 17
        Caption = 'End after'
        TabOrder = 1
        OnClick = rbEndAfterClick
      end
      object edOcurrences: TEdit
        Left = 72
        Top = 22
        Width = 33
        Height = 21
        Enabled = False
        TabOrder = 2
        Text = '10'
      end
      object udOcurrences: TUpDown
        Left = 105
        Top = 22
        Width = 16
        Height = 21
        Associate = edOcurrences
        Enabled = False
        Min = 1
        Max = 1000
        Position = 10
        TabOrder = 3
      end
      object rbEndBy: TRadioButton
        Left = 0
        Top = 48
        Width = 57
        Height = 17
        Caption = 'End by'
        TabOrder = 4
        OnClick = rbEndAfterClick
      end
      object RangeEndDate: TDateTimePicker
        Left = 72
        Top = 46
        Width = 153
        Height = 21
        Date = 38695.405175057880000000
        Time = 38695.405175057880000000
        Enabled = False
        TabOrder = 5
      end
    end
  end
end
