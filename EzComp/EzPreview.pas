unit EzPreview;

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Grids, ToolWin, ComCtrls, EzPrinter, EzPainting;

type
  EEzPreviewError = class(Exception);

  TfrmPrintPreview = class(TForm)
    ToolBar1: TToolBar;
    Panel1: TPanel;
    ToolButton3: TToolButton;
    Splitter1: TSplitter;
    pnlPageMap: TPanel;
    grPageMap: TStringGrid;
    Panel3: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    cbZoomFactor: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure grPageMapDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure pnlPageMapResize(Sender: TObject);
    procedure cbZoomFactorChange(Sender: TObject);
    procedure grPageMapSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ToolButton3Click(Sender: TObject);
  private
    PrintPreview: TEzPrintPreview;

  protected
    procedure LayoutPageMap;
    function  GetEzPrinter: TEzPrinter;
    procedure SetEzPrinter(P: TEzPrinter);

  public
    property EzPrinter: TEzPrinter read GetEzPrinter write SetEzPrinter;
  end;

var
  frmPrintPreview: TfrmPrintPreview;

implementation

{$R *.DFM}

uses Math;

procedure TfrmPrintPreview.FormCreate(Sender: TObject);
begin
  cbZoomFactor.ItemIndex := cbZoomFactor.Items.IndexOf('100%');

  PrintPreview := TEzPrintPreview.Create(Self);
  PrintPreview.Parent := Self;
  PrintPreview.Align := alClient;
end;

function TfrmPrintPreview.GetEzPrinter: TEzPrinter;
begin
  Result := PrintPreview.EzPrinter;
end;

procedure TfrmPrintPreview.SetEzPrinter(P: TEzPrinter);
begin
  PrintPreview.EzPrinter := P;
  LayoutPagemap;
end;

procedure TfrmPrintPreview.grPageMapDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  Text: string;

begin
  Text := IntToStr(ARow * grPageMap.ColCount + ACol + 1);

  grPageMap.Canvas.Font.Height := Rect.Bottom - Rect.Top - 2;
  EzRenderText(grPageMap.Canvas, Rect, Text, tsNormal, DT_CENTER or DT_VCENTER or DT_SINGLELINE);
end;

procedure TfrmPrintPreview.LayoutPageMap;
begin
  EzPrinter.CalcPrintSize(EzPrinter.PrintRange);
  grPageMap.ColCount := EzPrinter.PrintRange.Pages.HPages;
  grPageMap.RowCount := EzPrinter.PrintRange.Pages.VPages;
  grPageMap.DefaultColWidth := min(20,
    (grPageMap.ClientWidth - (grPageMap.ColCount * grPageMap.GridLineWidth)) div grPageMap.ColCount);
  grPageMap.DefaultRowHeight := min(20,
    (grPageMap.ClientHeight - (grPageMap.RowCount * grPageMap.GridLineWidth)) div grPageMap.RowCount);
end;

procedure TfrmPrintPreview.pnlPageMapResize(Sender: TObject);
begin
  pnlPageMap.Height := pnlPageMap.Width;
  LayoutPageMap;
end;

procedure TfrmPrintPreview.cbZoomFactorChange(Sender: TObject);
const
  Factor: array[1..5] of integer = (25, 50, 100, 200, 300);

begin
  if cbZoomFactor.ItemIndex = 0 then
    PrintPreview.ZoomFactor := 0 else
    PrintPreview.ZoomFactor := Factor[cbZoomFactor.ItemIndex];
end;

procedure TfrmPrintPreview.grPageMapSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  PrintPreview.HorizontalPage := ACol;
  PrintPreview.VerticalPage := ARow;
end;

procedure TfrmPrintPreview.ToolButton3Click(Sender: TObject);
begin
  Close;
end;

end.
