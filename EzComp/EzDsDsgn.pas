unit EzDsDsgn;

{ =============================================================================== }
{                            TEzDataset designer object                           }
{                                                                                 }
{ For compiling this file do the following:                                       }
{   BCB 4.0:                                                                      }
{       Add '-LUvcl40 -LUdcldb40' to the PFLAGS compiler options in               }
{       the *.bpk file.                                                           }
{   BCB 5.0:                                                                      }
{       Add '-LUvcl50 -LUdcldb50' to the PFLAGS compiler options in               }
{       the *.bpk file.                                                           }
{   BCB 6.0:                                                                      }
{       Add '-LUvcl -LUdcldb' to the PFLAGS compiler options in                   }
{       the *.bpk file.                                                           }
{ =============================================================================== }

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Grids, StdCtrls, Db, EzDataset,
  ExtCtrls, ImgList, ActnList, Menus, Buttons, math,
  ToolWin, EzColumnGrid, TypInfo,

{$IFDEF EZ_D6}
 DesignIntf, DesignWindows;
{$ELSE}
  LibIntf, Dsgnintf, DsgnWnds;
{$ENDIF}

resourcestring
  SNoDataset = 'No dataset selected';
  SNoDatalinks = 'You have to add one or more datalinks before editing the fieldmap.';
  SCouldNotGetDataSource = 'Could not retrieve datasource with given name.';

type
  TEzDatasetDesigner = class(TDesignWindow)
    pgPropPages: TPageControl;
    tsDatalinks: TTabSheet;
    tsFieldmap: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Panel4: TPanel;
    tvDatalinks: TTreeView;
    Label1: TLabel;
    cbKeyField: TComboBox;
    Label2: TLabel;
    cbParentField: TComboBox;
    Label3: TLabel;
    ImageList1: TImageList;
    btnAdd: TSpeedButton;
    btnMoveUp: TSpeedButton;
    btnMoveDown: TSpeedButton;
    btnDeleteLink: TSpeedButton;
    tsFields: TTabSheet;
    Panel10: TPanel;
    btnFieldEditor: TButton;
    Panel5: TPanel;
    bmpIcon: TImage;
    mmHelpText: TMemo;
    bmpParentChild: TImage;
    pnlButtons: TPanel;
    btnApply: TButton;
    btnUndo: TButton;
    mmHelpStrings: TMemo;
    tmSetHelptext: TTimer;
    cbDataSource: TComboBox;
    btnClose: TButton;
    Label6: TLabel;
    Panel8: TPanel;
    cbFieldmapData: TComboBox;
    SpeedButton1: TSpeedButton;
    lbFieldList: TListBox;
    btnUndoEdit: TSpeedButton;
    btnFillCells: TSpeedButton;
    chkCanExpand: TCheckBox;
    Panel6: TPanel;
    tvDetailFields: TTreeView;
    btnAddField: TButton;
    btnNewField: TButton;
    btnRemoveField: TButton;
    Label4: TLabel;
    lblEzDatasetFields: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure grFieldMapGetValue(Sender: TObject; ACol, ARow: Integer;
      var Value: Variant);
    procedure btnAddClick(Sender: TObject);
    procedure btnDeleteLinkClick(Sender: TObject);
    procedure btnMoveDownClick(Sender: TObject);
    procedure btnMoveUpClick(Sender: TObject);
    procedure tvDatalinksChange(Sender: TObject; Node: TTreeNode);
    procedure tvPreventCollapse(Sender: TObject; Node: TTreeNode;
      var AllowCollapse: Boolean);
    procedure cbDataSourceDropDown(Sender: TObject);
    procedure cbKeyFieldDropDown(Sender: TObject);
    procedure cbParentFieldDropDown(Sender: TObject);
    procedure cbDataSourceChange(Sender: TObject);
    procedure cbKeyFieldChange(Sender: TObject);
    procedure cbParentFieldChange(Sender: TObject);
    procedure grFieldMapDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure pgPropPagesChange(Sender: TObject);
    procedure grFieldMapCanEditShow(Sender: TObject; ACol, ARow: Integer;
      var DoShow: Boolean);
    procedure btnFieldEditorClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure grFieldMapSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure tmSetHelptextTimer(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnUndoClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cbFieldmapDataDropDown(Sender: TObject);
    procedure cbFieldmapDataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnUndoEditClick(Sender: TObject);
    procedure btnFillCellsClick(Sender: TObject);
    procedure chkCanExpandClick(Sender: TObject);
    procedure btnAddFieldClick(Sender: TObject);
    procedure btnRemoveFieldClick(Sender: TObject);
    procedure btnNewFieldClick(Sender: TObject);
    procedure lbFieldListClick(Sender: TObject);

  private
    UpdateCount: integer;
{$IFDEF EZ_D6}
    FDesigner: IDesigner;
{$ELSE}
    FDesigner: IFormDesigner;
{$ENDIF}
    FEzDataset: TEzDataset;
    FEzDatasetCopy: TEzDataset;
    FModified: Boolean;
    grFieldMap: TEzColumnGrid;

    function  GetCellValue(ACol, ARow: Integer) : string;
    function  GetDataSourceByName(Name: string) : TDataSource;
{$IFDEF EZ_D6}
    function  GetDesigner: IDesigner;
{$ELSE}
    function  GetDesigner: IFormDesigner;
{$ENDIF}
    function  GetEzDatalink: TEzDatalink;
    function  GetNodeType(ARow: Integer) : TNodeType;
    function  HasDesigner: Boolean;
    procedure InitDatalinksPage;
    procedure InitFieldsPage;
    procedure InitFieldmapPage;
    procedure LoadDatasets(List: TStrings);
    procedure LoadFieldNames(Name: string; List: TStrings);
    procedure LoadLinks;
    procedure LoadDetailFields;
    procedure LoadFieldsList;
    procedure SaveData;
    procedure SetModified(Value: Boolean);
    procedure UpdateProperties;
    procedure UpdateFieldMap;

  protected
    procedure Activated; override;

{$IFNDEF DSGNDEV}
    function  UniqueName(Component: TComponent): string; override;
{$ENDIF}
    procedure UpdateLinkValue;

  public
    destructor Destroy; override;

    procedure BeginUpdate;
    procedure EndUpdate;
    function  GetEditState: TEditState; override;

{$IFDEF EZ_D6}
    procedure DesignerClosed(const ADesigner: IDesigner; AGoingDormant: Boolean); override;
    function EditAction(Action: TEditAction) : Boolean; override;
{$ELSE}
    procedure EditAction(Action: TEditAction); override;
{$ENDIF}

    procedure InternalItemDeleted(AItem: TPersistent);

{$IFDEF EZ_D6}
    procedure ItemDeleted(const ADesigner: IDesigner; AItem: TPersistent); override;
    procedure ItemsModified(const Designer: IDesigner); override;
{$ELSE}
  {$IFNDEF DSGNDEV}
    procedure ComponentDeleted(Component: IPersistent); override;
  {$ENDIF}
{$ENDIF}

    property EzDatalink: TEzDatalink read GetEzDatalink;
{$IFDEF EZ_D6}
    property Designer: IDesigner read GetDesigner write FDesigner;
{$ELSE}
    property Designer: IFormDesigner read GetDesigner write FDesigner;
{$ENDIF}
    property EzDataset: TEzDataset read FEzDataset write FEzDataset;
    property Modified: Boolean read FModified write SetModified;
  end;

{$IFDEF EZ_D6}
  procedure ShowEzDatasetDesigner(Designer: IDesigner; ADataset: TCustomEzDataset);
{$ELSE}
  procedure ShowEzDatasetDesigner(Designer: IFormDesigner; ADataset: TCustomEzDataset);
{$ENDIF}

implementation

uses DSDesign, EzPainting

{$IFDEF WIDESTRINGSUPPORTED}
  , WideStrings
{$ENDIF}

{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
  ;
{$ENDIF}

{$R *.DFM}

{$IFDEF EZ_D6}
  procedure ShowEzDatasetDesigner(Designer: IDesigner; ADataset: TCustomEzDataset);
{$ELSE}
  procedure ShowEzDatasetDesigner(Designer: IFormDesigner; ADataset: TCustomEzDataset);
{$ENDIF}
var
  Editor: TEzDatasetDesigner;

begin
  Editor := TEzDatasetDesigner.Create(Application);

  Editor.Designer := Designer;
  Editor.EzDataset := ADataset as TEzDataset;
  Editor.Show;
end;

function AddPersistentField(Src, Dest: TDataset; FieldName: string; FieldIndex: Integer): Boolean;
var
  Field: TField;
  FieldDef: TFieldDef;

begin
  Result := False;
  if Dest.FindField(FieldName) = nil then
  begin
    // Clear default fields if this is the first
    // persistent field being added.
//    if Dest.Fields.Count = 0 then
//    begin
//      Dest.FieldDefs.Clear;
//      Dest.FieldDefs.Updated := False;
//    end;

    if Src.Fields.Count>0 then
    begin
      Field := Src.Fields[FieldIndex];
      CopyField(Dest.Owner, Dest, Field);
    end
    else
    begin
      FieldDef := Src.FieldDefs[FieldIndex];
      Field := FieldDef.CreateField(Dest, nil, FieldName, True);
      Field.Name := Dest.Name + FieldName;
      Field.DataSet := Dest;
    end;
    Result := True;
  end;
end;

procedure CopyFields(Source, Dest: TEzDataset);
var
  i: integer;
  Names: {$IFDEF WIDESTRINGSUPPORTED}TWideStrings{$ELSE}TStringList{$ENDIF};

begin
  Dest.Fields.Clear;
  if Source.Fields.Count = 0 then Exit;

  Names := {$IFDEF WIDESTRINGSUPPORTED}TWideStringList{$ELSE}TStringList{$ENDIF}.Create;
  try
    Source.GetFieldNames(Names);
    for i := 0  to Names.Count - 1 do
      AddPersistentField(Source, Dest, Names[i], i);
  finally
    Names.Free;
  end;
end;

procedure CopyDataset(Source, Dest: TEzDataset);
begin
  Dest.EzDatalinks.Assign(Source.EzDatalinks);
  CopyFields(Source, Dest);
  //  Dest.Fields.Assign(Source.Fields);
  Dest.FieldDefs.Updated := false;
end;

procedure UpdateFields(AOwner: TComponent; Source, Dest: TEzDataset);
var
  i: integer;
  Field: TField;
  Names: {$IFDEF WIDESTRINGSUPPORTED}TWideStrings{$ELSE}TStringList{$ENDIF};

begin
  // Do not clear destination fields in advance
  // This preserves Field.Name values;

  if Source.Fields.Count = 0 then
  begin
    Dest.Fields.Clear;
    Exit;
  end
  else
  begin
    Dest.FieldDefs.Clear;
    Dest.FieldDefs.Updated := False;
  end;

  Names := {$IFDEF WIDESTRINGSUPPORTED}TWideStringList{$ELSE}TStringList{$ENDIF}.Create;
  try
    Source.GetFieldNames(Names);
    for i := 0  to Names.Count - 1 do
    begin
      Field := Dest.FindField(Names[i]);
      if Field<>nil then
        CopyProperties(Source.Fields[i], Field) else
        AddPersistentField(Source, Dest, Names[i], i);
    end;
  finally
    Names.Free;
  end;

  // Delete fields from destination that no longer exist in source
  i := 0;
  while i < Dest.Fields.Count do
  begin
    if Source.FindField(Dest.Fields[i].FieldName) = nil then
      Dest.Fields[i].Free else
      inc(i);
  end;
end;

procedure UpdateDataset(Source, Dest: TEzDataset);
begin
  Dest.EzDatalinks.Assign(Source.EzDatalinks);
  UpdateFields(Dest.Owner, Source, Dest);
  //  Dest.Fields.Assign(Source.Fields);
  Dest.FieldDefs.Updated := false;
end;

//============================================================================
// General functions
//============================================================================
procedure TEzDatasetDesigner.Activated;
begin
; // Nothing to do
//  KV: 11 maart 2005
//  Following line removed since this raises an exception when used
//  in Delphi 2005
//  SetSelection(nil);
end;

procedure TEzDatasetDesigner.BeginUpdate;
begin
  inc(UpdateCount);
end;

procedure TEzDatasetDesigner.EndUpdate;
begin
  if UpdateCount > 0 then
    dec(UpdateCount);
end;

function TEzDatasetDesigner.GetEditState: TEditState;
begin
  if Assigned(ActiveControl) then
    Result := [esCanCopy, esCanCut, esCanPaste] else
    Result := [];
end;

{$IFDEF EZ_D6}
procedure TEzDatasetDesigner.DesignerClosed(const ADesigner: IDesigner; AGoingDormant: Boolean);
begin
  if ADesigner = Designer then
    Close;
end;
{$ENDIF}

{$IFDEF EZ_D6}
function TEzDatasetDesigner.EditAction(Action: TEditAction) : Boolean;
{$ELSE}
procedure TEzDatasetDesigner.EditAction(Action: TEditAction);
{$ENDIF}
begin
{$IFDEF EZ_D6}
  Result := True;
{$ENDIF}
  if not Assigned(ActiveControl) then Exit;
  case Action of
    eaCut: SendMessage(ActiveControl.Handle, WM_CUT, 0, 0);
    eaCopy: SendMessage(ActiveControl.Handle, WM_COPY, 0, 0);
    eaPaste: SendMessage(ActiveControl.Handle, WM_PASTE, 0, 0);
  end;
end;

procedure TEzDatasetDesigner.InternalItemDeleted(AItem: TPersistent);
begin
  if AItem = EzDataset then
  begin
    EzDataset := nil;
    Close;
  end
  else if (AItem is TField) and (TField(AItem).DataSet = EzDataSet) then begin
    if FEzDatasetCopy.FindField(TField(AItem).FieldName) <> nil then
      FEzDatasetCopy.FindField(TField(AItem).FieldName).Destroy;
    pgPropPagesChange(Self);
  end;
end;

procedure TEzDatasetDesigner.lbFieldListClick(Sender: TObject);
var
  Field: TField;

begin
  if HasDesigner and (FEzDatasetCopy.Fields.Count > 0) then
  begin
    Field := FEzDatasetCopy.Fields[lbFieldList.ItemIndex];
    Designer.SelectComponent(Field);
  end;
end;

{$IFDEF EZ_D6}
  procedure TEzDatasetDesigner.ItemDeleted(const ADesigner: IDesigner; AItem: TPersistent);
  begin
    inherited;
    InternalItemDeleted(AItem);
  end;
{$ELSE}
  {$IFNDEF DSGNDEV}
    procedure TEzDatasetDesigner.ComponentDeleted(Component: IPersistent);
    begin
      inherited;
      InternalItemDeleted(ExtractPersistent(Component));
    end;
  {$ENDIF}
{$ENDIF}

{$IFDEF EZ_D6}
procedure TEzDatasetDesigner.ItemsModified(const Designer: IDesigner);
var
  Selection: IDesignerSelections;
  Field: TField;
  i: Integer;

begin
  inherited;

  if not Assigned(EzDataset) then exit;
  Selection := CreateSelectionList;

  Designer.GetSelections(Selection);

  // Test if any of the fields held in dataset has changed
  for i:= 0 to Selection.Count-1 do
  begin
    if Selection[i] is TField then
    begin
      Field := Selection[i] as TField;
      if FEzDatasetCopy.Fields.IndexOf(Field)<>-1 then
      begin
        Modified := True;
        // Reloads list of fields
        LoadFieldsList;
        break;
      end;
    end;
  end;
end;
{$ENDIF}

procedure TEzDatasetDesigner.FormCreate(Sender: TObject);
begin
  // Hide helpstring memo (Visible = false does not work!!!)
  mmHelpStrings.Left := -100;
  UpdateCount := 0;
  FModified := false;
  FEzDatasetCopy := TEzDataset.Create(Self);
  FEzDatasetCopy.Name := 'InternalCopy_';

  grFieldmap := TEzColumnGrid.Create(Self);
  with grFieldmap do begin
    Parent := tsFieldmap;
    Align := alClient;
    OnCanEditShow := grFieldMapCanEditShow;
    OnDrawCell := grFieldmapDrawCell;
    OnGetValue := grFieldmapGetValue;
    OnSelectCell := grFieldmapSelectCell;

    DefaultRowHeight := 18;
    Flags := [igfShowWarnings, igfUseInsertCache];
//    Options := Options + [goEditing];

    with Columns.Add do begin
      Title := 'Dataset';
      Width := 80;
    end;
  end;
end;

procedure TEzDatasetDesigner.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Designer := nil;
  Release;
end;

procedure TEzDatasetDesigner.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := false;
  if Assigned(EzDataset) and Modified then
    case MessageDlg('Save changes?', mtConfirmation, mbYesNoCancel, 0) of
      mrYes: SaveData;
      mrNo: ;
      mrCancel: Exit;
    end;

  if HasDesigner then
    Designer.NoSelection;
  
  CanClose := true;
end;

procedure TEzDatasetDesigner.FormShow(Sender: TObject);
begin
  if Assigned(EzDataset) then
  try
    CopyDataset(EzDataset, FEzDatasetCopy);
  except
    EzDataset := nil;
    raise;
  end;

  pgPropPages.ActivePage := tsDatalinks;
  InitDatalinksPage;
  tmSetHelpText.Enabled := True;
end;

destructor TEzDatasetDesigner.Destroy;
begin
  EzDataset := nil;
  inherited;
end;

procedure TEzDatasetDesigner.tmSetHelptextTimer(Sender: TObject);
var
  wc: TWinControl;
begin
  wc := ActiveControl;
  while (wc <> nil) and (wc.Tag = 0) do
    wc := wc.Parent;

  if (wc <> nil) then
    mmHelpText.Lines.Text :=
      StringReplace(mmHelpStrings.Lines.Values['ID_' + IntToStr(wc.Tag)], '\n', #13, [rfReplaceAll, rfIgnoreCase]);
end;

function TEzDatasetDesigner.GetEzDatalink: TEzDatalink;
begin
  Result := nil;
  if (tvDataLinks.Selected <> nil) {and (tvDataLinks.Selected.Level = 1)} then
    Result := FEzDatasetCopy.EzDatalinks[tvDataLinks.Selected.Index];
end;

function TEzDatasetDesigner.GetDataSourceByName(Name: string) : TDataSource;
begin
  Result := nil;
  if HasDesigner then
    Result := Designer.GetComponent(Name) as TDataSource;
end;

{$IFDEF EZ_D6}
function TEzDatasetDesigner.GetDesigner: IDesigner;
begin
  Assert(HasDesigner);
  Result := FDesigner;
end;
{$ELSE}
function TEzDatasetDesigner.GetDesigner: IFormDesigner;
begin
  Assert(HasDesigner);
  Result := FDesigner;
end;
{$ENDIF}

procedure TEzDatasetDesigner.LoadDatasets(List: TStrings);
var
  sl: TStringlist;

begin
  if HasDesigner then
  begin
    sl := TStringlist.Create;
    try
      Designer.GetComponentNames(GetTypeData(TDataSource.ClassInfo), sl.Append);

      // Remove dataset being edited
      if Assigned(EzDataset) and (sl.IndexOf(EzDataset.Name) <> -1) then
        sl.Delete(sl.IndexOf(EzDataset.Name));

      List.Assign(sl);
    finally
      sl.Destroy;
    end;
  end;
end;

procedure TEzDatasetDesigner.LoadFieldNames(Name: string; List: TStrings);
var
  ds: TDataSource;

begin
  Cursor := crHourGlass;
  try
    ds := GetDataSourceByName(Name);
    if Assigned(ds) and Assigned(ds.Dataset) then
      ds.Dataset.GetFieldNames(List);
  finally
    Cursor := crDefault;
  end;
end;

procedure TEzDatasetDesigner.SetModified(Value: Boolean);
begin
  FModified := Value;
  btnApply.Enabled := Modified;
  btnUndo.Enabled := Modified;
end;

procedure TEzDatasetDesigner.btnApplyClick(Sender: TObject);
begin
  SaveData;
end;

procedure TEzDatasetDesigner.btnUndoClick(Sender: TObject);
begin
  if not Assigned(EzDataset) then exit;
  if MessageDlg('Undo changes?', mtConfirmation, mbOKCancel, 0) = mrOK then
  begin
    CopyDataset(EzDataset, FEzDatasetCopy);
    pgPropPagesChange(Self);
    Modified := false;
  end;
end;

procedure TEzDatasetDesigner.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TEzDatasetDesigner.SaveData;
begin
  if Modified and Assigned(EzDataset) then
  begin
    UpdateDataset(FEzDatasetCopy, EzDataset);
    Modified := false;
    try
      if HasDesigner then Designer.Modified;
    except
      Designer := nil;
      // ShowMessage('Exception when seting designers Modified state, data has been saved and exception ignored');
    end;
  end;
end;

{$IFNDEF DSGNDEV}
function TEzDatasetDesigner.UniqueName(Component: TComponent): string;
begin
  Result := 'NoName'
end;
{$ENDIF}

//============================================================================
// Datalinks page functions
//============================================================================
procedure TEzDatasetDesigner.LoadLinks;
var
  i: integer;
  CurrentNode: string;

  function FindNode(Text: string): TTreeNode;
  begin
    Result := tvDatalinks.Items[0];

    while (Result <> nil) do
    begin
      if Result.Text = Text then
        Exit;
      Result := Result.GetNext;
    end;
  end;

begin
  if Assigned(tvDatalinks.Selected) then
    CurrentNode := tvDatalinks.Selected.Text;

  tvDatalinks.Items.Clear;

  if FEzDatasetCopy.EzDatalinks.Count=0 then
  begin
    tvDatalinks.Enabled := False;
    tvDatalinks.Items.Add(nil, '<press ''+'' to add a datalink>');
    tvDatalinks.Items[0].ImageIndex := -1;
  end
  else
  begin
    tvDatalinks.Enabled := True;
    for i:=0 to FEzDatasetCopy.EzDatalinks.Count-1 do
    begin
      if Assigned(FEzDatasetCopy.EzDatalinks[i].DataSource) then
        tvDatalinks.Items.Add(nil, FEzDatasetCopy.EzDatalinks[i].DataSource.Name) else
        tvDatalinks.Items.Add(nil, '<select datasource>');
    end;

    if CurrentNode <> '' then
      tvDatalinks.Selected := FindNode(CurrentNode) else
      tvDatalinks.Selected := tvDatalinks.Items[0];
  end;
end;

procedure TEzDatasetDesigner.UpdateProperties;
begin
  BeginUpdate;
  try
    cbDataSource.Items.Clear;
    cbKeyField.Items.Clear;
    cbParentField.Items.Clear;
    cbDataSource.Enabled := EzDatalink <> nil;
    cbKeyField.Enabled := EzDatalink <> nil;
    cbParentField.Enabled := EzDatalink <> nil;
    chkCanExpand.Enabled := EzDatalink <> nil;

    if EzDatalink <> nil then begin
      if Assigned(EzDatalink.DataSource) then begin
        cbDataSource.Items.Add(EzDatalink.DataSource.Name);
        cbDataSource.ItemIndex := 0;
      end;
      cbKeyField.Text := FEzDatasetCopy.EzDatalinks[tvDataLinks.Selected.Index].KeyField;
      cbParentField.Enabled := (FEzDatasetCopy.EzDataLinks.Count = 1) or (tvDataLinks.Selected.Index > 0);
      cbParentField.Text := FEzDatasetCopy.EzDatalinks[tvDataLinks.Selected.Index].ParentRefField;
      chkCanExpand.Checked := FEzDatasetCopy.EzDatalinks[tvDataLinks.Selected.Index].CanExpand;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TEzDatasetDesigner.pgPropPagesChange(Sender: TObject);
begin
  if pgPropPages.ActivePage = tsDatalinks then
    InitDatalinksPage

  else if pgPropPages.ActivePage = tsFields then
    InitFieldsPage

  else if pgPropPages.ActivePage = tsFieldmap then
    InitFieldmapPage;
end;

procedure TEzDatasetDesigner.InitDatalinksPage;
begin
  bmpIcon.Visible := True;
  bmpParentChild.Visible := false;
  LoadLinks;
end;

procedure TEzDatasetDesigner.btnAddClick(Sender: TObject);
begin
  Modified := true;
  FEzDatasetCopy.EzDatalinks.Add;
  LoadLinks;
end;

procedure TEzDatasetDesigner.btnAddFieldClick(Sender: TObject);
var
  i, n: Integer;
  Dataset: TDataset;
  FieldName: string;
  FieldNames: TStringList;

//  function AddPersistentField(FieldName: string; Origin: TDataset; FieldIndex: Integer): Boolean;
//  var
//    Field: TField;
//    FieldDef: TFieldDef;
//
//  begin
//    Result := False;
//
//    if FEzDatasetCopy.FindField(FieldName) = nil then
//    begin
//      if Origin.Fields.Count>0 then
//      begin
//        Field := Origin.Fields[FieldIndex];
//        CopyField(Self, FEzDatasetCopy, Field);
//      end
//      else
//      begin
//        FieldDef := Origin.FieldDefs[FieldIndex];
//        Field := FieldDef.CreateField(Self, nil, FieldName, True);
//        Field.Name := FEzDataset.Name + FieldName;
//        Field.DataSet := FEzDatasetCopy;
//      end;
//      Result := True;
//    end;
//  end;

begin
  if (tvDetailFields.Selected = nil) then Exit;

  if tvDetailFields.Selected.Level = 0 then
  begin
    if MessageDlg('Copy all fields from selected dataset?', mtConfirmation, mbOkCancel, 0)<>idOK then Exit;

    Dataset := FEzDatasetCopy.EzDataLinks[tvDetailFields.Selected.Index].DetailDataset;
    FieldNames := TStringList.Create;
    try
      Dataset.GetFieldNames(FieldNames);
      n := 0;
      for i:=0 to FieldNames.Count-1 do
      begin
        if AddPersistentField(Dataset, FEzDatasetCopy, FieldNames[i], i) then
          inc(n);
      end;
    finally
      FieldNames.Free;
    end;

    if n>0 then
    begin
      Modified := True;
      LoadFieldsList;
    end;

    MessageDlg(Format('%d fields copied', [n]), mtInformation, [mbOk], 0);
  end
  else
  begin
    FieldName := tvDetailFields.Selected.Text;
    Dataset := FEzDatasetCopy.EzDataLinks[tvDetailFields.Selected.Parent.Index].DetailDataset;
    if AddPersistentField(DataSet, FEzDatasetCopy, FieldName, tvDetailFields.Selected.Index) then
    begin
      Modified := True;
      LoadFieldsList;
    end else
      MessageDlg(Format('Field with name %s already exists', [FieldName]), mtInformation, [mbOk], 0);
  end;
end;

procedure TEzDatasetDesigner.btnDeleteLinkClick(Sender: TObject);
begin
  Modified := true;
  FEzDatasetCopy.EzDatalinks[tvDatalinks.Selected.Index].Destroy;
  LoadLinks;
  tvDatalinksChange(nil, nil);  
end;

procedure TEzDatasetDesigner.btnMoveDownClick(Sender: TObject);
var
  i: integer;
begin
  Modified := true;
  i := tvDatalinks.Selected.Index;
  FEzDatasetCopy.EzDatalinks[i].Index := i+1;
  LoadLinks;
  tvDatalinks.Selected := tvDatalinks.Items[i+1];
end;

procedure TEzDatasetDesigner.btnMoveUpClick(Sender: TObject);
var
  i: integer;
begin
  Modified := true;
  i := tvDatalinks.Selected.Index;
  FEzDatasetCopy.EzDatalinks[i].Index := i-1;
  LoadLinks;
  tvDatalinks.Selected := tvDatalinks.Items[i-1];
end;

procedure TEzDatasetDesigner.btnNewFieldClick(Sender: TObject);
var
  FieldsEditor: TFieldsEditor;
  vShared: Boolean;

begin
  if not HasDesigner then Exit;
  FieldsEditor := CreateFieldsEditor(Designer, FEzDatasetCopy, TDSDesigner, vShared);
  if FieldsEditor <> nil then
  begin
    if FieldsEditor.DoNewField <> nil then
    begin
      Modified := True;
      LoadFieldsList;
    end;
    FieldsEditor.Free;
  end;
end;

procedure TEzDatasetDesigner.tvDatalinksChange(Sender: TObject; Node: TTreeNode);
begin
  btnDeleteLink.Enabled := (Node<>nil) and (FEzDatasetCopy.EzDatalinks.Count>0);
  btnMoveUp.Enabled := (Node<>nil) and (Node.Index > 0);
  btnMoveDown.Enabled := (Node<>nil) and (Node.Index < tvDatalinks.Items.Count-1);

  UpdateProperties;
end;

procedure TEzDatasetDesigner.tvPreventCollapse(Sender: TObject; Node: TTreeNode;
  var AllowCollapse: Boolean);
begin
  AllowCollapse := false;
end;

procedure TEzDatasetDesigner.cbDataSourceDropDown(Sender: TObject);
begin
  LoadDatasets(cbDataSource.Items);
  if Assigned(EzDatalink) and Assigned(EzDatalink.DataSource) then
    cbDataSource.ItemIndex := cbDataSource.Items.IndexOf(EzDatalink.DataSource.Name);
end;

procedure TEzDatasetDesigner.cbKeyFieldDropDown(Sender: TObject);
begin
  if cbDataSource.ItemIndex = -1 then
    raise Exception.Create(SNoDataset);

  LoadFieldNames(cbDataSource.Items[cbDataSource.ItemIndex], cbKeyField.Items);

  if EzDatalink <> nil then
    cbKeyField.Text := EzDatalink.KeyField;
end;

procedure TEzDatasetDesigner.cbParentFieldDropDown(Sender: TObject);
begin
  if cbDataSource.ItemIndex = -1 then
    raise Exception.Create(SNoDataset);
  LoadFieldNames(cbDataSource.Items[cbDataSource.ItemIndex], cbParentField.Items);
  if EzDatalink <> nil then
    cbParentField.Text := EzDatalink.ParentRefField;
end;

procedure TEzDatasetDesigner.cbDataSourceChange(Sender: TObject);
var
  ds: TDataSource;

begin
  if Assigned(EzDatalink) and (cbDataSource.ItemIndex <> - 1) then
  begin
    Modified := true;
    ds := GetDataSourceByName(cbDataSource.Items[cbDataSource.ItemIndex]);
    if not Assigned(ds) then
      raise Exception.Create(SCouldNotGetDataSource);

    EzDataLink.DataSource := ds;
    tvDatalinks.Selected.Text := ds.Name;
    UpdateProperties;
  end;
end;

procedure TEzDatasetDesigner.cbKeyFieldChange(Sender: TObject);
begin
  if Assigned(EzDatalink) then
  begin
    Modified := true;
    EzDataLink.KeyField := cbKeyField.Text;
  end;
end;

procedure TEzDatasetDesigner.cbParentFieldChange(Sender: TObject);
begin
  if Assigned(EzDatalink) then
  begin
    Modified := true;
    EzDataLink.ParentRefField := cbParentField.Text;
  end;
end;

// ==============================================================
// functions related to fields page
// ==============================================================
procedure TEzDatasetDesigner.InitFieldsPage;
begin
  bmpIcon.Visible := False;
  bmpParentChild.Visible := True;

  LoadDetailFields;
  LoadFieldsList;
end;

procedure TEzDatasetDesigner.LoadDetailFields;
var
  i, n: Integer;
  DataLink: TEzDatalink;
  FieldNames: TStringList;
  ParentNode, FieldNode: TTreeNode;

begin
  tvDetailFields.Items.Clear;
  FieldNames := TStringList.Create;
  try
    for i:=0 to FEzDatasetCopy.EzDatalinks.Count-1 do
    begin
      DataLink := FEzDatasetCopy.EzDatalinks[i];

      if DataLink.DetailDataset <> nil then
      begin
        // Add the main datasource node to the tree
        ParentNode := tvDetailFields.Items.Add(nil, DataLink.DisplayName);
        ParentNode.ImageIndex := 1;
        ParentNode.SelectedIndex := 1;
        DataLink.DetailDataset.GetFieldNames(FieldNames);

        for n:=0 to FieldNames.Count-1 do
        begin
          FieldNode := tvDetailFields.Items.AddChild(ParentNode, FieldNames[n]);
          FieldNode.ImageIndex := 5;
          FieldNode.SelectedIndex := 5;
        end;

        ParentNode.Expanded := True;
        FieldNames.Clear;
      end;
    end;
  finally
    FieldNames.Free;
  end;
end;

procedure TEzDatasetDesigner.LoadFieldsList;
begin
  FEzDatasetCopy.GetFieldNames(lbFieldList.Items);
  if FEzDatasetCopy.Fields.Count = 0 then
    lblEzDatasetFields.Caption := 'EzDataset''s (default) fields' else
    lblEzDatasetFields.Caption := 'EzDataset''s (persistent) fields';

  btnRemoveField.Enabled := FEzDatasetCopy.Fields.Count > 0;  
end;

procedure TEzDatasetDesigner.btnFieldEditorClick(Sender: TObject);
begin
  if not Assigned(EzDataset) then exit;

  if Modified then
    case MessageDlg('Changes must be saved, save changes?', mtConfirmation, mbOkCancel, 0) of
      mrOk: SaveData;
      mrCancel: Exit;
    end;

{$IFDEF EZ_D5}
    ShowFieldsEditor(Designer, EzDataset, TDSDesigner);
{$ELSE}
    // Delphi 4 code? Probably not used anymore!!
    ShowDatasetDesigner(Designer, EzDataset);
{$ENDIF}
  LoadFieldsList;
end;

procedure TEzDatasetDesigner.btnRefreshClick(Sender: TObject);
begin
//  if not Assigned(EzDataset) then exit;
//  CopyFields(Self, EzDataset, FEzDatasetCopy);
//  InitFieldsPage;
end;

procedure TEzDatasetDesigner.btnRemoveFieldClick(Sender: TObject);
var
  Field: TField;

begin
  if lbFieldList.ItemIndex=-1 then Exit;

  Field := FEzDatasetCopy.Fields[lbFieldList.ItemIndex];

  if MessageDlg(Format('Remove field ''%s''', [Field.FieldName]), mtConfirmation, mbOkCancel, 0)=idOK then
  begin
    if HasDesigner then
    begin
      ShowMessage('0');
      Designer.NoSelection;
    end;

    Field.Free;
    Modified := True;
    LoadFieldsList;
  end;
end;

// ==============================================================
// functions related to fieldmap page
// ==============================================================
procedure TEzDatasetDesigner.InitFieldmapPage;
begin
  bmpIcon.Visible := false;
  bmpParentChild.Visible := true;

  UpdateFieldMap;
end;

procedure TEzDatasetDesigner.UpdateFieldMap;

  procedure CreateColumn(Name: string);
  var
    Column: TEzColumnGridColumn;

  begin
    Column := grFieldMap.Columns.Add;
    Column.Title := Name;
    Column.DataType := dtstring;
    Column.Width := 80;
  end;

var
  i: integer;
  Names: TStringList;

begin
  with grFieldMap do
  begin
    // Disable ComboEditor
    cbFieldmapData.Enabled := false;

    // cleanup columns
    while Columns.Count > 1 do
      Columns[Columns.Count-1].Destroy;

    if not Assigned(EzDataset) or not Assigned(FEzDatasetCopy.MasterDataset) or
       not Enabled or (FEzDatasetCopy.EzDatalinks.Count = 0)
    then
    begin
      RowCount := 2;
      State := [igsEmpty];
    end
    else
    begin
      if FEzDatasetCopy.SelfReferencing then
        RowCount := 5 else
        RowCount := FEzDatasetCopy.EzDatalinks.Count+1;

      State := [];

      Names := TStringList.Create;
      try
        FEzDatasetCopy.GetFieldNames(Names);
        for i:=0 to Names.Count-1 do
          CreateColumn(Names[i]);
      finally
        Names.Destroy;
      end;
    end;
  end;
end;

function TEzDatasetDesigner.GetNodeType(ARow: Integer) : TNodeType;
begin
  Result := TNodeType(ARow-1);
end;

function TEzDatasetDesigner.GetCellValue(ACol, ARow: Integer) : string;
begin
  try
    Result := '';

    // check on Assigned(FEzDatasetCopy.MasterDataset) assures a valid Dataset
    if not Assigned(FEzDatasetCopy.MasterDataset) then
      Exit;

    if ACol = 1 then
    begin
      if FEzDatasetCopy.SelfReferencing then
        case GetNodeType(ARow) of
          ntSingle: Result := 'Single nodes';
          ntRoot: Result := 'Root nodes';
          ntParent: Result := 'Parent nodes';
          ntChild: Result := 'Child nodes';
        end
      else if Assigned(FEzDatasetCopy.EzDatalinks[ARow-1].Datasource) then
        Result := FEzDatasetCopy.EzDatalinks[ARow-1].Datasource.Name;
    end
    else if ACol > 1 then
      if FEzDatasetCopy.SelfReferencing then
        Result := FEzDatasetCopy.EzDatalinks[0].FieldLinks[GetNodeType(ARow)][grFieldMap.Columns[ACol-1].Title].Link
      else
        Result := FEzDatasetCopy.EzDatalinks[ARow-1].MDFields[grFieldMap.Columns[ACol-1].Title].Link;

  except
    grFieldMap.RowCount := 2;
    grFieldMap.State := [igsEmpty];
    raise;
  end;
end;

procedure TEzDatasetDesigner.grFieldMapGetValue(Sender: TObject; ACol,
  ARow: Integer; var Value: Variant);
begin
  Value := GetCellValue(ACol, ARow);
end;

procedure TEzDatasetDesigner.grFieldMapDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol = 1) and (ARow > 0) then
  begin
    grFieldMap.Canvas.Brush.Color := $00FDE1E1;
    grFieldMap.Canvas.FillRect(Rect);

    if FEzDatasetCopy.SelfReferencing then
    begin
      if ARow > 2 then
      begin
        inc(Rect.Left, (ARow-3)*8 + 3);
        ImageList1.Draw(grFieldMap.Canvas, Rect.Left, Rect.Top+2, 4);
        inc(Rect.Left, 8);
      end;
    end
    else if ARow > 1 then
    begin
      inc(Rect.Left, (ARow-2)*8 + 3);
      ImageList1.Draw(grFieldMap.Canvas, Rect.Left, Rect.Top+3, 4);
      inc(Rect.Left, 8);
    end;

    EzRenderTextRect(grFieldMap.Canvas, Rect, GetCellValue(ACol, ARow), btsFlatButton, bstNormal, 0, DT_LEFT or DT_VCENTER or DT_SINGLELINE);
  end;
end;

procedure TEzDatasetDesigner.grFieldMapCanEditShow(Sender: TObject; ACol,
  ARow: Integer; var DoShow: Boolean);
begin
  DoShow := false;
  if cbFieldmapData.Enabled then
    cbFieldmapData.SetFocus;
end;

procedure TEzDatasetDesigner.grFieldMapSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ACol = 1 then
    CanSelect := false
  else
  begin
    UpdateLinkValue;
    cbFieldmapData.Enabled := true;
    cbFieldmapData.Text := GetCellValue(ACol, ARow);
  end;
end;

function TEzDatasetDesigner.HasDesigner: Boolean;
begin
  Result := FDesigner<>nil;
end;

procedure TEzDatasetDesigner.cbFieldmapDataDropDown(Sender: TObject);
var
  i, n: integer;
  sl: TStringList;

begin
  cbFieldmapData.Items.Clear;
  if grFieldMap.Row >= 1 then
  begin
    sl := TStringList.Create;
    try
      if FEzDatasetCopy.SelfReferencing then
      begin
        FEzDatasetCopy.EzDatalinks[0].DetailDataset.GetFieldNames(sl);
        sl.Sort;

        if GetNodeType(grFieldMap.Row) in [ntSingle, ntRoot] then
          cbFieldmapData.Items.Assign(sl)
        else
        begin
          for i:=0 to sl.Count-1 do
            cbFieldmapData.Items.Add(sl[i]);
          for i:=0 to sl.Count-1 do
            cbFieldmapData.Items.Add('Root.' + sl[i]);
          for i:=0 to sl.Count-1 do
            cbFieldmapData.Items.Add('Parent.' + sl[i]);
        end;
      end
      else
      begin
        // Load fields from selected level
        with FEzDatasetCopy.EzDatalinks[grFieldMap.Row-1] do
          if Assigned(DetailDataset) then
            DetailDataset.GetFieldNames(cbFieldmapData.Items);

        // Append fields from higher levels
        for i:=0 to grFieldMap.Row-2 do
          with FEzDatasetCopy.EzDatalinks[i] do
            if Assigned(DetailDataset) then
            begin
              DetailDataset.GetFieldNames(sl);
              for n:=0 to sl.Count-1 do
                cbFieldmapData.Items.Add(DataSource.Name + '.' + sl[n]);
            end;
      end;
    finally
      sl.Destroy;
    end;
  end;
end;

procedure TEzDatasetDesigner.UpdateLinkValue;
var
  UpdateLink: TEzFieldLink;
begin
  if not cbFieldmapData.Focused then Exit;

  with FEzDatasetCopy, grFieldMap do
  begin
    UpdateLink := nil;

    if SelfReferencing then
      case Row of
        1: UpdateLink := EzDatalinks[0].SingleNodeFields[grFieldMap.Columns[Col-1].Title];
        2: UpdateLink := EzDatalinks[0].RootNodeFields[grFieldMap.Columns[Col-1].Title];
        3: UpdateLink := EzDatalinks[0].ParentNodeFields[grFieldMap.Columns[Col-1].Title];
        4: UpdateLink := EzDatalinks[0].ChildNodeFields[grFieldMap.Columns[Col-1].Title];
      end

    else if (Row >= 1) and (Col >= 2) then
      UpdateLink := EzDatalinks[Row-1].MDFields[grFieldMap.Columns[Col-1].Title];

    if Assigned(UpdateLink) then
      with UpdateLink do
        if Link <> cbFieldmapData.Text then
        begin
          Link := cbFieldmapData.Text;
          Self.Modified := true;
          Invalidate;
        end;
  end;
end;

procedure TEzDatasetDesigner.cbFieldmapDataKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
    begin
      UpdateLinkValue;
      grFieldMap.SetFocus;
    end;
    VK_ESCAPE:
    begin
      grFieldMap.SetFocus;
    end;
  end;
end;

procedure TEzDatasetDesigner.SpeedButton1Click(Sender: TObject);
begin
  UpdateLinkValue;
end;

procedure TEzDatasetDesigner.btnUndoEditClick(Sender: TObject);
begin
  cbFieldmapData.Text := GetCellValue(grFieldMap.Col, grFieldMap.Row);
end;

procedure TEzDatasetDesigner.btnFillCellsClick(Sender: TObject);
begin
  if MessageDlg('Map empty cells using default values?', mtConfirmation, mbOkCancel, 0) = idOK then
    if FEzDatasetCopy.MapDefaultFields then
    begin
      Modified := true;
      grFieldMap.Invalidate;
    end;
end;

procedure TEzDatasetDesigner.chkCanExpandClick(Sender: TObject);
begin
  if Assigned(EzDatalink) and (UpdateCount = 0) then
  begin
    Modified := true;
    EzDataLink.CanExpand := chkCanExpand.Checked;
  end;
end;

end.
