unit EzEpDsgn;

{ =============================================================================== }
{                            Endpoint designer object                             }
{                                                                                 }
{ For compiling this file do the following:                                       }
{   BCB 4.0:                                                                      }
{       Add '-LUvcl40 -LUdcldb40' to the PFLAGS compiler options in               }
{       the *.bpk file.                                                           }
{   BCB 5.0:                                                                      }
{       Add '-LUvcl50 -LUdcldb50' to the PFLAGS compiler options in               }
{       the *.bpk file.                                                           }
{   BCB 6.0:                                                                      }
{       Add '-LUvcl -LUdcldb' to the PFLAGS compiler options in                   }
{       the *.bpk file.                                                           }
{ =============================================================================== }

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ImgList, ComCtrls, StdCtrls, EzGantt

// To testdrive this designer, uncomment the next line and replace
// TEzEndPointDesigner = class(TDesignWindow) with
// TEzEndPointDesigner = class(TForm)

// {$DEFINE TEST}

{$IFNDEF TEST}
  {$IFDEF EZ_D6}
    ,DesignIntf, DesignWindows;
  {$ELSE}
    ,LibIntf, Dsgnintf, DsgnWnds;
  {$ENDIF}
{$ELSE}
  ;
{$ENDIF}

resourcestring
  SEndPointDoesNotExist = 'Generate end points for all images in the imagelist?';
  SImagesNotSet = 'No image list assigned to EndPoints.Images.';

type
  TEzEndPointDesigner = class(TDesignWindow)
    cbEndPoints: TComboBox;
    gbImageProperties: TGroupBox;
    pnlImageProperties: TPanel;
    Label7: TLabel;
    pbSampleImage: TPaintBox;
    btnUndo: TButton;
    btnApply: TButton;
    btnClose: TButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    edHotSpotX: TEdit;
    udHotSpotX: TUpDown;
    edHotSpotY: TEdit;
    udHotSpotY: TUpDown;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    edLCPx: TEdit;
    udLeftCPx: TUpDown;
    edLCPy: TEdit;
    udLeftCPy: TUpDown;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    edRCPx: TEdit;
    udRightCPx: TUpDown;
    edRCPy: TEdit;
    udRightCPy: TUpDown;
    rgActiveHotSpot: TRadioGroup;
    btnGenerate: TButton;
    btnAdd: TButton;
    btnDelete: TButton;
    Label8: TLabel;
    cbImages: TComboBox;
    gbSamples: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    pnlSampleGanttBar: TPanel;
    imgSampleBar: TImage;
    pnlMilestoneSample: TPanel;
    imgSampleMileStone: TImage;
    pnlPredecessorlineSample: TPanel;
    imgSamplePredLine: TImage;
    procedure FormCreate(Sender: TObject);
    procedure cbEndPointsChange(Sender: TObject);
    procedure pbSampleImagePaint(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edHotSpotXChange(Sender: TObject);
    procedure edHotSpotYChange(Sender: TObject);
    procedure edLCPxChange(Sender: TObject);
    procedure edLCPyChange(Sender: TObject);
    procedure edRCPxChange(Sender: TObject);
    procedure edRCPyChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnCloseClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure btnUndoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pbSampleImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnGenerateClick(Sender: TObject);
    procedure cbImagesDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cbImagesChange(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
  private
    FUpdating: Boolean;
    FSampleBar: TEzGanttbar;
    FEndPoints: TEzEndPointList;
    EndPointsCopy: TEzEndPointList;
    FSamplePredecessorLine: TPredecessorLine;
    FVisiblePredecessorLine: TVisiblePredecessorLine;
    FModified: Boolean;

{$IFNDEF TEST}
  {$IFDEF EZ_D6}
    FDesigner: IDesigner;
  {$ELSE}
    FDesigner: IFormDesigner;
  {$ENDIF}
{$ENDIF}

  protected
    procedure InternalItemDeleted(AItem: TPersistent);

{$IFNDEF DSGNDEV}
    function  UniqueName(Component: TComponent): string; override;
{$ENDIF}

    procedure LoadImage;
    procedure LoadImages;
    procedure RedrawSampleBitmap;
    procedure SetEndPoints(Points: TEzEndPointList);
    procedure SetModified(Value: Boolean);
    procedure UpdateEnabled;

  public
    procedure SaveData;

{$IFNDEF TEST}
  {$IFDEF EZ_D6}
      procedure ItemDeleted(const ADesigner: IDesigner; AItem: TPersistent); override;
  {$ELSE}
    {$IFNDEF DSGNDEV}
      procedure ComponentDeleted(Component: IPersistent); override;
    {$ENDIF}
  {$ENDIF}

  {$IFDEF EZ_D6}
      property Designer: IDesigner read FDesigner write FDesigner;
  {$ELSE}
      property Designer: IFormDesigner read FDesigner write FDesigner;
  {$ENDIF}
{$ENDIF}

    property EndPoints: TEzEndPointList read FEndPoints write SetEndPoints;
    property Modified: Boolean read FModified write SetModified;
  end;

{$IFNDEF TEST}
  {$IFDEF EZ_D6}
    procedure ShowEzEndPointDesigner(Designer: IDesigner; EndPoints: TEzEndPointList);
  {$ELSE}
    procedure ShowEzEndPointDesigner(Designer: IFormDesigner; EndPoints: TEzEndPointList);
  {$ENDIF}
{$ENDIF}

var
  EzEndPointDesigner: TEzEndPointDesigner;

implementation

uses Math, EzPainting, EzScheduler;

{$R *.DFM}

{$IFNDEF TEST}
  {$IFDEF EZ_D6}
    procedure ShowEzEndPointDesigner(Designer: IDesigner; EndPoints: TEzEndPointList);
  {$ELSE}
    procedure ShowEzEndPointDesigner(Designer: IFormDesigner; EndPoints: TEzEndPointList);
  {$ENDIF}
  var
    Editor: TEzEndPointDesigner;

  begin
    Editor := TEzEndPointDesigner.Create(Application);
    try
      Editor.Designer := Designer;
      Editor.EndPoints := EndPoints;
      Editor.Show;
    except
      Editor.Destroy;
      raise;
    end;
  end;
{$ENDIF}

procedure TEzEndPointDesigner.FormCreate(Sender: TObject);
begin
  FUpdating := false;
  FModified := false;

  FEndPoints := nil;
  EndPointsCopy := TEzEndPointList.Create(Self);

  FSampleBar := TEzGanttbar.Create(nil);
  FSampleBar.BarFill.Color := clBlue;
  FSampleBar.BarPen.Color := clRed;
  FSampleBar.BarPosition := -4;
  FSampleBar.BarHeight := 4;

  FSamplePredecessorLine := TPredecessorLine.Create(nil);
  FVisiblePredecessorLine := TVisiblePredecessorline.Create;
end;

procedure TEzEndPointDesigner.FormDestroy(Sender: TObject);
begin
  FSampleBar.Destroy;
  FSamplePredecessorLine.Destroy;
  FVisiblePredecessorLine.Destroy;
end;

procedure TEzEndPointDesigner.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Release;
end;

procedure TEzEndPointDesigner.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := false;
  if Assigned(EndPoints) and Modified then
    case MessageDlg('Save changes?', mtConfirmation, mbYesNoCancel, 0) of
      mrYes: SaveData;
      mrNo: ;
      mrCancel: Exit;
    end;
  CanClose := true;
end;

procedure TEzEndPointDesigner.cbImagesDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  H: integer;

begin
  with cbImages.Canvas do
  begin
    FillRect(Rect);
    EndPointsCopy.Images.Draw(cbImages.Canvas, Rect.Left+2, Rect.Top+2, Index);
    inc(Rect.Left, EndPointsCopy.Images.Width+4);
    H := TextHeight('E');
    TextRect(Rect, Rect.Left, Rect.Top+((cbImages.ItemHeight div 2) - (H div 2)), 'Image ' + IntToStr(Index));
  end;
end;

procedure TEzEndPointDesigner.cbEndPointsChange(Sender: TObject);
begin
  cbImages.ItemIndex := EndPointsCopy.Points[cbEndPoints.ItemIndex].ImageIndex;
  LoadImage;
  UpdateEnabled;
end;

procedure TEzEndPointDesigner.cbImagesChange(Sender: TObject);
begin
  if EndPointsCopy.Points[cbEndPoints.ItemIndex].ImageIndex <> cbImages.ItemIndex then
  begin
    Modified := true;
    EndPointsCopy.Points[cbEndPoints.ItemIndex].ImageIndex := cbImages.ItemIndex;
    LoadImage;
    UpdateEnabled;
  end;
end;

procedure TEzEndPointDesigner.InternalItemDeleted(AItem: TPersistent);
begin
  if AItem = EndPoints then
  begin
    EndPoints := nil;
    Close;
  end;
end;

{$IFNDEF TEST}
  {$IFDEF EZ_D6}
    procedure TEzEndPointDesigner.ItemDeleted(const ADesigner: IDesigner; AItem: TPersistent);
    begin
      InternalItemDeleted(AItem);
    end;
  {$ELSE}
    {$IFNDEF DSGNDEV}
      procedure TEzEndPointDesigner.ComponentDeleted(Component: IPersistent);
      begin
        InternalItemDeleted(ExtractPersistent(Component));
      end;
    {$ENDIF}
  {$ENDIF}
{$ENDIF}

procedure TEzEndPointDesigner.LoadImages;
var
  i: Integer;

begin
  cbEndPoints.Items.Clear;
  for i:=0 to EndPointsCopy.Points.Count-1 do
    cbEndPoints.Items.Add('Endpoint ' + IntToStr(i));

  cbImages.ItemHeight := EndPointsCopy.Images.Height + 4;
  cbImages.Items.Clear;
  for i:=0 to EndPointsCopy.Images.Count-1 do
    cbImages.Items.Add('Image');
end;

procedure TEzEndPointDesigner.LoadImage;
begin
  FUpdating := true;
  FSampleBar.LeftEndPoint.EndPointIndex := cbEndPoints.ItemIndex;
  FSampleBar.RightEndPoint.EndPointIndex := cbEndPoints.ItemIndex;

  with EndPointsCopy.Points[cbEndPoints.ItemIndex] do
  begin
    udHotSpotX.Position := Hotspot.x;
    udHotSpotY.Position := Hotspot.y;
    udLeftCPx.Position := LeftConnectionPoint.x;
    udLeftCPy.Position := LeftConnectionPoint.y;
    udRightCPx.Position := RightConnectionPoint.x;
    udRightCPy.Position := RightConnectionPoint.y;
  end;

  RedrawSampleBitmap;
  pbSampleImage.Invalidate;
  FUpdating := false;
end;

procedure TEzEndPointDesigner.pbSampleImagePaint(Sender: TObject);
var
  B: TBitmap;
  XScale, YScale: Double;
  Point: TPoint;

begin
  if cbImages.ItemIndex = -1 then
    with pbSampleImage.Canvas do
    begin
      Brush.Color := clWhite;
      FillRect(pbSampleImage.ClientRect);
    end
  else
  begin
    B := TBitmap.Create;
    EndPointsCopy.Images.GetBitmap(cbImages.ItemIndex, B);
    XScale := pbSampleImage.Width / EndPointsCopy.Images.Width;
    YScale := pbSampleImage.Height / EndPointsCopy.Images.Height;

    with pbSampleImage.Canvas do
    begin
      StretchDraw(pbSampleImage.ClientRect, B);

      Pen.Color := clBlack;
      Pen.Mode := pmCopy;
      Pen.Width := 1;

      MoveTo(Floor(XScale*udHotSpotX.Position + XScale/2), 0);
      LineTo(Floor(XScale*udHotSpotX.Position + XScale/2), pbSampleImage.Height);
      MoveTo(0, Floor(YScale*udHotSpotY.Position + YScale/2));
      LineTo(pbSampleImage.Width, Floor(YScale*udHotSpotY.Position + YScale/2));

      Pen.Width := 3;
      Pen.Mode := pmMergePenNot;

      // Draw Left marker
      Pen.Color := clRed;
      Point.x := Floor(XScale*udLeftCPx.Position + XScale/2);
      Point.y := Floor(YScale*udLeftCPy.Position + YScale/2);
      MoveTo(Point.x, Point.y - 4);
      LineTo(Point.x, Point.y + 4);
      MoveTo(Point.x - 4, Point.y);
      LineTo(Point.x + 4, Point.y);

      // Right marker
      Pen.Color := clGreen;
      Point.x := Floor(XScale*udRightCPx.Position + XScale/2);
      Point.y := Floor(YScale*udRightCPy.Position + YScale/2);
      MoveTo(Point.x, Point.y - 4);
      LineTo(Point.x, Point.y + 4);
      MoveTo(Point.x - 4, Point.y);
      LineTo(Point.x + 4, Point.y);
    end;
    B.Destroy;
  end;
end;

procedure TEzEndPointDesigner.RedrawSampleBitmap;
var
  Rect, BoundRect, BarRect, ClickRect: TRect;
  B: TBitmap;

begin
  if cbImages.ItemIndex = -1 then Exit;

  B := TBitmap.Create;
  try
    B.TransparentColor := EzTransparentColor;

    with imgSampleBar do
    begin
      Picture.Bitmap.Handle := 0;
      Picture.Bitmap.Width := Width;
      Picture.Bitmap.Height := Height;

      SetRect(Rect, 20, Height div 2 - 15, Width-20, Height div 2 + 15);

      with Picture.Bitmap.Canvas do
      begin
        Pen.Color := clGray;
        MoveTo(Rect.Left, 0);
        LineTo(Rect.Left, Height);
        MoveTo(Rect.Right, 0);
        LineTo(Rect.Right, Height);
        MoveTo(0, Rect.Top);
        LineTo(Width, Rect.Top);
        MoveTo(0, Rect.Bottom);
        LineTo(Width, Rect.Bottom);
      end;

      FSampleBar.LeftEndPoint.Alignment := gaBarTop;
      PaintGanttbar(B, EndPointsCopy, BoundRect, Rect, BoundRect, ClickRect, BarRect, True, FSampleBar, []);
      TransparentDraw(imgSampleBar.Picture.Bitmap.Canvas, B, BoundRect);
    end;

    with imgSampleMileStone do
    begin
      Picture.Bitmap.Handle := 0;
      Picture.Bitmap.Width := Width;
      Picture.Bitmap.Height := Height;

      SetRect(Rect, Width div 2, Height div 2 - 15, Width div 2, Height div 2 + 15);

      with Picture.Bitmap.Canvas do
      begin
        Pen.Color := clGray;
        MoveTo(Rect.Left, 0);
        LineTo(Rect.Left, Height);
        MoveTo(0, Rect.Top);
        LineTo(Width, Rect.Top);
        MoveTo(0, Rect.Bottom);
        LineTo(Width, Rect.Bottom);
      end;

      FSampleBar.LeftEndPoint.Alignment := gaRowCentered;
      PaintGanttbar(B, EndPointsCopy, BoundRect, Rect, BoundRect, ClickRect, BarRect, True, FSampleBar, [sfSelected]);
      TransparentDraw(imgSampleMileStone.Picture.Bitmap.Canvas, B, BoundRect);
    end;

    with imgSamplePredLine do
    begin
      Picture.Bitmap.Handle := 0;
      Picture.Bitmap.Width := Width;
      Picture.Bitmap.Height := Height;

      SetRect(Rect, 10, Height div 2 - 15, Width-10, Height div 2 + 15);

      with Picture.Bitmap.Canvas do
      begin
        Pen.Color := clGray;
        MoveTo(Rect.Left, 0);
        LineTo(Rect.Left, Height);
        MoveTo(Rect.Right, 0);
        LineTo(Rect.Right, Height);
        MoveTo(0, Rect.Top);
        LineTo(Width, Rect.Top);
        MoveTo(0, Rect.Bottom);
        LineTo(Width, Rect.Bottom);
      end;

      Rect.Top := Height div 2;
      Rect.Bottom := Rect.Top;

      Rect.Left := 40;
      Rect.Right := 15;
      FVisiblePredecessorline.Predecessor.Relation := prStartFinish;
      FSamplePredecessorLine.LeftSideArrow := cbEndPoints.ItemIndex;
      PaintPredecessorLine(
            imgSamplePredLine.Picture.Bitmap.Canvas,
            EndPointsCopy,
            FSamplePredecessorLine,
            FVisiblePredecessorline,
            Rect.TopLeft,
            Rect.BottomRight,
            0);

      Rect.Left := Width-40;
      Rect.Right := Width-15;
      FVisiblePredecessorline.Predecessor.Relation := prFinishStart;
      FSamplePredecessorLine.RightSideArrow := cbEndPoints.ItemIndex;
      PaintPredecessorLine(
            imgSamplePredLine.Picture.Bitmap.Canvas,
            EndPointsCopy,
            FSamplePredecessorLine,
            FVisiblePredecessorline,
            Rect.TopLeft,
            Rect.BottomRight,
            0);
    end;

  finally
    B.Destroy;
  end;
end;

procedure TEzEndPointDesigner.SetEndPoints(Points: TEzEndPointList);
begin
  if FEndPoints <> Points then
  begin
    Modified := false;

    if Assigned(Points) and not Assigned(Points.Images) then
      raise Exception.Create(SImagesNotSet);

    FEndPoints := Points;

    if FEndPoints = nil then
    begin
      EndPointsCopy.Images := nil;
      EndPointsCopy.Points.Clear;
      cbEndPoints.Items.Clear;
      RedrawSampleBitmap;
      pbSampleImage.Invalidate;
      Exit;
    end;

    EndPointsCopy.Assign(FEndPoints);

    udHotSpotX.Min := -EndPointsCopy.Images.Width;
    udHotSpotX.Max :=  EndPointsCopy.Images.Width;
    udLeftCPx.Min := -EndPointsCopy.Images.Width;
    udLeftCPx.Max :=  EndPointsCopy.Images.Width;
    udRightCPx.Min := -EndPointsCopy.Images.Width;
    udRightCPx.Max :=  EndPointsCopy.Images.Width;

    udHotSpotY.Min := -EndPointsCopy.Images.Height;
    udHotSpotY.Max :=  EndPointsCopy.Images.Height;
    udLeftCPy.Min := -EndPointsCopy.Images.Height;
    udLeftCPy.Max :=  EndPointsCopy.Images.Height;
    udRightCPy.Min := -EndPointsCopy.Images.Height;
    udRightCPy.Max :=  EndPointsCopy.Images.Height;

    LoadImages;
    UpdateEnabled;
  end;
end;

procedure TEzEndPointDesigner.SetModified(Value: Boolean);
begin
  FModified := Value;
  btnApply.Enabled := Modified;
  btnUndo.Enabled := Modified;
end;

procedure TEzEndPointDesigner.edHotSpotXChange(Sender: TObject);
begin
  if FUpdating or (cbEndPoints.ItemIndex = -1) then Exit;
  Modified := true;
  EndPointsCopy.Points[cbEndPoints.ItemIndex].HotSpot.x := udHotSpotX.Position;
  pbSampleImage.Invalidate;
  RedrawSampleBitmap;
end;

procedure TEzEndPointDesigner.edHotSpotYChange(Sender: TObject);
begin
  if FUpdating or (cbEndPoints.ItemIndex = -1) then Exit;
  Modified := true;
  EndPointsCopy.Points[cbEndPoints.ItemIndex].HotSpot.y := udHotSpotY.Position;
  pbSampleImage.Invalidate;
  RedrawSampleBitmap;
end;

procedure TEzEndPointDesigner.edLCPxChange(Sender: TObject);
begin
  if FUpdating or (cbEndPoints.ItemIndex = -1) then Exit;
  Modified := true;
  EndPointsCopy.Points[cbEndPoints.ItemIndex].LeftConnectionPoint.x := udLeftCPx.Position;
  pbSampleImage.Invalidate;
  RedrawSampleBitmap;
end;

procedure TEzEndPointDesigner.edLCPyChange(Sender: TObject);
begin
  if FUpdating or (cbEndPoints.ItemIndex = -1) then Exit;
  Modified := true;
  EndPointsCopy.Points[cbEndPoints.ItemIndex].LeftConnectionPoint.y := udLeftCPy.Position;
  pbSampleImage.Invalidate;
  RedrawSampleBitmap;
end;

procedure TEzEndPointDesigner.edRCPxChange(Sender: TObject);
begin
  if FUpdating or (cbEndPoints.ItemIndex = -1) then Exit;
  Modified := true;
  EndPointsCopy.Points[cbEndPoints.ItemIndex].RightConnectionPoint.x := udRightCPx.Position;
  pbSampleImage.Invalidate;
  RedrawSampleBitmap;
end;

procedure TEzEndPointDesigner.edRCPyChange(Sender: TObject);
begin
  if FUpdating or (cbEndPoints.ItemIndex = -1) then Exit;
  Modified := true;
  EndPointsCopy.Points[cbEndPoints.ItemIndex].RightConnectionPoint.y := udRightCPy.Position;
  pbSampleImage.Invalidate;
  RedrawSampleBitmap;
end;

{$IFNDEF DSGNDEV}
function TEzEndPointDesigner.UniqueName(Component: TComponent): string;
begin
  Result := 'NoName'
end;
{$ENDIF}

procedure TEzEndPointDesigner.SaveData;
begin
  if Assigned(Designer) then Designer.Modified;
  EndPoints.Assign(EndPointsCopy);
  Modified := false;
end;

procedure TEzEndPointDesigner.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TEzEndPointDesigner.btnApplyClick(Sender: TObject);
begin
  SaveData;
end;

procedure TEzEndPointDesigner.btnUndoClick(Sender: TObject);
var
  Points: TEzEndPointList;

begin
  if not Assigned(EndPoints) then exit;
  if MessageDlg('Undo changes?', mtConfirmation, mbOKCancel, 0) = mrOK then
  begin
    Points := EndPoints;
    EndPoints := nil;
    EndPoints := Points;
  end;
end;

procedure TEzEndPointDesigner.pbSampleImageMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  XScale, YScale: Double;

begin
  XScale := pbSampleImage.Width / EndPointsCopy.Images.Width;
  YScale := pbSampleImage.Height / EndPointsCopy.Images.Height;

  case rgActiveHotSpot.ItemIndex of
    0:
    begin
      udHotSpotX.Position := Floor(X / XScale);
      udHotSpotY.Position := Floor(Y / YScale);
    end;
    1:
    begin
      udLeftCPx.Position := Floor(X / XScale);
      udLeftCPy.Position := Floor(Y / YScale);
    end;
    2:
    begin
      udRightCPx.Position := Floor(X / XScale);
      udRightCPy.Position := Floor(Y / YScale);
    end;
  end;
end;

procedure TEzEndPointDesigner.btnGenerateClick(Sender: TObject);
begin
  Modified := true;
  EndPointsCopy.Generate;
  UpdateEnabled;
  LoadImages;
end;

procedure TEzEndPointDesigner.UpdateEnabled;
begin
  cbImages.Enabled := cbEndPoints.ItemIndex <> -1;
  btnDelete.Enabled := cbEndPoints.ItemIndex <> -1;
  btnGenerate.Enabled := EndPointsCopy.Points.Count < EndPointsCopy.Images.Count;
  gbImageProperties.Enabled := cbImages.ItemIndex <> -1;
end;

procedure TEzEndPointDesigner.btnAddClick(Sender: TObject);
begin
  Modified := true;
  EndPointsCopy.Points.Add;
  LoadImages;
end;

procedure TEzEndPointDesigner.btnDeleteClick(Sender: TObject);
begin
  Modified := true;
  EndPointsCopy.Points[cbEndPoints.ItemIndex].Destroy;
  LoadImages;
end;

end.
