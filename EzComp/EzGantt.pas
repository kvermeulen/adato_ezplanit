unit EzGantt;

interface

{$I Ez.inc}

uses
  Windows, ExtCtrls, Controls, StdCtrls, Classes, Db,
  Messages, SysUtils, Math, Graphics, Forms, Dialogs, ImgList, ActiveX,
  System.UITypes,
  EzArrays, EzDataSet, EzTimeBar, EzDateTime, EzScheduler;

const
  // Timer id's
  tmrTimeline = 1;
  tmrScroll = 2;
  tmrSelection = 4;
  RowBorderHitMargin = 2;

  ClipedDragThreshold = 2500; // Threshold that determines when a bar being dragged
                             // is fully prepared or cliped by the current client rectangle.

type
  EEzGanttError = class(Exception);

  PointArray = array[0..255,0..1] of Integer;
  PPointArray = ^PointArray;
  TSize = record
    Width: Integer;
    Height: Integer;
  end;

  PHRGN = ^HRGN;

  TCustomEzGanttChart = class;
  TEzGanttBar = class;
  TEzGanttLineMarker = class;
  TEzGanttLineMarkers = class;
  TEzGanttLineMarkersHelper = class;
  TEzEndPointList = class;
  TVisiblePredecessorline = class;
  TEndPointIndex = type Integer;

{$IFDEF EZ_VD4}
  TImageIndex = type Integer;
{$ENDIF}
{$IFDEF EZ_VCB4}
  TImageIndex = type Integer;
{$ENDIF}

  PPredecessor = ^TPredecessor;
  TPredecessor=record
    SuccessorNode: TNodeKey;
    PredecessorNode: TNodeKey;
    Relation: TEzPredecessorRelation;
    Text: string;
    Tag: Integer;
  end;
  TPredecessors = array of TPredecessor;

  // 6 points is the maximum number of points possible for a
  // single predecessorline
  PPredecessorlinePoints = ^TPredecessorlinePoints;
  TPredecessorlinePoints = array[0..5] of TPoint;

  // Additional flags for gantt bars stored in the cache
  TGanttbarState = (
    sfIsHiddenNode,   // Bar is associated with a hidden record
    sfIsHiddenParentNode, // Bar is associated with a parent record in hidden mode
    sfExternal,       // Bar was externaly created,
                      // that is not created by the TEzGanttChart itself.
    sfSelected,       // Bar is currently selected
                      // PrepareBar will add selection brackets to the bitmap.
    sfLeftSnap,       // Left side of bar is aligned with snappoint
                      // PrepareBar will add a snap indicator to the bitmap.
    sfRightSnap       // Right side of bar is aligned with snappoint
                      // PrepareBar will add a snap indicator to the bitmap.
  );
  TGanttbarStates = set of TGanttbarState;

  // Helper struct.
  // This structure stores a unique reference to a ganttbar.
  TEzGanttbarReference = record
    RowNode: TRecordNode;
    BarNode: TRecordNode;
    Bar: TEzGanttbar;
  end;

  // Structure used for storing gantt bar data in the internal cache
  PScreenbar = ^TScreenbar;
  TScreenbar = class
  public
    Node: TRecordNode;          // Node associated with this bar
    Row: Integer;               // The row on which bar is painted
    BarStart: TDateTime;        // Start date of bar
    BarStop: TDateTime;         // Stop date of bar
    BarProps: TEzGanttBar;      // Properties used for drawing bar
    BarStates: TGanttbarStates; // Additional info about this bar
    ActualSize: TRect;          // Actual rectangle of bar (between start date and end date)
    BoundRect: TRect;           // Extended rectangle of ganttbar. That is
                                // the bounding rectange of the bar, inclusive
                                // left and right bitmaps.
    ClickRect: TRect;           // Click rectangle of ganttbar. That is the rectangle
                                // used to detect a hit.
    BarRect:TRect;              // Rectange that indicates the actual bar region
                                // of a gantt bar.
    TextRect: TRect;            // Rectangle occupied by textlabel of bar
    Tag: LongInt;               // User defined variable
    TextShowing: Boolean;
    BarShowing: Boolean;
    constructor Create;
    destructor Destroy; override;
    procedure Assign(ASource: TScreenbar);
  end;
  TScreenBarArray = array of TScreenBar;
  PScreenBarArray = ^TScreenBarArray;

  //
  // TScreenRow maintains a sorted list of PScreenBar objects.
  // The screenbars are sorted by their StopDate so that
  // bars are actually draw left to right.
  //
  TScreenRow = class(TEzBaseArray)
  private
    FIsPrepared: Boolean;
    FRecalcRects: Boolean;

  protected
    procedure FreeItems(PItems: Pointer; numItems: Integer); override;
    procedure PutItem(index: Integer; value: TScreenBar);
    function  GetItem(index: Integer): TScreenBar;

  public
    constructor Create(itemcount, dummy: Integer); override;

    procedure Clear; override;
    function  CompareItems(var Item1, Item2): Integer; override;
    function  FindProgressBar(Bar: TScreenBar): integer;
    function  IndexOf(ADate: TDateTime): integer; reintroduce; overload;
    function  IndexOf(Bar: TScreenBar): integer; reintroduce; overload;
    function  IndexOfBar(ABar: TEzGanttbar): integer;
    function  IndexOfNode(Original: TEzGanttbar; Node: TRecordNode): integer;

    property Items[Index: Integer]: TScreenBar read GetItem write PutItem; default;
    property IsPrepared: Boolean read FIsPrepared write FIsPrepared;
    property RecalcRects: Boolean read FRecalcRects write FRecalcRects;
  end;

  // TPaintRow maintains a sorted list of PScreenBar objects.
  // Bars are sorted on their selected state, Z-Index and StopDate.
  // TPaintRow is internally used during paint operations to temporarily
  // sort bars. Object will not be release when they are removed from
  // the list.
  TPaintRow = class(TScreenRow)
    procedure FreeItems(PItems: Pointer; numItems: Integer); override;

  public
    function  CompareItems(var Item1, Item2): Integer; override;
  end;

  TScreenCache = class(TEzObjectList)
  protected
    Gantt: TCustomEzGanttChart;

    function Get(Index: Integer): TScreenrow;
    procedure Put(Index: Integer; Item: TScreenrow);

  public
    constructor Create(G: TCustomEzGanttChart); reintroduce;
    procedure Clear; override;
    procedure ClearBars; overload;
    procedure ClearBars(TopRow, BottomRow: Integer); overload;
    function  LocateScreenBar(Ref: TEzGanttbarReference): TScreenBar;
    property Items[Index: Integer]: TScreenRow read Get write Put; default;
  end;

  // Object used to store information used during draw operations
  TGanttDrawInfo = class(TComponent)
  private
    FScreenCache: TScreenCache;
    FPaintRow: TPaintRow;

  protected
    function  GetRow(Item: Integer): TScreenRow;
    procedure SetScreenCacheSize(Rows: Integer);

  public
    LeftDateExtend: TDateTime;  // Date of left clipping boundary
    RightDateExtend: TDateTime; // Date of right clipping boundary
    UpdateRect: TRect;          // Clipping rectangle extended to nearest boundaries.
    ExcludedRect: TRect;        // Rectangle not used, i.e. below last record
    RowsRect: TRect;            // Client rectangle, excluding ExcludedRect
    TopRow,                     // Row number of of row located at UpdateRect.Top
    ScreenTopRecNo,             // Record number of record located row 0
    TopRecNo,                   // Record number of record located row TopRow
    RecCount,                   // Number of records in the dataset buffer.
    ActiveRecord,               // Active record of dataset
    EndPointSize: Integer;      // Width of ganttbar endpoints
    ScreenTopNode: TRecordNode; // RecordNode of record located at first row


    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure ScrollCache(Distance: Integer);

    property PaintRow: TPaintRow read FPaintRow;
    property ScreenCache: TScreenCache read FScreenCache;
    property ScreenRow[Item: Integer]: TScreenRow read GetRow; default;
  end;

  TEzGanttHitPosition = (
    hpNowhere,          // No hit found, cursor in empty area of Gantt chart
    hpOnRow,            // On a row displaying record data
    hpOnBar,            // On a Gantt bar
    hpOnBarStart,       // On the left side of the bar
    hpOnBarStop,        // On the right side of the bar
    hpOnLineMarker,     // On a visible line marker
    hpOnPredecessorLine,// On a predecessor line
    hpOnRowTopBorder,   // On the top border of a row. Accuracy is determined by global HitMargin value.
    hpOnRowBottomBorder,// On the bottom border of a row. Accuracy is determined by global HitMargin value.
    hpInsertAsChild,    // Task will be inserted as a child of drop target
    hpInsertTask,       // The task will be inserted before drop target.
    hpInsertTaskAfter   // The task will be inserted after drop target.
  );
  TEzGanttHitPositions = set of TEzGanttHitPosition;

  // structure used to store information about a position in the Gantt chart
  PEzGanttHitInfo = ^TEzGanttHitInfo;
  TEzGanttHitInfo = record
    HitWhere: TPoint;         // Location where hit took place
    HitRow: integer;          // Index of row (or -1 for none)
    HitRowRect: TRect;        // Bounding rectangle for row at HitRow
    HitBar: integer;          // Index of bar in screen cache (or -1 for none)
    HitScreenbar: TScreenbar; // TEzGanttbar struct in screen cache

    HitSelectableBar: integer;// Index of bar in screen cache (or -1 for none)
                              // which has gfCanSelect set in Flags
    HitSelectableScreenbar: TScreenbar; // TEzGanttbar struct in screen cache

    HitLineMarker: TEzGanttLineMarker; // TEzGanttLineMarker object
    HitPositions: TEzGanttHitPositions;  // Type of hit
    HitPredecessorLine: TVisiblePredecessorline;
  end;

  TGanttBarFlag = (
    gfDrawParents,      // Gantt bar will be drawn for parent records
    gfDrawChilds,       // Gantt bar will be drawn for child records
    gfMileStone,        // Gantt bar will be drawn as a milestone, i.e. a single point
    gfCanMove,          // Allows moving this bar horizontally with the mouse
    gfCanCreate,        // Indicates this bar definition is used when creating a new bar.
    gfCanDragDrop,      // Allows bar to be draged inbetween rows
    gfIsDropTarget,     // Allows bar to be a drop target
    gfCanResize,        // Allows bar to be resized
    gfCanSelect,        // Allow this bar to be selected
    gfIgnoreOverlap,    // If set, bar is ignored when testing for overlapping bars
    gfCanResizeLeft,    // Allows bar to be resized on the left side,
    gfCanLink           // This bar allows linking (adding a predecessor relation)
                        // when another bar is dragged over this bar.
  );

  TGanttBarFlags = set of TGanttbarFlag;
  TGanttBarDragType = (
      bdNone,           // No dragging
      bdDragDrop,       // A drag drop operation is active (i.e. a bar is
                        // being dragged between different rows of
                        // the ganttchart)
      bdBarHorizontal,  // A bar is being moved horizontally on the same row
      bdBarDate,        // The right side of a ganttbar is being dragged,
                        // thereby shrinking or expanding the bar.
      bdBarStartDate,   // The left side of a ganttbar is being dragged,
                        // thereby shrinking or expanding the bar.
      bdProgressBar,    // A progressbar is being resized
      bdAddProgressBar, // A new progressbar is being dragged inside a parent
      bdAddPredecessorline, // A new predecessor relation is added by dragging
                            // a bar onto another bar.
      bdMilestone,      // A milestone is being ragged (i.e. a task with zero duration).
      bdNewbar,         // A new bar is added.
      bdLineMarker,     // A line marker is being dragged.
      bdSelection       // User is dragging a rectangle on screen to select
                        // multiple bars at the same time.
  );

  TGanttBarAlignment =
    (gaRowBottom, gaRowCentered, gaRowTop, gaAbsCenter,
     gaBarTop, gaBarCentered, gaBarBottom);

  TGanttbarLabelAligment = (laRowTop, laRowCenter, laRowBottom,
                            laBarTop, laBarCenter, laBarBottom,
                            laOnTopOfBar, laBelowBar);
  TGanttbarLabelPosition = (lpgLeft, lpgRight, lpgCenter, lpgBarLeft, lpgBarRight);
  TGanttbarEndPointType = (etFixed, etDuration, etPercentage);

  // Indicates which drag operations should be handled by the gantt chart
  TEzGanttDragOperation = (
    doLinkTasks,       // Allow linking of tasks using drag and drop
    doMoveTasks,       // Allow tasks to move to another parent or to another
                       // position in the task hierarchy using drag and drop
    doMoveHorizontal   // Allow tasks to be moved to another date using drag and drop
  );
  TEzGanttDragOperations = set of TEzGanttDragOperation;

  TClearCacheRowEvent = procedure(Sender: TObject; ARow: integer) of object;
  TMeasureRowEvent = procedure (Sender: TObject; ARow: Integer; var Height: Integer) of object;
  TGetBarPropsEvent = procedure (Sender: TObject; const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean) of object;
  TBarDraggingEvent = procedure (Sender: TObject; X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType;
                                  var BarUpdated: Boolean) of object;
  TLineMarkerStartDragEvent = procedure (Sender: TObject; X: Integer; LineMarker: TEzGanttLineMarker;
                                  var CanDrag: Boolean) of object;
  TLineMarkerEndDragEvent = procedure (Sender: TObject; X: Integer; LineMarker: TEzGanttLineMarker;
                                  var NewPosition: TDateTime; var Update: Boolean) of object;
  TLineMarkerDraggingEvent = procedure (Sender: TObject; X: Integer; LineMarker: TEzGanttLineMarker) of object;
  TLineMarkerDeleteEvent = procedure (Sender: TObject; LineMarker: TEzGanttLineMarker; var CanDelete: Boolean) of object;
  TSelectGanttbarEvent = procedure (Sender: TObject; ABar: TScreenbar; var CanSelect: Boolean) of object;
  TSelectGanttbarReferenceEvent = procedure (Sender: TObject; ABar: TEzGanttbarReference; var CanDeselect: Boolean) of object;
  TSelectPredecessorLineEvent = procedure (Sender: TObject; Predecessor: TPredecessor; var CanSelect: Boolean) of object;
  TDeselectPredecessorLineEvent = procedure (Sender: TObject; Predecessor: TPredecessor) of object;
  TStartbarDragEvent = procedure (Sender: TObject; X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType;
                                  var CanDrag: Boolean) of object;
  TEndbarDragEvent = procedure (Sender: TObject; X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType;
                                  var UpdateDataset: Boolean) of object;
  TBardataChangedEvent = procedure (Sender: TObject; BarInfo: TScreenbar; DragType: TGanttBarDragType) of object;
  TGetPredecessorEvent = procedure (Sender: TObject; var Predecessor: TPredecessor) of object;
  TCheckPredecessorVisibilityEvent = procedure (Sender: TObject;
                                  const Predecessor: TPredecessor;
                                  PredecessorNode, VisiblePredecessorNode, SuccessorNode,
                                  VisibleSuccessorNode: TRecordNode;
                                  var DoShow: Boolean) of object;
  TDrawBeginEvent = procedure (Sender: TObject; var bEraseBackground: Boolean) of object;
  TOnLinkTasksEvent = procedure (Sender: TObject; Predecessor, Successor: TNodeKey; Relation: TEzPredecessorRelation) of object;
  TOnMoveTaskEvent = procedure (Sender: TObject; Task: TRecordNode; var Location: TRecordNode; var PositionInfo: TEzGanttHitPositions; var DoMove: Boolean) of object;
  TEzGanttDragEvent = procedure(Sender: TCustomEzGanttChart; Source: TObject; State: TDragState;
                                    Pt: TPoint; HitInfo: TEzGanttHitInfo;
                                    var Operations: TEzGanttDragOperations;
                                    var Relation: TEzPredecessorRelation;
                                    var Accept: Boolean) of object;
  TEzGanttGetHintEvent = procedure(Sender: TCustomEzGanttChart; HitInfo: TEzGanttHitInfo;
                                    var HintText: string) of object;

  TEzOverGanttChartEvent = procedure(Sender: TCustomEzGanttChart; HitInfo: TEzGanttHitInfo) of object;
  TEzOnPrepareCacheRowEvent = procedure(Sender: TObject; currentNode : TRecordNode; RowNo : integer; aStates :TGanttbarStates) of object;

  TGanttOption = (
    // Indicates whether bars which are in a hidden state allow editing.
    gaoAllowHiddenEdit,
    // Tells the ganttchart to paint the bars belonging to childs
    // records on the parent row, even when the parent not
    // collapsed. Such bars are said to be in a hidden state.
    //
    // Normally TEzGanttchart draws such parents using the
    // ganttbars defined for this parent (for example using a
    // summary bar). However, when gaoAllwaysShowHiddens is set,
    // the row of the collapsed parent will be filled with the
    // ganttbars of the child records instead.
    // See also gaoShowHiddens.
    gaoAllwaysShowHiddens,
    // Allow creation of new tasks using the mouse.
    // At least one ganttbar definition must have the
    // gfCanCreate flags set for this to work.
    gaoCanCreateBars,
    // if set, snap while draging.
    // Property SnapSize sets the size of the snap intervals.
    gaoDoSnap,
    // Value no longer in use. Use TEzGanttChart.HatchOptions
    // to control hatching.
    gaoHatch,
    // If set, TEzGanttChart draws horizontal lines between the
    // rows of the ganttchart. Properties GridlinePen and
    // GridLineWidth control the visual properties of the
    // line drawn.
    gaoHorzLines,
    // Tells the ganttchart to paint the bars belonging to
    // childs records on the parent row, when the parent is
    // collapsed. Such bars are said to be in a hidden state.
    //
    // Normally TEzGanttchart draws such parents using the
    // ganttbars defined for this parent (for example using a
    // summary bar). However, when gaoShowHiddens is set, the
    // row of the collapsed parent will be filled with the
    // ganttbars of the child records instead.
    // See also gaoAllwaysShowHiddens.
    gaoShowHiddens,
    // Determines whether a message box appears asking the user
    // to confirm record deletions.
    gaoConfirmDelete,
    // If set, the ganttbar selection will not be erased when the
    // ganttchart looses focus.
    gaoKeepSelection,
    // If set, selected ganttbars will be drawn 'selected' even
    // when the ganttchart looses focus.
    gaoAllwaysShowSelection,
    // This options tells TEzGanttChart to maintain a row in the
    // cache for each record in the dataset. Normally the number
    // of rows in the screencache equals the number of rows on
    // screen. Therefore, whenever the dataset scrolls, cache rows
    // that scroll off the screen need to be cleared while rows
    // scrolling onto the screen have to be loaded. This could result
    // in a noticable timedelay during scroll operations when
    // the number of bars per row is large. If this option is set,
    // this delay will only occur the first time data is loaded and
    // every time when data changes in your dataset (when a record
    // is updated, the screencache is cleared and reloaded completely).
    gaoCacheAllRows,
    // This option tells TEzGanttchart to ignore DatasetChange
    // events fired from the dataset. As a result the screen cache
    // will not be cleared and reloaded every time data changes. Because
    // changes to the data are no longer aplied to the screen cache,
    // you are responsible for updating the screen cache whenever
    // data changes.
    gaoIgnoreDatasetChangedEvent,
    // Update ganttchart when thumb tracking the horizontal scrollbar
    gaoGoThumbTracking,
    // If this option is set, the user can select multiple ganttbars
    // by dragging a selection rectangle on screen. Only bars having
    // the gfCanSelect flag set can be selected.
    gaoDoRectangularSelection,

    // This options controls whether TEzGanttChart will post changes
    // to a ganttbar immediately or will leave the Dataset in edit mode.
    // If set, changes are posted immediately.
    gaoPostAllEdits
  );
  TGanttOptions = set of TGanttOption;

  TEzGanttDragObject = class(TDragControlObject)
  protected
    FPredecessorPen: TPen;
    FDragBitmap: TBitmap;
    FScreenCopy: TBitmap;
    FBar: TEzGanttbarReference;
    FBarProps: TEzGanttBar;      // Properties used for drawing bar
    FBarStart, FBarStop: TDateTime;
    FDragBitmapRect: TRect;                 // Size of actual bitmap being dragged
    FDragClickRect: TRect;                  // Click size of rectangle being dragged
    FDragScreenRect: TRect;                 // Drag rectangle in screen coordinates
    FGanttbarRect: TRect;
    FMouseOffset: TPoint;
    FEndPoints: TEzEndPointList;
    FRequiresPrepare: Boolean;
    XOffset: Integer;

    function  GetDragCursor(Accepted: Boolean; X, Y: Integer): TCursor; override;
    procedure InternalHideDragImage(Gantt: TCustomEzGanttChart; ScreenDC: HDC);
    procedure InternalShowDragImage(Gantt: TCustomEzGanttChart; ScreenDC: HDC);
    procedure UpdateDragImage(Gantt: TCustomEzGanttChart; X, Y: Integer;
      HitInfo: TEzGanttHitInfo);
    function UpdatePositiondata(Gantt: TCustomEzGanttChart; X, Y: Integer;
      HitInfo: TEzGanttHitInfo): Boolean;

  public
    constructor Create(AControl: TControl); override;
    destructor Destroy; override;

    procedure HideDragImage(Gantt: TCustomEzGanttChart); reintroduce; virtual;
    procedure PrepareDragImage;
    procedure ShowDragImage(Gantt: TCustomEzGanttChart); reintroduce; virtual;
    procedure ResetDragImage(Start, Stop: TDateTime; BarRect: TRect; PropertiesChanged: Boolean);

    property Bar: TEzGanttbarReference read FBar;
    property BarStart: TDateTime read FBarStart;
    property BarStop: TDateTime read FBarStop;
    property BarProps: TEzGanttBar read FBarProps;
    property GanttbarRect: TRect read FGanttbarRect write FGanttbarRect;
    property EndPoints: TEzEndPointList read FEndPoints write FEndPoints;
  end;

  TGanttbarLabel = class(TPersistent)
  protected
    FFont: TFont;
    FText: WideString;
    FPosition: TGanttbarLabelPosition;
    FAlignment: TGanttbarLabelAligment;
    FExtraOffset: Integer;
    FExtraVOffset: Integer;
    FMultiLine: Boolean;

    procedure SetFont(F: TFont);

  public
    procedure   Assign(Source: TPersistent); override;
    constructor Create;
    destructor Destroy; override;

  published
    property Alignment: TGanttbarLabelAligment read FAlignment write FAlignment
                default laBarCenter;
    property ExtraOffset: Integer read FExtraOffset write FExtraOffset default 0;
    property ExtraVOffset: Integer read FExtraVOffset write FExtraVOffset default 0;
    property Font: TFont read FFont write SetFont;
    property Text: WideString read FText write FText;
    property Position: TGanttbarLabelPosition read FPosition
                        write FPosition default lpgRight;
    property MultiLine: Boolean read FMultiLine write FMultiLine default False;
  end;

  TGanttBarEndPoint = class(TPersistent)
  private
    FAlignment: TGanttBarAlignment;
    FEndPointIndex: TEndPointIndex;
    FGanttBar: TEzGanttBar;

  public
    constructor Create(AOwner: TEzGanttBar);
    procedure   Assign(Source: TPersistent); override;

    property GanttBar: TEzGanttBar read FGanttBar;

  published
    property Alignment: TGanttBarAlignment read FAlignment write FAlignment default gaBarTop;
    property EndPointIndex: TEndPointIndex read FEndPointIndex write FEndPointIndex default -1;
  end;

  THatchOptions = class(TPersistent)
  protected
    FGanttchart: TCustomEzGanttChart;
{$IFNDEF BCB}
    FAlphaBlend: Boolean;
    FAlphaBlendBitmap: TBitmap;
    FAlphablendColor: TColor;
    FAlphablendValue: Integer;
{$ENDIF}
    FShow: Boolean;
    FHatchPen: TPen;
    FHatchBrush: TBrush;

    procedure GraphicsObjectChanged(Sender: TObject);
{$IFNDEF BCB}
    procedure SetAlphaBlend(Value: Boolean);
    procedure SetAlphaBlendColor(Value: TColor);
    procedure SetAlphaBlendValue(Value: Integer);
{$ENDIF}
    procedure SetHatchPen(Value: TPen);
    procedure SetHatchBrush(Value: TBrush);
    procedure SetShow(Value: Boolean);

  public
    constructor Create(Gantt: TCustomEzGanttChart);
    destructor  Destroy; override;

    procedure  Assign(Source: TPersistent); override;

  published
{$IFNDEF BCB}
    property AlphaBlend: Boolean read FAlphablend write SetAlphablend default false;
    property AlphaBlendColor: TColor read FAlphablendColor write SetAlphaBlendColor default clBlack;
    property AlphaBlendValue: Integer read FAlphablendValue write SetAlphaBlendValue default 255;
{$ENDIF}
    property HatchPen: TPen read FHatchPen write SetHatchpen;
    property HatchBrush: TBrush read FHatchBrush write SetHatchBrush;
    property Show: Boolean read FShow write SetShow default true;
  end;

  TPredecessorLineOption = (
    poDisplayOnTop,         // Paint ganttbars first then predecessor lines
    poSkipInvalidLines,     // Skip predecessor lines for which the
                            // predecessor and/or successor record could
                            // not be located in the current recordset.
    poShortenInvalidLines,  // Paint an abreviated predecessor line in case
                            // only the successor or predecessor record could
                            // be located.
                            // Only a short line and arrow will be drawn towards
                            // the successor ganttbar or from the predecessor
                            // bar.
    poSkipHiddenLines,      // Do not draw the predecessor line in case both the
                            // successor node and predecessor node are invisible.
    poKeepSelecttion,       // Keep selected predecessor line selection, even when
                            // gantt chart loses focus
    poAllwaysShowSelection  // Hightlight selected predecessor line, even when
                            // gantt chart loses focus
  );
  TPredecessorLineOptions = set of TPredecessorLineOption;

  TPredecessorLine = class(TPersistent)
  private
    FVisible: Boolean;
    FOptions: TPredecessorLineOptions;
    FGanttChart: TCustomEzGanttChart;
    FPen: TPen;
    FFont: TFont;

    FLeftSideArrow: TEndPointIndex;
    FRightSideArrow: TEndPointIndex;

    FStartField: string;
    FStartOffset: integer;
    FStartLength: integer;

    FEndField: string;
    FEndOffset: integer;
    FEndLength: integer;

    FDrawBitmap: TBitmap;
    FDrawBitmapIndex: Integer;

  protected
    procedure SetEndField(Value: string);
    procedure SetEndOffset(Value: Integer);
    procedure SetEndLength(Value: Integer);

    procedure SetOptions(Value: TPredecessorLineOptions);
    procedure SetPredecessorPen(P: TPen);
    procedure SetPredecessorFont(F: TFont);
    procedure SetLeftSideArrow(I: TEndPointIndex);
    procedure SetRightSideArrow(I: TEndPointIndex);
    procedure SetStartField(Value: string);
    procedure SetStartOffset(Value: Integer);
    procedure SetStartLength(Value: Integer);
    procedure SetVisible(Value: Boolean);

  public
    constructor Create(Gantt: TCustomEzGanttChart);
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;

    property GanttChart: TCustomEzGanttChart read FGanttChart;

  published
    property Pen: TPen read FPen write SetPredecessorPen;
    property Font: TFont read FFont write SetPredecessorFont;
    property Visible: Boolean read FVisible write SetVisible default true;
    property LeftSideArrow: TEndPointIndex read FLeftSideArrow write SetLeftSideArrow default -1;
    property Options: TPredecessorLineOptions read FOptions write SetOptions default [];
    property RightSideArrow: TEndPointIndex read FRightSideArrow write SetRightSideArrow default -1;
    property StartField: string read FStartField write SetStartField;
    property StartOffset: integer read FStartOffset write SetStartOffset default 0;
    property StartLength: integer read FStartLength write SetStartLength default 5;
    property EndField: string read FEndField write SetEndField;
    property EndOffset: integer read FEndOffset write SetEndOffset default 0;
    property EndLength: integer read FEndLength write SetEndLength default 5;
  end;

  TEzGanttbarShowFlag = (
    sfNormal,         // Ganttbar is visible when the node is in it's normal
                      // state. That is not sfHidden and not sfOnHiddenParent.
    sfHidden,         // Ganttbar is visible when the node is hidden.
                      // A node is hidden when it is painted as part of a
                      // collapsed parent node and gaoShowHiddens is set for
                      // TEzGantChart.Options.
    sfExpanded,       // Ganttbar is visible when the node is Expanded.
                      // This flag is only used for parent nodes.
    sfCollapsed,      // Ganttbar is visible when the node is Collapsed.
                      // This flag is only used for parent nodes.
    sfOnHiddenParent  // Ganttbar is visible when painted on a collapsed parent
                      // node and gaoShowHiddens is set for
                      // TEzGantChart.Options.
    );
  TEzGanttbarShowFlags = set of TEzGanttbarShowFlag;

  TEzGanttBarVisibility = class(TPersistent)
  protected
    FGanttBar: TEzGanttBar;
    FEzDatalinkName: string;
    FEzDatalinkLevel: Integer;
    FNodeTypes: TNodeTypes;
    FShowFor: TEzGanttbarShowFlags;

    procedure SetEzDatalinkName(Name: string);
    procedure SetNodeTypes(Value: TNodeTypes);

  public
    constructor Create(Bar: TEzGanttBar);
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

    property EzDatalinkLevel: Integer read FEzDatalinkLevel write FEzDatalinkLevel default -1;
    property GanttBar: TEzGanttBar read FGanttBar;

  published
    property EzDatalinkName: string read FEzDatalinkName write SetEzDatalinkName;
    property NodeTypes: TNodeTypes read FNodeTypes write SetNodeTypes default [ntSingle, ntChild];
    property ShowFor: TEzGanttbarShowFlags read FShowFor write FShowFor default [sfNormal, sfExpanded, sfCollapsed];
  end;

  TGanttBarClass = class of TEzGanttBar;
  TEzGanttBar = class(TCollectionItem)
  private
    FName: string;
    FFlags: TGanttbarFlags;
    FBarHeight: Smallint;
    FBarPosition: Smallint;
    FBarFill: TBrush;
    FBarPen: TPen;
    FZ_Index: integer;
    FStartField: string;
    FStopField: string;
    FTextLabel: TGanttbarLabel;
    FProgressBar: TEzGanttBar;
    FFixupProgressbarName: string;
    FParentBar: TEzGanttBar;
    FOriginal: TEzGanttBar;
    FEndpointType: TGanttbarEndPointType;
    FLeftEndPoint: TGanttBarEndPoint;
    FRightEndPoint: TGanttBarEndPoint;
    FVisibility: TEzGanttBarVisibility;

  protected
    function  GetDisplayName: string; override;
    function  GetGanttChart: TCustomEzGanttChart;
    function  GetProgressBar: string;

    procedure SetBarFill(B: TBrush);
    procedure SetBarPen(P: TPen);
    procedure SetEndpointType(Value: TGanttbarEndPointType);
    procedure SetName(const S: string);
    procedure SetProgressBar(S: string);
    procedure SetTextLabel(L: TGanttbarLabel);
    procedure SetFlags(F: TGanttbarFlags);
    procedure SetVisibility(Value: TEzGanttBarVisibility);
    procedure SetZ_Index(Z: Integer);

  public
    procedure   Assign(Source: TPersistent); override;
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;

    function  ParentBar: TEzGanttBar;

    property  GanttChart: TCustomEzGanttChart read GetGanttChart;
    property  ProgressBarPtr: TEzGanttBar read FProgressBar;

    property Original: TEzGanttBar read FOriginal;

  published
    property BarHeight: SmallInt read FBarHeight write FBarHeight default 10;
    property BarPosition: SmallInt read FBarPosition write FBarPosition default 0;
    property BarFill: TBrush read FBarFill write SetBarFill;
    property BarPen: TPen read FBarPen write SetBarPen;
    property Z_Index: integer read FZ_Index write SetZ_Index default 0;
    property EndpointType: TGanttbarEndPointType read FEndpointType write SetEndpointType default etFixed;
    property Name: string read FName write SetName;
    property LeftEndPoint: TGanttBarEndPoint read FLeftEndPoint write FLeftEndPoint;
    property RightEndPoint: TGanttBarEndPoint read FRightEndPoint write FRightEndPoint;
    property TextLabel: TGanttbarLabel read FTextLabel write SetTextLabel;

    property Flags: TGanttbarFlags read FFlags write SetFlags
              default [gfCanMove, gfCanResize];
    property ProgressBar: string read GetProgressBar write SetProgressBar;
    property StartField: string read FStartField write FStartField;
    property StopField: string read FStopField write FStopField;
    property Visibility: TEzGanttBarVisibility read FVisibility write SetVisibility;
  end;

  TEzGanttbarArray = class(TEzBaseArray)
  private
    FGantt: TCustomEzGanttChart;

  protected
    procedure FreeItems(PItems: Pointer; numItems: Integer); override;
    procedure PutItem(index: Integer; value: TEzGanttbarReference);
    function  GetItem(index: Integer): TEzGanttbarReference;

  public
    constructor Create(itemcount: Integer; AGantt: TCustomEzGanttChart); reintroduce;

    function  Add(RowNode: TRecordNode; ABar: TScreenBar): Integer; reintroduce; overload;
    function  Add(BarRef: TEzGanttbarReference): Integer; reintroduce; overload;
    procedure Clear; override;
    procedure Remove(RowNode: TRecordNode; ABar: TScreenBar); overload;
    procedure Remove(BarRef: TEzGanttbarReference); overload;

    function  CompareItems(var Item1, Item2): Integer; override;

    property Items[Index: Integer]: TEzGanttbarReference read GetItem write PutItem; default;
  end;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TEzGanttBars = class(TCollection)
  private
    FGanttChart: TCustomEzGanttChart;
    FFixupRequired: Boolean;

  protected
    function  GetEzGanttBar(Index: Integer): TEzGanttBar;
    procedure SetEzGanttBar(Index: Integer; Value: TEzGanttBar);
    function  GetOwner: TPersistent; override;
{$IFDEF EZ_D6}
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
{$ENDIF}
    procedure FixupProgressbars;

  public
    constructor Create(Chart: TCustomEzGanttChart; BarClass: TGanttBarClass); virtual;
    procedure   EndUpdate; override;
    function    IndexOfName(const Value: string) : integer;
    function    Add: TEzGanttBar;
    function    Updating: Boolean;
    property    Items[Index: Integer]: TEzGanttBar read GetEzGanttBar write SetEzGanttBar; default;
    property    GanttChart: TCustomEzGanttChart read FGanttChart;
  end;

  //=---------------------------------------------------------------------------=
  //
  // TEndPoint
  //
  //=---------------------------------------------------------------------------=
  TPersistentPoint = class(TPersistent)
  private
    FPoint: TPoint;

  protected
    function GetX: Integer;
    function GetY: Integer;
    procedure SetX(Value: Integer);
    procedure SetY(Value: Integer);

  public
    constructor Create;
    procedure   Assign(Source: TPersistent); override;

    property Point: TPoint read FPoint write FPoint;

  published
    property x: Integer read GetX write SetX;
    property y: Integer read GetY write SetY;
  end;

  TEzEndPoint = class(TCollectionItem)
  private
    FImageIndex: TImageIndex;
    FHotSpot: TPersistentPoint; // Hotspot of end point (i.e. alignment point with date)
    FLeftCP: TPersistentPoint;  // Point to align bar rectangle with when endpoint is used on the left side.
    FRightCP: TPersistentPoint; // Point to align bar rectangle with when endpoint is used on the right side.

  protected
    function  GetDisplayName: string; override;
    procedure SetImageIndex(Value: TImageIndex);
    procedure SetHotSpot(P: TPersistentPoint);
    procedure SetLeftCP(P: TPersistentPoint);
    procedure SetRightCP(P: TPersistentPoint);

  public
    procedure   Assign(Source: TPersistent); override;
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;

  published
    property HotSpot: TPersistentPoint read FHotSpot write SetHotSpot;
    property LeftConnectionPoint: TPersistentPoint read FLeftCP write SetLeftCP;
    property RightConnectionPoint: TPersistentPoint read FRightCP write SetRightCP;
    property ImageIndex: TImageIndex read FImageIndex write SetImageIndex default -1;
  end;

  TEzEndPointClass = class of TEzEndPoint;
  TEzEndPoints = class(TOwnedCollection)
  protected
    function  GetEndPoint(Index: Integer): TEzEndPoint;
    function  GetEndPointList : TEzEndPointList;
    procedure SetEndPoint(Index: Integer; Value: TEzEndPoint);

  public
    constructor Create(AOwner: TPersistent; AClass: TEzEndPointClass);
    destructor  Destroy; override;
    function    Add: TEzEndPoint;

    property EndPointList: TEzEndPointList read GetEndPointList;
    property Items[Index: Integer]: TEzEndPoint read GetEndPoint write SetEndPoint; default;
  end;

  TEzEndPointList = class(TComponent)
  private
    FPoints: TEzEndPoints;
    FImages: TCustomImageList;
    FImageChangeLink: TChangeLink;
    FOnChange: TNotifyEvent;

  protected
    procedure Change; virtual;
    procedure ImageListChange(Sender: TObject);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetPoints(Points: TEzEndPoints);
    procedure SetImages(Value: TCustomImageList);

    property OnChange: TNotifyEvent read FOnChange write FOnChange;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    procedure Generate;

  published
    property Images: TCustomImageList read FImages write SetImages;
    property Points: TEzEndPoints read FPoints write SetPoints;
  end;

  //=---------------------------------------------------------------------------=
  TVisiblePredecessorLine = class(TObject)
  public
    Predecessor: TPredecessor;
    Points: TPredecessorlinePoints;
    PointCount: Integer;
    constructor Create;
    procedure Assign(Line: TVisiblePredecessorLine);
  end;

  //=---------------------------------------------------------------------------=
  TVisiblePredecessorLines = class(TEzObjectList)
  protected
    Gantt: TCustomEzGanttChart;

    function Get(Index: Integer): TVisiblePredecessorLine;
    procedure Put(Index: Integer; Item: TVisiblePredecessorLine);

  public
    constructor Create(G: TCustomEzGanttChart); reintroduce;
    property Items[Index: Integer]: TVisiblePredecessorLine read Get write Put; default;
  end;

  //=---------------------------------------------------------------------------=
  // TEzGanttLineMarker
  // A TEzGanttLineMarker is a vertical line drawn on the gantt chart to mark
  // some position
  //=---------------------------------------------------------------------------=
  TEzGanttLineMarkerFlag = (lmfCanMove, lmfCanDelete, lmfIsCurrentTime);
  TEzGanttLineMarkerFlags = set of TEzGanttLineMarkerFlag;

  TEzGanttLineMarkerClass = class of TEzGanttLineMarker;
  TEzGanttLineMarker = class(TCollectionItem)
  private
    FName: WideString;
    FPosition: TDatetime;
    FPen: TPen;
    FFlags: TEzGanttLineMarkerFlags;
    FVisible: Boolean;
    FDragPos: integer;

  protected
    function  GetDisplayName: string; override;
    function  GetSelected: Boolean;
    procedure SetFlags(F: TEzGanttLineMarkerFlags);
    procedure SetName(N: WideString);
    procedure SetPen(P: TPen);
    procedure SetPosition(P: TDatetime);
    procedure SetSelected(S: Boolean);
    procedure SetVisible(V: Boolean);

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

    // Compares this line marker to the line marker indicated by rhs.
    // The comparisson is done by the value of position.
    function    CompareTo(rhs: TEzGanttLineMarker): integer;

    // Maintains the screen position of this line marker during
    // drag operation.
    property DragPos: integer read FDragPos;

  published
    // Flags controls the behaviour of this line marker
    property Flags: TEzGanttLineMarkerFlags read FFlags write SetFlags
              default [lmfCanMove, lmfCanDelete];

    // Name contains a descriptive text about this line marker
    property Name: WideString read FName write SetName;

    // The position at which this line marker will be drawn
    property Position: TDateTime read FPosition write SetPosition;

    // Pen stores the visual properties of this line marker
    property Pen: TPen read FPen write SetPen;

    // Selected indicates the selected state of his line marker
    property Selected: Boolean read GetSelected write SetSelected;

    // Visible indicates whether this line marker is currently visible in the Gantt chart
    property Visible: Boolean read FVisible write SetVisible default false;
  end;

  //
  // This class maintains a collection of TEzGanttLineMarker objects on behave
  // of the TEzGanttLineMarkersHelper class.
  //
  TEzGanttLineMarkers = class(TCollection)
  private
    FOwner: TCustomEzGanttChart;

  protected
    function  GetLineMarker(Index: Integer): TEzGanttLineMarker;
    procedure SetLineMarker(Index: Integer; Value: TEzGanttLineMarker);
    function  GetOwner: TPersistent; override;
    procedure Update(Item: TCollectionItem); override;

  public
    constructor Create(AOwner: TCustomEzGanttChart; MarkerClass: TEzGanttLineMarkerClass);
    function    Add: TEzGanttLineMarker;
    property    Items[Index: Integer]: TEzGanttLineMarker read GetLineMarker write SetLineMarker; default;
  end;

  //
  // This class maintains an array of TEzGanttLineMarker references on behave
  // of the TEzGanttLineMarkersHelper class. The array is sorted on the
  // position of the TEzGanttLineMarker objects.
  //
  TEzLineMarkersArray = class(TEzBaseArray)
  protected
    procedure PutItem(index: Integer; value: TEzGanttLineMarker);
    function GetItem(index: Integer): TEzGanttLineMarker;

  public
    constructor Create(itemcount, dummy: Integer); override;
    function  CompareDates(var Item1; D: TDateTime): integer;
    function  CompareItems(var Item1, Item2): Integer; override;
    function  UpperBound(var Index: integer; D: TDateTime) : Boolean;

    property Items[Index: Integer]: TEzGanttLineMarker read GetItem write PutItem; default;
  end;

  //
  // TLineMarkers is a helper class for maintaining Linemarker data
  //
  TEzGanttLineMarkersHelper = class(TPersistent)
  private
    FLineMarkers: TEzGanttLineMarkers;        // Collection of line markers
    FVisibleLineMarkers: TEzLineMarkersArray; // Array of visible line markers, sorted by date.
    FSelectedLineMarkers: TList;              // List of selected line markers

    FHighlightColor: TColor;
    FOwner: TCustomEzGanttChart;
    FVisible: Boolean;

  protected
    procedure SetLineMarkers(LM: TEzGanttLineMarkers);
    procedure SetVisible(V: Boolean);

  public
    constructor Create(AOwner: TCustomEzGanttChart); virtual;
    destructor  Destroy; override;

    procedure Assign(Source: TPersistent); override;

    // Removes all selected line markers from
    // the Lines collection.
    procedure DeleteSelected;

    // Deselects the line marker indicated by Marker.
    // DeselectLineMarker does nothing if Marker is not selected, otherwise
    // Marker is removed from the Selection List and the Gantt chart
    // is Invalidated.
    procedure DeselectLineMarker(Marker: TEzGanttLineMarker);

    // Returns whether Marker is currently selected
    function  IsSelected(Marker: TEzGanttLineMarker): Boolean;

    // Selects the line marker indicated by Marker.
    // SelectLineMarker does nothing if Marker is already selected, otherwise
    // Marker is added to the Selection List and the Gantt chart is Invalidated.
    procedure SelectLineMarker(Marker: TEzGanttLineMarker);

    // The TEzGanttChart object to which this TEzGanttLineMarkersHelper object
    // belongs.
    property Owner: TCustomEzGanttChart read FOwner;

    // Selected maintains a list of TEzGanttLineMarkers that are currently
    // selected.
    property Selected: TList read FSelectedLineMarkers;

  published
    // HighlightColor indicates the color a line marker should have when
    // it is selected.
    property HighlightColor: TColor read FHighlightColor write FHighlightColor
                default clHighlight;

    // Lines holds the collection of line markers
    property Lines: TEzGanttLineMarkers read FLineMarkers write SetLineMarkers;

    // Visible indicates whether line markers should be drawn.
    property Visible: Boolean read FVisible write SetVisible default true;
  end;

  //=---------------------------------------------------------------------------=
  // Helper object used for storing variables controlling
  // 'overlapping bar detection'.
  //=---------------------------------------------------------------------------=
  TOverlapMode = (
      omNone,             // Do not detect overlapping bars
{
      Not supported yet; only omHatchBackground is supported
      omHatchBar,         // Hatch bar area that overlaps with other bar
      omHatchBarComplete, // Completely hatch a bar that overlaps
}
      omHatchBackground  // Hatch the backgroun behind a bar that overlaps
//      omUseTwinRows
//    omShiftBars         // Shift bars vertically when they overlap
  );

  TEzOverlapSettings = class(TPersistent)
  private
    FMode: TOverlapMode;
    FHatchFill: TBrush;

  public
    constructor Create;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;

  published
    property Mode: TOverlapMode read FMode write FMode default omNone;
    property HatchFill: TBrush read FHatchFill write FHatchFill;
  end;

  TZeroProgressOption = (zpSkipRow, zpAttachToStart);
  TEzProgresslineOption = (poLeadInLeadOut, poPaintOnTop);
  TEzProgresslineOptions = set of TEzProgresslineOption;

  TEzProgresslineSettings = class(TPersistent)
  protected
    FGanttChart: TCustomEzGanttChart;
    FVisible: Boolean;
    FPen: TPen;
    FProgresslineField: String;
    FAttachToBar: String;
    FAttachToBarReference: TEzGanttbar;
    FWhenProgressIsZero: TZeroProgressOption;
    FOptions: TEzProgresslineOptions;
    FProgressLineDate: TDateTime;
    FVOffset: Integer;
    FGapSize: Integer;

    function  GetBarReference: TEzGanttbar;
    procedure SetAttachToBar(Value: String);
    procedure SetGapSize(Value: Integer);
    procedure SetPen(Value: TPen);
    procedure SetVisible(Value: Boolean);
    procedure SetProgressLineDate(Value: TDateTime);
    procedure SetProgresslineField(Value: String);
    procedure SetOptions(Value: TEzProgresslineOptions);
    procedure SetVOffset(Value: Integer);
    procedure SetWhenProgressIsZero(Value: TZeroProgressOption);

  public
    constructor Create(Owner: TCustomEzGanttChart); virtual;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;

    property BarReference: TEzGanttbar read GetBarReference;
    property GanttChart: TCustomEzGanttChart read FGanttChart;

  published
    property GapSize: Integer read FGapSize write SetGapSize default 10;
    property Pen: TPen read FPen write SetPen;
    property Visible: Boolean read FVisible write SetVisible default False;
    property Options: TEzProgresslineOptions read FOptions write SetOptions
        default [poLeadInLeadOut, poPaintOnTop];
    property ProgressLineDate: TDateTime read FProgressLineDate write SetProgressLineDate;
    property ProgresslineField: String read FProgresslineField write SetProgresslineField;
    property AttachToBar: String read FAttachToBar write SetAttachToBar;
    property VOffset: Integer read FVOffset write SetVOffset default 0;
    property WhenProgressIsZero: TZeroProgressOption read FWhenProgressIsZero write SetWhenProgressIsZero default zpSkipRow;
  end;

  //=----------------------------------------------------------------------------=
  // TEzGanttScrollBarOptions
  //=----------------------------------------------------------------------------=
  TEzGanttScrollBarOptions = class(TPersistent)
  private
    FAlwaysVisible: TScrollStyle;
    FOwner: TCustomEzGanttChart;
    FScrollBars: TScrollStyle;                   // used to hide or show vertical and/or horizontal scrollbar
    FLeftExtend, FRightExtend: TDateTime;
    procedure SetAlwaysVisible(Value: TScrollStyle);
    procedure SetScrollBars(Value: TScrollStyle);

  protected
    function GetOwner: TPersistent; override;
    procedure SetLeftExtend(Value: TDateTime);
    procedure SetRightExtend(Value: TDateTime);

  public
    constructor Create(AOwner: TCustomEzGanttChart);
    procedure Assign(Source: TPersistent); override;

    procedure ResetExtends;

  published
    property AlwaysVisible: TScrollStyle read FAlwaysVisible write SetAlwaysVisible default ssNone;
    property LeftExtend: TDateTime read FLeftExtend write SetLeftExtend;
    property RightExtend: TDateTime read FRightExtend write SetRightExtend;
    property ScrollBars: TScrollStyle read FScrollbars write SetScrollBars default ssBoth;
  end;

  //=---------------------------------------------------------------------------=
  // TGanttTimebarLink establishes the link between a TEzTimebar and a
  // TEzGanttChart component.
  //=---------------------------------------------------------------------------=
  TGanttTimebarLink = class(TEzTimebarLink)
    FGantt: TCustomEzGanttChart;

  public
    constructor Create(AGantt: TCustomEzGanttChart);
    destructor  Destroy; override;

  protected
    procedure LayoutChanged; override;
    procedure DateChanged(OldDate: TDateTime); override;
    procedure UpdateState; override;
  end;

  //=---------------------------------------------------------------------------=
  // TGanttDataLink establishes the link between a TEzDataset and a
  // TEzGanttChart component.
  //=---------------------------------------------------------------------------=
  TGanttDataLink = class(TDataLink)
  private
    FGantt: TCustomEzGanttChart;
    FSlaveMode: Boolean;

  protected
    procedure ActiveChanged; override;
    {$IFDEF XE2}
    procedure DataEvent(Event: TDataEvent; Info: NativeInt); override;
    {$ELSE}
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    {$ENDIF}
    procedure DataSetChanged; override;
    procedure DataSetScrolled(Distance: Integer); override;
    procedure SetBufferCount(Value: Integer); override;
    procedure SetSlaveMode(Value: Boolean);

  public
    constructor Create(AGantt: TCustomEzGanttChart);

    property SlaveMode: Boolean read FSlaveMode write SetSlaveMode default False;
  end;

  TCustomEzGanttChart = class(TCustomControl)
  private
    FIsDeleting: Boolean;
    FDrawBitmap: TBitmap;
    FBorderStyle: TBorderStyle;
    FMinDate: TDateTime;                    // Date where hatching stops (i.e. projects start date)
    FTruncMinDate: TDateTime;               // Min date truncated to the nearest main scale interval
    FMaxDate: TDateTime;                    // Date where hatching begins (i.e. projects end date)
    FTimeLinePos: Integer;                  // Current position of timeline if visible
    FTimeLine: TEzGanttLineMarker;          // line marker indicating current time
    FGanttbars: TEzGanttBars;               // Collection of TEzGanttbar objects
    FSelectedGanttBars: TEzGanttbarArray;   // Collection of bars selected by the user
    FSelectionRectangle: TRect;             // Rectangle used when selecting multiple bars using
                                            // a rectangular selection
    FDataLink: TGanttDataLink;              // Datalink to the datasource
    FTimebarLink: TGanttTimebarLink;        // (data-)link to the timescale
    FHitMargin: integer;                    // Margin used when testing for a hit
    FVisibleRowCount: Integer;

    FRowColor: TColor;                      // Backgroundcolor of gantt rows
    FPaintDragbar: Boolean;                 // Paint bar being dragged in Paint event or not?
    FSnapCount: integer;                    // Holds snap count (i.e. 5)
    FSnapScale: TScale;                     // Holds snap scale (scHours)
    FLineMarkers: TEzGanttLineMarkersHelper;// Linemarkers object
    FEndPoints: TEzEndPointList;            // EndPoints collection

    // Variables used to control drag operations on gantt bars
    FDragObject: TEzGanttDragObject;         // DragObject used to manage dragging
                                             // outside a real drag and drop operation.
    FActiveDragOperation: TEzGanttDragOperations;
    FDragEffect: TEzGanttHitPosition;        // Indicates the drageffect currently visible on screen
    FDragHitInfo: TEzGanttHitInfo;           // Hit information about current drag position
    FDragThreshold: Integer;                 // Distance the mouse must be moved before draging starts
    FDragOperations: TEzGanttDragOperations;
    FDragStartNode: TRecordNode;            // Record node where dragging started
    FDragStartBar: TScreenbar;              // Pointer to a cached Screenbar structure
                                            // or a pointer to a TEzGanttBar object
                                            // when dd'ing.
    FDropTargetNode: TRecordNode;           // Drop target RecordNode
    FDropTargetBar: TScreenbar;             // Drop target gantt bar, if any
    FDragBitmap: TBitmap;
    FDragOriginal: TBitmap;                 // Copy of screen area under image being dragged
    FDragPoints: TPredecessorlinePoints;    // Points buffer storing points of a dragged predecessor line
    FDragPointCount: integer;               // Number of points actually stored in FDragPoints
    FDragType: TGanttBarDragType;           // Type of dragging taking place
    FDragMarker: TEzGanttLineMarker;        // Indicates line marker being dragged
    FDragOperationStarting: Boolean;
    FDragRect: TRect;

    FMouseBarOffset: TPoint;                // Position of mouse cursor, relative to bar being dragged
    FMouseDownHitInfo: TEzGanttHitInfo;     // Hit information from where mouse was pressed

    FUpdateCount: Integer;
    FPredecessorLinesVisible: Integer;
    FOptions: TGanttOptions;
    FDefaultRowHeight: Integer;
    FGridLineWidth: Integer;
    FGridLinePen: TPen;
    FHatchOptions: THatchOptions;

    FDrawInfo: TGanttDrawInfo;              // structure with cached draw info
    FPredecessorLine: TPredecessorLine;     // property values for drawing predecessor lines
    FOverlapping: TEzOverlapSettings;       // Settings that control what to do when bars overlap
    FProgressLine: TEzProgresslineSettings;   // Settings that control painting of progress lines
    FScrollbarOptions: TEzGanttScrollBarOptions;
    FRestoreAccess: Boolean;
    FShadowRowsChanged: Boolean;

    // Hold predecessor lines that are visible on screen
    FVisiblePredecessorLines: TVisiblePredecessorLines;
    FSelectedPredecessorLine: TVisiblePredecessorLine;

    { events }
    FOnClearCacheRow: TClearCacheRowEvent;
    FGetBarProps: TGetBarPropsEvent;
    FOnMeasureRow: TMeasureRowEvent;
    FOnGetPredecessor: TGetPredecessorEvent;
    FOnGetPredecessorVisibility: TCheckPredecessorVisibilityEvent;
    FOnBarDragging: TBarDraggingEvent;
    FOnLineMarkerStartDrag: TLineMarkerStartDragEvent;
    FOnLineMarkerEndDrag: TLineMarkerEndDragEvent;
    FOnLineMarkerDragging: TLineMarkerDraggingEvent;
    FBeforeLineMarkerDelete: TLineMarkerDeleteEvent;
    FOnSelectGanttbar: TSelectGanttbarEvent;
    FOnDeselectGanttbar: TSelectGanttbarReferenceEvent;
    FOnSelectPredecessorLine: TSelectPredecessorLineEvent;
    FOnDeselectPredecessorLine: TDeselectPredecessorLineEvent;
    FOnSelectionChanged: TNotifyEvent;
    FOnStartbarDrag: TStartbarDragEvent;
    FOnEndbarDrag: TEndbarDragEvent;
    FOnBarDataChanged: TBardataChangedEvent;
    FOnDragOver: TEzGanttDragEvent;
    FOnDragDrop: TEzGanttDragEvent;
    FOnGetHint: TEzGanttGetHintEvent;
    FOnOverGanttChart: TEzOverGanttChartEvent;
    FOnPrepareCacheRow: TEzOnPrepareCacheRowEvent;
    FOnDrawBegin: TDrawBeginEvent;
    FOnDrawBackGround: TNotifyEvent;
    FOnDrawRows: TNotifyEvent;
    FOnDrawEnd: TNotifyEvent;
    FOnLinkTasks: TOnLinkTasksEvent;
    FOnMoveTask: TOnMoveTaskEvent;

  protected
    // =--------- Drag and drop functions --------------=
    procedure CMDrag(var Message: TCMDrag); message CM_DRAG;
    procedure DragOver(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: LongInt); reintroduce; virtual;
    procedure DragDrop(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: LongInt); reintroduce; virtual;

    procedure DragPositionInfo(EzDragObject: TEzGanttDragObject;
                          X, Y: Integer;
                          var HitInfo: TEzGanttHitInfo;
                          var Operations: TEzGanttDragOperations;
                          var Relation: TEzPredecessorRelation);

    procedure DoDragOver(Source: TObject; State: TDragState;
                         Pt: TPoint; HitInfo: TEzGanttHitInfo;
                         var Operations: TEzGanttDragOperations;
                         var Relation: TEzPredecessorRelation; var Accept: Boolean);

    procedure DoDragDrop(Source: TObject; State: TDragState;
                         Pt: TPoint; HitInfo: TEzGanttHitInfo;
                         var Operations: TEzGanttDragOperations;
                         var Relation: TEzPredecessorRelation; var Accept: Boolean);
    procedure DoEndDrag(Target: TObject; X, Y: Integer); override;
    procedure DoStartDrag(var DragObject: TDragObject); override;
    procedure DoOnOverGanttChart(HitInfo: TEzGanttHitInfo); virtual;

    //
    // General functions
    //

    // Called when the Ganttchart becomes active
    procedure Activate; virtual;
    function  AcquireFocus: Boolean;
    procedure Deactivate; virtual;
    function  BarDragging(X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType): Boolean; virtual;
    procedure BeginUpdate;
    function  CalcVisibleRowCount: Integer;
    function  CanUseTimebar: Boolean;
    procedure ClearDragEffects;
    procedure CheckDataset;
    procedure CreateWnd; override;
    procedure DoClearCacheRow(ARow: integer); virtual;
    procedure DoContextPopup(MousePos: TPoint; var Handled: Boolean); override;
    procedure DataSetChanged;
    procedure DeselectPredecessorline;
    function  DoBeforeLineMarkerDelete(LineMarker: TEzGanttLineMarker): Boolean; virtual;
    procedure DoBardataChanged(BarInfo: TScreenbar; DragType: TGanttBarDragType); virtual;
    procedure DoDrawBegin(var Erase: Boolean); virtual;
    function  DoGetHint(const HitInfo: TEzGanttHitInfo): string; virtual;
    function  DoEndbarDrag(X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType): Boolean; virtual;
    function  DoDeselectGanttbar(ABarRef: TEzGanttbarReference): Boolean; virtual;
    function  DoSelectPredecessorline(Predecessor: TPredecessor): Boolean; virtual;
    procedure DoDeselectPredecessorline(Predecessor: TPredecessor); virtual;
    procedure DoGetPredecessor(var Predecessor: TPredecessor); virtual;
    function  DoGetPredecessorLineVisibility(const Predecessor: TPredecessor; PredecessorNode, VisiblePredecessorNode, SuccessorNode, VisibleSuccessorNode: TRecordNode) : Boolean; virtual;
    procedure DoSelectionChanged; virtual;
    function  DoSelectGanttbar(ABar: TScreenbar): Boolean; virtual;
    function  DoStartbarDrag(X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType): Boolean; virtual;
    function  DoStartLineMarkerDrag(X: Integer; LineMarker: TEzGanttLineMarker): Boolean; virtual;
    function  DoEndLineMarkerDrag(X: Integer; LineMarker: TEzGanttLineMarker; var NewPosition: TDateTime): Boolean; virtual;
    procedure DoLineMarkerDragging(X: Integer; LineMarker: TEzGanttLineMarker); virtual;
    procedure DoOnPrepareCacheRow(CurrentNode: TRecordNode; RowNo: integer; aStates :TGanttbarStates);
    procedure PrepareCacheRow(ARow: Integer; ACacheRow: TScreenRow); virtual;
    procedure ResolveGanttbarConflicts(ARow: Integer; CacheRow: TScreenRow);
    procedure DrawRow(ACanvas: TCanvas;  ARect: TRect;  ARow: Integer); virtual;
    procedure DrawPredecessorLines(ACanvas: TCanvas);
    procedure DrawProgressLine(ACanvas: TCanvas);
    procedure DrawSelectedLineMarkers;
    procedure DrawTimeline(ARect: TRect);
    procedure CreateParams(var Params: TCreateParams); override;
    procedure EndUpdate;
    function  GetDataSource: TDataSource;
    function  GetSlaveMode: Boolean;
    function  GetSnapSize: string;
    function  GetTimebar: TEzTimebar;
    function  GetVisibleRowCount: Integer;
    procedure GraphicsObjectChanged(Sender: TObject);
    procedure HighlightPredecessorline;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure LineMarkersChanged; virtual;
    procedure LinkActive(Value: Boolean); virtual;
    procedure LinkTasks(Predecessor, Successor: TNodeKey; Relation: TEzPredecessorRelation); virtual;
    procedure MoveSelectedLineMarkers(Pos: Integer);
    procedure MoveTask(Location: TRecordNode; PositionInfo: TEzGanttHitPositions); virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure Paint; override;
    function  PredecessorLinesShowing: Boolean;
    procedure RunTimer(idTimer, Interval: integer);
    procedure ScrollVertical(Distance: Integer); virtual;
    procedure ScrollClientRect(Distance: Integer);
    function  SetBarProps(RowNode: TRecordNode; BarInfo: TScreenbar): Boolean; virtual;
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure SetDataSource(Value: TDataSource);
    procedure SetGridLinePen(Value: TPen);
    procedure SetHatchOptions(Value: THatchOptions);
    procedure SetMinDate(D: TDateTime);
    procedure SetMaxDate(D: TDateTime);
    procedure SetOptions(Value: TGanttOptions);
    procedure SetSlaveMode(Value: Boolean);
    procedure SetPredecessorLine(Source: TPredecessorLine);
    procedure SetPredecessorLinePen(Pen: TPen);
    procedure SetScrollbarOptions(Value: TEzGanttScrollBarOptions);
    procedure SetSelectionPen(ACanvas: TCanvas);
    procedure SetSnapSize(S: string);
    procedure SetGanttbars(Value: TEzGanttBars);
    procedure SetEndPoints(P: TEzEndPointList);
    procedure SetLineMarkers(LH: TEzGanttLineMarkersHelper);
    procedure SetOverLapping(O: TEzOverlapSettings);
    procedure SetProgressline(Value: TEzProgresslineSettings);
    procedure SetRowColor(Value: TColor);
    procedure SetTimebar(Bar: TEzTimebar);
    procedure StartDatasetEdit(RowNode, BarNode: TRecordNode);
    procedure StopDatasetEdit;

    procedure StopDragging;
    procedure StopTimer(idTimer: integer);
    procedure TimebarLayoutChanged;
    procedure TimebarDateChanged(OldDate: TDateTime);
    function  UpdateBarDates(Dragbar: TScreenbar; ARect: TRect): Boolean;
    procedure UpdateRowCount;
    procedure UpdateHorizontalScrollbar; virtual;
    procedure UpdateVerticalScrollBar; virtual;

    procedure UpdateTimebarState;
    function  VirtualRowCount: Integer;


    procedure CMHintShow(var Message: TCMHintShow); message CM_HINTSHOW;
    procedure CMHintShowPause(var Message: TCMHintShowPause); message CM_HINTSHOWPAUSE;
    procedure CMShowingChanged(var Message: TMessage); message CM_SHOWINGCHANGED;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SetFOCUS;
    procedure WMKillFocus(var Message: TMessage); message WM_KillFocus;
    procedure WMPrintClient(var Message: TWMPrintClient); message WM_PRINTCLIENT;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure WMHScroll(var Msg: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Msg: TWMVScroll); message WM_VSCROLL;
    procedure WMTimer(var Msg: TWMTimer); message WM_TIMER;
    procedure WMMouseWheel(var Message: TWMMouseWheel); message WM_MOUSEWHEEL;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    function  BarCanDrag(ARow, ABar: Integer): Boolean;
    function  BarRect(ARow, ABar:Integer) : TRect;
    function  CanCreateBar: TEzGanttbar;
    function  CheckGanttbarVisibility(Bar: TEzGanttBar; IsHiddenNode: Boolean) : Boolean;
    procedure InitializeDragObject(DragObject: TEzGanttDragObject; ClearDragArea: Boolean);
    procedure Invalidate; override;
    // public access because method is used by printer object.
    function  GetGanttbarDates(Bar: TEzGanttBar; var dtStart, dtStop: TDateTime) : Boolean;
    procedure GetHitTestInfoAt(X, Y: Integer; var HitInfo: TEzGanttHitInfo); virtual;
    function  GetRowAt(var Y: Integer): Integer;
    function  GetRowHeight(ARow: Integer): Integer;
    procedure LoadAllCacheRows;
    function  RecNo(ARow: integer) : integer;
    function  RowNode(ARow: integer) : TRecordNode;
    function  RowRect(ARow: Integer) : TRect;
    function  ScreenRowToCacheIndex(ARow: Integer): Integer;
    procedure SelectBarsInRectangle(ARect: TRect);
    function  SnapInterval(var Start, Stop: TDateTime): TGanttbarStates;
    function  SnapDate(var dtDate: TDateTime): Boolean;

    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsSingle;
    property DataLink: TGanttDataLink read FDataLink;
    property DragOperations: TEzGanttDragOperations read FDragOperations write FDragOperations
              default [doLinkTasks, doMoveTasks, doMoveHorizontal];
    property DragType: TGanttBarDragType read FDragType;
    property GridLinePen: TPen read FGridLinePen write SetGridLinePen;
    property HatchOptions: THatchOptions read FHatchOptions write SetHatchOptions;
    property VisibleRowCount: Integer read GetVisibleRowCount;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property DefaultRowHeight: Integer read FDefaultRowHeight write FDefaultRowHeight default 17;
    property DragStartNode: TRecordNode read FDragStartNode;
    property DragStartBar: TScreenbar read FDragStartBar;
    property DropTargetNode: TRecordNode read FDropTargetNode;
    property DropTargetBar: TScreenbar read FDropTargetBar;
    property DragThreshold: integer read FDragThreshold write FDragThreshold default 5;
    property DrawInfo: TGanttDrawInfo read FDrawInfo;
    property Ganttbars: TEzGanttBars read FGanttbars write SetGanttbars;
    property GridlineWidth: Integer read FGridLineWidth write FGridLineWidth default 1;
    property HitMargin: integer read FHitMargin write FHitMargin default 2;
    property EndPoints: TEzEndPointList read FEndPoints write SetEndPoints;
    property LineMarkers: TEzGanttLineMarkersHelper read FLineMarkers write SetLineMarkers;
    property MinDate: TDateTime read FMinDate write SetMinDate;
    property MaxDate: TDateTime read FMaxDate write SetMaxDate;
    property Font;
    property Options: TGanttOptions read FOptions write SetOptions
      default [gaoConfirmDelete, gaoPostAllEdits];
    property Overlapping: TEzOverlapSettings read FOverlapping write SetOverLapping;
    property PredecessorLine: TPredecessorLine read FPredecessorLine write SetPredecessorLine;
    property Progressline: TEzProgresslineSettings read FProgressLine write SetProgressline;
    property RowColor: TColor read FRowColor write SetRowColor default clWhite;
    property SelectedGanttBars: TEzGanttbarArray read FSelectedGanttBars;
    property SelectedPredecessorLine: TVisiblePredecessorLine read FSelectedPredecessorLine;
    property ScrollbarOptions: TEzGanttScrollBarOptions read FScrollbarOptions write SetScrollbarOptions;
    property SlaveMode: Boolean read GetSlaveMode write SetSlaveMode default false;
    property SnapCount: integer read FSnapCount;
    property SnapScale: TScale read FSnapScale;
    property SnapSize: string read GetSnapSize write SetSnapSize;
    property Timebar: TEzTimebar read GetTimebar write SetTimebar;
    property TimeLine: TEzGanttLineMarker read FTimeLine;
    property GetBarProps: TGetBarPropsEvent read FGetBarProps write FGetBarProps;
    property VisiblePredecessorLines: TVisiblePredecessorLines read FVisiblePredecessorLines;

    property OnClearCacheRow: TClearCacheRowEvent read FOnClearCacheRow write FOnClearCacheRow;
    property OnDragOver: TEzGanttDragEvent read FOnDragOver write FOnDragOver;
    property OnDragDrop: TEzGanttDragEvent read FOnDragDrop write FOnDragDrop;
    property OnDrawBegin: TDrawBeginEvent read FOnDrawBegin write FOnDrawBegin;
    property OnDrawBackGround: TNotifyEvent read FOnDrawBackGround write FOnDrawBackGround;
    property OnDrawRows: TNotifyEvent read FOnDrawRows write FOnDrawRows;
    property OnDrawEnd: TNotifyEvent read FOnDrawEnd write FOnDrawEnd;
    property OnGetHint: TEzGanttGetHintEvent read FOnGetHint write FOnGetHint;
    property OnLinkTasks: TOnLinkTasksEvent read FOnLinkTasks write FOnLinkTasks;
    property OnMoveTask: TOnMoveTaskEvent read FOnMoveTask write FOnMoveTask;
    property OnMeasureRow: TMeasureRowEvent read FOnMeasureRow write FOnMeasureRow;
    property OnGetPredecesor: TGetPredecessorEvent read FOnGetPredecessor write FOnGetPredecessor;
    property OnGetPredecessorVisibility: TCheckPredecessorVisibilityEvent
                read FOnGetPredecessorVisibility write FOnGetPredecessorVisibility;
    property OnBarDragging: TBarDraggingEvent read FOnBarDragging write FOnBarDragging;
    property OnOverGanttChart: TEzOverGanttChartEvent read FOnOverGanttChart write FOnOverGanttChart;
    property OnPrepareCacheRow: TEzOnPrepareCacheRowEvent read FOnPrepareCacheRow write FOnPrepareCacheRow;

    // Fired before a TEzGanttLineMarker object gets deleted from the Lines
    // collection. Allows the receiver to abort the deletion.
    property BeforeLineMarkerDelete: TLineMarkerDeleteEvent read FBeforeLineMarkerDelete write FBeforeLineMarkerDelete;

    // Fired when one or more line markers are going to be dragged.
    // LineMarker indicates the line the user is actualy dragging but all lines
    // stored in the Seleced list will be moved during the drag operation.
    property OnLineMarkerStartDrag: TLineMarkerStartDragEvent read FOnLineMarkerStartDrag write FOnLineMarkerStartDrag;

    // Fired when dragging ends of one or more line markers.
    property OnLineMarkerEndDrag: TLineMarkerEndDragEvent read FOnLineMarkerEndDrag write FOnLineMarkerEndDrag;

    // Fired during drag operations of one or more line markers.
    property OnLineMarkerDragging: TLineMarkerDraggingEvent read FOnLineMarkerDragging write FOnLineMarkerDragging;
    property OnSelectionChanged: TNotifyEvent read FOnSelectionChanged write FOnSelectionChanged;
    property OnSelectGanttbar: TSelectGanttbarEvent read FOnSelectGanttbar write FOnSelectGanttbar;
    property OnDeselectGanttbar: TSelectGanttbarReferenceEvent read FOnDeselectGanttbar write FOnDeselectGanttbar;
    property OnSelectPredecessorLine: TSelectPredecessorLineEvent read FOnSelectPredecessorLine write FOnSelectPredecessorLine;
    property OnDeselectPredecessorLine: TDeselectPredecessorLineEvent read FOnDeselectPredecessorLine write FOnDeselectPredecessorLine;
    property OnStartbarDrag: TStartbarDragEvent read FOnStartbarDrag write FOnStartbarDrag;
    property OnBarDataChanged: TBardataChangedEvent read FOnBarDataChanged write FOnBarDataChanged;
    property OnEndbarDrag: TEndbarDragEvent read FOnEndbarDrag write FOnEndbarDrag;
  end;

  TEzGanttChart = class(TCustomEzGanttChart)
  public
    property Canvas;

  published
    property Align;
    property BorderStyle;
    property Color;
    property Ctl3D;
    property DataSource;
    property DefaultRowHeight;
    property DragCursor;
    property DragKind;
    property DragMode;
    property DragOperations;
    property DragThreshold;
    property Enabled;
    property GridlineWidth;
    property GridLinePen;
    property HatchOptions;
    property MinDate;
    property MaxDate;
    property Ganttbars;
    property Hint;
    property Font;
    property EndPoints;
    property LineMarkers;
    property Options;
    property Overlapping;
    property PredecessorLine;
    property Progressline;
    property ParentColor;
    property ParentCtl3D;
    property ParentShowHint;
    property PopupMenu;
    property RowColor;
    property ScrollbarOptions;
    property ShowHint;
    property SlaveMode;
    property SnapSize;
    property TabOrder;
    property TabStop;
    property Timebar;
    property Visible;

    property GetBarProps;
    property OnClearCacheRow;
    property OnDblClick;
    property OnDrawBegin;
    property OnDrawBackGround;
    property OnDrawRows;
    property OnDrawEnd;
    property OnEndDrag;
    property OnGetHint;
    property OnGetPredecesor;
    property OnGetPredecessorVisibility;
    property OnKeyDown;
    property OnMeasureRow;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;        
    property OnLinkTasks;
    property OnMoveTask;
    property OnDragDrop;
    property OnDragOver;
    property OnStartDrag;
    property OnStartDock;
    property OnBarDataChanged;
    property OnBarDragging;
    property BeforeLineMarkerDelete;
    property OnLineMarkerStartDrag;
    property OnLineMarkerEndDrag;
    property OnLineMarkerDragging;
    property OnSelectionChanged;
    property OnSelectGanttbar;
    property OnDeselectGanttbar;
    property OnSelectPredecessorLine;
    property OnDeselectPredecessorLine;
    property OnStartbarDrag;
    property OnEndbarDrag;
    property OnOverGanttChart;
    property OnPrepareCacheRow;
  end;

  TEzGanttChartNavigator = class(TCustomControl)
  protected
    FGantt: TCustomEzGanttChart;

    procedure Paint; override;
  end;

  procedure PaintGanttbar(Bitmap: TBitmap; EndPoints: TEzEndPointList;
                       ClipRect, ActualSize: TRect; var BoundRect, ClickRect, BarRect: TRect;
                       UpdateRects: Boolean; Ganttbar: TEzGanttBar;
                       AState: TGanttbarStates); overload;

  procedure PaintGanttbar(ACanvas: TCanvas; EndPoints: TEzEndPointList;
                       ClipRect, ActualSize: TRect; var BoundRect, ClickRect, BarRect: TRect;
                       UpdateRects: Boolean; Ganttbar: TEzGanttBar;
                       AState: TGanttbarStates); overload;

  procedure PaintPredecessorLine(
        ACanvas: TCanvas;
        EndPoints: TEzEndPointList;
        PredecessorLine: TPredecessorLine;
        LineInfo: TVisiblePredecessorLine;
        StartPoint, EndPoint: TPoint;
        RowHeight: Integer);

var
  // Cursor constant referring to 'Drag Gantt bar' cursor
  crDragGanttbar: Integer;
  // Cursor constant used when user moves a single point of a taskbar
  crMoveDate: Integer;
  // Cursor constant referring to ADDBAR cursor
  crAddBar: Integer;
  // Cursor constant referring to TASKPROGRESS cursor
  crTaskProgress: Integer;
  // Cursor constant referring to 'free moving' cursor
  crCanMovebar: Integer;
  // Cursor constant referring to 'insert child task' cursor
  crAddChildTask: Integer;
  // Cursor constant referring to 'connect task' cursor
  crConnectTask: Integer;
  // Cursor constant referring to 'delete task' cursor
  crDeleteTask: Integer;
  // Cursor constant referring to 'remove child task' cursor
  crRemoveChildTask: Integer;
  // Cursor constant used when user moves a complete taskbar
  crMoveBar: Integer;
  // Cursor constant used when user moves a complete taskbar
  crMoveLineMarker: Integer;
  // Cursor constant for drag cursor INSERTBEFORE
  crInsertBefore: Integer;
  // Cursor constant for drag cursor INSERTAFTER
  crInsertAfter: Integer;
  // Cursor constant for drag cursor INSERTASCHILD
  crInsertAsChild: Integer;
  // Cursor constant for drag cursor PREDECESSORLINE
  crPredecessorline: Integer;

  SelectionSpace: Integer = 5;
  SnapPoint: TBitmap;
  SelectionPoint: TBitmap;
  SnapPointCount: Integer;
  EmptyEndPoint: TEzEndPoint;

implementation

uses EzStrings_3, EzPainting {$IFDEF EZ_D6}, Variants{$ENDIF};

{$R EzGantt.RES}

procedure EzGanttError(const Message: string; Gantt: TCustomEzGanttChart = nil);
begin
  if Assigned(Gantt) then
    raise EEzGanttError.Create(Format('%s: %s', [Gantt.Name, Message])) else
    raise EEzGanttError.Create(Message);
end;

procedure AllocBitmaps;
begin
  if SnapPointCount = 0 then
  begin
    SelectionPoint := TBitmap.Create;
    SelectionPoint.LoadFromResourceName(hInstance, 'SELECTIONPOINT');
    SnapPoint := TBitmap.Create;
    SnapPoint.LoadFromResourceName(hInstance, 'SNAPPOINT');
    EmptyEndPoint := TEzEndPoint.Create(nil);
  end;
  inc(SnapPointCount);
end;

procedure ReleaseBitmaps;
begin
  dec(SnapPointCount);
  if SnapPointCount = 0 then
  begin
    SelectionPoint.Free;
    SelectionPoint := nil;
    SnapPoint.Free;
    SnapPoint := nil;
    EmptyEndPoint.Free;
    EmptyEndPoint := nil;
  end;
end;

function AddCustomCursor(aHCursor: HCursor): Integer;
var
  i: Integer;
begin
   Result:=0;

   for i:=1 to High(TCursor) do
   begin
      if Screen.Cursors[i]=Screen.Cursors[crDefault] then
      begin
         Screen.Cursors[i]:=aHCursor;
         Result:=i;
         Break;
      end;
   end;
end;

procedure LoadScreenCursors;
begin
  crDragGanttbar := AddCustomCursor(LoadCursor(hInstance, 'DRAGGANTTBAR'));
  crCanMovebar := AddCustomCursor(LoadCursor(hInstance, 'CANMOVEBAR'));
  crMoveBar := AddCustomCursor(LoadCursor(hInstance, 'MOVEBAR'));
  crMoveDate := AddCustomCursor(LoadCursor(hInstance, 'MOVEDATE'));
  crAddBar := AddCustomCursor(LoadCursor(hInstance, 'ADDBAR'));
  crTaskProgress := AddCustomCursor(LoadCursor(hInstance, 'TASKPROGRESS'));
  crAddChildTask := AddCustomCursor(LoadCursor(hInstance, 'ADDCHILDTASK'));
  crConnectTask := AddCustomCursor(LoadCursor(hInstance, 'CONNECTTASK'));
  crDeleteTask := AddCustomCursor(LoadCursor(hInstance, 'DRAGGANTTBAR'));
  crRemoveChildTask := AddCustomCursor(LoadCursor(hInstance, 'REMOVECHILDTASK'));
  crMoveLineMarker := AddCustomCursor(LoadCursor(hInstance, 'MOVELINEMARKER'));
  crInsertBefore := AddCustomCursor(LoadCursor(hInstance, 'INSERTBEFORE'));
  crInsertAfter := AddCustomCursor(LoadCursor(hInstance, 'INSERTAFTER'));
  crInsertAsChild := AddCustomCursor(LoadCursor(hInstance, 'INSERTASCHILD'));
  crPredecessorline := AddCustomCursor(LoadCursor(hInstance, 'PREDECESSOR'));

{
  Screen.Cursors[crDragGanttbar] := LoadCursor(hInstance, 'DRAGGANTTBAR');
  Screen.Cursors[crCanMovebar] := LoadCursor(hInstance, 'CANMOVEBAR');
  Screen.Cursors[crMoveBar] := LoadCursor(hInstance, 'MOVEBAR');
  Screen.Cursors[crMoveDate] := LoadCursor(hInstance, 'MOVEDATE');
  Screen.Cursors[crAddBar] := LoadCursor(hInstance, 'ADDBAR');
  Screen.Cursors[crTaskProgress] := LoadCursor(hInstance, 'TASKPROGRESS');
  Screen.Cursors[crAddChildTask] := LoadCursor(hInstance, 'ADDCHILDTASK');
  Screen.Cursors[crConnectTask] := LoadCursor(hInstance, 'CONNECTTASK');
  Screen.Cursors[crDeleteTask] := LoadCursor(hInstance, 'DRAGGANTTBAR');
  Screen.Cursors[crRemoveChildTask] := LoadCursor(hInstance, 'REMOVECHILDTASK');
  Screen.Cursors[crMoveLineMarker] := LoadCursor(hInstance, 'MOVELINEMARKER');
  Screen.Cursors[crInsertBefore] := LoadCursor(hInstance, 'INSERTBEFORE');
  Screen.Cursors[crInsertAfter] := LoadCursor(hInstance, 'INSERTAFTER');
  Screen.Cursors[crInsertAsChild] := LoadCursor(hInstance, 'INSERTASCHILD');
}
end;

procedure PaintGanttbar(Bitmap: TBitmap; EndPoints: TEzEndPointList;
                       ClipRect, ActualSize: TRect; var BoundRect, ClickRect, BarRect: TRect;
                       UpdateRects: Boolean; Ganttbar: TEzGanttBar;
                       AState: TGanttbarStates);
var
  Off: TPoint;
  InterSect, Actual, Bound, Click, Bar: TRect;

begin
  if UpdateRects then
    PaintGanttbar(TCanvas(nil), EndPoints, ClipRect, ActualSize, BoundRect, ClickRect, BarRect, UpdateRects, Ganttbar, AState);

  if Assigned(Bitmap) then
  begin
    Actual := ActualSize;
    Bound := BoundRect;
    Click := ClickRect;
    Bar := BarRect;

    IntersectRect(InterSect, ClipRect, BoundRect);
    Off := InterSect.TopLeft;
    OffsetRect(InterSect, -Off.x, -Off.y);
    OffsetRect(Actual, -Off.x, -Off.y);
    OffsetRect(Bound, -Off.x, -Off.y);
    OffsetRect(Click, -Off.x, -Off.y);
    OffsetRect(Bar, -Off.x, -Off.y);

    Bitmap.Width := max(Bitmap.Width, InterSect.Right);
    Bitmap.Height := max(Bitmap.Height, InterSect.Bottom);
    Bitmap.Canvas.Brush.Style := bsSolid;
    Bitmap.Canvas.Brush.Color := EzTransparentColor;
    Bitmap.Canvas.FillRect(Rect(0, 0, InterSect.Right, InterSect.Bottom));
    PaintGanttbar(Bitmap.Canvas, EndPoints, InterSect, Actual, Bound, Click, Bar, False, Ganttbar, AState);
  end;
end;

procedure PaintGanttbar(ACanvas: TCanvas; EndPoints: TEzEndPointList;
                       ClipRect, ActualSize: TRect; var BoundRect, ClickRect, BarRect: TRect;
                       UpdateRects: Boolean; Ganttbar: TEzGanttBar;
                       AState: TGanttbarStates);
var
  RowCenter, ActualBarHeight, ImagesWidth: Integer;

  _BoundRect, _BarRect, _ClickRect, RTemp: TRect;

  procedure PaintEndPoint(DX: integer; GanttbarPoint: TGanttbarEndPoint; HotSpotY: Integer);
  var
    VOffset: Integer;

  begin
    if (DX > ClipRect.Right) or (DX+ImagesWidth < ClipRect.Left) then Exit;
    with GanttbarPoint, EndPoints.Points[GanttbarPoint.EndPointIndex] do
    begin
      VOffset := 0;
      case Alignment of
        gaRowBottom: VOffset := ActualSize.Bottom-HotSpotY;
        gaRowCentered: VOffset := ActualSize.Top+RowCenter-HotSpotY;
        gaRowTop: VOffset := ActualSize.Top-HotSpotY;
        gaAbsCenter: VOffset := ActualSize.Top+RowCenter-EndPoints.Images.Height div 2;
        gaBarBottom: VOffset := _BarRect.Bottom-HotSpotY;
        gaBarCentered: VOffset := _BarRect.Top+(_BarRect.Bottom-_BarRect.Top) div 2 - HotSpotY;
        gaBarTop: VOffset := _BarRect.Top-HotSpotY;
      end;

      EndPoints.Images.Draw(ACanvas, DX, VOffset, ImageIndex);
    end;
  end;

var
  Left, Right: TEzEndPoint;
  MileStone: Boolean;

begin
  Left := EmptyEndPoint;
  Right := EmptyEndPoint;
  ImagesWidth := 0;
  MileStone := ActualSize.Left >= ActualSize.Right;
  RowCenter := (ActualSize.Bottom-ActualSize.Top) div 2;

  with Ganttbar do
  begin
    if Assigned(EndPoints) and Assigned(EndPoints.Images) then
    begin
      ImagesWidth := EndPoints.Images.Width;
      if LeftEndPoint.EndPointIndex <> -1 then
        Left := EndPoints.Points[LeftEndPoint.EndPointIndex];

      if not MileStone and (RightEndPoint.EndPointIndex <> -1) then
        Right := EndPoints.Points[RightEndPoint.EndPointIndex];
    end;

    if UpdateRects then
      //
      // Recalculate ganttbar rectangles
      //
    begin
      if BarHeight > 0 then { >0 means absolute height, <0 means % of rowheight }
        ActualBarHeight := BarHeight else
        ActualBarHeight := ((ActualSize.Bottom-ActualSize.Top)*-BarHeight) div 100;

      if not MileStone then
        //
        // Paint ganttbar
        //
      begin
        _BarRect.Left := ActualSize.Left;
        _BarRect.Right := ActualSize.Right;
        _BarRect.Top := ActualSize.Top + RowCenter + BarPosition - ActualBarHeight div 2;
        _BarRect.Bottom := _BarRect.Top + ActualBarHeight;

        _BoundRect := ActualSize;
        _ClickRect := _BarRect;

        if Left <> EmptyEndPoint then
        begin
          _BarRect.Left   := ActualSize.Left+Left.RightConnectionPoint.x-Left.HotSpot.x;
          _BoundRect.Left := min(_BarRect.Left, ActualSize.Left-Left.HotSpot.x);
          _ClickRect.Left := ActualSize.Left-(Left.HotSpot.x-Left.LeftConnectionPoint.x);
        end;

        if Right <> EmptyEndPoint then
        begin
          _BarRect.Right   := ActualSize.Right-(Right.HotSpot.x - Right.LeftConnectionPoint.x);
          _BoundRect.Right := max(_BarRect.Right, ActualSize.Right+ImagesWidth-Right.LeftConnectionPoint.x);
          _ClickRect.Right := ActualSize.Right+(Right.RightConnectionPoint.x-Right.HotSpot.x);
        end;

        if sfSelected in AState then
        begin
          _BoundRect.Left := min(_BoundRect.Left, _ClickRect.Left-SelectionPoint.Width-1);
          _BoundRect.Right := max(_BoundRect.Right, _ClickRect.Right+SelectionPoint.Width+1);
        end;
      end
      else
        //
        // Paint milestone
        //
      begin
        _BarRect.Left := ActualSize.Left;
        _BarRect.Right := ActualSize.Left;
        _BarRect.Top := ActualSize.Top + RowCenter;
        _BarRect.Bottom := _BarRect.Top;

        _BoundRect := ActualSize;
        _ClickRect := ActualSize;

        dec(_BoundRect.Left, ImagesWidth div 2);
        _BoundRect.Right := _BoundRect.Left + ImagesWidth;

        if Left <> EmptyEndPoint then
        begin
          _ClickRect.Left  := ActualSize.Left-(Left.HotSpot.x-Left.LeftConnectionPoint.x);
          _ClickRect.Right := ActualSize.Left+(Left.RightConnectionPoint.x-Left.HotSpot.x);
        end
        else
          _ClickRect := _BoundRect;

        if sfSelected in AState then
        begin
          _BoundRect.Left := min(_BoundRect.Left, _ClickRect.Left-SelectionPoint.Width-1);
          _BoundRect.Right := max(_BoundRect.Right, _ClickRect.Right+SelectionPoint.Width+1);
        end;
      end;
    end // if UpdateRects
    else
      //
      // UpdateRects = False ==> Rectangles can be reused from a previous call
      //
    begin
      _BoundRect := BoundRect;
      _ClickRect := ClickRect;
      _BarRect := BarRect;
    end;

    if ACanvas <> nil then
    begin
      if not MileStone then
      begin
        ACanvas.Pen := BarPen;
        ACanvas.Brush := BarFill;
        SetRect(
              RTemp,
              max(ClipRect.Left-BarPen.Width-1, _BarRect.Left),
              _BarRect.Top,
              min(ClipRect.Right+BarPen.Width+1, _BarRect.Right),
              _BarRect.Bottom
          );
        ACanvas.Rectangle(RTemp);

        if Left <> EmptyEndPoint then
          PaintEndPoint(_BarRect.Left-Left.RightConnectionPoint.x, LeftEndPoint, Left.RightConnectionPoint.y);

        if Right <> EmptyEndPoint then
          PaintEndPoint(_BarRect.Right-Right.LeftConnectionPoint.x, RightEndPoint, Right.LeftConnectionPoint.y);
      end
        //
        // Paint milestone
        //
      else if Left <> EmptyEndPoint then
        PaintEndPoint(_BarRect.Left-Left.HotSpot.x, LeftEndPoint, Left.HotSpot.y);

      if sfSelected in AState then
        with _ClickRect do
        begin
          ACanvas.Draw(Left-SelectionPoint.Width-1, Top+(Bottom-Top) div 2 - SelectionPoint.Height div 2, SelectionPoint);
          ACanvas.Draw(Right+1, Top+(Bottom-Top) div 2 - SelectionPoint.Height div 2, SelectionPoint);

          if not MileStone then
          begin
            ACanvas.Draw(Left + (Right-Left) div 2 - SelectionPoint.Width div 2, Top-SelectionPoint.Height-1, SelectionPoint);
            ACanvas.Draw(Left + (Right-Left) div 2 - SelectionPoint.Width div 2, Bottom+1, SelectionPoint);
          end;
        end;
      end;

    if UpdateRects then
    begin
      BoundRect := _BoundRect;
      ClickRect := _ClickRect;
      BarRect := _BarRect;
    end;
  end;
end;

function GetRectH(const R: TRect): Integer;
begin
  Result := R.Bottom-R.Top;
end;

function GetRectW(const R: TRect): Integer;
begin
  Result := R.Right-R.Left;
end;

function GetPredecessorLinePoints(
    PredecessorLine: TPredecessorLine;
    var Points: TPredecessorlinePoints;
    Relation: TEzPredecessorRelation; RowHeight: integer) : integer;

  function sign(val: integer; sign: Boolean) : integer;
  begin
    if sign then
      Result := val else
      Result := -val;
  end;

var
  vdistance: Integer;
  StartPoint, EndPoint: TPoint;

begin
  with PredecessorLine do
  begin
    result := 0;
    StartPoint := Points[0];
    EndPoint := Points[1];

    vdistance := EndPoint.y - StartPoint.y;

    if vdistance <> 0 then

    case Relation of
      // U-turn line required
      prStartStart, prSameStart:
      begin
        Points[0].x := StartPoint.x;
        Points[0].y := StartPoint.y + sign(StartOffset, vdistance>0);
        Points[1].x := min(StartPoint.x-StartLength, EndPoint.x-EndLength);
        Points[1].y := Points[0].y;
        Points[2].x := Points[1].x;
        Points[2].y := EndPoint.y + sign(EndOffset, vdistance<0);
        Points[3].x := EndPoint.x;
        Points[3].y := Points[2].y;
        result := 4;
      end;

      // U-turn line required
      prFinishFinish:
      begin
        Points[0].x := StartPoint.x;
        Points[0].y := StartPoint.y + sign(StartOffset, vdistance>0);
        Points[1].x := max(StartPoint.x+StartLength, EndPoint.x+EndLength);
        Points[1].y := Points[0].y;
        Points[2].x := Points[1].x;
        Points[2].y := EndPoint.y + sign(EndOffset, vdistance<0);
        Points[3].x := EndPoint.x;
        Points[3].y := Points[2].y;
        result := 4;
      end;

      prFinishStart:
      begin
        // no horizontal line part required
        if StartPoint.x+StartLength <= EndPoint.x-EndLength then begin
          Points[0].x := StartPoint.x;
          Points[0].y := StartPoint.y + sign(StartOffset, vdistance>0);
          Points[3].x := EndPoint.x;
          Points[3].y := EndPoint.y - sign(EndOffset, vdistance>0);

          Points[1].x := StartPoint.x + (EndPoint.x-EndLength-StartPoint.x+StartLength) div 2;
          Points[1].y := Points[0].y;
          Points[2].x := Points[1].x;
          Points[2].y := Points[3].y;
          result := 4;
        end

        // create zigzag line
        else begin
          Points[0].x := StartPoint.x;
          Points[0].y := StartPoint.y + sign(StartOffset, vdistance>0);
          Points[1].x := StartPoint.x+StartLength;
          Points[1].y := Points[0].y;
          Points[2].x := Points[1].x;
          Points[2].y := EndPoint.y - sign((RowHeight shr 1)-1, vdistance>0);
          Points[3].x := EndPoint.x-EndLength;
          Points[3].y := Points[2].y;
          Points[4].x := Points[3].x;
          Points[4].y := EndPoint.y - sign(EndOffset, vdistance>0);
          Points[5].x := EndPoint.x;
          Points[5].y := Points[4].y;
          result := 6;
        end;
      end;

      prStartFinish:
      begin
        // no horizontal line part required
        if StartPoint.x-StartLength >= EndPoint.x+EndLength then begin
          Points[0].x := StartPoint.x;
          Points[0].y := StartPoint.y + sign(StartOffset, vdistance>0);
          Points[3].x := EndPoint.x;
          Points[3].y := EndPoint.y - sign(EndOffset, vdistance>0);

          Points[1].x := StartPoint.x - (StartPoint.x-StartLength-EndPoint.x+EndLength) div 2;
          Points[1].y := Points[0].y;
          Points[2].x := Points[1].x;
          Points[2].y := Points[3].y;
          result := 4;
        end

        // create zigzag line
        else begin
          Points[0].x := StartPoint.x;
          Points[0].y := StartPoint.y + sign(StartOffset, vdistance>0);
          Points[1].x := StartPoint.x-StartLength;
          Points[1].y := Points[0].y;
          Points[2].x := Points[1].x;
          Points[2].y := EndPoint.y - sign((RowHeight shr 1)-1, vdistance>0);
          Points[3].x := EndPoint.x+EndLength;
          Points[3].y := Points[2].y;
          Points[4].x := Points[3].x;
          Points[4].y := EndPoint.y - sign(EndOffset, vdistance>0);
          Points[5].x := EndPoint.x;
          Points[5].y := Points[4].y;
          result := 6;
        end;
      end;
    end

    else

    //
    // get line points for two bars one a sinlge row
    //
    case Relation of
      // create 6-points zig-zag
      prStartStart, prSameStart, prFinishFinish:
      begin
        Points[0].x := StartPoint.x;
        Points[0].y := StartPoint.y+StartOffset;
        Points[1].x := StartPoint.x+sign(StartLength, Relation=prFinishFinish);
        Points[1].y := Points[0].y;
        Points[2].x := Points[1].x;
        Points[2].y := Points[1].y-(RowHeight shr 1)+1;
        Points[3].x := EndPoint.x+sign(EndLength, Relation=prFinishFinish);
        Points[3].y := Points[2].y;
        Points[4].x := Points[3].x;
        Points[4].y := EndPoint.y+EndOffset;
        Points[5].x := EndPoint.x;
        Points[5].y := Points[4].y;
        result := 6;
      end;

      prFinishStart, prStartFinish:
        if ((Relation = prFinishStart) and (StartPoint.x < EndPoint.x)) or
           ((Relation = prStartFinish) and (StartPoint.x > EndPoint.x))
        then begin
          Points[0].x := StartPoint.x;
          Points[0].y := StartPoint.y+StartOffset;
          Points[1].x := EndPoint.x;
          Points[1].y := Points[0].y;
          result := 2;
        end
        else begin
          Points[0].x := StartPoint.x;
          Points[0].y := StartPoint.y+StartOffset;
          Points[1].x := StartPoint.x+sign(StartLength, Relation=prFinishStart);
          Points[1].y := Points[0].y;
          Points[2].x := Points[1].x;
          Points[2].y := Points[1].y-(RowHeight shr 1)+1;
          Points[3].x := EndPoint.x+sign(EndLength, Relation=prStartFinish);
          Points[3].y := Points[2].y;
          Points[4].x := Points[3].x;
          Points[4].y := EndPoint.y+EndOffset;
          Points[5].x := EndPoint.x;
          Points[5].y := Points[4].y;
          result := 6;
        end;
    end;
  end;
end;

procedure PaintPredecessorLine(
      ACanvas: TCanvas;
      EndPoints: TEzEndPointList;
      PredecessorLine: TPredecessorLine;
      LineInfo: TVisiblePredecessorLine;
      StartPoint, EndPoint: TPoint;
      RowHeight: Integer);
var
  bmp: TBitmap;
  EzEndPoint: TEzEndPoint;
  EndPointX, EndPointY: LongInt;
  EndPointRect: TRect;

begin
  with PredecessorLine do
  begin
    LineInfo.Points[0] := StartPoint;
    LineInfo.Points[1] := EndPoint;
    EzEndPoint := EmptyEndPoint;

    if LineInfo.Predecessor.Relation in [prFinishStart, prStartStart, prSameStart] then
    begin
      if RightSideArrow <> -1 then
        EzEndPoint := EndPoints.Points[RightSideArrow];
      dec(LineInfo.Points[1].x, EzEndPoint.HotSpot.x - EzEndPoint.LeftConnectionPoint.x);
      dec(LineInfo.Points[1].y, EzEndPoint.HotSpot.y - EzEndPoint.LeftConnectionPoint.y);
    end
    else
    begin
      if LeftSideArrow <> -1 then
        EzEndPoint := EndPoints.Points[LeftSideArrow];
      inc(LineInfo.Points[1].x, EzEndPoint.RightConnectionPoint.x - EzEndPoint.HotSpot.x);
      dec(LineInfo.Points[1].y, EzEndPoint.HotSpot.y - EzEndPoint.RightConnectionPoint.y);
    end;

    LineInfo.PointCount := GetPredecessorLinePoints(PredecessorLine, LineInfo.Points, LineInfo.Predecessor.Relation, RowHeight);

    ACanvas.Pen.Assign(PredecessorLine.Pen);
    Windows.Polyline(ACanvas.Handle, PPredecessorlinePoints(@LineInfo.Points)^, LineInfo.PointCount);
    if EzEndPoint <> EmptyEndPoint then
    begin
      if PredecessorLine.FDrawBitmapIndex <> EzEndPoint.ImageIndex then
      begin
        bmp := PredecessorLine.FDrawBitmap;
        bmp.Width := EndPoints.Images.Width;
        bmp.Height := EndPoints.Images.Height;
        bmp.TransparentColor := EzTransparentColor;
        bmp.Canvas.Brush.Style := bsSolid;
        bmp.Canvas.Brush.Color := EzTransparentColor;
        bmp.Canvas.FillRect(Rect(0, 0, EndPoints.Images.Width, EndPoints.Images.Height));
        EndPoints.Images.Draw(bmp.Canvas, 0, 0, EzEndPoint.ImageIndex);
        PredecessorLine.FDrawBitmapIndex := EzEndPoint.ImageIndex;
      end;
      EndPointX := EndPoint.x - EzEndPoint.HotSpot.x;
      EndPointY := EndPoint.y - EzEndPoint.HotSpot.y;
      SetRect(EndPointRect, EndPointX, EndPointY, EndPointX + EndPoints.Images.Width, EndPointY + EndPoints.Images.Height);
      TransparentDraw(ACanvas, PredecessorLine.FDrawBitmap, EndPointRect, True);
    end;
  end;
end;

procedure PreparePredecessorLine(
      EndPoints: TEzEndPointList;
      PredecessorLine: TPredecessorLine;
      LineInfo: TVisiblePredecessorLine;
      StartPoint, EndPoint: TPoint;
      RowHeight: Integer);
var
  EzEndPoint: TEzEndPoint;

begin
  with PredecessorLine do
  begin
    LineInfo.Points[0] := StartPoint;
    LineInfo.Points[1] := EndPoint;
    EzEndPoint := EmptyEndPoint;

    if LineInfo.Predecessor.Relation in [prFinishStart, prStartStart, prSameStart] then
    begin
      if RightSideArrow <> -1 then
        EzEndPoint := EndPoints.Points[RightSideArrow];
      dec(LineInfo.Points[1].x, EzEndPoint.HotSpot.x - EzEndPoint.LeftConnectionPoint.x);
      dec(LineInfo.Points[1].y, EzEndPoint.HotSpot.y - EzEndPoint.LeftConnectionPoint.y);
    end
    else
    begin
      if LeftSideArrow <> -1 then
        EzEndPoint := EndPoints.Points[LeftSideArrow];
      inc(LineInfo.Points[1].x, EzEndPoint.RightConnectionPoint.x - EzEndPoint.HotSpot.x);
      dec(LineInfo.Points[1].y, EzEndPoint.HotSpot.y - EzEndPoint.RightConnectionPoint.y);
    end;

    LineInfo.PointCount := GetPredecessorLinePoints(PredecessorLine, LineInfo.Points, LineInfo.Predecessor.Relation, RowHeight);
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TScreenBar methods
//  A TScreenBar is used for caching TEzGanttBar's that are visible on screen.
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TScreenBar.Create;
begin
  BarProps := TEzGanttBar.Create(nil);
  Tag := 0;
end;

destructor TScreenBar.Destroy;
begin
  BarProps.Free;
  inherited;
end;

procedure TScreenBar.Assign(ASource: TScreenbar);
begin
  Node := ASource.Node;
  Row := ASource.Row;
  BarStart := ASource.BarStart;
  BarStop := ASource.BarStop;
  BarProps.Assign(ASource.BarProps);
  BarStates := ASource.BarStates;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TScreenRow methods
//  TScreenRow implements a single row in the screen cache
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TScreenRow.Create(itemcount, dummy: Integer);
begin
  inherited Create(itemcount, Sizeof(TScreenBar));
  SortOrder := tsAscending;
  Duplicates := dupAccept;
  FIsPrepared := False;
end;

procedure TScreenRow.Clear;
begin
  inherited;
  FIsPrepared := False;
end;

function TScreenRow.CompareItems(var Item1, Item2): Integer;
var
  i1: TScreenBar absolute Item1;
  i2: TScreenBar absolute Item2;
  z1, z2: Integer;

begin
  z1 := i1.BarProps.Z_Index;
  z2 := i2.BarProps.Z_Index;
  if z1 < z2 then
    Result := -1
  else if z1 > z2 then
    Result := 1
  else if i1.BarStop < i2.BarStop then
    Result := -1
  else if i1.BarStop > i2.BarStop then
    Result := 1
  else
    Result := 0;
end;

procedure TScreenRow.FreeItems(PItems: Pointer; numItems: Integer);
var
  Bar: PScreenBar;
  i: Integer;
begin
  for i:=0 to numItems-1 do
  begin
    Bar :=@TScreenbarArray(PItems)[i];
    Bar^.Free;
  end;
end;

function TScreenRow.FindProgressBar(Bar: TScreenBar): integer;
begin
  Result := 0;
  while (Result<Count) do
  begin
    if Items[Result].BarProps.ParentBar=Bar.BarProps then
      Exit;
    inc(Result);
  end;
  Result:=-1;
end;

function TScreenRow.IndexOf(ADate: TDateTime): integer;
var
  Find: TScreenBar;

begin
  Find.BarStop := ADate;
  Result := inherited IndexOf(Find);
end;

function TScreenRow.IndexOf(Bar: TScreenBar): integer;
begin
  Result := 0;
  while (Result<Count) and (Items[Result]<>Bar) do
    inc(Result);
  if Result=Count then
    Result := -1;
end;

function TScreenRow.IndexOfBar(ABar: TEzGanttbar): integer;
var
  i: integer;
begin
  Result := -1;
  i := 0;
  if ABar.Original<>nil then
    ABar := ABar.Original;
  while (Result = -1) and (i < Count) do
    if Items[i].BarProps.Original = ABar then
      Result := i else
      Inc(i);
end;

function TScreenRow.IndexOfNode(Original: TEzGanttbar; Node: TRecordNode): integer;
var
  i: integer;
begin
  Result := -1;
  i := 0;
  while (Result = -1) and (i < Count) do
    if (Items[i].Node = Node) and (Items[i].BarProps.FOriginal = Original) then
      Result := i else
      Inc(i);
end;

procedure TScreenRow.PutItem(index: Integer; value: TScreenBar);
begin
  inherited PutItem(index, Value);
end;

function TScreenRow.GetItem(index: Integer): TScreenBar;
begin
  Result := nil;
  inherited GetItem(index, Result);
end;

//=---------------------------------------------------------------------------=\
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TScreenCache.Create(G: TCustomEzGanttChart);
begin
  inherited Create(True {OwnsObject = True});
  Gantt := G;
end;

function TScreenCache.Get(Index: Integer): TScreenrow;
begin
  Result := TScreenrow(inherited Items[Index]);
end;

procedure TScreenCache.Put(Index: Integer; Item: TScreenrow);
begin
  inherited Items[Index] := Item;
end;

procedure TScreenCache.Clear;
begin
{
  KV: 10 may 2005, TScreenCache now inherits from TEzObjectList and
  therefore objects are automatically freed.
  for i:=0 to Count-1 do
    Items[I].Destroy;
}
  inherited;
end;

procedure TScreenCache.ClearBars;
begin
  ClearBars(0, Count-1);
end;

procedure TScreenCache.ClearBars(TopRow, BottomRow: Integer);
var
  I: Integer;
begin
{
  KV: 14 aug 2006
  Code disabled, call to StopDragging moved to stopdrag event.
  with Gantt do
  begin
    // KV 12 dec 2005 Extra test added.
    if not FDragOperationStarting then
      StopDragging;
  end;
}

  for I:=TopRow to BottomRow do
  begin
    Gantt.DoClearCacheRow(i);
    Items[I].Clear;
  end;
end;

function TScreenCache.LocateScreenBar(Ref: TEzGanttbarReference): TScreenBar;
var
  CacheRow, Row, i: Integer;

begin
  Result := nil;

  with Gantt.DrawInfo do
  begin
    // Bar is located on inserted record
    if Ref.RowNode=nil then
      Row := Gantt.DataLink.ActiveRecord
    else
    begin
      Row := Ref.RowNode.RecNo;
      dec(Row, ScreenTopRecNo);
    end;

    CacheRow := Gantt.ScreenRowToCacheIndex(Row);
    if (CacheRow>=0) and (CacheRow<ScreenCache.Count) then
    begin
      i := ScreenRow[CacheRow].IndexOfNode(Ref.Bar, Ref.BarNode);
      if i<>-1 then
        Result := ScreenRow[CacheRow].Items[i];
    end;
    
{
    if (Row>=0) and (Row<RecCount) then
    begin
      CacheRow := Gantt.ScreenRowToCacheIndex(Row);
      i := ScreenRow[CacheRow].IndexOfNode(Ref.Bar, Ref.BarNode);
      if i<>-1 then
        Result := ScreenRow[CacheRow].Items[i];
    end;
}
  end;
end;
{
function TScreenCache.FindBarReference(BarRef: TEzGanttbarReference): TScreenBar;
var
  Row, i: Integer;
  OldActive: integer;

begin
  Result := nil;

  with Gantt, Datalink.Dataset as TCustomEzDataset do
  begin
    OldActive := DataLink.ActiveRecord;
    try
      Row := 0;
      DataLink.ActiveRecord := 0;
      while (Row < DataLink.BufferCount) and (RecordNode <> BarRef.RowNode) do
      begin
        inc(Row);
        DataLink.ActiveRecord := Row;
      end;

      if Row < DataLink.BufferCount then
      begin
        i := Items[ScreenRowToCacheIndex(Row)].IndexOfNode(BarRef.Bar, BarRef.BarNode);
        if i<>-1 then
          Result := Items[ScreenRowToCacheIndex(Row)][i];
      end;
    finally
      DataLink.ActiveRecord := OldActive;
    end;
  end;
end;
}

constructor TVisiblePredecessorLine.Create;
begin
  ZeroMemory(@Predecessor, sizeof(TPredecessor));
  Predecessor.SuccessorNode.Value := Null;
  Predecessor.PredecessorNode.Value := Null;
end;

procedure TVisiblePredecessorLine.Assign(Line: TVisiblePredecessorLine);
begin
  Predecessor := Line.Predecessor;
  Points := Line.Points;
  PointCount := Line.PointCount;
end;
//=---------------------------------------------------------------------------=\
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TVisiblePredecessorLines.Create(G: TCustomEzGanttChart);
begin
  inherited Create(True {OwnsObject = True});
  Gantt := G;
end;

function TVisiblePredecessorLines.Get(Index: Integer): TVisiblePredecessorLine;
begin
  Result := TVisiblePredecessorLine(inherited Items[Index]);
end;

procedure TVisiblePredecessorLines.Put(Index: Integer; Item: TVisiblePredecessorLine);
begin
  inherited Items[Index] := Item;
end;

//=---------------------------------------------------------------------------=\
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TGanttDrawInfo.Create(AOwner: TComponent);
begin
  inherited;
  FScreenCache := TScreenCache.Create(AOwner as TCustomEzGanttChart);
  FPaintRow := TPaintRow.Create(50, 0);
end;

destructor TGanttDrawInfo.Destroy;
begin
  inherited;
  FScreenCache.Destroy;
  FPaintRow.Destroy;
end;

function  TGanttDrawInfo.GetRow(Item: Integer): TScreenRow;
begin
  Result := FScreenCache[Item];
end;

procedure TGanttDrawInfo.SetScreenCacheSize(Rows: Integer);
var
  I: Integer;
begin
  FScreenCache.Count := Rows;

  I:=FScreenCache.Count-1;
  while (I>=0) and (FScreenCache[I] = nil) do
  begin
    FScreenCache[I] := TScreenrow.Create(5, 10);
    Dec(I);
  end;
end;

procedure TGanttDrawInfo.ScrollCache(Distance: Integer);
var
  i, CurrentCount: Integer;

begin
  CurrentCount := ScreenCache.Count;

  if Distance > 0 then
  begin
    // Free rows at the beginning of cache and move remaining rows upwards
    for I:=0 to Distance-1 do
      FScreenCache[I] := nil; // This will free item too!

    FScreenCache.Pack;
    SetScreenCacheSize(CurrentCount);
  end

  else if Distance < 0 then
  begin
    Distance := -Distance;
    for i:=CurrentCount-1 downto CurrentCount-Distance do
      FScreenCache[I] := nil; // This will free item too.

    FScreenCache.Pack;
    for I:=0 to Distance-1 do
      FScreenCache.Insert(0, TScreenrow.Create(5, 10));
  end
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TGanttbarLabel.Create;
begin
  inherited;
  FAlignment := laBarCenter;
  FFont := TFont.Create;
  FPosition := lpgRight;
  FExtraOffset := 0;
  FExtraVOffset := 0;
  FMultiLine := False;
end;

destructor TGanttbarLabel.Destroy;
begin
  inherited;
  FFont.Destroy;
end;

procedure TGanttbarLabel.Assign(Source: TPersistent);
begin
  if Source is TGanttbarLabel then
  begin
    FAlignment := (Source as TGanttbarLabel).FAlignment;
    FText := (Source as TGanttbarLabel).FText;
    FFont.Assign((Source as TGanttbarLabel).Font);
    FPosition := (Source as TGanttbarLabel).FPosition;
    FExtraOffset := (Source as TGanttbarLabel).FExtraOffset;
    FExtraVOffset := (Source as TGanttbarLabel).FExtraVOffset;
    FMultiLine := (Source as TGanttbarLabel).FMultiLine;
  end
  else
    inherited Assign(Source);
end;

procedure TGanttbarLabel.SetFont(F: TFont);
begin
  FFont.Assign(F);
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TGanttBarEndPoint.Create(AOwner: TEzGanttBar);
begin
  FGanttBar := AOwner;
  FAlignment := gaBarTop;
  FEndPointIndex := -1;
end;

procedure TGanttBarEndPoint.Assign(Source: TPersistent);
begin
  if Source is TGanttBarEndPoint then
  begin
    FAlignment := (Source as TGanttBarEndPoint).FAlignment;
    FEndPointIndex := (Source as TGanttBarEndPoint).FEndPointIndex;
  end
  else
    inherited Assign(Source);
end;

//=---------------------------------------------------------------------------=
// TEzGanttBarVisibility
//
// Determines the visibility of a TEzGanttBar
//=---------------------------------------------------------------------------=
constructor TEzGanttBarVisibility.Create(Bar: TEzGanttBar);
begin
  inherited Create;
  FGanttBar := Bar;
  FEzDatalinkLevel := -1;
  FNodeTypes := [ntSingle, ntChild];
  FShowFor := [sfNormal, sfExpanded, sfCollapsed];
end;

destructor TEzGanttBarVisibility.Destroy;
begin
  inherited;
end;

procedure TEzGanttBarVisibility.Assign(Source: TPersistent);
begin
  if Source is TEzGanttBarVisibility then
  begin
    FEzDatalinkName := (Source as TEzGanttBarVisibility).EzDatalinkName;
    FEzDatalinkLevel := (Source as TEzGanttBarVisibility).EzDatalinkLevel;
    FNodeTypes := (Source as TEzGanttBarVisibility).NodeTypes;
    FShowFor := (Source as TEzGanttBarVisibility).ShowFor;
  end
  else
    inherited Assign(Source);
end;

procedure TEzGanttBarVisibility.SetEzDatalinkName(Name: string);
var
  g: TCustomEzGanttChart;

begin
  if (Name <> FEzDatalinkName) then
  begin
    FEzDatalinkName := Name;

    if (Name <> '') and
       Assigned(Ganttbar.Collection) and
       Assigned((Ganttbar.Collection as TEzGanttBars).GanttChart) then
    begin
      g := (Ganttbar.Collection as TEzGanttBars).GanttChart;
      if (csDesigning in g.ComponentState) and
          Assigned(g.DataLink.DataSet) and
         (g.DataLink.DataSet as TCustomEzDataset).SelfReferencing
      then
        MessageDlg(SDatalinkNameNotUsed, mtInformation, [mbOK], 0);
    end;
  end;
end;

procedure TEzGanttBarVisibility.SetNodeTypes(Value: TNodeTypes);
var
  g: TCustomEzGanttChart;

begin
  if (Value <> FNodeTypes) then
  begin
    FNodeTypes := Value;

    if (Value <> [ntSingle, ntChild]) and
       Assigned(Ganttbar.Collection) and
       Assigned((Ganttbar.Collection as TEzGanttBars).GanttChart) then
    begin
      g := (Ganttbar.Collection as TEzGanttBars).GanttChart;
      if (csDesigning in g.ComponentState) and
          Assigned(g.DataLink.DataSet) and
         not (g.DataLink.DataSet as TCustomEzDataset).SelfReferencing
      then
        MessageDlg(SNodeTypesNotUsed, mtInformation, [mbOK], 0);
    end;
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGanttbarArray
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzGanttbarArray.Create(itemcount: Integer; AGantt: TCustomEzGanttChart);
begin
  inherited Create(itemcount, Sizeof(TEzGanttbarReference));
  FGantt := AGantt;
//  SortOrder := tsAscending;
  SortOrder := tsNone;
  Duplicates := dupIgnore;
end;

function TEzGanttbarArray.Add(RowNode: TRecordNode; ABar: TScreenBar): Integer;
var
  BarRef: TEzGanttbarReference;
  R: TRect;
begin
  Result := -1;
  if not (sfSelected in ABar.BarStates) and ((FGantt=nil) or FGantt.DoSelectGanttbar(ABar)) then
  begin
    BarRef.RowNode := RowNode;
    BarRef.BarNode := ABar.Node;
    BarRef.Bar := ABar.BarProps.Original;
    Result := inherited Add(BarRef);
    if Result <> -1 then
    begin
      Include(ABar.BarStates, sfSelected);
      InflateRect(ABar.BoundRect, SelectionPoint.Width+1, 0);
      UnionRect(R, ABar.BoundRect, ABar.TextRect);
      if FGantt<>nil then
        InvalidateRect(FGantt.Handle, @R, False);
    end;
  end;
end;

function TEzGanttbarArray.Add(BarRef: TEzGanttbarReference): Integer;
begin
  Result := inherited Add(BarRef);
end;

procedure TEzGanttbarArray.Clear;
begin
  if (FGantt=nil) or (csDestroying in FGantt.ComponentState) or not FGantt.DataLink.Active then
    inherited Clear
  else if Count>0 then
  begin
    while Count>0 do
    begin
      if (FGantt=nil) or FGantt.DoDeselectGanttbar(Items[Count-1]) then
        Remove(Items[Count-1]);
    end;
  end;
end;

procedure TEzGanttbarArray.FreeItems(PItems: Pointer; numItems: Integer);
begin
  // Nothing to do
end;

procedure TEzGanttbarArray.Remove(RowNode: TRecordNode; ABar: TScreenBar);
var
  BarRef: TEzGanttbarReference;
  i: Integer;
  R: TRect;

begin
  BarRef.RowNode := RowNode;
  BarRef.BarNode := ABar.Node;
  BarRef.Bar := ABar.BarProps.Original;
  if FindItem(i, BarRef) then
  begin
    Delete(i);
    Exclude(ABar.BarStates, sfSelected);
    UnionRect(R, ABar.BoundRect, ABar.TextRect);
    if FGantt<>nil then
      InvalidateRect(FGantt.Handle, @R, False);
    InflateRect(ABar.BoundRect, -(SelectionPoint.Width+1), 0);
  end;
end;

procedure TEzGanttbarArray.Remove(BarRef: TEzGanttbarReference);
var
  i: Integer;
  Bar: TScreenBar;
  R: TRect;

begin
  if FindItem(i, BarRef) then
  begin
    if FGantt<>nil then
    begin
      Bar := FGantt.DrawInfo.ScreenCache.LocateScreenbar(BarRef);
      if Bar<>nil then
      begin
        Exclude(Bar.BarStates, sfSelected);
        UnionRect(R, Bar.BoundRect, Bar.TextRect);
        InvalidateRect(FGantt.Handle, @R, False);
        InflateRect(Bar.BoundRect, -(SelectionPoint.Width+1), 0);
      end;
    end;
    Delete(i);
  end;
end;

function TEzGanttbarArray.CompareItems(var Item1, Item2): Integer;
var
  i1: TEzGanttbarReference absolute Item1;
  i2: TEzGanttbarReference absolute Item2;

begin
  if Integer(i1.RowNode)<Integer(i2.RowNode) then
    Result := -1
  else if Integer(i1.RowNode)>Integer(i2.RowNode) then
    Result := 1
  else if Integer(i1.BarNode)<Integer(i2.BarNode) then
    Result := -1
  else if Integer(i1.BarNode)>Integer(i2.BarNode) then
    Result := 1
  else if Integer(i1.Bar)<Integer(i2.Bar) then
    Result := -1
  else if Integer(i1.Bar)>Integer(i2.Bar) then
    Result := 1
  else
    Result := 0;
end;

function TEzGanttbarArray.GetItem(index: Integer): TEzGanttbarReference;
begin
{
  Result.RowNode := nil;
  Result.BarNode := nil;
  Result.Bar := nil;
}
  inherited GetItem(index, Result);
end;

procedure TEzGanttbarArray.PutItem(index: Integer; value: TEzGanttbarReference);
begin
  inherited PutItem(index, Value);
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzGanttBar.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  FFlags := [gfCanMove, gfCanResize];
  FProgressBar := nil;
  FBarHeight := 10;
  FBarPosition := 0;
  FBarFill := TBrush.Create;
  FBarPen := TPen.Create;
  FLeftEndPoint := TGanttBarEndPoint.Create(Self);
  FRightEndPoint := TGanttBarEndPoint.Create(Self);
  FTextLabel := TGanttbarLabel.Create;
  FVisibility := TEzGanttBarVisibility.Create(Self);
  FZ_Index := 0;
end;

destructor TEzGanttBar.Destroy;
begin
  inherited Destroy;
  FBarFill.Destroy;
  FBarPen.Destroy;
  FLeftEndPoint.Destroy;
  FRightEndPoint.Destroy;
  FTextLabel.Destroy;
  FVisibility.Destroy;
end;

procedure TEzGanttBar.Assign(Source: TPersistent);
begin
  if Source is TEzGanttBar then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      FName := TEzGanttBar(Source).Name;
      FFlags := TEzGanttBar(Source).Flags;
      FBarHeight := TEzGanttBar(Source).BarHeight;
      FBarPosition := TEzGanttBar(Source).BarPosition;
      FBarFill.Assign(TEzGanttBar(Source).BarFill);
      FBarPen.Assign(TEzGanttBar(Source).BarPen);
      FLeftEndPoint.Assign(TEzGanttBar(Source).LeftEndPoint);
      FRightEndPoint.Assign(TEzGanttBar(Source).RightEndPoint);
      FTextLabel.Assign(TEzGanttBar(Source).FTextLabel);
      FStartField := TEzGanttBar(Source).StartField;
      FStopField := TEzGanttBar(Source).StopField;
//      FProgressBar := TEzGanttBar(Source).ProgressBarPtr;
      ProgressBar := TEzGanttBar(Source).ProgressBar;
      FEndpointType := TEzGanttBar(Source).FEndpointType;
      FZ_Index := TEzGanttBar(Source).FZ_Index;
      FVisibility.Assign(TEzGanttBar(Source).Visibility);
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TEzGanttBar.GetDisplayName : string;
begin
  if FName = '' then
    Result := 'EzGanttbar ' + IntToStr(Index) else
    Result := FName;
end;

function TEzGanttBar.GetGanttChart: TCustomEzGanttChart;
begin
  if Assigned(Collection) and (Collection is TEzGanttBars) then
    Result := TEzGanttBars(Collection).GanttChart
  else
    Result := nil;
end;

function TEzGanttBar.GetProgressBar: string;
begin
  if FProgressBar <> nil then
    Result := FProgressBar.Name else
    Result := '';
end;

function TEzGanttBar.ParentBar: TEzGanttBar;
var
  i: integer;
begin
  Result := FParentBar;
  if (Result = nil) and (Collection <> nil) then
    with Collection as TEzGanttBars do
    begin
      i := 0;
      while (Result = nil) and (i < Count) do
        if Items[i].FProgressBar = Self then
          Result := Items[i] else
          Inc(i);
    end;
end;

procedure TEzGanttBar.SetBarFill(B: TBrush);
begin
  FBarFill.Assign(B);
end;

procedure TEzGanttBar.SetBarPen(P: TPen);
begin
  FBarPen.Assign(P);
end;

procedure TEzGanttBar.SetEndpointType(Value: TGanttbarEndPointType);
begin
  if Value <> FEndpointType then
  begin
    if (Value = etPercentage) and
       not (csLoading in (Collection as TEzGanttBars).FGanttChart.ComponentState) and
       (ParentBar = nil)
    then
      raise Exception.Create('etPercentage can only be used with progress bars');

    FEndpointType := Value;
    Changed(false);
  end;
end;

procedure TEzGanttBar.SetName(const S: string);
begin
  if FName <> S then
  begin
    if Assigned(Collection) and
      ((Collection as TEzGanttbars).IndexOfName(S) <> -1)
    then
      raise Exception.Create(SDuplicateName);

    FName := S;
    Changed(false);
  end;
end;

procedure TEzGanttBar.SetFlags(F: TGanttbarFlags);
var
  i: integer;

begin
  if FFlags <> F then
  begin
    FFlags := F - [gfDrawParents, gfDrawChilds];

    // Make sure that only one bar can have the gfCanCreate set.
    if (Collection <> nil) and (gfCanCreate in F) then
      with Collection as TEzGanttbars do
        for i:=0 to Count-1 do
          if (Items[i] <> Self) and (gfCanCreate in Items[i].FFlags) then
            Items[i].FFlags := Items[i].FFlags - [gfCanCreate];

    Changed(False);
  end;
end;

procedure TEzGanttBar.SetProgressBar(S: string);
var
  i: integer;
begin
  with (Collection as TEzGanttBars) do
  begin
    if S = '' then
      FProgressBar := nil

    else if not Assigned(Collection) or Updating then
    begin
      FFixupProgressbarName := S;
      if Assigned(Collection) then
        FFixupRequired := true;
    end
    else

    begin
      i := IndexOfName(S);
      if i = -1 then
        FProgressBar := nil else
        FProgressBar := Collection.Items[i] as TEzGanttbar;
    end;
  end;
  changed(false);
end;

procedure TEzGanttBar.SetTextLabel(L: TGanttbarLabel);
begin
  FTextLabel.Assign(L);
end;

procedure TEzGanttBar.SetVisibility(Value: TEzGanttBarVisibility);
begin
  FVisibility.Assign(Value);
end;

procedure TEzGanttBar.SetZ_Index(Z: Integer);
begin
  if FZ_Index <> Z then
  begin
    FZ_Index := Z;
    changed(false);
  end;
end;

//=---------------------------------------------------------------------------=
constructor TEzGanttBars.Create(Chart: TCustomEzGanttChart; BarClass: TGanttBarClass);
begin
  inherited Create(BarClass);
  FGanttChart := Chart;
  FFixupRequired := False;
end;

function TEzGanttBars.Add: TEzGanttBar;
begin
  Result := TEzGanttBar(inherited Add);
end;

procedure TEzGanttBars.EndUpdate;
begin
  Inherited;
  if (UpdateCount = 0) and FFixupRequired then
    FixupProgressbars;
end;

function TEzGanttBars.Updating: Boolean;
begin
  Result := UpdateCount > 0;
end;

function TEzGanttBars.IndexOfName(const Value: string) : integer;
var
  i: integer;
begin
  Result := -1;
  i := 0;
  while (Result = -1) and (i < Count) do
    if Items[i].Name = Value then
      Result := i else
      Inc(i);
end;

procedure TEzGanttBars.FixupProgressbars;
var
  i, n: integer;

begin
  for i:=0 to Count-1 do
    with Items[i] do
      if FFixupProgressbarName <> '' then
      begin
        n := IndexOfName(FFixupProgressbarName);
        FProgressBar := Items[n];
        FFixupProgressbarName := '';
      end;
  FFixupRequired := false;
end;

function TEzGanttBars.GetOwner: TPersistent;
begin
  Result := FGanttChart;
end;

function TEzGanttBars.GetEzGanttBar(Index: Integer): TEzGanttBar;
begin
  Result := TEzGanttBar(inherited Items[Index]);
end;

{$IFDEF EZ_D6}
procedure TEzGanttBars.Notify(Item: TCollectionItem; Action: TCollectionNotification);
var
  i: Integer;

begin
  inherited;
  if (Action in [TCollectionNotification.cnDeleting, TCollectionNotification.cnExtracting]) then
  begin
    // Remove any references to this bar in case it is used as a progressbar
    for i:=0 to Count-1 do
      if (Items[i] <> Item) and (Items[i].ProgressBar = TEzGanttBar(Item).Name) then
        Items[i].ProgressBar := '';

    if GanttChart.Progressline.AttachToBar=TEzGanttBar(Item).Name then
      GanttChart.Progressline.AttachToBar:='';
  end;
end;
{$ENDIF}

procedure TEzGanttBars.SetEzGanttBar(Index: Integer; Value: TEzGanttBar);
begin
  Items[Index].Assign(Value);
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TPersistentPoint
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TPersistentPoint.Create;
begin
  FPoint.x := 0;
  FPoint.y := 0;
end;

procedure TPersistentPoint.Assign(Source: TPersistent);
begin
  if Source is TPersistentPoint then
    FPoint := (Source as TPersistentPoint).Point else
    inherited;
end;

function TPersistentPoint.GetX: Integer;
begin
  Result := FPoint.x;
end;

function TPersistentPoint.GetY: Integer;
begin
  Result := FPoint.y;
end;

procedure TPersistentPoint.SetX(Value: Integer);
begin
  FPoint.x := Value;
end;

procedure TPersistentPoint.SetY(Value: Integer);
begin
  FPoint.y := Value;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEndPoint
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzEndPoint.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  FImageIndex := -1;
  FHotSpot := TPersistentPoint.Create;
  FLeftCP := TPersistentPoint.Create;
  FRightCP := TPersistentPoint.Create;
end;

destructor TEzEndPoint.Destroy;
begin
  inherited Destroy;
  FHotSpot.Destroy;
  FLeftCP.Destroy;
  FRightCP.Destroy;
end;

procedure TEzEndPoint.Assign(Source: TPersistent);
begin
  if Source is TEzEndPoint then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      FHotSpot.Assign((Source as TEzEndPoint).FHotSpot);
      FLeftCP.Assign((Source as TEzEndPoint).FLeftCP);
      FRightCP.Assign((Source as TEzEndPoint).FRightCP);
      FImageIndex := (Source as TEzEndPoint).FImageIndex;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TEzEndPoint.GetDisplayName: string;
begin
  Result := 'Endpoint ' + IntToStr(Index);
end;

procedure TEzEndPoint.SetImageIndex(Value: TImageIndex);
begin
  if Value <> FImageIndex then
  begin
    FImageIndex := Value;
    Changed(false);
  end;
end;

procedure TEzEndPoint.SetHotSpot(P: TPersistentPoint);
begin
  if (FHotSpot.x <> P.x) or (FHotSpot.y <> P.y) then
  begin
    FHotSpot.Assign(P);
    Changed(false);
  end;
end;

procedure TEzEndPoint.SetLeftCP(P: TPersistentPoint);
begin
  if (FLeftCP.x <> P.x) or (FLeftCP.y <> P.y) then
  begin
    FLeftCP.Assign(P);
    Changed(false);
  end;
end;

procedure TEzEndPoint.SetRightCP(P: TPersistentPoint);
begin
  if (FRightCP.x <> P.x) or (FRightCP.y <> P.y) then
  begin
    FRightCP.Assign(P);
    Changed(false);
  end;
end;

constructor TEzEndPoints.Create(AOwner: TPersistent; AClass: TEzEndPointClass);
begin
  inherited Create(AOwner, AClass);
end;

destructor TEzEndPoints.Destroy;
begin
  inherited;
end;

function TEzEndPoints.Add: TEzEndPoint;
begin
  Result := TEzEndPoint(inherited Add);
end;

function TEzEndPoints.GetEndPoint(Index: Integer): TEzEndPoint;
begin
  Result := TEzEndPoint(inherited Items[Index]);
end;

function TEzEndPoints.GetEndPointList : TEzEndPointList;
begin
  Result := (inherited GetOwner) as TEzEndPointList;
end;

procedure TEzEndPoints.SetEndPoint(Index: Integer; Value: TEzEndPoint);
begin
  Items[Index].Assign(Value);
end;

constructor TEzEndPointList.Create(AOwner: TComponent);
begin
  inherited;
  FPoints := TEzEndPoints.Create(Self, TEzEndPoint);
  FImageChangeLink := TChangeLink.Create;
  FImageChangeLink.OnChange := ImageListChange;
end;

destructor TEzEndPointList.Destroy;
begin
  inherited;
  FImages := nil;
  FImageChangeLink.Destroy;
  FPoints.Destroy;
end;

procedure TEzEndPointList.Assign(Source: TPersistent);
begin
  if Source is TEzEndPointList then
  begin
    FPoints.BeginUpdate;
    try
      FPoints.Assign((Source as TEzEndPointList).FPoints);
      Images := (Source as TEzEndPointList).FImages;
    finally
      FPoints.EndUpdate;
    end;
  end else
    inherited;
end;

procedure TEzEndPointList.Generate;
begin
  if not Assigned(Images) then Exit;
  while Points.Count < Images.Count do
    Points.Add.ImageIndex := Points.Count;
end;

procedure TEzEndPointList.Change;
begin
end;

procedure TEzEndPointList.SetPoints(Points: TEzEndPoints);
begin
  FPoints.Assign(Points);
end;

procedure TEzEndPointList.SetImages(Value: TCustomImageList);
begin
  if Images <> nil then Images.UnRegisterChanges(FImageChangeLink);
  FImages := Value;
  if Images <> nil then Images.RegisterChanges(FImageChangeLink);
end;

procedure TEzEndPointList.ImageListChange(Sender: TObject);
begin
  if Sender = Images then Change;
end;

procedure TEzEndPointList.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);

  if (Operation = opRemove) and (AComponent = Images) then
    Images := nil;
end;


//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGanttChartNavigator
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
procedure TEzGanttChartNavigator.Paint;
begin

end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGanttDragObject
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzGanttDragObject.Create(AControl: TControl);
begin
  inherited;
  FPredecessorPen := TPen.Create;
  FPredecessorPen.Color := clRed;
  FPredecessorPen.Style := psSolid;
  FPredecessorPen.Mode := pmNotXor;

  // An empty rectange indicates images are not prepared
  SetRectEmpty(FDragScreenRect);
  SetRectEmpty(FDragBitmapRect);

  FDragBitmap := TBitmap.Create;
  FDragBitmap.TransparentColor := EzTransparentColor;
  FScreenCopy := TBitmap.Create;
end;

destructor TEzGanttDragObject.Destroy;
begin
  inherited;
  FPredecessorPen.Free;
  FDragBitmap.Free;
  FScreenCopy.Free;
  FBarProps.Free;
end;

procedure TEzGanttDragObject.HideDragImage(Gantt: TCustomEzGanttChart);
var
  ScreenDC: HDC;

begin
  if IsRectEmpty(FDragScreenRect) then Exit;

  GDIFlush;
  ScreenDC := GetDC(0);
  try
    InternalHideDragImage(Gantt, ScreenDC);
  finally
    ReleaseDC(0, ScreenDC);
  end;
end;

function TEzGanttDragObject.GetDragCursor(Accepted: Boolean; X, Y: Integer): TCursor;
begin
  if Accepted and (DragTarget<>nil) and (TObject(DragTarget) is TCustomEzGanttChart) then
    Result := (TObject(DragTarget) as TCustomEzGanttChart).DragCursor else
    Result := inherited GetDragCursor(Accepted, X, Y);
end;

procedure TEzGanttDragObject.InternalHideDragImage(Gantt: TCustomEzGanttChart; ScreenDC: HDC);
var
  W, H: Integer;

begin
  if not IsRectEmpty(FDragScreenRect) then
  begin
    W := FDragScreenRect.Right-FDragScreenRect.Left;
    H := FDragScreenRect.Bottom-FDragScreenRect.Top;
    StretchBlt(
      ScreenDC,
      FDragScreenRect.Left, FDragScreenRect.Top, W, H,
      FScreenCopy.Canvas.Handle,
      0, 0, W, H,
      cmSrcCopy);
    SetRectEmpty(FDragScreenRect);
  end;
end;

procedure TEzGanttDragObject.UpdateDragImage(
      Gantt: TCustomEzGanttChart;
      X, Y: Integer;
      HitInfo: TEzGanttHitInfo);
var
  ScreenDC: HDC;

begin
  GDIFlush;
  ScreenDC := GetDC(0);
  try
    InternalHideDragImage(Gantt, ScreenDC);
    InternalShowDragImage(Gantt, ScreenDC);
  finally
    ReleaseDC(0, ScreenDC);
  end;
end;

function TEzGanttDragObject.UpdatePositiondata(
    Gantt: TCustomEzGanttChart;
    X, Y: Integer;
    HitInfo: TEzGanttHitInfo): Boolean;
var
  H, W: Integer;
  P: TPoint;
  Duration: Double;
  Snap: TGanttbarStates;
  SavedRect: TRect;

begin
  H := GetRectH(FDragBitmapRect);
  W := GetRectW(FDragBitmapRect);
  if (W=0) or (H=0) then
  begin
    Result := False;
    Exit;
  end;

  SavedRect := FDragBitmapRect;
  P := Gantt.ScreenToClient(Point(X, Y));

  // Snap to row
  P.Y := HitInfo.HitRowRect.Top+(HitInfo.HitRowRect.Bottom-HitInfo.HitRowRect.Top) div 2 - H div 2;

  // We want bars to stay the same length so use the actual duration
  // when updating the record.
  Duration := BarStop - BarStart;
  // Calculate new start position

  FBarStart := Gantt.TimeBar.X2DateTime(P.X-FMouseOffset.X);
  FBarStop := BarStart + Duration;
  if (gaoDoSnap in Gantt.Options) and (Gantt.SnapCount <> 0) then
  begin
    Snap := Gantt.SnapInterval(FBarStart, FBarStop);
    if sfLeftSnap in Snap then
      FBarStop := FBarStart + Duration

    else if sfRightSnap in Snap then
      FBarStart := FBarStop-Duration;
  end;

  X := Gantt.TimeBar.DateTime2X(FBarStart);
  SetRect(FDragBitmapRect, X, P.Y, X+W, P.Y+H);
  Result := not EqualRect(SavedRect, FDragBitmapRect);
  if Result then
    OffsetRect(FGanttbarRect, FDragBitmapRect.Left-SavedRect.Left, 0);
end;

procedure TEzGanttDragObject.PrepareDragImage;
var
  rtmp, ClientRect: TRect;
begin
  // Prepare ganttbar bitmap.
  // If ganttbar is smaller than ClipedDragThreshold
  // then we prepare and drag the complete bitmap. Otherwise the bitmap
  // is cliped by the current row.
  if GetRectW(FGanttbarRect) <= ClipedDragThreshold then
    // Small ganttbar ==> prepare complete bitmap
  begin
    FRequiresPrepare := False;
    // Calculate size of bounding rectange
    PaintGanttbar(TCanvas(nil), EndPoints, rtmp, FGanttbarRect, FDragBitmapRect, FDragClickRect, rtmp, True, FBarProps, []);

    // paint ganttbar onto FDragBitmap's canvas
    // using bounding rectangle FDragBitmapRect as cliprect.
    PaintGanttbar(FDragBitmap, EndPoints, FDragBitmapRect, FGanttbarRect, FDragBitmapRect, FDragClickRect, rtmp, False, FBarProps, []);

    XOffset := FGanttbarRect.Left-FDragBitmapRect.Left;
  end
  else
    // Large ganttbar ==> only prepare visible part of bar
  begin
    FRequiresPrepare := True;
    if Control<>nil then
      ClientRect := Control.ClientRect else
      SetRect(ClientRect, 0, 0, 400, 20);

    PaintGanttbar(FDragBitmap, EndPoints, ClientRect, FGanttbarRect, FDragBitmapRect, FDragClickRect, rtmp, True, FBarProps, []);
    IntersectRect(FDragBitmapRect, FDragBitmapRect, ClientRect);
  end;
end;

procedure TEzGanttDragObject.ShowDragImage(Gantt: TCustomEzGanttChart);
var
  ScreenDC: HDC;

begin
  GDIFlush;
  ScreenDC := GetDC(0);
  try
    InternalShowDragImage(Gantt, ScreenDC);
  finally
    ReleaseDC(0, ScreenDC);
  end;
end;

procedure TEzGanttDragObject.InternalShowDragImage(
  Gantt: TCustomEzGanttChart; ScreenDC: HDC);
var
  W, H: Integer;
  CR, R: TRect;

begin
  if IsRectEmpty(FDragBitmapRect) or FRequiresPrepare then
    PrepareDragImage;

  CR := Gantt.ClientRect;
  InterSectRect(R, FDragBitmapRect, CR);

  if not IsRectEmpty(R) then
  begin
    W := GetRectW(R);
    H := GetRectH(R);

    FDragScreenRect.TopLeft := Gantt.ClientToScreen(R.TopLeft);
    FDragScreenRect.BottomRight := Gantt.ClientToScreen(R.BottomRight);

    OffsetRect(FDragScreenRect, -XOffset, 0);
    
    // Copy current contents of screen for rectangle covered by DragRect
    FScreenCopy.Width := max(FScreenCopy.Width, W);
    FScreenCopy.Height := max(FScreenCopy.Height, H);
    StretchBlt(
      FScreenCopy.Canvas.Handle,
      0, 0, W, H,
      ScreenDC,
      FDragScreenRect.Left, FDragScreenRect.Top, W, H,
      cmSrcCopy);

    TransparentDraw(ScreenDC, FDragBitmap,
      FDragScreenRect.Left,
      FDragScreenRect.Top,
      W, H,
      // X offset in src bitmap when image extends beyond left edge
      -min(FDragBitmapRect.Left,0),
      0,
      True);
  end;
end;

procedure TEzGanttDragObject.ResetDragImage(Start, Stop: TDateTime; BarRect: TRect; PropertiesChanged: Boolean);
begin
  // if size doesn't change, we only have to update position
  if not PropertiesChanged and not IsRectEmpty(FDragBitmapRect) and ((Stop-Start)=(FBarStop-FBarStart)) then
    FDragBitmapRect := BarRect
  else
  begin
    FGanttbarRect := BarRect;
    SetRectEmpty(FDragBitmapRect);
  end;
  FBarStart := Start;
  FBarStop := Stop;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGanttLineMarker
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzGanttLineMarker.Create(Collection: TCollection);
begin
  inherited;
  FPen := TPen.Create;
  FFlags := [lmfCanMove, lmfCanDelete];
  FPosition := 0;
  FDragPos := 0;
  FVisible := true;
end;

destructor TEzGanttLineMarker.Destroy;
begin
  inherited;
  FPen.Destroy;
end;

procedure TEzGanttLineMarker.Assign(Source: TPersistent);
begin
  if Source is TEzGanttLineMarker then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      FName := (Source as TEzGanttLineMarker).Name;
      FPen.Assign((Source as TEzGanttLineMarker).Pen);
      FPosition := (Source as TEzGanttLineMarker).Position;
      FFlags := (Source as TEzGanttLineMarker).FFlags;
      FVisible := (Source as TEzGanttLineMarker).FVisible;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TEzGanttLineMarker.CompareTo(rhs: TEzGanttLineMarker): integer;
begin
  if FPosition = rhs.Position then
    Result := 0
  else if FPosition < rhs.Position then
    Result := -1
  else
    Result := 1;
end;

function TEzGanttLineMarker.GetDisplayName: string;
begin
  if FName = '' then
    Result := 'Marker ' + IntToStr(Index) else
    Result := FName;
end;

function TEzGanttLineMarker.GetSelected: Boolean;
begin
  Result := (Collection as TEzGanttLineMarkers).FOwner.LineMarkers.IsSelected(Self);
end;

procedure TEzGanttLineMarker.SetFlags(F: TEzGanttLineMarkerFlags);
var
  i: integer;

begin
  if lmfIsCurrentTime in F then F := [lmfIsCurrentTime];
  if FFlags <> F then
  begin
    // Turn off lmfIsCurrentTime flags for all line markers
    if (lmfIsCurrentTime in F) and Assigned(Collection) then
      with Collection as TEzGanttLineMarkers do
        for i:=0 to Count-1 do
          Items[i].FFlags := Items[i].FFlags - [lmfIsCurrentTime];

    FFlags := F;
    Changed(false);
  end;
end;

procedure TEzGanttLineMarker.SetName(N: WideString);
begin
  // No change notification is sent here because changing the name
  // does not require a screen refresh.
  if FName <> N then FName := N;
end;

procedure TEzGanttLineMarker.SetPen(P: TPen);
begin
  FPen.Assign(P);
  Changed(false);
end;

procedure TEzGanttLineMarker.SetPosition(P: TDatetime);
begin
  if P <> FPosition then
  begin
    FPosition := P;
    Changed(false);
  end;
end;

procedure TEzGanttLineMarker.SetSelected(S: Boolean);
begin
  if Selected <> S then
    with (Collection as TEzGanttLineMarkers).FOwner.LineMarkers do
      if S then
        SelectLineMarker(Self) else
        DeselectLineMarker(Self);
end;

procedure TEzGanttLineMarker.SetVisible(V: Boolean);
begin
  if V <> FVisible then
  begin
    FVisible := V;
    Changed(false);
  end;
end;


//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGanttLineMarkers
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzGanttLineMarkers.Create(AOwner: TCustomEzGanttChart; MarkerClass: TEzGanttLineMarkerClass);
begin
  FOwner := AOwner;
  inherited Create(MarkerClass);
end;

function TEzGanttLineMarkers.Add: TEzGanttLineMarker;
begin
  Result := TEzGanttLineMarker(inherited Add);
end;

function TEzGanttLineMarkers.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

function TEzGanttLineMarkers.GetLineMarker(Index: Integer): TEzGanttLineMarker;
begin
  Result := TEzGanttLineMarker(inherited Items[Index]);
end;

procedure TEzGanttLineMarkers.SetLineMarker(Index: Integer; Value: TEzGanttLineMarker);
begin
  Items[Index].Assign(Value);
end;

procedure TEzGanttLineMarkers.Update(Item: TCollectionItem);
begin
  FOwner.LineMarkersChanged;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzLineMarkersArray
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
Type
  TLineMarkersArray = Array[0..High(Integer) div Sizeof(TEzGanttLineMarker)-1] of TEzGanttLineMarker;
  PLineMarkersArray = ^TLineMarkersArray;

constructor TEzLineMarkersArray.Create(itemcount, dummy: Integer);
begin
  inherited Create(itemcount, Sizeof(TEzGanttLineMarker));
  SortOrder := tsAscending;
end;

function TEzLineMarkersArray.CompareDates(var Item1; D: TDateTime): integer;
var
  i1: TEzGanttLineMarker absolute Item1;

begin
  if i1.Position = D then
    Result := 0
  else if i1.Position < D then
    Result := -1
  else
    Result := 1;
end;

function TEzLineMarkersArray.CompareItems(var Item1, Item2): Integer;
var
  i1: TEzGanttLineMarker absolute Item1;
  i2: TEzGanttLineMarker absolute Item2;
begin
  Result := i1.CompareTo(i2);
end;

function TEzLineMarkersArray.GetItem(index: Integer): TEzGanttLineMarker;
begin
  Result := nil;
  inherited GetItem(index, Result);
end;

procedure TEzLineMarkersArray.PutItem(index: Integer; value: TEzGanttLineMarker);
begin
  inherited PutItem(index, Value);
end;

function TEzLineMarkersArray.UpperBound(var Index: integer; D: TDateTime) : Boolean;
var
  L, H, I, C: Integer;

begin
  Result := false;
  L := 0;
  H := Count - 1;
  while (L <= H) do
  begin
    I := (L + H) shr 1;
    C := CompareDates(GetItemPtr(I)^, D);
    if (C < 0) then
      L := I + 1
    else
    begin
      H := I - 1;
      if (C = 0) or (I = 0) or (CompareDates(GetItemPtr(I-1)^, D) < 0) then
      begin
        Result := true;
        L := I;
      end;
    end;
  end;
  Index := L;
end;

constructor TEzGanttLineMarkersHelper.Create(AOwner: TCustomEzGanttChart);
begin
  FOwner := AOwner;
  FLineMarkers := TEzGanttLineMarkers.Create(AOwner, TEzGanttLineMarker);
  FVisibleLineMarkers := TEzLineMarkersArray.Create(0, 0);
  FSelectedLineMarkers := TList.Create;
  FHighlightColor := clHighLight;
  FVisible := true;
end;

destructor TEzGanttLineMarkersHelper.Destroy;
begin
  inherited;
  FLineMarkers.Destroy;
  FVisibleLineMarkers.Destroy;
  FSelectedLineMarkers.Destroy;
end;

procedure TEzGanttLineMarkersHelper.Assign(Source: TPersistent);
begin
  if Source is TEzGanttLineMarkersHelper then
  begin
    FLineMarkers.BeginUpdate;
    try
      FHighlightColor := (Source as TEzGanttLineMarkersHelper).HighlightColor;
      FLineMarkers.Assign((Source as TEzGanttLineMarkersHelper).FLineMarkers);
      FVisible := (Source as TEzGanttLineMarkersHelper).Visible;
    finally
      FLineMarkers.EndUpdate;
    end;
  end else
    inherited;
end;

procedure TEzGanttLineMarkersHelper.DeleteSelected;
var
  i: integer;
begin
  FLineMarkers.BeginUpdate;
  try
    for i:=0 to FSelectedLineMarkers.Count-1 do
      if FOwner.DoBeforeLineMarkerDelete(TEzGanttLineMarker(FSelectedLineMarkers[i])) then
        TEzGanttLineMarker(FSelectedLineMarkers[i]).Destroy;
  finally
    FSelectedLineMarkers.Clear;
    FLineMarkers.EndUpdate;
  end;
end;

procedure TEzGanttLineMarkersHelper.DeselectLineMarker(Marker: TEzGanttLineMarker);

  procedure DeselectMarker(Marker: TEzGanttLineMarker);
  var
    R: TRect;

  begin
    with FOwner do
    begin
      R.Left := Timebar.DateTime2X(Marker.Position)-1;
      if (R.Left>=0) and (R.Left<Width) then
      begin
        R.Top := 0;
        R.Right := R.Left+Marker.Pen.Width+1;
        R.Bottom := Height;
        InvalidateRect(Handle, @R, false);
      end;
    end;
  end;

var
  i: integer;

begin
  if Marker = nil then
  begin
    for i:=0 to FSelectedLineMarkers.Count-1 do
      DeselectMarker(TEzGanttLineMarker(FSelectedLineMarkers[i]));
    FSelectedLineMarkers.Clear;
  end else
  begin
    DeselectMarker(Marker);
    FSelectedLineMarkers.Remove(Marker);
  end;
end;

function TEzGanttLineMarkersHelper.IsSelected(Marker: TEzGanttLineMarker): Boolean;
begin
  Result := FSelectedLineMarkers.IndexOf(Marker) <> -1;
end;

procedure TEzGanttLineMarkersHelper.SelectLineMarker(Marker: TEzGanttLineMarker);
var
  R: TRect;

begin
  if IsSelected(Marker) then Exit;
  FSelectedLineMarkers.Add(Marker);

  with FOwner do
  begin
    R.Left := Timebar.DateTime2X(Marker.Position)-1;
    if (R.Left>=0) and (R.Left<Width) then
    begin
      R.Top := 0;
      R.Right := R.Left+Marker.Pen.Width+1;
      R.Bottom := Height;
      InvalidateRect(Handle, @R, false);
    end;
  end;
end;


procedure TEzGanttLineMarkersHelper.SetLineMarkers(LM: TEzGanttLineMarkers);
begin
  FLineMarkers.Assign(LM);
end;

procedure TEzGanttLineMarkersHelper.SetVisible(V: Boolean);
begin
  if FVisible <> V then
  begin
    FVisible := V;
    FOwner.LineMarkersChanged;
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzScrollBarOptions
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=

constructor TEzGanttScrollBarOptions.Create(AOwner: TCustomEzGanttChart);

begin
  inherited Create;
  FOwner := AOwner;
  FAlwaysVisible := ssNone;
  FScrollBars := ssBoth;
  ResetExtends;
end;

//----------------------------------------------------------------------------------------------------------------------

procedure TEzGanttScrollBarOptions.SetAlwaysVisible(Value: TScrollStyle);

begin
  if FAlwaysVisible <> Value then
  begin
    FAlwaysVisible := Value;
    if not (csLoading in FOwner.ComponentState) and FOwner.HandleAllocated then
      FOwner.RecreateWnd;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------

procedure TEzGanttScrollBarOptions.SetScrollBars(Value: TScrollStyle);

begin
  if FScrollbars <> Value then
  begin
    FScrollBars := Value;
    if not (csLoading in FOwner.ComponentState) and FOwner.HandleAllocated then
      FOwner.RecreateWnd;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------

function TEzGanttScrollBarOptions.GetOwner: TPersistent;

begin
  Result := FOwner;
end;

//----------------------------------------------------------------------------------------------------------------------

procedure TEzGanttScrollBarOptions.Assign(Source: TPersistent);

begin
  if Source is TEzGanttScrollBarOptions then
  begin
    AlwaysVisible := TEzGanttScrollBarOptions(Source).AlwaysVisible;
    ScrollBars := TEzGanttScrollBarOptions(Source).ScrollBars;
  end
  else
    inherited;
end;

procedure TEzGanttScrollBarOptions.ResetExtends;
begin
  FLeftExtend := MaxInt;
  FRightExtend := 0;
end;

procedure TEzGanttScrollBarOptions.SetLeftExtend(Value: TDateTime);
begin
  Value := min(FRightExtend, Value);
  if FLeftExtend<>Value then
  begin
    FLeftExtend:=Value;
    FOwner.UpdateHorizontalScrollbar;
  end;
end;

procedure TEzGanttScrollBarOptions.SetRightExtend(Value: TDateTime);
begin
  Value := max(FLeftExtend, Value);
  if FRightExtend<>Value then
  begin
    FRightExtend:=Value;
    FOwner.UpdateHorizontalScrollbar;
  end;
end;


//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TGanttTimebarLink
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TGanttTimebarLink.Create(AGantt: TCustomEzGanttChart);
begin
  inherited Create;
  FGantt := AGantt;
end;

destructor TGanttTimebarLink.Destroy;
begin
  inherited;
end;

procedure TGanttTimebarLink.LayoutChanged;
begin
  FGantt.TimebarLayoutChanged;
end;

procedure TGanttTimebarLink.DateChanged(OldDate: TDateTime);
begin
  FGantt.TimebarDateChanged(OldDate);
end;

procedure TGanttTimebarLink.UpdateState;
begin
  FGantt.UpdateTimebarState;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// THatchOptions
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor THatchOptions.Create(Gantt: TCustomEzGanttChart);
begin
  FGanttchart := Gantt;
{$IFNDEF BCB}
  FAlphaBlend := False;
  FAlphaBlendBitmap := TBitmap.Create;
  FAlphaBlendBitmap.Width := 1;
  FAlphaBlendBitmap.Height := 1;
  FAlphaBlendColor := clBlack;
  FAlphaBlendValue := 255;
{$ENDIF}
  FShow := True;
  FHatchPen := TPen.Create;
  FHatchPen.OnChange := GraphicsObjectChanged;
  FHatchPen.Style := psSolid;
  FHatchPen.Color := clBlue;
  FHatchBrush := TBrush.Create;
  FHatchBrush.OnChange := GraphicsObjectChanged;
  FHatchBrush.Style := bsBDiagonal;
  FHatchBrush.Color := clBlue;
end;

destructor THatchOptions.Destroy;
begin
  inherited;
{$IFNDEF BCB}
  FAlphaBlendBitmap.Destroy;
{$ENDIF}
  FHatchPen.Destroy;
  FHatchBrush.Destroy;
end;

procedure THatchOptions.Assign(Source: TPersistent);
begin
  if Source is THatchOptions then
  begin
    FGanttchart := THatchOptions(Source).FGanttChart;
{$IFNDEF BCB}
    FAlphaBlend := THatchOptions(Source).FAlphaBlend;
    FAlphablendColor := THatchOptions(Source).FAlphablendColor;
    FAlphablendValue := THatchOptions(Source).FAlphablendValue;
{$ENDIF}
    FShow := THatchOptions(Source).FShow;
    FHatchPen.Assign(THatchOptions(Source).FHatchPen);
    FHatchBrush.Assign(THatchOptions(Source).FHatchBrush);
  end else
    inherited;
end;

procedure THatchOptions.GraphicsObjectChanged(Sender: TObject);
begin
  FGanttChart.Invalidate;
end;

{$IFNDEF BCB}
procedure THatchOptions.SetAlphaBlend(Value: Boolean);
begin
  if FAlphaBlend<>Value then
  begin
    FAlphaBlend := Value;
    FGanttChart.Invalidate;
  end;
end;
{$ENDIF}
{$IFNDEF BCB}
procedure THatchOptions.SetAlphaBlendColor(Value: TColor);
begin
  if FAlphaBlendColor<>Value then
  begin
    FAlphaBlendColor := Value;
    FGanttChart.Invalidate;
  end;
end;
{$ENDIF}
{$IFNDEF BCB}
procedure THatchOptions.SetAlphaBlendValue(Value: Integer);
begin
  Value := min(Value, 255);
  if FAlphaBlendValue<>Value then
  begin
    FAlphaBlendValue := Value;
    FGanttChart.Invalidate;
  end;
end;
{$ENDIF}

procedure THatchOptions.SetHatchPen(Value: TPen);
begin
  FHatchpen.Assign(Value); // OnChange will be called
end;

procedure THatchOptions.SetHatchBrush(Value: TBrush);
begin
  FHatchBrush.Assign(Value); // OnChange will be called
end;

procedure THatchOptions.SetShow(Value: Boolean);
begin
  if FShow<>Value then
  begin
    FShow := Value;
    FGanttChart.Invalidate;
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TPaintRow
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
procedure TPaintRow.FreeItems(PItems: Pointer; numItems: Integer);
begin
  ; // Nothing to do, TPaintRow does not free objects contained in the list.
end;

function TPaintRow.CompareItems(var Item1, Item2): Integer;
const
  IsSelected: array[Boolean, Boolean] of ShortInt = ((0, -1),(1, 0));

var
  i1: TScreenBar absolute Item1;
  i2: TScreenBar absolute Item2;
  Z1, Z2: Integer;

begin
  // Progressbars must be displayed on top of parent bars
  if i1.BarProps.ParentBar = i2.BarProps then
    Result := 1
  else if i1.BarProps = i2.BarProps.ParentBar then
    Result := -1
  else
  begin
    // Selected bars are displayed on top of others
    Result := IsSelected[sfSelected in i1.BarStates, sfSelected in i2.BarStates];

    if Result=0 then
    begin
      z1 := i1.BarProps.Z_Index;
      z2 := i2.BarProps.Z_Index;
      if z1 < z2 then
        Result := -1
      else if z1 > z2 then
        Result := 1
      else if i1.BarStop < i2.BarStop then
        Result := -1
      else if i1.BarStop > i2.BarStop then
        Result := 1
      else
        Result := 0;
    end;
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TGanttDatalink
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TGanttDataLink.Create(AGantt: TCustomEzGanttChart);
begin
  inherited Create;
  FSlaveMode := False;
  FGantt := AGantt;
end;

procedure TGanttDataLink.ActiveChanged;
begin
  FGantt.LinkActive(Active);
end;

{$IFDEF XE2}
procedure TGanttDataLink.DataEvent(Event: TDataEvent; Info: NativeInt);
{$ELSE}
procedure TGanttDataLink.DataEvent(Event: TDataEvent; Info: Longint);
{$ENDIF}
begin
  if SlaveMode and Active then
  begin
    if Event = deGetMaxPosition then
      with DataSet as TCustomEzDataset do
        MaxPosition := min(MaxPosition, TCustomEzGanttChart(FGantt).VirtualRowCount)

    else if Event = deBufferCountChanged then
    begin
      FGantt.UpdateRowCount;
{
      KV: 18/8/2006
      Code dissabled and replaced with a call to UpdateRowCount.
      Method UpdateRowCount does the same thing.
      if BufferCount <> Info then
        with FGantt do
        begin
          FDrawInfo.ScreenCache.ClearBars;
          UpdateRowCount;
          Invalidate;
        end;
}
    end
    else
      inherited;

  end else
//  if (Event=deUpdateState) and (Dataset.State=dsInactive) then

    inherited;
end;

procedure TGanttDataLink.DataSetChanged;
begin
  FGantt.DataSetChanged;
end;

procedure TGanttDataLink.DataSetScrolled(Distance: Integer);
begin
  FGantt.ScrollVertical(Distance);
end;

procedure TGanttDataLink.SetBufferCount(Value: Integer);
var
  c: integer;

begin
  if SlaveMode then
    with DataSet as TCustomEzDataset do
    begin
      UpdateMaxPosition;
      c := MaxPosition;
      if c <> BufferCount then
      begin
        inherited SetBufferCount(c);
        NotifyBufferCountChanged(c);
      end;
    end
  else
    inherited;
end;

procedure TGanttDataLink.SetSlaveMode(Value: Boolean);
begin
  if Value <> FSlaveMode then
  begin
    FSlaveMode := Value;
    if Active then
      Dataset.Resync([]);
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzOverlapSettings.Create;
begin
  inherited;
  FMode := omNone;
  FHatchFill := TBrush.Create;
end;

destructor TEzOverlapSettings.Destroy;
begin
  FHatchFill.Destroy;
  inherited;
end;

procedure TEzOverlapSettings.Assign(Source: TPersistent);
begin
  if Source is TEzOverlapSettings then
  begin
    FMode := (Source as TEzOverlapSettings).Mode;
    FHatchFill.Assign((Source as TEzOverlapSettings).HatchFill);
  end else
    inherited;
end;

constructor TEzProgresslineSettings.Create(Owner: TCustomEzGanttChart);
begin
  inherited Create;
  FGanttChart := Owner;
  FGapSize := 10;
  FVisible := False;
  FPen := TPen.Create;
  FOptions := [poPaintOnTop];
  FProgresslineDate := Now;
  FWhenProgressIsZero := zpSkipRow;
  FAttachToBarReference := nil;    
end;

destructor TEzProgresslineSettings.Destroy;
begin
  inherited;
  FPen.Free;
end;

procedure TEzProgresslineSettings.Assign(Source: TPersistent);
begin
  if Source is TEzProgresslineSettings then
  begin
    FVisible := (Source as TEzProgresslineSettings).Visible;
    FProgresslineField := (Source as TEzProgresslineSettings).ProgresslineField;
    FPen.Assign((Source as TEzProgresslineSettings).Pen);
    FProgresslineDate := (Source as TEzProgresslineSettings).ProgressLineDate;
    FVOffset := (Source as TEzProgresslineSettings).FVOffset;
    FGapSize := (Source as TEzProgresslineSettings).GapSize;
    FAttachToBar := (Source as TEzProgresslineSettings).FAttachToBar;
    FWhenProgressIsZero := (Source as TEzProgresslineSettings).WhenProgressIsZero;
    FAttachToBarReference := nil;    
  end else
    inherited;
end;

function TEzProgresslineSettings.GetBarReference: TEzGanttbar;
var
  i: Integer;

begin
  if FAttachToBarReference=nil then
  begin
    i := GanttChart.Ganttbars.IndexOfName(FAttachToBar);
    if i=-1 then
      raise Exception.Create(Format('A bar with name ''%s'' does not exist', [FAttachToBar]));
    FAttachToBarReference := GanttChart.Ganttbars[i];
  end;
  Result := FAttachToBarReference;
end;

procedure TEzProgresslineSettings.SetGapSize(Value: Integer);
begin
  if FGapSize <> Value then
  begin
    FGapSize := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TEzProgresslineSettings.SetPen(Value: TPen);
begin
  FPen.Assign(Value);
  if Assigned(FGanttChart) then
    FGanttChart.Invalidate;
end;

procedure TEzProgresslineSettings.SetVisible(Value: Boolean);
begin
  if FVisible <> Value then
  begin
    FVisible := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TEzProgresslineSettings.SetOptions(Value: TEzProgresslineOptions);
begin
  Value := Value - [poLeadInLeadOut];
  if FOptions <> Value then
  begin
    FOptions := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TEzProgresslineSettings.SetProgressLineDate(Value: TDateTime);
begin
  if FProgresslineDate <> Value then
  begin
    FProgresslineDate := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TEzProgresslineSettings.SetProgresslineField(Value: String);
begin
  if FProgresslineField <> Value then
  begin
    FProgresslineField := Value;
    FAttachToBar := '';
    FAttachToBarReference:=nil;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TEzProgresslineSettings.SetAttachToBar(Value: String);
begin
  if FAttachToBar <> Value then
  begin
    FAttachToBar:=Value;
    FAttachToBarReference:=nil;
    FProgresslineField := '';
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TEzProgresslineSettings.SetVOffset(Value: Integer);
begin
  if FVOffset <> Value then
  begin
    FVOffset := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TEzProgresslineSettings.SetWhenProgressIsZero(Value: TZeroProgressOption);
begin
  if FWhenProgressIsZero <> Value then
  begin
    FWhenProgressIsZero := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

constructor TPredecessorLine.Create(Gantt: TCustomEzGanttChart);
begin
  inherited Create;
  FVisible := true;
  FGanttChart := Gantt;
  FPen := TPen.Create;
  FFont := TFont.Create;
  FOptions := [];
  FStartOffset := 0;
  FEndOffset := 0;
  FStartLength := 5;
  FEndLength := 5;
  FLeftSideArrow := -1;
  FRightSideArrow := -1;

  FDrawBitmap := TBitmap.Create;
  FDrawBitmapIndex := -1;
end;

destructor TPredecessorLine.Destroy;
begin
  inherited;
  FPen.Free;
  FFont.Free;
  FDrawBitmap.Free;
end;

procedure TPredecessorLine.Assign(Source: TPersistent);
begin
  if Source is TPredecessorLine then
  begin
    Pen := TPredecessorLine(Source).Pen;
    Font := TPredecessorLine(Source).Font;
    FOptions := TPredecessorLine(Source).Options;
    FStartField := TPredecessorLine(Source).StartField;
    FEndField := TPredecessorLine(Source).EndField;
    FStartOffset := TPredecessorLine(Source).StartOffset;
    FEndOffset := TPredecessorLine(Source).EndOffset;
    FStartLength := TPredecessorLine(Source).StartLength;
    FEndLength := TPredecessorLine(Source).EndLength;
    FLeftSideArrow := TPredecessorLine(Source).LeftSideArrow;
    FRightSideArrow := TPredecessorLine(Source).RightSideArrow;
    FVisible := TPredecessorLine(Source).Visible;
  end
  else
    inherited;
end;

procedure TPredecessorLine.SetEndField(Value: string);
begin
  if FEndField <> Value then
  begin
    FEndField := Value;
    if FEndField = '' then
      FVisible := false;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetEndOffset(Value: Integer);
begin
  if FEndOffset <> Value then
  begin
    FEndOffset := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetEndLength(Value: Integer);
begin
  if FEndLength <> Value then
  begin
    FEndLength := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetOptions(Value: TPredecessorLineOptions);
begin
  Exclude(Value, poSkipHiddenLines);
  if Value <> FOptions then
  begin
    FOptions := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetPredecessorPen(P: TPen);
begin
  FPen.Assign(P);
  if Assigned(FGanttChart) then
    FGanttChart.Invalidate;
end;

procedure TPredecessorLine.SetPredecessorFont(F: TFont);
begin
  FFont.Assign(F);
  if Assigned(FGanttChart) then
    FGanttChart.Invalidate;
end;

procedure TPredecessorLine.SetLeftSideArrow(I: TEndPointIndex);
begin
  if FLeftSideArrow <> I then
  begin
    FLeftSideArrow := I;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetRightSideArrow(I: TEndPointIndex);
begin
  if FRightSideArrow <> I then
  begin
    FRightSideArrow := I;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetStartField(Value: string);
begin
  if FStartField <> Value then
  begin
    FStartField := Value;
    if FStartField = '' then
      FVisible := false;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetStartOffset(Value: Integer);
begin
  if FStartOffset <> Value then
  begin
    FStartOffset := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetStartLength(Value: Integer);
begin
  if FStartLength <> Value then
  begin
    FStartLength := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

procedure TPredecessorLine.SetVisible(Value: Boolean);
begin
  if FVisible <> Value then
  begin
    FVisible := Value;
    if Assigned(FGanttChart) then
      FGanttChart.Invalidate;
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGanttChart
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
procedure TCustomEzGanttChart.Activate;
  //
  // Called when the Ganttchart becomes active
  //
var
  i, n: Integer;
  TheBar: TEzGanttbar;

begin
  UpdateRowCount;
  UpdateVerticalScrollBar;

  LineMarkersChanged;

  if not Assigned(Datasource) or not Assigned(Datasource.Dataset) then
    Exit;

  if not (Datasource.DataSet is TCustomEzDataset) then
    EzGanttError(SWrongDatasetType, Self);

  FPredecessorLine.Visible := FPredecessorLine.Visible and
                              (FPredecessorLine.FEndField <> '') and
                              (FPredecessorLine.FStartField <> '');

  FProgressline.Visible := FProgressline.Visible and
                          ((FProgressline.ProgresslineField<>'') or (FProgressline.AttachToBar<>''));
                              
  with Datasource.DataSet as TCustomEzDataset do
    if not SelfReferencing then
      for i:=0 to GanttBars.Count-1 do
      begin
        TheBar := Ganttbars[i];
        TheBar.Visibility.FEzDatalinkLevel := -1;
        if TheBar.Visibility.FEzDatalinkName = '' then continue;
        n:=0;
        while (n < EzDatalinks.Count) and (TheBar.Visibility.FEzDatalinkLevel = -1) do
        begin
          if EzDatalinks[n].DisplayName = TheBar.Visibility.FEzDatalinkName then
            TheBar.Visibility.FEzDatalinkLevel := n;
          inc(n);
        end;
      end;
end;

function TCustomEzGanttChart.AcquireFocus: Boolean;
begin
  Result := Focused;
  if not Result and CanFocus and not (csDesigning in ComponentState) then
  begin
    SetFocus;
    Result := Focused;
  end;
end;

procedure TCustomEzGanttChart.Deactivate;
begin
  UpdateVerticalScrollBar;
  Invalidate;
  FDrawInfo.ScreenCache.Clear;

  if Assigned(FTimeLine) then
  begin
    StopTimer(tmrTimeline);
    FTimeLine := nil;
  end;
end;

function TCustomEzGanttChart.BarCanDrag(ARow, ABar: Integer): Boolean;
var
  Field: TField;

begin
  Result := True;

  with (Datalink.Dataset as TCustomEzDataSet), FDrawInfo[ScreenRowToCacheIndex(ARow)][ABar] do
  begin
    // can't drag a hidden bar if gaoAllowHiddenEdit not set
    // can't drag bar if gfCanMove is not set
    if (not (gaoAllowHiddenEdit in FOptions) and (sfIsHiddenNode in BarStates)) or
       (BarProps.Flags * [gfCanMove, gfCanResize, gfCanResizeLeft] = [])
    then
    begin
      Result := false;
      Exit;
    end;

    if BarProps.StartField<>'' then
      //
      // Check whether start and stop field can be modified
      //
    begin
      StartDirectAccess;
      try
        Activecursor.CurrentNode := Node;

        Field := FieldByName(BarProps.StartField);
        Result := FieldCanModify(Field);
        if Result and (BarProps.StopField<>'') then
        begin
          Field := FieldByName(BarProps.StopField);
          Result := FieldCanModify(Field);
        end;
      finally
        EndDirectAccess;
      end;
    end;
  end;
end;

function TCustomEzGanttChart.BarDragging(X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType) : Boolean;
begin
  Result := false;
  if Assigned(FOnBarDragging) then FOnBarDragging(Self, X, Y, BarInfo, DragType, Result);
end;

function TCustomEzGanttChart.BarRect(ARow, ABar:Integer) : TRect;
var
  I:Integer;
begin
  with Result, FDrawInfo[ScreenRowToCacheIndex(ARow)][ABar] do
  begin
    Top := 0;
    for I:=0 to ARow-1 do
      Inc(Top, GetRowHeight(I)+FGridlineWidth);
    Bottom := Top + GetRowHeight(ARow);
    Left := Timebar.DateTime2X(BarStart);
    if BarStart = BarStop then
      Right := Left else
      Right := Timebar.DateTime2X(BarStop)-1;
  end;
end;

function TCustomEzGanttChart.CanCreateBar: TEzGanttbar;
var
  i: Integer;

begin
  i:=0;
  while i<GanttBars.Count do
  begin
    Result := GanttBars[i];
    if gfCanCreate in Result.Flags then
      Exit;
    inc(i);
  end;
  Result := nil;
end;

function TCustomEzGanttChart.GetRowAt(var Y: Integer): Integer;

  // Returns the index of the row at vertical position Y.
  // On return, Y is set to the upper boundary of the row.

var
  P, R {Rounded value}: Integer;
begin
  P := 0;
  R := 0;

  if Y<0 then
  begin
    R := RecNo(0)-1;
    Result := 0;
    while (R>0) and (P>Y) do
    begin
      dec(Result);
      dec(P, GetRowHeight(Result)+FGridlineWidth);
      dec(R);
    end;
    Y:=P;
  end
  else
  begin
    Result := 0;
    while P < Y do
    begin
      R := P;
      inc(P, GetRowHeight(Result)+FGridlineWidth);
      inc(Result);
    end;

    if P > Y then
    begin
      Y := R;
      Dec(Result)
    end else
      Y := P;
  end;
end;

function TCustomEzGanttChart.CalcVisibleRowCount: Integer;
var
  vPos: Integer;
begin
  vPos := 0;
  Result := 0;
  // A row is visible even if the last gridline does not display.
  while (vPos-FGridlineWidth) <= ClientHeight do
  begin
    Inc(vPos, GetRowHeight(Result)+FGridlineWidth);
    Inc(Result);
  end;
  if (Result>1) and (vPos > ClientHeight) then
    Dec(Result);
end;

procedure TCustomEzGanttChart.LoadAllCacheRows;
begin
  with DataLink.DataSet as TEzDataset do
  begin
    FDrawInfo.RecCount := RecordCount;
    FDrawInfo.SetScreenCacheSize(RecordCount);

    DisableControls;
    try
      First;
      while not Eof do
      begin
        if not FDrawInfo[RecNo-1].IsPrepared then
          PrepareCacheRow(DataLink.ActiveRecord, FDrawInfo[RecNo-1]);

        Next;
      end;
//      UpdateRowCount;
    finally
      EnableControls;
    end;
  end;
end;

function TCustomEzGanttChart.RecNo(ARow: integer) : integer;
var
  OldActive: integer;

begin
  if ARow >= DataLink.BufferCount then
    Result := -1 else
    begin
      OldActive := DataLink.ActiveRecord;
      try
        DataLink.ActiveRecord := ARow;
        Result := (Datalink.Dataset as TCustomEzDataset).RecNo;
      finally
        DataLink.ActiveRecord := OldActive;
      end;
    end;
end;

function TCustomEzGanttChart.RowNode(ARow: integer) : TRecordNode;
var
  OldActive: integer;

begin
  if ARow < DataLink.BufferCount then
  begin
    OldActive := DataLink.ActiveRecord;
    try
      DataLink.ActiveRecord := ARow;
      Result := (Datalink.Dataset as TCustomEzDataset).RecordNode;
    finally
      DataLink.ActiveRecord := OldActive;
    end;
  end else
    Result := nil;
end;

function TCustomEzGanttChart.RowRect(ARow:Integer) : TRect;
var
  I:Integer;
begin
  with Result do
  begin
    Top := 0;
    for I:=0 to ARow-1 do Inc(Top, GetRowHeight(I)+FGridlineWidth);
    Bottom := Top + GetRowHeight(ARow); //-FGridlineWidth;
    Left := 0;
    Right := ClientWidth;
  end;
end;

function TCustomEzGanttChart.SnapInterval(var Start, Stop: TDateTime) : TGanttbarStates;
var
  snap1, snap2: TDateTime;
  Duration: Double;

begin
  Duration := Stop - Start;

  if Duration = 0 then
  begin
    Result := [sfLeftSnap];
    Start := RoundDate(FSnapScale, FSnapCount, 0, Start);
    Stop := Start;
  end

  else if Duration > 0 then
  begin
    snap1 := RoundDate(FSnapScale, FSnapCount, 0, Start);
    snap2 := RoundDate(FSnapScale, FSnapCount, 0, Stop);

    if abs(snap1-Start) < abs(snap2-Stop) then
    begin
      Result := [sfLeftSnap];
      Start := snap1;
      Stop := Start + Duration;
    end
    else
    begin
      Result := [sfRightSnap];
      Stop := snap2;
      Start := Stop - Duration;
    end;
  end;
end;

function TCustomEzGanttChart.SnapDate(var dtDate: TDateTime): Boolean;
begin
  Result := false;
  if (gaoDoSnap in FOptions) and (FSnapCount <> 0) then
  begin
    Result := true;
    dtDate := RoundDate(FSnapScale, FSnapCount, 0, dtDate);
  end;
end;

procedure TCustomEzGanttChart.CMHintShow(var Message: TCMHintShow);
var
  IsFocused: Boolean;
  ParentForm: TCustomForm;
  HitInfo: TEzGanttHitInfo;
  Pt: TPoint;

begin
  with Message do
  begin
    Result := 1;

    // Make sure a hint is only shown if the tree or at least its parent form is active. Active editing is ok too.
    ParentForm := GetParentForm(Self);
    if Assigned(ParentForm) then
      IsFocused := ParentForm.Focused or (Screen.ActiveForm = ParentForm) else
      IsFocused := False;

    if (GetCapture = 0) and ShowHint and (FDragType = bdNone) and IsFocused then
    begin
      Pt := ScreenToClient(Mouse.CursorPos);
      GetHitTestInfoAt(Pt.X, Pt.Y, HitInfo);

      HintInfo.HintStr := DoGetHint(HitInfo);
      if HintInfo.HintStr <> '' then
      begin
        Result := 0;
        HintInfo.HideTimeout := 10000;
        if (hpOnBar in HitInfo.HitPositions) then
          HintInfo.CursorRect := HitInfo.HitScreenbar.BoundRect
        else if (hpOnRow in HitInfo.HitPositions) then
          HintInfo.CursorRect := HitInfo.HitRowRect;
      end;
    end;
  end;
end;

procedure TCustomEzGanttChart.CMHintShowPause(var Message: TCMHintShowPause);
begin
end;

procedure TCustomEzGanttChart.CMShowingChanged(var Message: TMessage);
begin
  inherited;
  if Showing then
  begin
    UpdateHorizontalScrollbar;
    UpdateVerticalScrollbar;
  end;
end;

constructor TCustomEzGanttChart.Create(AOwner: TComponent);
begin
  inherited;
  AllocBitmaps;
  ControlStyle := ControlStyle - [csSetCaption] + [csCaptureMouse, csOpaque, csReplicatable];

  Enabled := False;
  FVisibleRowCount := -1;
  FIsDeleting := False;
  FDrawBitmap := TBitmap.Create;
  FDragBitmap := TBitmap.Create;
  FDragOriginal := TBitmap.Create;

  FScrollbarOptions := TEzGanttScrollBarOptions.Create(Self);
  FPredecessorLine := TPredecessorLine.Create(Self);
  FLineMarkers := TEzGanttLineMarkersHelper.Create(Self);

  FPaintDragbar := False;
  FMinDate := Now - 10;
  FMaxDate := Now + 365;
  FTruncMinDate := 0;
  FRowColor := clWhite;
  FDrawInfo := TGanttDrawInfo.Create(Self);
  FTimebarLink := TGanttTimebarLink.Create(Self);
  FGanttbars := TEzGanttBars.Create(Self, TEzGanttBar);
  FSelectedGanttBars := TEzGanttbarArray.Create(0, Self);
  FDataLink := TGanttDataLink.Create(Self);
  FGridLinePen := TPen.Create;
  FGridLinePen.OnChange := GraphicsObjectChanged;
  FHatchOptions := THatchOptions.Create(Self);
  FOverlapping := TEzOverlapSettings.Create;
  FProgressLine := TEzProgresslineSettings.Create(Self);
  FVisiblePredecessorLines := TVisiblePredecessorLines.Create(Self);
  Width := 200;
  Height := 200;
  FBorderStyle := bsSingle;
  FUpdateCount := 0;
  FDragStartBar := nil;
  FDragType := bdNone;
  FDragThreshold := 5;
  FHitMargin := 2;
  FDefaultRowHeight := 17;
  FGridLineWidth := 1;
  FOptions := [gaoConfirmDelete, gaoPostAllEdits];
  FDragOperations := [doLinkTasks, doMoveTasks, doMoveHorizontal];
//  DragCursor := crDragGanttbar;

  FTimeLinePos := -1;  // Line isn't visible;
end;

destructor TCustomEzGanttChart.Destroy;
begin
  inherited Destroy;
  ReleaseBitmaps;
  StopTimer(tmrTimeline);
  FScrollbarOptions.Free;
  FPredecessorLine.Free;
  FLineMarkers.Free;
  FTimebarLink.Free;
  FDrawBitmap.Free;
  FGridLinePen.Free;
  FHatchOptions.Free;
  FGanttbars.Free;
  FSelectedGanttBars.Free;
  FDragBitmap.Free;
  FDragOriginal.Free;
  FDataLink.Free;
  FDataLink := nil;
  FOverlapping.Free;
  FProgressLine.Free;
  FVisiblePredecessorLines.Free;
  if FSelectedPredecessorLine <> nil then
    FSelectedPredecessorLine.Free;
end;

function TCustomEzGanttChart.CheckGanttbarVisibility(Bar: TEzGanttBar; IsHiddenNode: Boolean) : Boolean;
var
  Vis, Exp: Boolean;

begin
  CheckDataset;

  with Bar.Visibility, Datalink.Dataset as TEzDataset do
  begin
    if ShowFor=[] then
    begin
      Result := False;
      Exit;
    end;

    UpdateCursorPos;
    Exp := ActiveCursor.Expanded;
    Vis := not IsHiddenNode and ActiveCursor.IsVisible;

    if ActiveCursor.HasChildren then
    begin
      if ((gaoShowHiddens in FOptions) and Vis and not Exp) or
         (gaoAllwaysShowHiddens in FOptions)
      then
        Result := sfOnHiddenParent in ShowFor
      else if not Vis then
        Result := sfHidden in ShowFor
      else
        Result := sfNormal in ShowFor;

      if Result then
        if Exp then
          Result := sfExpanded in ShowFor else
          Result := sfCollapsed in ShowFor;

    end else if not Vis then
      Result := sfHidden in ShowFor

    else
      Result := sfNormal in ShowFor;

    if Result then
      if SelfReferencing then
        Result := ActiveCursor.NodeType in NodeTypes else
        Result := ActiveCursor.Level = FEzDatalinkLevel;
  end;
end;

procedure TCustomEzGanttChart.CreateParams(var Params: TCreateParams);
const
  ScrollBar: array[TScrollStyle] of Cardinal = (0, WS_HSCROLL, 0 {WS_VSCROLL}, WS_HSCROLL or WS_VSCROLL);

begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or (WS_CLIPCHILDREN + WS_CLIPSIBLINGS) or ScrollBar[ScrollBarOptions.FScrollBars];

    WindowClass.style := CS_DBLCLKS;
    if FBorderStyle = bsSingle then
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
        Style := Style or WS_BORDER;
  end;
end;

procedure TCustomEzGanttChart.CreateWnd;
begin
  inherited;
  UpdateRowCount;
end;

procedure TCustomEzGanttChart.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

procedure TCustomEzGanttChart.EndUpdate;
begin
  if FUpdateCount > 0 then
    Dec(FUpdateCount);
end;

function TCustomEzGanttChart.GetRowHeight(ARow: Integer): Integer;
begin
  Result := FDefaultRowHeight;
  if Assigned(FOnMeasureRow) then FOnMeasureRow(Self, ARow, Result);
end;

function TCustomEzGanttChart.GetSlaveMode: Boolean;
begin
  Result := Datalink.SlaveMode;
end;

function TCustomEzGanttChart.GetGanttbarDates(Bar: TEzGanttBar; var dtStart, dtStop: TDateTime) : Boolean;
var
  F: TField;
  ParentStart: TDateTime;

begin
  dtStart := 0;
  Result := False;

  CheckDataset;

  with Bar, Datalink.Dataset do
  begin
    F := FindField(StartField);
    if (F = nil) or (F.IsNull) then Exit;
    dtStart := F.AsDateTime;

    F := FindField(StopField);
    if (F <> nil) and not F.IsNull then
      case Bar.EndpointType of
        etFixed:      dtStop := F.AsDateTime;
        etDuration:   dtStop := dtStart + F.AsFloat;
        etPercentage:
          if (Bar.ParentBar <> nil) and GetGanttbarDates(Bar.ParentBar, ParentStart, dtStop) then
          begin
            if F.DataType = ftFloat then
              dtStop := dtStart + (dtStop - ParentStart) * F.AsFloat else
              dtStop := dtStart + ((dtStop - ParentStart) * F.AsInteger)/100;
          end else
            dtStop := dtStart-1; // returns false
      end
    else
      dtStop := dtStart;

    if ((dtStart < dtStop) and not (gfMileStone in Bar.Flags)) or ((dtStart = dtStop) and (gfMileStone in Bar.Flags)) then
      Result := True;
  end;
end;

procedure TCustomEzGanttChart.GetHitTestInfoAt(X, Y: Integer; var HitInfo: TEzGanttHitInfo);
var
  i: integer;
  PosDate: TDateTime;

  function TestBarHit(TheBar: TScreenbar): boolean;
  var
    HitRect: TRect;

  begin
    Result := False;
    HitRect := TheBar.ClickRect;
    if not (gfMilestone in TheBar.BarProps.Flags) then
      InflateRect(HitRect, FHitMargin, 0);

    if PtInRect(HitRect, Point(X,Y)) then
    begin
      Result := True;
      Include(HitInfo.HitPositions, hpOnBar);

      if X <= HitRect.Left + 2*FHitMargin then
        Include(HitInfo.HitPositions, hpOnBarStart);

      if X >= HitRect.Right - 2*FHitMargin then
        Include(HitInfo.HitPositions, hpOnBarStop);
    end;
  end;

  procedure DetectBarHit;
  var
    ScreenRow: TScreenRow;
    CacheIndex: Integer;
    Bar: TScreenBar;

  begin
    // When detecting a hit on a bar, first search the selected bars
    // list.
    i:=FSelectedGanttBars.Count-1;
    while (i>=0) and ((HitInfo.HitBar=-1) or (HitInfo.HitSelectableBar=-1)) do
    begin
      Bar := FDrawInfo.ScreenCache.LocateScreenBar(FSelectedGanttBars[i]);
      if (Bar<>nil) and TestBarHit(Bar) then
      begin
        if HitInfo.HitBar=-1 then
        begin
          HitInfo.HitScreenbar := Bar;

          // If CacheAll is active, then Bar.Row holds an absolute row index.
          // Therefore there is no need to call ScreenRowToCacheIndex here.
          HitInfo.HitBar := FDrawInfo[Bar.Row].IndexOf(Bar);
        end;

        if gfCanSelect in Bar.BarProps.Flags then
        begin
          HitInfo.HitSelectableScreenbar := Bar;

          // If CacheAll is active, then Bar.Row holds an absolute row index.
          // Therefore there is no need to call ScreenRowToCacheIndex here.
          HitInfo.HitSelectableBar := FDrawInfo[Bar.Row].IndexOf(Bar);
        end;
      end;
      // KV: 26-9-2006 allways call dec(i)
      dec(i);
    end;

    if (HitInfo.HitBar=-1) and (HitInfo.HitSelectableBar=-1) then
    begin
      // No hit found in selected bars, test the normal screencache
      CacheIndex := ScreenRowToCacheIndex(HitInfo.HitRow);
      if CacheIndex>=FDrawInfo.ScreenCache.Count then Exit;
      ScreenRow := FDrawInfo[CacheIndex];
      i := ScreenRow.Count-1;
      while (i >= 0) and ((HitInfo.HitBar=-1) or (HitInfo.HitSelectableBar=-1)) do
      begin
        Bar := ScreenRow[i];
        if TestBarHit(Bar) then
        begin
          if HitInfo.HitBar=-1 then
          begin
            HitInfo.HitScreenbar := Bar;
            HitInfo.HitBar := i;
          end;

          if gfCanSelect in Bar.BarProps.Flags then
          begin
            HitInfo.HitSelectableScreenbar := Bar;
            HitInfo.HitSelectableBar := i;
          end;
        end;
        dec(i);
      end;
    end;
  end;

  procedure DetectLinemarkerHit;
  begin
    with LineMarkers.FVisibleLineMarkers do
    begin
      PosDate := Timebar.X2Datetime(X-FHitMargin);
      if Upperbound(i, PosDate) and
       ((Items[i].Flags * [lmfCanMove, lmfCanDelete]) <> []) and
        (Items[i].Position <= Timebar.X2Datetime(X+FHitMargin))
      then
      begin
        HitInfo.HitLineMarker := Items[i];
        Include(HitInfo.HitPositions, hpOnLineMarker);
      end;
    end;
  end;

  function NormalizeRect(R: TRect): TRect;
  begin
    SetRect(Result, min(R.Left, R.Right), min(R.Top, R.Bottom), max(R.Left, R.Right), max(R.Top, R.Bottom));
  end;

  procedure DetectPredecessorlineHit;
  var
    i, j: Integer;

  begin
    i:=FVisiblePredecessorLines.Count-1;
    while (i>=0) and not (hpOnPredecessorline in HitInfo.HitPositions) do
      with FVisiblePredecessorLines[i] do
      begin
        j:=0;
        while j<PointCount-1 do
        begin
          if (Points[j].X=Points[j+1].X) then
          // Vertical line
          begin
            if PtInRect(NormalizeRect(Rect(Points[j].X-1, Points[j].Y, Points[j+1].X+2, Points[j+1].Y)), Point(X, Y)) then
            begin
              Include(HitInfo.HitPositions, hpOnPredecessorline);
              HitInfo.HitPredecessorLine := FVisiblePredecessorLines[i];
              break;
            end
          end
          else if PtInRect(NormalizeRect(Rect(Points[j].X, Points[j].Y-1, Points[j+1].X, Points[j+1].Y+2)), Point(X, Y)) then
          // Horizontal line
          begin
            Include(HitInfo.HitPositions, hpOnPredecessorline);
            HitInfo.HitPredecessorLine := FVisiblePredecessorLines[i];
            break;
          end;
          inc(j);
        end;
        dec(i);
      end;
  end;

begin
  with HitInfo do
  begin
    HitWhere := Point(X,Y);
    HitRow := -1;
    HitRowRect := Rect(0,0,0,0);
    HitBar := -1;
    HitSelectableBar := -1;
    HitPositions := [];

    if (Y < 0) or (Y > ClientHeight) or (X < 0) or (X> ClientWidth) then Exit;

    //
    // Get row index and set top to the upper boundary of the row.
    //
    SetRect(HitRowRect, 0, Y, ClientWidth, 0);
    HitRow := GetRowAt(HitRowRect.Top);
    HitRowRect.Bottom := HitRowRect.Top + GetRowHeight(HitRow);

    if (csDesigning in ComponentState) or not Datalink.Active then
    begin
      Include(HitPositions, hpNowhere);
      Exit;
    end;

    if PredecessorLinesShowing and (poDisplayOnTop in FPredecessorLine.Options) then
      DetectPredecessorlineHit;

    if not (hpOnPredecessorline in HitPositions) and
      (
        // detect row hit when we are over an active record
        (HitRow < DataLink.RecordCount)
        or
        // detect row hit when dataset is empty and we over the first, empty, row
        ((HitRow=0) and DataLink.DataSet.IsEmpty)
      )
    then
    begin
      Include(HitPositions, hpOnRow);
      DetectBarHit;

      if not (hpOnBar in HitPositions) then
      begin
        with HitInfo.HitRowRect do
          if PtInRect(Rect(Left, Top, Right, Top+RowBorderHitMargin+1), HitInfo.HitWhere) then
            Include(HitInfo.HitPositions, hpOnRowTopBorder)
          else if PtInRect(Rect(Left, Bottom-RowBorderHitMargin, Right, Bottom+1), HitInfo.HitWhere) then
            Include(HitInfo.HitPositions, hpOnRowBottomBorder)
      end;
    end;

    if not (hpOnBar in HitPositions) then
    begin
      if PredecessorLinesShowing and not (poDisplayOnTop in FPredecessorLine.Options) then
        DetectPredecessorlineHit;

      if not (hpOnPredecessorline in HitPositions) and LineMarkers.Visible then
        DetectLinemarkerHit;
    end;
  end;
end;

function TCustomEzGanttChart.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TCustomEzGanttChart.DoGetPredecessor(var Predecessor: TPredecessor);
begin
  if Assigned(FOnGetPredecessor) then FOnGetPredecessor(Self, Predecessor);
end;

function TCustomEzGanttChart.DoGetPredecessorLineVisibility(const Predecessor: TPredecessor; PredecessorNode, VisiblePredecessorNode, SuccessorNode, VisibleSuccessorNode: TRecordNode): Boolean;
begin
  Result := True;
  if Assigned(FOnGetPredecessorVisibility) then
    FOnGetPredecessorVisibility(Self, Predecessor, PredecessorNode, VisiblePredecessorNode, SuccessorNode, VisibleSuccessorNode, Result);
end;

function TCustomEzGanttChart.GetSnapSize: string;
begin
  Result := '';
  if FSnapCount <> 0 then
  begin
    Result := IntToStr(FSnapCount);
    case FSnapScale of
      msYear: Result := Result + ' year(s)';
      msQuarter: Result := Result + ' quarter(s)';
      msMonth: Result := Result + ' month(s)';
      msWeek: Result := Result + ' week(s)';
      msDay: Result := Result + ' day(s)';
      msHour: Result := Result + ' hour(s)';
      msMinute: Result := Result + ' minute(s)';
    end;
  end;
end;

function TCustomEzGanttChart.GetTimebar: TEzTimebar;
begin
  Result := FTimebarLink.Timebar;
end;

function TCustomEzGanttChart.GetVisibleRowCount: Integer;
begin
  if (gaoCacheAllRows in Options) then
  begin
    if FVisibleRowCount=-1 then
      FVisibleRowCount := CalcVisibleRowCount;
    Result := FVisibleRowCount;
  end else
    Result := FDrawInfo.ScreenCache.Count;
end;

procedure TCustomEzGanttChart.GraphicsObjectChanged(Sender: TObject);
begin
  invalidate;
end;

procedure TCustomEzGanttChart.HighlightPredecessorline;
begin
  SetPredecessorLinePen(Canvas.Pen);
  Windows.Polyline(Canvas.Handle, FSelectedPredecessorLine.Points, FSelectedPredecessorLine.PointCount);
end;

procedure TCustomEzGanttChart.InitializeDragObject(DragObject: TEzGanttDragObject; ClearDragArea: Boolean);
var
  R: TRect;

begin
  DragObject.FBar.RowNode := RowNode(FMouseDownHitInfo.HitRow);
  DragObject.FBar.BarNode := FDragStartBar.Node;
  DragObject.FBar.Bar := FDragStartBar.BarProps.Original;
  DragObject.FBarStart := FDragStartBar.BarStart;
  DragObject.FBarStop := FDragStartBar.BarStop;
  DragObject.FBarProps := TEzGanttBar.Create(nil);
  DragObject.FBarProps.Assign(FDragStartBar.BarProps);
  DragObject.FMouseOffset := FMouseBarOffset;
  DragObject.FGanttbarRect := FDragRect;
  DragObject.FEndPoints := EndPoints;

  if ClearDragArea then
  begin
    UnionRect(R, FDragStartBar.BoundRect, FDragStartBar.TextRect);
    InvalidateRect(Handle, @R, False);
    UpdateWindow(Handle);
  end;
end;

procedure TCustomEzGanttChart.Invalidate;
begin
  FTimeLinePos := -1;
  ClearDragEffects;
  inherited;
end;

procedure TCustomEzGanttChart.KeyDown(var Key: Word; Shift: TShiftState);
var
  BarRef: TEzGanttbarReference;

begin
  if Assigned(OnKeyDown) then OnKeyDown(Self, Key, Shift);
  if (Key = VK_DELETE) and Datalink.Active and (FSelectedGanttBars.Count > 0) then
  begin
    FIsDeleting := True;

    if not (gaoConfirmDelete in Options) or
       (MessageDlg(SConfirmDeleteBars, mtConfirmation, mbOKCancel, 0) = idOK)
    then
      with (Datalink.Dataset as TCustomEzDataset) do
      begin
        DisableControls;
        StartDirectAccess([cpShowAll]);
        try
          while FSelectedGanttBars.Count > 0 do
          begin
            BarRef := FSelectedGanttBars[FSelectedGanttBars.Count-1];
            FSelectedGanttBars.Remove(BarRef);
            if ActiveCursor.Exists(BarRef.BarNode) then
            begin
              RecordNode := BarRef.BarNode;
              Delete;
            end;
          end;
        finally
          EndDirectAccess;
          EnableControls;
        end;
      end;
    FIsDeleting := False;
  end;
end;

procedure TCustomEzGanttChart.LineMarkersChanged;
var
  i: integer;
  M: TEzGanttLineMarker;

begin
  if Assigned(Datalink.Dataset) and Datalink.Dataset.Active then
  begin
    if Assigned(FTimeLine) then
    begin
      StopTimer(tmrTimeline);
      FTimeLine := nil;
    end;
    LineMarkers.FVisibleLineMarkers.Clear;

    if LineMarkers.Visible then
      for i:=0 to LineMarkers.Lines.Count-1 do
      begin
        M := LineMarkers.Lines[i];
        with M do
          if Visible then
          begin
            if lmfIsCurrentTime in M.Flags then
              FTimeLine := M
            else if Position <> 0 then
              LineMarkers.FVisibleLineMarkers.Add(M);
          end;
    end;
    Invalidate;
    if Assigned(FTimeLine) then
      RunTimer(tmrTimeline, 1000);
  end;
end;

procedure TCustomEzGanttChart.LinkActive(Value: Boolean);
begin
  if Value then
    Activate else
    Deactivate;
end;

procedure TCustomEzGanttChart.LinkTasks(Predecessor, Successor: TNodeKey; Relation: TEzPredecessorRelation);
begin
  if Assigned(FOnLinkTasks) then FOnLinkTasks(Self, Predecessor, Successor, Relation);
end;

procedure TCustomEzGanttChart.MoveTask(Location: TRecordNode; PositionInfo: TEzGanttHitPositions);
var
  DoMove: Boolean;

begin
  DoMove := true;
  if Assigned(FOnMoveTask) then FOnMoveTask(Self, FDragStartNode, Location, PositionInfo, DoMove);

  if DoMove then
  begin
    if Location=nil then
      (Datalink.Dataset as TCustomEzDataset).MoveRecord(FDragStartNode, nil, ipInsertAfter)
    else if hpInsertAsChild in PositionInfo then
      (Datalink.Dataset as TCustomEzDataset).MoveRecord(FDragStartNode, Location, ipInsertAsChild)
    else if hpInsertTask in PositionInfo then
      (Datalink.Dataset as TCustomEzDataset).MoveRecord(FDragStartNode, Location, ipInsertBefore)
    else if hpInsertTaskAfter in PositionInfo then
      (Datalink.Dataset as TCustomEzDataset).MoveRecord(FDragStartNode, Location, ipInsertAfter);
  end;
end;

procedure TCustomEzGanttChart.MoveSelectedLineMarkers(Pos: Integer);
var
  i: integer;
  d, dtDelta: TDateTime;

begin
  with FLineMarkers do
  begin
    // Hide
    DrawSelectedLineMarkers;

    dtDelta := Timebar.X2DateTime(Pos) - FDragMarker.Position;
    for i:=0 to Selected.Count-1 do
      with TEzGanttLineMarker(Selected[i]) do
      begin
        d := Position + dtDelta;
        SnapDate(d);
        FDragPos := Timebar.DateTime2X(d);
      end;

    // Fire event
    DoLineMarkerDragging(Pos, FDragMarker);

    // Show
    DrawSelectedLineMarkers;
  end;
end;

procedure TCustomEzGanttChart.ScrollClientRect(Distance: Integer);
var
  SRect: TRect;
begin
  if FTimeLinePos <> -1 then
  begin
    Inc(FTimeLinePos, Distance);
    if (FTimeLinePos < 0) or (FTimeLinePos >= ClientRect.Right) then
      FTimeLinePos := -1;
  end;

  // Scroll client area
  //
  // If an update region already exists, we do not scroll the window
  // but instead invalidate the entire control.
  //
  if GetUpdateRect(Handle, SRect{dummy parameter}, false)=false then
  begin
    SRect := ClientRect;
    ScrollWindowEx(Self.Handle, Distance, 0, @SRect, @SRect,
      0, nil, SW_Invalidate);
  end else
    Invalidate;
end;

procedure TCustomEzGanttChart.DoClearCacheRow(ARow: integer);
begin
  if Assigned(FOnClearCacheRow) then FOnClearCacheRow(Self, ARow);
end;

procedure TCustomEzGanttChart.DoContextPopup(MousePos: TPoint; var Handled: Boolean);
var
  Move: Integer;

begin
  // Let dataset follow mouse cursor
  Move := GetRowAt(MousePos.Y) - Datalink.ActiveRecord;
  if Move <> 0 then
    FDatalink.MoveBy(Move);

  inherited;
end;

procedure TCustomEzGanttChart.DataSetChanged;
begin
  if not HandleAllocated then Exit;
  if (gaoIgnoreDatasetChangedEvent in Options) then
  begin
    if (Datalink.Dataset as TCustomEzDataset).TopNode <> DrawInfo.ScreenTopNode then
    begin
      UpdateVerticalScrollBar;
      Invalidate;
    end;
  end
  else
  begin
    FDrawInfo.ScreenCache.ClearBars;
    UpdateRowCount;
    UpdateVerticalScrollBar;
    Invalidate;
  end;
end;

procedure TCustomEzGanttChart.DeselectPredecessorline;
begin
  DoDeselectPredecessorline(FSelectedPredecessorLine.Predecessor);
  HighlightPredecessorline;
  FSelectedPredecessorLine.Destroy;
  FSelectedPredecessorLine := nil;
end;

function TCustomEzGanttChart.DoBeforeLineMarkerDelete(LineMarker: TEzGanttLineMarker): Boolean;
begin
  Result := true;
  if Assigned(FBeforeLineMarkerDelete) then FBeforeLineMarkerDelete(Self, LineMarker, Result);
end;

procedure TCustomEzGanttChart.DoBardataChanged(BarInfo: TScreenbar; DragType: TGanttBarDragType);
begin
  if Assigned(FOnBardataChanged) then FOnBardataChanged(Self, BarInfo, DragType);
end;

procedure TCustomEzGanttChart.DoDrawBegin(var Erase: Boolean);
begin
  if Assigned(FOnDrawBegin) then FOnDrawBegin(Self, Erase);
end;

function TCustomEzGanttChart.DoGetHint(const HitInfo: TEzGanttHitInfo): string;
begin
  Result := GetShortHint(Hint);
  if Assigned(FOnGetHint) then
    FOnGetHint(Self, HitInfo, Result);
end;

function TCustomEzGanttChart.DoEndbarDrag(X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType): Boolean;
begin
  FPaintDragbar := True;
  try
    // Update dataset defaults to true when dd existing bars or when
    // a new bar has valid fields.
    Result := (DragType <> bdNewBar) or (BarInfo.BarProps.StartField <> '');
    if Assigned(FOnEndbarDrag) then FOnEndbarDrag(Self, X, Y, BarInfo, DragType, Result);
  finally
    FPaintDragbar := False;
  end;
end;

function TCustomEzGanttChart.DoDeselectGanttbar(ABarRef: TEzGanttbarReference): Boolean;
begin
  Result := True;
  if Assigned(FOnDeselectGanttbar) then FOnDeselectGanttbar(Self, ABarRef, Result);
end;

function TCustomEzGanttChart.DoSelectPredecessorline(Predecessor: TPredecessor): Boolean;
begin
  Result := True;
  if Assigned(FOnSelectPredecessorline) then FOnSelectPredecessorline(Self, Predecessor, Result);
end;

procedure TCustomEzGanttChart.DoDeselectPredecessorline(Predecessor: TPredecessor);
begin
  if Assigned(FOnDeselectPredecessorline) then FOnDeselectPredecessorline(Self, Predecessor);
end;

procedure TCustomEzGanttChart.DoSelectionChanged;
begin
  if Assigned(FOnSelectionChanged) then FOnSelectionChanged(Self);
end;

function TCustomEzGanttChart.DoSelectGanttbar(ABar: TScreenbar): Boolean;
begin
  Result := True;
  if Assigned(FOnSelectGanttbar) then FOnSelectGanttbar(Self, ABar, Result);
end;

function TCustomEzGanttChart.DoStartbarDrag(X, Y: Integer; BarInfo: TScreenbar; DragType: TGanttBarDragType): Boolean;
begin
  Result := true;
  if Assigned(FOnStartbarDrag) then FOnStartbarDrag(Self, X, Y, BarInfo, DragType, Result);
end;

function TCustomEzGanttChart.DoStartLineMarkerDrag(X: Integer; LineMarker: TEzGanttLineMarker): Boolean;
begin
  Result := true;
  if Assigned(FOnLineMarkerStartDrag) then FOnLineMarkerStartDrag(Self, X, LineMarker, Result);
end;

function TCustomEzGanttChart.DoEndLineMarkerDrag(X: Integer; LineMarker: TEzGanttLineMarker; var NewPosition: TDateTime): Boolean;
begin
  Result := true;
  if Assigned(FOnLineMarkerEndDrag) then FOnLineMarkerEndDrag(Self, X, LineMarker, NewPosition, Result);
end;

procedure TCustomEzGanttChart.DoLineMarkerDragging(X: Integer; LineMarker: TEzGanttLineMarker);
begin
  if Assigned(FOnLineMarkerDragging) then FOnLineMarkerDragging(Self, X, LineMarker);
end;

procedure TCustomEzGanttChart.DoOnPrepareCacheRow(CurrentNode: TRecordNode; RowNo: integer; aStates :TGanttbarStates);
begin
  if Assigned(FOnPrepareCacheRow) then
    FOnPrepareCacheRow(self, CurrentNode, RowNo, AStates);
end;

procedure TCustomEzGanttChart.CheckDataset;
begin
  if not Assigned(Datalink.Dataset) then
    EzGanttError(SNoDataset, Self);
end;

function TCustomEzGanttChart.CanUseTimebar: Boolean;
begin
  Result := (Timebar<>nil) and Timebar.HasActiveScale;
end;

procedure TCustomEzGanttChart.ClearDragEffects;
begin
  if doLinkTasks in FActiveDragOperation then
  begin
    SetPredecessorLinePen(Canvas.Pen);
    Windows.Polyline(Canvas.Handle, PPredecessorlinePoints(@FDragPoints)^, FDragPointCount);
    FDragPointCount := 0;
  end

  // Clean up doMoveTasks effect
  // Not required when doMoveHorizontal is also set because then
  // a drag image is used.
  else if (FActiveDragOperation * [doMoveTasks, doMoveHorizontal])=[doMoveTasks] then
  begin
    with Canvas do
    begin
      Pen.Style := psSolid;
      Pen.Mode := pmNotXor;

      case FDragEffect of
        hpInsertAsChild:
        begin
          Pen.Width := 1;
          Pen.Color := $E0E0E0;
          Brush.Color := $E0E0E0;
          Rectangle(FDragHitInfo.HitRowRect.Left, FDragHitInfo.HitRowRect.Top, FDragHitInfo.HitRowRect.Right, FDragHitInfo.HitRowRect.Bottom);
        end;

        hpInsertTask:
        begin
          Pen.Color := clBlue;
          Pen.Width := 2;
          MoveTo(FDragHitInfo.HitRowRect.Left, FDragHitInfo.HitRowRect.Top-1);
          LineTo(FDragHitInfo.HitRowRect.Right, FDragHitInfo.HitRowRect.Top-1);
        end;

        hpInsertTaskAfter:
        begin
          Pen.Color := clBlue;
          Pen.Width := 2;
          MoveTo(FDragHitInfo.HitRowRect.Left, FDragHitInfo.HitRowRect.Bottom);
          LineTo(FDragHitInfo.HitRowRect.Right, FDragHitInfo.HitRowRect.Bottom);
        end;
      end;
      Pen.Mode := pmCopy;
    end;
  end;
  FActiveDragOperation := [];
end;

procedure TCustomEzGanttChart.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  rtmp: TRect;
  HitInfo: TEzGanttHitInfo;

  function GetDragType: TGanttBarDragType;
  begin
    Result := bdNone;

    if (hpOnBar in HitInfo.HitPositions) then
      with HitInfo, HitInfo.HitScreenbar do
      begin
        // hovering over the left side of a bar having a progressbar?
        if (hpOnBarStart in HitPositions) and
           Assigned(BarProps.Original) and Assigned(BarProps.Original.ProgressBarPtr) and
           (gfCanResize in BarProps.Original.ProgressBarPtr.Flags) and
           // Verify that bar does not have a progress bar jet
           (FDrawInfo[ScreenRowToCacheIndex(HitRow)].FindProgressBar(HitInfo.HitScreenbar)=-1)
        then
          Result := bdAddProgressBar

        // KV: 31/7/2006
        // Removed BarCanDrag check.
        // With this check, a summary bar could never be moved/resized.
        // We now leave it to the developer to handle this.
        else {if BarCanDrag(HitRow, HitBar) then}
        begin
          if (HitPositions * [hpOnBarStart, hpOnBarStop] = [hpOnBarStart, hpOnBarStop]) then
            Result := bdMilestone

          else if (hpOnBarStop in HitPositions) and (gfCanResize in BarProps.Flags) then
          begin
            if BarProps.ParentBar <> nil then
              Result := bdProgressBar else
              Result := bdBarDate;
          end

          else if (hpOnBarStart in HitPositions) and (gfCanResizeLeft in BarProps.Flags) then
          begin
            Result := bdBarStartDate;
          end

          else
            //
            // We are hovering over a bar, test whether this bar may be moved
            //
          begin
            // if hovering over an progressbar, shift focus to parent bar
            if BarProps.ParentBar <> nil then
            begin
              HitBar := FDrawInfo[ScreenRowToCacheIndex(HitRow)].IndexOfBar(BarProps.ParentBar);
              HitScreenBar := FDrawInfo[ScreenRowToCacheIndex(HitRow)][HitBar];
            end;

            // Does bar allow moving?
            if (BarProps.Flags * [gfCanMove, gfCanLink])<>[] then
              Result := bdBarHorizontal;
          end;
        end;

        if (Result = bdNone) and (gfCanDragDrop in BarProps.Flags) then
          Result := bdBarHorizontal;
      end
    else if (hpOnLineMarker in HitInfo.HitPositions) then
      Result := bdLineMarker;
  end;

  procedure StartRectangularSelection;
  begin
    SetSelectionPen(Canvas);
    SetRect(FSelectionRectangle, HitInfo.HitWhere.X, HitInfo.HitWhere.Y, X, Y);
    FDragType := bdSelection;
    Canvas.DrawFocusRect(FSelectionRectangle);
    RunTimer(tmrSelection, 100);
  end;

  procedure StartbarDragging(DragType: TGanttBarDragType; IsHidden: Boolean);
  var
    Bar: TScreenBar;
    NewBar: TEzGanttBar;

  begin
    SetRectEmpty(FDragRect);
    FDragPointCount := 0;
    FMouseBarOffset := Point(0, 0);

    if DragType <> bdNewBar then
    begin
      FDragStartBar := FDrawInfo[ScreenRowToCacheIndex(HitInfo.HitRow)][HitInfo.HitBar];
      FDragstartNode := (Datalink.Dataset as TEzDataset).RecordNode;
      FDragRect := HitInfo.HitScreenbar.ActualSize;
    end;

    case DragType of
      bdAddProgressBar:
      begin
        FDragType := bdAddProgressBar;
        Bar := FDragStartBar;
        FDragStartBar := TScreenbar.Create;

        // Assign start and stop dates to drag bar
        FDragStartBar.Assign(Bar);
        // Copy visual properties of progress bar to drag bar
        FDragStartBar.BarProps.Assign(Bar.BarProps.Original.ProgressBarPtr);
        FDragStartBar.BarProps.FParentBar := Bar.BarProps;
        FDragStartBar.BarStates := Bar.BarStates;
        FDragRect.Right := FDragRect.Left+2; // avoid single point bar
      end;

      bdProgressBar:
        FDragType := bdProgressBar;
      bdMileStone: FDragType := bdMilestone;
      bdBarDate, bdBarStartDate:
      begin
        FDragType := DragType;
        {
        // Remove progressbar if bar has one
        if Assigned(FDragStartBar.BarProps.ProgressBarPtr) then
        begin
          FDrawInfo[ScreenRowToCacheIndex(FDragStartBar.Row)].Delete(FDrawInfo[ScreenRowToCacheIndex(FDragStartBar.Row)].IndexOfBar(FDragStartBar.BarProps.ProgressBarPtr));
          FDragStartBar.BarProps.FProgressBar := nil;
        end;
        }
      end;
      bdBarHorizontal:
      begin
        // Start a drag drop operation
        FDragType := bdDragDrop;
        FMouseBarOffset := Point(FMouseDownHitInfo.HitWhere.x-FDragRect.Left, FMouseDownHitInfo.HitWhere.y-FDragRect.Top);

        // Remove progressbar if bar has one
        {
        if Assigned(FDragStartBar.BarProps.ProgressBarPtr) then
        begin
          i := FDrawInfo[ScreenRowToCacheIndex(FDragStartBar.Row)].IndexOfBar(FDragStartBar.BarProps.ProgressBarPtr);
          if i<>-1 then
            FDrawInfo[ScreenRowToCacheIndex(FDragStartBar.Row)].Delete(i);
          FDragStartBar.BarProps.FProgressBar := nil;
        end;
        }
      end;

      bdNewBar:
      begin
        FDragType := bdNewBar;
        FDragStartBar := TScreenbar.Create;
        FDragStartBar.Row := FMouseDownHitInfo.HitRow;
        FDragStartBar.Node := nil;
        FDragStartBar.BarStart := Timebar.X2DateTime(FMouseDownHitInfo.HitWhere.x);
        SnapDate(FDragStartBar.BarStart);
        FDragStartBar.BarStop := Timebar.X2DateTime(X);
        SnapDate(FDragStartBar.BarStop);
        SetRect(FDragRect, Timebar.DateTime2X(FDragStartBar.BarStart), HitInfo.HitRowRect.Top, Timebar.DateTime2X(FDragStartBar.BarStop), HitInfo.HitRowRect.Bottom);

        // Initialize barprops with Ganttbar having gfCanCreate flag
        NewBar := CanCreateBar;
        FDragStartBar.BarProps.Assign(NewBar);

        if (Datalink.Dataset as TEzDataset).SelfReferencing then
        begin
          if (Datalink.Dataset as TEzDataset).HasChildren then
            FDragStartBar.BarStates := [sfIsHiddenNode] else
            FDragStartBar.BarStates := [];
        end
        else
        begin
          if NewBar.Visibility.FEzDatalinkLevel>(Datalink.Dataset as TEzDataset).Level then
            FDragStartBar.BarStates := [sfIsHiddenNode] else
            FDragStartBar.BarStates := [];
        end;
        SetCursor(Screen.Cursors[crAddBar]);
      end;

      bdDragDrop:
        FDragType := bdDragDrop;
    end;

    if FDragType = bdDragDrop then
    begin
      FDragstartNode := FDragStartBar.Node;
      BeginDrag(true, 0);
      FDragStartBar := nil; // DragStartbar not used during dd.
    end

    else if DoStartbarDrag(X, Y, FDragStartBar, FDragType) then
    begin
      // Prepare screen for an internal move operation (like resizing a bar)
      // i.e. bar being dragged is removed from screen.

      // Create a TEzGanttDragObject
      // This object is used to handle the drag image
      FDragObject := TEzGanttDragObject.Create(Self);
      InitializeDragObject(FDragObject, True);
      RunTimer(tmrScroll, 100);
    end
    else
      //
      // Dragging was canceled in OnStartbarDrag event
      //
    begin
      if FDragType in [bdNewbar, bdAddProgressBar] then
        FreeAndNil(FDragStartBar);

      FDragType := bdNone;
      FDragStartBar := nil;
    end;
  end;

  procedure StartLineMarkerDrag;
    //
    // Starts dragging of line markers
    //
  var
    i: integer;
  begin
    FDragType := bdLineMarker;
    FDragMarker := HitInfo.HitLineMarker;

    if DoStartLineMarkerDrag(X, FDragMarker) then
      with FLineMarkers do
      begin
        FDragRect.Top := 0;
        FDragRect.Bottom := RowRect(VisibleRowCount-1).Bottom + FGridLineWidth;

        // Calculate positions of
        for i:=0 to Selected.Count-1 do
          with TEzGanttLineMarker(Selected[i]) do
            FDragPos := Timebar.DateTime2X(Position);

        UpdateWindow(Handle);
        DrawSelectedLineMarkers;
        RunTimer(tmrScroll, 100);
      end;
  end;

  function HasCanCreateBar: Boolean;
  begin
    Result := CanCreateBar<>nil;
  end;

  procedure SaveStartbarDragging(DragType: TGanttBarDragType; IsHidden: Boolean);
  begin
    FDragOperationStarting := True;
    try
      StartbarDragging(DragType, IsHidden);
    finally
      FDragOperationStarting := False;
    end;
  end;

  procedure StartDragging;
  var
    dt: TGanttBarDragType;

  begin
    dt := GetDragType;
    if dt = bdLineMarker then
      StartLineMarkerDrag

    else if (dt <> bdNone) then
      SaveStartbarDragging(dt, False)

    else
    begin
      if (hpOnRow in HitInfo.HitPositions) and
         (gaoCanCreateBars in FOptions) and
         (HasCanCreateBar) and
         (abs(Y-FMouseDownHitInfo.HitWhere.y)<2) and
         (X>FMouseDownHitInfo.HitWhere.x)
      then
      begin
        // Remove any selected ganttbars
        FSelectedGanttbars.Clear;
        if (Datalink.Dataset as TEzDataset).HasChildren then
          SaveStartbarDragging(bdNewBar, True) else
          SaveStartbarDragging(bdNewBar, False);
      end
      else if (gaoDoRectangularSelection in Options) then
        StartRectangularSelection;
    end;
  end;

  procedure UpdateCursor;
  var
    NewCursor: TCursor;

  begin
    NewCursor := crDefault;

    if not (hpNowhere in HitInfo.HitPositions) then
    begin
      if (hpOnPredecessorline in HitInfo.HitPositions) then
        NewCursor := crPredecessorline

      else case GetDragType of
        bdProgressBar,
        bdAddProgressBar:
          NewCursor := crTaskProgress;
        bdMileStone,
        bdBarDate,
        bdBarStartDate:
          NewCursor := crMoveDate;
        bdBarHorizontal: NewCursor := crCanMoveBar;
        bdLineMarker: NewCursor := crMoveLineMarker;
        bdDragDrop: NewCursor := crDragGanttbar;
      end;
    end;

    Cursor := NewCursor;
  end;

var
  RedrawDragBitmap: Boolean;
  w: integer;

begin
  if FDragType = bdNone then
  begin
    if (ssLeft in Shift) then
    begin
      if (abs(FMouseDownHitInfo.HitWhere.X - X) > FDragThreshold) or (abs(FMouseDownHitInfo.HitWhere.Y - Y) > FDragThreshold) then
      begin
        HitInfo := FMouseDownHitInfo;
        StartDragging;
      end;
    end
    else
    begin
      GetHitTestInfoAt(X, Y, HitInfo);
      UpdateCursor;
      DoOnOverGanttChart(HitInfo);
    end;
  end
    //
    // Handle dragging of line markers
    //
  else if DragType = bdLineMarker then
    MoveSelectedLineMarkers(X)

    //
    // Update selection rectangle
    //
  else if DragType = bdSelection then
  begin
    SetSelectionPen(Canvas);
    Canvas.DrawFocusRect(FSelectionRectangle);
    SetRect(FSelectionRectangle, min(FMouseDownHitInfo.HitWhere.X, X), min(FMouseDownHitInfo.HitWhere.Y, Y), max(FMouseDownHitInfo.HitWhere.X, X), max(FMouseDownHitInfo.HitWhere.Y, Y));
    Canvas.DrawFocusRect(FSelectionRectangle);
  end
    //
    // Handle dragging of bars, milestones and others types
    //
  else if FDragType in [{bdBarHorizontal, }bdMilestone, bdBarDate, bdBarStartDate, bdNewbar, bdProgressBar, bdAddProgressBar] then
  begin
    rtmp := FDragRect;

    if FDragType = bdMilestone then
    begin
      rtmp.Left := X;
      rtmp.right := rtmp.left;
    end
    else if FDragType = bdBarStartDate then
      rtmp.Left := min(rtmp.Right, X)
    else
      rtmp.Right := max(rtmp.Left, X);

    // Exit if bar hasn't moved
    if not UpdateBarDates(FDragStartBar, rtmp) then Exit;

    // Restore original screen
    with FDragObject.FDragBitmapRect do
      Canvas.CopyRect(
            FDragObject.FDragBitmapRect,
            FDragOriginal.Canvas,
            Rect(0,0, Right-Left, Bottom-Top)
      );

    w := GetRectW(FDragRect);
    FDragRect.Left := Timebar.DateTime2X(FDragStartBar.BarStart);
    FDragRect.Right := Timebar.DateTime2X(FDragStartBar.BarStop);

    RedrawDragBitmap := GetRectW(FDragRect)<>w;

    // Call event handler.
    // Handler returns true when the FDrawBitmap needs new preparation.
    if BarDragging(X, Y, FDragStartBar, FDragType) then
    begin
      FDragRect.Left := Timebar.DateTime2X(FDragStartBar.BarStart);
      FDragRect.Right := max(FDragRect.Left, Timebar.DateTime2X(FDragStartBar.BarStop));
      RedrawDragBitmap := true;
    end;

    if GetRectW(FDragRect)>ClipedDragThreshold then
      // Large ganttbar ==> only prepare visible part of bar
    begin
      PaintGanttbar(FDragBitmap, EndPoints, ClientRect, FDragRect, FDragObject.FDragBitmapRect, FDragObject.FDragClickRect, rtmp, True, FDragStartBar.BarProps, [sfSelected]);
      IntersectRect(FDragObject.FDragBitmapRect, FDragObject.FDragBitmapRect, ClientRect);
    end
    else if RedrawDragBitmap then
      // Small ganttbar ==> prepare complete bitmap
    begin
      // Calculate size of bounding rectange
      PaintGanttbar(TCanvas(nil), EndPoints, rtmp, FDragRect, FDragObject.FDragBitmapRect, FDragObject.FDragClickRect, rtmp, True, FDragStartBar.BarProps, [sfSelected]);
      // paint ganttbar onto FDragBitmap's canvas
      // using bounding rectangle FDragBitmapRect as cliprect.
      PaintGanttbar(FDragBitmap, EndPoints, FDragObject.FDragBitmapRect, FDragRect, FDragObject.FDragBitmapRect, FDragObject.FDragClickRect, rtmp, False, FDragStartBar.BarProps, [sfSelected]);
    end
    else
      // Recalculate ganttbar rectangles based on new screen position.
      // Do not redraw ganttbar.
      PaintGanttbar(TCanvas(nil), EndPoints, rtmp, FDragRect, FDragObject.FDragBitmapRect, FDragObject.FDragClickRect, rtmp, True, FDragStartBar.BarProps, [sfSelected]);

    //
    // Before painting ganttbar to screen, make a copy of the current contents
    //
    with FDragObject.FDragBitmapRect do
    begin
      FDragOriginal.Width := max(FDragOriginal.Width, Right-Left);
      FDragOriginal.Height := max(FDragOriginal.Height, Bottom-Top);
      FDragOriginal.Canvas.CopyRect(
            Rect(0,0, Right-Left, Bottom-Top),
            Canvas,
            FDragObject.FDragBitmapRect
      );

      TransparentDraw(Canvas, FDragBitmap, FDragObject.FDragBitmapRect, True);

      if sfLeftSnap in FDragStartBar.BarStates then
        with FDragObject.FDragClickRect do
          Canvas.Draw(Left-SnapPoint.Width-1, Top+(Bottom-Top) div 2 - SnapPoint.Height div 2, SnapPoint);

      if sfRightSnap in FDragStartBar.BarStates then
        with FDragObject.FDragClickRect do
          Canvas.Draw(Right+1, Top+(Bottom-Top) div 2 - SnapPoint.Height div 2, SnapPoint);
    end;
  end;

  inherited;
end;

procedure TCustomEzGanttChart.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

  procedure UpdateDatasetCursor(vPos: Integer);
  var
    Move: Integer;
  begin
    // Let dataset follow mouse cursor
    Move := GetRowAt(vPos) - Datalink.ActiveRecord;
    if Move <> 0 then
    begin
      FDatalink.MoveBy(Move);
      Update;
      GetHitTestInfoAt(X, Y, FMouseDownHitInfo);
    end;
  end;

begin
  if (Button = mbLeft) then
  begin
    if not AcquireFocus then Exit;

    GetHitTestInfoAt(X, Y, FMouseDownHitInfo);

    if hpOnBar in FMouseDownHitInfo.HitPositions then
    begin
      if not (ssShift in Shift) and not (sfSelected in FMouseDownHitInfo.HitScreenbar.BarStates) then
        FSelectedGanttBars.Clear;

      if not (sfSelected in FMouseDownHitInfo.HitScreenbar.BarStates) and
             (gfCanSelect in FMouseDownHitInfo.HitScreenbar.BarProps.Flags)
      then
        FSelectedGanttBars.Add(RowNode(FMouseDownHitInfo.HitRow), FMouseDownHitInfo.HitScreenbar);
    end
    else if not (ssShift in Shift) then
      FSelectedGanttBars.Clear;

    if hpOnLineMarker in FMouseDownHitInfo.HitPositions then
      with FLineMarkers do
      begin
        if not (ssShift in Shift) and
           not IsSelected(FMouseDownHitInfo.HitLineMarker)
        then
          DeSelectLineMarker(nil);

        SelectLineMarker(FMouseDownHitInfo.HitLineMarker);
      end;

    // Remove selected predecessorline from screen
    if FSelectedPredecessorLine <> nil then
      DeselectPredecessorLine;

    if (hpOnPredecessorline in FMouseDownHitInfo.HitPositions) and
        DoSelectPredecessorline(FMouseDownHitInfo.HitPredecessorline.Predecessor)
    then
      //
      // Hovering over a predecessorline, highlight it!
      //
    begin
      FSelectedPredecessorLine := TVisiblePredecessorLine.Create;
      FSelectedPredecessorLine.Assign(FMouseDownHitInfo.HitPredecessorline);
      HighlightPredecessorline;
    end;

    if not (ssDouble in Shift) and (hpOnRow in FMouseDownHitInfo.HitPositions) then
      UpdateDatasetCursor(Y);
  end;

  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TCustomEzGanttChart.StartDatasetEdit(RowNode, BarNode: TRecordNode);
begin
  FRestoreAccess := false;
  with Datalink.Dataset as TEzDataset do
    if not (State in [dsEdit, dsInsert]) then
    begin
      if (RowNode<>BarNode) then // Editing a hidden record
      begin
        FRestoreAccess := true;
        StartDirectAccess([cpShowAll]);
        if FDragType = bdNewbar then
          InsertChild else
          ActiveCursor.CurrentNode := BarNode;
      end else
        RecordNode := RowNode;

      Edit;
    end;
end;

procedure TCustomEzGanttChart.StopDatasetEdit;
begin
  with Datalink.Dataset as TEzDataset do
  try
    if (State in [dsEdit, dsInsert]) and
       (
          (gaoPostAllEdits in Self.Options)
         or
          // Edits to hidden records are always posted immediately
          (FDragStartBar<>nil) and (sfIsHiddenNode in FDragStartBar.BarStates)
       )
    then
      Post;
  finally
    if FRestoreAccess then
      EndDirectAccess;
  end;
end;

procedure TCustomEzGanttChart.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

  procedure UpdateDataset;
  var
    dtStart, dtStop: TDateTime;
    f: double;

  begin
    with FDragStartBar do
      case FDragType of
        bdNewbar:
          with Datalink.Dataset do
          begin
            FieldByName(BarProps.StartField).AsDateTime := BarStart;
            if BarProps.StopField <> '' then
            begin
              if BarProps.EndpointType = etFixed then
                FieldByName(BarProps.StopField).AsDateTime := BarStop else
                FieldByName(BarProps.StopField).AsFloat := BarStop-BarStart;
            end;

            // DragType must be set to bdNone,
            // otherwise dragged ganttbar will not be painted.
            FDragType := bdNone;
            FDrawInfo[ScreenRowToCacheIndex(FDragStartBar.Row)].Add(FDragStartBar);
          end;

        bdProgressBar, bdAddProgressBar:
          with Datalink.Dataset do
          begin
            FieldByName(BarProps.StartField).AsDateTime := BarStart;
            case BarProps.EndpointType of
              etFixed:
                FieldByName(BarProps.StopField).AsDateTime := BarStop;

              etDuration:
                FieldByName(BarProps.StopField).AsFloat := BarStop-BarStart;

              etPercentage:
              begin
                GetGanttbarDates(BarProps.ParentBar, dtStart, dtStop);
                f := (BarStop-BarStart) / (dtStop-dtStart);
                if FieldByName(BarProps.StopField).DataType = ftFloat then
                  FieldByName(BarProps.StopField).AsFloat := f else
                  FieldByName(BarProps.StopField).AsInteger := round(f * 100);
              end;
            end;

            // Add Newly created progressbar to screencache
            if DragType = bdAddProgressBar then
            begin
              FDragStartBar.BarProps.FParentBar.FProgressBar := FDragStartBar.BarProps;
              FDrawInfo[ScreenRowToCacheIndex(FDragStartBar.Row)].Add(FDragStartBar);
            end;
          end;

        bdBarDate:
          with Datalink.Dataset do
            if BarProps.EndpointType = etFixed then
              FieldByName(BarProps.StopField).AsDateTime := BarStop else
              FieldByName(BarProps.StopField).AsFloat := BarStop-BarStart;

        bdBarStartDate:
          with Datalink.Dataset do
            FieldByName(BarProps.StartField).AsDateTime := BarStart;

        bdMilestone:
          Datalink.Dataset.FieldByName(BarProps.StartField).AsDateTime := BarStart;
      end;

      FDragRect := RowRect(FDragStartBar.Row);
      InvalidateRect(Handle, @FDragRect, False);
  end;

var
  i: integer;
  dtNewPosition, dtDelta: TDateTime;
  SavedDragType: TGanttBarDragType;

begin
  // Ignore event when a drag and drop operation is being started
  if FDragOperationStarting then Exit;

  try
    if FDragType = bdNone then
    begin
      if not (hpOnLineMarker in FMouseDownHitInfo.HitPositions) then
        FLineMarkers.DeselectLineMarker(nil);
    end
    else if FDragType = bdLineMarker then
      with FLineMarkers do
      begin
        Lines.BeginUpdate;
        try
          dtNewPosition := Timebar.X2DateTime(X);
          SnapDate(dtNewPosition);

          if DoEndLineMarkerDrag(X, FDragMarker, dtNewPosition) then
          begin
            dtDelta := dtNewPosition - FDragMarker.Position;
            for i:=0 to Selected.Count-1 do
              with TEzGanttLineMarker(Selected[i]) do
              begin
                dtNewPosition := Position + dtDelta;
                SnapDate(dtNewPosition);
                Position := dtNewPosition;
                FDragPos := 0;
              end;
            Invalidate;
          end;
        finally
          Lines.EndUpdate;
        end;
      end

    else if FDragType = bdSelection then
    begin
      StopTimer(tmrSelection);
      SetSelectionPen(Canvas);
      Canvas.DrawFocusRect(FSelectionRectangle);
      if gaoDoRectangularSelection in Options then
        SelectBarsInRectangle(FSelectionRectangle);
    end

    else if FDragType <> bdDragDrop then
    begin
      SavedDragType := FDragType;

      if DoEndbarDrag(X, Y, FDragStartBar, FDragType) then
      try
        StartDatasetEdit(RowNode(FDragStartBar.Row), FDragStartBar.Node);
        FDragType := SavedDragType;

        UpdateDataset;
        DoBardataChanged(FDragStartBar, SavedDragType);
      finally
        StopDatasetEdit;
      end else
        // Make sure screen is restored in original state
        InvalidateRect(Handle, @FDragObject.FDragBitmapRect, False);
    end;

    inherited;
  finally
    StopDragging;
  end;
end;

procedure TCustomEzGanttChart.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);

  if Operation = opRemove then
  begin
    if (FDataLink <> nil) then
      if AComponent = DataSource then
      begin
        DataSource := nil;
      end;

    if AComponent = EndPoints then
      EndPoints := nil;

    if AComponent = FTimebarLink.Timebar then
      FTimebarLink.Timebar := nil;
  end;
end;

function TCustomEzGanttChart.ScreenRowToCacheIndex(ARow: Integer): Integer;
begin
  if (gaoCacheAllRows in Options) then
    Result := FDrawInfo.ScreenTopRecNo-1+ARow else
    Result := ARow;
end;

procedure TCustomEzGanttChart.ScrollVertical(Distance: Integer);
var
  R, RowsRect: TRect;
  FirstCacheRow, LastCacheRow, BarIndex, RowIndex, Count, VOffset: Integer;

begin
  if HandleAllocated then
  begin
    if (Distance <> 0) then
    begin
      UpdateVerticalScrollBar;

      if (FDragType <> bdNone) then
        ClearDragEffects;
      Count := VisibleRowCount;

      if Abs(Distance) >= Count then
      begin
        if not (gaoCacheAllRows in Options) then
          FDrawInfo.ScreenCache.ClearBars;
        Invalidate;
        Exit;
      end
      else
      begin
        FirstCacheRow := 0;
        LastCacheRow := 0;
        VOffset := 0;
        if Distance > 0 then
        begin
          R := RowRect(Distance);
          VOffset := -R.Top;
          FirstCacheRow := 0;
          LastCacheRow := Count-Distance-1;
        end;

        if not (gaoCacheAllRows in Options) then
          //
          // Cache never scrolls when all rows are cached
          //
          FDrawInfo.ScrollCache(Distance);

        if Distance < 0 then
        begin
          R := RowRect((-Distance)-1);
          VOffset := R.Bottom + FGridLineWidth;
          FirstCacheRow := -Distance;
          LastCacheRow := Count-1;
        end;

        if gaoCacheAllRows in Options then
        begin
          FirstCacheRow := ScreenRowToCacheIndex(FirstCacheRow+Distance);
          LastCacheRow := ScreenRowToCacheIndex(LastCacheRow+Distance);
        end;

        // Update position of rectangles for each bar remaining in the
        // screencache.
        for RowIndex:=FirstCacheRow to LastCacheRow do
          with FDrawInfo.ScreenCache[RowIndex] do
            for BarIndex:=0 to Count-1 do
              with Items[BarIndex] do
              begin
                Row := RowIndex;
                OffsetRect(ActualSize, 0, VOffset);
                OffsetRect(BoundRect, 0, VOffset);
                OffsetRect(ClickRect, 0, VOffset);
                OffsetRect(BarRect, 0, VOffset);
              end;

        if DragType = bdSelection then
          // Remove selection rectangle from screen
          Canvas.DrawFocusRect(FDragRect);

        RowsRect := RowRect(Count-1);
        RowsRect.Top := 0;

        // Scroll current window
        //
        // If an update region already exists, we do not scroll the window
        // but instead invalidate the entire control.
        //
        if GetUpdateRect(Handle, R{dummy parameter}, false)=false then
        begin
          R := ClientRect;
          R.Bottom := RowsRect.Bottom;
          ScrollWindowEx(Handle, 0, VOffset, @RowsRect, @R, 0, nil, 0);

          if VOffset < 0 then
          begin
            RowsRect.Top := RowsRect.Bottom+VOffset;
            RowsRect.Bottom := ClientHeight;
            InvalidateRect(Handle, @RowsRect, False);
          end
          else
          begin
            RowsRect.Top := 0;
            RowsRect.Bottom := VOffset;
            InvalidateRect(Handle, @RowsRect, False);
          end;
        end else
          Invalidate;

        if DragType = bdSelection then
        begin
          UpdateWindow(Handle);
          inc(FMouseDownHitInfo.HitWhere.y, VOffset);
          Canvas.DrawFocusRect(FDragRect);
        end
        else if (FDragType <> bdNone) then
        begin
          OffsetRect(FDragRect, 0, VOffset);
          inc(FMouseDownHitInfo.HitWhere.y, VOffset);
        end;
      end;
    end;
  end;
end;

procedure TCustomEzGanttChart.SelectBarsInRectangle(ARect: TRect);
var
  CacheIndex, AbsIndex, MaxIndex, RowTop, Y, RowBottom, i: Integer;
  ACacheRow, TempRow: TScreenRow;
  TheBar: TScreenbar;
  AState: TGanttbarStates;
  R: TRect;

begin
  TempRow := TScreenRow.Create(0, 0);
  try
    RowTop := ARect.Top;
    AbsIndex := GetRowAt(RowTop)+FDrawInfo.ScreenTopRecNo-1;

    Y := ARect.Bottom;
    MaxIndex := GetRowAt(Y)+FDrawInfo.ScreenTopRecNo-1;

    while (AbsIndex<FDatalink.Dataset.RecordCount) and (AbsIndex<=MaxIndex) do
    begin
      CacheIndex := AbsIndex-(FDrawInfo.ScreenTopRecNo-1);
      RowBottom := RowTop+GetRowHeight(CacheIndex)+FGridlineWidth;

      if not (gaoCacheAllRows in Options) and ((CacheIndex<0) or (CacheIndex>=FDrawInfo.RecCount)) then
      begin
        TempRow.Clear;
        ACacheRow := TempRow;
      end else
        ACacheRow := FDrawInfo[CacheIndex];

      if not ACacheRow.IsPrepared then
        PrepareCacheRow(CacheIndex, ACacheRow);

      i := ACacheRow.Count-1;
      while (i >= 0) do
      begin
        TheBar := ACacheRow[i];

        if not (sfSelected in TheBar.BarStates) and (gfCanSelect in TheBar.BarProps.Flags) then
        begin
          AState := [];

          // Calculate outer bounds of ganttbar
          with TheBar do
          begin
            // ActualSize hold the rectangle of the ganttbar
            SetRect(
              ActualSize,
              Timebar.DateTime2X(BarStart),
              RowTop,
              Timebar.DateTime2X(BarStop),
              RowBottom
            );
            PaintGanttbar(TCanvas(nil), EndPoints, ARect, ActualSize,
                BoundRect, ClickRect, BarRect, True, BarProps, AState);

            IntersectRect(R, ARect, ClickRect);
            if not IsRectEmpty(R) then
              FSelectedGanttBars.Add(RowNode(TheBar.Row), TheBar);
          end;
        end;
        dec(i);
      end;
      RowTop := RowBottom;
      inc(AbsIndex);
    end;
  finally
    TempRow.Destroy;
  end;
end;

function TCustomEzGanttChart.SetBarProps(RowNode: TRecordNode; BarInfo: TScreenbar): Boolean;
var
  cur: TRecordNode;

begin
  Result := True;
  if Assigned(FGetBarProps) then
    with DataLink.Dataset as TCustomEzDataset do
    begin
      cur := ActiveCursor.CurrentNode;
      try
        FGetBarProps(Self, RowNode, BarInfo, Result);
      finally
        ActiveCursor.CurrentNode := cur;
      end;
    end;
end;

procedure TCustomEzGanttChart.SetBorderStyle(Value: TBorderStyle);
begin
  if FBorderStyle <> Value then
  begin
    FBorderStyle := Value;
    RecreateWnd;
  end;
end;

procedure TCustomEzGanttChart.SetDataSource(Value: TDataSource);
begin
  if Value = FDatalink.Datasource then Exit;
{$IFDEF EZ_D5}
  if FDataLink.DataSource <> nil then FDataLink.DataSource.RemoveFreeNotification(Self);
{$ENDIF}
  FDataLink.DataSource := Value;
  if Value <> nil then Value.FreeNotification(Self);
end;

procedure TCustomEzGanttChart.SetGridLinePen(Value: TPen);
begin
  FGridLinePen.Assign(Value);
  Invalidate;
end;

procedure TCustomEzGanttChart.SetHatchOptions(Value: THatchOptions);
begin
  FHatchOptions.Assign(Value);
  Invalidate;
end;

procedure TCustomEzGanttChart.SetMinDate(D: TDateTime);
begin
  if D <> FMinDate then
  begin
    FMinDate := D;
    Invalidate;
  end;
end;

procedure TCustomEzGanttChart.SetMaxDate(D: TDateTime);
begin
  if D <> FMaxDate then
  begin
    FMaxDate := D;
    Invalidate;
  end;
end;

procedure TCustomEzGanttChart.SetOptions(Value: TGanttOptions);
begin
  Exclude(Value, gaoHatch);
  if FOptions <> Value then
    if Value - FOptions = [gaoDoSnap] then
      FOptions := Value
    else
    begin
      FOptions := Value;
      FDrawInfo.ScreenCache.ClearBars;
      Invalidate;
    end;
end;

procedure TCustomEzGanttChart.SetSlaveMode(Value: Boolean);
begin
  Datalink.SlaveMode := Value;
end;

procedure TCustomEzGanttChart.SetPredecessorLine(Source: TPredecessorLine);
begin
  FPredecessorLine.Assign(Source);
end;

procedure TCustomEzGanttChart.SetPredecessorLinePen(Pen: TPen);
begin
  Pen.Color := clRed;
  Pen.Style := psSolid;
  Pen.Mode := pmNotXor;
end;

procedure TCustomEzGanttChart.SetScrollBarOptions(Value: TEzGanttScrollBarOptions);
begin
  FScrollBarOptions.Assign(Value);
end;

procedure TCustomEzGanttChart.SetSelectionPen(ACanvas: TCanvas);
begin
  ACanvas.Pen.Color := clBlack;
  ACanvas.Pen.Style := psSolid;
  ACanvas.Brush.Color := clWhite;
  ACanvas.Brush.Style := bsSolid;
end;

procedure TCustomEzGanttChart.SetGanttbars(Value: TEzGanttBars);
begin
  FGanttbars.Assign(Value);
end;

procedure TCustomEzGanttChart.SetEndPoints(P: TEzEndPointList);
begin
  if FEndPoints <> P then
  begin
    FEndPoints := P;
    Invalidate;
  end;
end;

procedure TCustomEzGanttChart.SetLineMarkers(LH: TEzGanttLineMarkersHelper);
begin
  FLineMarkers.Assign(LH);
end;

procedure TCustomEzGanttChart.SetOverLapping(O: TEzOverlapSettings);
begin
  FOverlapping.Assign(O);
end;

procedure TCustomEzGanttChart.SetProgressline(Value: TEzProgresslineSettings);
begin
  FProgressline.Assign(Value);
end;

procedure TCustomEzGanttChart.SetRowColor(Value: TColor);
begin
  if Value<>FRowColor then
  begin
    FRowColor := Value;
    Invalidate;
  end;
end;

procedure TCustomEzGanttChart.SetSnapSize(S: string);
var
  i: integer;
begin
  i:=1;

  while (i <= length(S)) and {$IFDEF EZ_D2009}CharInSet(S[i], ['0'..'9']){$ELSE}(S[i] in ['0'..'9']){$ENDIF} do
    inc(i);

  if (i > 1) and (i <= length(S)) then
    FSnapCount := StrToInt(Copy(S, 1, i-1)) else
    FSnapCount := 0;

  while (i <= length(S)) and (Pos(S[i], 'mMhHdDwWqQyY') = 0) do
    inc(i);

  if (i <= length(S)) then
    case S[i] of
      'm': FSnapScale := msMinute;
      'M': FSnapScale := msMonth;
      'h', 'H': FSnapScale := msHour;
      'd', 'D': FSnapScale := msDay;
      'w', 'W': FSnapScale := msWeek;
      'q', 'Q': FSnapScale := msQuarter;
      'y', 'Y': FSnapScale := msYear;
    else
      FSnapCount := 0;
    end;
end;

procedure TCustomEzGanttChart.SetTimebar(Bar: TEzTimebar);
begin
  FTimebarLink.Timebar := Bar;
end;

procedure TCustomEzGanttChart.StopDragging;
begin
  StopTimer(tmrScroll);

  FDragStartBar := nil;
  FDragType := bdNone;
  FDragMarker := nil;
  FDropTargetBar := nil;
  FMouseDownHitInfo.HitPositions := [];

  if (FDragObject<>nil) and (FDragObject.Control=Self) then
    FreeAndNil(FDragObject);
end;

procedure TCustomEzGanttChart.StopTimer(idTimer: integer);
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
    KillTimer(Handle, idTimer);
end;

procedure TCustomEzGanttChart.Paint;
var
  LineDate: TDateTime;
  LinePos, i: integer;
  bErase: Boolean;
  R: TRect;

  procedure DrawScaleLines(Scale: TScale; ScaleCount, ScaleOffset: integer);
  begin
    with Canvas, Timebar, FDrawInfo do
    begin
      LineDate := TruncDate(Scale, ScaleCount, ScaleOffset, X2DateTime(UpdateRect.Left));
      LinePos := DateTime2X(LineDate);
      while LinePos <= UpdateRect.Right do
      begin
        MoveTo(LinePos, UpdateRect.Top);
        LineTo(LinePos, UpdateRect.Bottom);
        LineDate := AddTicks(Scale, ScaleCount, LineDate, 1);
        LinePos := DateTime2X(LineDate);
      end;
    end;
  end;

  procedure Hatch;
  var
    bf: TBlendFunction;
    R: TRect;

  begin
    with HatchOptions, Canvas, FDrawInfo do
    begin
{$IFNDEF BCB}
      if AlphaBlend then
      begin
        FAlphaBlendBitmap.Canvas.Brush.Color := FAlphaBlendColor;
        FAlphaBlendBitmap.Canvas.FillRect(Rect(0,0,FAlphaBlendBitmap.Width, FAlphaBlendBitmap.Height));

        bf.BlendOp := AC_SRC_OVER;
        bf.BlendFlags := 0;
        bf.SourceConstantAlpha := FAlphaBlendValue;
        bf.AlphaFormat := 0; //AC_SRC_ALPHA;

        // Hatch GantChart before project start
        LinePos := Timebar.DateTime2X(FMinDate);
        if (LinePos > UpdateRect.Left) then
        begin
          R := Rect(UpdateRect.Left-1, UpdateRect.Top-2, min(LinePos, UpdateRect.Right)+1, RowsRect.Bottom);
          Windows.AlphaBlend(
                Handle,
                R.Left, R.Top, R.Right-R.Left, R.Bottom-R.Top,
                FAlphaBlendBitmap.Canvas.Handle,
                0, 0, 1, 1,
                bf
            );
        end;

        // Hatch GantChart after project stop
        LinePos := Timebar.DateTime2X(FMaxDate);
        if (LinePos <= UpdateRect.Right) then
        begin
          R := Rect(max(0, LinePos), UpdateRect.Top-2, UpdateRect.Right+1, RowsRect.Bottom);
          Windows.AlphaBlend(
                Handle,
                R.Left, R.Top, R.Right-R.Left, R.Bottom-R.Top,
                FAlphaBlendBitmap.Canvas.Handle,
                0, 0, 1, 1,
                bf
            );
        end;
      end
      else
{$ENDIF} // Alphablend
      begin
        Pen.Assign(FHatchPen);
        Brush.Assign(FHatchBrush);

        // Hatch GantChart before project start
        LinePos := Timebar.DateTime2X(FMinDate);
        if (LinePos > UpdateRect.Left) then
          RectAngle(UpdateRect.Left-1, UpdateRect.Top-2, LinePos, RowsRect.Bottom);

        // Hatch GantChart after project stop
        LinePos := Timebar.DateTime2X(FMaxDate);
        if (LinePos <= UpdateRect.Right) then
          RectAngle(LinePos, UpdateRect.Top-2, UpdateRect.Right+1, RowsRect.Bottom);
      end;
    end;
  end;

  procedure DrawHorizontalLines;
  var
    Y, Row: Integer;
  begin
    with Canvas, FDrawInfo do
    begin
      Pen.Assign(FGridLinePen);
      Pen.Width := FGridlineWidth;
      Y := UpdateRect.Top - FGridlineWidth;
      Row := TopRow;
      while (Row < RecCount) do
      begin
        Inc(Y, GetRowHeight(Row) + FGridlineWidth);
        MoveTo(UpdateRect.Left, Y);
        LineTo(UpdateRect.Right, Y);
        Inc(Row);
      end;
    end;
  end;

  procedure DrawLineMarkers;
  var
    x, i, l: integer;
  begin
    with FLineMarkers.FVisibleLineMarkers, Canvas do
      if Upperbound(i, FDrawInfo.LeftDateExtend) then
      begin
        l:=i;
        while (l < Count) do
        begin
          x := Timebar.DateTime2X(Items[l].Position);
          if (x >= FDrawInfo.UpdateRect.Left) and (x < FDrawInfo.UpdateRect.Right) then
          begin
            Canvas.Pen := Items[l].Pen;
            if FLineMarkers.Selected.IndexOf(Items[l]) <> -1 then
              Canvas.Pen.Color := FLineMarkers.HighLightColor;

            Canvas.MoveTo(x, FDrawInfo.UpdateRect.Top);
            Canvas.LineTo(x, FDrawInfo.UpdateRect.Bottom);
          end;
          inc(l);
        end;
      end;
  end;

  procedure CalcDrawInfo;
  begin
    with FDrawInfo do
    begin
      if Assigned(EndPoints) and Assigned(EndPoints.Images) then
        EndPointSize := EndPoints.Images.Width else
        EndPointSize := 5; // must be greater than GanntBar.BarPen.width

      if not EzIsPrinting then
        UpdateRect := Canvas.ClipRect else
        UpdateRect := ClientRect;

      TopRow := GetRowAt(UpdateRect.Top);

      // Calculate date extends for current drawing rectangle.
      // These dates are used to determine if bars are visible.
      LeftDateExtend := Timebar.X2DateTime(UpdateRect.Left);
      RightDateExtend := Timebar.X2DateTime(UpdateRect.Right);

      if Datalink.Active then
      begin
        ActiveRecord := DataLink.ActiveRecord;
        DataLink.ActiveRecord := 0;
        ScreenTopRecNo := max(1, DataLink.Dataset.RecNo);
        ScreenTopNode := (DataLink.Dataset as TCustomEzDataset).TopNode;
        DataLink.ActiveRecord := TopRow;
        TopRecNo := max(1, DataLink.Dataset.RecNo);
        RecCount := DataLink.RecordCount;

        SetRect(ExcludedRect, 0, RowRect(RecCount-1).Bottom + FGridLineWidth, Width, Height);
      end
      else
      begin
        ActiveRecord := 0;
        ScreenTopRecNo := -1;
        TopRow := 0;
        TopRecNo := -1;
        RecCount := 0;
        SetRect(ExcludedRect, 0, RowRect(0).Bottom + FGridLineWidth, Width, Height);
      end;

      SetRect(RowsRect, 0, 0, Width, ExcludedRect.Top);
    end;
  end;

  procedure PrepareCacheRows;
  var
    Row, CacheIndex: Integer;
    RowRect: TRect;

  begin
    FShadowRowsChanged := False;
    FDrawBitmap.TransparentColor := EzTransparentColor;

    RowRect := FDrawInfo.UpdateRect;
    Row := FDrawInfo.TopRow;
    while (Row < FDrawInfo.RecCount) and (RowRect.Top < FDrawInfo.UpdateRect.Bottom) do
    begin
      RowRect.Bottom := RowRect.Top + GetRowHeight(Row);
      CacheIndex := ScreenRowToCacheIndex(Row);
      if (gaoCacheAllRows in Options) and (CacheIndex>=FDrawInfo.ScreenCache.Count) then
        FDrawInfo.SetScreenCacheSize(CacheIndex+1);

      if not FDrawInfo[CacheIndex].IsPrepared then
        PrepareCacheRow(Row, FDrawInfo[CacheIndex]);

      RowRect.Top := RowRect.Bottom + FGridlineWidth;
      Inc(Row);
    end;

    if FShadowRowsChanged then
    begin
      Invalidate;
//      Datalink.DataSet.Resync([]);
      Datalink.DataSet.Refresh;
      Abort;
    end;
  end;

  procedure DrawCacheRows;
  var
    Row: Integer;
    RowRect: TRect;

  begin
    FDrawBitmap.TransparentColor := EzTransparentColor;

    RowRect := FDrawInfo.UpdateRect;
    Row := FDrawInfo.TopRow;
    while (Row < FDrawInfo.RecCount) and (RowRect.Top < FDrawInfo.UpdateRect.Bottom) do
    begin
      RowRect.Bottom := RowRect.Top + GetRowHeight(Row);
      DrawRow(Canvas, RowRect, Row);
      RowRect.Top := RowRect.Bottom + FGridlineWidth;
      Inc(Row);
    end;
  end;

begin
  // KV: 6 feb 2006:
  // Check seems unnessesary, however sometimes paint
  // gets called with an empty clipping rectange.
  if not EzIsPrinting and IsRectEmpty(Canvas.ClipRect) then Exit;

  // Do nothing when we don't have a timebar
  if (csDesigning in ComponentState) or not CanUseTimebar then
  begin
    Exit; // Background is already painted by Delphi, no need to do it here
  end;

  with Canvas do
  try
    bErase := True;
    CalcDrawInfo;

    DoDrawBegin(bErase);

    if bErase then
    begin
      // Fill background
      Pen.Mode := pmCopy;
      Brush.Color := FRowColor;
      Brush.Style := bsSolid;
      IntersectRect(R, FDrawInfo.UpdateRect, FDrawInfo.RowsRect);
      FillRect(R);

      // Repaint background
      Brush.Color := Self.Color;
      IntersectRect(R, FDrawInfo.UpdateRect, FDrawInfo.ExcludedRect);
      FillRect(R);
    end;

    for i:=0 to Timebar.ActiveScale.Bands.Count-1 do
      with Timebar.ActiveScale.Bands[i] do
        if GanttShowsDividers then
        begin
          // Paint vertical lines at timebars main intervals.
          Pen.Assign(FGridLinePen);
          DrawScaleLines(Scale, Count, Offset);
        end;

    // Prevent painting on the unused area beneeth the gantt.
    // Both DrawPredecessorLines and DrawProgressLine depend
    // on this.
    with DrawInfo.ExcludedRect do
      ExcludeClipRect(Canvas.Handle, Left, Top, Right, Bottom);

    if gaoHorzLines in FOptions then
      DrawHorizontalLines;

    DrawLineMarkers;

    if Assigned(FOnDrawBackGround) then FOnDrawBackGround(Self);

    if Datalink.Active and not Datalink.Dataset.IsEmpty then
    begin
      if PredecessorLinesShowing and not (poDisplayOnTop in FPredecessorLine.Options) then
      begin
        DrawPredecessorLines(Canvas);
        Datalink.Dataset.CursorPosChanged;
      end;

      if FProgressline.Visible and not (poPaintOnTop in FProgressline.Options) then
        DrawProgressLine(Canvas);

      if Assigned(FOnDrawRows) then FOnDrawRows(Self);

      PrepareCacheRows;
      DrawCacheRows;

      if PredecessorLinesShowing and (poDisplayOnTop in FPredecessorLine.Options) then
      begin
        DrawPredecessorLines(Canvas);
        Datalink.Dataset.CursorPosChanged;
      end;

      if FProgressline.Visible and (poPaintOnTop in FProgressline.Options) then
        DrawProgressLine(Canvas);
    end;

    if HatchOptions.Show then Hatch;

    with FDrawInfo do
      if Assigned(FTimeLine) and
         (FTimeLinePos >= UpdateRect.Left) and (FTimeLinePos <= UpdateRect.Right)
      then
        DrawTimeline(Rect(FTimeLinePos, UpdateRect.Top, FTimeLinePos, UpdateRect.Bottom));

  finally
    if Assigned(FOnDrawEnd) then FOnDrawEnd(Self);
    if Datalink.Active then
      DataLink.ActiveRecord := FDrawInfo.ActiveRecord;
  end;
end;

function TCustomEzGanttChart.PredecessorLinesShowing: Boolean;
begin
  Result := FPredecessorLine.Visible and Assigned(FOnGetPredecessor);
end;

procedure TCustomEzGanttChart.RunTimer(idTimer, Interval: integer);
begin
  if not (csDesigning in ComponentState) and HandleAllocated then
    SetTimer(Handle, idTimer, Interval, nil);
end;

procedure TCustomEzGanttChart.PrepareCacheRow(ARow: Integer; ACacheRow: TScreenRow);
var
  Childs: Integer;
  BarRef: TEzGanttbarReference;

  procedure CalcRowBars(AState: TGanttbarStates);
  var
    BI, i, Dummy: Integer;
    StartDate, StopDate: TDateTime;
    Parent: TEzGanttBar;
    ABar: TScreenbar;

  begin
    with (DataLink.Dataset as TCustomEzDataset) do
{      if State = dsInsert then
        BarRef.BarNode := nil
      else }
      if DirectAccess then
        BarRef.BarNode := ActiveCursor.CurrentNode
      else
        BarRef.BarNode := RecordNode;

    DoOnPrepareCacheRow(BarRef.BarNode, ARow, AState);

    for BI := 0 to FGanttbars.Count-1 do
    begin
      BarRef.Bar := FGanttbars[BI];
      with FDrawInfo do
      begin
        // Bar is located on a inserted record, only bar with CanCreate can be displayed
        if (BarRef.BarNode=nil) then
        begin
          if not (gfCanCreate in BarRef.Bar.Flags) or not GetGanttbarDates(BarRef.Bar, StartDate, StopDate) then
            continue;
        end
        else if not CheckGanttbarVisibility(BarRef.Bar, sfIsHiddenNode in AState) or
                not GetGanttbarDates(BarRef.Bar, StartDate, StopDate) then
          continue;

        ABar := TScreenBar.Create;

        with ABar do
        begin
          Node := BarRef.BarNode;
          Row := ARow;
          BarStates := AState;
          if FSelectedGanttBars.FindItem(Dummy, BarRef) then
            Include(BarStates, sfSelected);
          BarStart := StartDate;
          BarStop := StopDate;
          BarProps.Assign(BarRef.Bar);
          BarProps.FOriginal := BarRef.Bar;

          // Calls OnGetBarprops event
          if SetBarProps(BarRef.RowNode, ABar) then
          begin
            // if this bar is a progressbar then we need to set progressbar's
            // FParentBar property to point to the parent bar. We can locate the
            // parent bar by finding a bar connected to the same recordnode and
            // having the same name
            Parent := BarRef.Bar.ParentBar;
            if Parent <> nil then
            begin
              i := ACacheRow.IndexOfNode(Parent, Node);
              if i <> -1 then
              begin
                BarProps.FParentBar := ACacheRow[i].BarProps;
                if BarProps.FParentBar <> nil then
                  BarProps.FParentBar.FProgressBar := BarProps;
              end;
            end;
            ACacheRow.Add(ABar);
          end
          else
            //
            // Don't show ==> Delete this bar
            //
            ABar.Destroy;
        end;
      end;
    end;
  end;

begin
  with FDrawInfo, DataLink.Dataset as TCustomEzDataset do
  begin
    if (ARow<0) or (ARow>=RecCount) then
    begin
      StartDirectAccess;
      ActiveCursor.RecNo := ScreenTopRecNo+ARow;
    end
    else
      DataLink.ActiveRecord := ARow;

    try
    BarRef.RowNode := RecordNode;

    // Check if we need to draw hidden childs instead of active record bar
    if ((gaoShowHiddens in FOptions) and HasChildren and not Expanded) or
       ((gaoAllwaysShowHiddens in FOptions) and HasChildren)
    then
    begin
      StartDirectAccess([cpShowAll]);
      try
        Childs := ActiveCursor.Childs(ActiveCursor.CurrentNode);
        CalcRowBars([sfIsHiddenParentNode]);
        ActiveCursor.MoveNext;

        //
        // Loop trough children and add bars for child nodes.
        //
        while Childs > 0 do
        begin
          CalcRowBars([sfIsHiddenNode]);
          dec(Childs);
          ActiveCursor.MoveNext;
        end;
      finally
        EndDirectAccess;
      end;
    end
    else
      CalcRowBars([]);
    finally
      if (ARow<0) or (ARow>=RecCount) then
        EndDirectAccess;
    end;
  end;

//  if Overlapping.Mode = omUseTwinRows then
//    ResolveGanttbarConflicts(ARow, ACacheRow);

  ACacheRow.IsPrepared := true;
end;

procedure TCustomEzGanttChart.ResolveGanttbarConflicts(ARow: Integer; CacheRow: TScreenRow);
var
  sb1, sb2: TScreenBar;
  si1, si2: Integer;
  Node, ShadowRow: TRecordNode;

  function GetShadowRow(RowNode: TRecordNode): TRecordNode;
  var
    Key: TNodeKey;
    ScreenRow: TScreenRow;

  begin
    if (RowNode.Next<>nil) and (nfShadowAfter in RowNode.Next.Flags) then
    begin
      Result := Node.Next;
    end
    else
    begin
      Key.Value := 9999;
      Key.Level := Node.Key.Level;
      Result := (DataLink.DataSet as TEzDataset).InsertRecordNode(Key, Node, [nfVirtual, nfShadowAfter], False);
      ScreenRow := TScreenrow.Create(5, 10);
      ScreenRow.IsPrepared := True;
      FDrawInfo.ScreenCache.Insert(ARow+1, ScreenRow);
      Inc(FDrawInfo.RecCount);
      FShadowRowsChanged := True;
    end;
  end;

begin
  ShadowRow := nil;
  Node := RowNode(ARow);
  si1:=0;
  while si1<CacheRow.Count do
  begin
    sb1 := CacheRow[si1];
    if (sb1.BarProps.FParentBar=nil) and not (gfIgnoreOverlap in sb1.BarProps.Flags) then
    begin
      si2:=si1+1;
      while si2<CacheRow.Count do
      begin
        sb2 := CacheRow[si2];
        if (sb2.BarProps.FParentBar=nil) and
           not (gfIgnoreOverlap in sb2.BarProps.Flags) and
          (sb1.BarProps.Z_Index = sb2.BarProps.Z_Index) and
          (sb1.BarStart<sb2.BarStop) and (sb1.BarStop>sb2.BarStart)
        then
        begin
          // Bars overlap; move second ganttbar to next twin row
          if ShadowRow=nil then
            ShadowRow := GetShadowRow(Node);

          FDrawInfo.ScreenRow[ARow+1].Add(sb2);
          FDrawInfo.ScreenRow[ARow].Release(si2);
        end;
        inc(si2);
      end;
    end; // if

    inc(si1);
  end;
end;

//=------------------ Drag and drop functions ----------------=
procedure TCustomEzGanttChart.CMDrag(var Message: TCMDrag);
var
  S: TObject;
  effect: Longint;

begin
  effect := 0;
  with Message, DragRec^ do begin
    S := Source;

    case DragMessage of
      dmDragEnter, dmDragMove, dmDragLeave:
      begin
        DragOver(S, Pos.X, Pos.Y, DragMessage, effect);
        Message.Result := effect;
      end;

      dmDragDrop:
      begin
        DragDrop(S, Pos.X, Pos.Y, DragMessage, effect);
        Message.Result := effect;
      end;

      dmFindTarget: begin
        Result := Integer(ControlAtPos(ScreenToClient(Pos), False));
        if Result = 0 then
          Result := Integer(Self);
      end;
    end;
  end;
end;

procedure TCustomEzGanttChart.DragOver(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: Longint);
var
  Accept: Boolean;
  Relation: TEzPredecessorRelation;
  EzDragObject: TEzGanttDragObject;
  SavedDragOperation: TEzGanttDragOperations;

  function DoScrolling(DoX, DoY: Boolean): Boolean;
  var
    Pos: TPoint;
    hdst: Integer;
    DragRec: TDragRec;

  begin
    Result := False;
    Pos := ScreenToClient(Mouse.CursorPos);

    if DoY then
    begin
      if (Pos.y >= ClientRect.Top) and (Pos.y < ClientRect.Top+10) then
      begin
        if EzDragObject<>nil then
          EzDragObject.HideDragImage(Self);

        Datalink.ActiveRecord := 0;
        Result := Datalink.Dataset.MoveBy(-1)<>0;
      end
      else if (Pos.y < ClientRect.Bottom) and (Pos.y > ClientRect.Bottom-10) then
      begin
        if EzDragObject<>nil then
          EzDragObject.HideDragImage(Self);

        Datalink.ActiveRecord := Datalink.RecordCount-1;
        Result := Datalink.Dataset.MoveBy(1)<>0;
      end;
    end;

    if DoX then
    begin
      hdst:=0;
      if (Pos.X>=ClientRect.Right-15) and (Pos.X<ClientRect.Right) then
        hdst := 4-(ClientRect.Right-Pos.X+4) div 5 {value between 1 and 3}
      else if (Pos.X<=ClientRect.Left+15) and (Pos.X>=ClientRect.Left) then
        hdst := -4+(Pos.X-ClientRect.Left+4) div 5; {value between -1 and -3}

      // Scroll horizontally
      if hdst <> 0 then
      begin
        Result := True;
        if (EzDragObject<>nil) then
          EzDragObject.HideDragImage(Self);
        with Timebar do
          Date := AddTicks(MajorScale, MajorCount, Date, hdst);
      end;
    end;

    // If any scrolling took place,
    // update screen + post a new OnDragOver event
    if Result then
    begin
      Update;
      DragRec.Pos := Point(X, Y);
      DragRec.Target := nil;
      DragRec.Source := Source as TDragObject;
      DragRec.Docking := False;
      Sleep(100); // Do not scroll too fast!
      SendMessage(Handle, CM_DRAG, Longint(DragMessage), Longint(@DragRec));
    end;
end;

var
  PosChanged: Boolean;

begin
  SavedDragOperation := FActiveDragOperation;
  ClearDragEffects;

  if (Source is TEzGanttDragObject) then
    EzDragObject := Source as TEzGanttDragObject else
    EzDragObject := nil;

  if DoScrolling(True, True) then Exit;

  Effect := DROPEFFECT_NONE;

  // Save the trouble, there is no dragobject and no event handler; nothing to do
  if (EzDragObject=nil) and not Assigned(FOnDragOver) then Exit;

  // Gather information about current drag position
  DragPositionInfo(EzDragObject, X, Y, FDragHitInfo, FActiveDragOperation, Relation);

  Accept := (EzDragObject<>nil) and (FActiveDragOperation<>[]);

  if Accept then
    PosChanged := EzDragObject.UpdatePositiondata(Self, X, Y, FDragHitInfo) else
    PosChanged := False;

  // Call event handler
  DoDragOver(Source, TDragState(DragMessage), Point(X, Y), FDragHitInfo, FActiveDragOperation, Relation, Accept);

  // Hide ganttbar drag image when doMoveHorizontal operation is no longer active
  if (EzDragObject<>nil) and
      (not Accept or
      ((doMoveHorizontal in SavedDragOperation) and not (doMoveHorizontal in FActiveDragOperation))
      )
  then
    EzDragObject.HideDragImage(Self);

  if Accept then
  begin
    Effect := Effect or DROPEFFECT_COPY or DROPEFFECT_MOVE;

    if (doLinkTasks in FActiveDragOperation) then
    begin
      // set start and stop points for predecessorline
      with FDragRect do
        if Relation in [prStartFinish, prStartStart, prSameStart] then
          FDragPoints[0] := Point(Left, Top + (Bottom-Top) div 2) else
          FDragPoints[0] := Point(Right, Top + (Bottom-Top) div 2);

      with FDragHitInfo.HitScreenbar.BoundRect do
        if Relation in [prStartFinish, prFinishFinish] then
          FDragPoints[1] := Point(Right, Top + (Bottom-Top) div 2) else
          FDragPoints[1] := Point(Left, Top + (Bottom-Top) div 2);

      FDragPointCount := GetPredecessorLinePoints(PredecessorLine, FDragPoints, Relation, FDragHitInfo.HitScreenbar.BoundRect.Bottom-FDragHitInfo.HitScreenbar.BoundRect.Top);
      SetPredecessorLinePen(Canvas.Pen);
      Windows.Polyline(Canvas.Handle, PPredecessorlinePoints(@FDragPoints)^, FDragPointCount);
      DragCursor := crConnectTask;
    end

    else if (EzDragObject<>nil) and (doMoveHorizontal in FActiveDragOperation) then
    begin
      if (DragMessage=dmDragLeave) then
        EzDragObject.HideDragImage(Self)
      else if PosChanged or IsRectEmpty(EzDragObject.FDragBitmapRect) then
        EzDragObject.UpdateDragImage(Self, X, Y, FDragHitInfo);
      DragCursor := crDefault;
    end

    else if doMoveTasks in FActiveDragOperation then
    begin
      with Canvas do
      begin
        Pen.Style := psSolid;
        Pen.Mode := pmNotXor;

        if hpInsertTask in FDragHitInfo.HitPositions then
        begin
          FDragEffect := hpInsertTask;
          DragCursor := crInsertBefore;

          Pen.Color := clBlue;
          Pen.Width := 2;
          MoveTo(FDragHitInfo.HitRowRect.Left, FDragHitInfo.HitRowRect.Top-1);
          LineTo(FDragHitInfo.HitRowRect.Right, FDragHitInfo.HitRowRect.Top-1);
        end
        else if hpInsertTaskAfter in FDragHitInfo.HitPositions then
        begin
          FDragEffect := hpInsertTaskAfter;
          DragCursor := crInsertAfter;

          Pen.Color := clBlue;
          Pen.Width := 2;
          MoveTo(FDragHitInfo.HitRowRect.Left, FDragHitInfo.HitRowRect.Bottom);
          LineTo(FDragHitInfo.HitRowRect.Right, FDragHitInfo.HitRowRect.Bottom);
        end
        else if hpInsertAsChild in FDragHitInfo.HitPositions then
        begin
          FDragEffect := hpInsertAsChild;
          DragCursor := crInsertAsChild;

          Pen.Width := 1;
          Pen.Color := $E0E0E0;
          Brush.Color := $E0E0E0;
          Rectangle(FDragHitInfo.HitRowRect.Left, FDragHitInfo.HitRowRect.Top, FDragHitInfo.HitRowRect.Right, FDragHitInfo.HitRowRect.Bottom);
        end;
        Pen.Mode := pmCopy;
      end;
    end;
  end;
end;

procedure TCustomEzGanttChart.DragDrop(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: LongInt);

// Handles dmDragDrop message:
// Calculates HitInformation and then links or moves tasks

var
  Accept: Boolean;
  HitInfo: TEzGanttHitInfo;
  Operations: TEzGanttDragOperations;
  Relation: TEzPredecessorRelation;
  EzDragObject: TEzGanttDragObject;
  ARect: TRect;
  SB: TScreenBar;

begin
  if (Source is TEzGanttDragObject) then
    EzDragObject := Source as TEzGanttDragObject else
    EzDragObject := nil;

  ClearDragEffects;
  
  Effect := DROPEFFECT_NONE;

  // Gather information about current drag position
  DragPositionInfo(EzDragObject, X, Y, HitInfo, Operations, Relation);

  Accept := (EzDragObject<>nil) and (Operations <> []);

  // Call event handler
  DoDragDrop(Source, TDragState(DragMessage), Point(X, Y), HitInfo, Operations, Relation, Accept);

  if Accept then
  begin
    // Links tasks
    if (doLinkTasks in Operations) and (Relation <> prNoRelation) then
      LinkTasks(FDragStartNode.Key, FDropTargetBar.Node.Key, Relation)

    // Move a task to another parent
    else if (doMoveTasks in Operations) then
    begin
      MoveTask(FDropTargetNode, HitInfo.HitPositions);
    end;

    if (doMoveHorizontal in Operations) then
    begin
      StartDatasetEdit(EzDragObject.Bar.RowNode, EzDragObject.Bar.BarNode);
      try
        with Datalink.Dataset do
        begin

          FieldByName(EzDragObject.Bar.Bar.StartField).AsDateTime := EzDragObject.BarStart;
          if EzDragObject.Bar.Bar.StopField <> '' then
          begin
            if EzDragObject.Bar.Bar.EndpointType = etFixed then
              FieldByName(EzDragObject.Bar.Bar.StopField).AsDateTime := EzDragObject.BarStop else
              FieldByName(EzDragObject.Bar.Bar.StopField).AsFloat :=
                  EzDragObject.BarStop-EzDragObject.BarStart;
          end;

          SB := FDrawInfo.FScreenCache.LocateScreenBar(EzDragObject.Bar);
          if SB<>nil then
          begin
            SB.BarStart := EzDragObject.BarStart;
            SB.BarStop := EzDragObject.BarStop;
            ARect := RowRect(SB.Row);
            InvalidateRect(Handle, @ARect, False);
          end;
        end;
      finally
        StopDatasetEdit;
      end;
    end;
  end;
end;

procedure TCustomEzGanttChart.DragPositionInfo(
      EzDragObject: TEzGanttDragObject;
      X, Y: Integer;
      var HitInfo: TEzGanttHitInfo;
      var Operations: TEzGanttDragOperations;
      var Relation: TEzPredecessorRelation);

var
  Pos: TPoint;

  // This method gathers drag information about current mouse position.

  // HitInfo:     General HitInfo for position.
  // Operations:  Returns DragOperations that are valid for position
  //                doLinkTasks if two tasks can be linked
  //                doMoveTask if task can move to another parent.
  // Relation:    Returns type of link (like prStartStart),
  //              only if Operations contains doLinkTasks flag.

  procedure TryLinkTasks;
  begin
    with (Datalink.Dataset as TEzDataset) do
      if (doLinkTasks in DragOperations) and
         (gfCanLink in EzDragObject.Bar.Bar.FFlags) and
          // Cannot link task with itself
         (FDropTargetNode <> EzDragObject.Bar.BarNode) and
         (gfIsDropTarget in FDropTargetBar.BarProps.Flags) and
          // Don't allow linking between a task and one of it's parent(s)
         (not ActiveCursor.NodeHasParent(EzDragObject.Bar.BarNode, FDropTargetNode)) and
          // Don't allow linking between a parent and one of it's childs
         (not ActiveCursor.NodeHasParent(FDropTargetNode, EzDragObject.Bar.BarNode))
      then
      begin
        //
        // Make entire bar a drop target since moving is disabled
        // and we are not moving one the same row
        //
        if (not (gfCanMove in EzDragObject.Bar.Bar.FFlags) or not (doMoveTasks in DragOperations)) and
           (FDropTargetNode<>FDragStartNode)
        then
          with HitInfo.HitScreenbar.BoundRect do
            if Pos.X <= Left+((Right-Left) div 2) then
              Include(HitInfo.HitPositions, hpOnBarStart) else
              Include(HitInfo.HitPositions, hpOnBarStop);

        if (hpOnBarStart in HitInfo.HitPositions) then
        begin
          Include(Operations, doLinkTasks);
          if FMouseDownHitInfo.HitWhere.x < FDragRect.left + (FDragRect.Right-FDragRect.left) div 2 then
            Relation := prStartStart else
            Relation := prFinishStart;
        end
        else if (hpOnBarStop in HitInfo.HitPositions) then
        begin
          Include(Operations, doLinkTasks);
          if FMouseDownHitInfo.HitWhere.x < FDragRect.left + (FDragRect.Right-FDragRect.left) div 2 then
            Relation := prStartFinish else
            Relation := prFinishFinish;
        end;
      end;
  end;

  procedure TryMoveLast;
  var
    LastNode: TRecordNode;

  begin
    with (Datalink.Dataset as TEzDataset) do
      if (doMoveTasks in DragOperations) and not ActiveCursor.IsEmpty then
      begin
        // Move task beneeth last record as a root record
        LastNode := ActiveCursor.EndNode.Prev;
        if (FDragStartNode <> LastNode) then
        begin
          HitInfo.HitPositions := [hpNoWhere, hpOnRow, hpInsertTaskAfter];
          FDropTargetNode := LastNode;
        end;
      end;
  end;

{
  procedure TryMoveTask;
  begin
    with (Datalink.Dataset as TEzDataset) do
      if (doMoveTasks in DragOperations) then
      begin
        if (FDropTargetNode<>EzDragObject.Bar.BarNode) then
        begin
          if
            // A parent cannot be dropped on one of its childs
            ActiveCursor.NodeHasParent(FDropTargetNode, EzDragObject.Bar.BarNode) or
            // A child cannot be dragged onto its parent.
            (EzDragObject.Bar.BarNode.Parent = FDropTargetNode)
          then
            Exit;

          Include(Operations, doMoveTasks);
        end

        // Remove a task from its parent by dragging it to the same row outside
        // the ganttbar
        else if Assigned(EzDragObject.Bar.BarNode.Parent) then
        begin
          FDropTargetNode := nil;
          Include(Operations, doMoveTasks);
        end;
      end;
  end;
}

  procedure TryMoveTask;
  begin
    with (Datalink.Dataset as TEzDataset) do
    begin
      if (EzDragObject.Control<>Self) then
        Include(HitInfo.HitPositions, hpInsertAsChild)

      else if (FDropTargetNode<>FDragStartNode) then
        //
        // Dragging to a different row
        //
      begin
        //
        // set doMoveTasks and doMoveHorizontal
        // when hovering over and parent row with hiddens activated,
        //
        if ActiveCursor.HasChildren(FDropTargetNode) and
           (
             (gaoAllwaysShowHiddens in FOptions) or
             ((gaoShowHiddens in FOptions) and not ActiveCursor.IsExpanded(FDropTargetNode))
            )
        then
        begin
          Include(HitInfo.HitPositions, hpInsertAsChild);
          Include(Operations, doMoveHorizontal);
          Exit;
        end;

        if
          // A parent cannot be dropped on one of its childs
          ActiveCursor.NodeHasParent(FDropTargetNode, FDragStartNode) or
          // A child cannot be dragged onto its parent.
          (FDragStartNode.Parent = FDropTargetNode)
        then
          Exit;

        Include(HitInfo.HitPositions, hpInsertAsChild);
      end;
    end;
  end;

  procedure TryReorderTask;
  var
    AdjacentNode: TRecordNode;

  begin
    with (Datalink.Dataset as TEzDataset) do
      if EzDragObject.Control<>Self then
        //
        // When dragging from another control, handle simple drops
        //
      begin
        if hpOnRowTopBorder in HitInfo.HitPositions then
          Include(HitInfo.HitPositions, hpInsertTask)

        else if FDropTargetNode.Child <> nil then
          Include(HitInfo.HitPositions, hpInsertAsChild)

        else
          Include(HitInfo.HitPositions, hpInsertTaskAfter);
      end
      else if (FDropTargetNode <> FDragStartNode) then
      begin
        if ActiveCursor.NodeHasParent(FDropTargetNode, FDragStartNode) then
          // A parent cannot be dropped on one of its childs
          Exit;

        if hpOnRowTopBorder in HitInfo.HitPositions then
        begin
          if FDropTargetNode.Prev = FDragStartNode then
            // Cannot insert a task before the next task because node
            // is already in this position.
            Exit;

          AdjacentNode := ActiveCursor.PrevNode(FDropTargetNode);
        end
        else
        begin
          if FDropTargetNode.Next = FDragStartNode then
            // Cannot insert a task after the previous task because node
            // is already in this position.
            Exit;

          AdjacentNode := ActiveCursor.NextNode(FDropTargetNode);
        end;

        if AdjacentNode = ActiveCursor.TopNode then
          //
          // Hovering over top border of the first record.
          // Insert can only take place before the first record.
          //
          Include(HitInfo.HitPositions, hpInsertTask)

        else if AdjacentNode.Parent = FDropTargetNode.Parent then
          //
          // Nodes share the same parent, Insert can only take place between the
          // two nodes.
          //
        begin
          Include(HitInfo.HitPositions, hpInsertTaskAfter);
          if hpOnRowTopBorder in HitInfo.HitPositions then
          begin
            // Move hit to previous row
            dec(HitInfo.HitRow);
            HitInfo.HitRowRect := RowRect(HitInfo.HitRow);
            FDropTargetNode := AdjacentNode;
          end;
        end
        else
          //
          // Nodes do not share the same parent.
          // 1:  T1
          //        T2
          //
          // 2:     T2
          //     T3
          //
        begin
          if hpOnRowTopBorder in HitInfo.HitPositions then
            Include(HitInfo.HitPositions, hpInsertTask)
          else
          if (hpOnRowBottomBorder in HitInfo.HitPositions) and
             (FDragStartNode.Parent <> FDropTargetNode)
          then
            Include(HitInfo.HitPositions, hpInsertTaskAfter);
        end;

{
        if (FDropTargetNode.Parent <> FDragStartNode.Parent) and
           // A child cannot be dragged onto its parent.
           (FDragStartNode.Parent<>FDropTargetNode)
         then
          Include(HitInfo.HitPositions, hpInsertAsChild);
}
      end;
  end;

begin
  ZeroMemory(@HitInfo, sizeof(HitInfo));
  HitInfo.HitRow := -1;
  Operations := [];
  FDropTargetNode := nil;
  FDropTargetBar := nil;
  Relation := prNoRelation;

  if (DragOperations<>[]) then
  begin
    Pos := ScreenToClient(Point(X,Y));
    GetHitTestInfoAt(Pos.X, Pos.Y, HitInfo);

    if hpNowhere in HitInfo.HitPositions then
      TryMoveLast

    else if (hpOnRow in HitInfo.HitPositions) then
    begin
      FDropTargetNode := RowNode(HitInfo.HitRow);

      if (hpOnBar in HitInfo.HitPositions) then
      begin
        FDropTargetBar := FDrawInfo[ScreenRowToCacheIndex(HitInfo.HitRow)][HitInfo.HitBar];

        // if hovering over a progressbar, shift focus to parent bar
        if FDropTargetBar.BarProps.ParentBar <> nil then
          with HitInfo do begin
            HitBar := FDrawInfo[ScreenRowToCacheIndex(HitRow)].IndexOfBar(FDropTargetBar.BarProps.ParentBar);
            FDropTargetBar := FDrawInfo[ScreenRowToCacheIndex(HitRow)][HitBar];
            HitScreenbar := FDropTargetBar;

            Exclude(HitPositions, hpOnBarStart);
            Exclude(HitPositions, hpOnBarStop);

            if Pos.X <= HitScreenbar.BoundRect.Left+FHitMargin then
              Include(HitPositions, hpOnBarStart);

            if Pos.X >= HitScreenbar.BoundRect.Right-FHitMargin then
              Include(HitPositions, hpOnBarStop);
          end;

        // Test for possible linking of two tasks,
        // only if we are dragging ourselves.
        if Assigned(FDragStartNode) then
          TryLinkTasks;
      end;

      //
      // If we are not on a link task position, then try other drag drop operations
      //
      if not (doLinkTasks in Operations) then
      begin
        //
        // When dragging on the same row, move bar horizontally
        //
        if (doMoveHorizontal in DragOperations) and (gfCanMove in EzDragObject.Bar.Bar.FFlags) and (FDropTargetNode=FDragStartNode) then
          Include(Operations, doMoveHorizontal)

        else if (doMoveTasks in DragOperations) and (EzDragObject<>nil) and (gfCanDragDrop in EzDragObject.Bar.Bar.FFlags) then
        begin
          if (HitInfo.HitPositions * [hpOnRowTopBorder, hpOnRowBottomBorder] <> []) then
            TryReorderTask;

          if HitInfo.HitPositions * [hpInsertAsChild, hpInsertTask, hpInsertTaskAfter]=[] then
            // Test moving a task to another parent
            TryMoveTask;

          if HitInfo.HitPositions * [hpInsertAsChild, hpInsertTask, hpInsertTaskAfter]<>[] then
            Include(Operations, doMoveTasks);
        end;
      end;
    end;
  end;
end;

procedure TCustomEzGanttChart.DoDragOver(
      Source: TObject; State: TDragState;
      Pt: TPoint; HitInfo: TEzGanttHitInfo;
      var Operations: TEzGanttDragOperations; var Relation: TEzPredecessorRelation;
      var Accept: Boolean);

  // Handle OnDragOver event

begin
  if Assigned(FOnDragOver) then
    FOnDragOver(Self, Source, State, Pt, HitInfo, Operations, Relation, Accept);
end;

procedure TCustomEzGanttChart.DoDragDrop(
      Source: TObject; State: TDragState;
      Pt: TPoint; HitInfo: TEzGanttHitInfo;
      var Operations: TEzGanttDragOperations; var Relation: TEzPredecessorRelation;
      var Accept: Boolean);

  // Handle OnDragDrop event

begin
  if Assigned(FOnDragDrop) then
    FOnDragDrop(Self, Source, State, Pt, HitInfo, Operations, Relation, Accept);
end;

procedure TCustomEzGanttChart.DoEndDrag(Target: TObject; X, Y: Integer);
begin
  FDragType := bdNone;
  inherited DoEndDrag(Target, X, Y);
  StopDragging;
end;

procedure TCustomEzGanttChart.DoStartDrag(var DragObject: TDragObject);
begin
  inherited DoStartDrag(DragObject);

  if DragObject=nil then
  begin
    FDragObject := TEzGanttDragObject.Create(Self);
    DragObject := FDragObject;
    InitializeDragObject(FDragObject, False);
  end;

//  KV: 15 may 2006 Do not post changes
//  if Datalink.Dataset.State in [dsEdit, dsInsert] then Datalink.Dataset.Post;
end;
//=------------------ End of Drag and drop functions ----------------=

procedure TCustomEzGanttChart.DoOnOverGanttChart(HitInfo: TEzGanttHitInfo);
begin
  if Assigned(FOnOverGanttChart) then FOnOverGanttChart(Self, HitInfo);
end;

procedure TCustomEzGanttChart.DrawRow(ACanvas: TCanvas;  ARect: TRect;  ARow: Integer);
const
  DTFlags = {DT_VCENTER or }DT_NOPREFIX;
  PositionFlags: array[TGanttbarLabelPosition] of UINT = (DT_RIGHT, DT_LEFT, DT_CENTER, DT_LEFT, DT_RIGHT);
  MultiLineFlags: array[Boolean] of UINT = (DT_SINGLELINE, DT_WORDBREAK);

var
  Bar: TScreenbar;
  DrawState: TGanttbarStates;
  OverlapRects: TList;
  BarIndex: integer;
  InterSect: TRect;

  procedure CalcOverlapRects;

  // Calculates overlapping rectangle between bar at Index and
  // bars located on the left

  var
    i1, i2: integer;
    PR: PRect;
    s1, s2: TScreenbar;

  begin
    for i1:=FDrawInfo.PaintRow.Count-1 downto 0 do
    begin
      s1 := FDrawInfo.PaintRow.Items[i1];
      if (s1.BarProps.FParentBar=nil) and not (gfIgnoreOverlap in s1.BarProps.Flags) then
      begin
        i2:=i1-1;
        while i2>=0 do
        begin
          s2 := FDrawInfo.PaintRow.Items[i2];

          // Stop checking when s2
          if (s1.BarStart>s2.BarStop) and
             // Both bars are selected
             (
               (sfSelected in (s1.BarStates*s2.BarStates))
               or
               not (sfSelected in (s2.BarStates))
             )
           then
            break;

          if (s2.BarProps.FParentBar=nil) and
             not (gfIgnoreOverlap in s2.BarProps.Flags) and
            (s1.BarProps.Z_Index = s2.BarProps.Z_Index) and
            (s1.BarStart<s2.BarStop) and (s1.BarStop>s2.BarStart)
          then
          begin
            New(PR);
            PR^.Top := ARect.Top;
            PR^.Bottom := ARect.Bottom;
            PR^.Left := Timebar.DateTime2X(max(s1.BarStart, s2.BarStart));
            PR^.Right := Timebar.DateTime2X(min(s1.BarStop, s2.BarStop));
            OverlapRects.Add(PR);
          end;
          dec(i2);
        end;
      end;
    end;
  end;

  procedure HatchOverlapRects;
  var
    i: integer;

  begin
    OverlapRects := TList.Create;
    try
      CalcOverlapRects;

      with Overlapping do
      begin
        ACanvas.Brush := HatchFill;
        ACanvas.Pen.Style := psClear;
        for i:=0 to OverlapRects.Count-1 do
          with PRect(OverlapRects[i])^ do
            ACanvas.Rectangle(Left, Top, Right, Bottom);
      end;
    finally
      for i:=0 to OverlapRects.Count-1 do
        Dispose(OverlapRects[i]);

      OverlapRects.Free;
    end;
  end;

  procedure PrepareBar;
  begin
    with Bar do
    begin
      // ActualSize hold the rectangle of the ganttbar
      SetRect(
          ActualSize,
          Timebar.DateTime2X(BarStart),
          ARect.Top,
          Timebar.DateTime2X(BarStop),
          ARect.Bottom
        );

      // Calculate outer bounds of ganttbar
      DrawState := Bar.BarStates;

      // Remove selected flag when a bar is selected but the ganttchart does
      // not have focus.
      if not (gaoAllwaysShowSelection in Options) and not Focused then
        Exclude(DrawState, sfSelected);

      PaintGanttbar(TCanvas(nil), EndPoints, DrawInfo.UpdateRect, ActualSize, BoundRect, ClickRect, BarRect, True, BarProps, DrawState);
      InterSectRect(InterSect, DrawInfo.UpdateRect, BoundRect);
      BarShowing := not IsRectEmpty(InterSect);
    end;
  end;

  procedure PrepareTextLabel;
  var
    p: Integer;

  begin
    with Bar do
    begin
      TextShowing := False;
      // Prepare textlabel
      if Length(BarProps.TextLabel.Text)>0 then
      begin
        with BarProps.TextLabel do
        begin
          SetRect(TextRect, BarRect.Left, ARect.Top, BarRect.Right, ARect.Bottom);

          // Calculate text rectangle
          ACanvas.Font := BarProps.TextLabel.Font; // Use full reference for clarity
          DrawTextW(ACanvas.Handle, PWideChar(BarProps.TextLabel.Text), -1, TextRect, MultiLineFlags[MultiLine] or DTFlags or DT_CALCRECT);

          case Position of
            lpgLeft:
              OffsetRect(TextRect, BarRect.Left-ExtraOffset-TextRect.Right, 0);

            lpgBarLeft:
              OffsetRect(TextRect, FExtraOffset, 0);

            lpgBarRight:
              OffsetRect(TextRect, BarRect.Right-TextRect.Right-ExtraOffset, 0);

            lpgRight:
              OffsetRect(TextRect, BarRect.Right-TextRect.Left+ExtraOffset, 0);

            lpgCenter:
            begin
              p := (BarRect.Left+(BarRect.Right-BarRect.Left) div 2) -
                   (TextRect.Left+(TextRect.Right-TextRect.Left) div 2);
              OffsetRect(TextRect, p + FExtraOffset, 0);
            end;
          end;

          case Alignment of
            laRowTop:
              OffsetRect(TextRect, 0, ARect.Top-TextRect.Top+ExtraVOffset);

            laRowCenter:
            begin
              p := (ARect.Top+(ARect.Bottom-ARect.Top) div 2) -
                   (TextRect.Top+(TextRect.Bottom-TextRect.Top) div 2);
              OffsetRect(TextRect, 0, p + ExtraVOffset);
            end;

            laRowBottom:
              OffsetRect(TextRect, 0, ARect.Bottom-TextRect.Bottom+ExtraVOffset);

            laBarTop:
              OffsetRect(TextRect, 0, BarRect.Top-TextRect.Top+ExtraVOffset);

            laBarCenter:
            begin
              p := (BarRect.Top+(BarRect.Bottom-BarRect.Top) div 2) -
                   (TextRect.Top+(TextRect.Bottom-TextRect.Top) div 2);

              OffsetRect(TextRect, 0, p + ExtraVOffset);
            end;

            laBarBottom:
              OffsetRect(TextRect, 0, BarRect.Bottom-TextRect.Bottom+ExtraVOffset);

            laOnTopOfBar:
              OffsetRect(TextRect, 0, BarRect.Top-TextRect.Bottom+ExtraVOffset);

            laBelowBar:
              OffsetRect(TextRect, 0, BarRect.Bottom-TextRect.Top+ExtraVOffset);
          end;

          // Clip text into ganttbar
          if Position in [lpgBarLeft, lpgBarRight, lpgCenter] then
            InterSectRect(TextRect, BarRect, TextRect);

          InterSectRect(InterSect, DrawInfo.UpdateRect, TextRect);
          TextShowing := not IsRectEmpty(InterSect);
        end;
      end;
    end; // with Bar do
  end;

begin
  with FDrawInfo[ScreenRowToCacheIndex(ARow)] do
  begin
    FDrawInfo.PaintRow.Clear;

    for BarIndex:=0 to Count-1 do
    begin
      Bar := Items[BarIndex];

      if (FDragType in [bdDragDrop, bdNone, bdLineMarker, bdSelection]) or
         ((FDragStartBar<>nil) and (FDragStartBar<>Bar) and (FDragStartBar.BarProps<>Bar.BarProps.ParentBar))
      then
      begin
        PrepareBar;
        PrepareTextLabel;
        if Bar.BarShowing or Bar.TextShowing then
          FDrawInfo.PaintRow.Add(Bar);
      end;
    end; // for BarIndex:=0 to Count-1 do

    if Overlapping.Mode=omHatchBackground then
        HatchOverlapRects;

    for BarIndex:=0 to FDrawInfo.PaintRow.Count-1 do
    begin
      Bar := FDrawInfo.PaintRow.Items[BarIndex];
      with Bar do
      begin
        if BarShowing then
        begin
          // Calculate outer bounds of ganttbar
          DrawState := Bar.BarStates;

          // Remove selected flag when a bar is selected but the ganttchart does
          // not have focus.
          if not (gaoAllwaysShowSelection in Options) and not Focused then
            Exclude(DrawState, sfSelected);

          PaintGanttbar(FDrawBitmap, EndPoints, DrawInfo.UpdateRect, ActualSize, BoundRect, ClickRect, BarRect, False, BarProps, DrawState);
          InterSectRect(InterSect, DrawInfo.UpdateRect, BoundRect);
          if not IsRectEmpty(InterSect) then
            TransparentDraw(ACanvas, FDrawBitmap, InterSect);
        end;

        if TextShowing then
        begin
          ACanvas.Font := BarProps.TextLabel.Font;
          EzRenderText(ACanvas, TextRect, BarProps.TextLabel.Text, tsNormal,
            {PositionFlags[BarProps.TextLabel.Position] or }
            MultiLineFlags[BarProps.TextLabel.MultiLine] or DTFlags);
        end;
      end;
    end;

    // Now paint bar being dragged
    // This will only happen when the mouse has already been released
    // and the application is currently handling the OnEndBarDrag event
    if FPaintDragbar and (FDragStartBar<>nil) then
      // FDragBitmap and FDragBitmapRect are set in MouseMove event
      TransparentDraw(Canvas, FDragBitmap, FDragObject.FDragBitmapRect, True);

  end;
end;

procedure TCustomEzGanttChart.DrawPredecessorLines(ACanvas: TCanvas);
var
  LineInfo: TVisiblePredecessorLine;
  SuccNode, PredNode, VisibleSuccNode, VisiblePredNode: TRecordNode;
  RecordPoint, PredPoint: TPoint;
  ARect, Extendedrect: TRect;
  RecNo: Integer;
  tmp: TRect;

  function GetVisibleNode(var Node: TRecordNode): Boolean;
    //
    // Returns the first visible parent or the node itself when it is visible.
    //
  begin
    Result := False;
    if Node = nil then Exit;

    with (Datalink.Dataset as TCustomEzDataset).ActiveCursor do
    begin
      // Test if node is not hidden in this cursor
      while not IsNodeVisible(Node) do
        Node := GetParent(Node);
      Result := True;
    end;
  end;

  function GetPoint(Fieldname: string; Node, VisibleNode: TRecordNode) : TPoint;
  var
    SafeRecNo: integer;

  begin
    with FDrawInfo, (Datalink.Dataset as TCustomEzDataset) do
    begin
      // y-position is allways determined by visible parent node
      ActiveCursor.CurrentNode := VisibleNode;

      SafeRecNo := ActiveCursor.RecNo;
      // adjust record number if point lies beneeth inserted record.
      // (use FSavedActiveRecord because we are in direct access mode)

      if (State = dsInsert) and (SafeRecNo >= ScreenTopRecNo+ActiveRecord) then
        inc(SafeRecNo);

      if SafeRecNo < ScreenTopRecNo then
        Result.y := -EndPointSize else

      if SafeRecNo >= ScreenTopRecNo+RecCount then
        Result.y := ClientHeight + EndPointSize
      else
      begin
        ARect := RowRect(SafeRecNo - ScreenTopRecNo);
        Result.y := ARect.Top + (ARect.Bottom - ARect.Top) div 2;
      end;

      // x position is determined by either visible parent of node itself
      if (Node<>VisibleNode) and ((gaoAllwaysShowHiddens in FOptions) or (gaoShowHiddens in FOptions)) then
        ActiveCursor.CurrentNode := Node;

      Result.x := Timebar.DateTime2X(Datalink.Dataset.FieldByName(Fieldname).AsDateTime);
    end;
  end;

  procedure UpdateSelectedPredecessorline;
  begin
    if (FSelectedPredecessorLine<>nil) and
       (CompareNodeKeys(FSelectedPredecessorLine.Predecessor.PredecessorNode, LineInfo.Predecessor.PredecessorNode) = 0) and
       (CompareNodeKeys(FSelectedPredecessorLine.Predecessor.SuccessorNode, LineInfo.Predecessor.SuccessorNode) = 0)
    then
    begin
      FSelectedPredecessorLine.Points := LineInfo.Points;
      FSelectedPredecessorLine.PointCount := LineInfo.PointCount;
      HighlightPredecessorline;
    end;
  end;

begin
  FVisiblePredecessorLines.Clear;
  LineInfo := TVisiblePredecessorLine.Create;
  ExtendedRect := FDrawInfo.UpdateRect;

  with PredecessorLine do
    InflateRect(ExtendedRect, max(StartLength, EndLength), max(StartLength, EndLength));

  (Datalink.Dataset as TCustomEzDataset).StartDirectAccess([cpShowAll]);
  try
    DoGetPredecessor(LineInfo.Predecessor);
    with FDrawInfo do
    begin
      while not VarIsNull(LineInfo.Predecessor.SuccessorNode.Value) do // Null is the stop condition
      begin

        with Datalink.Dataset as TCustomEzDataset do
        begin
          SuccNode := ActiveCursor.FindNode(LineInfo.Predecessor.SuccessorNode);
          if SuccNode = ActiveCursor.EndNode then
            SuccNode := nil;

          PredNode := ActiveCursor.FindNode(LineInfo.Predecessor.PredecessorNode);
          if PredNode = ActiveCursor.EndNode then
            PredNode := nil;
        end;

        if (SuccNode = PredNode) then
          //
          // Line is invalid
          //
          if (poSkipInvalidLines in PredecessorLine.Options) then
          begin
            // get next predecessor
            LineInfo.Predecessor.SuccessorNode.Value := Null;
            DoGetPredecessor(LineInfo.Predecessor);
            continue;
          end else
            EzGanttError(SInvalidPredecessorLine, Self)
        else
        begin
          VisibleSuccNode := SuccNode;
          VisiblePredNode := PredNode;
          GetVisibleNode(VisibleSuccNode);
          GetVisibleNode(VisiblePredNode);

          if(
              (VisibleSuccNode<>SuccNode) and
              (VisiblePredNode<>PredNode) and
              (VisibleSuccNode=VisiblePredNode) and
              not (gaoAllwaysShowHiddens in FOptions) and
              not (gaoShowHiddens in FOptions)
            )
            or not DoGetPredecessorLineVisibility(LineInfo.Predecessor, PredNode, VisiblePredNode, SuccNode, VisibleSuccNode)
          then
             //
             // both predecessor and successor have the same parent and this
             // parent is collapsed. Such a line is not visible and will not be drawn.
             //
          begin
            // get next predecessor
            LineInfo.Predecessor.SuccessorNode.Value := Null;
            DoGetPredecessor(LineInfo.Predecessor);
            continue;
          end;

          if not (poShortenInvalidLines in PredecessorLine.Options) then
            if SuccNode = nil then
              EzGanttError(SSuccessorNotFound, Self)
            else if PredNode = nil then
              EzGanttError(SPredecessorNotFound, Self);

          RecNo := 0;
          if SuccNode <> nil then
          begin
            RecNo := SuccNode.RecNo;
            if LineInfo.Predecessor.Relation in [prFinishStart, prStartStart, prSameStart] then
              RecordPoint := GetPoint(PredecessorLine.StartField, SuccNode, VisibleSuccNode) else
              RecordPoint := GetPoint(PredecessorLine.EndField, SuccNode, VisibleSuccNode);

            if PredNode = nil then
            begin
              PredPoint := RecordPoint;
              inc(PredPoint.x, 20);
            end;
          end;

          if PredNode <> nil then
          begin
            if LineInfo.Predecessor.Relation in [prStartFinish, prStartStart, prSameStart] then
              PredPoint := GetPoint(PredecessorLine.StartField, PredNode, VisiblePredNode) else
              PredPoint := GetPoint(PredecessorLine.EndField, PredNode, VisiblePredNode);

            if SuccNode = nil then
            begin
              RecNo := PredNode.RecNo;
              RecordPoint := PredPoint;
              dec(RecordPoint.X, 20);
            end;
          end;

          if (
               (SuccNode <> PredNode)
               or
               ((SuccNode = PredNode) and ((gaoAllwaysShowHiddens in FOptions) or (gaoShowHiddens in FOptions)))
             )
          then
          begin
            // Test if line is visible in extended rectangle and if so,
            // the line needs to be painted
            if (
                 ((RecordPoint.x < ExtendedRect.Right) and (PredPoint.x >= ExtendedRect.left) and (RecordPoint.y < ExtendedRect.Bottom) and (PredPoint.y > ExtendedRect.Top))
                 or
                 ((PredPoint.x < ExtendedRect.Right) and (RecordPoint.x >= ExtendedRect.Left) and (PredPoint.y < ExtendedRect.Bottom) and (RecordPoint.y > ExtendedRect.Top))
               )
            then
            begin
              PaintPredecessorLine(ACanvas, EndPoints, PredecessorLine, LineInfo, PredPoint, RecordPoint, GetRowHeight(TopRow-TopRecNo+RecNo));
              FVisiblePredecessorLines.Add(LineInfo);
              UpdateSelectedPredecessorline;
              LineInfo := TVisiblePredecessorLine.Create;
            end
            else
              // Test if line is vivible in the client rectangle and if so,
              // add the line to the visible lines list
              tmp := ClientRect;
              if (
                 ((RecordPoint.x < tmp.Right) and (PredPoint.x >= tmp.left) and (RecordPoint.y < tmp.Bottom) and (PredPoint.y > tmp.Top))
                 or
                 ((PredPoint.x < tmp.Right) and (RecordPoint.x >= tmp.Left) and (PredPoint.y < tmp.Bottom) and (RecordPoint.y > tmp.Top))
               )
              then

              //
              // Line is not painted, however we need to store the points so we
              // can detect mouse hits.
              //
              begin
                PreparePredecessorLine(EndPoints, PredecessorLine, LineInfo, PredPoint, RecordPoint, GetRowHeight(TopRow-TopRecNo+RecNo));
                FVisiblePredecessorLines.Add(LineInfo);
                UpdateSelectedPredecessorline;
                LineInfo := TVisiblePredecessorLine.Create;
              end;
          end;
        end;

        // get next predecessor
        LineInfo.Predecessor.SuccessorNode.Value := Null;
        DoGetPredecessor(LineInfo.Predecessor);
      end;
    end;
  finally
    (Datalink.Dataset as TCustomEzDataset).EndDirectAccess;
    LineInfo.Destroy;
  end;
end;

procedure TCustomEzGanttChart.DrawProgressLine(ACanvas: TCanvas);
type
  PPoints = ^TPoints;
  TPoints = array[0..1000] of TPoint;

var
  RowRect: TRect;
  F: TField;
  X: Integer;
  Points: array of TPoint;
  PointCount: Integer;
  ProgressDatePos: Integer;
  Row: Integer;
  H: Integer;
  ADate: TDateTime;

  procedure GetProgressRowDate;
  var
    i: Integer;
    Bar: TScreenbar;

  begin
    if FProgressline.ProgresslineField<>'' then
      //
      // Read data directly from the dataset
      //
    begin
      DataLink.ActiveRecord := Row;
      ADate := F.AsDateTime;
    end
    else
      //
      // Read data from the screencache
      //
    begin
      with FDrawInfo.FScreenCache[ScreenRowToCacheIndex(Row)] do
      begin
        ADate := 0;
        i:=0;
        while (ADate=0) and (i<Count) do
        begin
          Bar :=Items[i];
          if not (sfIsHiddenNode in Bar.BarStates) and (Bar.BarProps.Original=FProgressline.BarReference) then
          begin
            // Check if bar hosts a progressbar
            if Bar.BarProps.ProgressBarPtr<>nil then
            begin
              Bar := Items[i+1];
              ADate := Bar.BarStop;
            end

            else if FProgressline.WhenProgressIsZero=zpAttachToStart then
              ADate := Bar.BarStart

            else
              i:=Count; // Stop
              
          end else
            inc(i);
        end;
      end;
    end;
  end;

  procedure AddLinePoint(P: TPoint);
  begin
    if PointCount=0 then
    begin
      Points[0] := P;
      inc(PointCount);
    end

    else if (PointCount>1) and (Points[PointCount-1].X=P.X) and (Points[PointCount-1].X=Points[PointCount-2].X) then
      Points[PointCount-1].Y := P.Y

    else
    begin
      if PointCount=High(Points) then
        SetLength(Points, PointCount+10);
      Points[PointCount] := P;
      inc(PointCount);
    end;
  end;

begin
  // Attach line to the datetime value stored in field
  if FProgressline.ProgresslineField<>'' then
  begin
    F := Datalink.Dataset.FindField(FProgressline.ProgresslineField);
    if F=nil then Exit;
  end;


  with FDrawInfo {, Datalink.Dataset as TCustomEzDataset} do
  begin
    PointCount := 0;
    SetLength(Points, 10);

    ProgressDatePos := Timebar.DateTime2X(FProgressline.ProgressLineDate);

    RowRect := UpdateRect;
    Row := TopRow;
    while (Row < RecCount) and (RowRect.Top < UpdateRect.Bottom) do
    begin
      RowRect.Bottom := RowRect.Top + GetRowHeight(Row);

      // Get date value for current row (value returned in var ADate)
      GetProgressRowDate;
      if ADate=0 then
        //
        // There is no progress date for this row,
        // Paint a straight line at ProgressLineDate position
        //
      begin
        AddLinePoint(Point(ProgressDatePos, RowRect.Top));
        AddLinePoint(Point(ProgressDatePos,RowRect.Bottom));
      end else
      begin
        H:=RowRect.Bottom-RowRect.Top;
        AddLinePoint(Point(ProgressDatePos, RowRect.Top));
        AddLinePoint(Point(ProgressDatePos,
          RowRect.Top+FProgressline.VOffset+(H-FProgressline.GapSize) div 2));

        X := Timebar.DateTime2X(ADate);
        AddLinePoint(Point(X, RowRect.Top + (RowRect.Bottom-RowRect.Top) div 2 + FProgressline.VOffset));
        AddLinePoint(Point(ProgressDatePos,
          RowRect.Bottom+FProgressline.VOffset-(H-FProgressline.GapSize) div 2));
        AddLinePoint(Point(ProgressDatePos,RowRect.Bottom));
      end;

      RowRect.Top := RowRect.Bottom + FGridlineWidth;
      Inc(Row);
    end;
  end;

  ACanvas.Pen := FProgressline.Pen;
  Windows.Polyline(ACanvas.Handle, PPoints(Points)^, PointCount);
end;

procedure TCustomEzGanttChart.DrawSelectedLineMarkers;
var
  i: integer;
  Marker: TEzGanttLineMarker;

begin
  with Canvas, FLineMarkers do
    // Remove all lines from screen
    for i:=0 to Selected.Count-1 do
    begin
      Marker := TEzGanttLineMarker(Selected[i]);
      Pen := Marker.Pen;
      Pen.Color := HighLightColor;
      Pen.Mode := pmNotXor;
      MoveTo(Marker.FDragPos, 0);
      LineTo(Marker.FDragPos, RowRect(VisibleRowCount-1).Bottom + FGridLineWidth);
    end;
end;

procedure TCustomEzGanttChart.DrawTimeline(ARect: TRect);
begin
  with Canvas do
  begin
    Pen := FTimeline.Pen;
    Pen.Mode := pmNotXor;
    MoveTo(ARect.Left, ARect.Top);
    LineTo(ARect.Left, ARect.Bottom);
  end;
end;

procedure TCustomEzGanttChart.TimebarLayoutChanged;
begin
  if Timebar.ColSizing or (FUpdateCount > 0) then Exit;
  try
    BeginUpdate;
    Enabled := CanUseTimebar;
    ScrollbarOptions.ResetExtends;
    UpdateHorizontalScrollbar;
    Invalidate;
    FTimeLinePos := -1;
  finally
    EndUpdate;
  end;
end;

procedure TCustomEzGanttChart.TimebarDateChanged(OldDate: TDateTime);
var
  dst: integer;

  procedure OffsetDragLines(dx: integer);
  begin
    DrawSelectedLineMarkers;
    ScrollClientRect(dst);
    UpdateWindow(Handle);
    DrawSelectedLineMarkers;
  end;

  procedure InflateDragRect(distance: integer);
  var
    cp: TPoint;
    rtmp: TRect;

  begin
    // Restore original screen
    with FDragObject.FDragBitmapRect do
      Canvas.CopyRect(
            FDragObject.FDragBitmapRect,
            FDragOriginal.Canvas,
            Rect(0,0, Right-Left, Bottom-Top)
      );

    ScrollClientRect(distance);
    UpdateWindow(Handle);

    dec(FDragRect.Left, -distance);
    dec(FMouseDownHitInfo.HitWhere.x , -distance);
    cp := ScreenToClient(Point(Mouse.Cursorpos.x, Mouse.Cursorpos.y));

    FDragRect.Right := max(FDragRect.Left+1, cp.X+FMouseBarOffset.x);

    if GetRectW(FDragRect)>ClipedDragThreshold then
      // Large ganttbar ==> only prepare visible part of bar
    begin
      PaintGanttbar(FDragBitmap, EndPoints, ClientRect, FDragRect, FDragObject.FDragBitmapRect, FDragObject.FDragClickRect, rtmp, True, FDragStartBar.BarProps, [sfSelected]);
      IntersectRect(FDragObject.FDragBitmapRect, FDragObject.FDragBitmapRect, ClientRect);
    end
    else
      // Small ganttbar ==> prepare complete bitmap
    begin
      // Calculate size of bounding rectange
      PaintGanttbar(TCanvas(nil), EndPoints, rtmp, FDragRect, FDragObject.FDragBitmapRect, FDragObject.FDragClickRect, rtmp, True, FDragStartBar.BarProps, [sfSelected]);
      // paint ganttbar onto FDragBitmap's canvas
      // using bounding rectangle FDragBitmapRect as cliprect.
      PaintGanttbar(FDragBitmap, EndPoints, FDragObject.FDragBitmapRect, FDragRect, FDragObject.FDragBitmapRect, FDragObject.FDragClickRect, rtmp, False, FDragStartBar.BarProps, [sfSelected]);
    end;

    with FDragObject.FDragBitmapRect do
    begin
      FDragOriginal.Width := max(FDragOriginal.Width, Right-Left);
      FDragOriginal.Height := max(FDragOriginal.Height, Bottom-Top);
      FDragOriginal.Canvas.CopyRect(
            Rect(0,0, Right-Left, Bottom-Top),
            Canvas,
            FDragObject.FDragBitmapRect
      );
      TransparentDraw(Canvas, FDragBitmap, FDragObject.FDragBitmapRect, True);
    end;
  end;


  procedure UpdateSelectionRect(dx: integer);
  begin
    SetSelectionPen(Canvas);
    // Remove selection rectangle from screen
    Canvas.DrawFocusRect(FSelectionRectangle);
    ScrollClientRect(dx);
    inc(FMouseDownHitInfo.HitWhere.x, dx);
    UpdateWindow(Handle);
    Canvas.DrawFocusRect(FSelectionRectangle);
  end;

begin
  if FUpdateCount > 0 then Exit;
  try
    BeginUpdate;
    UpdateHorizontalScrollbar;

    with Timebar do
      dst := MajorWidth * TickCount(MajorScale, MajorCount, Date, OldDate);

    if FDragType <> bdNone then
      dec(FMouseDownHitInfo.HitWhere.x , -dst);

    case FDragType of
      bdNone: ScrollClientRect(dst);
      bdBarDate, bdNewbar, bdProgressBar, bdAddProgressBar: InflateDragRect(dst);
      bdDragDrop:
      begin
        ClearDragEffects;
        ScrollClientRect(dst);
        OffsetRect(FDragRect, dst, 0);
      end;
      bdLineMarker: OffsetDragLines(dst);
      bdSelection: UpdateSelectionRect(dst);
    end;

  finally
    EndUpdate;
  end;
end;

function TCustomEzGanttChart.UpdateBarDates(Dragbar: TScreenbar; ARect: TRect) : Boolean;
var
  SavedStart, SavedStop, dtStart, dtStop, Duration: TDateTime;
  SnapState: TGanttbarStates;

begin
  with Dragbar do
  begin
    SavedStart := BarStart;
    SavedStop := BarStop;
    SnapState := [];

    case FDragType of
      bdBarHorizontal:
      begin
        // We want bars to stay the same length so use the actual duration
        // when updating the record.
        Duration := BarStop - BarStart;
        // Calculate new start position
        BarStart := TimeBar.X2DateTime(ARect.Left);
        BarStop := BarStart + Duration;
        if (gaoDoSnap in FOptions) and (FSnapCount <> 0) then
          SnapState := SnapInterval(BarStart, BarStop);
      end;

      bdProgressBar, bdAddProgressBar:
      begin
        if (sfIsHiddenNode in BarStates) then
          with Datalink.Dataset as TCustomEzDataset do
          try
            StartDirectAccess([cpShowAll]);
            ActiveCursor.CurrentNode := Node;
            GetGanttbarDates(BarProps.ParentBar, dtStart, dtStop);
          finally
            EndDirectAccess;
          end
        else
          GetGanttbarDates(BarProps.ParentBar, dtStart, dtStop);

        BarStop := Timebar.X2DateTime(ARect.Right);

        if (gaoDoSnap in FOptions) and (FSnapCount <> 0) then
        begin
          BarStop := RoundDate(FSnapScale, FSnapCount, 0, BarStop);
          Include(SnapState, sfRightSnap);
        end;

        if BarStop > dtStop then
          BarStop := dtStop;
      end;

      bdBarDate, bdNewbar:
      begin
        BarStop := Timebar.X2DateTime(ARect.Right);

        if (gaoDoSnap in FOptions) and (FSnapCount <> 0) then
        begin
          BarStop := RoundDate(FSnapScale, FSnapCount, 0, BarStop);
          Include(SnapState, sfRightSnap);
        end
      end;

      bdBarStartDate:
      begin
        BarStart := Timebar.X2DateTime(ARect.Left);

        if (gaoDoSnap in FOptions) and (FSnapCount <> 0) then
        begin
          BarStart := RoundDate(FSnapScale, FSnapCount, 0, BarStart);
          Include(SnapState, sfLeftSnap);
        end
      end;

      bdMilestone:
      begin
        BarStart := TimeBar.X2DateTime(ARect.Right);

        if (gaoDoSnap in FOptions) and (FSnapCount <> 0) then
        begin
          BarStart := RoundDate(FSnapScale, FSnapCount, 0, BarStart);
          Include(SnapState, sfLeftSnap);
        end;
        BarStop := BarStart;
      end;
    end;

    if BarStop < BarStart then
      BarStop := BarStart;

    Result := (SavedStart <> BarStart) or (SavedStop <> BarStop);

    if Result then
      BarStates := BarStates - [sfLeftSnap, sfRightSnap] + SnapState;
  end;

end;

procedure TCustomEzGanttChart.UpdateRowCount;
var
  NewCount: Integer;

  procedure InvalidateRows;
  var
    R1, R2: TRect;
  begin
    if NewCount>FDataLink.BufferCount then
    begin
      R1 := RowRect(FDataLink.BufferCount);
      R2 := RowRect(NewCount);
      R1.Bottom := R2.Bottom;
      InvalidateRect(Handle, @R1, False);
    end;
  end;

begin
  if FDataLink.Active and not FDataLink.Dataset.IsEmpty then
  begin
    if gaoCacheAllRows in Options then
    begin
      NewCount := CalcVisibleRowCount;
      InvalidateRows;
      FVisibleRowCount := NewCount;
      if NewCount<>FDataLink.BufferCount then
        FDataLink.BufferCount := NewCount;
    end
    else
    begin
      FDataLink.BufferCount := CalcVisibleRowCount;
      if FDataLink.BufferCount <> VisibleRowCount then
      begin
        FDrawInfo.ScreenCache.ClearBars;
        FDrawInfo.SetScreenCacheSize(FDataLink.BufferCount);
        Invalidate;
      end;
    end;
  end;
end;

procedure TCustomEzGanttChart.UpdateHorizontalScrollbar;
var
  ScrollInfo: TScrollInfo;

begin
  if not HandleAllocated or not Showing or not Enabled or not CanUseTimebar or
     not (ScrollbarOptions.ScrollBars in [System.UITypes.TScrollStyle.ssBoth, System.UITypes.TScrollStyle.ssHorizontal])
  then
    Exit;

  with Timebar do
  begin
    FillChar(ScrollInfo, SizeOf(ScrollInfo), 0);
    ScrollInfo.cbSize := SizeOf(ScrollInfo);

    if Date < ScrollbarOptions.FLeftExtend then ScrollbarOptions.FLeftExtend := Date;
    if Date > ScrollbarOptions.FRightExtend then ScrollbarOptions.FRightExtend := Date;

    ScrollInfo.fMask := SIF_ALL;
    ScrollInfo.nMin  := 0;
    // Use Timebar.Width for clarity
    ScrollInfo.nPage := Timebar.Width div MajorWidth;
    ScrollInfo.nPos := TickCount(MajorScale, MajorCount, ScrollbarOptions.FLeftExtend, Date);
    ScrollInfo.nMax := TickCount(MajorScale, MajorCount, ScrollbarOptions.FLeftExtend, ScrollbarOptions.FRightExtend) + integer(ScrollInfo.nPage);
    SetScrollInfo(Self.Handle, SB_HORZ, ScrollInfo, true);
  end;
end;

procedure TCustomEzGanttChart.UpdateVerticalScrollBar;
var
  SIOld, SINew: TScrollInfo;
  rc: DWORD;

begin
  if Datalink.Active and HandleAllocated and (ScrollbarOptions.ScrollBars in [ssBoth, ssVertical]) then
  begin
    with Datalink.DataSet do
    begin
      SIOld.cbSize := sizeof(SIOld);
      SIOld.fMask := SIF_ALL;

      GetScrollInfo(Self.Handle, SB_VERT, SIOld);
      SINew := SIOld;

      if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssVertical]) then
        SINew.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
        SINew.fMask := SIF_ALL;

      if IsSequenced then
      begin
        SINew.nMin := 1;
        SINew.nPage := VirtualRowCount; //Self.VisibleRowCount;
        rc := RecordCount;

        if rc<=SINew.nPage then // Scrollbar may be hidden
        begin
          SINew.nMax := 1;
          SINew.nPage := 1;
          SINew.nPos := 0;
        end
        else
        begin
          SINew.nMax := Integer(rc + SINew.nPage - 1);
          if State in [dsInactive, dsBrowse, dsEdit] then
            SINew.nPos := RecNo;
        end;
      end
      else
      begin
        SINew.nMin := 0;
        SINew.nPage := 0;
        SINew.nMax := 4;
        if DataLink.BOF then
          SINew.nPos := 0
        else if DataLink.EOF then
          SINew.nPos := 4
        else
          SINew.nPos := 2;
      end;
      if (SINew.nMin <> SIOld.nMin) or (SINew.nMax <> SIOld.nMax) or
        (SINew.nPage <> SIOld.nPage) or (SINew.nPos <> SIOld.nPos)
      then
        SetScrollInfo(Self.Handle, SB_VERT, SINew, True);
    end;
  end
  else
  begin
    if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssVertical]) then
      SINew.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
      SINew.fMask := SIF_ALL;

    SINew.nMax := 1;
    SINew.nMin := 1;
    SINew.nPage := 1;
    SINew.nPos := 1;
    SetScrollInfo(Self.Handle, SB_VERT, SINew, True);
  end;
end;

procedure TCustomEzGanttChart.UpdateTimebarState;
begin
  if CanUseTimebar then
  begin
    Enabled := True;
    Activate;
  end else
    Enabled := False;
end;

function TCustomEzGanttChart.VirtualRowCount: Integer;
begin
  Result := CalcVisibleRowCount;
end;

procedure TCustomEzGanttChart.WMHScroll(var Msg: TWMHScroll);
var
  Delta: Integer;
  Saved: TDateTime;

begin
  if FUpdateCount > 0 then Exit;

  if Visible and CanFocus and TabStop and not (csDesigning in ComponentState) then
    SetFocus;

  BeginUpdate;
  try
    Delta := 0;
    case Msg.ScrollCode of
      SB_LINEUP: Delta := -1;
      SB_LINEDOWN: Delta := 1;
      SB_PAGEUP: Delta := -Timebar.Width div Timebar.MajorWidth;
      SB_PAGEDOWN: Delta := Timebar.Width div Timebar.MajorWidth;
      SB_THUMBTRACK:
      with Timebar do
      begin
        if gaoGoThumbTracking in Options then
        begin
          if FPredecessorLinesVisible = 0 then
            // 0 = FPredecessorLinesVisible not set
            // 1 = Predecessor lines where visible
            // 2 = Predecessor lines where not visible
          begin
            if FPredecessorLine.Visible then
              // Hide predecessor lines while scrolling
            begin
              FPredecessorLinesVisible := 1;
              FPredecessorLine.FVisible := False;
            end else
              FPredecessorLinesVisible := 2;
          end;

          Delta := TickCount(MajorScale, MajorCount, Date, Ticks2Date(ScrollbarOptions.FLeftExtend, Msg.Pos))
        end
        else
          Date := Ticks2Date(ScrollbarOptions.FLeftExtend, Msg.Pos);
      end;

      SB_THUMBPOSITION:
        if not (gaoGoThumbTracking in Options) or (FPredecessorLinesVisible = 1) then
        begin
          if FPredecessorLinesVisible = 1 then
            FPredecessorLine.FVisible := True;

          FPredecessorLinesVisible := 0;

          with Timebar do
            Date := Ticks2Date(ScrollbarOptions.FLeftExtend, Msg.Pos);
          UpdateHorizontalScrollbar;
          Invalidate;
        end;
      SB_BOTTOM:
      begin
        Timebar.Date := ScrollbarOptions.FRightExtend;
        UpdateHorizontalScrollbar;
      end;
      SB_TOP:
      begin
        Timebar.Date := ScrollbarOptions.FLeftExtend;
        UpdateHorizontalScrollbar;
      end;
    end;

    if Delta <> 0 then
      with Timebar do
      begin
        Saved := Date;
        Date := AddTicks(MajorScale, MajorCount, Date, Delta);
        if Saved<>Date then
        begin
          UpdateHorizontalScrollbar;
          Delta := TickCount(MajorScale, MajorCount, Saved, Date);
          ScrollClientRect(-Delta*MajorWidth);
        end;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TCustomEzGanttChart.WMVScroll(var Msg: TWMVScroll);
var
  SI: TScrollInfo;
begin
  if not AcquireFocus then Exit;
  if FDatalink.Active then
    with Msg, FDataLink.DataSet do
      case ScrollCode of
        SB_LINEUP: FDataLink.MoveBy(-FDatalink.ActiveRecord - 1);
        SB_LINEDOWN: FDataLink.MoveBy(FDatalink.RecordCount - FDatalink.ActiveRecord);
        SB_PAGEUP: FDataLink.MoveBy(-VisibleRowCount);
        SB_PAGEDOWN: FDataLink.MoveBy(VisibleRowCount);
        SB_THUMBTRACK:
          if IsSequenced and (gaoGoThumbTracking in Options) then
          begin
            if Pos <= 1 then First
            else if Pos >= RecordCount then Last
            else RecNo := Pos;
          end;
        SB_THUMBPOSITION:
          if IsSequenced then
          begin
            SI.cbSize := sizeof(SI);
            SI.fMask := SIF_ALL;
            GetScrollInfo(Self.Handle, SB_VERT, SI);
            if SI.nTrackPos <= 1 then
              First
            else if SI.nTrackPos >= RecordCount then
              Last
            else
              RecNo := SI.nTrackPos;
          end
          else
            case Pos of
              0: First;
              1: FDataLink.MoveBy(-VisibleRowCount);
              2: Exit;
              3: FDataLink.MoveBy(VisibleRowCount);
              4: Last;
            end;
        SB_BOTTOM:
          Last;
        SB_TOP:
          First;
        SB_ENDSCROLL:
          UpdateVerticalScrollBar;
      end;
end;

procedure TCustomEzGanttChart.WMKillFocus(var Message: TMessage);
begin
  inherited;
  if FIsDeleting then Exit;

  if (FSelectedGanttbars.Count>0) then
    if not (gaoKeepSelection in Options) then
      FSelectedGanttbars.Clear
    else if not (gaoAllwaysShowSelection in Options) then
      Invalidate;

  if FSelectedPredecessorLine<>nil then
  begin
    if not (poKeepSelecttion in PredecessorLine.Options) then
      DeselectPredecessorline
    else if not (poAllwaysShowSelection in PredecessorLine.Options) then
      HighlightPredecessorline;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------
procedure TCustomEzGanttChart.WMPrintClient(var Message: TWMPrintClient);
var
  Rgn: HRgn;
  R: TRect;

begin
  // Draw only if the window is visible or visibility is not required.
  if ((Message.Flags and PRF_CHECKVISIBLE) = 0) or IsWindowVisible(Handle) then
  begin
    EzBeginPrint;
    try
      R := ClientRect;
      if (BorderStyle = bsSingle) then OffsetRect(R, 2, 2);
      LPtoDP(Message.DC, R, 2);
      Rgn := CreateRectRgnIndirect(R);
      SelectClipRgn(Message.DC, Rgn);
      PaintTo(Message.DC, 0, 0);
      SelectClipRgn(Message.DC, 0);
      DeleteObject(Rgn);
    finally
      EzEndPrint;
    end;
  end;
end;

procedure TCustomEzGanttChart.WMSize(var Message: TWMSize);
begin
  inherited;
  if FUpdateCount = 0 then
  begin
    UpdateRowCount;
    UpdateVerticalScrollBar;
  end;
end;

procedure TCustomEzGanttChart.WMTimer(var Msg: TWMTimer);

  procedure UpdateTimeline;
  var
    NewPos: integer;
    R: TRect;

  begin
    NewPos := Timebar.DateTime2X(Now);
    if (NewPos <> FTimelinePos) then
    begin
      R.Left := -1;
      // remove current line
      if (FTimelinePos > ClientRect.Left) and (FTimelinePos < ClientRect.Right) then
      begin
        SetRect(R, FTimeLinePos, 0, FTimeLinePos, RowRect(VisibleRowCount-1).Bottom + FGridLineWidth);
        DrawTimeline(R);
      end;

      FTimeLinePos := NewPos;

      if (FTimelinePos > ClientRect.Left) and (FTimelinePos < ClientRect.Right) then
      begin
        if R.left = -1 then
          SetRect(R, FTimeLinePos, 0, FTimeLinePos, RowRect(VisibleRowCount-1).Bottom + FGridLineWidth) else
          SetRect(R, FTimeLinePos, 0, FTimeLinePos, R.Bottom);
        DrawTimeline(Rect(FTimeLinePos, 0, FTimeLinePos, RowRect(VisibleRowCount-1).Bottom + FGridLineWidth));
      end;
    end;
  end;

  procedure DoXScrolling;
  var
    hdst: Integer;
    Pos: TPoint;

  begin
    hdst := 0;

    if (FDragType <> bdNone) then
    begin
      Pos := ScreenToClient(Mouse.CursorPos);

      if Pos.X >= (ClientRect.Right-Timebar.MajorWidth) then
        hdst := max(5, Pos.X-ClientRect.Right)
      else if Pos.X < (ClientRect.Left+Timebar.MajorWidth) then
        hdst := min(Pos.X-ClientRect.Left, -5);

      // Scroll horizontally
      if hdst <> 0 then
        with Timebar do
          Date := AddTicks(MajorScale, MajorCount, Date, hdst div 5);
    end;
  end;

  procedure DoScrolling(DoX, DoY: Boolean);
  var
    Pos: TPoint;
    hdst: Integer;
  begin
    Pos := ScreenToClient(Mouse.CursorPos);

    if DoY then
    begin
      if (Pos.y >= ClientRect.Top) and (Pos.y < ClientRect.Top+10) then
      begin
        Datalink.ActiveRecord := 0;
        Datalink.Dataset.MoveBy(-1);
      end
      else if (Pos.y < ClientRect.Bottom) and (Pos.y > ClientRect.Bottom-10) then
      begin
        Datalink.ActiveRecord := Datalink.RecordCount-1;
        Datalink.Dataset.MoveBy(1);
      end;
    end;

    if DoX then
    begin
      hdst:=0;
      if (Pos.X>=ClientRect.Right-15) and (Pos.X<ClientRect.Right) then
        hdst := 4-(ClientRect.Right-Pos.X+4) div 5 {value between 1 and 3}
      else if (Pos.X<=ClientRect.Left+15) and (Pos.X>=ClientRect.Left) then
        hdst := -4+(Pos.X-ClientRect.Left+4) div 5; {value between -1 and -3}

      // Scroll horizontally
      if hdst <> 0 then
        with Timebar do
          Date := AddTicks(MajorScale, MajorCount, Date, hdst);
    end;
  end;

  procedure UpdateSelectionRect;
  begin
    DoScrolling(True, True);
    {
    // Make sure window is repainted
    UpdateWindow(Handle);

    Canvas.DrawFocusRect(FDragRect);
    }
  end;

begin
//  if CanUseTimebar then
    case Msg.TimerID of
      tmrTimeline: UpdateTimeline;
      tmrScroll: DoScrolling(True, False);
      tmrSelection: UpdateSelectionRect;
    end;
end;

procedure TCustomEzGanttChart.WMMouseWheel(var Message: TWMMouseWheel);
var
 ScrollLines: integer;

begin
  if inherited DoMouseWheel(
        KeysToShiftState(Message.Keys),
        Message.WheelDelta,
        SmallPointToPoint(Message.Pos))
  then
    Exit;
  
  if not Assigned(Datasource) or
     not Assigned(Datasource.Dataset) or
     not Datasource.Dataset.Active
  then
    Exit;

  SystemParametersInfo(SPI_GETWHEELSCROLLLINES, 0, @ScrollLines, 0);
  Datasource.Dataset.MoveBy(-ScrollLines * Message.WheelDelta div WHEEL_DELTA);
end;

procedure TCustomEzGanttChart.WMSetFocus(var Message: TWMSetFocus);
begin
  inherited;
  if FIsDeleting then Exit;
  if (FSelectedGanttbars.Count>0) and not (gaoAllwaysShowSelection in Options) then
    Invalidate
  else if (FSelectedPredecessorline<>nil) and not (poAllwaysShowSelection in Predecessorline.Options) then
    HighlightPredecessorline;
end;

initialization
begin
  LoadScreenCursors;
end;

end.


