unit EzScheduler;

{$I Ez.inc}

interface

uses Classes, SysUtils, EzDataset, EzArrays, EzCalendar, ContNrs;

type
  EEzScheduleError = class(Exception);
  TEzResourceAssignments = class;
  TEzScheduler = class;
  TEzScheduleTask = class;
  ITaskScheduleService = interface;
  IGroupScheduleService = interface;
  TScheduleBranch = class;

  TScheduleIntervalFlag = (
    // Do not schedule this interval
    ifDoNotSchedule,
    // Schedule this interval against the project calendar
    ifProjectCalendar,
    // Schedule this interval against the resource default availability profile.
    // That's the profile before allocating any scheduled task.
    ifResourceDefaultAvailability,
    // Schedule this interval against the resource actual availability profile.
    // That's the profile after allocating scheduled task.
    ifResourceActualAvailability
  );

  TEzWorkInterval = class
    TimeSpan: TEzTimespan;
    ResourceAssigments: TEzResourceAssignments;

  public
    destructor Destroy; override;
  end;

  TEzWorkIntervals = class(TEzObjectList)
  protected
    function GetItem(Index: Integer): TEzWorkInterval;
    procedure SetItem(Index: Integer; Value: TEzWorkInterval);
  public
    property Items[Index: Integer]: TEzWorkInterval read GetItem write SetItem; default;
  end;

  TEzScheduleInterval = class(TPersistent)
  public
    Start: TDateTime;
    Stop: TDateTime;
    ScheduleFlag: TScheduleIntervalFlag;
    Duration: Double;
    Tag: Integer;
    IsSlackInterval: Boolean;

    constructor Create; virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  end;

  TEzScheduleIntervals = class(TObjectList)
  protected
    function GetItem(Index: Integer): TEzScheduleInterval;
    procedure SetItem(Index: Integer; Value: TEzScheduleInterval);
  public
    property Items[Index: Integer]: TEzScheduleInterval read GetItem write SetItem; default;
  end;

  TEzCapacityKey = Variant;
  TEzTaskAssignment = class(TPersistent)
  protected
    FKey: Variant;
    FAssigned: Double;
    FAffectsDuration: Boolean;
    ScheduleSucceeded: Boolean;
    FSpeedFactor: Double;

  public
    constructor Create; overload; virtual;
    constructor Create(CopyFrom: TEzTaskAssignment); overload; virtual;
    procedure Assign(Source: TPersistent); override;

    property AffectsDuration: Boolean read FAffectsDuration write FAffectsDuration;
    property Assigned: Double read FAssigned write FAssigned;
    property Key: Variant read FKey write FKey;
    property SpeedFactor: Double read FSpeedFactor write FSpeedFactor;

  end;

  TEzTaskAssignments = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzTaskAssignment;
    function  GetText: string;
    procedure SetItem(Index: Integer; Value: TEzTaskAssignment);
    procedure SetText(Value: string);

  public
    property Text: string read GetText write SetText;
    property Items[Index: Integer]: TEzTaskAssignment read GetItem write SetItem; default;
  end;

  TEzResourceAssignment = class(TEzTaskAssignment)
  private
    procedure SetScheduleProfile(const Value: TEzAvailabilityProfile);
  protected
    FIgnoreOverallocations: Boolean;

    // CapacityKey holds the capacity for which this resource was selected.
    // If we're scheduling against capacity then all resources supporting
    // the required capacity are added to the profile. In order to keep
    // track of which resource was selected because of which capacity
    // this property is used.
    FCapacityKey: TEzCapacityKey;

    FScheduleProfile: TEzAvailabilityProfile;

    property CapacityKey: TEzCapacityKey read FCapacityKey write FCapacityKey;
  published

  public
    constructor Create; override;
    constructor Create(CopyFrom: TEzResourceAssignment); overload;
    destructor  Destroy; override;

    property IgnoreOverallocations: Boolean read FIgnoreOverallocations write FIgnoreOverallocations;
    property ScheduleProfile: TEzAvailabilityProfile read FScheduleProfile write SetScheduleProfile;
  end;

  TEzResourceAssignments = class(TEzTaskAssignments)
  protected
    function  GetItem(Index: Integer): TEzResourceAssignment;
    procedure SetItem(Index: Integer; Value: TEzResourceAssignment);

    function IndexOfKey(Key: Variant): Integer;

  public
    property Items[Index: Integer]: TEzResourceAssignment read GetItem write SetItem; default;
  end;

  TEzSortedResourceAssignments = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzResourceAssignment;

    function IndexOfKey(Key: Variant): Integer;

  public
    property Items[Index: Integer]: TEzResourceAssignment read GetItem; default;
  end;

  TEzCapacityListItem = class
  protected
    FKey: TEzCapacityKey;
    FSpeedFactor: Double;

  public
    constructor Create(AKey: TEzCapacityKey);
    property Key: TEzCapacityKey read FKey write FKey;
    property SpeedFactor: Double read FSpeedFactor write FSpeedFactor;
  end;

  TEzCapacityList = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzCapacityListItem;
    procedure SetItem(Index: Integer; Value: TEzCapacityListItem);

  public
    property Items[Index: Integer]: TEzCapacityListItem read GetItem write SetItem; default;
  end;

  TEzCapacityAssignment = class(TEzTaskAssignment)
  protected
    FMergeProfiles: Boolean;

  public
    constructor Create; override;
    constructor Create(CopyFrom: TEzCapacityAssignment); overload;

    property MergeProfiles: Boolean read FMergeProfiles write FMergeProfiles;
  end;

  TEzCapacityAssignments = class(TEzTaskAssignments)
  protected
    function  GetItem(Index: Integer): TEzCapacityAssignment;
    procedure SetItem(Index: Integer; Value: TEzCapacityAssignment);

  public
    property Items[Index: Integer]: TEzCapacityAssignment read GetItem write SetItem; default;
  end;

  TEzSortedCapacityAssignments = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzCapacityAssignment;

    function IndexOfKey(Key: Variant): Integer;

  public
    property Items[Index: Integer]: TEzCapacityAssignment read GetItem; default;
  end;

  TEzResourcePath = class
    protected
      FResources: TEzResourceAssignments;
      FCapacities: TEzCapacityAssignments;
      FSpeedFactor: Double;

      function get_SpeedFactor: Double;

    public
      constructor Create(
          _Resources: TEzResourceAssignments;
          _Capacities: TEzCapacityAssignments);
      destructor Destroy; override;

      function Clone: TEzResourcePath;

      property Resources: TEzResourceAssignments read FResources;
      property Capacities: TEzCapacityAssignments read FCapacities;
      property SpeedFactor: Double read get_SpeedFactor;
  end;

  // A list of TEzResourceAssignments lists
  TEzResourcePathList = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzResourcePath;

  public

    property Items[Index: Integer]: TEzResourcePath read GetItem; default;
  end;
  
  TEzResource = class(TPersistent)
  private
    // Key value which uniquely identifies this resource
    FKey: Variant;

    // Descriptive name for this resource
    FName: string;

    // Indicates whether thios resource may switch to
    // a different task and back.
    FAllowTaskSwitching: Boolean;

  	// Pointer to the original availability graph for this resource.
	  // ActualAvailability will use Availability as a source to copy
  	// data from. When a task is scheduled, then the allocated intervals
	  // are substracted from ActualAvailability.
  	// Availability allways contains the original data.
	  FAvailability: TEzAvailabilityProfile;

  	// Pointer to the actual availability graph for this resource. This graph
    // keep track of the allocated and unallocated intervals.
  	FActualAvailability: TEzAvailabilityProfile;

    // Boolean value which indicates if the profile refrenced by property
    // Availability is owned by this instance. If OwnsAvailability is true, then
    // the object referenced by Availability will be destroyed when this object
    // will be destroyed.
    FOwnsAvailability: Boolean;

    FCapacities: TEzCapacityList;

    // A list of resources or capacities required for this resource
    // to function. Whenever this resource is scheduled, the resources
    // from this list should be allocated as well.
    FOperators: TEzTaskAssignments;

    public
      constructor Create(Scheduler: TEzScheduler; Resource: TEzResourceAssignment); reintroduce; overload;
      constructor Create(AKey: Variant); reintroduce; overload;
      destructor  Destroy; override;
      procedure   Assign(Source: TPersistent); override;
      procedure   ProfileRequired;

    published
      property Key: Variant read FKey;
      property Name: string read FName write FName;
      property AllowTaskSwitching: Boolean read FAllowTaskSwitching write FAllowTaskSwitching;
	    property Availability: TEzAvailabilityProfile read FAvailability write FAvailability;
	    property ActualAvailability: TEzAvailabilityProfile read FActualAvailability write FActualAvailability;
      property Capacities: TEzCapacityList read FCapacities;
      property Operators: TEzTaskAssignments read FOperators;
      property OwnsAvailability: Boolean read FOwnsAvailability write FOwnsAvailability;
  end;
  PEzScheduleResource = ^TEzResource;

  TEzResources = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzResource;

  public
    function  ResourceByKey(Key: Variant): TEzResource;
    function  ResourceExists(Key: Variant): Boolean;

    property Items[Index: Integer]: TEzResource read GetItem; default;
  end;

  TEzPredecessorRelation = (
    // Schedule successor task after the finish date of predecessor
    prFinishStart,
    // Schedule the end date of the successor task at the start date
    // of predecessor
    prStartFinish,
    // Schedule the start date of the successor after the start date
    // of predecessor
    prStartStart,
    // Schedule the end date of the successor before the end date
    // of predecessor
    prFinishFinish,
    // Schedule the start date of the successor at the start date
    // of predecessor
    prSameStart,
    // There is no relation between the successor and the predecessor.
    prNoRelation
  );

  TEzConstraint = (
    // Do Not Level,
    // Do not schedule this task but update the resource availability using
    // the start and stop dates from the task.
    cnDNL,
    // Must Start On,
    // The start date of this task must be equal to the constraint date.
    cnMSO,
    // Must Finish On
    cnMFO,
    // Start No Earlier Than,
    cnSNET,
    // Finish At or Before,
    cnFAB,
    // Start No Later Than
    cnSNLT,
    // Finish No Later Than,
    cnFNLT,
    // As Soon As Possible
    cnASAP,
    // As Late As Possible
    cnALAP,
    // Ignore task
    // Do not schedule this task and do not allocate the task interval
    // from the resources.
    cnIgnore
  );

  TEzCapacityResourceListItem = class
  protected
    FKey: TEzCapacityKey;
    FResources: TEzResources;
  public
    constructor Create(AKey: TEzCapacityKey); reintroduce;
    destructor  Destroy; override;
    property Key: TEzCapacityKey read FKey write FKey;
    property Resources: TEzResources read FResources;
  end;

  TEzCapacityResourceList = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzCapacityResourceListItem;
    procedure SetItem(Index: Integer; Value: TEzCapacityResourceListItem);

    function IndexOf(AKey: TEzCapacityKey): Integer; overload;

  public
    property Items[Index: Integer]: TEzCapacityResourceListItem read GetItem write SetItem; default;
  end;

  TEzPredecessor = class(TPersistent)
  public
    Key: TNodeKey;
    Relation: TEzPredecessorRelation;
    Lag: Double;

    constructor Create;
    procedure Assign(Source: TPersistent); override;
  end;

  TEzPredecessors = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzPredecessor;
    function  GetText: string;
    procedure SetItem(Index: Integer; Value: TEzPredecessor);
    procedure SetText(Value: string);
  public
    function IndexOf(Key: TNodeKey): integer;
    property Items[Index: Integer]: TEzPredecessor read GetItem write SetItem; default;
    property Text: string read GetTExt write SetText;
  end;

  TEzPredecessorSuccessor = class(TObject)
  public
    Predecessor, Successor: TEzScheduleTask;
    constructor Create(APred, ASucc: TEzScheduleTask);
  end;

  TEzPredecessorSuccessors = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzPredecessorSuccessor;
    procedure SetItem(Index: Integer; Value: TEzPredecessorSuccessor);
  public
    procedure AddRelation(APred, ASucc: TEzScheduleTask);
    function  RelationExists(APred, ASucc: TEzScheduleTask): Boolean;

    property Items[Index: Integer]: TEzPredecessorSuccessor read GetItem write SetItem; default;
  end;

  TEzScheduleSuccessor = class(TPersistent)
  public
    Task: TEzScheduleTask;
    Relation: TEzPredecessorRelation;
    Lag: Double;

    procedure Assign(Source: TPersistent); override;
  end;

  TEzScheduleSuccessors = class(TObjectList)
  protected
    function GetItem(Index: Integer): TEzScheduleSuccessor;
    procedure SetItem(Index: Integer; Value: TEzScheduleSuccessor);
  public
    property Items[Index: Integer]: TEzScheduleSuccessor read GetItem write SetItem; default;
  end;

  TScheduleWindowBoundType = (
    // Date boundary is not set, schedule window still empty
    btEmpty,

    // Date boundary is set from project start date
    btProjectStartDate,
    // Date boundary is set from project end date
    btProjectEndDate,
    // Date boundary is set from start date of indicated task
    btTaskStartDate,
    // Date boundary is set from end date of indicated task
    btTaskEndDate,
    // Date boundary is set from constraint date of indicated task
    btConstraintDate,
    // Date boundary is set from the start date of the schedule window
    btTaskWindowStartDate,
    // Date boundary is set from the end date of the schedule window
    btTaskWindowEndDate,
    // Date boundary is set from the start date of the slack window
    btSlackWindowStartDate,
    // Date boundary is set from the end date of the slack window
    btSlackWindowEndDate
    );

  // Holds internal data used by the scheduler for storing intermediate results.
  TEzInternalData = record
    // Reserved for internal use by the scheduler;
    // Stores the maximum resource assignment for this task. Tasks with
    // higher assignments will be scheduled first.
    MaxAssignement: Double;

    // Reserved for internal use by the scheduler;
    // Successors holds a list of successor tasks.
    Successors: TEzScheduleSuccessors;

    // Reference to the Task which marks the left side of the schedule window.
    // Most probably a predecessor.
    LeftTask: TEzScheduleTask;
    LeftBoundaryType: TScheduleWindowBoundType;

    // Reference to the Task which marks the right side of the schedule window.
    // Most probably a successor.
    RightTask: TEzScheduleTask;
    RightBoundaryType: TScheduleWindowBoundType;

    // Window holds the interval in which the task should be scheduled
    // (also called a schedule window). Window.Start marks the left side
    // of the window and Window.Stop marks the right side.
    //
    // The window is either determined by the project schedule window or by
    // tasks to the left or right side of the current task (successors and
    // predecessors).
    //
    // The scheduler will try to schedule a task between these boundaries.
    Window: TEzTimeSpan;

    // Direction in which the scheduler should try to find a suitable interval
    // for this task.
    Direction: TEzScheduleDirection;

    // Profile holds the combined availability profile to schedule against.
    Profile: TEzAvailabilityProfile;
    OwnsProfile: Boolean;
  end;

  TEzScheduleTask = class(TPersistent)
  public
    // The unique key of the task
    Key: TNodeKey;

    // Key value of the parent if this task is a child
    Parent: TNodeKey;

    // Constraint to comply with when scheduling.
    Constraint: TEzConstraint;

    // DateTime value in case coConstraint requires one.
    ConstraintDate: TDateTime;

    // Priority of task.
    // Lower values indicate higher priorities.
    Priority: Integer;
    Flags: TEzScheduleResults;
    Intervals: TEzScheduleIntervals;

    // A list of resources or capacities required to complete this task
    Resources: TEzTaskAssignments;

    // A list of resources assigned to this task.
//    Resources: TEzResourceAssignments;
    ResourceAllocation: TAllocationType;

    // A capacity required to complete this task.
    // A task can either have resources or a capacity assigned (not both)
//    Capacity: TEzCapacityAssignment;

    // A list of predecessors assigned to this task.
    Predecessors: TEzPredecessors;

    // Stores an integer value as part of a schedule task.
    // Description:
    //  Tag has no predefined meaning. The Tag property is provided for the
    //  convenience of developers. It can be used for storing an additional
    //  integer value or it can be typecast to any 32-bit value such as a
    //  component reference or a pointer.
    Tag: Integer;

    // ScheduleData holds data required by the schedule engine.
    ScheduleData: TEzInternalData;

    // Indicates the the schedulewindow for this task.
    // The task will not be scheduled outside this window.
    WindowStart: TDateTime;
    WindowStop: TDateTime;

    // Holds a list of work intervals allocated for this task.
    WorkIntervals: TEzWorkIntervals;

    //NEW 04 JUILLET 2011 DP
    PickFinishASAPPath : Boolean;
    // The amount of freetime required between this task and any of it's
    // successors
    Slack: Double;
    SlackWindowStart: TDateTime;
    SlackWindowStop: TDateTime;

    constructor Create; reintroduce; virtual;
    destructor  Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure Reset;
  end;

  TEzSortedTaskList = class(TEzSortedObjectList)
  protected
    function  CompareItems(Obj1, Obj2: TObject): Integer; override;
    procedure SetTasks(Index: Integer; Task: TEzScheduleTask);
    function  GetTasks(Index: Integer): TEzScheduleTask;

  public
    constructor Create; reintroduce;
    property Tasks[Index: Integer]: TEzScheduleTask read GetTasks write SetTasks; default;
  end;

  // Extends TObjectQueue with a PushBack method.
  TEzObjectQueue = class(TObjectQueue)
  public
    // Empty queue
    procedure Clear;
    // Pushes an item at the back of the queue instead of at the beginning.
    procedure PushBack(AItem: TObject);
  end;

  TMessageSeverity = (
    msFatal,
    msError,
    msWarning,
    msNote
  );

  TEzScheduleMessage = class(TObject)
  public
    // Holds the key of the task to which this message belongs
    TaskKey: TNodeKey;
    // Holds the key of the resource which caused the scheduler to fail, if any.
    ResourceKey: Variant;
    // Text of message
    MessageText: string;
    // Left date boundary of the schedule window used during scheduling
    ScheduleWindowStart: TDateTime;
    // Right date boundary of the schedule window used during scheduling
    ScheduleWindowEnd: TDateTime;
    // Key of the (predecessor) task that marks the left boundary, if any.
    ScheduleWindowStartTaskKey: TNodeKey;
    // Key of the (successor) task that marks the right boundary, if any.
    ScheduleWindowEndTaskKey: TNodeKey;
    // Flag indicating how the left boundary was set
    StartBoundaryType: TScheduleWindowBoundType;
    // Flag indicating how the right boundary was set
    EndBoundaryType: TScheduleWindowBoundType;
    // Flag indicating severity of this schedule error
    Severity: TMessageSeverity;

    constructor Create; overload; virtual;
    constructor Create(Key: TNodeKey; AMessage: string; ASeverity: TMessageSeverity=msWarning); overload; virtual;

    procedure Assign(Source: TEzScheduleMessage);
  end;

  TEzScheduleMessages = class(TEzObjectList)
  protected
    function  GetItem(Index: Integer): TEzScheduleMessage;

  public
    constructor Create; reintroduce;

    property Items[Index: Integer]: TEzScheduleMessage read GetItem; default;
  end;

  TEzScheduleThread = class(TThread)
  protected
    Scheduler: TEzScheduler;
    procedure Execute(); override;

  public
    constructor Create(AScheduler: TEzScheduler);
  end;

  TLoadResourceData = procedure (Scheduler: TEzScheduler; Resource: TEzResource) of object;
  TTestResourceCapacityEvent = procedure(
                                    Task: TEzScheduleTask;
                                    Capacity: TEzCapacityAssignment;
                                    Resource: TEzResource;
                                    var AllowResource: Boolean) of object;

  TGetResourceScheduleProfile = procedure(
                                    Task: TEzScheduleTask;
                                    Interval: TEzScheduleInterval;
                                    Assignment: TEzResourceAssignment;
                                    Resource: TEzResource) of object;

  TGetResourceSpeedFactor = procedure(
                                    Resource: TEzResource;
                                    CapacityKey: TEzCapacityKey;
                                    out SpeedFactor: Double) of object;

  TGetResourceAllocationProfile = procedure(
                                    Task: TEzScheduleTask;
                                    Assignment: TEzResourceAssignment;
                                    Resource: TEzResource;
                                    var Profile: TEzAvailabilityProfile) of object;

  TEzScheduler = class(TComponent)
  private
    procedure UpdateTaskScheduleDirection(Task: TEzScheduleTask);

  protected
    FDefaultTaskScheduleService: ITaskScheduleService;
    FUseWorkingPeriods: Boolean;
    FReturnWorkIntervals: Boolean;
    FSchedulebeyondScheduleWindow: Boolean;
    FScheduleTasks: TEzSortedTaskList;
    FPriorityStack: TEzObjectStack;
    FPredecessorStack: TEzObjectStack;
    FSuccessorStack: TEzObjectStack;
    FTrackPath: TEzPredecessorSuccessors;
    FScheduleMessages: TEzScheduleMessages;
    FTaskCursor: TEzDBCursor;
    FProjectProfile: TEzAvailabilityProfile;
    FProjectWindowAbsoluteStart: TDateTime;
    FProjectWindowAbsoluteStop: TDateTime;
    FProjectWindowStart: TDateTime;
    FProjectWindowStop: TDateTime;
    FProjectMakeSpanStart: TDateTime;
    FProjectMakeSpanStop: TDateTime;
    FResources: TEzResources;
    FCapacityResources: TEzCapacityResourceList;
    FScheduleIntervals: TEzTimespanArray;
    FScheduleResults: TEzScheduleResults;

    FOnLoadResourceData: TLoadResourceData;
    FOnTestResourceCapacity: TTestResourceCapacityEvent;
    FOnGetResourceScheduleProfile: TGetResourceScheduleProfile;
    FOnGetResourceSpeedFactor: TGetResourceSpeedFactor;
    FOnGetResourceAllocationProfile: TGetResourceAllocationProfile;

		procedure CopyParentsResources(Task, Parent: TEzScheduleTask);
		procedure CopyParentPredecessors(Task, Parent: TEzScheduleTask);
    procedure DoLoadResourceData(Resource: TEzResource);
    function  DoTestResourceCapacity(Task: TEzScheduleTask;
                                     Capacity: TEzCapacityAssignment;
                                     Resource: TEzResource): Boolean;

    procedure AddScheduleMessage(AMessage: TEzScheduleMessage); overload;
    procedure AddScheduleMessage(Task: TEzScheduleTask; AMessage: string); overload;
    procedure AddScheduleMessage(Task: TEzScheduleTask; ResourceKey: Variant; AMessage: string); overload;
    function  CreateScheduleMessage(Task: TEzScheduleTask; ResourceKey: Variant; AMessage: string) : TEzScheduleMessage;

    procedure BuildResourceCapacityList;
    procedure BuildScheduleList;
    procedure CalculateSlackWindow(Task: TEzScheduleTask; TaskWindow: TEzTimeSpan);
    procedure CheckScheduleResult(Task: TEzScheduleTask; Window: TEzTimeSpan); virtual;
    procedure FillSuccessors;
    function  GetProjectProfile: TEzAvailabilityProfile; virtual;
    function  GetTaskScheduleData(Task: TEzScheduleTask; ScheduleWithPriority: Boolean; PriorityStack: TEzObjectStack) : Boolean;
    function  GetTaskWindow(Task: TEzScheduleTask) : TEzTimeSpan; virtual;
    function  IsFixedPoint(Task: TEzScheduleTask; Direction: TEzScheduleDirection) : Boolean; virtual;
    procedure LoadResourceData(Resource: TEzResourceAssignment); virtual;
    procedure NormalizePredecessors(Task: TEzScheduleTask);
    procedure ProcessResources(Task: TEzScheduleTask);
    procedure Run;
    function  PrepareScheduleTask(Task: TEzScheduleTask; ScheduleWithPriority: Boolean) : Boolean;
    function  TestParentResourceAllocated(Path: TEzResourcePath; var Window: TEzTimespan) : Boolean;
    procedure ScheduleTaskGroup(Task: TEzScheduleTask);
    function  ScheduleHierarchy(  const service: IGroupScheduleService;
                                  Branch: TScheduleBranch;
                                  ReverseScheduleOrder: Boolean;
                                  ForceDirection: TEzScheduleDirection;
                                  var Window: TEzTimespan;
                                  var PrevailingDirection: TEzScheduleDirection;
                                  var PathSuccessfull: Boolean) : Boolean;

    function  ScheduleAsGroup(Task: TEzScheduleTask) : Boolean;
    procedure ScheduleTasks; virtual;
    procedure ScheduleTask(TaskKey: TNodeKey; Task: TEzScheduleTask; ScheduleWithPriority: Boolean);
    procedure ScheduleTaskIntervals(  service: ITaskScheduleService;
                                      Task: TEzScheduleTask;
                                      var TaskWindow: TEzTimeSpan);
    procedure ScheduleTask_DoNotLevel(Task: TEzScheduleTask);
    procedure ScheduleToProjectCalendar(Key: Variant; Task: TEzScheduleTask; Interval: TEzScheduleInterval; var ResultInterval: TEzTimeSpan);
//    procedure ScheduleToOriginalProfile(Key: Variant; Task: TEzScheduleTask; Interval: TEzScheduleInterval; var ResultInterval: TEzTimeSpan);
    procedure ScheduleToResourceProfile(Service: ITaskScheduleService;
                                        const Key: Variant;
                                        Task: TEzScheduleTask;
                                        Interval: TEzScheduleInterval;
                                        var ResultInterval: TEzTimeSpan);
    procedure StoreScheduleTask(Node: TRecordNode); virtual;
//    function  TrackFailingResource(Task: TEzScheduleTask; Interval: TEzScheduleInterval): TEzResource;
    procedure UpdateWindowWithTask(var ScheduleData: TEzInternalData; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithPredecessor(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithPredecessorTaskWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
    procedure UpdateWindowWithPredecessorSlackWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
    procedure UpdateWindowWithSuccessor(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithSuccessorTaskWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask); virtual;
    procedure UpdateWindowWithSuccessorSlackWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask); virtual;    
    function  VerifyTaskWindow(Task: TEzScheduleTask): Boolean; virtual;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure AddTask(Task: TEzScheduleTask);
    function  FindTask(Key: TNodeKey): TEzScheduleTask;
    procedure RemoveTask(Task: TEzScheduleTask);
    function  GetNextScheduledTask(var Task: TEzScheduleTask): Boolean;
    procedure Reset(ResetAll: Boolean = True);
    procedure Reschedule;

    property Messages: TEzScheduleMessages read FScheduleMessages;
    property ProjectMakeSpanStart: TDateTime read FProjectMakeSpanStart;
    property ProjectMakeSpanStop: TDateTime read FProjectMakeSpanStop;

    property ProjectProfile: TEzAvailabilityProfile
                  read FProjectProfile
                  write FProjectProfile;

    property Resources: TEzResources read FResources;
    property ScheduleResults: TEzScheduleResults read FScheduleResults;
    property UseWorkingPeriods: Boolean read FUseWorkingPeriods write FUseWorkingPeriods default False;

  published
    property SchedulebeyondScheduleWindow: Boolean
              read FSchedulebeyondScheduleWindow
              write FSchedulebeyondScheduleWindow
              default False;

    property ProjectWindowAbsoluteStart: TDateTime read FProjectWindowAbsoluteStart write FProjectWindowAbsoluteStart;
    property ProjectWindowStart: TDateTime read FProjectWindowStart write FProjectWindowStart;
    property ProjectWindowAbsoluteStop: TDateTime read FProjectWindowAbsoluteStop write FProjectWindowAbsoluteStop;
    property ProjectWindowStop: TDateTime read FProjectWindowStop write FProjectWindowStop;
    property ReturnWorkIntervals: Boolean read FReturnWorkIntervals write FReturnWorkIntervals default false;
    property OnLoadResourceData: TLoadResourceData
                read FOnLoadResourceData write FOnLoadResourceData;
    property OnTestResourceCapacity: TTestResourceCapacityEvent
                read FOnTestResourceCapacity write FOnTestResourceCapacity;
    property OnGetResourceScheduleProfile: TGetResourceScheduleProfile
                read FOnGetResourceScheduleProfile write FOnGetResourceScheduleProfile;
    property OnGetResourceSpeedFactor: TGetResourceSpeedFactor
                read FOnGetResourceSpeedFactor write FOnGetResourceSpeedFactor;
    property OnGetResourceAllocationProfile: TGetResourceAllocationProfile
                read FOnGetResourceAllocationProfile write FOnGetResourceAllocationProfile;
  end;

  ITaskScheduleService = interface
    procedure AddScheduleMessage(AMessage: TEzScheduleMessage); overload;
    procedure AddScheduleMessage(Task: TEzScheduleTask; AMessage: string); overload;
    procedure Clear;
    procedure CrossJoinProfiles(Task: TEzScheduleTask; Paths: TEzResourcePathList);

    // Returns True when this service holds resource assignments besides
    // resources assigned to the task to be scheduled
    function  ContainsResourceAssigments: Boolean;

    procedure AllocateResources( Task: TEzScheduleTask;
                                 Intervals: TEzTimespanArray;
                                 ResourcePath: TEzResourcePath);

    procedure GetResourceScheduleProfile( Task: TEzScheduleTask;
                                          Interval: TEzScheduleInterval;
                                          Assignment: TEzResourceAssignment;
                                          Resource: TEzResource);

    function  GetResourceAllocationProfile( Task: TEzScheduleTask;
                                            Assignment: TEzResourceAssignment;
                                            Resource: TEzResource) : TEzAvailabilityProfile;

    function  PrepareResourceProfile( Task: TEzScheduleTask;
                                      Interval: TEzScheduleInterval;
                                      Path: TEzResourcePath;
                                      var PathHasMergedProfile: Boolean) : TEzAvailabilityProfile;
  end;

  TTaskScheduleService = class(TInterfacedObject, ITaskScheduleService)
  protected
    _comitting: Boolean;
    _ProfilesToDestroy: TObjectList;
    _Owner: TEzScheduler;

  private
    procedure AddScheduleMessage(AMessage: TEzScheduleMessage); overload; virtual;
    procedure AddScheduleMessage(Task: TEzScheduleTask; AMessage: string); overload; virtual;

    function  GetResourceOriginalScheduleProfile(Assignment: TEzResourceAssignment; Resource: TEzResource) : TEzAvailabilityProfile; virtual;

    procedure GetResourceScheduleProfile( Task: TEzScheduleTask;
                                          Interval: TEzScheduleInterval;
                                          Assignment: TEzResourceAssignment;
                                          Resource: TEzResource); virtual;

    function  GetResourceAllocationProfile( Task: TEzScheduleTask;
                                            Assignment: TEzResourceAssignment;
                                            Resource: TEzResource) : TEzAvailabilityProfile; virtual;

    procedure AddAssigmentToProfile(  Task: TEzScheduleTask;
                                      Interval: TEzScheduleInterval;
                                      Profile: TEzAvailabilityProfile;
                                      Assignment: TEzResourceAssignment);
    procedure AddMergedProfilesToProfile( Task: TEzScheduleTask;
                                          Interval: TEzScheduleInterval;
                                          Profile: TEzAvailabilityProfile;
                                          Path: TEzResourcePath;
                                          var PathHasMergedProfile: Boolean;
                                          var i_res: Integer;
                                          I_bIsFirstMergedProfile : Boolean);//NEW 30 MAI 2012 DP - SOME MERGED PROFILE WHERE GETTING UNITS REQUIRED = 0

    function ReturnWorkIntervals: Boolean; virtual;
  protected
    procedure Clear; virtual;
    procedure CrossJoinProfiles(Task: TEzScheduleTask; Paths: TEzResourcePathList); virtual;

    // Returns True when this service holds resource assignments besides
    // resources assigned to the task to be scheduled
    function  ContainsResourceAssigments: Boolean; virtual;

    procedure AllocateResources( Task: TEzScheduleTask;
                                 Intervals: TEzTimespanArray;
                                 ResourcePath: TEzResourcePath); virtual;

    function PrepareResourceProfile(  Task: TEzScheduleTask;
                                      Interval: TEzScheduleInterval;
                                      Path: TEzResourcePath;
                                      var PathHasMergedProfile: Boolean) : TEzAvailabilityProfile; virtual;
    procedure SaveWorkIntervals(Task: TEzScheduleTask; WorkIntervals: TEzWorkIntervals); virtual;
  public
    constructor Create(AOwner: TEzScheduler);
    procedure BeforeDestruction; override;
  end;

  // Holds a temporary allocation profile for a given resource
  TTemporaryAllocationProfile = class
  protected
    Resource: TEzResource;
    Allocation: TEzAvailabilityProfile;
    ScheduleProfile: TEzAvailabilityProfile;

  public
    destructor Destroy; override;
  end;

  // Holds a list of temporary allocation profile per resource
  TSortedAllocationProfiles = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TTemporaryAllocationProfile;

    function Find(Key: TEzResource): TTemporaryAllocationProfile;

  public
    property Items[Index: Integer]: TTemporaryAllocationProfile read GetItem; default;
  end;

  // Holds a temporary allocation profile for a given resource
  TTemporaryAllocation = class
  protected
    Task: TEzScheduleTask;
    Intervals: TEzTimespanArray;
    ResourcePath: TEzResourcePath;

  public
    constructor Create;
    destructor  Destroy; override;
  end;

  // Holds a list of temporary allocation profile per resource
  TSortedAllocations = class(TEzSortedObjectList)
  protected
    function  CompareItems(Item1, Item2: TObject): Integer; override;
    function  GetItem(Index: Integer): TTemporaryAllocation;
    function  Find(Key: TEzScheduleTask): TTemporaryAllocation;
  public
    property Items[Index: Integer]: TTemporaryAllocation read GetItem; default;
  end;

  TScheduleBranch = class(TList)
  protected
    FParent: TEzScheduleTask;

  public
    BestService: IGroupScheduleService;

    destructor Destroy; override;

    property Parent: TEzScheduleTask read FParent write FParent;
  end;

  IGroupScheduleService = interface(ITaskScheduleService)

    function  get_OuterWindow: TEzTimespan;
    procedure set_OuterWindow(Value: TEzTimespan);
    function  get_Parent: IGroupScheduleService;
    function  get_ResourcePath: TEzResourcePath;
    procedure set_ResourcePath(Value: TEzResourcePath);

    procedure AllocateResources( Task: TEzScheduleTask;
                                 Intervals: TEzTimespanArray;
                                 ResourcePath: TEzResourcePath);
    procedure Commit;
    procedure GetResourceScheduleProfile( Task: TEzScheduleTask;
                                          Interval: TEzScheduleInterval;
                                          Assignment: TEzResourceAssignment;
                                          Resource: TEzResource);
    function  GetResourceAllocationProfile( Task: TEzScheduleTask;
                                            Assignment: TEzResourceAssignment;
                                            Resource: TEzResource) : TEzAvailabilityProfile;

    procedure Restart;
    procedure SendScheduleMessages;

    property OuterWindow: TEzTimespan
      read  get_OuterWindow
      write set_OuterWindow;
    property Parent: IGroupScheduleService
      read  get_Parent;
    property ResourcePath: TEzResourcePath
      read  get_ResourcePath
      write set_ResourcePath;
  end;

  TGroupScheduleService = class(TTaskScheduleService, IGroupScheduleService)
  protected
    _tempAllocationProfiles: TSortedAllocationProfiles;
    _tempAllocations: TSortedAllocations;
    _savedAllocations: TSortedAllocations;
    _savedWorkIntervals: TEzWorkIntervals;
    _outerWindow: TEzTimespan;
    _parent: IGroupScheduleService;
    _resourcePath: TEzResourcePath;
    _scheduleMessages: TEzScheduleMessages;
    _parentTask: TEzScheduleTask;

    function  get_OuterWindow: TEzTimespan;
    procedure set_OuterWindow(Value: TEzTimespan);
    function  get_Parent: IGroupScheduleService;
    function  get_ResourcePath: TEzResourcePath;
    procedure set_ResourcePath(Value: TEzResourcePath);

    procedure Commit;
    procedure Restart;
    procedure SendScheduleMessages;

    procedure AddScheduleMessage(AMessage: TEzScheduleMessage); override;
    procedure AddScheduleMessage(Task: TEzScheduleTask; AMessage: string); override;
    procedure CrossJoinProfiles(Task: TEzScheduleTask; Paths: TEzResourcePathList); override;

    // Returns True when this service holds resource assignments besides
    // resources assigned to the task to be scheduled
    function  ContainsResourceAssigments: Boolean; override;

    procedure AllocateResources( Task: TEzScheduleTask;
                                 Intervals: TEzTimespanArray;
                                 ResourcePath: TEzResourcePath); override;

    function  ReturnWorkIntervals: Boolean; override;
    procedure SaveWorkIntervals(Task: TEzScheduleTask; WorkIntervals: TEzWorkIntervals); override;

    function GetResourceOriginalScheduleProfile(Assignment: TEzResourceAssignment; Resource: TEzResource): TEzAvailabilityProfile; override;

    procedure GetResourceScheduleProfile( Task: TEzScheduleTask;
                                          Interval: TEzScheduleInterval;
                                          Assignment: TEzResourceAssignment;
                                          Resource: TEzResource); override;

    function  GetResourceAllocationProfile( Task: TEzScheduleTask;
                                            Assignment: TEzResourceAssignment;
                                            Resource: TEzResource) : TEzAvailabilityProfile; override;

    function PrepareResourceProfile(  Task: TEzScheduleTask;
                                      Interval: TEzScheduleInterval;
                                      Path: TEzResourcePath;
                                      var PathHasMergedProfile: Boolean) : TEzAvailabilityProfile; override;
  public
    constructor Create( AOwner: TEzScheduler;
                        ParentTask: TEzScheduleTask;
                        const ParentService: IGroupScheduleService);

    procedure BeforeDestruction; override;
  end;

  function IsMileStone(Task: TEzScheduleTask): Boolean;

var
  ccc:Integer;

implementation

uses EzStrings_3, Math, EzDatetime
{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
;
{$ENDIF}

function SpeedFactorComparer(Item1, Item2: Pointer) : Integer;
var
  a1: TEzResourcePath absolute Item1;
  a2: TEzResourcePath absolute Item2;
begin
  if a1.SpeedFactor > a2.SpeedFactor then
    Result := -1
  else if a1.SpeedFactor < a2.SpeedFactor then
    Result := 1
  else
    Result := 0;
end;

procedure EzScheduleError(const Message: string; Scheduler: TEzScheduler = nil);
begin
  if Assigned(Scheduler) then
    raise EEzScheduleError.Create(Format('%s: %s', [Scheduler.Name, Message])) else
    raise EEzScheduleError.Create(Message);
end;

function PosEx(const SubStr, S: string; Offset: Cardinal = 1): Integer;
var
  I,X: Integer;
  Len, LenSubStr: Integer;
begin
  if Offset = 1 then
    Result := Pos(SubStr, S)
  else
  begin
    I := Offset;
    LenSubStr := Length(SubStr);
    Len := Length(S) - LenSubStr + 1;
    while I <= Len do
    begin
      if S[I] = SubStr[1] then
      begin
        X := 1;
        while (X < LenSubStr) and (S[I + X] = SubStr[X + 1]) do
          Inc(X);
        if (X = LenSubStr) then
        begin
          Result := I;
          exit;
        end;
      end;
      Inc(I);
    end;
    Result := 0;
  end;
end;

procedure SplitString(Value: string; sl: TStringList);
var
  i, l, p: Integer;
  q: Boolean;

begin
  i:=1;
  p:=1;
  q:=False;
  l:=length(Value);
  if l=0 then Exit;
  while i<=l do
  begin
    if (Value[i] = '''') and (i<l) and (Value[i+1] <> '''') then
      q := not q;

    if (Value[i] = ',') and not q then
    begin
      sl.Add(Copy(Value, p, i-p));
      p:=i+1;
    end;
    inc(i);
  end;
  sl.Add(Copy(Value, p, i-p));
end;

function SaveStrToFloat(Text: string): Double;
var
  C: Char;

begin
  C := {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator;
  {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator := '.';
  try
    Result := StrToFloat(StringReplace(Text, ',', '.', [rfReplaceAll]));
  finally
    {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator := C;
  end;
end;

function SafeString(Value: Variant): String;
begin
  if not VarIsNull(Value) then
    Result := Value else
    Result := SNoString;
end;

function EstimateTaskduration(Task: TEzScheduleTask): Double;
var
  i: Integer;

begin
  Result := 0;
  for i:=0 to Task.Intervals.Count-1 do
    Result := Result+Task.Intervals[i].Duration;
end;

function GetExtendsFromIntervals(Intervals: TEzTimespanArray): TEzTimeSpan; overload;
var
  i: Integer;
  ts: TEzTimespan;

begin
  if Intervals.Count=0 then
    EzScheduleError(SIntevalsAreEmpty);

  Result.Start := MaxEzDatetime;
  Result.Stop := 0;
  for i:=0 to Intervals.Count-1 do
  begin
    ts := Intervals[i];
    Result.Start := min(Result.Start, ts.Start);
    Result.Stop := max(Result.Stop, ts.Stop);
  end;
end;

function GetExtendsFromIntervals(Intervals: TEzScheduleIntervals): TEzTimeSpan; overload;
var
  i: Integer;
  ts: TEzScheduleInterval;
begin
  if Intervals.Count=0 then
    EzScheduleError(SIntevalsAreEmpty);

  Result.Start := MaxEzDatetime;
  Result.Stop := 0;
  for i:=0 to Intervals.Count-1 do
  begin
    ts := Intervals[i];
    Result.Start := min(Result.Start, DatetimeToEzDatetime(ts.Start));
    Result.Stop := max(Result.Stop, DatetimeToEzDatetime(ts.Stop));
  end;
end;

function IsMileStone(Task: TEzScheduleTask): Boolean;
begin
  Result := (Task.Intervals.Count=0) and (Task.ConstraintDate>0);
end;
//=----------------------------------------------------------------------------=
// TEzTaskAssignment
//=----------------------------------------------------------------------------=
constructor TEzTaskAssignment.Create;
begin
  inherited;
  Assigned := 1.0;
  ScheduleSucceeded := False;
  FAffectsDuration := False;
  FSpeedfactor := -1;
end;

procedure TEzTaskAssignment.Assign(Source: TPersistent);
begin
  if Source is TEzTaskAssignment then
  begin
    FKey := TEzTaskAssignment(Source).Key;
    FAssigned := TEzTaskAssignment(Source).Assigned;
    FAffectsDuration := TEzTaskAssignment(Source).AffectsDuration;
    FSpeedfactor := TEzTaskAssignment(Source).SpeedFactor;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzTaskAssignments
//=----------------------------------------------------------------------------=
function TEzTaskAssignments.GetItem(Index: Integer): TEzTaskAssignment;
begin
  Result := TEzTaskAssignment(inherited GetItem(Index));
end;

function TEzTaskAssignments.GetText: string;
var
  i: Integer;
  r: TEzTaskAssignment;

begin
  Result := '';
  for i:=0 to Count-1 do
  begin
    r := Items[i];
    if Result <> '' then
      Result := result + ',';

    if r.Assigned <> 1.0 then
      Result := r.Key + ':' + FloatToStr(r.Assigned) else
      Result := r.Key;
  end;
end;

procedure TEzTaskAssignments.SetItem(Index: Integer; Value: TEzTaskAssignment);
begin
  inherited SetItem(Index, Value);
end;

//
// Handle Text property
// Strings can be assigned like: John,Henk or John:2,0,Piet
//
procedure TEzTaskAssignments.SetText(Value: string);
var
  sl: TStringList;
  i, p1: Integer;
  R: TEzTaskAssignment;
  val: string;

begin
  Clear;
  sl := TStringList.Create;
  try
    SplitString(Value, sl);

    for i:=0 to sl.count-1 do
    begin
      R := TEzTaskAssignment.Create;
      val := sl[i];
      p1 := Pos(':', val);

      if p1 = 0 then
      begin
        R.Key := val;
        R.Assigned := 1.0;
      end
      else
      begin
        R.Key := Copy(val, 1, p1-1);
        R.Assigned := SaveStrToFloat(Copy(val,p1+1,MaxInt));
      end;
      Add(R);
    end;
  finally
    sl.Destroy;
  end;
end;

constructor TEzCapacityListItem.Create(AKey: Variant);
begin
  FKey := AKey;
  FSpeedFactor := 1.0;
end;

function TEzCapacityList.GetItem(Index: Integer): TEzCapacityListItem;
begin
  Result := TEzCapacityListItem(inherited GetItem(Index));
end;

procedure TEzCapacityList.SetItem(Index: Integer; Value: TEzCapacityListItem);
begin
  inherited SetItem(Index, Value);
end;

function TEzResourceAssignments.GetItem(Index: Integer): TEzResourceAssignment;
begin
  Result := TEzResourceAssignment(inherited GetItem(Index));
end;

function TEzResourceAssignments.IndexOfKey(Key: Variant): Integer;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    if Items[i].Key = Key then
    begin
      Result := i;
      Exit;
    end;
  end;
  Result := -1;
end;

procedure TEzResourceAssignments.SetItem(Index: Integer; Value: TEzResourceAssignment);
begin
  inherited SetItem(Index, Value);
end;

constructor TEzCapacityAssignment.Create(CopyFrom: TEzCapacityAssignment);
begin
  inherited Create(CopyFrom);
  FMergeProfiles := CopyFrom.MergeProfiles;
end;

constructor TEzCapacityAssignment.Create;
begin
  inherited;
  FSpeedFactor := -1; // Not set
end;

function TEzCapacityAssignments.GetItem(Index: Integer): TEzCapacityAssignment;
begin
  Result := TEzCapacityAssignment(inherited GetItem(Index));
end;

procedure TEzCapacityAssignments.SetItem(Index: Integer; Value: TEzCapacityAssignment);
begin
  inherited SetItem(Index, Value);
end;

//=----------------------------------------------------------------------------=
// TEzScheduleInterval
//=----------------------------------------------------------------------------=
constructor TEzScheduleInterval.Create;
begin
  inherited;
  ScheduleFlag := ifResourceActualAvailability;
end;

destructor TEzScheduleInterval.Destroy;
begin
  inherited;
end;

procedure TEzScheduleInterval.Assign(Source: TPersistent);
begin
  if Source is TEzScheduleInterval then
  begin
    Start := TEzScheduleInterval(Source).Start;
    Stop := TEzScheduleInterval(Source).Stop;
    ScheduleFlag := TEzScheduleInterval(Source).ScheduleFlag;
    Duration := TEzScheduleInterval(Source).Duration;
    Tag := TEzScheduleInterval(Source).Tag;
    IsSlackInterval := TEzScheduleInterval(Source).IsSlackInterval;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleIntervals
//=----------------------------------------------------------------------------=
function TEzScheduleIntervals.GetItem(Index: Integer): TEzScheduleInterval;
begin
  Result := TEzScheduleInterval(inherited GetItem(Index));
end;

procedure TEzScheduleIntervals.SetItem(Index: Integer; Value: TEzScheduleInterval);
begin
  inherited SetItem(Index, Value);
end;

//=----------------------------------------------------------------------------=
// TEzResource
//=----------------------------------------------------------------------------=
constructor TEzResource.Create(AKey: Variant);
begin
  inherited Create;
  FKey := AKey;
  FAllowTaskSwitching := False;
  FAvailability := nil;
  FName := '';
  FOwnsAvailability := True;
  FCapacities := TEzCapacityList.Create(True);
  FOperators := TEzTaskAssignments.Create(True);
end;

constructor TEzResource.Create(Scheduler: TEzScheduler; Resource: TEzResourceAssignment);
begin
  Create(Resource.Key);
  Scheduler.DoLoadResourceData(Self);
end;

destructor TEzResource.Destroy;
begin
  inherited;
  FActualAvailability.Free;
  FCapacities.Free;
  if FOwnsAvailability then
    FAvailability.Free;
  FOperators.Free;
end;

procedure TEzResource.Assign(Source: TPersistent);
begin
  if Source is TEzResource then
  begin
    FKey := TEzResource(Source).Key;
    FName := TEzResource(Source).Name;
	  FAvailability := TEzResource(Source).Availability;
    FOperators := TEzResource(Source).Operators;
    ActualAvailability.Assign(TEzResource(Source).ActualAvailability);
    FCapacities.Assign(TEzResource(Source).Capacities);
  end else
    inherited;
end;

procedure TEzResource.ProfileRequired;
begin
  if FActualAvailability=nil then
  begin
    FActualAvailability := TEzAvailabilityProfile.Create(0);
    FActualAvailability.AddSource(FAvailability, False);
  end;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleTask
//=----------------------------------------------------------------------------=
constructor TEzScheduleTask.Create;
begin
  inherited;
  Constraint := cnAsap;
  Flags := [];
  Intervals := TEzScheduleIntervals.Create;
  Resources := TEzTaskAssignments.Create(True);
  ResourceAllocation:= alFixed;
  Predecessors := TEzPredecessors.Create(True);
  ScheduleData.Direction := sdUndefined;
  ScheduleData.Successors := TEzScheduleSuccessors.Create;
  ScheduleData.LeftTask := nil;
  ScheduleData.RightTask := nil;
  ScheduleData.Profile := nil;
  WindowStart := 0;
  WindowStop := 0;
  Slack := 0;
  SlackWindowStart := 0;
  SlackWindowStop := 0;
  PickFinishASAPPath := False; //NEW 04 JUILLET 2011 DP
  WorkIntervals := nil;
end;

destructor TEzScheduleTask.Destroy;
begin
  inherited;
  Intervals.Free;
  Resources.Free;
  Predecessors.Free;
  ScheduleData.Successors.Free;
  if ScheduleData.OwnsProfile then
    ScheduleData.Profile.Free;

  WorkIntervals.Free;
end;

procedure TEzScheduleTask.Reset;
begin
  Flags := [];
  ScheduleData.Direction := sdUndefined;
  ScheduleData.Successors.Free;
  ScheduleData.Successors := TEzScheduleSuccessors.Create;
  ScheduleData.LeftTask := nil;
  ScheduleData.RightTask := nil;
  ScheduleData.Profile := nil;
end;

procedure TEzScheduleTask.Assign(Source: TPersistent);
begin
end;

constructor TEzSortedTaskList.Create;
begin
  inherited Create(False {object are not automatically freed});
  Duplicates := dupAccept;
end;

function TEzSortedTaskList.CompareItems(Obj1, Obj2: TObject): Integer;
var
  i1: TEzScheduleTask;
  i2: TEzScheduleTask;

begin
  i1 := TEzScheduleTask(Obj1);
  i2 := TEzScheduleTask(Obj2);

	if i1.Constraint <> i2.Constraint then
  begin
    if i1.Constraint < i2.Constraint then
      Result := -1 else
      Result := 1;
    Exit;
  end;

	if i1.Priority <> i2.Priority then
  begin
    if i1.Priority < i2.Priority then
      Result := -1 else
      Result := 1;
    Exit;
  end;

  if (i1.Constraint < cnASAP) and (i1.ConstraintDate <> i2.ConstraintDate) then
  begin
    if i1.ConstraintDate < i2.ConstraintDate then
      Result := -1 else
      Result := 1;
    Exit;
  end;

  if i1.ScheduleData.MaxAssignement = i1.ScheduleData.MaxAssignement then
    Result := 0
  else if i1.ScheduleData.MaxAssignement < i1.ScheduleData.MaxAssignement then
    Result := -1
  else
    Result := 1;
end;

procedure TEzSortedTaskList.SetTasks(Index: Integer; Task: TEzScheduleTask);
begin
  inherited Items[index] := Task;
end;

function TEzSortedTaskList.GetTasks(Index: Integer): TEzScheduleTask;
begin
  result := TEzScheduleTask(inherited Items[index]);
end;

//=----------------------------------------------------------------------------=
// TEzObjectQueue
//=----------------------------------------------------------------------------=
procedure TEzObjectQueue.Clear;
begin
  List.Clear;
end;

procedure TEzObjectQueue.PushBack(AItem: TObject);
begin
  List.Insert(List.Count, AItem);
end;

//=----------------------------------------------------------------------------=
// TEzCapacityResourceListItem
//=----------------------------------------------------------------------------=
constructor TEzCapacityResourceListItem.Create(AKey: TEzCapacityKey);
begin
  FKey := AKey;
  FResources := TEzResources.Create(False);
end;

destructor TEzCapacityResourceListItem.Destroy;
begin
  inherited;
  FResources.Free;
end;

//=----------------------------------------------------------------------------=
// TEzCapacityResourceList
//=----------------------------------------------------------------------------=
function TEzCapacityResourceList.CompareItems(Item1, Item2: TObject): Integer;
var
  i1: TEzCapacityResourceListItem;
  i2: TEzCapacityResourceListItem;

begin
  i1 := TEzCapacityResourceListItem(Item1);
  i2 := TEzCapacityResourceListItem(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzCapacityResourceList.GetItem(Index: Integer): TEzCapacityResourceListItem;
begin
  Result := TEzCapacityResourceListItem(inherited GetItem(Index));
end;

procedure TEzCapacityResourceList.SetItem(Index: Integer; Value: TEzCapacityResourceListItem);
begin
  inherited SetItem(Index, Value);
end;

function TEzCapacityResourceList.IndexOf(AKey: TEzCapacityKey): Integer;
var
  Find: TEzCapacityResourceListItem;

begin
  Find := TEzCapacityResourceListItem.Create(AKey);
  try
    Result := IndexOf(Find);
  finally
    Find.Destroy;
  end;
end;

//=----------------------------------------------------------------------------=
// TEzPredecessor
//=----------------------------------------------------------------------------=
constructor TEzPredecessor.Create;
begin
  Relation := prFinishStart;
  Lag := 0.0;
end;

procedure TEzPredecessor.Assign(Source: TPersistent);
begin
  if Source is TEzPredecessor then
  begin
    Key := TEzPredecessor(Source).Key;
    Relation := TEzPredecessor(Source).Relation;
    Lag := TEzPredecessor(Source).Lag;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzPredecessors
//=----------------------------------------------------------------------------=
function TEzPredecessors.GetItem(Index: Integer): TEzPredecessor;
begin
  Result := TEzPredecessor(inherited GetItem(Index));
end;

function TEzPredecessors.GetText: string;
var
  i: Integer;
  P: TEzPredecessor;

begin
  Result := '';
  for i:=0 to Count-1 do
  begin
    P := Items[i];
    if Result <> '' then
      Result := Result + ',';
    Result := Result + IntToStr(P.Key.Value);
    if P.Relation <> prFinishStart then
    begin
      case P.Relation of
        prSameStart:
          Result := Result + ':S+';
        prStartStart:
          Result := Result + ':SS';
        prStartFinish:
          Result := Result + ':SF';
        prFinishFinish:
          Result := Result + ':FF';
      end;
    end;

    if P.Lag <> 0.0 then
      Result := Result + ':' + FloatToStr(P.Lag)
  end;
end;

function TEzPredecessors.IndexOf(Key: TNodeKey): integer;
begin
  Result := 0;
  while (Result<Count) and (CompareNodeKeys(Items[Result].Key, Key) <> 0) do
    inc(Result);
  if Result = Count then
    Result := -1;
end;

procedure TEzPredecessors.SetItem(Index: Integer; Value: TEzPredecessor);
begin
  inherited SetItem(Index, Value);
end;

procedure TEzPredecessors.SetText(Value: string);
const
  RelationChars = ['f', 'F', 's', 'S'];
var
  sl: TStringList;
  i, p1, p2: Integer;
  P: TEzPredecessor;
  val: string;

  function TranslateRelation(Value: string): TEzPredecessorRelation;
  begin
    Value := UpperCase(Value);
    if Value = 'FS' then
      Result := prFinishStart
    else if Value = 'S+' then
      Result := prSameStart
    else if Value = 'SS' then
      Result := prStartStart
    else if Value = 'SF' then
      Result := prStartFinish
    else if Value = 'FF' then
      Result := prFinishFinish
    else
    begin
      Result := prFinishStart;
      EzScheduleError(SInvalidPredecessorRelation);
    end;
  end;

begin
  Clear;
  if Value = '' then Exit;
  sl := TStringList.Create;
  try
    SplitString(Value, sl);
    for i:=0 to sl.count-1 do
    begin
      P := TEzPredecessor.Create;
      val := sl[i];
      p1 := Pos(':', val);
      if p1 <> 0 then
        p2 := PosEx(':', val, p1+1) else
        p2 := 0;

      if p1 = 0 then
      begin
        P.Key.Value := StrToInt(val);
        P.Key.Level := -1;
        P.Relation := prFinishStart;
        P.Lag := 0.0;
      end
      else if p2 = 0 then
      begin
        P.Key.Value := StrToInt(Copy(val, 1, p1-1));
        P.Key.Level := -1;

        if (length(val)-p1 = 2) and
        {$IFDEF EZ_D2009}
          CharInSet(val[p1+1], RelationChars) and CharInSet(val[p1+2], RelationChars)
        {$ELSE}
          (val[p1+1] in RelationChars) and (val[p1+2] in RelationChars)
        {$ENDIF}
        then
        begin
          P.Relation := TranslateRelation(Copy(Val, p1+1,MaxInt));
          P.Lag := 0.0;
        end
        else
        begin
          P.Relation := prFinishStart;
          P.Lag := SaveStrToFloat(Copy(val,p1+1,MaxInt));
        end;
      end
      else
      begin
        P.Key.Value := StrToInt(Copy(val, 1, p1-1));
        P.Key.Level := -1;
        P.Relation := TranslateRelation(Copy(Val, p1+1,p2-p1-1));
        P.Lag := SaveStrToFloat(Copy(val,p2+1,MaxInt));
      end;
      Add(P);
    end;
  finally
    sl.Destroy;
  end;
end;

//=----------------------------------------------------------------------------=
// TEzPredecessorSuccessors
//=----------------------------------------------------------------------------=
constructor TEzPredecessorSuccessor.Create(APred, ASucc: TEzScheduleTask);
begin
  inherited Create;
  Predecessor := APred;
  Successor := ASucc;
end;

procedure TEzPredecessorSuccessors.AddRelation(APred, ASucc: TEzScheduleTask);
begin
  Add(TEzPredecessorSuccessor.Create(APred, ASucc));
end;

function TEzPredecessorSuccessors.CompareItems(Item1, Item2: TObject): Integer;
var
  R1, R2: TEzPredecessorSuccessor;
begin
  R1 := TEzPredecessorSuccessor(Item1);
  R2 := TEzPredecessorSuccessor(Item2);
  Result := CompareNodeKeys(R1.Predecessor.Key, R2.Predecessor.Key);
  if Result = 0 then
    Result := CompareNodeKeys(R1.Successor.Key, R2.Successor.Key);
end;

function TEzPredecessorSuccessors.GetItem(Index: Integer): TEzPredecessorSuccessor;
begin
  Result := TEzPredecessorSuccessor(inherited Items[Index]);
end;

procedure TEzPredecessorSuccessors.SetItem(Index: Integer; Value: TEzPredecessorSuccessor);
begin
  inherited Items[Index] := Value;
end;

function TEzPredecessorSuccessors.RelationExists(APred, ASucc: TEzScheduleTask): Boolean;
var
  Search: TEzPredecessorSuccessor;
begin
  Search := TEzPredecessorSuccessor.Create(APred, ASucc);
  try
    Result := IndexOf(Search)<>-1;
  finally
    Search.Destroy;
  end;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleSuccessor
//=----------------------------------------------------------------------------=
procedure TEzScheduleSuccessor.Assign(Source: TPersistent);
begin
  if Source is TEzScheduleSuccessor then
  begin
    Task := TEzScheduleSuccessor(Source).Task;
    Relation := TEzScheduleSuccessor(Source).Relation;
    Lag := TEzScheduleSuccessor(Source).Lag;
  end else
    inherited;
end;

//=----------------------------------------------------------------------------=
// TEzScheduleSuccessors
//=----------------------------------------------------------------------------=
function TEzScheduleSuccessors.GetItem(Index: Integer): TEzScheduleSuccessor;
begin
  Result := TEzScheduleSuccessor(inherited GetItem(Index));
end;

procedure TEzScheduleSuccessors.SetItem(Index: Integer; Value: TEzScheduleSuccessor);
begin
  inherited SetItem(Index, Value);
end;

constructor TEzScheduleMessage.Create;
begin
  TaskKey.Value := Null;
  MessageText := '';
  Severity := msWarning;
  ResourceKey := Null;
  ScheduleWindowStart := 0;
  ScheduleWindowEnd := 0;
  ScheduleWindowStartTaskKey.Level := -1;
  ScheduleWindowStartTaskKey.Value := Null;
  ScheduleWindowEndTaskKey.Level := -1;
  ScheduleWindowEndTaskKey.Value := Null;
end;

constructor TEzScheduleMessage.Create(Key: TNodeKey; AMessage: string; ASeverity: TMessageSeverity=msWarning);
begin
  Create;

  TaskKey := Key;
  MessageText := AMessage;
  Severity := ASeverity;
end;

procedure TEzScheduleMessage.Assign(Source: TEzScheduleMessage);
begin
  TaskKey := Source.TaskKey;
  MessageText := Source.MessageText;
  Severity := Source.Severity;
  ResourceKey := Source.ResourceKey;
  ScheduleWindowStart := Source.ScheduleWindowStart;
  ScheduleWindowEnd := Source.ScheduleWindowEnd;
  ScheduleWindowStartTaskKey := Source.ScheduleWindowStartTaskKey;
  ScheduleWindowEndTaskKey := Source.ScheduleWindowEndTaskKey;
  StartBoundaryType := Source.StartBoundaryType;
  EndBoundaryType := Source.EndBoundaryType;  
end;

//=----------------------------------------------------------------------------=
// TEzMessageList
//=----------------------------------------------------------------------------=
constructor TEzScheduleMessages.Create;
begin
  inherited Create(True);
  OwnsObjects := True;
end;

function TEzScheduleMessages.GetItem(Index: Integer): TEzScheduleMessage;
begin
  Result := TEzScheduleMessage(inherited Items[Index]);
end;

function TEzResources.CompareItems(Item1, Item2: TObject): Integer;
var
  i1: TEzResource;
  i2: TEzResource;

begin
  i1 := TEzResource(Item1);
  i2 := TEzResource(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzResources.GetItem(Index: Integer): TEzResource;
begin
  Result := TEzResource(inherited Items[Index]);
end;

function TEzResources.ResourceExists(Key: Variant): Boolean;
var
  Find: TEzResource;

begin
  Find := TEzResource.Create;
  try
    Find.FKey := Key;
    if IndexOf(Find) = -1 then
      Result := False else
      Result := True;
  finally
    Find.Destroy;
  end;
end;

function TEzResources.ResourceByKey(Key: Variant): TEzResource;
var
  Find: TEzResource;
  i: Integer;

begin
  Find := TEzResource.Create;
  try
    Find.FKey := Key;
    if not IndexOf(i, Find) then
      EzScheduleError(Format(SResourceNotFound, [SafeString(Key)]));
    Result := Items[i];
  finally
    Find.Destroy;
  end;
end;

constructor TEzScheduleThread.Create(AScheduler: TEzScheduler);
begin
  Scheduler := AScheduler;
  inherited Create(False {Run immediately});
end;

procedure TEzScheduleThread.Execute();
begin
  Scheduler.Run;
end;

constructor TEzScheduler.Create(AOwner: TComponent);
begin
  inherited;
  FDefaultTaskScheduleService := TTaskScheduleService.Create(Self);
  FTaskCursor := TEzDBCursor.Create(nil);
  FTaskCursor.Options := FTaskCursor.Options + [cpAutoCreateParents, cpShowAll];
  FScheduleTasks := TEzSortedTaskList.Create;
  FPriorityStack := TEzObjectStack.Create(False);
  FTrackPath := TEzPredecessorSuccessors.Create(True);
  FPredecessorStack := TEzObjectStack.Create(False);
  FSuccessorStack := TEzObjectStack.Create(False);
  FScheduleMessages := TEzScheduleMessages.Create;
  FResources := TEzResources.Create(True);
  FCapacityResources := TEzCapacityResourceList.Create(True);
  FScheduleIntervals := TEzTimespanArray.Create;
  FUseWorkingPeriods := False;
  FReturnWorkIntervals := False;
end;

destructor TEzScheduler.Destroy;
begin
  // Reset will free-up memory associated with tasks etc.
  Reset;

  inherited;
  FTaskCursor.Free;
  FScheduleTasks.Free;
  FPriorityStack.Free;
  FTrackPath.Free;
  FPredecessorStack.Free;
  FSuccessorStack.Free;
  FScheduleMessages.Free;
  FResources.Free;
  FCapacityResources.Free;
  FScheduleIntervals.Free;
end;

procedure TEzScheduler.AddScheduleMessage(AMessage: TEzScheduleMessage);
begin
  FScheduleMessages.Add(AMessage);
end;

procedure TEzScheduler.AddScheduleMessage(Task: TEzScheduleTask; AMessage: string);
begin
  FScheduleMessages.Add(CreateScheduleMessage(Task, Null, AMessage));
end;

procedure TEzScheduler.AddScheduleMessage(Task: TEzScheduleTask; ResourceKey: Variant; AMessage: string);
begin
  FScheduleMessages.Add(CreateScheduleMessage(Task, ResourceKey, AMessage));
end;

function TEzScheduler.CreateScheduleMessage(Task: TEzScheduleTask; ResourceKey: Variant; AMessage: string) : TEzScheduleMessage;
var
  SMessage: TEzScheduleMessage;
begin
  SMessage := TEzScheduleMessage.Create(Task.Key, AMessage);

  SMessage.ResourceKey := ResourceKey;
  SMessage.ScheduleWindowStart := EzDateTimeToDateTime(Task.ScheduleData.Window.Start);
  SMessage.ScheduleWindowEnd := EzDateTimeToDateTime(Task.ScheduleData.Window.Stop);
  if Task.ScheduleData.LeftTask<>nil then
    SMessage.ScheduleWindowStartTaskKey := Task.ScheduleData.LeftTask.Key;
  if Task.ScheduleData.RightTask<>nil then
    SMessage.ScheduleWindowEndTaskKey := Task.ScheduleData.RightTask.Key;
  SMessage.StartBoundaryType := Task.ScheduleData.LeftBoundaryType;
  SMessage.EndBoundaryType := Task.ScheduleData.RightBoundaryType;

  Result := SMessage;
end;

procedure TEzScheduler.AddTask(Task: TEzScheduleTask);
var
  Node: TRecordNode;

begin
  if FTaskCursor.FindNode(Task.Key) <> FTaskCursor.EndNode then
    EzScheduleError(Format(SDuplicateTask, [IntToStr(Task.Key.Level) + ':' + SafeString(Task.Key.Value)]));

  Node := FTaskCursor.Add(Task.Parent, Task.Key, [], True);
  Node.Data := Task;
end;

function TEzScheduler.FindTask(Key: TNodeKey): TEzScheduleTask;
var
  Node: TRecordNode;
begin
  Node := FTaskCursor.FindNode(Key);
  if Node = FTaskCursor.EndNode then
    Result := nil else
    Result := Node.Data;
end;

procedure TEzScheduler.RemoveTask(Task: TEzScheduleTask);
var
  Node: TRecordNode;
begin
  Node := FTaskCursor.FindNode(Task.Key);
  if Node<>FTaskCursor.EndNode then
  begin
    FTaskCursor.SortedKeys.DeleteKey(Node.Key);
    FTaskCursor.CutOffNode(Node);
  end;
end;

procedure TEzScheduler.BuildResourceCapacityList;
var
  Find: TEzCapacityResourceListItem;
  i, n, AIndex: Integer;
begin
  Find := TEzCapacityResourceListItem.Create(Null);
  try
    for i:=0 to Resources.Count-1 do
    begin
      for n:=0 to Resources[i].Capacities.Count-1 do
      begin
        // Check if this capacity key already exists
        Find.Key := Resources[i].Capacities[n].Key;
        AIndex := FCapacityResources.IndexOf(Find);
        if AIndex=-1 then
          AIndex := FCapacityResources.Add(TEzCapacityResourceListItem.Create(Find.Key));

        // Add resource to this capacity
        FCapacityResources[AIndex].Resources.Add(Resources[i]);
      end;
    end;
  finally
    Find.Free;
  end;
end;

procedure TEzScheduler.BuildScheduleList;
  //
  // Add all tasks in reverse order.
  //
begin
  if FTaskCursor.IsEmpty then Exit;
  FTaskCursor.MoveLast;

  while not FTaskCursor.Bof do
  begin
    if not FTaskCursor.HasChildren then
      StoreScheduleTask(FTaskCursor.CurrentNode);
    FTaskCursor.MovePrevious;
  end;
end;

procedure TEzScheduler.CalculateSlackWindow(Task: TEzScheduleTask; TaskWindow: TEzTimeSpan);
var
  Int: TEzScheduleInterval;
  Extends: TEzTimeSpan;

begin
  Int := TEzScheduleInterval.Create;
  try
    Int.Assign(Task.Intervals[0]);
    Int.Duration := Task.Slack;
    Int.IsSlackInterval := True;

    // Update schedule window
    Extends := GetExtendsFromIntervals(Task.Intervals);
    if Task.ScheduleData.Direction=sdForwards then
      Task.ScheduleData.Window.Start := Extends.Stop else
      Task.ScheduleData.Window.Stop := Extends.Start;

    // If no resources assigned or when indicated by the user,
    // schedule task against projects calendar
    if (Task.Resources.Count=0) or (int.ScheduleFlag = ifProjectCalendar) then
      ScheduleToProjectCalendar(Task.Key.Value, Task, Int, TaskWindow)
    else
    begin
      ScheduleToResourceProfile(  FDefaultTaskScheduleService,
                                  Task.Key.Value,
                                  Task,
                                  Int,
                                  TaskWindow);

      FDefaultTaskScheduleService.Clear;
    end;

    // Slack window runs from star/stop of task to scheduled end/start
    TaskWindow.Start := min(Extends.Start, TaskWindow.Start);
    TaskWindow.Stop := max(Extends.Stop, TaskWindow.Stop);

    if not (srSlackScheduleFailed in Task.Flags) and
      (
           (DatetimeToEzDatetime(Task.SlackWindowStart)<>TaskWindow.Start) or
           (DatetimeToEzDatetime(Task.SlackWindowStop)<>TaskWindow.Stop)
      )
    then
    begin
      Task.SlackWindowStart := EzDatetimeToDatetime(TaskWindow.Start);
      Task.SlackWindowStop := EzDatetimeToDatetime(TaskWindow.Stop);
      Include(Task.Flags, srSlackWindowChanged);
    end;
  finally
    Int.Free;
  end;
end;

procedure TEzScheduler.CheckScheduleResult(Task: TEzScheduleTask; Window: TEzTimeSpan);
begin

end;

procedure TEzScheduler.FillSuccessors;
var
  i, p: Integer;
  Task, Predecessor: TEzScheduleTask;
  Node: TRecordNode;
  PredecessorData: TEzPredecessor;
  Successor: TEzScheduleSuccessor;


begin
  for i:=0 to FScheduleTasks.Count-1 do
  begin
    Task := FScheduleTasks[i];
    for p:=0 to Task.Predecessors.Count-1 do
    begin
      PredecessorData := Task.Predecessors[p];
      Node := FTaskCursor.FindNode(PredecessorData.Key);
      if Node = FTaskCursor.EndNode then
      begin
        AddScheduleMessage(Task, Format(SPredecessorTaskNotFound, [SafeString(PredecessorData.Key.Value)]));
        continue;
      end;
      Predecessor := Node.Data;
      Successor := TEzScheduleSuccessor.Create;
      Successor.Task := Task;
      Successor.Relation := PredecessorData.Relation;
      Successor.Lag := PredecessorData.Lag;
      Predecessor.ScheduleData.Successors.Add(Successor);
    end;
  end;
end;

function TEzScheduler.GetNextScheduledTask(var Task: TEzScheduleTask): Boolean;
begin
  Result := False;
  if FTaskCursor.IsEmpty then Exit;

  if (Task = nil) then
    FTaskCursor.MoveFirst else
    FTaskCursor.FindNode(Task.Key);

  if FTaskCursor.Eof then
    EzScheduleError(STaskNotFound, Self);

  repeat
    if Task <> nil then
    begin
      FTaskCursor.MoveNext;
      if FTaskCursor.Eof then Exit;
    end;
    Task := FTaskCursor.CurrentNode.Data;
  until ((Task.Flags*[srIntervaldataChanged, srSlackWindowChanged])<>[]);

  Result := True;
end;

function TEzScheduler.GetProjectProfile: TEzAvailabilityProfile;
begin
  if FProjectProfile=nil then
    EzScheduleError(SProjectAvailabilityNotSet, Self);
  Result := FProjectProfile;
end;

function TEzScheduler.GetTaskWindow(Task: TEzScheduleTask) : TEzTimeSpan;
var
  i: Integer;

begin
  if Task.WindowStart<>0 then
  begin
    Result.Start := DateTimeToEzDatetime(Task.WindowStart);
    Result.Stop := DateTimeToEzDatetime(Task.WindowStop);
  end

  else
  begin
    Result.Start := END_OF_TIME;
    Result.Stop := 0;

    for i:=0 to Task.Intervals.Count-1 do
    begin
      Result.Start := min(Result.Start, DatetimeToEzDatetime(Task.Intervals[i].Start));
      Result.Stop := max(Result.Stop, DatetimeToEzDatetime(Task.Intervals[i].Stop));
    end;
  end;
end;

procedure TEzScheduler.UpdateTaskScheduleDirection(Task: TEzScheduleTask);

  procedure InvestigatePredecessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Preds: TEzPredecessors;
    Pred: TEzScheduleTask;
    i_pred: Integer;

  begin
    Preds := CurrentTask.Predecessors;
    i_pred := Preds.Count-1;

    while i_pred>=0 do
    begin
      Pred := FTaskCursor.FindNode(Preds[i_pred].Key).Data;

      //
      // Check for recursive predecessor relations
      //
      if FTrackPath.RelationExists(Pred, CurrentTask) then
      begin
        dec(i_pred);
        continue;
        // EzScheduleError(Format(SCircularRelation, [SafeString(Pred.Key.Value), SafeString(CurrentTask.Key.Value)]));
      end;
      FTrackPath.AddRelation(Pred, CurrentTask);

      if IsFixedPoint(Pred, sdBackwards) then
      begin
        Task.ScheduleData.Direction := sdForwards;
        Exit;
      end;

      InvestigatePredecessors(Pred, True);

      dec(i_pred);
    end;
  end;

  //
  // Investigate successors
  //
  procedure InvestigateSuccessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Succs: TEzScheduleSuccessors;
    Successor: TEzScheduleSuccessor;
    i_succ: Integer;

  begin
    i_succ := CurrentTask.ScheduleData.Successors.Count-1;
    Succs := CurrentTask.ScheduleData.Successors;

    while i_succ>=0 do
    begin
      Successor := Succs[i_succ];

      if IsFixedPoint(Successor.Task, sdForwards) then
      begin
        Task.ScheduleData.Direction := sdBackwards;
        Exit;
      end;

      InvestigateSuccessors(Successor.Task, True);

      dec(i_succ);
    end;
  end;

begin
  if Task.ScheduleData.Direction in [sdForwards, sdBackwards] then Exit;
  FTrackPath.Clear;
  InvestigatePredecessors(Task, False);
  if Task.ScheduleData.Direction in [sdForwards, sdBackwards] then Exit;
  InvestigateSuccessors(Task, False);
  if not (Task.ScheduleData.Direction in [sdForwards, sdBackwards]) then
    Task.ScheduleData.Direction := sdForwards;
end;

function TEzScheduler.GetTaskScheduleData(
  Task: TEzScheduleTask;
  ScheduleWithPriority: Boolean;
  PriorityStack: TEzObjectStack) : Boolean;

var
  HasUnscheduledSuccessors,
  HasUnscheduledPredecessors, HasScheduledPredecessors: Boolean;
  HasSuccessorFixedPoint: Boolean;
  TaskIsFixedPoint: Boolean;
  i: Integer;


  procedure InvestigatePredecessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Preds: TEzPredecessors;
    Pred: TEzScheduleTask;
    i_pred: Integer;

  begin
    Preds := CurrentTask.Predecessors;
    i_pred := Preds.Count-1;

    while i_pred>=0 do
    begin
      Pred := FTaskCursor.FindNode(Preds[i_pred].Key).Data;

      //
      // Check for recursive predecessor relations
      //
      if FTrackPath.RelationExists(Pred, CurrentTask) then
        EzScheduleError(Format(SCircularRelation, [SafeString(Pred.Key.Value), SafeString(CurrentTask.Key.Value)]), Self);
      FTrackPath.AddRelation(Pred, CurrentTask);

      // Handle taskwindow of predecessor
      if Pred.Slack>0 then
        updateWindowWithPredecessorSlackWindow(Pred, Preds[i_pred], Task) else
        updateWindowWithPredecessorTaskWindow(Pred, Preds[i_pred], Task);

      if not (srScheduledFailed in Pred.Flags) then
      begin
        if srScheduledOK in Pred.Flags then
        begin
          //
          // If there is a scheduled predecessor, then there's no need
          // to investigate successors of task. The task can simply
          // be scheduled against it's predecessor(s).
          //
          HasScheduledPredecessors := True;

          //
          // no need to update schedule window when there still are
          // unscheduled predecessors (these need scheduling first anyway)
          // or when this procedure is recursively called
          //
          if not HasUnscheduledPredecessors and not IsNested then
            updateWindowWithPredecessor(Pred, Preds[i_pred], Task);
        end

        else if not TaskIsFixedPoint then
          //
          // An unscheduled predecessor exists and this needs scheduling first
          //
        begin
          HasUnscheduledPredecessors := true;

          if FPredecessorStack.IndexOf(Pred) = -1 {Prevent double checking of the same predecessor} then
          begin
            //
            //  push predecessor into queue so it will be scheduled before current task.
            //
            FPredecessorStack.Push(Pred);

            //
            // Load predecessors of predecessor
            //
            InvestigatePredecessors(Pred, True);
          end;
        end;
      end; // if not (srScheduledFailed in Pred.Flags) then

      dec(i_pred);
    end;
  end;

  //
  // Investigate successors
  //
  procedure InvestigateSuccessors(CurrentTask: TEzScheduleTask; IsNested: Boolean);
  var
    Succs: TEzScheduleSuccessors;
    Successor: TEzScheduleSuccessor;
    i_succ: Integer;

  begin
    i_succ := CurrentTask.ScheduleData.Successors.Count-1;
    Succs := CurrentTask.ScheduleData.Successors;

    while i_succ>=0 do
    begin
      Successor := Succs[i_succ];

      // Handle taskwindow of successor
      if Successor.Task.Slack>0 then
        updateWindowWithSuccessorSlackWindow(Successor, CurrentTask) else
        updateWindowWithSuccessorTaskWindow(Successor, CurrentTask);

      if not (srScheduledFailed in Successor.Task.Flags) then
      begin
        if srScheduledOK in Successor.Task.Flags then
        begin
          //
          // no need to update schedule window when there still are
          // unscheduled successors (these need scheduling first anyway)
          // or when this procedure is recursively called
          //
          // KV: 11 feb 2008
          // Check for HasUnscheduledSuccessors removed
          if {not HasUnscheduledSuccessors and} not IsNested then
            updateWindowWithSuccessor(Successor, CurrentTask);

          if Successor.Task.ScheduleData.Direction = sdBackwards then
          begin
            CurrentTask.ScheduleData.Direction := sdBackwards;
            HasSuccessorFixedPoint := True;
          end;
        end
        else if not TaskIsFixedPoint

        // KV: 12 June 2008
        // Check removed. We allways look at the successors regardless
        // of any scheduled predecessor.
        // This solves the problem if in a group of tasks one predecessor
        // has a MSO constraint and a successor has a FAB constraint.
        {and not HasScheduledPredecessors}


        then
          //
          // if a task is a fixed point itself, or when scheduled predecessors
          // exist, then we don't care about unscheduled successors. The task can
          // simply be scheduled against it's predecessor(s) or the fixed point.
          //
          // Unscheduled successor exists and need handling first.
          //
        begin
          HasUnscheduledSuccessors := True;

          if Successor.Task.Constraint in [cnMFO, cnFAB] then
          begin
            HasSuccessorFixedPoint := True;
            // KV: 23 may 2008
            // Next line added:
            // If current chain ends with a fixed successors, we
            // keep track of all successors in between.
            // These task need scheduling before current task.
            FSuccessorStack.Push(Successor.Task);
          end

          else if FSuccessorStack.IndexOf(Successor.Task) = -1 {Prevent double checking of successor} then
          begin
            InvestigateSuccessors(Successor.Task, True);

            // KV: 11 feb 2008
            // If current chain ends with a fixed successors, we
            // keep track of all successors in between.
            // These task need scheduling before current task.
            if HasSuccessorFixedPoint then
              FSuccessorStack.Push(Successor.Task);

            if Successor.Task.ScheduleData.Direction = sdBackwards then
              CurrentTask.ScheduleData.Direction := sdBackwards;
          end;
        end;
      end;
      dec(i_succ);
    end;
  end;

begin
  Result := True;
	HasUnscheduledPredecessors := false;
	HasUnscheduledSuccessors := false;
  HasSuccessorFixedPoint := False;
  HasScheduledPredecessors := False;

  FTrackPath.Clear;
  FSuccessorStack.Clear;
  FPredecessorStack.Clear;
  TaskIsFixedPoint := IsFixedPoint(Task, sdForwards);
  InvestigatePredecessors(Task, False);
//  if not HasScheduledPredecessors then
    //
    // If a task has scheduled predecessors then there is no need to
    // investigate the successors since this task can simply be scheduled
    // against it's predecessors.
    //
  begin
    TaskIsFixedPoint := IsFixedPoint(Task, sdBackwards);
    InvestigateSuccessors(Task, False);
  end;

  if HasSuccessorFixedPoint then
    //
    // There is a fixed point successor ==> we need to schedule successor first
    // and then schedule this task backwards against these successors.
    //
  begin
    Task.ScheduleData.Direction := sdBackwards;

    if FSuccessorStack.Count>0 then
    begin
      PriorityStack.Add(Task);
      for i:=FSuccessorStack.Count-1 downto 0 do
        PriorityStack.Add(FSuccessorStack[i]);

      // Cannot continue scheduling, schedule tasks from priority queue
      // first
      Result := False;
    end;
  end

  else if HasUnscheduledPredecessors then
  begin
    if FPredecessorStack.Count>0 then
    begin
      PriorityStack.Add(Task);
      for i:=0 to FPredecessorStack.Count-1 do
        PriorityStack.Add(FPredecessorStack[i]);
      // Cannot continue scheduling, schedule tasks from priority queue
      // first
      Result := False;
    end;
  end;
end;

{
function TEzScheduler.GetTaskScheduleData(Task: TEzScheduleTask; ScheduleWithPriority: Boolean) : Boolean;
var
  PredecessorsRequired, hasUnscheduledPredecessor: Boolean;
	i_succ, i_pred, Stop: Integer;
  Succs: TEzScheduleSuccessors;
  Preds: TEzPredecessors;
  Pred: TEzScheduleTask;
  Successor: TEzScheduleSuccessor;
  TrackPath: TEzPredecessorSuccessors;
  SuccessorTask: TEzScheduleTask;
  TaskIsPushed: Boolean;

begin
  Result := False;
  TaskIsPushed := False;

  // Controls whether predecessors can be fixed points to schedule against
  PredecessorsRequired := true;
	hasUnscheduledPredecessor := false;

  TrackPath := TEzPredecessorSuccessors.Create(True);
  try
    //
    // Investigate successors
    //
    i_succ := 0;
    Succs := Task.ScheduleData.Successors;
    Stop := Succs.Count;

    while not TaskIsPushed and (i_succ < Stop) do
    begin
      Successor := Succs[i_succ];
      if srScheduledOK in Successor.Task.Flags then
      begin
        // Successors act as fixed point.
        // predecessors may therefore be ignored.
        PredecessorsRequired := false;
        updateWindowWithSuccessor(Successor, Task);
      end

    	//
  		//	if this task, or one of it's successors, is a fixed point then push this task
	 		//  as a unscheduled successor.
  		//
	 		else if not ScheduleWithPriority and
             (IsFixedPoint(Successor.Task, sdBackwards) or locateFixedPoint(Successor.Task)) then
      begin
        // By adding successor to the route queue before this task, we ensure
        // that the successor will be scheduled first.
        if not TaskIsPushed then
        begin
          TaskIsPushed := True;
          FPriorityStack.Push(Task);
        end;
  			FPriorityStack.Push(Successor.Task);
      end;
      inc(i_succ);
    end;

    // If any successor was added, schedule them first before continueing
  	if TaskIsPushed then Exit;

	  //
    //	update schedulewindow if current task is a fixed point itself.
    //
  	if IsFixedPoint(Task, sdForwards) then
    begin
      // This task is a fixed point itself.
      // predecessors may therefore be ignored.
	    PredecessorsRequired := false;
  		updateWindowWithTask(Task.ScheduleData, Task);
    end;

    //
    // Investigate predecessors
    //
    Preds := Task.Predecessors;
    SuccessorTask := Task;
    i_pred := 0;
    Stop := Preds.Count;

    while i_pred<Stop do
    begin
      Pred := FTaskCursor.FindNode(Preds[i_pred].Key).Data;

      if TrackPath.RelationExists(Pred, SuccessorTask) then
        EzScheduleError(Format(SCircularRelation, [string(Pred.Key.Value), string(SuccessorTask.Key.Value)]));
      TrackPath.AddRelation(pred, SuccessorTask);

      if srScheduledFailed in Pred.Flags then
        inc(i_pred)

      else if srScheduledOK in Pred.Flags then
      begin
        if not hasUnscheduledPredecessor then //no need to update window when there are unscheduled predecessors
          updateWindowWithPredecessor(Pred, Preds[i_pred], Task);
        inc(i_pred);
      end
      else if PredecessorsRequired then
        //
        // Predecessor needs scheduling first
        //
      begin
        hasUnscheduledPredecessor := true;

				//
				//  push current task into queue to ensure rescheduling after predecessors.
				//
        if not TaskIsPushed then
        begin
          TaskIsPushed := True;
          FPriorityStack.Push(Task);
        end;

				//
				//  push predecessor into queue so it will be scheduled before current task.
				//
				FPriorityStack.Push(Pred);

				//
				//  go to predecessor of predecessor
				//
        SuccessorTask := Pred;
        Preds := Pred.Predecessors;
        i_pred := 0;
        Stop := Preds.Count;
      end else
        inc(i_pred);
    end;

  	if hasUnscheduledPredecessor and PredecessorsRequired then
      Result := False else
	    Result := True;
  finally
    TrackPath.Destroy;
  end;
end;
}

function TEzScheduler.IsFixedPoint(Task: TEzScheduleTask; Direction: TEzScheduleDirection) : Boolean;
begin
  case direction of
    sdForwards:
	    Result := Task.Constraint in [cnDNL, cnMSO, cnMFO, cnFAB];
    sdBackwards:
      Result := Task.Constraint in [cnDNL, cnMSO, cnMFO, cnSNET];
  else
    Result := False;
  end;
end;

procedure TEzScheduler.CopyParentsResources(Task, Parent: TEzScheduleTask);
var
  i, Current: Integer;
  Res: TEzResourceAssignment;

  function CheckHasResource(Resource: TEzTaskAssignment): Boolean;
  var
    n: Integer;

  begin
    Result := False;
    for n:=0 to Current-1 do
      if (Task.Resources[n].Key = Resource.Key) and (Resource is TEzResourceAssignment) then
      begin
        Result := True;
        LoadResourceData(Resource as TEzResourceAssignment);
        AddScheduleMessage(Task, Format(SDuplicateResources, [Resources.ResourceByKey(Resource.Key).Name]));
      end;
  end;

begin
  if Parent.Resources.Count = 0 then Exit;

  Current := Task.Resources.Count;

  for i:=0 to Parent.Resources.Count-1 do
  begin
    if not CheckHasResource(Parent.Resources[i]) then
    begin
      Res := TEzResourceAssignment.Create;
      Res.Assign(Parent.Resources[i]);
      // Res.IsParentResource := True;
      Task.Resources.Add(Res);
    end;
  end;
end;

procedure TEzScheduler.CopyParentPredecessors(Task, Parent: TEzScheduleTask);
var
  i: Integer;
  p: TEzPredecessor;

begin
  if Parent.Predecessors.Count = 0 then Exit;

  for i:=0 to Parent.Predecessors.Count-1 do
  begin
    if Task.Predecessors.IndexOf(Parent.Predecessors[i].Key) = -1 then
    begin
      p := TEzPredecessor.Create;
      p.Assign(Parent.Predecessors[i]);
      Task.Predecessors.Add(p);
    end;
  end;
end;

procedure TEzScheduler.DoLoadResourceData(Resource: TEzResource);
begin
  if Assigned(FOnLoadResourceData) then
    FOnLoadResourceData(Self, Resource);
  if not Assigned(Resource.Availability) then
    EzScheduleError(Format(SCouldNotLoadAvailability, [SafeString(Resource.Key)]), Self);
end;

function TEzScheduler.DoTestResourceCapacity(
  Task: TEzScheduleTask;
  Capacity: TEzCapacityAssignment;
  Resource: TEzResource): Boolean;
begin
  Result := True;
  if Assigned(FOnTestResourceCapacity) then
    FOnTestResourceCapacity(Task, Capacity, Resource, Result);
end;

procedure TEzScheduler.LoadResourceData(Resource: TEzResourceAssignment);
var
  res: TEzResource;

begin
  if not FResources.ResourceExists(Resource.Key) then
  begin
    res := TEzResource.Create(Self, Resource);
    FResources.Add(res);
  end;
end;

procedure TEzScheduler.NormalizePredecessors(Task: TEzScheduleTask);
  //
  // Convert predecessor relations to a parent task into predecessor relations
  // to all child tasks of that parent.
  //
var
  p, childs: Integer;
  PredecessorData: TEzPredecessor;
  PredecessorTask: TEzScheduleTask;
  TaskNode, SavedPos, Node: TRecordNode;
  NewPredecessor: TEzPredecessor;
  NewPredecessors: TEzPredecessors;

begin
  if Task.Predecessors.Count=0 then Exit;

  SavedPos := FTaskCursor.CurrentNode;
  NewPredecessors := TEzPredecessors.Create(true {will be reset later!!!});
  try
    p:=0;
    while p<Task.Predecessors.Count do
    begin
      PredecessorData := Task.Predecessors[p];
      Node := FTaskCursor.FindNode(PredecessorData.Key);

      if Node = FTaskCursor.EndNode then
      begin
        AddScheduleMessage(Task, Format(SPredecessorTaskNotFound, [SafeString(PredecessorData.Key.Value)]));
        Task.Predecessors.Delete(p);
//        inc(p);
        continue;
      end;

      TaskNode := FTaskCursor.FindNode(Task.Key);
      if FTaskCursor.NodeHasParent(TaskNode, Node) then
      begin
        AddScheduleMessage(Task, Format(SPredecessorRelationInvalid, [SafeString(PredecessorData.Key.Value), SafeString(Task.Key.Value)]));
        Task.Predecessors.Delete(p);
        continue;
      end;

      if FTaskCursor.HasChildren(Node) then
        //
        // Predecessor is a parent ==> convert parent links into child links
        //
      begin
        FTaskCursor.CurrentNode := Node.Child;
        for childs:=1 to FTaskCursor.Childs(Node) do
        begin
          if not FTaskCursor.HasChildren then
          begin
            PredecessorTask := FTaskCursor.CurrentNode.Data;
            NewPredecessor := TEzPredecessor.Create;
            NewPredecessor.Key := PredecessorTask.Key;
            NewPredecessor.Relation := PredecessorData.Relation;
            NewPredecessor.Lag := PredecessorData.Lag;
            NewPredecessors.Add(NewPredecessor);
          end;
          FTaskCursor.MoveNext;
        end;

        // Remove predecessor relation to parent record.
        Task.Predecessors.Delete(p);
      end else
        inc(p);
    end;

    //
    // insert new relations to predecessor list of task
    //
    NewPredecessors.OwnsObjects := False;
    p:=0;
    while p<NewPredecessors.Count do
    begin
      if Task.Predecessors.IndexOf(NewPredecessors[p].Key) = -1 then
        Task.Predecessors.Add(NewPredecessors[p]);
      inc(p);
    end;
  finally
    FTaskCursor.CurrentNode := SavedPos;
    NewPredecessors.Destroy;
  end;
end;

procedure TEzScheduler.ProcessResources(Task: TEzScheduleTask);
var
  i: Integer;

begin
  Task.ScheduleData.MaxAssignement := 0;
  for i:=0 to Task.Resources.Count-1 do
  begin
    if (Task.Resources[i] is TEzResourceAssignment) then
      LoadResourceData(Task.Resources[i] as TEzResourceAssignment);
    Task.ScheduleData.MaxAssignement := max(Task.ScheduleData.MaxAssignement, Task.Resources[i].Assigned);
  end;
end;

procedure TEzScheduler.Reset(ResetAll: Boolean = True);
var
  i: Integer;
  Task: TEzScheduleTask;

begin
  FScheduleResults := [];

  // empty task list (FScheduleTasks does not own objects and therefore memory must be freed manually)
  FScheduleTasks.Clear;
  FScheduleMessages.Clear;
  FProjectWindowStart := 0;
  FProjectWindowStop := 0;
  FProjectWindowAbsoluteStart := MaxDouble;
  FProjectWindowAbsoluteStop := 0;
  FProjectMakeSpanStart := 0;
  FProjectMakeSpanStop := 0;

  FCapacityResources.Clear;

  if ResetAll then
  begin
    // Free memory allocated for tasks
    FTaskCursor.MoveFirst;
    while not FTaskCursor.Eof do
    begin
      TEzScheduleTask(FTaskCursor.CurrentNode.Data).Free;
      FTaskCursor.MoveNext;
    end;

    // Empty hierarchical cursor
    FTaskCursor.Clear;
    FResources.Clear;
  end
  else
  begin
    FTaskCursor.MoveFirst;
    while not FTaskCursor.Eof do
    begin
      Task := TEzScheduleTask(FTaskCursor.CurrentNode.Data);
      if Task<>nil then
        Task.Reset;
      FTaskCursor.MoveNext;
    end;

    for i:=0 to FResources.Count-1 do
    begin
      if FResources[i].ActualAvailability<>nil then
      begin
        FResources[i].ActualAvailability.Free;
        FResources[i].ActualAvailability := nil;
      end;
    end;
  end;
end;

procedure TEzScheduler.Reschedule;
begin
  Run;
end;

procedure TEzScheduler.Run;
begin
  if (ProjectWindowStart = 0) or (ProjectWindowStart >= ProjectWindowStop) then
    EzScheduleError(SInvalidProjectDates, Self);

  FProjectWindowAbsoluteStart := min(FProjectWindowAbsoluteStart, FProjectWindowStart);
  FProjectWindowAbsoluteStop := max(FProjectWindowAbsoluteStop, FProjectWindowStop);

  BuildResourceCapacityList;
  BuildScheduleList;
  FillSuccessors;
  ScheduleTasks;

  if Messages.Count>0 then
    Include(FScheduleResults, srScheduledWithErrors);
end;

procedure TEzScheduler.ScheduleTasks;
var
  i: Integer;
  Task: TEzScheduleTask;

begin
  for i:=0 to FScheduleTasks.Count-1 do
  begin
    Task := FScheduleTasks[i];

    ScheduleTask(Task.Key, Task, False);

    // Empty priority queue before scheduling next task
    while FPriorityStack.Count>0 do
    begin
      Task := TEzScheduleTask(FPriorityStack.Pop);
      ScheduleTask(Task.Key, Task, True);
    end;
  end;

  Include(FScheduleResults, srScheduledOK);
end;

procedure TEzScheduler.ScheduleTask(TaskKey: TNodeKey; Task: TEzScheduleTask; ScheduleWithPriority: Boolean);
var
  TaskWindow: TEzTimeSpan;

begin
  // Debugging code
  // This code is useless, however it prevents Delphi from removing
  // this parameter from code.
{$IFDEF DEBUG}
  if VarIsNull(TaskKey.Value) then;
{$ENDIF}

  if [srScheduledOK, srScheduledFailed]*Task.Flags<>[] {Task has already been scheduled} then
    Exit;

  if not PrepareScheduleTask(Task, ScheduleWithPriority) then
    Exit;

  //
  // Schedule window has been determined, continue with scheduling this task.
  //

  if ScheduleAsGroup(Task) then
    ScheduleTaskGroup(Task) else
    ScheduleTaskIntervals(  FDefaultTaskScheduleService,
                            Task,
                            TaskWindow);
end;

function TEzScheduler.PrepareScheduleTask(Task: TEzScheduleTask; ScheduleWithPriority: Boolean) : Boolean;
var
  TaskWindow: TEzTimeSpan;

begin
  Result := True; // Task can be scheduled

  Task.ScheduleData.Window.Start := DatetimeToEzDatetime(ProjectWindowStart);
  Task.ScheduleData.LeftBoundaryType := btProjectStartDate;
  Task.ScheduleData.Window.Stop := DatetimeToEzDatetime(ProjectWindowStop);
  Task.ScheduleData.RightBoundaryType := btProjectEndDate;

	//
	//	no need for scheduling a task having a Ignore constraint
	//  no need to allocate rescources too
	//
	if (Task.Constraint=cnIgnore) or IsMileStone(Task) then
  begin
    if Task.Constraint in [cnMFO, cnFAB] then
      Task.ScheduleData.Direction := sdBackwards else
      Task.ScheduleData.Direction := sdForwards;
    Include(Task.Flags, srScheduledOK);
    Result := False;
		Exit;
  end

  else if Task.Intervals.Count=0 then
    //
    // Task without intervals cannot be scheduled.
    //
  begin
    Include(Task.Flags, srScheduledFailed);
    AddScheduleMessage(Task, SNoIntervals);
    Result := False;
    Exit;
  end

	//
	//	no need for scheduling a task having a DNL constraint
	//
	else if Task.Constraint = cnDNL then
  begin
		UpdateTaskScheduleDirection(Task);
    Assert(Task.ScheduleData.Direction in [sdForwards, sdBackwards]);
    TaskWindow := GetTaskWindow(Task);
    ScheduleTask_DoNotLevel(Task);
		CheckScheduleResult(Task, TaskWindow);
    Result := False;
		Exit;
  end

	else if Task.Constraint <= cnMFO then
    UpdateWindowWithTask(Task.ScheduleData, Task)

  else
  begin
		if not GetTaskScheduleData(Task, ScheduleWithPriority, FPriorityStack) then
    begin
      //
      // Task cannot be scheduled because a predecessor or successor needs
      // scheduling first.
      //
      Result := False;
      Exit;
    end;

		if(Task.ScheduleData.Direction in [sdUndefined, sdDontCare]) then
      {
        KV: 8 april 2005, this line replaced by next 3 lines
            Task.ScheduleData.Direction := sdForwards;
      }
      if Task.Constraint in [cnMFO, cnFAB] then
        Task.ScheduleData.Direction := sdBackwards else
        Task.ScheduleData.Direction := sdForwards;

    UpdateWindowWithTask(Task.ScheduleData, Task)
  end;
end;

procedure TEzScheduler.ScheduleTaskIntervals(
  Service: ITaskScheduleService;
  Task: TEzScheduleTask;
  var TaskWindow: TEzTimeSpan);

var
  i_int: Integer;
  Int: TEzScheduleInterval;
  IntervaldataChanged: Boolean;

begin
  // Schedule each interval of task seperately
  i_int:=0;
  IntervaldataChanged := False;
  while not (srScheduledFailed in Task.Flags) and (i_int < Task.Intervals.Count) do
  begin
    int := Task.Intervals[i_int];

    // If no resources assigned or when indicated by the user,
    // schedule task against projects calendar
    if not Service.ContainsResourceAssigments and ((Task.Resources.Count=0) or (int.ScheduleFlag = ifProjectCalendar)) then
      ScheduleToProjectCalendar(Task.Key.Value, Task, Int, TaskWindow)
    else
    begin
      ScheduleToResourceProfile(  Service,
                                  Task.Key.Value,
                                  Task,
                                  Int,
                                  TaskWindow);

      Service.Clear;
    end;

    if (not (srScheduledFailed in Task.Flags)) and
       (
         (DatetimeToEzDatetime(Int.Start)<>TaskWindow.Start) or
         (DatetimeToEzDatetime(Int.Stop)<>TaskWindow.Stop)
       )
    then
    begin
      Int.Start := EzDatetimeToDatetime(TaskWindow.Start);
      Int.Stop := EzDatetimeToDatetime(TaskWindow.Stop);
      IntervaldataChanged := True;
      if FProjectMakeSpanStart = 0 then
        FProjectMakeSpanStart := Int.Start else
        FProjectMakeSpanStart := min(FProjectMakeSpanStart, Int.Start);

      if FProjectMakeSpanStop = 0 then
        FProjectMakeSpanStop := Int.Stop else
        FProjectMakeSpanStop := max(FProjectMakeSpanStop, Int.Stop);
    end;

    if not (srScheduledFailed in Task.Flags) and (i_int<Task.Intervals.Count-1) then
      //
      // If another interval requires scheduling, then update schedule window
      // with results. This prevents multiple intervals from overlapping.
      //
    begin
      if Task.ScheduleData.Direction = sdForwards then
        Task.ScheduleData.Window.Start := TaskWindow.Stop else
        Task.ScheduleData.Window.Stop := TaskWindow.Start;
    end;

    // Next interval
    inc(i_int);
  end;

  // Reserve slack
  if not (srScheduledFailed in Task.Flags) and (Task.Slack>0) then
    CalculateSlackWindow(Task, TaskWindow);

  if IntervaldataChanged then
    include(Task.Flags, srIntervaldataChanged);
end;

function TEzScheduler.ScheduleAsGroup(Task: TEzScheduleTask) : Boolean;
var
  parent: TEzScheduleTask;
  parentNode: TRecordNode;

begin
  Result := False;
  parentNode := FTaskCursor.FindNode(Task.Key).Parent;
  while parentNode <> nil do
  begin
    parent := parentNode.Data;
    if parent.Resources.Count > 0 then
    begin
      Result := True;
      Exit;
    end;

    parentNode := parentNode.Parent;
  end;
end;

procedure TEzScheduler.ScheduleTaskGroup(Task: TEzScheduleTask);

  procedure AddChildren(childNode: TRecordNode; branch: TScheduleBranch);
  var
    subBranch: TScheduleBranch;
    task, priorityTask: TEzScheduleTask;
    PriorityStack: TEzObjectStack;

  begin
    PriorityStack := TEzObjectStack.Create(False);
    try
      while childNode <> nil do
      begin
        task := childNode.Data;
        if (task.Constraint <> cnIgnore) and not (srScheduledFailed in task.Flags) then
        begin
          if not FTaskCursor.HasChildren(childNode) then
          begin
            if not GetTaskScheduleData(task, False, PriorityStack) then
              //
              // Other tasks need scheduling first
              //
            begin
              while PriorityStack.Count > 0 do
              begin
                priorityTask := TEzScheduleTask(PriorityStack.Pop);
                if (CompareNodeKeys(priorityTask.Parent, childNode.Parent.Key) = 0) and (branch.IndexOf(priorityTask) = -1) then
                  branch.Add(priorityTask);
              end;
            end
            else if branch.IndexOf(priorityTask) = -1 then
              branch.Add(task)
          end
          else
          begin
            subBranch := TScheduleBranch.Create;
            subBranch.Parent := task;
            branch.Add(subBranch);
            AddChildren(childNode.Child, subBranch);
          end;
        end;

        childNode := childNode.Next;
      end;
    finally
      PriorityStack.Free;
    end;
  end;

var
  branch: TScheduleBranch;
  d: TDateTime;
  parentNode: TRecordNode;
  parentWithResources: TRecordNode;
  parentData: TEzScheduleTask;
  taskNode: TRecordNode;
  service: IGroupScheduleService;
  Window: TEzTimespan;
  PrevailingDirection: TEzScheduleDirection;
  savedReturnWorkIntervals: Boolean;
  Succeeded: Boolean;

begin
  taskNode := FTaskCursor.FindNode(Task.Key);

  parentNode := taskNode.Parent;
  while parentNode <> nil do
  begin
    parentData := parentNode.Data;
    if parentData.Resources.Count > 0 then
      parentWithResources := parentNode;

    parentNode := parentNode.Parent;
  end;

  // Start scheduling at this node
  parentNode := parentWithResources;

  // Build an hierarchy with parent / tasks
  branch := TScheduleBranch.Create;
  try
    branch.Parent := parentNode.Data;
    AddChildren(parentNode.Child, branch);

    // No need to free!
    service := TGroupScheduleService.Create(Self, branch.Parent, nil);

    savedReturnWorkIntervals := FReturnWorkIntervals;
    FReturnWorkIntervals := False;

    Window.Start := 0;
    Window.Stop := 0;
    PrevailingDirection := sdUndefined;

    while not ScheduleHierarchy(  service,
                                  branch,
                                  False,
                                  sdUndefined,
                                  {var}Window,
                                  {var}PrevailingDirection,
                                  {var}Succeeded) do
    begin
      if Window.Start > Window.Stop then;
    end;

{$IFDEF DEBUG}
    d := EzDateTimeToDateTime(Window.Start);
    if d = 0 then;
    d := EzDateTimeToDateTime(Window.Stop);
    if d = 0 then;
{$ENDIF}

    if not Succeeded then
    begin
      service.SendScheduleMessages;
      Exit;
    end;

{$IFDEF DISSABLE_REVERSE_LOOP}
    if PrevailingDirection = sdForwards then
      // Sqeeze out any slack by scheduling this group in the oposite direction
    begin
      service.Restart;
      opositeWindow.Start := 0;
      opositeWindow.Stop := 0;

      if PrevailingDirection = sdForwards then
      begin
        ForceDirection := sdBackwards;
        opositeWindow.Stop := Window.Stop;
      end
      else
      begin
        ForceDirection := sdForwards;
        opositeWindow.Start := Window.Start;
      end;

      while not ScheduleHierarchy(  service,
                                    branch,
                                    True,
                                    ForceDirection,
                                    {var}opositeWindow,
                                    {var}PrevailingDirection,
                                    {var}Succeeded) do
      begin
        if (opositeWindow.Start > Window.Start) or (opositeWindow.Stop < Window.Stop) then
          Window := opositeWindow;
      end;

      if not Succeeded then
      begin
        service.SendScheduleMessages;
        Exit;
      end;
    end;
{$ENDIF}

    FReturnWorkIntervals := savedReturnWorkIntervals;

    // Allocates all resources directly assigned to a task
    service.Commit;
  finally
    branch.Free;
  end;
end;

function TEzScheduler.TestParentResourceAllocated(
  Path: TEzResourcePath;
  var Window: TEzTimespan) : Boolean;

  function CheckAllocatedForwards(Profile: TEzAvailabilityProfile): Boolean;
  var
    av_point: TEzAvailabilityPoint;
    i_point: Integer;

  begin
    Result := False;

    if not Profile.IndexOf(i_point, Window.Start) then
      dec(i_point);

    av_point := Profile[i_point];

    while True do
    begin
      //
      // No need to call prepare date range
      //
      if (av_point.Units<=0) and (afIsAllocated in av_point.Flags) then
      begin
        Result := True;
        // Return next date to schedule from
        inc(i_point);
        while i_point < Profile.Count do
        begin
          av_point := Profile[i_point];
          if not (afIsAllocated in av_point.Flags) then
          begin
            Window.Start := av_point.DateValue;
            Exit;
          end;
          inc(i_point);
        end;
        // Should never be reached
        Assert(False);
        Exit;
      end;

      inc(i_point);
      if (i_point >= Profile.Count) then
        Exit;
      av_point := Profile[i_point];
      if av_point.DateValue >= Window.Stop then
        Exit;
    end;
  end;

var
  i: Integer;
  Resource: TEzResource;
  ResourceAssignment: TEzResourceAssignment;
  Profile: TEzAvailabilityProfile;

begin
  Result := False;

  for i := 0 to Path.Resources.Count - 1 do
  begin
    ResourceAssignment := Path.Resources[i];
    Resource := Resources.ResourceByKey(ResourceAssignment.Key);
    if Resource.AllowTaskSwitching then
      continue;

    Profile := Resource.ActualAvailability;
    Result := CheckAllocatedForwards(Profile);
    if Result then
      Exit;
  end;
end;

function TEzScheduler.ScheduleHierarchy(
  const service: IGroupScheduleService;
  Branch: TScheduleBranch;
  ReverseScheduleOrder: Boolean;
  ForceDirection: TEzScheduleDirection;
  var Window: TEzTimespan;
  var PrevailingDirection: TEzScheduleDirection;
  var PathSuccessfull: Boolean) : Boolean;

var
  outerWindow: TEzTimespan;

  procedure SetPrevailingDirection(Direction: TEzScheduleDirection);
  begin
    if PrevailingDirection = sdUndefined then
      PrevailingDirection := Direction
    else if PrevailingDirection <> Direction then
      PrevailingDirection := sdForwardsAndBackwards;
  end;

  procedure ExtendOuterWindow(wnd: TEzTimespan);
  begin
    if (outerWindow.Start = 0) or (outerWindow.Stop = 0) then
      outerWindow := wnd
    else
      // Extend window
    begin
      outerWindow.Start := Min(outerWindow.Start, wnd.Start);
      outerWindow.Stop := Max(outerWindow.Stop, wnd.Stop);
    end;
  end;

var
  BreakOnSpeedFactor: Boolean;
  subBranch: TScheduleBranch;
  i: Integer;
  pathService: IGroupScheduleService;
  obj: TObject;
  pathIndex: Integer;
  Paths: TEzResourcePathList;
  task: TEzScheduleTask;
  branchWindow: TEzTimespan;
  bestWindow: TEzTimespan;
  succeeded: Boolean;

begin
  Result := True; // Success

  // Clear service when we go again
  Branch.BestService := nil;

  Paths := TEzResourcePathList.Create(True);
  try
    service.CrossJoinProfiles(Branch.Parent, Paths);
    bestWindow.Start := 0;
    pathIndex := 0;
    while pathIndex < Paths.Count do
    begin
      PathSuccessfull := True;

      // Create a nested service
      pathService := TGroupScheduleService.Create(Self, branch.Parent, service);
      pathService.ResourcePath := Paths[pathIndex];

      // Schedule this branch inside this window
      outerWindow := Window;

      if ReverseScheduleOrder then
        i := Branch.Count - 1 else
        i := 0;

      while True do
      begin
        obj := Branch[i];
        if obj is TScheduleBranch then
        begin
          branchWindow := Window;
          subBranch := obj as TScheduleBranch;
          Result := ScheduleHierarchy(  pathService,
                                        subBranch,
                                        ReverseScheduleOrder,
                                        ForceDirection,
                                        branchWindow, //outerWindow,
                                        PrevailingDirection,
                                        succeeded);

          if not Result then
            Exit; // Bail out and restart

          ExtendOuterWindow(branchWindow);
        end
        else
        begin
          task := obj as TEzScheduleTask;

          if not PrepareScheduleTask(task, False) then
            Exit;

          if ForceDirection <> sdUndefined then
            task.ScheduleData.Direction := ForceDirection;

          if task.ScheduleData.Direction = sdForwards then
          begin
            SetPrevailingDirection(sdForwards);
            if outerWindow.Start <> 0 then
              task.ScheduleData.Window.Start := Max(task.ScheduleData.Window.Start, outerWindow.Start);
          end
          else
          begin
            SetPrevailingDirection(sdBackwards);
            if outerWindow.Stop <> 0 then
              task.ScheduleData.Window.Stop := Min(task.ScheduleData.Window.Stop, outerWindow.Stop);
          end;

          ScheduleTaskIntervals(pathService, task, branchWindow);

          // Abort this path?
          if (srScheduledFailed in task.Flags) or
             (
                // Result is worse than previous result, we better abort now
                (bestWindow.Start <> 0) and
                (
                  (PrevailingDirection in [sdForwards, sdForwardsAndBackwards]) and (branchWindow.Stop >= bestWindow.Stop)
                ) or
                (
                  (PrevailingDirection = sdBackwards) and (branchWindow.Start <= bestWindow.Start)
                )
             )
          then
          begin
            if (srScheduledFailed in task.Flags) then
              pathService.SendScheduleMessages;
            PathSuccessfull := False;
            break;
          end;

          ExtendOuterWindow(branchWindow);
        end;

        if ReverseScheduleOrder then
        begin
          dec(i);
          if i < 0 then
            break;
        end
        else
        begin
          inc(i);
          if i = Branch.Count then
            break;
        end;
      end;

      if PathSuccessfull then
      begin
        if not ReverseScheduleOrder and TestParentResourceAllocated(Paths[pathIndex], outerWindow) then
          //
          // Parent resource is already allocated over scheduled interval ==>
          // Restart scheduling from window.start or window.stop
          //
        begin
          // TestParentResourceAllocated returns the date to continue
          // scheduling from in window.Start/window.Stop
          Window := outerWindow;
          Result := False;
          Exit; // Bail out, we must restart schedule loop
        end
        else
        begin
          // If current path has higher speed factor than next path,
          // we stop scheduling: Highest performer will be selected
          //NEW 04 JUILLET 2011 DP
          if not task.PickFinishASAPPath then
          begin
            BreakOnSpeedFactor := (pathIndex + 1 < Paths.Count) and (Paths[pathIndex].SpeedFactor > Paths[pathIndex + 1].SpeedFactor);

            if (Branch.BestService = nil) or
               BreakOnSpeedFactor or
              (
                (PrevailingDirection in [sdForwards, sdForwardsAndBackwards]) and
                ((outerWindow.Start < bestWindow.Start) or (outerWindow.Stop < bestWindow.Stop))
              ) or
              (
                (PrevailingDirection = sdBackwards) and
                ((outerWindow.Stop > bestWindow.Stop) or (outerWindow.Start > bestWindow.Start))
              )
            then
            begin
              bestWindow := outerWindow;
              pathService.OuterWindow := outerWindow;
              Branch.BestService := pathService;
              if BreakOnSpeedFactor then
                break;
            end;
          end
          else //PICK FINISH ASAP PATH
          begin
            if (Branch.BestService = nil) or
              (
                (PrevailingDirection in [sdForwards, sdForwardsAndBackwards]) and
                (((outerWindow.Stop < bestWindow.Stop)  or (bestWindow.Stop = 0)) or //WE PICK THE PATH ENDING FIRST
                 ((outerWindow.Start > bestWindow.Start) and (outerWindow.Stop <= bestWindow.Stop)) //OR THE PATH ENDING FIRST USING THE QUICKEST RESOURCES
                )
              ) or
              (
                (PrevailingDirection = sdBackwards) and
                ((outerWindow.Stop > bestWindow.Stop) or //WE PICK THE PATH ENDING AS LATE AS POSSIBLE
                 ((outerWindow.Stop >= bestWindow.Stop) and (outerWindow.Start > bestWindow.Start))   //OR THE PATH ENDING ALAP USING THE QUICKEST RESOURCES
                )
              )
            then
            begin
              bestWindow := outerWindow;
              pathService.OuterWindow := outerWindow;
              Branch.BestService := pathService;
            end;
          end;
        end;
      end;

      // Select next Path to try
      inc(pathIndex);
    end;

    // Do we have any schedule result?
    if Branch.BestService <> nil then
    begin
      Window := Branch.BestService.OuterWindow;

      // Commit allocations for this branch
      Branch.BestService.Commit;
    end;
  finally
    Paths.Free;
  end;
end;

procedure TEzScheduler.ScheduleToProjectCalendar(Key: Variant; Task: TEzScheduleTask; Interval: TEzScheduleInterval; var ResultInterval: TEzTimeSpan);
var
  i_int: Integer;
  Profile: TEzAvailabilityProfile;
  Data: TEzCalendarScheduleData;
  Intervals: TEzTimeSpanArray;
  WorkInterval: TEzWorkInterval;

begin
{$IFDEF DEBUG}
  if VarIsNull(Key) then; // Prevent key from removing by optimiser
{$ENDIF}

  Profile := GetProjectProfile;

  Data.Start := Task.ScheduleData.Window.Start;
  Data.Stop := Task.ScheduleData.Window.Stop;
  Data.AbsoluteStart := DatetimeToEzDatetime(FProjectWindowAbsoluteStart);
  Data.AbsoluteStop := DatetimeToEzDatetime(FProjectWindowAbsoluteStop);
  Data.Duration := DoubleToEzDuration(Interval.Duration);
  Data.UnitsRequired := 1; // Assigned = 1
  Data.Direction := Task.ScheduleData.Direction;
  Data.AllocationType := Task.ResourceAllocation;
  Data.SchedulebeyondScheduleWindow := FScheduleBeyondScheduleWindow;
  Data.FailingProfile := nil;
  Data.ScheduleResults := [];

  FScheduleIntervals.Clear;
  Intervals := FScheduleIntervals;

  Profile.ScheduleTask(Data, Intervals);
  if srScheduledOK in Data.ScheduleResults then
  begin
    if not Interval.IsSlackInterval then
      Task.Flags := Data.ScheduleResults;

    // Return result
    ResultInterval := GetExtendsFromIntervals(Intervals);

    if ReturnWorkIntervals then
    begin
      Task.WorkIntervals := TEzWorkIntervals.Create(True);
      for i_int := 0 to Intervals.Count - 1 do
      begin
        WorkInterval := TEzWorkInterval.Create;
        WorkInterval.TimeSpan := Intervals[i_int];
        Task.WorkIntervals.Add(WorkInterval);
      end;
    end;

    if srScheduledOutsideWindow in Task.Flags then
    begin
      if Interval.IsSlackInterval then
        AddScheduleMessage(Task, SSlackScheduledOutsideWindow) else
        AddScheduleMessage(Task, SScheduledOutsideWindow);
    end;
  end
  else
  begin
    if Interval.IsSlackInterval then
    begin
      Include(Task.Flags, srSlackScheduleFailed);
      AddScheduleMessage(Task, SSlackScheduleFailed);
    end
    else
    begin
      Task.Flags := Data.ScheduleResults;
      AddScheduleMessage(Task, STaskScheduleFailed);
    end;
  end;
end;

procedure TEzScheduler.ScheduleTask_DoNotLevel(Task: TEzScheduleTask);
var
  i_res, i_interval, i_point: Integer;
  Timespan: TEzTimespan;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource;
  OverAllocated, MessageAdded: Boolean;
  ResourceAssigments: TEzTaskAssignments;
  WorkInterval: TEzWorkInterval;

  Profile: TEzAvailabilityProfile;
  avpoint: TEzAvailabilityPoint;

begin
  if (Task.WorkIntervals = nil) or (Task.WorkIntervals.Count=0) then
    //
    // There is no workinterval data available.
    // Create a dummy resource path so that we can call AllocateResources
    // which will determine the workintervals for us.
    //
  begin
    Task.WorkIntervals := TEzWorkIntervals.Create(True);

    Profile := TEzAvailabilityProfile.Create;
    try
      Profile.Operator := opScheduleProfile;

      i_res := 0;
      while i_res < Task.Resources.Count do
      begin
        if Task.Resources[i_res] is TEzCapacityAssignment then
        begin
          AddScheduleMessage(Task, SCannotUseCapacityWithDNL);
          Exit;
        end;

        Assignment := Task.Resources[i_res] as TEzResourceAssignment;

        // Use the resource's original availability profile
        Resource := FResources.ResourceByKey(Assignment.Key);
        Profile.AddSource(Resource.Availability, False);
        inc(i_res);
      end;

      Timespan := GetTaskWindow(Task);
      Profile.PrepareDateRange(Timespan);

      i_point := Profile.DateToIndex(Timespan.Start);
      avPoint := Profile[i_point];
      repeat
        if avPoint.Units > 0.0 then
        begin
          WorkInterval := TEzWorkInterval.Create;
          WorkInterval.TimeSpan.Start := max(Timespan.Start, avPoint.DateValue);
          if i_point < Profile.Count - 1  then
          begin
            inc(i_point);
            avPoint := Profile[i_point];
            WorkInterval.TimeSpan.Stop := min(Timespan.Stop, avPoint.DateValue);
          end else
            WorkInterval.TimeSpan.Stop := Timespan.Stop;

          Task.WorkIntervals.Add(WorkInterval);
        end
        else
        begin
          inc(i_point);
          if i_point < Profile.Count then
            avPoint := Profile[i_point];
        end;
      until (i_point = Profile.Count) or (avPoint.DateValue >= Timespan.Stop);
    finally
      Profile.Free;
    end;
  end;

  //
  // Workintervals loaded now.
  //

  MessageAdded := False;

  for i_interval:=0 to Task.WorkIntervals.Count-1 do
  begin
    WorkInterval := Task.WorkIntervals[i_interval];

    if WorkInterval.ResourceAssigments <> nil then
      ResourceAssigments := WorkInterval.ResourceAssigments else
      ResourceAssigments := Task.Resources;

    Timespan := WorkInterval.TimeSpan;

    i_res:=0;
    while i_res < ResourceAssigments.Count do
    begin
      Assignment := (ResourceAssigments[i_res] as TEzResourceAssignment);
      Resource := FResources.ResourceByKey(Assignment.Key);

      Resource.ProfileRequired;
      OverAllocated := Resource.ActualAvailability.AllocateInterval(Timespan.Start, Timespan.Stop, Assignment.Assigned, True);

      if OverAllocated and not MessageAdded then
      begin
        MessageAdded := True;
        AddScheduleMessage(Task, Format(SResourceOverallocated, [Resource.Name]));
      end;

      inc(i_res);
    end;
  end;

  Include(Task.Flags, srScheduledOK);
end;

procedure TEzScheduler.ScheduleToResourceProfile(
    Service: ITaskScheduleService;
    const Key: Variant;
    Task: TEzScheduleTask;
    Interval: TEzScheduleInterval;
    var ResultInterval: TEzTimeSpan);
var
  i_path: Integer;
  Data: TEzCalendarScheduleData;
  Intervals: TEzTimeSpanArray;
  Profile: TEzAvailabilityProfile;
  BestResult: TEzTimeSpan;
  BestIntervals: TEzTimeSpanArray;
  BestPath: TEzResourcePath;
  BestFlags: TEzScheduleResults;
  BreakOnSpeedFactor: Boolean;
  PathHasMergedProfile: Boolean;
  Paths: TEzResourcePathList;

  procedure Reset;
  begin
    PathHasMergedProfile := False;
    Intervals.Clear;
    Data.AllResourcesAllowSwithing := True;
    Profile.Free;
  end;

begin
  Assert(Interval.IsSlackInterval=False);

  if VarIsNull(Key) then; // Prevent key from removing by optimiser

  Data.Start := Task.ScheduleData.Window.Start;
  Data.Stop := Task.ScheduleData.Window.Stop;
  Data.AbsoluteStart := DatetimeToEzDatetime(FProjectWindowAbsoluteStart);
  Data.AbsoluteStop := DatetimeToEzDatetime(FProjectWindowAbsoluteStop);

  Intervals := FScheduleIntervals;

  BestFlags := [];
  BestResult.Start := END_OF_TIME;
  BestResult.Stop := 0;
  BestPath := nil;
  Profile := nil;

  Paths := TEzResourcePathList.Create(True);
  BestIntervals := TEzTimeSpanArray.Create;

  try
    Service.CrossJoinProfiles(Task, Paths);

    // Task fails when no resource support the capacity requested
    if srScheduledFailed in Task.Flags then
      Exit;

    Data.Direction := Task.ScheduleData.Direction;
    Data.UnitsRequired := 1;                // Must be 1 when scheduling against
                                            // a profile with opScheduleProfile
    Data.AllResourcesAllowSwithing := False;// Must be false when scheduling against
                                            // a profile with opScheduleProfile
    Data.ResourceAllowsSwithing := False;   // Must be false when scheduling against
                                            // a profile with opScheduleProfile
    Data.SchedulebeyondScheduleWindow := FScheduleBeyondScheduleWindow;
    Data.AllocationType := Task.ResourceAllocation;

    for i_path := 0 to Paths.Count - 1 do
    begin
      Reset;

      Profile := Service.PrepareResourceProfile(  Task,
                                                  Interval,
                                                  Paths[i_path],
                                                  PathHasMergedProfile);

      Data.Duration := DoubleToEzDuration(Interval.Duration / Paths[i_path].SpeedFactor);
      Data.FailingProfile := nil;
      Data.ScheduleResults := [];
      Profile.ScheduleTask(Data, Intervals);

      if i_path = 0 then
        // Make sure that results are stored at least once!
        BestFlags := Data.ScheduleResults;

      if srScheduledOK in Data.ScheduleResults then
      begin
        ResultInterval := GetExtendsFromIntervals(Intervals);

        //NEW 04 JUILLET 2011 DP
        if not task.PickFinishASAPPath then
        begin
          // If current path has higher speed factor than next path,
          // we stop scheduling: Highest performer will be selected
          BreakOnSpeedFactor := (i_path + 1 < Paths.Count) and (Paths[i_path].SpeedFactor > Paths[i_path + 1].SpeedFactor);

          if BreakOnSpeedFactor or
            ((Data.Direction=sdForwards) and ((ResultInterval.Start<BestResult.Start) or (ResultInterval.Stop<BestResult.Stop))) or
            ((Data.Direction=sdBackwards) and ((ResultInterval.Stop>BestResult.Stop) or (ResultInterval.Start>BestResult.Start)))
          then
          begin
            BestResult := ResultInterval;
            BestPath := Paths[i_path];
            BestIntervals.Assign(Intervals);
            BestFlags := Data.ScheduleResults;
            if PathHasMergedProfile then
              BestFlags := BestFlags + [srTaskHasMergedProfiles];

            if BreakOnSpeedFactor then
              break;
          end;
        end
        else //we pick the finish asap path
        begin
          if ((Data.Direction=sdForwards) and (((ResultInterval.Stop < BestResult.Stop) or (BestResult.Stop = 0)) or //WE PICK THE PATH ENDING FIRST
                                              //OR THE PATH ENDING FIRST USING THE QUICKEST RESOURCES
                                              ((ResultInterval.Start > BestResult.Start) and (ResultInterval.Stop <= BestResult.Stop)))
             )
          or ((Data.Direction=sdBackwards) and ((ResultInterval.Stop > BestResult.Stop) or //WE PICK THE PATH ENDING AS LATE AS POSSIBLE
                                               //OR THE PATH ENDING ALAP USING THE QUICKEST RESOURCES
                                               ((ResultInterval.Stop >= BestResult.Stop) and (ResultInterval.Start > BestResult.Start)))
             )
          then
          begin
            BestResult := ResultInterval;
            BestPath := Paths[i_path];
            BestIntervals.Assign(Intervals);
            BestFlags := Data.ScheduleResults;
            if PathHasMergedProfile then
              BestFlags := BestFlags + [srTaskHasMergedProfiles];
          end;
        end;
      end;
    end;

    // Return Flags, whatever they may be...
    Task.Flags := BestFlags;

    if srScheduledOK in BestFlags then
    begin
      ResultInterval := BestResult;

      Service.AllocateResources(Task, BestIntervals, BestPath);

      if srScheduledOutsideWindow in Task.Flags then
        Service.AddScheduleMessage(Task, SScheduledOutsideWindow);
    end;

    if srScheduledFailed in Task.Flags then
      Service.AddScheduleMessage(Task, STaskScheduleFailed);
  finally
    Paths.Free;
    BestIntervals.Free;
    Profile.Free;
  end;
end;

//procedure TEzScheduler.ScheduleToResourceProfile(
//    const Key: Variant;
//    Task: TEzScheduleTask;
//    Interval: TEzScheduleInterval;
//    TestSchedule: Boolean;
//    var ResultInterval: TEzTimeSpan);
//var
//  Resource: TEzResource;
//  Assignment: TEzResourceAssignment;
//  Data: TEzCalendarScheduleData;
//  Intervals: TEzTimeSpanArray;
//  Joins: TEzResourceAssignmentsCrossJoins;
//
//  procedure PrepareAvailabilityProfile;
//  var
//    i_res: Integer;
//
//  begin
//    Task.ScheduleData.Profile := TEzAvailabilityProfile.Create;
//    Task.ScheduleData.OwnsProfile := True;
//
//    Task.ScheduleData.Profile.Operator := opScheduleProfile;
//    i_res := 0;
//    while i_res < Task.Resources.Count do
//    begin
//      Assignment := Task.Resources[i_res];
//      Resource := FResources.ResourceByKey(Assignment.Key);
//
//      if (Task.ResourceAllocation=alFixed) and (Assignment.Assigned<=0.0) then
//        // Assignment should be greater than 0
//        AddScheduleMessage(Task, Format(SResourceAssignmentIsNull, [Resource.Name]))
//
//      else
//      begin
//        Resource.ProfileRequired;
//        Resource.ActualAvailability.AllowTaskSwitching := Resource.AllowTaskSwitching;
//
//        // if schedule type == calendar or if constraint == MFO or MSO
//    		// then we ignore any task already scheduled and schedule
//        // this task against the original resource availability.
//        if (Interval.ScheduleFlag=ifResourceDefaultAvailability) or (Task.Constraint in [cnMSO, cnMFO]) then
//          Task.ScheduleData.Profile.AddSource(Resource.Availability, Assignment.Assigned, False) else
//          Task.ScheduleData.Profile.AddSource(Resource.ActualAvailability, Assignment.Assigned, False);
//      end;
//
//      inc(i_res);
//    end;
//  end;
//
//  procedure SaveScheduleResult(ScheduleResults: TEzScheduleResults);
//  begin
//    if not Interval.IsSlackInterval then
//    begin
//      Task.Flags := ScheduleResults;
//
//      if srScheduledOK in Task.Flags then
//      begin
//        ResultInterval := GetExtendsFromIntervals(Intervals);
//        AllocateResources(Task, Intervals);
//        if srScheduledOutsideWindow in Task.Flags then
//          AddScheduleMessage(Task, SScheduledOutsideWindow);
//      end
//
//      else if srScheduledFailed in Task.Flags then
//      begin
//        Resource := TrackFailingResource(Task, Interval);
//        if Resource<>nil then
//          AddScheduleMessage(Task, Resource.Key, SResourceScheduleFailed) else
//          AddScheduleMessage(Task, STaskScheduleFailed);
//      end;
//    end
//    else
//      //
//      // Slack interval scheduled
//      //
//    begin
//      if srScheduledOK in ScheduleResults then
//      begin
//        ResultInterval := GetExtendsFromIntervals(Intervals);
//        if srScheduledOutsideWindow in ScheduleResults then
//          AddScheduleMessage(Task, SSlackScheduledOutsideWindow);
//      end
//
//      else if srScheduledFailed in ScheduleResults then
//      begin
//        Include(Task.Flags, srSlackScheduleFailed);
//        AddScheduleMessage(Task, SSlackScheduleFailed);
//      end;
//    end;
//  end;
//
//begin
//  Joins := TEzResourceAssignmentsCrossJoins.Create;
//  CrossJoinProfiles(Task, Joins);
//
//  if VarIsNull(Key) then; // Prevent debugger from removing this variable
//
//  Data.Start := Task.ScheduleData.Window.Start;
//  Data.Stop := Task.ScheduleData.Window.Stop;
//  Data.AbsoluteStart := DatetimeToEzDatetime(FProjectWindowAbsoluteStart);
//  Data.AbsoluteStop := DatetimeToEzDatetime(FProjectWindowAbsoluteStop);
//
//  if FUseWorkingPeriods then
//    Intervals := Interval.GetWorkingPeriods else
//    Intervals := FScheduleIntervals;
//
//  Intervals.Clear;
//
//  if Task.ScheduleData.Profile = nil then
//    PrepareAvailabilityProfile;
//
//  if Task.ScheduleData.Profile.Sources.Count=0 then
//  begin
//    include(Task.Flags, srScheduledOK);
//    Exit;
//  end;
//
//  Data.Direction := Task.ScheduleData.Direction;
//  Data.Duration := DoubleToEzDuration(Interval.Duration);
//  Data.UnitsRequired := 1;                // Must be 1 when scheduling against
//                                          // a profile with opScheduleProfile
//  Data.AllResourcesAllowSwithing := False;// Must be false when scheduling against
//                                          // a profile with opScheduleProfile
//  Data.ResourceAllowsSwithing := False;   // Must be false when scheduling against
//                                          // a profile with opScheduleProfile
//  Data.SchedulebeyondScheduleWindow := FScheduleBeyondScheduleWindow;
//  Data.AllocationType := Task.ResourceAllocation;
//  Data.FailingProfile := nil;
//  Data.ScheduleResults := [];
//  Task.ScheduleData.Profile.ScheduleTask(Data, Intervals);
//  if TestSchedule then
//    Task.Flags := Data.ScheduleResults else
//    SaveScheduleResult(Data.ScheduleResults);
//end;

procedure TEzScheduler.StoreScheduleTask(Node: TRecordNode);
var
  ParentNode: TRecordNode;
  Task, Parent: TEzScheduleTask;

begin
  Task := Node.Data;

	//
  // A task with constraint cnIgnore does not require any scheduling
  //
  if Task.Constraint = cnIgnore then
  begin
    Include(Task.Flags, srScheduledOK);
    Exit;
  end;

  ParentNode := Node.Parent;
  while ParentNode <> nil do
  begin
    Parent := ParentNode.Data;

 		// Add resources of parent to this task
		// CopyParentsResources(Task, Parent);

 		// Add predecessors of parent to this task
		CopyParentPredecessors(Task, Parent);

  	// If parents constraint is more restrictive than childs constraint, then
		// use parents constraint
		if Parent.Constraint < Task.Constraint then
    begin
			Task.Constraint := Parent.Constraint;
      Task.ConstraintDate := Parent.ConstraintDate;
    end
    else if Parent.Constraint = cnIgnore then
    begin
      Include(Task.Flags, srScheduledOK);
      Exit;
    end;

    // if parents priority is higher then childs priority, then
    // overwrite childs priority
    Task.Priority := min(Task.Priority, Parent.Priority);

    ParentNode := ParentNode.Parent;
  end;

	// Check if we end up with a valid constraint and constraint date
	if (Task.Constraint > cnDNL) and (Task.Constraint < cnASAP) then
  begin
		if Task.ConstraintDate = 0 then
    begin
      AddScheduleMessage(Task, SNoConstraintDate);
      Task.Constraint := cnASAP;
    end
		else if Task.ConstraintDate < ProjectWindowAbsoluteStart then
    begin
      AddScheduleMessage(Task, SConstraintBeforeProjectStart);
      Task.Constraint := cnASAP;
    end
		else if Task.ConstraintDate > ProjectWindowAbsoluteStop then
    begin
      AddScheduleMessage(Task, SConstraintAfterProjectEnd);
      Task.Constraint := cnASAP;
    end;
  end;

  ProcessResources(Task);
	NormalizePredecessors(Task);
  FScheduleTasks.Add(Task);
end;

//function TEzScheduler.TrackFailingResource(Task: TEzScheduleTask; Interval: TEzScheduleInterval): TEzResource;
//var
//  SavedFlags: TEzScheduleResults;
//  SavedResources: TEzResourceAssignments;
//  i_res: Integer;
//  ResultInterval: TEzTimeSpan;
//
//begin
//  Result := nil;
//
//  if Task.Resources.Count=1 then
//    Result := FResources.ResourceByKey(Task.Resources[0].Key)
//
//  else
//  begin
//    i_res := 0;
//    SavedFlags := Task.Flags;
//    SavedResources := Task.Resources;
//
//    Task.Resources := TEzResourceAssignments.Create(False {does not own objects});
//    Task.Flags := [];
//    try
//      repeat
//        Task.Resources.Clear;
//        Task.Resources.Add(SavedResources[i_res]);
//        ScheduleToResourceProfile(Task.key.Value, Task, Interval, True, ResultInterval);
//        if (srScheduledOK in Task.Flags) then
//          inc(i_res);
//      until (i_res>=SavedResources.Count) or not (srScheduledOK in Task.Flags);
//
//      if i_res<SavedResources.Count then
//        Result := FResources.ResourceByKey(SavedResources[i_res].Key);
//    finally
//      Task.Resources.Free;
//      Task.Resources := SavedResources;
//      Task.Flags := SavedFlags;
//    end;
//  end;
//end;

procedure TEzScheduler.UpdateWindowWithTask(var ScheduleData: TEzInternalData; Task: TEzScheduleTask);
var
  Estimate: TEzDuration;

begin
	case Task.Constraint of
		cnMSO:
    begin
      ScheduleData.LeftTask := Task;
      ScheduleData.LeftBoundaryType := btConstraintDate;
      ScheduleData.Window.Start := DatetimeToEzDatetime(Task.ConstraintDate);
      ScheduleData.Direction := sdForwards;
    end;

		cnMFO:
    begin
      ScheduleData.RightTask := Task;
      ScheduleData.RightBoundaryType := btConstraintDate;
      ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate);
      ScheduleData.Direction := sdBackwards;
		end;

		cnSNET:
      if DatetimeToEzDatetime(Task.ConstraintDate)>ScheduleData.Window.Start then
      begin
        ScheduleData.LeftTask := Task;
        ScheduleData.LeftBoundaryType := btConstraintDate;
        ScheduleData.Window.Start := DatetimeToEzDatetime(Task.ConstraintDate);
        if ScheduleData.Direction = sdUndefined then
          ScheduleData.Direction := sdForwards;
      end;

		cnSNLT:
    begin
      Estimate := DoubleToEzDuration(EstimateTaskDuration(Task));
      if DatetimeToEzDatetime(Task.ConstraintDate)<ScheduleData.Window.Stop+Estimate then
      begin
        ScheduleData.RightTask := Task;
        ScheduleData.RightBoundaryType := btConstraintDate;
        ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate)+Estimate;
        if ScheduleData.Direction = sdUndefined then
          ScheduleData.Direction := sdForwards;
      end;
    end;

		cnFNLT:
    begin
      if DatetimeToEzDatetime(Task.ConstraintDate)<ScheduleData.Window.Stop then
      begin
        ScheduleData.RightTask := Task;
        ScheduleData.RightBoundaryType := btConstraintDate;
        ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate);
        if ScheduleData.Direction = sdUndefined then
          ScheduleData.Direction := sdForwards;
      end;
    end;

		cnFAB:
			if DatetimeToEzDatetime(Task.ConstraintDate)<ScheduleData.Window.Stop then
      begin
				ScheduleData.RightTask := Task;
        ScheduleData.RightBoundaryType := btConstraintDate;
				ScheduleData.Window.Stop := DatetimeToEzDatetime(Task.ConstraintDate);
				ScheduleData.Direction := sdBackwards;
      end;
  end;

  if Task.WindowStart<>0 then
    ScheduleData.Window.Start :=
      max(ScheduleData.Window.Start, DateTimeToEzDateTime(Task.WindowStart));

  if Task.WindowStop<>0 then
    ScheduleData.Window.Stop :=
      min(ScheduleData.Window.Stop, DateTimeToEzDateTime(Task.WindowStop));
end;

procedure TEzScheduler.UpdateWindowWithPredecessorTaskWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Relation.Relation of
    prFinishStart, prStartStart:
      if Predecessor.WindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.WindowStop+Relation.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Predecessor;
          Task.ScheduleData.LeftBoundaryType := btTaskWindowEndDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Predecessor.WindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.WindowStart-Relation.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Predecessor;
          Task.ScheduleData.RightBoundaryType := btTaskWindowStartDate;
        end;
      end;
  end;
end;

procedure TEzScheduler.UpdateWindowWithPredecessorSlackWindow(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Relation.Relation of
    prFinishStart, prStartStart:
      if Predecessor.SlackWindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.SlackWindowStop+Relation.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Predecessor;
          Task.ScheduleData.LeftBoundaryType := btSlackWindowEndDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Predecessor.SlackWindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Predecessor.SlackWindowStart-Relation.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Predecessor;
          Task.ScheduleData.RightBoundaryType := btSlackWindowStartDate;
        end;
      end;
  end;
end;

procedure TEzScheduler.UpdateWindowWithPredecessor(Predecessor: TEzScheduleTask; Relation: TEzPredecessor; Task: TEzScheduleTask);
var
  sd: TEzScheduleDirection;
	ADate: TEzDateTime;
  BoundaryType: TScheduleWindowBoundType;

begin
  sd := sdForwards;
  case Relation.Relation of
    prFinishStart:
    begin
			sd := sdForwards;
      if IsMileStone(Predecessor) then
      begin
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate+Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
        ADate := GetExtendsFromIntervals(Predecessor.Intervals).Stop+DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskEndDate;
      end;

			if(ADate > Task.ScheduleData.Window.Start) then
      begin
        Task.ScheduleData.Window.Start := ADate;
        Task.ScheduleData.LeftTask := Predecessor;
        Task.ScheduleData.LeftBoundaryType := BoundaryType;
      end;
    end;

		prStartFinish:
    begin
      if IsMileStone(Predecessor) then
      begin
  			sd := sdForwards;
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate-Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Predecessor.Intervals).Start-DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskStartDate;
      end;

			if(ADate<Task.ScheduleData.Window.Stop) then
      begin
        Task.ScheduleData.Window.Stop := ADate;
        Task.ScheduleData.RightTask := Predecessor;
        Task.ScheduleData.RightBoundaryType := BoundaryType;
      end;
    end;

		prStartStart:
    begin
			sd := sdForwards;
      if IsMileStone(Predecessor) then
      begin
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate+Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			ADate := GetExtendsFromIntervals(Predecessor.Intervals).Start+
                 DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskStartDate;
      end;

			if(ADate>Task.ScheduleData.Window.Start) then
      begin
        Task.ScheduleData.Window.Start := ADate;
        Task.ScheduleData.LeftTask := Predecessor;
        Task.ScheduleData.LeftBoundaryType := BoundaryType;
      end;
    end;

		prFinishFinish:
    begin
      if IsMileStone(Predecessor) then
      begin
  			sd := sdForwards;
  			ADate := DatetimeToEzDatetime(Predecessor.ConstraintDate-Relation.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
        sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Predecessor.Intervals).Stop-
                 DoubleToEzDuration(Relation.Lag);
        BoundaryType := btTaskEndDate;
      end;

			if(ADate<Task.ScheduleData.Window.Stop) then
      begin
        Task.ScheduleData.Window.Stop := ADate;
        Task.ScheduleData.RightTask := Predecessor;
        Task.ScheduleData.RightBoundaryType := BoundaryType;
      end;
    end;
  end;

  case Task.ScheduleData.Direction of
		sdUndefined:
      Task.ScheduleData.Direction := sd;

		sdForwards:
			if sd = sdBackwards then
				Task.ScheduleData.Direction := sdDontCare;

		sdBackwards:
			if sd = sdForwards then
				Task.ScheduleData.Direction := sdDontCare;
	end;
end;

procedure TEzScheduler.UpdateWindowWithSuccessor(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask);
var
  sd: TEzScheduleDirection;
	ADate: TEzDateTime;
  BoundaryType: TScheduleWindowBoundType;
  
begin
  sd := sdBackwards;
  case Successor.Relation of
    prFinishStart:
    begin
      sd := sdForwards;

      if IsMileStone(Successor.Task) then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate-Successor.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Start-
                 DoubleToEzDuration(Successor.Lag);
        BoundaryType := btTaskStartDate;
      end;

			if(ADate<Task.ScheduleData.Window.Stop) then
      begin
        Task.ScheduleData.Window.Stop := ADate;
        Task.ScheduleData.RightTask := Successor.Task;
        Task.ScheduleData.RightBoundaryType := BoundaryType;
      end;
    end;

		prStartFinish:
    begin
			sd := sdForwards;
      if IsMileStone(Successor.Task) then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate+Successor.Lag);
        BoundaryType := btConstraintDate;
      end
      else
      begin
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Stop+
                 DoubleToEzDuration(Successor.Lag);
        BoundaryType := btTaskEndDate;
      end;

			if(ADate>Task.ScheduleData.Window.Start) then
      begin
        Task.ScheduleData.Window.Start := ADate;
        Task.ScheduleData.LeftTask := Successor.Task;
        Task.ScheduleData.LeftBoundaryType := BoundaryType;
      end;
    end;

		prStartStart:
    begin
      if IsMileStone(Successor.Task) then
      begin
  			sd := sdForwards;
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate-Successor.Lag);
  			if(ADate>Task.ScheduleData.Window.Start) then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btConstraintDate;
        end;
      end
      else
      begin
        sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Start+
                 DoubleToEzDuration(estimateTaskduration(Task))-
                 DoubleToEzDuration(Successor.Lag);
  			if(ADate<Task.ScheduleData.Window.Stop) then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btTaskStartDate;
        end;
      end;
    end;

		prFinishFinish:
    begin
      if IsMileStone(Successor.Task) then
      begin
        sd := sdForwards;
        ADate := DatetimeToEzDatetime(Successor.Task.ConstraintDate+Successor.Lag);
  			if(ADate>Task.ScheduleData.Window.Start) then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btConstraintDate;
        end;
      end
      else
      begin
        sd := sdBackwards;
  			ADate := GetExtendsFromIntervals(Successor.Task.Intervals).Stop-
                 DoubleToEzDuration(estimateTaskduration(Task))+
                 DoubleToEzDuration(Successor.Lag);
  			if(ADate<Task.ScheduleData.Window.Stop) then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btTaskEndDate;
        end;
      end;
    end;
  end;

  case Task.ScheduleData.Direction of
		sdUndefined:
      Task.ScheduleData.Direction := sd;

		sdForwards:
			if sd = sdBackwards then
				Task.ScheduleData.Direction := sdDontCare;

		sdBackwards:
			if sd = sdForwards then
				Task.ScheduleData.Direction := sdDontCare;
	end;
end;

procedure TEzScheduler.UpdateWindowWithSuccessorTaskWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Successor.Relation of
    prFinishStart, prStartStart:
      if Successor.Task.WindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.WindowStart-Successor.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btTaskWindowStartDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Successor.Task.WindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.WindowStop+Successor.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btTaskWindowEndDate;
        end;
      end;
  end;
end;

procedure TEzScheduler.UpdateWindowWithSuccessorSlackWindow(Successor: TEzScheduleSuccessor; Task: TEzScheduleTask);
var
  ADate: TEzDateTime;

begin
  case Successor.Relation of
    prFinishStart, prStartStart:
      if Successor.Task.SlackWindowStart<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.SlackWindowStart-Successor.Lag);
        if ADate<Task.ScheduleData.Window.Stop then
        begin
          Task.ScheduleData.Window.Stop := ADate;
          Task.ScheduleData.RightTask := Successor.Task;
          Task.ScheduleData.RightBoundaryType := btSlackWindowStartDate;
        end;
      end;

		prStartFinish, prFinishFinish:
      if Successor.Task.SlackWindowStop<>0 then
      begin
        ADate := DatetimeToEzDatetime(Successor.Task.SlackWindowStop+Successor.Lag);
        if ADate>Task.ScheduleData.Window.Start then
        begin
          Task.ScheduleData.Window.Start := ADate;
          Task.ScheduleData.LeftTask := Successor.Task;
          Task.ScheduleData.LeftBoundaryType := btSlackWindowEndDate;
        end;
      end;
  end;
end;

function TEzScheduler.VerifyTaskWindow(Task: TEzScheduleTask): Boolean;
begin
  if Task.ScheduleData.Window.Stop<Task.ScheduleData.Window.Start then
  begin
    Result := False;
    AddScheduleMessage(Task, SEmptyScheduleWindow);
  end else
    Result := True;
end;

{ TEzSortedResourceAssignments }

function TEzSortedResourceAssignments.CompareItems(Item1,
  Item2: TObject): Integer;
var
  i1: TEzResourceAssignment;
  i2: TEzResourceAssignment;

begin
  i1 := TEzResourceAssignment(Item1);
  i2 := TEzResourceAssignment(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzSortedResourceAssignments.GetItem(
  Index: Integer): TEzResourceAssignment;
begin
  Result := TEzResourceAssignment(inherited GetItem(Index));
end;

function TEzSortedResourceAssignments.IndexOfKey(Key: Variant): Integer;
var
  Find: TEzResourceAssignment;

begin
  Find := TEzResourceAssignment.Create;
  try
    Find.FKey := Key;
    if not IndexOf(Result, Find) then
      Result := -1;
  finally
    Find.Free;
  end;
end;

{ TEzSortedCapacityAssignments }

function TEzSortedCapacityAssignments.CompareItems(Item1,
  Item2: TObject): Integer;
var
  i1: TEzCapacityAssignment;
  i2: TEzCapacityAssignment;

begin
  i1 := TEzCapacityAssignment(Item1);
  i2 := TEzCapacityAssignment(Item2);
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TEzSortedCapacityAssignments.GetItem(
  Index: Integer): TEzCapacityAssignment;
begin
  Result := TEzCapacityAssignment(inherited GetItem(Index));
end;

function TEzSortedCapacityAssignments.IndexOfKey(Key: Variant): Integer;
var
  Find: TEzCapacityAssignment;

begin
  Find := TEzCapacityAssignment.Create;
  try
    Find.FKey := Key;
    IndexOf(Result, Find);
  finally
    Find.Free;
  end;
end;

{ TEzResourceAssignment }
constructor TEzResourceAssignment.Create;
begin
  inherited;

end;

constructor TEzResourceAssignment.Create(CopyFrom: TEzResourceAssignment);
begin
  inherited Create(CopyFrom);
  FCapacityKey := CopyFrom.CapacityKey;

  FScheduleProfile := CopyFrom.ScheduleProfile;
end;

destructor TEzResourceAssignment.Destroy;
begin
  inherited;
end;

procedure TEzResourceAssignment.SetScheduleProfile(
  const Value: TEzAvailabilityProfile);
begin
  FScheduleProfile := Value;
end;

constructor TEzTaskAssignment.Create(CopyFrom: TEzTaskAssignment);
begin
  FKey := CopyFrom.Key;
  FAssigned := CopyFrom.Assigned;
  FAffectsDuration := CopyFrom.AffectsDuration;
  ScheduleSucceeded := CopyFrom.ScheduleSucceeded;
  FSpeedFactor := CopyFrom.SpeedFactor;
end;

{ TEzResourcePath }

constructor TEzResourcePath.Create(
    _Resources: TEzResourceAssignments;
    _Capacities: TEzCapacityAssignments);

begin
  FResources := _Resources;
  FCapacities := _Capacities;
  FSpeedFactor := 0.0; // Not set
end;

destructor TEzResourcePath.Destroy;
begin
  inherited;
  FResources.Free;
  FCapacities.Free;
end;

function TEzResourcePath.Clone: TEzResourcePath;
var
  i: Integer;
  res: TEzResourceAssignments;
  cap: TEzCapacityAssignments;

begin
  // Always allocate resource assignments list, even when source is empty
  res := TEzResourceAssignments.Create(True);
  for i := 0 to Resources.Count - 1 do
    res.Add(TEzResourceAssignment.Create(Resources[i]));

  if (Capacities <> nil) and (Capacities.Count > 0) then
  begin
    cap := TEzCapacityAssignments.Create(True);
    for i := 0 to Capacities.Count - 1 do
      cap.Add(TEzCapacityAssignment.Create(Capacities[i]));
  end else
    cap := nil;

  Result := TEzResourcePath.Create(res, cap);
end;


function TEzResourcePath.get_SpeedFactor: Double;
var
  i: Integer;

begin
  if FSpeedFactor = 0.0 then
  begin
    FSpeedFactor := 1.0;
    for i := 0 to FResources.Count - 1 do
      if FResources[i].SpeedFactor > 0 then
        FSpeedFactor := FSpeedFactor * FResources[i].SpeedFactor;
  end;

  Result := FSpeedFactor;
end;

{ TEzResourcePathList }

function TEzResourcePathList.GetItem(Index: Integer): TEzResourcePath;
begin
  Result := TEzResourcePath(inherited GetItem(Index));
end;

{ TEzWorkingPeriods }

function TEzWorkIntervals.GetItem(Index: Integer): TEzWorkInterval;
begin
  Result := TEzWorkInterval(inherited GetItem(Index));
end;

procedure TEzWorkIntervals.SetItem(Index: Integer; Value: TEzWorkInterval);
begin
  inherited SetItem(Index, Value);
end;

{ TEzWorkInterval }

destructor TEzWorkInterval.Destroy;
begin
  inherited;
  ResourceAssigments.Free;
end;

{ TTaskScheduleService }

procedure TTaskScheduleService.Clear;
begin
  _ProfilesToDestroy.Clear;
end;

constructor TTaskScheduleService.Create(AOwner: TEzScheduler);
begin
  inherited Create;
  _Owner := AOwner;
  _ProfilesToDestroy := TObjectList.Create(True);
end;

procedure TTaskScheduleService.CrossJoinProfiles(
  Task: TEzScheduleTask;
  Paths: TEzResourcePathList);
var
  BaseResources: TEzResourceAssignments;
  BaseCapacities: TEzCapacityAssignments;

  TaskAssignment: TEzTaskAssignment;
  ResourceAssignment: TEzResourceAssignment;

  i_ass: Integer;
  Resource: TEzResource;

  procedure AddResourceAssignment(
      Assignment: TEzResourceAssignment;
      AList: TEzResourceAssignments);
  var
    i_index: Integer;

  begin
    i_index := AList.IndexOf(Assignment);

    if i_index = -1 then
      // New resource
      AList.Add(TEzResourceAssignment.Create(Assignment))
    else
      // If resource is already added to this task,
      // update the assigned units to the maximum.
      AList[i_index].Assigned :=
        Max(Assignment.Assigned, AList[i_index].Assigned);
  end;

  procedure AddCapacityAssignment(
      Assignment: TEzCapacityAssignment;
      AList: TEzCapacityAssignments);
  var
    i_index: Integer;

  begin
    i_index := AList.IndexOf(Assignment);

    if i_index = -1 then
      // New Capacity
      AList.Add(TEzCapacityAssignment.Create(Assignment))
    else
      // If Capacity is already added to this task,
      // update the assigned units to the maximum.
      AList[i_index].Assigned :=
        Max(Assignment.Assigned, AList[i_index].Assigned);
  end;

  procedure AddResourceOperators(
    Resource: TEzResource;
    ResourceList: TEzResourceAssignments;
    CapacityList: TEzCapacityAssignments);
  var
    Operators: TEzTaskAssignments;
    i_opp: Integer;

  begin
    Operators := Resource.Operators;

    i_opp := 0;
    while i_opp < Operators.Count do
    begin
      if Operators[i_opp] is TEzResourceAssignment then
        AddResourceAssignment(Operators[i_opp] as TEzResourceAssignment, ResourceList)
      else
        AddCapacityAssignment(Operators[i_opp] as TEzCapacityAssignment, CapacityList);
      inc(i_opp);
    end;
  end;

  function CloneRPath(Path: TEzResourceAssignments): TEzResourceAssignments;
  var
    i: Integer;

  begin
    Result := TEzResourceAssignments.Create(True);
    for i := 0 to Path.Count - 1 do
      Result.Add(TEzResourceAssignment.Create(Path[i]));
  end;

  function CloneCPath(Path: TEzCapacityAssignments): TEzCapacityAssignments;
  var
    i: Integer;

  begin
    Result := TEzCapacityAssignments.Create(True);
    for i := 0 to Path.Count - 1 do
      Result.Add(TEzCapacityAssignment.Create(Path[i]));
  end;

  function GetResourceSpeedFactor(Resource: TEzResource; CapacityKey: TEzCapacityKey): Double;
  var
    capacities: TEzCapacityList;
    i: Integer;
  begin
    Result := 1;

    if Assigned(_Owner.OnGetResourceSpeedFactor) then
    begin
      _Owner.OnGetResourceSpeedFactor(Resource, CapacityKey, Result);
      Exit;
    end
    else
    begin
      capacities := Resource.Capacities;
      for i := 0 to capacities.Count - 1 do
      begin
        if capacities[i].Key = CapacityKey then
        begin
          Result := capacities[i].SpeedFactor;
          Exit;
        end;
      end;
    end;

    // We should never end up here
    Assert(False);
  end;

  procedure CrossJoinCapacities(
      ListIndex: Integer;
      RPath: TEzResourceAssignments;
      CPath: TEzCapacityAssignments);
  var
    CapacityAssignment: TEzCapacityAssignment;
    CapacityResources: TEzResources;
    i_cap, i_res: Integer;
    ResourceAssignment: TEzResourceAssignment;
    ResourceWasAdded: Boolean;
    RClone: TEzResourceAssignments;
    CClone: TEzCapacityAssignments;

  begin
    RClone := nil;
    CClone := nil;

    CapacityAssignment := CPath[ListIndex];

    i_cap := _Owner.FCapacityResources.IndexOf(CapacityAssignment.Key);
    if i_cap=-1 then
    begin
      AddScheduleMessage(Task, Format(SCapacityNotSupported, [SafeString(CapacityAssignment.Key)]));
      include(Task.Flags, srScheduledFailed);
      Exit;
    end;

    ResourceWasAdded := False;
    CapacityResources := _Owner.FCapacityResources[i_cap].Resources;

    for i_res := 0 to CapacityResources.Count-1 do
    begin
      Resource := _Owner.FCapacityResources[i_cap].Resources[i_res];

      // Add this resource as a possible resource to schedule for this task
      // However make sure this resource it was not selected as a
      // fixed resource and not as a resource for another capacity.
      if (RPath.IndexOfKey(Resource.Key) = -1) then
      begin
        if not _Owner.DoTestResourceCapacity(Task, CapacityAssignment, Resource) then
          continue;

        if not CapacityAssignment.MergeProfiles or (ResourceWasAdded = False) then
          // if the ResourceWasAdded variable is still at false then
          // it is the first resource being added for the current capacity
          //
          // Clone current path if we are not merging
          // or
          // when this is the first resource being added for the current capacity
          //
        begin
          RClone := CloneRPath(RPath);
          CClone := CloneCpath(CPath);
        end;

        ResourceWasAdded := True;

        ResourceAssignment := TEzResourceAssignment.Create;
        ResourceAssignment.Key := Resource.Key;
        ResourceAssignment.Assigned := CapacityAssignment.Assigned;
        ResourceAssignment.AffectsDuration := CapacityAssignment.AffectsDuration;
        ResourceAssignment.CapacityKey := CapacityAssignment.Key;

        if CapacityAssignment.SpeedFactor <> -1.0 then
          //
          // The SpeedFactor has been set for this
          // task, resource and capacity combination
          //
          ResourceAssignment.SpeedFactor := CapacityAssignment.SpeedFactor else
          ResourceAssignment.SpeedFactor := GetResourceSpeedFactor(Resource, CapacityAssignment.Key);

        // Add assignment to current path
        RClone.Add(ResourceAssignment);

        AddResourceOperators(Resource, RClone, CClone);

        if not CapacityAssignment.MergeProfiles then
        begin
          if ListIndex < CClone.Count - 1 then
            // Go down the chain, add resources for other capacities as well
            CrossJoinCapacities(ListIndex + 1, RClone, CClone) else
            Paths.Add(TEzResourcePath.Create(RClone, CClone));
        end;
      end;
    end;

    if not ResourceWasAdded then
    begin
      AddScheduleMessage(Task, Format(SCapacityNotSupported, [SafeString(CapacityAssignment.Key)]));
      include(Task.Flags, srScheduledFailed);
      Exit;
    end;

    if CapacityAssignment.MergeProfiles then
    begin
      if ListIndex < CClone.Count - 1 then
        // Go down the chain, add resources for other capacities as well
        CrossJoinCapacities(ListIndex + 1, RClone, CClone) else
        Paths.Add(TEzResourcePath.Create(RClone, CClone));
    end;
 end;

begin
  BaseResources := TEzResourceAssignments.Create(True);
  BaseCapacities := TEzCapacityAssignments.Create(True);

  i_ass := 0;
  while i_ass < Task.Resources.Count do
  begin
    TaskAssignment := Task.Resources[i_ass];

    if TaskAssignment is TEzResourceAssignment then
    begin
      ResourceAssignment := TaskAssignment as TEzResourceAssignment;
      AddResourceAssignment(ResourceAssignment, BaseResources);

      //
      // Add operators of resource to the list as well
      //
      Resource := _Owner.Resources.ResourceByKey(ResourceAssignment.Key);
      AddResourceOperators(Resource, BaseResources, BaseCapacities);
    end
    else
    begin
      AddCapacityAssignment(TaskAssignment as TEzCapacityAssignment, BaseCapacities);
    end;

    inc(i_ass);
  end;

  // Does this task use any 'resource by capacity'?
  if BaseCapacities.Count > 0 then
  begin
    CrossJoinCapacities(0, BaseResources, BaseCapacities);
    BaseResources.Free;
    BaseCapacities.Free;
  end else
    Paths.Add(TEzResourcePath.Create(BaseResources, BaseCapacities));

  Paths.Sort(SpeedFactorComparer);
end;

// Returns True when this service holds resource assignments besides
// resources assigned to the task to be scheduled
function TTaskScheduleService.ContainsResourceAssigments: Boolean;
begin
  Result := False;
end;

procedure TTaskScheduleService.BeforeDestruction;
begin
  inherited;
  _ProfilesToDestroy.Free;
end;

procedure TTaskScheduleService.GetResourceScheduleProfile(
  Task: TEzScheduleTask;
  Interval: TEzScheduleInterval;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource);

begin
  if Assigned(_Owner.FOnGetResourceScheduleProfile) then
    _Owner.FOnGetResourceScheduleProfile(Task, Interval, Assignment, Resource);

  if Assignment.ScheduleProfile = nil then
  begin
    // if schedule type == calendar or if constraint == MFO or MSO
    // then we ignore any task already scheduled and schedule
    // this task against the original resource availability.
    if (Interval.ScheduleFlag=ifResourceDefaultAvailability) or (Task.Constraint in [cnMSO, cnMFO]) then
      Assignment.ScheduleProfile := Resource.Availability else
      Assignment.ScheduleProfile := Resource.ActualAvailability;
  end;
end;

function TTaskScheduleService.GetResourceAllocationProfile(
  Task: TEzScheduleTask;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource) : TEzAvailabilityProfile;

begin
  Result := nil;

  if Assigned(_Owner.FOnGetResourceAllocationProfile) then
    _Owner.FOnGetResourceAllocationProfile(Task, Assignment, Resource, Result);

  if Result = nil then
    Result := Resource.ActualAvailability;
end;

function TTaskScheduleService.GetResourceOriginalScheduleProfile(
  Assignment: TEzResourceAssignment;
  Resource: TEzResource): TEzAvailabilityProfile;
begin
  Result := Assignment.ScheduleProfile;
end;

procedure TTaskScheduleService.AllocateResources(
  Task: TEzScheduleTask;
  Intervals: TEzTimespanArray;
  ResourcePath: TEzResourcePath);

var
  CapacityKey: TEzCapacityKey;
  LocalWorkInterval: TEzWorkInterval;
  WorkIntervals: TEzWorkIntervals;
  MessageAdded: Boolean;

  procedure AllocateInterval(
      Assignment: TEzResourceAssignment;
      Profile: TEzAvailabilityProfile;
      Resource: TEzResource;
      Interval: TEzTimeSpan);
  var
    OverAllocated: Boolean;
    s: string;

  begin
  {$IFDEF DEBUG}
    s := DateTimeToStr(EzDateTimeToDateTime(Interval.Start));
    if s = '' then;
    s := DateTimeToStr(EzDateTimeToDateTime(Interval.Stop));
    if s = '' then;

  {$ENDIF}
    if (Interval.Start<Interval.Stop) then
    begin
      if Task.ResourceAllocation=alProfile then
      begin
        // When scheduling to a resource profile,
        // all availability is consumed.
        Profile.SetInterval(Interval.Start, Interval.Stop, 0, [afIsAllocated])
      end
      else
      begin
        OverAllocated := Profile.AllocateInterval(Interval.Start, Interval.Stop, Assignment.Assigned, True);
        if not Assignment.IgnoreOverallocations and OverAllocated and not MessageAdded then
        begin
          MessageAdded := True;
          AddScheduleMessage(Task, Format(SResourceOverallocated, [Resource.Name]));
        end;
      end;
    end;
  end;

  procedure AllocateWorkInterval(WorkInterval: TEzWorkInterval; ResourceAssignment: TEzResourceAssignment);
  var
    Profile: TEzAvailabilityProfile;
    Resource: TEzResource;

  begin
    // ignore resource when assignement equals 0
    if (Task.ResourceAllocation=alFixed) and (ResourceAssignment.Assigned<=0.0) then
      Exit;

    Resource := _Owner.Resources.ResourceByKey(ResourceAssignment.Key);

    // Profile := Resource.ActualAvailability;
    // KV: 30-11-2011
    Profile := GetResourceAllocationProfile(Task, ResourceAssignment, Resource);
    // KV: 6-12-2011 Back to original implementation
    // Profile := Resource.ActualAvailability;

    // Because of the MSO constraint, the task has been scheduled against
    // the availability profile of the resource instead of against the
    // actual availability profile. Therefore the actual availability profile
    // has not been prepared.
    if (Task.Constraint in [cnMSO, cnMFO]) then
      Profile.PrepareDateRange(WorkInterval.TimeSpan);

    AllocateInterval(ResourceAssignment, Profile, Resource, WorkInterval.TimeSpan);
  end;

  function SelectBestResource(ResourceIndex: Integer; WorkInterval: TEzWorkInterval): Boolean;
  var
    av_point, av_next: TEzAvailabilityPoint;
    i_point: Integer;
    bestIndex: Integer;
    Resource: TEzResource;
    ResourceAssignment: TEzResourceAssignment;
    Stop: TEzDateTime;
    Profile: TEzAvailabilityProfile;

  begin
    // Result indicates whether interval has been split
    Result := False;
    Stop := WorkInterval.TimeSpan.Start;
    bestIndex := -1;

    repeat
      ResourceAssignment := ResourcePath.Resources[ResourceIndex];

      Resource := _Owner.Resources.ResourceByKey(ResourceAssignment.Key);
      Profile := GetResourceOriginalScheduleProfile(ResourceAssignment, Resource);
//      Profile := ResourceAssignment.ScheduleProfile;

      if not Profile.IndexOf(i_point, WorkInterval.TimeSpan.Start) then
        dec(i_point);

      av_point := Profile[i_point];
      if (av_point.Units >= ResourceAssignment.Assigned) then
      begin
        if i_point < Profile.Count - 1 then
          av_next := Profile[i_point + 1] else
          av_next := nil;

        if (av_next = nil) or (av_next.DateValue >= WorkInterval.TimeSpan.Stop) then
        begin
          if ReturnWorkIntervals then
            // This resource can work the whole interval.
            // No need to look any further.
            WorkInterval.ResourceAssigments.Add(TEzResourceAssignment.Create(ResourceAssignment));

          AllocateWorkInterval(WorkInterval, ResourceAssignment);

          Exit;
        end;

        if av_next.DateValue > Stop then
        begin
          bestIndex := ResourceIndex;
          Stop := av_next.DateValue;
        end;
      end;

      inc(ResourceIndex);
    until (ResourceIndex = ResourcePath.Resources.Count) or
          (CapacityKey <> ResourcePath.Resources[ResourceIndex].CapacityKey);

    Assert(
        (bestIndex >= 0) and
        (bestIndex < ResourcePath.Resources.Count) and
        (WorkInterval.TimeSpan.Start<Stop));

    // Interval has been split
    Result := True;
    ResourceAssignment := ResourcePath.Resources[bestIndex];

    if ReturnWorkIntervals then
      WorkInterval.ResourceAssigments.Add(TEzResourceAssignment.Create(ResourceAssignment));

    WorkInterval.TimeSpan.Stop := Stop;
    AllocateWorkInterval(WorkInterval, ResourceAssignment);
  end;

  function SetupWorkInterval(ts: TEzTimeSpan; AllocResources: Boolean): TEzWorkInterval;
  begin
    if ReturnWorkIntervals then
    begin
      Result := TEzWorkInterval.Create;
      Result.TimeSpan := ts;
      if AllocResources then
        Result.ResourceAssigments := TEzResourceAssignments.Create(True);
      WorkIntervals.Add(Result);
    end
    else
    begin
      if LocalWorkInterval=nil then
      begin
        LocalWorkInterval := TEzWorkInterval.Create;
        if AllocResources then
          LocalWorkInterval.ResourceAssigments := TEzResourceAssignments.Create(True);
      end;

      Result := LocalWorkInterval;
      Result.TimeSpan := ts;
    end;
  end;

  procedure BuildWorkIntervalList;
  var
    i_int, i_res: Integer;
    IntervalHasBeenSplit: Boolean;
    Stop: TEzDateTime;
    ResourceAssignment: TEzResourceAssignment;
    WorkInterval: TEzWorkInterval;
  begin
    i_int := 0;
    Stop := 0;
    IntervalHasBeenSplit := False;

    while i_int < Intervals.Count do
    begin
      WorkInterval := SetupWorkInterval(Intervals[i_int], True);
      if IntervalHasBeenSplit then
        WorkInterval.TimeSpan.Start := Stop;
      Stop := 0;
      IntervalHasBeenSplit := False;
        
      i_res := 0;
      while i_res < ResourcePath.Resources.Count do
      begin
        ResourceAssignment := ResourcePath.Resources[i_res];
        CapacityKey := ResourceAssignment.CapacityKey;

        // Merged profile?
        if (i_res < ResourcePath.Resources.Count - 1) and
           (CapacityKey = ResourcePath.Resources[i_res + 1].CapacityKey) and
           (CapacityKey <> UnAssigned) //NEW 31 AOUT 2011 DP - IF CapacityKey IS UNASSIGNED THEN IT IS NOT A MERGED PROFILE.
        then
        begin
          if SelectBestResource(i_res, WorkInterval) then
          begin
            IntervalHasBeenSplit := True;
            if (Stop > WorkInterval.TimeSpan.Stop)
            or (Stop = 0 )
            then
              Stop := WorkInterval.TimeSpan.Stop;
          end;

          // Skip other resources supporting the same capacity
          while (i_res < ResourcePath.Resources.Count) and (CapacityKey = ResourcePath.Resources[i_res].CapacityKey) do
            inc(i_res);
        end
        else
        begin
          //MOVED DOWN 15 FEV 2012 DP - ON DOIT FAIRE LE ALLOCATE WORK INTERVAL APRES AVOIR DETERMINER LE TIMESPAN LE PLUS PETIT
          //if ReturnWorkIntervals then
          //  WorkInterval.ResourceAssigments.Add(TEzResourceAssignment.Create(ResourceAssignment));
          //AllocateWorkInterval(WorkInterval, ResourceAssignment);
          inc(i_res);
        end;
      end;
      //MOVED DOWN 15 FEV 2012 DP - ON DOIT FAIRE LE ALLOCATE WORK INTERVAL des ressource pas merged APRES AVOIR DETERMINER LE TIMESPAN LE PLUS PETIT
      i_res := 0;
      while i_res < ResourcePath.Resources.Count do
      begin
        ResourceAssignment := ResourcePath.Resources[i_res];
        CapacityKey := ResourceAssignment.CapacityKey;
        if (i_res < ResourcePath.Resources.Count - 1) and
           (CapacityKey <> ResourcePath.Resources[i_res + 1].CapacityKey) and
           (CapacityKey <> UnAssigned) //NEW 31 AOUT 2011 DP - IF CapacityKey IS UNASSIGNED THEN IT IS NOT A MERGED PROFILE.
        then
        begin
          if ReturnWorkIntervals then
            WorkInterval.ResourceAssigments.Add(TEzResourceAssignment.Create(ResourceAssignment));
          AllocateWorkInterval(WorkInterval, ResourceAssignment);
        end;
        inc(i_res);
      end;

      if not IntervalHasBeenSplit then
        inc(i_int);
    end;
  end;

  procedure AllocateIntervals;
  var
    i_int, i_res: Integer;
    Interval: TEzTimespan;
    Profile: TEzAvailabilityProfile;
    Resource: TEzResource;
    ResourceAssignment: TEzResourceAssignment;

  begin
      for i_res:=0 to ResourcePath.Resources.Count-1 do
      begin
        ResourceAssignment := ResourcePath.Resources[i_res];

        // ignore resource when assignement equals 0
        if (Task.ResourceAllocation=alFixed) and (ResourceAssignment.Assigned<=0.0) then
          break;

        Resource := _Owner.Resources.ResourceByKey(ResourceAssignment.Key);
        Profile := GetResourceAllocationProfile(Task, ResourceAssignment, Resource);

        for i_int:=0 to Intervals.Count-1 do
        begin
          Interval := Intervals[i_int];
          if ReturnWorkIntervals then
            SetupWorkInterval(Intervals[i_int], False);
          AllocateInterval(ResourceAssignment, Profile, Resource, Interval);
        end;
      end;
  end;

var
  i_ass: Integer;

begin
  // Return when the path is empty
  // Empty paths exists when a group of tasks is scheduled and a parent task
  // does not have any resources assigned.
  if ResourcePath.Resources.Count = 0 then
    Exit;

  MessageAdded := False;
  LocalWorkInterval := nil;

  if ReturnWorkIntervals then
  begin
    WorkIntervals := TEzWorkIntervals.Create(True);
    SaveWorkIntervals(Task, WorkIntervals);
  end;

  try
    if srTaskHasMergedProfiles in Task.Flags then
      BuildWorkIntervalList
    else
    begin
      AllocateIntervals;
      if ReturnWorkIntervals and (WorkIntervals.Count > 0) then
      begin
        WorkIntervals[0].ResourceAssigments := TEzResourceAssignments.Create(True);
        for i_ass := 0 to ResourcePath.Resources.Count - 1 do
          WorkIntervals[0].ResourceAssigments.Add(TEzResourceAssignment.Create(ResourcePath.Resources[i_ass]));
      end;
    end;
  finally
    LocalWorkInterval.Free;
  end;
end;

procedure TTaskScheduleService.AddAssigmentToProfile(
  Task: TEzScheduleTask;
  Interval: TEzScheduleInterval;
  Profile: TEzAvailabilityProfile;
  Assignment: TEzResourceAssignment);
var
  Resource: TEzResource;
  resourceProfile: TEzAvailabilityProfile;

begin
  Resource := _Owner.Resources.ResourceByKey(Assignment.Key);

  if (Task.ResourceAllocation=alFixed) and (Assignment.Assigned<=0.0) then
    // Assignment should be greater than 0
    AddScheduleMessage(Task, Format(SResourceAssignmentIsNull, [Resource.Name]))

  else
  begin
    Resource.ProfileRequired;

    // if schedule type == calendar or if constraint == MFO or MSO
    // then we ignore any task already scheduled and schedule
    // this task against the original resource availability.
//    if (Interval.ScheduleFlag=ifResourceDefaultAvailability) or (Task.Constraint in [cnMSO, cnMFO]) then
//    begin
//      useProfile := GetResourceAvailabilityProfile(Resource);
//      useProfile.UnitsRequired := Assignment.Assigned;
//      useProfile.AffectsDuration := Assignment.AffectsDuration;
//      Profile.AddSource(useProfile, False)
//    end
//    else
//    begin
//      useProfile := GetResourceActualAvailabilityProfile(Resource);
//      useProfile.AllowTaskSwitching := Resource.AllowTaskSwitching;
//      useProfile.UnitsRequired := Assignment.Assigned;
//      useProfile.AffectsDuration := Assignment.AffectsDuration;
//      Profile.AddSource(useProfile, False);
//    end;

    // Fills Assignment.ScheduleProfile
    GetResourceScheduleProfile( Task,
                                Interval,
                                Assignment,
                                Resource);

    resourceProfile := Assignment.ScheduleProfile;
    if (Interval.ScheduleFlag <> ifResourceDefaultAvailability) and not (Task.Constraint in [cnMSO, cnMFO]) then
      resourceProfile.AllowTaskSwitching := Resource.AllowTaskSwitching;

    resourceProfile.UnitsRequired := Assignment.Assigned;
    resourceProfile.AffectsDuration := Assignment.AffectsDuration;
    Profile.AddSource(resourceProfile, False);
  end;
end;

procedure TTaskScheduleService.AddMergedProfilesToProfile(
  Task: TEzScheduleTask;
  Interval: TEzScheduleInterval;
  Profile: TEzAvailabilityProfile;
  Path: TEzResourcePath;
  var PathHasMergedProfile: Boolean;
  var i_res: Integer;
  I_bIsFirstMergedProfile : Boolean);//NEW 30 MAI 2012 DP - SOME MERGED PROFILE WHERE GETTING UNITS REQUIRED = 0
var
  MergedProfile: TEzAvailabilityProfile;
  ResourceAssignment: TEzResourceAssignment;
begin
  PathHasMergedProfile := True;
  MergedProfile := TEzAvailabilityProfile.Create;
  _ProfilesToDestroy.Add(MergedProfile);

  MergedProfile.ScheduleProfileBaseUnits := 0.0;
  MergedProfile.Operator := opMergeScheduleProfile;

  repeat
    ResourceAssignment := Path.Resources[i_res];
    AddAssigmentToProfile(Task, Interval, MergedProfile, ResourceAssignment);
    //AS WAS 30 MAI 2012 DP - SOME MERGED PROFILE WHERE GETTING UNITS REQUIRED = 0
    //if i_res = 0 then
    //AS IS 30 MAI 2012 DP - SOME MERGED PROFILE WHERE GETTING UNITS REQUIRED = 0
    if I_bIsFirstMergedProfile then
      // ResourceAssignment.Assigned the same for all resources.
      // We therefore set it only once
      MergedProfile.UnitsRequired := ResourceAssignment.Assigned;

    inc(i_res);
  until (i_res = Path.Resources.Count) or (Path.Resources[i_res].CapacityKey <> ResourceAssignment.CapacityKey);

  MergedProfile.AffectsDuration := ResourceAssignment.AffectsDuration;
  Profile.AddSource(MergedProfile, False);
end;

procedure TTaskScheduleService.AddScheduleMessage(AMessage: TEzScheduleMessage);
begin
  _Owner.AddScheduleMessage(AMessage);
end;

procedure TTaskScheduleService.AddScheduleMessage(
  Task: TEzScheduleTask;
  AMessage: string);
begin
  _Owner.AddScheduleMessage(Task, AMessage);
end;

function TTaskScheduleService.PrepareResourceProfile(
  Task: TEzScheduleTask;
  Interval: TEzScheduleInterval;
  Path: TEzResourcePath;
  var PathHasMergedProfile: Boolean) : TEzAvailabilityProfile;
var
  i_res: Integer;
  Assignment: TEzResourceAssignment;
  //NEW 30 MAI 2012 DP - SOME MERGED PROFILE WHERE GETTING UNITS REQUIRED = 0
  bIsFirstMergedProfile : Boolean;
begin
  Result := TEzAvailabilityProfile.Create;
  Result.Operator := opScheduleProfile;

  i_res := 0;
  while i_res < Path.Resources.Count do
  begin
    Assignment := Path.Resources[i_res];

    if (i_res < Path.Resources.Count - 1) and
       (not VarIsEmpty(Assignment.CapacityKey)) and
       (Path.Resources[i_res + 1].CapacityKey = Assignment.CapacityKey)
    then
    begin
      //NEW 30 MAI 2012 DP - SOME MERGED PROFILE WHERE GETTING UNITS REQUIRED = 0
      bIsFirstMergedProfile := (i_res = 0) or (Path.Resources[i_res - 1].CapacityKey <> Assignment.CapacityKey);
      AddMergedProfilesToProfile( Task,
                                  Interval,
                                  Result,
                                  Path,
                                  PathHasMergedProfile,
                                  i_res,
                                  bIsFirstMergedProfile);//NEW 30 MAI 2012 DP - SOME MERGED PROFILE WHERE GETTING UNITS REQUIRED = 0
    end
    else
    begin
      AddAssigmentToProfile(Task, Interval, Result, Assignment);
      inc(i_res);
    end;
  end;
end;

function TTaskScheduleService.ReturnWorkIntervals: Boolean;
begin
  Result := _owner.ReturnWorkIntervals;
end;

procedure TTaskScheduleService.SaveWorkIntervals(Task: TEzScheduleTask;
    WorkIntervals: TEzWorkIntervals);
begin
  Task.WorkIntervals := WorkIntervals;
end;

{ TGroupScheduleService }
procedure TGroupScheduleService.CrossJoinProfiles(Task: TEzScheduleTask; Paths: TEzResourcePathList);
var
  allocation: TTemporaryAllocation;
begin
  // Return resources selected from a previous run?
  if _savedAllocations <> nil then
  begin
    allocation := _savedAllocations.Find(Task);
    Paths.Add(allocation.ResourcePath.Clone);
  end
  else if _parent <> nil then
    _parent.CrossJoinProfiles(Task, Paths)
  else
    inherited;
end;

// Returns True when this service holds resource assignments besides
// resources assigned to the task to be scheduled
function TGroupScheduleService.ContainsResourceAssigments: Boolean;
begin
  Result := ((_resourcePath <> nil) and (_resourcePath.Resources.Count > 0)) or
            ((_parent <> nil) and _parent.ContainsResourceAssigments);
end;

procedure TGroupScheduleService.AddScheduleMessage(AMessage: TEzScheduleMessage);
begin
  // Cache message for now....
  _scheduleMessages.Add(AMessage);
end;

procedure TGroupScheduleService.AddScheduleMessage(
  Task: TEzScheduleTask;
  AMessage: string);
begin
  // Cache message for now....
  _scheduleMessages.Add(_Owner.CreateScheduleMessage(Task, Null, AMessage));
end;

procedure TGroupScheduleService.AllocateResources(
  Task: TEzScheduleTask;
  Intervals: TEzTimespanArray;
  ResourcePath: TEzResourcePath);

  function CloneIntervals: TEzTimespanArray;
  begin
    Result := TEzTimespanArray.Create(Intervals.Count);
    Result.Assign(Intervals);
  end;

var
  allocation: TTemporaryAllocation;

begin
  inherited;

  // KV:
  // Uncomment next line if we want to prevent child tasks to run in parallel
  // when the parent has a resource assigned as well.
  // Without this line, child tasks may run in parallel if they do not share a resource.

  // Allocate all parent resources as well
  // These resources have been added to the schedule profile but are not actually
  // in the resource list of the task. We therefore need to allocate them manually.
  // Remember: this is only a temporary allocation!
//  if _ResourcePath <> nil then
//    inherited AllocateResources(Task, Intervals, _ResourcePath);

  // Remember all allocations, we might need to commit them later on
  if not _comitting then
  begin
    allocation := TTemporaryAllocation.Create;
    allocation.Task := Task;
    allocation.Intervals := CloneIntervals;
    allocation.ResourcePath := ResourcePath.Clone;
    _tempAllocations.Add(allocation);
  end;
end;

function TGroupScheduleService.ReturnWorkIntervals: Boolean;
begin
  Result := inherited ReturnWorkIntervals;
//  Result := True;
end;

procedure TGroupScheduleService.SaveWorkIntervals(Task: TEzScheduleTask; WorkIntervals: TEzWorkIntervals);
begin
  inherited;
//  _savedWorkIntervals := WorkIntervals;
end;

procedure TGroupScheduleService.BeforeDestruction;
begin
  inherited;
  _scheduleMessages.Free;
  _tempAllocationProfiles.Free;
  _tempAllocations.Free;
  _savedAllocations.Free;
end;

procedure TGroupScheduleService.Commit;
var
  allocation: TTemporaryAllocation;
  i: Integer;
  intervals: TEzTimespanArray;
begin
//  // Commit from saved WorkIntervals
//  if _savedWorkIntervals = nil then
//    Exit;
//
//  for i := 0 to _savedWorkIntervals.Count - 1 do
//  begin
//
//  end;
//
//
//Exit;
//
  _comitting := True;

  for i := 0  to _tempAllocations.Count -  1 do
  begin
    allocation := _tempAllocations[i];

    if _parent <> nil then
      _parent.AllocateResources(  allocation.Task,
                                  allocation.Intervals,
                                  allocation.ResourcePath)
    else
    begin
      allocation.Task.Flags := allocation.Task.Flags + [srIntervaldataChanged];

      // Call inherited because we do want to clone this allocation
      // again (see TGroupScheduleService.AllocateResources).
      inherited AllocateResources(  allocation.Task,
                                    allocation.Intervals,
                                    allocation.ResourcePath);
    end;
  end;

  // Allocate parent resources
  // _ResourcePath holds an empty path if this (parent-)task does not have any resources
  if (_ResourcePath <> nil) then
  begin
    intervals := TEzTimespanArray.Create(1);
    intervals.Add(_outerWindow);

    if _parent <> nil then
      _parent.AllocateResources(  _parentTask,
                                  intervals,
                                  _ResourcePath)
    else
      // Call inherited because we do want to clone this allocation
      // again (see TGroupScheduleService.AllocateResources).
      inherited AllocateResources(  _parentTask,
                                    intervals,
                                   _ResourcePath);

    intervals.Free;
  end;

  SendScheduleMessages;

  _comitting := False;
end;

constructor TGroupScheduleService.Create(
  AOwner: TEzScheduler;
  ParentTask: TEzScheduleTask;
  const ParentService: IGroupScheduleService);
begin
  inherited Create(AOwner);
  _scheduleMessages := TEzScheduleMessages.Create;
  _parentTask := ParentTask;
  _parent := ParentService;
  _tempAllocationProfiles := TSortedAllocationProfiles.Create(True);
  _tempAllocations := TSortedAllocations.Create(True);
end;

procedure TGroupScheduleService.Restart;
begin
  _tempAllocationProfiles.Clear;

  // Remember selected resources per task
  // We need this information to hard code resource assignments when removing
  // slack from a group of tasks
  _savedAllocations := _tempAllocations;

  _tempAllocations := TSortedAllocations.Create(True);

  _scheduleMessages.Clear;
end;

procedure TGroupScheduleService.SendScheduleMessages;
begin
  while _scheduleMessages.Count > 0 do
  begin
    if _parent <> nil then
      _parent.AddScheduleMessage(_scheduleMessages[0]) else
      inherited AddScheduleMessage(_scheduleMessages[0]);

    _scheduleMessages.Extract(_scheduleMessages[0]);
  end;
end;

procedure TGroupScheduleService.set_OuterWindow(Value: TEzTimespan);
begin
  _outerWindow := Value;
end;

procedure TGroupScheduleService.set_ResourcePath(Value: TEzResourcePath);
var
  i: Integer;

begin
  _ResourcePath := Value;

  // Make sure all resources have their profiles ready
  for i := 0 to _ResourcePath.Resources.Count - 1 do
    _Owner.Resources.ResourceByKey(_ResourcePath.Resources[i].Key).ProfileRequired;
end;

function TGroupScheduleService.get_OuterWindow: TEzTimespan;
begin
  Result := _outerWindow;
end;

function TGroupScheduleService.get_Parent: IGroupScheduleService;
begin
  Result := _parent;
end;

function TGroupScheduleService.get_ResourcePath: TEzResourcePath;
begin
  Result := _ResourcePath;
end;

procedure TGroupScheduleService.GetResourceScheduleProfile(
  Task: TEzScheduleTask;
  Interval: TEzScheduleInterval;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource);

var
  profile: TEzAvailabilityProfile;
  temp: TTemporaryAllocationProfile;

begin
  temp := _tempAllocationProfiles.Find(Resource);
  if temp = nil then
  begin
    if _parent <> nil then
      _parent.GetResourceScheduleProfile(Task, Interval, Assignment, Resource) else
      inherited GetResourceScheduleProfile(Task, Interval, Assignment, Resource);

    // Replace profile with a temporary profile
    profile := Assignment.ScheduleProfile;

    Assignment.ScheduleProfile := TEzAvailabilityProfile.Create;
    Assignment.ScheduleProfile.Operator := opSubstract;
    Assignment.ScheduleProfile.AddSource(profile, False);

    temp := TTemporaryAllocationProfile.Create;
    temp.Resource := Resource;
    temp.Allocation := Assignment.ScheduleProfile;
    temp.ScheduleProfile := profile;
    _tempAllocationProfiles.Add(temp);
  end else
    Assignment.ScheduleProfile := temp.Allocation;
end;

// TGroupScheduleService.
function TGroupScheduleService.GetResourceAllocationProfile(
  Task: TEzScheduleTask;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource) : TEzAvailabilityProfile;

begin
  // Return real profile when comitting
  if _comitting then
  begin
    if _parent <> nil then
      Result := _parent.GetResourceAllocationProfile(Task, Assignment, Resource) else
      Result := inherited GetResourceAllocationProfile(Task, Assignment, Resource);
  end else
    // Return temporary profile during test scheduling
    Result := _tempAllocationProfiles.Find(Resource).Allocation;
end;

function TGroupScheduleService.GetResourceOriginalScheduleProfile(
  Assignment: TEzResourceAssignment;
  Resource: TEzResource): TEzAvailabilityProfile;

var
  temp: TTemporaryAllocationProfile;

begin
  temp := _tempAllocationProfiles.Find(Resource);
  if temp <> nil then
    Result := temp.ScheduleProfile else
    Result := inherited GetResourceOriginalScheduleProfile(Assignment, Resource);
end;

// Prepare a resource schedule profile
// PrepareResourceProfile returns a resource's schedule profile.
// This method first updates the Path by adding all parent resources, then
// calls the inherited PrepareResourceProfile to get the schedule profile, then
// restores the Path.
function TGroupScheduleService.PrepareResourceProfile(
  Task: TEzScheduleTask;
  Interval: TEzScheduleInterval;
  Path: TEzResourcePath;
  var PathHasMergedProfile: Boolean): TEzAvailabilityProfile;

var
  added: Integer;
  i: Integer;
  n: Integer;
  parentPath: TEzResourcePath;
  service: IGroupScheduleService;
begin
  added := 0;
  service := Self;

  while service <> nil do
  begin
    parentPath := service.ResourcePath;
    if (parentPath <> nil) then
    begin
      for n := 0 to parentPath.Resources.Count - 1 do
      begin
        inc(added);
        // Add new object since Path.Resources will release object when removed
        Path.Resources.Add(TEzResourceAssignment.Create(parentPath.Resources[n]));
      end;
    end;

    service := service.Parent;
  end;

  Result := inherited PrepareResourceProfile(Task, Interval, Path, PathHasMergedProfile);

  // Restore original Path
  for i := 1 to added do
    Path.Resources.Delete(Path.Resources.Count - 1);
end;

{ TSortedAllocations }

function TSortedAllocationProfiles.CompareItems(Item1, Item2: TObject): Integer;
var
  i1: TEzResource;
  i2: TEzResource;

begin
  i1 := TTemporaryAllocationProfile(Item1).Resource;
  i2 := TTemporaryAllocationProfile(Item2).Resource;
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key = i2.Key then
    Result := 0
  else
    Result := 1;
end;

function TSortedAllocationProfiles.GetItem(Index: Integer): TTemporaryAllocationProfile;
begin
  Result := TTemporaryAllocationProfile(inherited Items[Index]);
end;

function TSortedAllocationProfiles.Find(Key: TEzResource): TTemporaryAllocationProfile;
var
  tmp: TTemporaryAllocationProfile;
  i: Integer;

begin
  tmp := TTemporaryAllocationProfile.Create;
  try
    tmp.Resource := Key;

    if not IndexOf(i, tmp) then
      Result := nil else
      Result := GetItem(i);
  finally
    tmp.Free;
  end;
end;

{ TSortedAllocations }
function TSortedAllocations.CompareItems(Item1, Item2: TObject): Integer;
var
  i1: TEzScheduleTask;
  i2: TEzScheduleTask;

begin
  i1 := TTemporaryAllocation(Item1).Task;
  i2 := TTemporaryAllocation(Item2).Task;
  Result := CompareNodeKeys(i1.Key, i2.Key);
end;

function TSortedAllocations.Find(Key: TEzScheduleTask): TTemporaryAllocation;
var
  tmp: TTemporaryAllocation;
  i: Integer;

begin
  tmp := TTemporaryAllocation.Create;
  try
    tmp.Task := Key;

    if not IndexOf(i, tmp) then
      Result := nil else
      Result := GetItem(i);
  finally
    tmp.Free;
  end;
end;

function TSortedAllocations.GetItem(Index: Integer): TTemporaryAllocation;
begin
  Result := TTemporaryAllocation(inherited Items[Index]);
end;

{ TTemporaryAllocationProfile }

destructor TTemporaryAllocationProfile.Destroy;
begin
  inherited;
  Allocation.Free;
end;

{ TTemporaryAllocation }
constructor TTemporaryAllocation.Create;
begin
  inherited;
  inc(ccc);
end;

destructor TTemporaryAllocation.Destroy;
begin
  dec(ccc);
  inherited;
  Intervals.Free;
  ResourcePath.Free;
end;

{ TScheduleBranch }

destructor TScheduleBranch.Destroy;
var
  i: Integer;
  obj: TObject;
begin
  for i := 0 to Count - 1 do
  begin
    obj := Self[i];
    if obj is TScheduleBranch then
      obj.Free;
  end;

  inherited;
end;

end.
