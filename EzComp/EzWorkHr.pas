unit EzWorkHr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, EzCalendar;

type
  TfrmSetWorkingHours = class(TForm)
    GdWorkingHours: TStringGrid;
    Label1: TLabel;
    btnOK: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure GdWorkingHoursSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    procedure AssignRuleSet(Rules: TEzCalendarRules);
    procedure UpdateRulesSet(Rules: TEzCalendarRules);

    procedure ClearGrid;
  end;

var
  frmSetWorkingHours: TfrmSetWorkingHours;

implementation

{$R *.DFM}

uses Math;

function SafeReadTime(S: string): TDateTime;
begin
  try
    Result := StrToTime(S);
  except
    // Try reading something like 24:00
    if (copy(S, 1, 2) = '24') then
      Result := 1
    // Try reading something like 00:00
    else if (copy(S, 1, 2) = '00') then
      Result := 1
    else
      raise;
  end;
end;

function TimeTo24String(T: TDateTime): string;
begin
  if Frac(T) = 0.0 then
    Result := '24:00:00' else
    Result := TimeToStr(T);
end;

procedure TfrmSetWorkingHours.FormCreate(Sender: TObject);
begin
  ClearGrid;
end;

procedure TfrmSetWorkingHours.ClearGrid;
begin
  with GdWorkingHours do
  begin
    RowCount := 2;
    GdWorkingHours.Rows[0].CommaText := 'From,To';
    GdWorkingHours.Cells[0,1] := '';
    GdWorkingHours.Cells[1,1] := '';
  end;
end;

procedure TfrmSetWorkingHours.AssignRuleSet(Rules: TEzCalendarRules);
var
  l: TList;
  i: Integer;

begin
  ClearGrid;
  l := TList.Create;
  try
    Rules.GetRules(0, 0, [crtWorkingHour], l);
    GdWorkingHours.RowCount := l.Count+2;
    for i:=0 to l.Count-1 do
    begin
      GdWorkingHours.Cells[0,i+1] := TimeToStr(TEzCalendarRule(l[i]).Start);
      GdWorkingHours.Cells[1,i+1] := TimeTo24String(TEzCalendarRule(l[i]).Stop);
    end;

    // Empty last cell
    GdWorkingHours.Cells[0,GdWorkingHours.RowCount-1] := '';
    GdWorkingHours.Cells[1,GdWorkingHours.RowCount-1] := '';
  finally
    l.Free;
  end;
end;

procedure TfrmSetWorkingHours.UpdateRulesSet(Rules: TEzCalendarRules);
var
  i: Integer;
  Rule: TEzCalendarRule;

begin
  // Cleanup all working hour rules from Ruleset
  i := 0;
  while i<Rules.Count do
    if Rules[i].RuleType = crtWorkingHour then
      Rules.Delete(i) else
      inc(i);

  for i:=1 to GdWorkingHours.RowCount-1 do
    if (GdWorkingHours.Cells[0,i]<>'') and (GdWorkingHours.Cells[1,i]<>'') then
    begin
      Rule := TEzCalendarRule.Create;
      Rule.Start := EncodeWorkingHour(SafeReadTime(GdWorkingHours.Cells[0,i]));
      Rule.Stop := EncodeWorkingHour(SafeReadTime(GdWorkingHours.Cells[1,i]));
      Rule.RuleType := crtWorkingHour;
      Rule.IntervalType := itWorking;
      Rules.Add(Rule);
    end;
end;

procedure TfrmSetWorkingHours.GdWorkingHoursSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
begin
  with GdWorkingHours do
  begin
    if (Cells[Col, Row] <> '') then
    begin
      if Col = 0 then
        Cells[Col, Row] := TimeToStr(SafeReadTime(Cells[Col, Row])) else
        Cells[Col, Row] := TimeTo24String(SafeReadTime(Cells[Col, Row]));
    end;
    if ((Cells[0, RowCount-1] <> '') or (Cells[1, RowCount-1] <> '')) then
    begin
      RowCount := RowCount + 1;
      Cells[0, RowCount-1] := '';
      Cells[1, RowCount-1] := '';
    end;
  end;
end;

procedure TfrmSetWorkingHours.FormDestroy(Sender: TObject);
begin
  if frmSetWorkingHours=Self then
    frmSetWorkingHours:=nil;
end;

end.
