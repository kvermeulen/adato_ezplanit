object EzEndPointDesigner: TEzEndPointDesigner
  Left = 462
  Top = 266
  Caption = 'Endpoint designer'
  ClientHeight = 345
  ClientWidth = 578
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 19
    Top = 12
    Width = 74
    Height = 13
    Caption = 'Select endpoint'
  end
  object Label8: TLabel
    Left = 5
    Top = 40
    Width = 88
    Height = 13
    Caption = 'Image for endpoint'
  end
  object cbEndPoints: TComboBox
    Left = 96
    Top = 8
    Width = 185
    Height = 21
    Style = csDropDownList
    ItemHeight = 0
    TabOrder = 0
    OnChange = cbEndPointsChange
  end
  object gbImageProperties: TGroupBox
    Left = 8
    Top = 96
    Width = 377
    Height = 209
    Caption = 'Image properties'
    Enabled = False
    TabOrder = 1
    object pnlImageProperties: TPanel
      Left = 10
      Top = 20
      Width = 150
      Height = 150
      Color = clWhite
      TabOrder = 0
      object pbSampleImage: TPaintBox
        Left = 1
        Top = 1
        Width = 148
        Height = 148
        Cursor = crCross
        Align = alClient
        OnMouseDown = pbSampleImageMouseDown
        OnPaint = pbSampleImagePaint
      end
    end
    object rgActiveHotSpot: TRadioGroup
      Left = 168
      Top = 15
      Width = 201
      Height = 185
      Items.Strings = (
        ''
        ''
        '')
      TabOrder = 4
    end
    object GroupBox1: TGroupBox
      Left = 200
      Top = 32
      Width = 161
      Height = 49
      Caption = 'Hot spot'
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 20
        Width = 9
        Height = 13
        Caption = 'X'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 84
        Top = 20
        Width = 9
        Height = 13
        Caption = 'Y'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edHotSpotX: TEdit
        Left = 24
        Top = 16
        Width = 33
        Height = 21
        TabOrder = 0
        Text = '0'
        OnChange = edHotSpotXChange
      end
      object udHotSpotX: TUpDown
        Left = 57
        Top = 16
        Width = 16
        Height = 21
        Associate = edHotSpotX
        Min = -1
        TabOrder = 1
      end
      object edHotSpotY: TEdit
        Left = 96
        Top = 16
        Width = 33
        Height = 21
        TabOrder = 2
        Text = '0'
        OnChange = edHotSpotYChange
      end
      object udHotSpotY: TUpDown
        Left = 129
        Top = 16
        Width = 16
        Height = 21
        Associate = edHotSpotY
        Min = -100
        TabOrder = 3
      end
    end
    object GroupBox2: TGroupBox
      Left = 200
      Top = 88
      Width = 161
      Height = 49
      Caption = 'Left connection point'
      TabOrder = 2
      object Label3: TLabel
        Left = 8
        Top = 20
        Width = 9
        Height = 13
        Caption = 'X'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 84
        Top = 20
        Width = 9
        Height = 13
        Caption = 'Y'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edLCPx: TEdit
        Left = 24
        Top = 16
        Width = 33
        Height = 21
        TabOrder = 0
        Text = '0'
        OnChange = edLCPxChange
      end
      object udLeftCPx: TUpDown
        Left = 57
        Top = 16
        Width = 16
        Height = 21
        Associate = edLCPx
        Min = -1
        TabOrder = 1
      end
      object edLCPy: TEdit
        Left = 96
        Top = 16
        Width = 33
        Height = 21
        TabOrder = 2
        Text = '0'
        OnChange = edLCPyChange
      end
      object udLeftCPy: TUpDown
        Left = 129
        Top = 16
        Width = 16
        Height = 21
        Associate = edLCPy
        Min = -100
        TabOrder = 3
      end
    end
    object GroupBox3: TGroupBox
      Left = 200
      Top = 144
      Width = 161
      Height = 49
      Caption = 'Right connection point'
      TabOrder = 3
      object Label5: TLabel
        Left = 8
        Top = 20
        Width = 9
        Height = 13
        Caption = 'X'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 84
        Top = 20
        Width = 9
        Height = 13
        Caption = 'Y'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edRCPx: TEdit
        Left = 24
        Top = 16
        Width = 33
        Height = 21
        TabOrder = 0
        Text = '0'
        OnChange = edRCPxChange
      end
      object udRightCPx: TUpDown
        Left = 57
        Top = 16
        Width = 16
        Height = 21
        Associate = edRCPx
        Min = -1
        TabOrder = 1
      end
      object edRCPy: TEdit
        Left = 96
        Top = 16
        Width = 33
        Height = 21
        TabOrder = 2
        Text = '0'
        OnChange = edRCPyChange
      end
      object udRightCPy: TUpDown
        Left = 129
        Top = 16
        Width = 16
        Height = 21
        Associate = edRCPy
        Min = -100
        TabOrder = 3
      end
    end
  end
  object btnUndo: TButton
    Left = 326
    Top = 312
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Undo'
    Enabled = False
    TabOrder = 2
    OnClick = btnUndoClick
  end
  object btnApply: TButton
    Left = 407
    Top = 314
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Apply'
    Enabled = False
    TabOrder = 3
    OnClick = btnApplyClick
  end
  object btnClose: TButton
    Left = 495
    Top = 314
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Close'
    TabOrder = 4
    OnClick = btnCloseClick
  end
  object btnGenerate: TButton
    Left = 310
    Top = 63
    Width = 75
    Height = 25
    Hint = 'Creates an endpoint for every image in the imagelist.'
    Caption = '&Generate'
    TabOrder = 5
    OnClick = btnGenerateClick
  end
  object btnAdd: TButton
    Left = 310
    Top = 8
    Width = 75
    Height = 25
    Hint = 'Adds a new endpoint to the collection'
    Caption = '&Add'
    TabOrder = 6
    OnClick = btnAddClick
  end
  object btnDelete: TButton
    Left = 310
    Top = 36
    Width = 75
    Height = 25
    Hint = 'Removes selected endpoint from the collection'
    Caption = '&Delete'
    Enabled = False
    TabOrder = 7
    OnClick = btnDeleteClick
  end
  object cbImages: TComboBox
    Left = 96
    Top = 40
    Width = 185
    Height = 22
    Style = csOwnerDrawFixed
    Enabled = False
    ItemHeight = 16
    TabOrder = 8
    OnChange = cbImagesChange
    OnDrawItem = cbImagesDrawItem
  end
  object gbSamples: TGroupBox
    Left = 392
    Top = 8
    Width = 177
    Height = 297
    Caption = 'Samples'
    TabOrder = 9
    object Label9: TLabel
      Left = 33
      Top = 192
      Width = 120
      Height = 26
      Alignment = taCenter
      Caption = 'Sample of endpoint used '#13#10'with predecessor lines.'
    end
    object Label10: TLabel
      Left = 25
      Top = 16
      Width = 120
      Height = 26
      Alignment = taCenter
      Caption = 'Sample of endpoint used '#13#10'with a gantbar.'
    end
    object Label11: TLabel
      Left = 26
      Top = 104
      Width = 120
      Height = 26
      Alignment = taCenter
      Caption = 'Sample of endpoint used '#13#10'as a milestone.'
    end
    object pnlSampleGanttBar: TPanel
      Left = 13
      Top = 45
      Width = 150
      Height = 41
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 0
      object imgSampleBar: TImage
        Left = 0
        Top = 0
        Width = 150
        Height = 41
        Align = alClient
      end
    end
    object pnlMilestoneSample: TPanel
      Left = 61
      Top = 133
      Width = 55
      Height = 41
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 1
      object imgSampleMileStone: TImage
        Left = 0
        Top = 0
        Width = 55
        Height = 41
        Align = alClient
      end
    end
    object pnlPredecessorlineSample: TPanel
      Left = 35
      Top = 222
      Width = 108
      Height = 41
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 2
      object imgSamplePredLine: TImage
        Left = 0
        Top = 0
        Width = 108
        Height = 41
        Align = alClient
      end
    end
  end
end
