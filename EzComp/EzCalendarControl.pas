unit EzCalendarControl;

interface

uses Windows,
     Messages,
     Graphics,
     Forms,
     SysUtils,
     DesignPanel,
     Classes,
     ActnList,
     StdCtrls,
     Controls,
     ComCtrls,
     Grids,
     EzCalendar,
     EzCalendarGrid,
     EzArrays,
     ExtCtrls;

type
  TEzCalendarPanel = class;

  TOnRuleChangeEvent = procedure(Sender: TEzCalendarPanel; Event: TEzListNotification; Rule: TEzCalendarRule) of object;

  TEzCalendarRuleNotifier = class(TEzListNotifier)
  protected
    FOwner: TEzCalendarPanel;
    procedure Notify(AObject: TObject; Action: TEzListNotification); override;

  public
    constructor Create(AOwner: TEzCalendarPanel; Rules: TEzCalendarRules); virtual;
  end;

  TEzCalendarPanel = class(TDesignPanel)
    ActionList1: TActionList;
    acAddRule: TAction;
    acDeleteRule: TAction;
    acSetWorkingHours: TAction;
    pnlOuter: TPanel;
    gbDaySelector: TGroupBox;
    Label4: TLabel;
    CalendarGrid: TEzCalendarGrid;
    pnlFill: TPanel;
    pnlRules: TPanel;
    pnlWorkingHours: TPanel;
    lvRules: TListView;
    pnlBottom: TPanel;
    btnEdit: TButton;
    BtnDeleteRule: TButton;
    btnWorkingHours: TButton;
    dtpDate: TDateTimePicker;
    btnAdd: TButton;
    acEditRule: TAction;
    lbWorkinghoursLabel: TLabel;
    lbWorkingHours: TLabel;
    rbRulesPerDate: TRadioButton;
    rbAllRules: TRadioButton;
    procedure CalendarGridChange(Sender: TObject);
    procedure CalendarGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure acAddRuleExecute(Sender: TObject);
    procedure acDeleteRuleExecute(Sender: TObject);
    procedure acDeleteRuleUpdate(Sender: TObject);
    procedure acSetWorkingHoursExecute(Sender: TObject);
    procedure dtpDateChange(Sender: TObject);
    procedure acEditRuleExecute(Sender: TObject);
    procedure acEditRuleUpdate(Sender: TObject);
    procedure rbAllRulesClick(Sender: TObject);
    procedure rbRulesPerDateClick(Sender: TObject);

  private
    BDummy: Boolean;
    FModified: Boolean;
    FWorkinghourColor: TColor;
    FRules: TEzCalendarRules;
    FLink: TEzCalendarRuleNotifier;
    FAvailability: TEzAvailabilityProfile;
    FUpdateCount: Integer;

    FOnDateChange: TNotifyEvent;
    FOnRuleChanging: TOnRuleChangeEvent;
    FOnRuleChanged: TOnRuleChangeEvent;
    FOnTranslateRule: TOnTranslateRule;

  protected
    procedure CMShowingChanged(var Message: TMessage); message CM_SHOWINGCHANGED;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;

    procedure DoRulesNotify(Event: TEzListNotification; Rule: TEzCalendarRule); virtual;
    procedure Loaded; override;
    function  GetBorderStyle: TBorderStyle;
    function  GetBorderWidth: Integer;
    function  get_Date: TDateTime;
    function  GetFlat: Boolean;
    function  GetColors(Index: Integer): TColor;
    function  GetItems(Index: Integer): TEzCalendarRule;
    procedure SetColors(Index: Integer; Value: TColor);
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure SetBorderWidth(Value: Integer);
    procedure set_Date(Value: TDateTime);
    procedure SetFlat(Value: Boolean);
    procedure SetRules(Value: TEzCalendarRules);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure BeginUpdate; virtual;
    procedure EndUpdate; virtual;

    procedure Clear;
    procedure Refresh;
    procedure ReloadRules;

    property Modified: Boolean read FModified write FModified;
    property Rules: TEzCalendarRules read FRules write SetRules;
    property Items[Index: Integer]: TEzCalendarRule read GetItems; default;
    property Availability: TEzAvailabilityProfile read FAvailability;

  published
    property Align;
    property BorderWidth: Integer read GetBorderWidth write SetBorderWidth;
    property BorderStyle: TBorderStyle read GetBorderStyle write SetBorderStyle;
    property Color;
    property CalendarColor: TColor index 1 read GetColors write SetColors;
    property Date: TDateTime read get_Date write set_Date;
    property Flat: Boolean read GetFlat write SetFlat default False;
    property Font;
    property ParentFont;
    property ParentColor;
    property RulesColor: TColor index 2 read GetColors write SetColors;
    property UpdateCount: Integer read FUpdateCount;
    property WorkinghourColor: TColor index 3 read GetColors write SetColors default clGreen;
    property Visible;
    property OldCreateOrder: Boolean read BDummy write BDummy;
    property OnDateChange: TNotifyEvent read FOnDateChange write FOnDateChange;
    property OnRuleChanged: TOnRuleChangeEvent read FOnRuleChanged write FOnRuleChanged;
    property OnRuleChanging: TOnRuleChangeEvent read FOnRuleChanging write FOnRuleChanging;
    property OnTranslateRule: TOnTranslateRule read FOnTranslateRule write FOnTranslateRule;
  end;

implementation

uses EzRuleEd, EzWorkHr;

{$R *.DFM}

//= TEzCalendarPanelDataLink ---------------------------------------------------=

constructor TEzCalendarRuleNotifier.Create(AOwner: TEzCalendarPanel; Rules: TEzCalendarRules);
begin
  inherited Create(Rules);
  FOwner := AOwner;
end;

procedure TEzCalendarRuleNotifier.Notify(AObject: TObject; Action: TEzListNotification);
begin
  FOwner.DoRulesNotify(Action, TEzCalendarRule(AObject));
end;

//= TEzCalendarPanel ------------------------------------------------------=
constructor TEzCalendarPanel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Width := 650;
  Height := 300;
  ControlStyle := ControlStyle - [csAcceptsControls];
  pnlRules.ControlStyle := pnlRules.ControlStyle - [csAcceptsControls];
  pnlOuter.ControlStyle := pnlOuter.ControlStyle - [csAcceptsControls];
  pnlWorkingHours.ControlStyle := pnlWorkingHours.ControlStyle - [csAcceptsControls];
  pnlBottom.ControlStyle := pnlBottom.ControlStyle - [csAcceptsControls];
  gbDaySelector.ControlStyle := gbDaySelector.ControlStyle - [csAcceptsControls];

  CalendarGrid.CalendarDate := Int(Now);
  FWorkinghourColor := clGreen;
  ParentColor := True;

  FRules := TEzCalendarRules.Create;
  FLink := TEzCalendarRuleNotifier.Create(Self, FRules);
  FAvailability := TEzAvailabilityProfile.Create(1);
  FAvailability.Rules := FRules;
end;

destructor TEzCalendarPanel.Destroy;
begin
  BeginUpdate;
  FRules.Free;
  FAvailability.Free;
  inherited;
end;

procedure TEzCalendarPanel.Clear;
begin
  Rules.Clear;
  Availability.Clear;
end;

procedure TEzCalendarPanel.BeginUpdate;
begin
  inc(FUpdateCount);
end;

procedure TEzCalendarPanel.EndUpdate;
begin
  if FUpdateCount>0 then
  begin
    dec(FUpdateCount);

    if FUpdateCount=0 then
      Refresh;
  end;
end;

procedure TEzCalendarPanel.CalendarGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  dtDate: TDateTime;
  WorkingHours: Double;
  WorkRect: TRect;
  H: Integer;

begin
  if ARow > 0 then
    with CalendarGrid.Canvas do
    begin
      dtDate := CalendarGrid.CellDate[ACol, ARow];
      if dtDate <> 0 then
      begin
        if (csDesigning in ComponentState) then
          WorkingHours := 8.0/24 else
          WorkingHours := FAvailability.CalcWorkingHours(dtDate, dtDate+1);

        if WorkingHours > 0 then
        begin
          WorkRect := Rect;
          H := Round((WorkRect.Bottom-WorkRect.Top) * WorkingHours);
          WorkRect.Top := WorkRect.Top + (WorkRect.Bottom-WorkRect.Top) div 2 - H div 2;
          WorkRect.Bottom := WorkRect.Top + H;
          Brush.Style := bsSolid;
          Brush.Color := FWorkinghourColor;
          FillRect(WorkRect);
          Brush.Style := bsClear;
        end;
      end;
    end;
end;

procedure TEzCalendarPanel.Refresh;
begin
  if (FUpdateCount>0) or not showing or (csDesigning in ComponentState) then Exit;
  CalendarGrid.Invalidate;
  lbWorkingHours.Caption := FormatFloat('#0.00', 24*FAvailability.CalcWorkingHours(CalendarGrid.CalendarDate, CalendarGrid.CalendarDate+1));
  ReloadRules;
end;

procedure TEzCalendarPanel.CalendarGridChange(Sender: TObject);
begin
  dtpDate.Date := CalendarGrid.CalendarDate;
  Refresh;
  if Assigned(FOnDateChange) then FOnDateChange(Self);
end;

procedure TEzCalendarPanel.CMShowingChanged(var Message: TMessage);
begin
  inherited;
  if Showing then
  begin
    lbWorkingHours.Left := lbWorkinghoursLabel.Left + lbWorkinghoursLabel.Width + 5;
    Refresh;
  end;
end;

procedure TEzCalendarPanel.WMPaint(var Message: TWMPaint);
begin
//  RefreshDialog;
  inherited;
end;

procedure TEzCalendarPanel.DoRulesNotify(Event: TEzListNotification; Rule: TEzCalendarRule);
begin
  if FUpdateCount>0 then Exit;
  if (Event in [elnAdding, elnDeleting, elnChanging]) then
  begin
    if Assigned(FOnRuleChanging) then
      FOnRuleChanging(Self, Event, Rule);
  end
  else
  begin
    Refresh;
    if Assigned(FOnRuleChanged) then
      FOnRuleChanged(Self, Event, Rule);
  end;
end;

procedure TEzCalendarPanel.Loaded;
begin
  inherited;
end;

procedure TEzCalendarPanel.ReloadRules;
var
  RuleStrings: TStringList;
  i: Integer;
  Item: TListItem;

begin
  lvRules.Items.Clear;
  RuleStrings := TStringList.Create;
  try
    if rbRulesPerDate.Checked then
      FRules.GetRulesText(CalendarGrid.CalendarDate, crtAll, RuleStrings) else
      FRules.GetRulesText(crtAll, RuleStrings);

    for i:=0 to RuleStrings.Count-1 do
    begin
      Item := lvRules.Items.Add;
      Item.Caption := RuleStrings[i];
      Item.Data := RuleStrings.Objects[i];
    end;
  finally
    RuleStrings.Destroy;
  end;
end;

procedure TEzCalendarPanel.SetRules(Value: TEzCalendarRules);
begin
  FRules := Value;
  FAvailability.Rules := FRules;
  Refresh;
end;

procedure TEzCalendarPanel.acAddRuleExecute(Sender: TObject);
var
  Rule: TEzCalendarRule;

begin
  //
  // Create rule editor on request
  //
  if frmRule = nil then
  begin
    frmRule := TfrmRule.Create(Self);
    frmRule.Color := Color;
  end;

  with frmRule do
  begin
    RuleStart := CalendarGrid.CalendarDate;
    RuleStop := CalendarGrid.CalendarDate;
    if ShowModal = idOK then
    begin
      Rule := TEzCalendarRule.Create;
      GetRule(Rule);
      FRules.SafeAdd(Rule);
    end;
  end;
end;

procedure TEzCalendarPanel.acDeleteRuleExecute(Sender: TObject);
var
  Rule: TEzCalendarRule;

begin
  Rule := Items[lvRules.Selected.Index];
  Rules.Delete(Rules.IndexOf(Rule));
  // Extra refresh required because profile is only updated after call to
  // delete
  Refresh;
end;

procedure TEzCalendarPanel.acDeleteRuleUpdate(Sender: TObject);
begin
  acDeleteRule.Enabled := Assigned(lvRules.Selected);
end;

function TEzCalendarPanel.GetColors(Index: Integer): TColor;
begin
  Result := clBlack;
  case Index of
//    0: Result := pnlOuter.Color;
    1: Result := CalendarGrid.Color;
    2: Result := lvRules.Color;
    3: Result := FWorkinghourColor;
  end;
end;

function TEzCalendarPanel.GetItems(Index: Integer): TEzCalendarRule;
begin
  Result := TEzCalendarRule(lvRules.Items[Index].Data);
end;

procedure TEzCalendarPanel.SetColors(Index: Integer; Value: TColor);
begin
  case Index of
//    0: pnlOuter.Color := Value;
    1: CalendarGrid.Color := Value;
    2: lvRules.Color := Value;
    3:
      if FWorkinghourColor <> Value then
      begin
        FWorkinghourColor := Value;
        CalendarGrid.Invalidate;
      end;
  end;
end;

function TEzCalendarPanel.GetBorderStyle: TBorderStyle;
begin
  Result := pnlOuter.BorderStyle;
end;

procedure TEzCalendarPanel.SetBorderStyle(Value: TBorderStyle);
begin
  pnlOuter.BorderStyle := Value;
end;

function TEzCalendarPanel.GetBorderWidth: Integer;
begin
  Result := pnlOuter.BorderWidth;
end;

procedure TEzCalendarPanel.SetBorderWidth(Value: Integer);
begin
  pnlOuter.BorderWidth := Value;
end;

function  TEzCalendarPanel.get_Date: TDateTime;
begin
  Result := CalendarGrid.CalendarDate;
end;

function TEzCalendarPanel.GetFlat: Boolean;
begin
  Result := CalendarGrid.BorderStyle = bsNone;
end;

procedure TEzCalendarPanel.set_Date(Value: TDateTime);
begin
  CalendarGrid.CalendarDate := Value;
end;

procedure TEzCalendarPanel.SetFlat(Value: Boolean);
begin
  if Value then
  begin
    CalendarGrid.BorderStyle := bsNone;
    lvRules.BorderStyle := bsNone;
  end
  else
  begin
    CalendarGrid.BorderStyle := bsSingle;
    lvRules.BorderStyle := bsSingle;
  end;
end;

procedure TEzCalendarPanel.acSetWorkingHoursExecute(Sender: TObject);
begin
  if frmSetWorkingHours = nil then
  begin
    frmSetWorkingHours := TfrmSetWorkingHours.Create(Self);
    frmSetWorkingHours.Color := Color;
  end;

  frmSetWorkingHours.AssignRuleSet(FRules);
  if frmSetWorkingHours.ShowModal = idOK then
    frmSetWorkingHours.UpdateRulesSet(FRules);
end;

procedure TEzCalendarPanel.dtpDateChange(Sender: TObject);
begin
  CalendarGrid.CalendarDate := Trunc(dtpDate.Date);
end;

procedure TEzCalendarPanel.acEditRuleExecute(Sender: TObject);
var
  Rule: TEzCalendarRule;

begin
  //
  // Create rule editor on request
  //
  if frmRule = nil then
  begin
    frmRule := TfrmRule.Create(Self);
    frmRule.Color := Color;
  end;

  with frmRule do
  begin
    Rule := Items[lvRules.Selected.Index];
    SetRule(Rule);
    if ShowModal = idOK then
    begin
      Rules.Delete(Rules.IndexOf(Rule));
      Rule := TEzCalendarRule.Create;
      GetRule(Rule);
      FRules.SafeAdd(Rule);
    end;
  end;
end;

procedure TEzCalendarPanel.acEditRuleUpdate(Sender: TObject);
begin
  acEditRule.Enabled := Assigned(lvRules.Selected) and (TEzCalendarRule(Items[lvRules.Selected.Index]).RuleType<>crtWorkingHour);
end;

procedure TEzCalendarPanel.rbAllRulesClick(Sender: TObject);
begin
  ReloadRules;
end;

procedure TEzCalendarPanel.rbRulesPerDateClick(Sender: TObject);
begin
  ReloadRules;
end;

end.
