echo off
set SAVEPATH=%PATH%
set PATH=D:\PROGRA~1\BORLAND\CBUILDER6\BIN;%PATH%
..\UpdateRcVersion EzPlanit_CB6.rc %1
..\UpdateRcVersion EzPlanitDs_CB6.rc %1
ECHO ******************************
ECHO Using EzPlanIT_CB6_Updated.mak
ECHO ******************************

if %2 == DEMO goto :DEMO
if %2 == REG goto :REG
goto :End

:DEMO
make -B -fEzPlanIT_CB6_Demo.mak
make -B -fEzPlanitDs_CB6.mak
goto :END

:REG
make -B -fEzPlanIT_CB6.mak
make -B -fEzPlanitDs_CB6.mak
goto :End

:END
set PATH=%SAVEPATH%

