unit EzCalendar;

{$I Ez.inc}

interface

uses
  SysUtils, Classes, EzArrays, ContNrs, Math, EzDateTime;

const
  DONT_CARE=1904; // Value doesn't care, however should be a leapyear

  //
  // Definition of ruletype year constants. These constants are used to encode
  // various rule types into date values.
  //
  //
  rtyWorkingHour = 1902;
  rtyRecurDayWeek = 1903;
  rtyRecurDayOfMonth = 1910;
  rtyRecurDayPerMonth = 1915;
  rtyRecurDayOfYear = 1920;
  rty1stDayInMonth = 1930;
  rty2ndDayInMonth = 1931;
  rty3rdDayInMonth = 1932;
  rty4thDayInMonth = 1933;
  rty5thDayInMonth = 1934;

  Unprepared = -1;

type
  //
  // Exception handling classes
  //
  EEzRuleError = class(Exception);
  EEzAvailabilityError = class(Exception);

  //
  // Forward declarations
  //
  TEzAvailabilityProfile = class;
  TEzCalendarRules = class;

  EzCalendarLocalization = record
    WorkinghourText,
    WorkingText,
    NonWorkingText: string;
  end;

  // TEzTimeSpan stores a time interval.
  PEzTimespan = ^TEzTimespan;
  TEzTimespan = record
    // Start date/time of interval
    Start,
    // Stop date/time of interval
    Stop: TEzDateTime;
  end;

  TEzTimespanArray = class(TEzBaseArray)
  protected
    function  GetItem(Index: Integer): TEzTimespan;
    procedure SetItem(Index: Integer; Value: TEzTimespan);

  public
    constructor Create(InitialSize: Integer = 0); reintroduce; virtual;
    property Items[Index: Integer]: TEzTimespan read GetItem write SetItem; default;
  end;

  // Indicates the direction in which the scheduler tries to find a suitable
  // interval for a given task.
  TEzScheduleDirection = (
    // Schedule in forward direction (that is into the future)
    sdForwards,
    // Schedule in backward direction (that is into the past)
    sdBackwards,
    // Schedule direction is not determined jet
    sdUndefined,
    // There is no preference between scheduling forwards or backwards.
    sdDontCare,
    sdForwardsAndBackwards
  );

  // TAllocationType tells the scheduler how the resource must be allocated.
  TAllocationType = (
    // The resource is allocated at a fixed number of units over the duration
    // of the task. The number of units to alloacte are set using property Assigned.
    alFixed,

    // The resource is allocated at the maximum availability indicated by
    // the availability profile for the resource.
    // If the profile indicates the resource has an availability of 3 units
    // at a certain interval, then the resource will be assigned to this task at
    // 3 units during that interval. Then, when scheduling is not complete,
    // the next interval will be allocated at the number of units set for that
    // interval (for example 2).
    alProfile);

  TEzScheduleResult = (
    // Schedule succeeded
    srScheduledOK,
    // Schedule failed
    srScheduledFailed,
    // Scheduling a subsequent resource on a task failed but the scheduler
    // might try to reschedule the task starting with this resource.
    srValidateReschedule,
    // Scheduling a subsequent resource on a task failed because the resource
    // does not allow task switching. The scheduler
    // might try to reschedule the task starting with this resource.
    srValidateResourceMayNotSwitch,
    // Schedule succeeded however schedule intervals have been updated
    srWindowUpdated,
    // The task has been scheduled, but not within it's window.
    srScheduledOutsideWindow,
    // The interval data has changed for this task.
    srIntervaldataChanged,
    // The scheduler completed the schedule, however not all tasks are
    // scheduled OK and messages are stored in messages collection
    srScheduledWithErrors,
    srSlackScheduleFailed,
    srSlackWindowChanged,
    // srTaskHasMergedProfiles indicates that task has been scheduled
    // against merged resource profiles.
    srTaskHasMergedProfiles
  );
  TEzScheduleResults = set of TEzScheduleResult;

  TEzCalendarScheduleData = record
    Start, Stop: TEzDateTime;
    AbsoluteStart, AbsoluteStop: TEzDateTime;
    Duration: TEzDuration;
    UnitsRequired: Double;
    Direction: TEzScheduleDirection;
    ResourceAllowsSwithing: Boolean;
    AllResourcesAllowSwithing: Boolean;
    SchedulebeyondScheduleWindow: Boolean;
    AllocationType: TAllocationType;

    // Return data
    ScheduleResults: TEzScheduleResults;
    FailingProfile: TEzAvailabilityProfile;
  end;

  TEzCalendarRuleType = (
    crtWorkingHour,
    crtRecurringDayPerMonth,
    crtRecurringDayOfMonth,
    crtRecurringDayOfWeek,
    crtRecurringDayPerYear,
    crtRecurringDayOfYear,
    crtFixedPeriod
  );
  TEzCalendarRuleTypes = set of TEzCalendarRuleType;

  TEzIntervalType = (
    itWorking,
    itNonWorking
  );

  TEzDayIndex = (diFirst, diSecond, diThird, diForth, diLast);

  PEzCalendarRule = ^TEzCalendarRule;
  TEzCalendarRule = class(TObject)
  public
    Start: TDateTime;
    Stop: TDateTime;
    RangeStart, RangeEnd: TDateTime;
    Occurrences: Integer;
    Count: Integer;
    RuleType: TEzCalendarRuleType;
    IntervalType: TEzIntervalType;
    Tag: Integer;
    Flags: Integer;

    constructor Create;
  end;

  TAvailabilityFlag = (
    afIsAllocated,
    afNoScheduleAcross
  );
  TAvailabilityFlags = set of TAvailabilityFlag;

  TEzAvailabilityOperator = (
      opSum,
      opSubstract,
      opAnd,
      opOr,
      opScheduleProfile,
      opMergeScheduleProfile
    );

  TEzAvailabilityPoint = class(TObject)
  protected
    FDateValue: TEzDatetime;

    function  GetDateTime: TDateTime;
    procedure SetDateTime(Value: TDateTime);
  public
    Units: Double;
    Flags: TAvailabilityFlags;
    Tag: Integer;

    constructor Create; overload; virtual;
    constructor Create(Copy: TEzAvailabilityPoint); overload; virtual;
    constructor Create(ADate: TDateTime; AUnits: Double; AFlags: TAvailabilityFlags); overload; virtual;
    constructor Create(ADate: TEzDateTime; AUnits: Double; AFlags: TAvailabilityFlags); overload; virtual;

    property DateTime: TDateTime read GetDateTime write SetDateTime;
    property DateValue: TEzDateTime read FDateValue write FDateValue;
  end;

  TEzCalendarRulesEvents = (creRuleAdding, creRuleAdded, creRuleDeleting, creRuleDeleted, creRuleChanging, creRuleChanged);
  TOnTranslateRule = procedure(Sender: TEzCalendarRules; Rule: TEzCalendarRule; Translation: string) of object;

  TEzCalendarRules = class(TEzSortedObjectList)
  protected
    FOnTranslateRule: TOnTranslateRule;

    function  CompareItems(Obj1, Obj2: TObject): Integer; override;
    function  GetItem(Index: Integer): TEzCalendarRule;
    procedure SetItem(Index: Integer; Value: TEzCalendarRule);

  public
    constructor Create(InitialSize: Integer = 0); reintroduce; virtual;

    function  Add(Start, Stop: TDatetime; IntervalType: TEzIntervalType; RuleType: TEzCalendarRuleType) : TEzCalendarRule; reintroduce; overload;
    function  Add(Start, Stop: TDatetime; IntervalType: TEzIntervalType) : TEzCalendarRule; reintroduce; overload;
    function  AddEncoded(Start, Stop: TDatetime; IntervalType: TEzIntervalType) : TEzCalendarRule; overload; virtual;
    function  AddEncoded(EncodedDate: TDatetime; IntervalType: TEzIntervalType) : TEzCalendarRule; overload; virtual;
    function  IndexOfObject(Rule: TEzCalendarRule): Integer; virtual;
    function  TranslateRule(Rule: TEzCalendarRule): string; virtual;

    function  GetRules(Start, Stop: TDatetime; Filter: TEzCalendarRuleTypes; AList: TList): Boolean; virtual;
    procedure GetRulesText(ADate: TDatetime; Filter: TEzCalendarRuleTypes; AList: TStringList); overload; virtual;
    procedure GetRulesText(Filter: TEzCalendarRuleTypes; AList: TStringList); overload; virtual;

    procedure SafeAdd(Rule: TEzCalendarRule);

    property Items[Index: Integer]: TEzCalendarRule read GetItem write SetItem; default;
    property OnTranslateRule: TOnTranslateRule read FOnTranslateRule write FOnTranslateRule;
  end;

  TEzAvailabilityRuleNotifier = class(TEzListNotifier)
  protected
    FOwner: TEzAvailabilityProfile;
    procedure Notify(AObject: TObject; Action: TEzListNotification); override;

  public
    constructor Create(AOwner: TEzAvailabilityProfile; Rules: TEzCalendarRules); virtual;
  end;

  TEzAvailabilitySourceNotifier = class(TEzListNotifier)
  protected
    FOwner: TEzAvailabilityProfile;
    procedure Notify(AObject: TObject; Action: TEzListNotification); override;

  public
    constructor Create(AOwner, ASource: TEzAvailabilityProfile); virtual;
  end;

  TEzAvailabilityProfile = class(TEzSortedObjectList)
  protected
    FTag: Integer;
    FSources: TEzObjectList;
    FSourceNotifiers: TEzObjectList;
    FCalendarRules: TEzCalendarRules;
    FDefaultAllocation: Double;
    FPreparedRange: TEzTimespan;
    FPrepareBlockSize: TEzDuration;
    FRuleLink: TEzAvailabilityRuleNotifier;
    FScheduleProfileBaseUnits: Double;
    FOwnsRules: Boolean;
    FOperator: TEzAvailabilityOperator;
    FPreparing: Boolean;

    // FAllowTaskSwitching and FUnitsRequired are required when
    // scheduling against a schedule profile
    // (operator=opScheduleProfile or operator=opMergeScheduleProfile)
    FAllowTaskSwitching: Boolean;
    FUnitsRequired: Double;
    FAffectsDuration: Boolean;

    procedure ApplyRules(Start, Stop: TEzDateTime); overload; virtual;
    procedure ApplyRules(ADate: TEzDateTime; Rules: TList); overload; virtual;
    function  CompareItems(Obj1, Obj2: TObject): Integer; override;
    function  CompareExact(var Item1, Item2): Integer;

    function  GetItem(Index: Integer): TEzAvailabilityPoint;
    function  GetOwnsSources: Boolean;
    function  GetUnits(ADate: TDateTime): Double;
    function  InternalSumInterval(Start, Stop: TEzDateTime; Delta: Double; AllowNegative, MarkAllocated: Boolean): Boolean;
    procedure SafeAdd(var Point: TEzAvailabilityPoint);
    procedure ScheduleBackward(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray);
    procedure ScheduleForward(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray);
    procedure SetDefaultAllocation(Value: Double);
    procedure SetItem(Index: Integer; Value: TEzAvailabilityPoint);
    procedure SetOperator(Value: TEzAvailabilityOperator);
    procedure SetOwnsSources(Value: Boolean);
    procedure SetRules(Value: TEzCalendarRules);

  public
    constructor Create(DefaultAllocation: Double = 0.0); reintroduce; virtual;
    destructor  Destroy; override;

    procedure Assign(Source: TEzAvailabilityProfile);
    function  Add(var Point: TEzAvailabilityPoint): Integer; reintroduce; overload;
    function  Add(ADate: TDatetime; Units: Double; Flags: TAvailabilityFlags = []): TEzAvailabilityPoint; reintroduce; overload;
    function  Add(ADate: TEzDatetime; Units: Double; Flags: TAvailabilityFlags = []): TEzAvailabilityPoint; reintroduce; overload;
    procedure AddSource(Source: TEzAvailabilityProfile; Notify: Boolean=True); overload;

    function  AllocateInterval(Start, Stop: TEzDateTime; Units: Double; AllowNegative: Boolean): Boolean; virtual;
    procedure AndInterval(Start, Stop: TDateTime; Units: Double); overload;
    procedure AndInterval(Start, Stop: TEzDateTime; Units: Double); overload;
    function  CalcWorkingHours(Start, Stop: TDateTime): Double;
    procedure Clear; override;
    procedure ClearSources;

    procedure CopyFrom(var Interval: TEzTimespan; CopyFrom: TEzAvailabilityProfile);
    procedure CopyWithOperator(Interval: TEzTimespan; CopyFrom: TEzAvailabilityProfile);
    procedure CopyScheduleProfile(Interval: TEzTimespan);
    procedure CopyTo(Interval: TEzTimespan; CopyTo: TEzTimespanArray);
    function  DateToIndex(ADate: TEzDateTime): Integer; overload;
    function  DateToIndex(ADate: TDateTime): Integer; overload;
    function  IndexOf(var Index: Integer; ADate: TDateTime): Boolean; reintroduce; overload;
    function  IndexOf(var Index: Integer; ADate: TEzDateTime): Boolean; reintroduce; overload;
    procedure OrInterval(Start, Stop: TDateTime; Units: Double); overload;
    procedure OrInterval(Start, Stop: TEzDateTime; Units: Double); overload;
    procedure PrepareDateRange(Interval: TEzTimespan);
    function  RequiresPrepare: Boolean;
    procedure ScheduleTask(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray);
    procedure SetInterval(Start, Stop: TEzDateTime; Units: Double; Flags: TAvailabilityFlags); overload;
    procedure SetInterval(Start, Stop: TDateTime; Units: Double; Flags: TAvailabilityFlags); overload;
    function  SumInterval(Start, Stop: TEzDateTime; Delta: Double; AllowNegative: Boolean): Boolean; overload;
    function  SumInterval(Start, Stop: TDateTime; Delta: Double; AllowNegative: Boolean): Boolean; overload;
    function  ValidateDate(Value: TEzDateTime): Boolean;

    property AffectsDuration: Boolean read FAffectsDuration write FAffectsDuration;
    property AllowTaskSwitching: Boolean read FAllowTaskSwitching write FAllowTaskSwitching;
    property DefaultAllocation: Double read FDefaultAllocation write SetDefaultAllocation;
    property Items[Index: Integer]: TEzAvailabilityPoint read GetItem write SetItem; default;
    property Operator: TEzAvailabilityOperator read FOperator write SetOperator default opSubstract;
    property OwnsRules: Boolean read FOwnsRules write FOwnsRules;
    property OwnsSources: Boolean read GetOwnsSources write SetOwnsSources;
    property Rules: TEzCalendarRules read FCalendarRules write SetRules;
    property ScheduleProfileBaseUnits: Double
                read FScheduleProfileBaseUnits
                write FScheduleProfileBaseUnits;

    property Sources: TEzObjectList read FSources;
    property Tag: Integer read FTag write FTag;
    property Units[ADate: TDateTime]: Double read GetUnits;
    property UnitsRequired: Double read FUnitsRequired write FUnitsRequired;
  end;

  function MakeTimeSpan(Start, Stop: TDateTime): TEzTimespan; overload;
  function MakeTimeSpan(Start, Stop: TEzDateTime): TEzTimespan; overload;

  function DecodeRuleType(ADate: TDateTime): TEzCalendarRuleType;
  procedure EncodeRule(Rule: TEzCalendarRule; Start, Stop: TDatetime; IntervalType: TEzIntervalType; RuleType: TEzCalendarRuleType);
  function EncodeWorkingHour(ATime: TDateTime): TDateTime;
  function EncodeRecurringDayOfWeek(DOW: Integer): TDateTime;
  function EncodeRecurringDayOfMonth(ADay: Integer): TDateTime;
  function EncodeRecurringDayPerMonth(ADay: Integer; DayIndex: TEzDayIndex): TDateTime;
  function EncodeRecurringDayOfYear(ADay, AMonth: Integer): TDateTime;
  function EncodeRecurringDayPerYear(ADay, AMonth: Integer; DayIndex: TEzDayIndex): TDateTime;

var
  CalendarLocalization: EzCalendarLocalization;

const
  crtAll = [crtWorkingHour, crtRecurringDayOfMonth, crtRecurringDayPerMonth,
            crtRecurringDayOfWeek, crtRecurringDayPerYear, crtRecurringDayOfYear,
            crtFixedPeriod];

implementation

uses Controls, EzStrings_3;

function Ceil64(E: Extended): Int64;
begin
  Result := Trunc(E);
  if Frac(E)<>0 then
    inc(Result);
end;

function MakeTimeSpan(Start, Stop: TDateTime): TEzTimespan;
begin
  Result.Start := DatetimeToEzDatetime(Start);
  Result.Stop := DatetimeToEzDatetime(Stop);
end;

function MakeTimeSpan(Start, Stop: TEzDateTime): TEzTimespan;
begin
  Result.Start := Start;
  Result.Stop := Stop;
end;

constructor TEzAvailabilityPoint.Create;
begin
  inherited;
end;

constructor TEzAvailabilityPoint.Create(Copy: TEzAvailabilityPoint);
begin
  inherited Create;
  DateValue := Copy.DateValue;
  Units := Copy.Units;
  Flags := Copy.Flags;
  Tag := Copy.Tag;
end;

constructor TEzAvailabilityPoint.Create(ADate: TEzDateTime; AUnits: Double; AFlags: TAvailabilityFlags);
begin
  inherited Create;
  DateValue := ADate;
  Units := AUnits;
  Flags := AFlags;
  Tag := 0;
end;

constructor TEzAvailabilityPoint.Create(ADate: TDateTime; AUnits: Double; AFlags: TAvailabilityFlags);
begin
  Create(DatetimeToEzDatetime(ADate), AUnits, AFlags);
end;

function TEzAvailabilityPoint.GetDateTime: TDateTime;
begin
  Result := EzDateTimeToDateTime(FDateValue);
end;

procedure TEzAvailabilityPoint.SetDateTime(Value: TDateTime);
begin
  FDateValue := DateTimeToEzDateTime(Value);
end;

constructor TEzTimespanArray.Create(InitialSize: Integer = 0);
begin
  inherited Create(10, SizeOf(TEzTimespan));
end;

function TEzTimespanArray.GetItem(Index: Integer): TEzTimespan;
begin
  inherited GetItem(Index, Result);
end;

procedure TEzTimespanArray.SetItem(Index: Integer; Value: TEzTimespan);
begin
  inherited PutItem(Index, Value);
end;

function AvailabilityFlagsToInt(const s: TAvailabilityFlags): Integer;
begin
  Result := 0;
  if afIsAllocated in s then
    inc(Result, Ord(afIsAllocated));

  if afNoScheduleAcross in s then
    inc(Result, Ord(afNoScheduleAcross));
end;

procedure EzRuleError(const Message: string);
begin
  raise EEzRuleError.Create(Message);
end;

procedure EzAvailabilityError(const Message: string);
begin
  raise EEzAvailabilityError.Create(Message);
end;

function DayIndex(AYear, AMonth, ADay: Word; var IsLast: Boolean) : TEzDayIndex;
begin
	Result := TEzDayIndex((ADay-1) div 7);
	if Ord(Result) > 4 then
    IsLast := (MonthDays[IsLeapYear(AYear)][AMonth] - ADay) < 7 else
		IsLast := false;
end;

procedure EncodeRule(Rule: TEzCalendarRule; Start, Stop: TDatetime; IntervalType: TEzIntervalType; RuleType: TEzCalendarRuleType);
var
  AYear, ADay, AMonth: Word;
  EndodedDate: TDateTime;

begin
  if Start > Stop then
    EzRuleError(SNullInterval);

  if Start = Stop then
    Stop := Start + 1;

  EndodedDate := 0;
  case RuleType of
  	crtWorkingHour:
      EndodedDate := EncodeWorkingHour(0);

	  crtRecurringDayOfWeek:
    begin
      DecodeDate(Start, AYear, AMonth, ADay);
      EndodedDate := EncodeRecurringDayOfWeek(ADay);
    end;

  	crtRecurringDayOfMonth:
    begin
      DecodeDate(Start, AYear, AMonth, ADay);
      EndodedDate := EncodeRecurringDayOfMonth(ADay);
    end;

  	crtRecurringDayPerMonth:
    begin
      DecodeDate(Start, AYear, AMonth, ADay);
      EndodedDate := EncodeRecurringDayPerMonth(ADay, TEzDayIndex(AMonth-1));
    end;

  	crtRecurringDayOfYear:
    begin
      DecodeDate(Start, AYear, AMonth, ADay);
      EndodedDate := EncodeRecurringDayOfYear(ADay, AMonth);
    end;

  	crtRecurringDayPerYear:
    begin
      DecodeDate(Start, AYear, AMonth, ADay);
      EndodedDate := EncodeRecurringDayPerYear(ADay, AMonth, TEzDayIndex(AYear-rty1stDayInMonth));
    end;

  	crtFixedPeriod:
	  	//
		  // August 13th 12:00, 2002 till September 4th 14:00, 2002 is a non working period
  		//
    begin
      Rule.Start := Start;
      Rule.Stop := Stop;
    end;

  end;

  // Update rule class
  if RuleType <> crtFixedPeriod then
  begin
    Rule.Start := EndodedDate + Frac(Start);
    if (Frac(Stop)=0) then
      Rule.Stop := EndodedDate + 1 else
      Rule.Stop := EndodedDate + Frac(Stop);
  end;
  Rule.RuleType := RuleType;
  Rule.IntervalType := IntervalType;
end;

function DecodeRuleType(ADate: TDateTime): TEzCalendarRuleType;
var
  AYear, AMonth, ADay: Word;

begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  case AYear of
    rtyWorkingHour: Result := crtWorkingHour;
    rtyRecurDayWeek: Result := crtRecurringDayOfWeek;
    rtyRecurDayOfMonth: Result := crtRecurringDayOfMonth;
    rtyRecurDayPerMonth: Result := crtRecurringDayPerMonth;
    rtyRecurDayOfYear: Result := crtRecurringDayOfYear;
    rty1stDayInMonth..rty5thDayInMonth: Result := crtRecurringDayPerYear;
  else
    Result := crtFixedPeriod;
  end;
end;

function EncodeWorkingHour(ATime: TDateTime): TDateTime;
  //
  // Sample: Working hours are set between 8:00 and 17:00
  // Date encoding: 1/1/rtyWorkingHour [start time] [stop time]
  //
begin
  Result := EncodeDate(rtyWorkingHour, 1, 1) + Frac(ATime);
end;

function EncodeRecurringDayOfWeek(DOW: Integer): TDateTime;
  //
	// Sample: Every sunday is a non working day
	// Date encoding: [sun-sat]/1/rtyRecurDayWeek
  //
begin
  if DOW > 7 then EzRuleError(SInvalidDayValue);
  Result := EncodeDate(rtyRecurDayWeek, 1, DOW);
end;

function EncodeRecurringDayOfMonth(ADay: Integer): TDateTime;
 	//
	// Sample: Every 13th is a non working day
 	// Date encoding: [1..31]/1/rtyRecurDateMonth
  //
begin
  Result := EncodeDate(rtyRecurDayOfMonth, 1, ADay);
end;

function EncodeRecurringDayPerMonth(ADay: Integer; DayIndex: TEzDayIndex): TDateTime;
  //
	// Sample: Every 1st monday is non working day
  // Date encoding: [sun-sat]/[1..5]/rtyRecurDayMonth
	//
begin
  if ADay > 7 then EzRuleError(SInvalidDayValue);
  Result := EncodeDate(rtyRecurDayPerMonth, Ord(DayIndex)+1, ADay);
end;

function EncodeRecurringDayOfYear(ADay, AMonth: Integer): TDateTime;
  		//
	  	// Sample: Every August 13th is a non working day
		  // Date encoding: [1..31]/[1..12]/rtyRecurDateYear
  		//
begin
  Result := EncodeDate(rtyRecurDayOfYear, AMonth, ADay);
end;

function EncodeRecurringDayPerYear(ADay, AMonth: Integer; DayIndex: TEzDayIndex): TDateTime;
	  	//
		  // Sample: Every 1st monday in August is a non working day
  		// Date encoding: [sun-sat]/[1..12]/[rty1stDayInMonth..rty5thDayInMonth]
	  	//
begin
  if ADay > 7 then EzRuleError(SInvalidDayValue);
  Result := EncodeDate(Ord(DayIndex)+rty1stDayInMonth, AMonth, ADay);
end;

constructor TEzCalendarRules.Create(InitialSize: Integer = 0);
begin
  inherited Create(True);
  Duplicates := dupError;
end;

procedure TEzCalendarRules.SafeAdd(Rule: TEzCalendarRule);
begin
  try
    inherited Add(Rule);
  except
    if IndexOfObject(Rule) = -1 then
      Rule.Destroy;
    raise;
  end;
end;

function TEzCalendarRules.Add(Start, Stop: TDatetime; IntervalType: TEzIntervalType; RuleType: TEzCalendarRuleType): TEzCalendarRule;
begin
  Result := TEzCalendarRule.Create;
  EncodeRule(Result, Start, Stop, IntervalType, RuleType);
  SafeAdd(Result);
end;

function TEzCalendarRules.Add(Start, Stop: TDatetime; IntervalType: TEzIntervalType): TEzCalendarRule;
begin
  Result := TEzCalendarRule.Create;
  EncodeRule(Result, Start, Stop, IntervalType, DecodeRuleType(Start));
  SafeAdd(Result);
end;

function TEzCalendarRules.AddEncoded(Start, Stop: TDatetime; IntervalType: TEzIntervalType) : TEzCalendarRule;
begin
  Result := TEzCalendarRule.Create;
  try
    Result.Start := Start;
    Result.Stop := Stop;
    Result.IntervalType := IntervalType;
    Result.RuleType := DecodeRuleType(Start);
    Add(Result);
  except
    Result.Destroy;
    raise;
  end;
end;

function TEzCalendarRules.AddEncoded(EncodedDate: TDatetime; IntervalType: TEzIntervalType) : TEzCalendarRule;
begin
  Result := AddEncoded(EncodedDate, EncodedDate+1, IntervalType);
end;

function TEzCalendarRules.IndexOfObject(Rule: TEzCalendarRule): Integer;
begin
  Result := 0;
  while (Result<Count) and (Items[Result]<>Rule) do
    inc(Result);
  if Result=Count then
    Result := -1;
end;

function TEzCalendarRules.CompareItems(Obj1, Obj2: TObject): Integer;
var
  i1: TEzCalendarRule;
  i2: TEzCalendarRule;

begin
  i1 := TEzCalendarRule(Obj1);
  i2 := TEzCalendarRule(Obj2);

  if i1.Start < i2.Start then
    Result := -1
  else if i1.Start > i2.Start then
    Result := 1
  else
    Result := 0;
end;

function TEzCalendarRules.GetItem(Index: Integer): TEzCalendarRule;
begin
  Result := TEzCalendarRule(inherited Items[Index]);
end;

function TEzCalendarRules.GetRules(Start, Stop: TDatetime; Filter: TEzCalendarRuleTypes; AList: TList): Boolean;
var
  Search, Rule: TEzCalendarRule;
  AYear, ADay, AMonth, DOW, n_thDay: Word;
  IsLast: Boolean;
  i: Integer;

  function RuleInDateRange(Rule: TEzCalendarRule): Boolean;
  begin
    Result :=
        ((Rule.RangeStart=0) or (Rule.RangeStart<=Start)) and
        ((Rule.RangeEnd=0) or (Rule.RangeEnd>=Start));
  end;

  function RuleInRange(Scale: TScale; Rule: TEzCalendarRule): Boolean;
  var
    Ticks: Integer;

  begin
    Result := RuleInDateRange(Rule);
    if Result and ((Rule.Count>1) or (Rule.Occurrences>0)) then
    begin
      Ticks := TickCount(Scale, 1, Rule.RangeStart, Start);
      Result :=
        (Ticks mod Rule.Count=0) and
        ((Rule.Occurrences=0) or (Ticks div Rule.Count < Rule.Occurrences));
    end;
  end;

  procedure AddToList(Scale: TScale);
  var
    Rule: TEzCalendarRule;

  begin
    if i<Count then
    begin
      Rule := Items[i];
      // Add all rules that apply to the given date
      while (Rule<>nil) and (Floor(Search.Start)=Floor(Rule.Start)) do
      begin
        if RuleInRange(Scale, Rule) then
          AList.Add(Rule);
        inc(i);
        if i<Count then
          Rule := Items[i] else
          Rule := nil;
      end;
    end;
  end;

  procedure AddToListNoScale;
  var
    Rule: TEzCalendarRule;

  begin
    if i<Count then
    begin
      Rule := Items[i];
      // Add all rules that apply to the given date
      while (Rule<>nil) and (Floor(Search.Start)=Floor(Rule.Start)) do
      begin
        if RuleInDateRange(Rule) then
          AList.Add(Rule);
        inc(i);
        if i<Count then
          Rule := Items[i] else
          Rule := nil;
      end;
    end;
  end;

begin
  DecodeDate(Start, AYear, AMonth, ADay);

  DOW := 0;
  if [crtRecurringDayOfWeek, crtRecurringDayPerMonth, crtRecurringDayPerYear] * Filter <> [] then
    // Rules are encode using DayOfWeek, therefore do not use ISO DayOfTheWeek method.
    DOW := DayOfWeek(Start);

  n_thDay := 0;
  if [crtRecurringDayPerMonth, crtRecurringDayPerYear] * Filter <> [] then
    n_thDay := NthDay(AYear, AMonth, ADay, IsLast);

  Search := TEzCalendarRule.Create;
  try
    // Working hours rules
  	if crtWorkingHour in Filter then
    begin
      Search.Start := EncodeDate(rtyWorkingHour, 1, 1);
      IndexOf(i, Search);
      AddToListNoScale;
    end;

  	// Recurring days
	  if crtRecurringDayOfWeek in Filter then
    begin
      Search.Start := EncodeDate(rtyRecurDayWeek, 1, DOW) + Frac(Start);
      IndexOf(i, Search);
      AddToList(msWeek);
    end;

	  // Recurring dates per month
	  if crtRecurringDayOfMonth in Filter then
    begin
      Search.Start := EncodeDate(rtyRecurDayOfMonth, 1, ADay) + Frac(Start);
      IndexOf(i, Search);
      AddToList(msMonth);
    end;

  	// Recurring days in month
	  if crtRecurringDayPerMonth in Filter then
    begin
      Search.Start := EncodeDate(rtyRecurDayPerMonth, n_thDay, DOW) + Frac(Start);
      IndexOf(i, Search);
      AddToList(msMonth);

  		// When day given is both the 4th day and the last day in this month,
	  	// then also add days given for the 5th day (== last)
		  if (n_thDay = 4) and IsLast then
      begin
        Search.Start := EncodeDate(rtyRecurDayPerMonth, n_thDay+1, DOW) + Frac(Start);
        IndexOf(i, Search);
        AddToList(msMonth);
      end;
    end;

	  // Recurring dates per year
	  if crtRecurringDayOfYear in Filter then
    begin
      Search.Start := EncodeDate(rtyRecurDayOfYear, AMonth, ADay) + Frac(Start);
      IndexOf(i, Search);
      AddToList(msYear);
    end;

  	// Recurring days per year
	  if crtRecurringDayPerYear in Filter then
    begin
      Search.Start := EncodeDate(rty1stDayInMonth-1+n_thDay, AMonth, DOW) + Frac(Start);
      IndexOf(i, Search);
      AddToList(msYear);

  		// When day given is both the 4th day and the last day in this month,
	  	// then also add days given for the 5th day (== last)
		  if (n_thDay = 4) and IsLast then
      begin
        Search.Start := EncodeDate(rty1stDayInMonth+n_thDay, AMonth, DOW) + Frac(Start);
        IndexOf(i, Search);
        AddToList(msYear);
      end;
    end;

  	// Special days
	  if crtFixedPeriod in Filter then
    begin
      Search.Start := EncodeDate(rty5thDayInMonth+1, 1, 1);
      IndexOf(i, Search);
      if i<Count then
      begin
        Rule := Items[i];
        // Add all rules that apply to the given date
        while (Rule<>nil) and (Floor(Rule.Start)<=Start) do
        begin
          if (Floor(Start)<=Floor(Rule.Stop)) and (Floor(Start)>=Floor(Rule.Start)) then
            AList.Add(Rule);
          inc(i);
          if i<Count then
            Rule := Items[i] else
            Rule := nil;
        end;
      end;
    end;
  finally
    Search.Destroy;
  end;
  Result := AList.Count>0;
end;

procedure TEzCalendarRules.GetRulesText(Filter: TEzCalendarRuleTypes; AList: TStringList);
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    AList.AddObject(TranslateRule(TEzCalendarRule(Items[i])), Items[i]);
end;

procedure TEzCalendarRules.GetRulesText(ADate: TDatetime; Filter: TEzCalendarRuleTypes; AList: TStringList);
var
  List: TList;
  i: Integer;

begin
  List := TList.Create;
  try
    GetRules(ADate, ADate, Filter, List);
    for i:=0 to List.Count-1 do
      AList.AddObject(TranslateRule(TEzCalendarRule(List[i])), List[i]);
  finally
    List.Destroy;
  end;
end;

procedure TEzCalendarRules.SetItem(Index: Integer; Value: TEzCalendarRule);
begin
  inherited Items[Index] := Value;
end;

function TEzCalendarRules.TranslateRule(Rule: TEzCalendarRule): string;

  function ShortestTime(T: TTime) : string;
  var
    Hour, Min, Sec, MSec: Word;
  begin
    DecodeTime(T, Hour, Min, Sec, MSec);

    Result := Format('%d:%2.2d', [Hour, Min]);
    if Sec <> 0 then
      Result := Result + Format(':%2.2d', [Sec]);
    if MSec <> 0 then
      Result := Result + Format('.%d', [MSec]);
  end;

  function WorkingText(IT: TEzIntervalType) : string;
  begin
    case IT of
      itWorking: Result := CalendarLocalization.WorkingText;
      itNonWorking: Result := CalendarLocalization.NonWorkingText;
    end;
  end;

  function EveryText(Count: Integer): string;
  begin
    if Count>1 then
      Result := Format(SEveryText, [DayNumNames[Count]]) else
      Result := '';
  end;

  function EveryTextWithThe(Count: Integer): string;
  begin
    Result := EveryText(Count);
    if Result='' then Result := SThe;
  end;

  function RecurringRangeText(Rule: TEzCalendarRule): string;
  begin
    Result := '';
    if Rule.RangeStart<>0 then
      Result := Format(SStartAtText, [FormatDateTime('d mmm yyyy', Rule.RangeStart)]);

    if Rule.RangeEnd<>0 then
    begin
      if Rule.RangeStart<>0 then
        Result := Result + ' and ' + Format(SEndAtText, [FormatDateTime('d mmm yyyy', Rule.RangeEnd)]) else
        Result := Result + ', ' + Format(SEndAtText, [FormatDateTime('d mmm yyyy', Rule.RangeEnd)]);
    end;

    if Rule.Occurrences<>0 then
      Result := Result + Format(SOccurrencesText, [Rule.Occurrences]);
  end;

  function BetweenText(Rule: TEzCalendarRule): string;
  begin
    if (Frac(Rule.Start) <> 0) or (Frac(Rule.Stop) <> 0) then
      Result := Format(SBetweenText, [ShortestTime(Rule.Start), ShortestTime(Rule.Stop)]) else
      Result := '';
  end;
var
  AYear, AMonth, ADay: Word;

begin
  if Assigned(OnTranslateRule) then
    OnTranslateRule(Self, Rule, Result)
  else
  begin
    case Rule.RuleType of
      crtWorkingHour:
        Result := Format(CalendarLocalization.WorkinghourText, [ShortestTime(Rule.Start), ShortestTime(Rule.Stop)]);

      crtRecurringDayOfWeek:
      begin
        DecodeDate(Rule.Start, AYear, AMonth, ADay);
        Result := Format(SRecurDayWeek, [
            WorkingText(Rule.IntervalType),
            EveryText(Rule.Count),
            {$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[ADay],
            BetweenText(Rule),
            RecurringRangeText(Rule)]);
      end;

      crtRecurringDayOfMonth:
      begin
        DecodeDate(Rule.Start, AYear, AMonth, ADay);
        Result := Format(SRecurDateMonth, [
          WorkingText(Rule.IntervalType),
          DayNumNames[ADay],
          EveryTextWithThe(Rule.Count),
          BetweenText(Rule),
          RecurringRangeText(Rule)]);
      end;

      crtRecurringDayPerMonth:
      begin
        DecodeDate(Rule.Start, AYear, AMonth, ADay);
        Result := Format(SRecurDayMonth, [
          WorkingText(Rule.IntervalType),
          DayMonthNames[AMonth],
          {$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[ADay],
          EveryTextWithThe(Rule.Count),
          BetweenText(Rule),
          RecurringRangeText(Rule)]);
      end;

      crtRecurringDayOfYear:
      begin
        DecodeDate(Rule.Start, AYear, AMonth, ADay);
        Result := Format(SRecurDateYear, [
          WorkingText(Rule.IntervalType),
          {$IFDEF XE3}FormatSettings.{$ENDIF}LongMonthNames[AMonth],
          DayNumNames[ADay],
          BetweenText(Rule),
          RecurringRangeText(Rule)]);
      end;

      crtRecurringDayPerYear:
      begin
        DecodeDate(Rule.Start, AYear, AMonth, ADay);
        Result := Format(SRecurDayYear, [
          WorkingText(Rule.IntervalType),
          DayMonthNames[AYear-rty1stDayInMonth+1],
          {$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[ADay],
          {$IFDEF XE3}FormatSettings.{$ENDIF}LongMonthNames[AMonth],
          BetweenText(Rule),
          RecurringRangeText(Rule)]);
          
{
        if (Frac(Rule.Start) <> 0) or (Frac(Rule.Stop) <> 0) then
          Result := Format(SRecurDayYearInterval, [WorkingText(Rule.IntervalType), DayMonthNames[AYear-rty1stDayInMonth+1], LongDayNames[ADay], LongMonthNames[AMonth], ShortestTime(Rule.Start), ShortestTime(Rule.Stop)]) else
          Result := Format(SRecurDayYear, [WorkingText(Rule.IntervalType), DayMonthNames[AYear-rty1stDayInMonth+1], LongDayNames[ADay], LongMonthNames[AMonth]]);
}
      end;

      crtFixedPeriod:
        if (Frac(Rule.Start) = 0) and (Rule.Stop-Rule.Start = 1) then
          Result := Format(SPeriodDay, [WorkingText(Rule.IntervalType), FormatDateTime('d mmm yyyy', Rule.Start)])
        else if Floor(Rule.Start) = Floor(Rule.Stop) then
          Result := Format(SPeriodDayTime, [WorkingText(Rule.IntervalType), FormatDateTime('d mmm yyyy', Rule.Start), ShortestTime(Rule.Start), ShortestTime(Rule.Stop)])
        else
          Result := Format(SPeriodDayDay, [WorkingText(Rule.IntervalType), FormatDateTime('d mmm yyyy', Rule.Start), ShortestTime(Rule.Start), FormatDateTime('d mmm yyyy', Rule.Stop), ShortestTime(Rule.Stop)]);
    end;
  end;
end;

//= End of TEzCalendarRules ---------------------------------------------------=

//=----------------------------------------------------------------------------=

//= TEzAvailabilityRuleNotifier ---------------------------------------------------=

constructor TEzAvailabilityRuleNotifier.Create(AOwner: TEzAvailabilityProfile; Rules: TEzCalendarRules);
begin
  inherited Create(Rules);
  FOwner := AOwner;
end;

procedure TEzAvailabilityRuleNotifier.Notify(AObject: TObject; Action: TEzListNotification);
begin
  FOwner.Clear;
end;

//= End of TEzAvailabilityRuleNotifier ---------------------------------------------------=

//=----------------------------------------------------------------------------=

//= TEzAvailabilitySourceNotifier ---------------------------------------------------=

constructor TEzAvailabilitySourceNotifier.Create(AOwner, ASource: TEzAvailabilityProfile);
begin
  inherited Create(ASource);
  FOwner := AOwner;
end;

procedure TEzAvailabilitySourceNotifier.Notify(AObject: TObject; Action: TEzListNotification);
begin
  FOwner.Clear;
end;

//= End of TEzAvailabilitySourceNotifier ---------------------------------------------------=

//=----------------------------------------------------------------------------=

//= TEzAvailabilityProfile ----------------------------------------------------=
constructor TEzAvailabilityProfile.Create(DefaultAllocation: Double = 0.0);
begin
  inherited Create(True);
  FOperator:=opSubstract;
  FOwnsRules:=False;
  FPreparing:=False;
  FPreparedRange.Start := Unprepared;
  FPreparedRange.Stop := END_OF_TIME;
  FPrepareBlockSize := DoubleToEzDuration(10);
  FDefaultAllocation := DefaultAllocation;
  Add(0, FDefaultAllocation);
  Duplicates := dupError;
  FCalendarRules := nil;
  FRuleLink := TEzAvailabilityRuleNotifier.Create(Self, nil);
  FSources := TEzObjectList.Create(False);
  FSourceNotifiers := TEzObjectList.Create(True);
  FScheduleProfileBaseUnits := 1.0;
end;

destructor TEzAvailabilityProfile.Destroy;
begin
  inherited;
  if FOwnsRules then
    FCalendarRules.Free;

  FSources.Free;
  FSourceNotifiers.Free;
  FRuleLink.Free;
end;

procedure TEzAvailabilityProfile.SafeAdd(var Point: TEzAvailabilityPoint);
begin
  try
    Add(Point);
  except
    if IndexOf(Point) = -1 then
      Point.Destroy;
    raise;
  end;
end;

procedure TEzAvailabilityProfile.Assign(Source: TEzAvailabilityProfile);
var
  i: Integer;

begin
  FDestroying:=True;
  try
    Clear;
    for i:=0 to Source.Count-1 do
      insert(i, TEzAvailabilityPoint.Create(Source[i]));
  finally
    FDestroying:=False;
  end;
end;

function TEzAvailabilityProfile.Add(var Point: TEzAvailabilityPoint): Integer;
var
  P: TEzAvailabilityPoint;

begin
  if IndexOf(Result, Point) then
    //
    // A point with the same date already exists
    //
  begin
    if (Result+1) < Count then
      // If new point equals next point, then we move next point to this point.
    begin
      P := Items[Result+1];
      if (P.Units = Point.Units) and (P.Flags = Point.Flags) then
        Delete(Result+1);
    end;

    if Result = 0 then
      // Special case, the first point is updated.
    begin
      Items[0] := Point;
      Exit;
    end;

    P := Items[Result-1];
    if (P.Units = Point.Units) and (P.Flags = Point.Flags) then
      //
      // If new point equals prev point, then nothing needs changing.
      // Because a point exists at given date, we remove existing point,
      // destroy the new point and return Prev point instead
      //
    begin
      Delete(Result);
      Point.Destroy;
      Point := P;
    end
    else
      //
      // Update existing point with new point
      //
      Items[Result] := Point;
  end

  else if (Count=0) then
    Insert(Result, Point)

  else if (Result < Count) then
    //
    // New point is in between two other points.
    // Investigate previous and next points
    //
  begin
    P := Items[Result];
    if (P.Units = Point.Units) and (P.Flags = Point.Flags) then
    begin
      //
      // Next point is equal, update next point backwards with new data!
      //
      Items[Result] := Point;
    end
    else
    begin
      P := Items[Result-1];
      if (P.Units <> Point.Units) or (P.Flags <> Point.Flags) then
        Insert(Result, Point) else
        Result := -1;
    end;
  end
  else
    //
    // New point is at the end of the dataset
    //
  begin
    P := Items[Result-1];
    if (P.Units <> Point.Units) or (P.Flags <> Point.Flags) then
      Insert(Result, Point) else
      Result := -1;
  end;
end;

function TEzAvailabilityProfile.Add(ADate: TDatetime; Units: Double; Flags: TAvailabilityFlags): TEzAvailabilityPoint;
begin
  Result := Add(DatetimeToEzDatetime(ADate), Units, Flags);
end;

function TEzAvailabilityProfile.Add(ADate: TEzDatetime; Units: Double; Flags: TAvailabilityFlags): TEzAvailabilityPoint;
begin
  Result := TEzAvailabilityPoint.Create;
  Result.DateValue := ADate;
  Result.Units := Units;
  Result.Flags := Flags;
  SafeAdd(Result);
end;

procedure TEzAvailabilityProfile.AddSource(Source: TEzAvailabilityProfile; Notify: Boolean);
begin
  if Source = Self then
    EzAvailabilityError(SCantAddSelf);

  if FSources.IndexOf(Source) <> -1 then
    EzAvailabilityError(SDuplicateSource);

  FSources.Add(Source);

  if Notify then
    FSourceNotifiers.Add(TEzAvailabilitySourceNotifier.Create(Self, Source));

  Clear;

  Assert(Count > 0);
end;

function TEzAvailabilityProfile.AllocateInterval(Start, Stop: TEzDateTime; Units: Double; AllowNegative: Boolean): Boolean;
begin
  PrepareDateRange(MakeTimespan(Start, Stop));
  Result := InternalSumInterval(Start, Stop, -Units, AllowNegative, True);
end;

procedure TEzAvailabilityProfile.AndInterval(Start, Stop: TDateTime; Units: Double);
begin
  Assert(False);
{
  if Units = 0.0 then
  begin
    PrepareDateRange(MakeTimespan(Start, Stop));
    SetInterval(Start, Stop, 0.0, []);
  end;
}
end;

procedure TEzAvailabilityProfile.AndInterval(Start, Stop: TEzDateTime; Units: Double);
begin
  if Units = 0.0 then
  begin
    PrepareDateRange(MakeTimespan(Start, Stop));
    SetInterval(Start, Stop, 0.0, []);
  end;
end;

procedure TEzAvailabilityProfile.OrInterval(Start, Stop: TDateTime; Units: Double);
begin
  Assert(False);

{
  if Units>0 then
  begin
    PrepareDateRange(MakeTimespan(Start, Stop));
    SetInterval(Start, Stop, Units, []);
  end;
}
end;

procedure TEzAvailabilityProfile.OrInterval(Start, Stop: TEzDateTime; Units: Double);
begin
  if Units>0 then
  begin
    PrepareDateRange(MakeTimespan(Start, Stop));
    SetInterval(Start, Stop, Units, []);
  end;
end;

procedure TEzAvailabilityProfile.ApplyRules(Start, Stop: TEzDateTime);
var
  i: TEzDatetime;
  ADay: Integer;
  RulesList: TObjectList;

begin
  if not Assigned(FCalendarRules) then
    EzAvailabilityError(SRulesRequired);

  RulesList := TObjectList.Create;
  RulesList.OwnsObjects := False;
  try
    i:=Start;
    while i<Stop do
    begin
      ADay := Trunc(EzDatetimeToDate(i));
      if Rules.GetRules(ADay, ADay+1, crtAll, RulesList) then
      begin
        ApplyRules(i, RulesList);
        RulesList.Clear;
      end;
      inc(i, ONE_DAY);
    end;
  finally
    RulesList.Destroy;
  end;
end;

procedure TEzAvailabilityProfile.ApplyRules(ADate: TEzDateTime; Rules: TList);
var
  IsWorkingDay: Boolean;
  TempProfile: TEzAvailabilityProfile;
  Rule: TEzCalendarRule;
  i: Integer;
  RuleStart, RuleStop, Last: TEzDateTime;
  ts: TEzTimespan;
  LocalPoint, TempPoint, NextTempPoint: TEzAvailabilityPoint;
  LocalIndex, TempIndex: Integer;

begin
  if not Assigned(FCalendarRules) then
    EzAvailabilityError(SRulesRequired);

	IsWorkingDay := true;

	// Check 'full day' rules and set IsWorkingDay
	// A 'full day' rule starts at 00:00 and runs to 00:00
	for i:=0 to Rules.Count-1 do
  begin
    Rule := Rules[i];
    RuleStart := max(ADate, DatetimeToEzDatetime(Rule.Start));
    RuleStop := min(ADate+ONE_DAY, DatetimeToEzDatetime(Rule.Stop));

		if (Rule.RuleType <> crtWorkingHour) and (FracEzDatetime(RuleStart) = 0) and (FracEzDatetime(RuleStop) = 0) then
      IsWorkingDay := Rule.IntervalType = itWorking;
  end;

  // Create Availability object which we can use to apply rules on
  TempProfile := TEzAvailabilityProfile.Create(0);
  try
  	//
	  // Apply working hours
  	//
	  if IsWorkingDay then
    begin
      TempProfile.Add(0, 1);
  		Last := ADate;

    	for i:=0 to Rules.Count-1 do
      begin
        Rule := TEzCalendarRule(Rules[i]);

			  if Rule.RuleType = crtWorkingHour then
        begin
          RuleStart := ADate + DatetimeToEzDatetime(Frac(Rule.Start));
          RuleStop := ADate + DatetimeToEzDatetime(Frac(Rule.Stop));

  				// Set interval before dtStart to 0
	  			if(RuleStart > Last) then
            TempProfile.SetInterval(Last, RuleStart, 0, []);

				  TempProfile.AndInterval(RuleStart, RuleStop, 1.0);
				  Last := RuleStop;
        end;
      end;
  		// Set interval behind dtLast 0
	  	if(Last > ADate) and (Last < ADate+ONE_DAY) then
        TempProfile.SetInterval(Last, ADate+ONE_DAY, 0.0, []);
    end;

  	//
	  // Apply other rules
  	//
    for i:=0 to Rules.Count-1 do
    begin
      Rule := TEzCalendarRule(Rules[i]);

    	if (Rule.RuleType <> crtWorkingHour) and
		     ((Frac(Rule.Start) <> 0)  or (Frac(Rule.Stop) <> 0))
      then
    	  //
		    // Apply 'interval' rule. This rule applies to a part of the day only and is used
    		// to override working intervals
		    //
      begin
    	  RuleStart := ADate + DatetimeToEzDatetime(Frac(Rule.Start));
		    RuleStop := ADate + DatetimeToEzDatetime(Frac(Rule.Stop));

    		if Rule.IntervalType = itWorking then
    		  //
		    	// if interval is a working interval then we use 'or'
				  //
          TempProfile.OrInterval(RuleStart, RuleStop, 1.0)
        else
		    	//
				  // if interval is a working interval then we use 'or'
    		  //
		    	TempProfile.AndInterval(RuleStart, RuleStop, 0.0);
      end;
    end;

    if TempProfile.Count = 1 then
  		//
	  	// Profile is a single straight line
		  //
    begin
      if TempProfile[0].Units=0 then
    		SetInterval(ADate, ADate+ONE_DAY, 0, [])
    end
    else
    begin
      TempIndex := 1;
      TempPoint := TempProfile[TempIndex];

      if TempPoint.DateValue>ADate then
        //
        // Update interval between start of day and first point in TempProfile.
        //
        SetInterval(ADate, TempPoint.DateValue, TempProfile[TempIndex-1].Units, []);

      NextTempPoint:=nil; // Prevent might not been initialized warning
      while TempIndex<TempProfile.Count-1 do
      begin
        NextTempPoint := TempProfile[TempIndex+1];

        if TempPoint.Units = 0 then
          SetInterval(TempPoint.DateValue, NextTempPoint.DateValue, 0, [])
        else
        begin
          if not IndexOf(LocalIndex, TempPoint.DateValue) then
            dec(LocalIndex);

          LocalPoint := Items[LocalIndex];
          while (LocalIndex<Count) and (LocalPoint.DateValue < NextTempPoint.DateValue) do
          begin
            if LocalPoint.Units>0 then
            begin
              ts.Start := max(LocalPoint.DateValue, TempPoint.DateValue);
              if LocalIndex<Count-1 then
                ts.Stop := min(ADate+ONE_DAY, min(NextTempPoint.DateValue, Items[LocalIndex+1].DateValue)) else
                ts.Stop := min(ADate+ONE_DAY, NextTempPoint.DateValue);
              SetInterval(ts.Start, ts.Stop, LocalPoint.Units, [])
            end;
            inc(LocalIndex);
            if LocalIndex<Count then
              LocalPoint := Items[LocalIndex];
          end;
        end;
        inc(TempIndex);
        TempPoint := NextTempPoint;
      end;

      if NextTempPoint.DateValue<ADate+ONE_DAY then
        //
        // Update interval between start of day and first point in TempProfile.
        //
        SetInterval(NextTempPoint.DateValue, ADate+ONE_DAY, NextTempPoint.Units, []);

    end;
  finally
    TempProfile.Destroy;
  end;
end;

function TEzAvailabilityProfile.CompareExact(var Item1, Item2): Integer;
var
  i1: TEzAvailabilityPoint absolute Item1;
  i2: TEzAvailabilityPoint absolute Item2;
  n1, n2: Integer;

begin
  if i1.DateValue < i2.DateValue then
    Result := -1
  else if i1.DateValue > i2.DateValue then
    Result := 1
  else begin
    n1 := AvailabilityFlagsToInt(i1.Flags);
    n2 := AvailabilityFlagsToInt(i2.Flags);
    if n1<n2 then
      Result := -1
    else if n1>n2 then
      Result := 1
    else
      Result := 0;
  end;
end;

function TEzAvailabilityProfile.CompareItems(Obj1, Obj2: TObject): Integer;
var
  i1: TEzAvailabilityPoint;
  i2: TEzAvailabilityPoint;

begin
  i1 := TEzAvailabilityPoint(Obj1);
  i2 := TEzAvailabilityPoint(Obj2);

  if i1.DateValue < i2.DateValue then
    Result := -1
  else if i1.DateValue > i2.DateValue then
    Result := 1
  else
    Result := 0;
end;

function TEzAvailabilityProfile.CalcWorkingHours(Start, Stop: TDateTime): Double;
var
  i_point, i_next: Integer;
  NextDate: TEzDateTime;
  av_point: TEzAvailabilityPoint;
  EzStart, EzStop: TEzDatetime;

begin
  EzStart := DatetimeToEzDatetime(Start);
  EzStop := DatetimeToEzDatetime(Stop);
  PrepareDateRange(MakeTimespan(EzStart, EzStop));
  Result := 0;

  if not IndexOf(i_point, EzStart) then
    dec(i_point);

  i_next := i_point+1;

  repeat
    if i_next < Count then
      NextDate := Items[i_next].DateValue else
      NextDate := END_OF_TIME;

    av_point := Items[i_point];
    if av_point.Units>0 then
      Result := Result +
                (EzDatetimeToDatetime(min(EzStop, NextDate))-
                 EzDatetimeToDatetime(max(EzStart, av_point.DateValue)))*av_point.Units;
    i_point := i_next;
    inc(i_next);

  until NextDate>EzStop;
end;

procedure TEzAvailabilityProfile.Clear;
begin
	FPreparedRange.Start := Unprepared;
	FPreparedRange.Stop := END_OF_TIME;
	inherited Clear;
  if not Destroying then
    TList(Self).Add(TEzAvailabilityPoint.Create(0, FDefaultAllocation, []));
end;

procedure TEzAvailabilityProfile.ClearSources;
begin
  FSourceNotifiers.Clear;
  FSources.Clear;
  Clear;
end;

procedure TEzAvailabilityProfile.CopyFrom(var Interval: TEzTimespan; CopyFrom: TEzAvailabilityProfile);
var
  i_p1: Integer;
  p: TEzAvailabilityPoint;
  Go: Boolean;
begin
  // Make sure profile is prepared over requested interval
  CopyFrom.PrepareDateRange(Interval);

  if CopyFrom.Count = 0 then
    EzAvailabilityError(SNoDataToCopy);

  if not CopyFrom.IndexOf(i_p1, Interval.Start) then
    dec(i_p1);

  p := CopyFrom[i_p1];

  // Clear data between start and stop
  SetInterval(Interval.Start, Interval.Stop, p.Units, p.Flags);

  inc(i_p1);
  if i_p1 < CopyFrom.Count then
  begin
    Go := True;
    p := CopyFrom[i_p1];

    while Go and (p.DateValue < Interval.Stop) do
    begin
      // Calls TEzSortedObjectList.Add !!!
      Add(TObject(TEzAvailabilityPoint.Create(p)));
      inc(i_p1);
      if i_p1 < CopyFrom.Count then
        p := CopyFrom[i_p1] else
        Go := False;
    end;
  end;
end;

procedure TEzAvailabilityProfile.CopyScheduleProfile(Interval: TEzTimespan);
var
  SelectSources: array of Boolean;
  iIndexes: array of Integer; // an index for each source
  avpPoints: array of TEzAvailabilityPoint; // a point for each source
  avpNew, avpCurrent, avpSavedEnd: TEzAvailabilityPoint;
  i: Integer;
  ADate: TEzDateTime;
  profile: TEzAvailabilityProfile;

  function FindNextDate: TEzDateTime;
  var
    iSource, n: Integer;
    APoint: TEzAvailabilityPoint;
  begin
    Result := Interval.Stop;

    for iSource:=0 to FSources.Count-1 do
      with TEzAvailabilityProfile(FSources[iSource]) do
      begin
        SelectSources[iSource] := False;
        if (iIndexes[iSource]+1<Count) then
        begin
          APoint := Items[iIndexes[iSource]+1];
          if APoint.DateValue<Result then
            //
            // We have a new minimum
            //
          begin
            // Reset selected sources
            for n:=0 to iSource-1 do
              SelectSources[n] := False;

            // Remember that this source matches minimum date
            SelectSources[iSource] := True;
            Result := APoint.DateValue;
          end
          else if (APoint.DateValue=Result) then
            // This source matches minimum date too!!
            SelectSources[iSource] := True;
        end;
      end;

      for iSource:=0 to FSources.Count-1 do
        if SelectSources[iSource] then
        begin
          inc(iIndexes[iSource]);
          avpPoints[iSource] := TEzAvailabilityProfile(FSources[iSource])[iIndexes[iSource]];
        end;
  end;

begin
  SetLength(SelectSources, FSources.Count);
  SetLength(iIndexes, FSources.Count);
  SetLength(avpPoints, FSources.Count);

  for i:=0 to FSources.Count-1 do
  begin
{$IFDEF DEBUG}
  try
{$ENDIF}
    profile := TEzAvailabilityProfile(FSources[i]);
    // Make sure profile is prepared over requested interval
    profile.PrepareDateRange(Interval);
    // Get start position
    iIndexes[i] := profile.DateToIndex(Interval.Start);
    avpPoints[i] := profile.Items[iIndexes[i]];

{$IFDEF DEBUG}
  except
    profile := TEzAvailabilityProfile(FSources[i]);
    // Make sure profile is prepared over requested interval
    profile.PrepareDateRange(Interval);

    // Get start position
    iIndexes[i] := profile.DateToIndex(Interval.Start);
    avpPoints[i] := profile.Items[iIndexes[i]];
  end;
{$ENDIF}

  end;

  ADate := Interval.Start;
  avpSavedEnd := TEzAvailabilityPoint.Create(Items[DateToIndex(Interval.Stop)]);
  avpCurrent := Items[DateToIndex(Interval.Start)];

  avpNew := TEzAvailabilityPoint.Create(ADate, FScheduleProfileBaseUnits, []);

  try
    while ADate<Interval.Stop do
    begin
      i:=0;

      while (i<FSources.Count) do
      begin
        if (i=0) and (Operator in [opAnd, opOr]) then
          avpNew.Units := avpPoints[i].Units

        else if Operator in [opScheduleProfile, opAnd] then
          // Set new points units to the lowest availability
          avpNew.Units := max(0, min(avpNew.Units, avpPoints[i].Units))

        else
          // Operator in [opMergedProfile, opOr] then
          avpNew.Units := max(0, max(avpNew.Units, avpPoints[i].Units));

        if afNoScheduleAcross in avpPoints[i].Flags then
          include(avpNew.Flags, afNoScheduleAcross);

        if not (afIsAllocated in avpNew.Flags) and
           (afIsAllocated in avpPoints[i].Flags) and
           not TEzAvailabilityProfile(FSources[i]).AllowTaskSwitching
        then
          Include(avpNew.Flags, afIsAllocated);
        inc(i);
      end;

      if (avpNew.Units<>avpCurrent.Units) or (avpNew.Flags<>avpCurrent.Flags) then
      begin
        if avpCurrent.DateValue=ADate then
        begin
          avpCurrent.Units := avpNew.Units;
          avpCurrent.Flags := avpNew.Flags;
          avpNew.Units := FScheduleProfileBaseUnits;
          avpNew.Flags := [];
        end
        else
        begin
          avpNew.DateValue := ADate;
          inherited Add(avpNew);
          avpCurrent := avpNew;
          avpNew := TEzAvailabilityPoint.Create(0, FScheduleProfileBaseUnits, []);
        end;
      end else
      begin
        avpNew.Units := FScheduleProfileBaseUnits;
        avpNew.Flags := [];
      end;

      ADate := FindNextDate;
    end;

    // Make sure existing data outside interval was is not altered.
    // Reinsert point at Interval.Stop if it is different.
    if ((avpSavedEnd.Units<>avpCurrent.Units) or (avpSavedEnd.Flags<>avpCurrent.Flags)) and
       not IndexOf(i, Interval.Stop)
    then
    begin
      avpSavedEnd.DateValue := Interval.Stop;
      inherited Add(avpSavedEnd);
      avpSavedEnd := nil; // Prevent destroy
    end;
  finally
    avpNew.Free;
    avpSavedEnd.Free;
  end;
end;

procedure TEzAvailabilityProfile.CopyWithOperator(Interval: TEzTimespan; CopyFrom: TEzAvailabilityProfile);
var
  i: Integer;
  p: TEzAvailabilityPoint;
  Delta: Double;
  Start, Stop: TEzDateTime;

begin
  // Make sure profile is prepared over requested interval
  CopyFrom.PrepareDateRange(Interval);

  i := CopyFrom.DateToIndex(Interval.Start);
  p := CopyFrom.Items[i];

  while (i<CopyFrom.Count) and (p.DateValue < Interval.Stop) do
  begin
    // KV: 6 feb 2009
    // We want to substract negative profiles as well.
    // ==> Skip check on units
//    if (p.Units>0) or (Operator in [opAnd, opOr]) then
    begin
      Delta := P.Units;
      Start := max(p.DateValue, Interval.Start);
      if i<CopyFrom.Count-1 then
      begin
        inc(i);
        p := CopyFrom.Items[i];
        Stop := min(p.DateValue, Interval.Stop);
      end
      else
      begin
        Stop := Interval.Stop;
        i:=CopyFrom.Count;
      end;

      case Operator of
        opSum: SumInterval(Start, Stop, Delta, True);
        opSubstract:  SumInterval(Start, Stop, -Delta, True);
        opAnd: AndInterval(Start, Stop, Delta);
        opOr: OrInterval(Start, Stop, Delta);
      end;
    end
//    else
//    begin
//      inc(i);
//      if i<CopyFrom.Count then
//        p := CopyFrom.Items[i];
//    end;
  end;
end;

procedure TEzAvailabilityProfile.CopyTo(Interval: TEzTimespan; CopyTo: TEzTimespanArray);
var
  i: Integer;
  p: TEzAvailabilityPoint;
  Timespan: TEzTimeSpan;

begin
  PrepareDateRange(Interval);

  i := DateToIndex(Interval.Start);
  p := Items[i];

  while (i<Count) and (p.DateValue < Interval.Stop) do
  begin
    if p.Units>0 then
    begin
      Timespan.Start := max(p.DateValue, Interval.Start);
      if i < Count-1 then
      begin
        inc(i);
        p := Items[i];
        Timespan.Stop := min(p.DateValue, Interval.Stop);
      end
      else
      begin
        // KV: 27-6-2006
        // This line replaced with next: Timespan.Stop := min(p.DateValue, Interval.Stop);
        Timespan.Stop := Interval.Stop;        
        i:=Count;
      end;

      CopyTo.Add(Timespan);
    end
    else
    begin
      inc(i);
      if i<Count then
        p := Items[i];
    end;
  end;
end;

function TEzAvailabilityProfile.DateToIndex(ADate: TDateTime): Integer;
begin
  Result := DateToIndex(DatetimeToEzDateTime(ADate));
end;

function TEzAvailabilityProfile.DateToIndex(ADate: TEzDateTime): Integer;
begin
  if not IndexOf(Result, ADate) then
    dec(Result);
end;

function TEzAvailabilityProfile.IndexOf(var Index: Integer; ADate: TEzDateTime): Boolean;
var
  Find: TEzAvailabilityPoint;

begin
  Find := TEzAvailabilityPoint.Create;
  try
    Find.DateValue := ADate;
    Result := IndexOf(Index, Find);
  finally
    Find.Destroy;
  end;
end;

function TEzAvailabilityProfile.IndexOf(var Index: Integer; ADate: TDateTime): Boolean;
begin
  Result := IndexOf(Index, DatetimeToEzDateTime(ADate));
end;

function TEzAvailabilityProfile.GetItem(Index: Integer): TEzAvailabilityPoint;
begin
  Result := TEzAvailabilityPoint(inherited GetItem(Index));
end;

function TEzAvailabilityProfile.GetOwnsSources: Boolean;
begin
  Result := FSources.OwnsObjects;
end;

function TEzAvailabilityProfile.GetUnits(ADate: TDateTime): Double;
var
  i: Integer;

begin
  if IndexOf(i, ADate) then
    Result := Items[i].Units else
    Result := Items[i-1].Units;
end;

function TEzAvailabilityProfile.InternalSumInterval(Start, Stop: TEzDateTime; Delta: Double; AllowNegative, MarkAllocated: Boolean): Boolean;
var
  iStart, iStop, i: Integer;
  Find: TEzAvailabilityPoint;
  P1, P2: TEzAvailabilityPoint;
  Units: Double;

  function TreatFlagsEqual(F1, F2: TAvailabilityFlags): Boolean;
  begin
    Result :=
      ((F1*[afNoScheduleAcross])=(F2*[afNoScheduleAcross]))
      and
      (
        (MarkAllocated and (afIsAllocated in F2))
        or
        (not MarkAllocated and not (afIsAllocated in F2))
      );
  end;

  function GetFlags: TAvailabilityFlags;
  begin
    if MarkAllocated then
      Result := [afIsAllocated] else
      Result := [];
  end;

  function SumUnits(Value, Delta: Double): Double;
  begin
    if AllowNegative then
      Result := Value+Delta else
      Result := max(0, Value+Delta);
  end;

begin
  if Start >= Stop then
    EzAvailabilityError(SInvalidDates);

  Result := False;
  Find := TEzAvailabilityPoint.Create;
  try
    if Stop<>END_OF_TIME then
      //
      // Handle Stop point
      //
    begin
      Find.DateValue := Stop;
      if not IndexOf(iStop, Find) then
      begin
        P1 := Items[iStop-1];
        Insert(iStop, TEzAvailabilityPoint.Create(Stop, P1.Units, P1.Flags));
      end
      else
        //
        // Stop point exists, check whether it can savely be removed
        //
        begin
        P1 := Items[iStop-1];
        P2 := Items[iStop];
        if (SumUnits(P1.Units, Delta)=P2.Units) and TreatFlagsEqual(P1.Flags, P2.Flags) then
          //
          // Delete end point
          //
          Delete(iStop);
      end;
    end
    else
    begin
      iStop := Count-1;
      P1 := Items[iStop];
    end;

    //
    // Handle Start point
    //
    Find.DateValue := Start;
    if not IndexOf(iStart, Find) then
    begin
      if iStart > 0 then
        Units := SumUnits(Items[iStart-1].Units, Delta) else
        Units := Delta;
      Result := Result or (Units<0); // Negative value inserted

      Insert(iStart, TEzAvailabilityPoint.Create(Start, Units, GetFlags));
      inc(iStop);
    end
    else if (iStart > 0) then
    begin
      P1 := Items[iStart-1];
      P2 := Items[iStart];
      if (P1.Units = SumUnits(P2.Units, Delta)) and TreatFlagsEqual(P2.Flags, P1.Flags {P1 will be tested for allocated flag!}) then
      begin
        Delete(iStart);
        Dec(iStart); // Make sure next point gets updated in for-loop
        Dec(iStop);
      end else
      begin
        Units := SumUnits(P2.Units, Delta);
        Items[iStart] := TEzAvailabilityPoint.Create(P2.DateValue, Units, GetFlags);
        Result := Result or (Units<0); // Negative value inserted
      end;
    end
    else
    begin
      Units := SumUnits(P1.Units, Delta);
      Items[iStart] := TEzAvailabilityPoint.Create(P1.DateValue, Units, GetFlags);
      Result := Result or (Units<0); // Negative value inserted
    end;

    // Handle points between iStart and iStop
    for i:=iStart+1 to iStop-1 do
    begin
      P1 := Items[i];
      Units := SumUnits(P1.Units, Delta);
      Items[i] := TEzAvailabilityPoint.Create(P1.DateValue, Units, GetFlags);
      Result := Result or (Units<0); // Negative value inserted
    end;
  finally
    Find.Destroy;
  end;
end;

procedure TEzAvailabilityProfile.PrepareDateRange(Interval: TEzTimespan);
var
  i: Integer;
  Profile: TEzAvailabilityProfile;
  ts: TEzTimeSpan;

begin
	if FPreparing or not RequiresPrepare then Exit;

  if Interval.Stop<=Interval.Start then
    EzAvailabilityError(SInvalidDates);

  // We cannot prepare an unlimited range when rules are set
	if (FCalendarRules <> nil) and ((Interval.Start = 0) or (Interval.Stop = END_OF_TIME)) then
    EzAvailabilityError(SDaterangeRequired);

  // Round to whole days when requesting a range
	if Interval.Start <> 0 then
  begin
		Interval.Start := FloorEzDateTime(Interval.Start);
	  if FracEzDateTime(Interval.Stop) <> 0 then
		  Interval.Stop := FloorEzDatetime(Interval.Stop) + TIME_FACTOR;
  end;

  // if some range is already active, then remove possible overlaps.
	if FPreparedRange.Start <> Unprepared then
  begin
		if (Interval.Start >= FPreparedRange.Start) and (Interval.Stop <= FPreparedRange.Stop) then
	  	Exit; // Nothing to do

    if (Interval.Start<FPreparedRange.Start) and (Interval.Stop>FPreparedRange.Stop) then
    begin
      FPreparing := False;
      ts.Start := Interval.Start;
      ts.Stop := FPreparedRange.Start;
      PrepareDateRange(ts);
      ts.Start := FPreparedRange.Stop;
      ts.Stop :=Interval.Stop;
      PrepareDateRange(ts);
      Exit;
    end;

    if Interval.Start >= FPreparedRange.Start then
			Interval.Start := FPreparedRange.Stop;
  	if Interval.Stop<=FPreparedRange.Stop then
			Interval.Stop := FPreparedRange.Start;
  end;

  FPreparing := True;
  DisableNotifications;
  try
  	// Copy data from source(s) for range requested
    if FSources.Count>0 then
    begin
      if Operator in [opScheduleProfile, opMergeScheduleProfile, opAnd, opOr] then
        CopyScheduleProfile(Interval)

      else
      begin
        Profile := TEzAvailabilityProfile(FSources[0]);

        if (FSources.Count=1) and (FCalendarRules=nil) and not Profile.RequiresPrepare then
        begin
          // Special case, make a plain copy of Source
          Assign(Profile);
          Interval.Start := 0;
          Interval.Stop := END_OF_TIME;
        end
        else
        begin
          Profile.PrepareDateRange(Interval);
          CopyFrom(Interval, Profile);

          for i:=1 to FSources.Count-1 do
          begin
            Profile := TEzAvailabilityProfile(FSources[i]);
            Profile.PrepareDateRange(Interval);
            CopyWithOperator(Interval, Profile);
          end;
        end;
      end;
    end;

	  // Apply rules
  	if FCalendarRules <> nil then
      ApplyRules(Interval.Start, Interval.Stop);

  	// Save current range
	  if FPreparedRange.Start <> Unprepared then
  	begin
	  	FPreparedRange.Start := min(FPreparedRange.Start, Interval.Start);
		  FPreparedRange.Stop := max(FPreparedRange.Stop, Interval.Stop);
  	end
	  else
  	begin
	  	FPreparedRange.Start := Interval.Start;
		  FPreparedRange.Stop := Interval.Stop;
  	end;
  finally
    FPreparing := False;
    EnableNotifications;
  end;
end;

function TEzAvailabilityProfile.RequiresPrepare: Boolean;
begin
  Result := ((FCalendarRules <> nil) and (FCalendarRules.Count>0)) or
             (FSources.Count>0);
end;

procedure TEzAvailabilityProfile.ScheduleBackward(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray);
var
  i_point: Integer;
  reuse_current_point: Boolean;
  av_point: TEzAvailabilityPoint;
  Allocated, IntervalDuration: TEzDuration;
  d: TDateTime;
  s: string;
  StartDate, StopDate, IntervalDate, MaxDate: TEzDateTime;
  TotalUnits, IntervalUnits: Double;

  function DataStopDate: TEzDateTime;
  begin
    if Data.SchedulebeyondScheduleWindow then
      Result := Data.AbsoluteStop else
      Result := Data.Stop;
  end;

  function DataStartDate: TEzDateTime;
  begin
    if Data.SchedulebeyondScheduleWindow then
      Result := Data.AbsoluteStart else
      Result := Data.Start;
  end;

  function PreparePreviousRange(Profile: TEzAvailabilityProfile): Boolean;
{
  var
    CountValue: Integer;
}
  begin
    if not Profile.RequiresPrepare then
    begin
      Result := True;
      Exit;
    end
    else if Profile.FPreparedRange.Start<DataStartDate then
    begin
      Include(Data.ScheduleResults, srScheduledFailed);
      Result := False;
    end
    else
    begin
      Profile.ValidateDate(Profile.FPreparedRange.Start-Profile.FPrepareBlockSize);
      Result := True;
    end;
{
    CountValue := Profile.Count;
    repeat
      Profile.ValidateDate(Profile.FPreparedRange.Start-Profile.FPrepareBlockSize)
    until (CountValue<>Profile.Count) or (Profile.FPreparedRange.Start<DataStartDate);

    Result := CountValue<>Profile.Count;
    if not Result then
      Include(ScheduleResult, srScheduledFailed);
}
  end;

  procedure StoreInterval(Start, Stop: TEzDateTime);
  var
    s: TDateTime;
    Timespan: TEzTimespan;

  begin
{$IFDEF DEBUG}
    s := EzDatetimeToDatetime(Start);
    if s = Now then;
    s := EzDatetimeToDatetime(Stop);
    if s = Now then;
{$ENDIF}

		// Normalize intervals:
		// We extend previous interval if start of current interval
		// aligns with stop of previous interval else insert a new
		// interval.
		if (Intervals.Count > 0) and (Intervals[0].Start <= Stop) then
    begin
      Timespan := Intervals[0];
      Timespan.Start := Start;
			Intervals[0] := Timespan;
    end
    else
    begin
      Timespan := MakeTimespan(Start, Stop);
			Intervals.InsertAt(0, Timespan);
    end;
  end;

  function GotoPreviousDate(AProfile: TEzAvailabilityProfile; var AIndex: Integer; ADate: TEzDateTime): TEzDateTime;
  var
    av: TEzAvailabilityPoint;

  begin
    if not reuse_current_point then
      dec(AIndex);

    av := AProfile.Items[AIndex];
    if (av.DateValue<AProfile.FPreparedRange.Start) then
    begin
      repeat
        PreparePreviousRange(AProfile);
        if Data.ScheduleResults=[] then
        begin
          AProfile.IndexOf(AIndex, ADate);
          dec(AIndex);
          av := AProfile.Items[AIndex];
        end;
      until (Data.ScheduleResults<>[]) or (av.DateValue>AProfile.FPreparedRange.Start);
    end;

    reuse_current_point := False;
    Result := av.DateValue;
  end;

  procedure MoveIntervalDate(iSource, iSourcePoint: Integer);
  var
    av_source: TEzAvailabilityPoint;
    Profile: TEzAvailabilityProfile;

  begin
    Profile := TEzAvailabilityProfile(Sources[iSource]);
    with Profile do
    begin
      av_source := Items[iSourcePoint];

      while (Data.ScheduleResults=[]) and (av_source.Units<Profile.FUnitsRequired) do
      begin
        IntervalDate := av_source.DateValue;
        StopDate := IntervalDate;
        GotoPreviousDate(Profile, iSourcePoint, IntervalDate);
        if Data.ScheduleResults=[] then av_source := Items[iSourcePoint];
      end;
    end;
  end;

  function CheckUnitsAvailable: Boolean;
  var
    iSource, iSourcePoint: Integer;
    av_source: TEzAvailabilityPoint;
    Profile: TEzAvailabilityProfile;

  begin
    IntervalUnits := 0.0;

    if operator<>opScheduleProfile then
    begin
      Result := av_point.Units>=Data.UnitsRequired;
      if Result then
      begin
        IntervalUnits := Data.UnitsRequired;
        StartDate := max(DataStartDate, av_point.DateValue);
      end;
    end
    else
    begin
      Result := True;
      iSource:=0;
      StartDate := IntervalDate;
      while Result and (iSource<Sources.Count) do
      begin
        Profile := TEzAvailabilityProfile(Sources[iSource]);
        //with Profile do
        begin
          // Locate the nearest point in source profile, lying just before
          // current stopdate.
          Profile.IndexOf(iSourcePoint, StopDate);
          dec(iSourcePoint);

          av_source := Profile.Items[iSourcePoint];
          Result := av_source.Units >= Profile.FUnitsRequired;

          // If addequate units are available,
          // then get datevalue of this point in profile.
          // Because point lies before stopdate, we can use av_source.DateValue.
          if Result then
          begin
            if Profile.AffectsDuration then
              IntervalUnits := IntervalUnits + Profile.UnitsRequired;

            // KV: 11-7-2011
            // Reset i_point
            if av_source.DateValue > StartDate then
            begin
              reuse_current_point := True;
              StartDate := av_source.DateValue;
            end;

            // Old code was:
            // StartDate := max(StartDate, av_source.DateValue);
          end else
            // Move IntervalDate to the next point in source profile
            // where availability > required units
            MoveIntervalDate(iSource, iSourcePoint);

          inc(iSource);
        end;
      end;
    end;

    if IntervalUnits = 0.0 then
      IntervalUnits := 1.0;
  end;

  function AllocateProfileUnits: Double;
  var
    iSource, iSourcePoint: Integer;
    av_source: TEzAvailabilityPoint;

  begin
    if operator<>opScheduleProfile then
      //
      // Schedule against single source profile
      // (i.e. profile belongs to one resource only)
      //
    begin
      Result := av_point.Units;
      StartDate := max(DataStartDate, av_point.DateValue);
    end
    else
      //
      // Schedule against multi resource profile
      //
    begin
      Result := 0.0;
      iSource:=0;
      StartDate := IntervalDate;
      while (iSource<Sources.Count) do
      begin
        with TEzAvailabilityProfile(Sources[iSource]) do
        begin
          // Locate the nearest point in source profile, lying just before
          // current stopdate.
          IndexOf(iSourcePoint, StopDate);
          dec(iSourcePoint);

          av_source := Items[iSourcePoint];
          Result := Result+av_source.Units;

          // Get datevalue of this point in profile.
          // Because point lies before stopdate, we can use av_source.DateValue.
          StartDate := max(StartDate, av_source.DateValue);

          inc(iSource);
        end;
      end;
    end;
  end;

  // Restart scheduling
  // Restart will update StartDate to the next availability point
  // NOT having flag Flag.
  procedure Restart(Flag: TAvailabilityFlag);
  var
    av_source: TEzAvailabilityPoint;

  begin
    Intervals.clear;
    Allocated := 0;

    StopDate := IntervalDate;
    IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);
    av_source := Items[i_point];

    while (Data.ScheduleResults=[]) and (Flag in av_source.Flags) do
    begin
      StopDate := IntervalDate;
      IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);
      av_source := Items[i_point];
    end;
  end;

begin
  Allocated := 0;

  // Always start scheduling from the end of tasks schedule window.
  StopDate := Data.Stop;

  {$IFDEF DEBUG}
  d := EzDateTimeToDateTime(Data.Stop);
  if d = 0 then;
  {$ENDIF}

  // Initialize IntervalDate, date should always lie before Data.Stop date
  IndexOf(i_point, Data.Stop);
  IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);

  {$IFDEF DEBUG}
  d := EzDateTimeToDateTime(IntervalDate);
  if d = 0 then;
  {$ENDIF}

  repeat
    // Locate nearest point in graph,
    // we always use IntervalDate to get index of nearest point
{    if not IndexOf(i_point, IntervalDate) then
      IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);
}

    av_point := Items[i_point];

    {$IFDEF DEBUG}
    d := EzDateTimeToDateTime(av_point.DateValue);
    if d = 0 then;
    {$ENDIF}

    if (av_point.DateValue<FPreparedRange.Start) then
      //
      // We are outside prepared daterange, therefore we need to prepare
      // next range and try again.
      //
    begin
      PreparePreviousRange(Self);
      if Data.ScheduleResults=[] then
      begin
        if not IndexOf(i_point, IntervalDate) then
          dec(i_point);
        av_point := Items[i_point];
      end
      else
        //
        // Preparing did not succeed, schedule fails.
        //
        Exit;
    end;

    if afNoScheduleAcross in av_point.Flags then
      //
      // Task may not be scheduled across current interval (holidays etc.)
      //
      Restart(afNoScheduleAcross)

    // KV: 23 mei 2007
    // Code dissabled because this would restart scheduling even when a resource
    // had adequate availability.
    // For example when the profile has 4 units available, and another task was
    // already scheduled, this code would still restart the scheduling even though
    // current availability would be 3.
//    else if (afIsAllocated in av_point.Flags) and (not Data.ResourceAllowsSwithing) and
//            (Intervals.Count>0)
//    then
//			//
//			// afIsAllocated will be set for a certain interval if a resource
//      // is assigned to a (previously scheduled) task during that interval.
//      // Because a resource can only be working at one task at a time, we need
//      // to restart.
//      //
//      Restart

    else if av_point.Units<=0 then
    begin
      if (afIsAllocated in av_point.Flags) and (not Data.ResourceAllowsSwithing) and (Intervals.Count>0) then
      begin
        // Restart will set
        // StopDate and IntervalDate as well
        Restart(afIsAllocated);
      end
      else
      begin
        StopDate := av_point.DateValue;
        IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);
      end;
    end

      //
      // In case of a fixed allocation,
      // Check if interval's availability is sufficient,
      // then assign this interval to the task
      //
    else if (Data.AllocationType=alFixed) and CheckUnitsAvailable then
    begin
      IntervalDuration := Trunc((StopDate - StartDate) * IntervalUnits);
      if Allocated + IntervalDuration + ONE_SECOND > Data.Duration {Prevents rounding issues} then
      begin
        MaxDate := Max(StartDate, StopDate - Ceil64((Data.Duration-Allocated) / IntervalUnits));

        // Prevent rouding issues.
        // If calculated date is very near to StartDate, we allocate
        // the whole interval (i.e. we do not update StartDate with MaxDate).
        // Otherwise we do...
        if (MaxDate - ONE_SECOND) > StartDate then
          StartDate := MaxDate;

        Allocated := Data.Duration;
      end else
        inc(Allocated, IntervalDuration);

      // Remember:
      // Here we have no risk of scheduling beyond prepared interval because
      // av_point is always located to the left of StartDate
      // (graph is left to right and scheduling goes right to left)

      StoreInterval(StartDate, StopDate);

      if Allocated>=Data.Duration then
        include(Data.ScheduleResults, srScheduledOK)
      else
      begin
        StopDate := StartDate;

        if i_point>0 then
          IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);
      end;
    end

    else if (Data.AllocationType=alProfile) then
    begin
      TotalUnits := AllocateProfileUnits;
      if TotalUnits>0 then
      begin
        // StartDate is set in AllocateProfileUnits !!
        if StopDate-Trunc((Data.Duration-Allocated)/TotalUnits)>=StartDate+ONE_SECOND then
          StartDate := StopDate-Trunc((Data.Duration-Allocated)/TotalUnits);

        Allocated := Trunc(Allocated+(StopDate-StartDate)*TotalUnits);

        StoreInterval(StartDate, StopDate);
        if Allocated+ONE_SECOND >= Data.Duration then
          include(Data.ScheduleResults, srScheduledOK)
        else
        begin
          StopDate := StartDate;
          if i_point>0 then
            IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);
        end;
      end
      else
      begin
        StopDate := StartDate;
        if i_point>0 then
          IntervalDate := GotoPreviousDate(Self, i_point, IntervalDate);
      end;
    end;

    {
      KV: 8 april 2005
      Extra check added:
      Stop scheduling when we run outside schedulewindow
    }
    if not (srScheduledOK in Data.ScheduleResults) and (Stopdate<=DataStartDate) then
      include(Data.ScheduleResults, srScheduledFailed);

  until Data.ScheduleResults<>[];

  // KV: 8 april 2005
  // Check whether task has been scheduled inside schedule window.
  // A task can be scheduled successfull inside the absolute project window,
  // but outside the schedule window.
  if (srScheduledOK in Data.ScheduleResults) and (Intervals[Intervals.Count-1].Start < Data.Start) then
    include(Data.ScheduleResults, srScheduledOutsideWindow);
end;

type
  TPrepareRangeResult = (prNone, prDataAdded, prRangeExtended);

procedure TEzAvailabilityProfile.ScheduleForward(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray);
var
  i_point: Integer;
  av_point: TEzAvailabilityPoint;
  Allocated, IntervalDuration: TEzDuration;
  StartDate, StopDate, MinDate: TEzDateTime;
  TotalUnits, IntervalUnits: Double;

  function DataStopDate: TEzDateTime;
  begin
    if Data.SchedulebeyondScheduleWindow then
      Result := Data.AbsoluteStop else
      Result := Data.Stop;
  end;

  function DataStartDate: TEzDateTime;
  begin
    if Data.SchedulebeyondScheduleWindow then
      Result := Data.AbsoluteStart else
      Result := Data.Start;
  end;

  function PrepareNextRange(Profile: TEzAvailabilityProfile; PrepareTo: TEzDateTime): TPrepareRangeResult;
  var
    CountValue: Integer;
    d: TDateTime;

  begin
    Result := prNone;

    {$IFDEF DEBUG}
    d := EzDatetimeToDatetime(Profile.FPreparedRange.Stop);
    if d = Now then;
    d := EzDatetimeToDatetime(DataStopDate);
    if d = Now then;
    {$ENDIF}
    if Profile.FPreparedRange.Stop>DataStopDate then
    begin
      Include(Data.ScheduleResults, srScheduledFailed);
      Exit;
    end;

    CountValue := Profile.Count;
    while Result = prNone do
    begin
      Profile.ValidateDate(Profile.FPreparedRange.Stop);
      if (CountValue<>Profile.Count) then
        Result := prDataAdded
      else if Profile.FPreparedRange.Stop>=PrepareTo then
        Result := prRangeExtended;
    end;
  end;

  procedure StoreInterval(Start, Stop: TEzDateTime);
  var
    Timespan: TEzTimeSpan;

  begin
		// Normalize intervals:
		// We extend previous interval if start of current interval
		// aligns with stop of previous interval else insert a new
		// interval.
		if (Intervals.Count > 0) and (Intervals[Intervals.Count-1].Stop>=Start) then
    begin
      Timespan := Intervals[Intervals.Count-1];
      Timespan.Stop := Stop;
			Intervals[Intervals.Count-1] := Timespan;
    end
    else
    begin
      Timespan := MakeTimespan(Start, Stop);
			Intervals.Add(Timespan);
    end;
  end;

  procedure MoveStartDate(iSource, iSourcePoint: Integer);
  var
    av_source: TEzAvailabilityPoint;
    Profile: TEzAvailabilityProfile;

  begin
    Profile := TEzAvailabilityProfile(Sources[iSource]);
    with Profile do
    begin
      av_source := Items[iSourcePoint];
      while (Data.ScheduleResults=[]) and (av_source.Units<Profile.FUnitsRequired) do
      begin
        inc(iSourcePoint);
        if iSourcePoint=Profile.Count then
          while (Data.ScheduleResults=[]) and (PrepareNextRange(Profile, DataStopDate)<>prDataAdded) do;
        if Data.ScheduleResults=[] then av_source := Items[iSourcePoint];
      end;
      if Data.ScheduleResults=[] then StartDate := av_source.DateValue;
    end;
  end;

  function AllocateProfileUnits: Double;
  var
    iSource, iSourcePoint: Integer;
    av_source: TEzAvailabilityPoint;

  begin
    if operator<>opScheduleProfile then
      //
      // Schedule against single source profile
      // (i.e. profile belongs to one resource only)
      //
    begin
      Result := av_point.Units;
      StopDate := DataStopDate;

      // More data available in profile?
      if (i_point<Count-1) then
        StopDate := min(StopDate, Items[i_point+1].DateValue)

      else if (StopDate>=FPreparedRange.Stop) then
        case PrepareNextRange(Self, StopDate) of
          prDataAdded:
            StopDate := min(StopDate, Items[i_point+1].DateValue);
          prRangeExtended:
            StopDate := min(StopDate, FPreparedRange.Stop);
        else
          ;
        end;
    end
    else
      //
      // Schedule against multi resource profile
      //
    begin
      Result := 0.0;
      StopDate := DataStopDate;

      for iSource:=0 to Sources.Count-1 do
        with TEzAvailabilityProfile(Sources[iSource]) do
        begin
          if not IndexOf(iSourcePoint, StartDate) then
            dec(iSourcePoint);

          av_source := Items[iSourcePoint];
          Result := Result+av_source.Units;

          // then get datevalue of next point in profile.
          if (iSourcePoint<Count-1) then
            StopDate := min(StopDate, Items[iSourcePoint+1].DateValue) else
            StopDate := min(StopDate, FPreparedRange.Stop);
        end;
    end;
  end;

  function CheckUnitsAvailable: Boolean;
  var
    iSource, iSourcePoint: Integer;
    av_source: TEzAvailabilityPoint;
    Profile: TEzAvailabilityProfile;

  begin
    IntervalUnits := 0.0;

    if operator<>opScheduleProfile then
    begin
      Result := av_point.Units >= Data.UnitsRequired;
      if Result then
      begin
        IntervalUnits := Data.UnitsRequired;

        // More data available in profile?
        if (i_point<Count-1) then
	  			StopDate := Items[i_point+1].DateValue

        else
        begin
          StopDate := StartDate+(Data.Duration-Allocated);
          if (StopDate>=FPreparedRange.Stop) then
            case PrepareNextRange(Self, StopDate) of
              prDataAdded:
                StopDate := Items[i_point+1].DateValue;
              prRangeExtended:
                StopDate := FPreparedRange.Stop;
            else
              ;
            end;
        end;
      end;
    end
    else
    begin
      Result := True;
      iSource:=0;
      StopDate := DataStopDate;
      while Result and (iSource<Sources.Count) do
      begin
        Profile := TEzAvailabilityProfile(Sources[iSource]);
        with Profile do
        begin
          if not IndexOf(iSourcePoint, StartDate) then
            dec(iSourcePoint);

          av_source := Items[iSourcePoint];
          Result := av_source.Units>=Profile.FUnitsRequired;

          // If addequate units are available, then get datevalue of next point
          // in profile.
          if Result then
          begin
            if Profile.AffectsDuration then
              IntervalUnits := IntervalUnits + Profile.UnitsRequired;

            if (iSourcePoint<Count-1) then
              StopDate := min(StopDate, Items[iSourcePoint+1].DateValue) else
              StopDate := min(StopDate, FPreparedRange.Stop);
          end else
          begin
            // Move start date to the next point with adequate availability
            inc(iSourcePoint);
            MoveStartDate(iSource, iSourcePoint);
          end;

          inc(iSource);
        end;
      end;
    end;

    if IntervalUnits = 0.0 then
      IntervalUnits := 1.0;
  end;

  // Restart scheduling
  // Restart will update StartDate to the next availability point
  // NOT having flag Flag.
  procedure Restart(Flag: TAvailabilityFlag);
  var
    av_source: TEzAvailabilityPoint;
    i: Integer;

  begin
    Intervals.clear;
    Allocated := 0;

    i := i_point;
    av_source := Items[i];

    while (Data.ScheduleResults=[]) and (Flag in av_source.Flags) do
    begin
      inc(i);
      if i=Count then
        while (Data.ScheduleResults=[]) and (PrepareNextRange(Self, DataStopDate)<>prDataAdded) do;
      if Data.ScheduleResults=[] then av_source := Items[i];
    end;

    if Data.ScheduleResults=[] then StartDate := av_source.DateValue;
  end;

begin
  Allocated := 0;

  // We always start scheduling from the start of the schedule window.
  StartDate := Data.Start;
  StopDate := Data.AbsoluteStop;

  repeat
    // Always use startdate to get a new point
    if not IndexOf(i_point, StartDate) then
      dec(i_point);

    av_point := Items[i_point];

    if (afNoScheduleAcross in av_point.Flags) and (Intervals.Count>0) then
      //
      // Task may not be scheduled across current interval (holidays etc.)
      //
    begin
      Restart(afNoScheduleAcross);
    end

    // KV: 23 mei 2007
    // Code dissabled because this would restart scheduling even when a resource
    // had adequate availability.
    // For example when the profile has 4 units available, and another task was
    // already scheduled, this code would still restart the scheduling even though
    // current availability would be 3.
    else if (av_point.Units<=0) and (afIsAllocated in av_point.Flags) and not Data.ResourceAllowsSwithing and (Intervals.Count>0) then
			//
			// afIsAllocated will be set for a interval if a resource
      // is assigned to a (previously scheduled) task during that interval.
      // Because a resource can only be working at one task at a time, we need
      // to restart.
      //
    begin
      Restart(afIsAllocated);
    end

    else if av_point.Units<=0 then
    begin
      while (Data.ScheduleResults=[]) and (av_point.Units<=0) do
      begin
        if (afIsAllocated in av_point.Flags) and not Data.ResourceAllowsSwithing and (Intervals.Count>0) then
          Restart(afIsAllocated);

        if (i_point<Count-1) then
          inc(i_point)
        else if (PrepareNextRange(Self, DataStopDate)=prDataAdded) then
          inc(i_point);

        if Data.ScheduleResults=[] then av_point := Items[i_point];
      end;
      if Data.ScheduleResults=[] then StartDate := av_point.DateValue;
    end

      //
      // In case of a fixed allocation,
      // Check if interval's availability is sufficient,
      // then assign this interval to the task
      //

    else if (Data.AllocationType=alFixed) and CheckUnitsAvailable then
    begin
      IntervalDuration := Trunc((StopDate - StartDate) * IntervalUnits);
      if Allocated + IntervalDuration + ONE_SECOND > Data.Duration then
      begin
        MinDate := Min(StopDate, StartDate + Ceil64((Data.Duration - Allocated) / IntervalUnits));

        // Prevent rouding issues.
        // If calculated date is very near to StopDate, we allocate
        // the whole interval (i.e. we do not update StopDate with MinDate).
        // Otherwise we do...
        if (MinDate + ONE_SECOND) < StopDate then
          StopDate := MinDate;

        Allocated := Data.Duration;
      end else
        inc(Allocated, IntervalDuration);

      StoreInterval(StartDate, StopDate);

      if Allocated >= Data.Duration then
        include(Data.ScheduleResults, srScheduledOK) else
        // Continue scheduling from the end of the allocated interval
        StartDate:=StopDate;
    end

    else if (Data.AllocationType=alProfile) then
    begin
      TotalUnits := AllocateProfileUnits;

      //
      // StopDate is set in AllocateProfileUnits to the end of the current
      // interval !! Trim StopDate to the maximum availability required
      //
      StopDate := min(StopDate, StartDate+Trunc((Data.Duration-Allocated)/TotalUnits));

      Allocated := Trunc(Allocated+(StopDate-StartDate)*TotalUnits);

      {
      // debug code
      if StopDate-StartDate<ONE_SECOND then
        StopDate := StopDate;
      }

      StoreInterval(StartDate, StopDate);
      if Allocated+ONE_SECOND >= Data.Duration then
        include(Data.ScheduleResults, srScheduledOK) else
        // Continue scheduling from the end of the allocated interval
        StartDate:=StopDate;
    end;


    {
      KV: 8 april 2005
      Extra check added:
      Stop scheduling when we run outside schedulewindow.
      Use start for checking, since only this date is updated in loop.
    }
    if not (srScheduledOK in Data.ScheduleResults) and (StartDate>=DataStopDate) then
      include(Data.ScheduleResults, srScheduledFailed);

  until Data.ScheduleResults<>[];

  // KV: 8 april 2005
  // Check whether task has been scheduled inside schedule window.
  // A task can be scheduled successfull inside the absolute project window,
  // but outside the schedule window.
  if (srScheduledOK in Data.ScheduleResults) and (Intervals[Intervals.Count-1].Stop > Data.Stop) then
    include(Data.ScheduleResults, srScheduledOutsideWindow);
end;

procedure TEzAvailabilityProfile.ScheduleTask(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray);
var
  ts: TEzTimespan;

begin
  case Data.Direction of
    sdForwards:
      if Data.Duration = 0 then
      begin
        ts.Start := Data.Start;
        ts.Stop := Data.Start;
        Intervals.Add(ts);
        Include(Data.ScheduleResults, srScheduledOK);
      end
      else
      begin
      	ValidateDate(Data.Start);
        ScheduleForward(Data, Intervals);
      end;

    sdBackwards:
      if Data.Duration = 0 then
      begin
        ts.Start := Data.Stop;
        ts.Stop := Data.Stop;
        Intervals.Add(ts);
        include(Data.ScheduleResults, srScheduledOK);
      end
      else
      begin
        ts.Start := Data.Stop-FPrepareBlockSize;
        ts.Stop := Data.Stop;
      	Preparedaterange(ts);
        ScheduleBackward(Data, Intervals);
      end;
  else
    EzAvailabilityError(SInvalidScheduleDirection);
  end;
end;

procedure TEzAvailabilityProfile.SetDefaultAllocation(Value: Double);
begin
  if FDefaultAllocation<>Value then
  begin
    FDefaultAllocation := Value;
    Clear;
  end;
end;

procedure TEzAvailabilityProfile.SetItem(Index: Integer; Value: TEzAvailabilityPoint);
begin
  inherited Items[Index] := Value;
end;

procedure TEzAvailabilityProfile.SetOperator(Value: TEzAvailabilityOperator);
begin
  if Value<>FOperator then
  begin
    FOperator := Value;
    Clear;
  end;
end;

procedure TEzAvailabilityProfile.SetOwnsSources(Value: Boolean);
begin
  FSources.OwnsObjects := Value;
end;

procedure TEzAvailabilityProfile.SetInterval(Start, Stop: TDateTime; Units: Double; Flags: TAvailabilityFlags);
begin
  SetInterval(DatetimeToEzDatetime(Start), DatetimeToEzDatetime(Stop), Units, Flags);
end;

procedure TEzAvailabilityProfile.SetInterval(Start, Stop: TEzDateTime; Units: Double; Flags: TAvailabilityFlags);
var
  iStart, iStop, i: Integer;
  Find, P: TEzAvailabilityPoint;

begin
  if Start >= Stop then
    EzAvailabilityError(SInvalidDates);

  Find := TEzAvailabilityPoint.Create;
  try
    //
    // Handle Stop point
    //
    Find.DateValue := Stop;
    if not IndexOf(iStop, Find) then
      //
      // Insert a point at stop when it does not exist
      //
    begin
      if Stop <> END_OF_TIME then
      begin
        if iStop > 0 then
          P := Items[iStop-1] else
          P := nil;

        if (P=nil) or (P.Units <> Units) or (P.Flags <> Flags) then
          Insert(iStop, TEzAvailabilityPoint.Create(Stop, P.Units, P.Flags));
      end;
    end
    else
    begin
      P := Items[iStop];
      if (P.Units = Units) and (P.Flags = Flags) then
        //
        // Increase index so that point will be removed
        //
        inc(iStop);
    end;

    //
    // Handle Start point
    //
    Find.DateValue := Start;
    if not IndexOf(iStart, Find) then
    begin
      if iStart > 0 then
        P := Items[iStart-1] else
        P := Items[iStart];

      if (P.Units <> Units) or (P.Flags <> Flags) then
      begin
        Insert(iStart, TEzAvailabilityPoint.Create(Start, Units, Flags));
        inc(iStop);
      end else
        dec(iStart);
    end

    else
    begin
      // Startpoint does exist; check whether it can be removed savely
      // Make sure first point is not erased
      P := Items[max(iStart-1, 0)];
      if (iStart>0) and (P.Units = Units) and (P.Flags = Flags) then
        //
        // Decrease index so that point will be removed
        //
        dec(iStart)
      else
        //
        // Update existing point with new data
        //
      begin
        Items[iStart] := TEzAvailabilityPoint.Create(Start, Units, Flags);
      end;
    end;

    // Remove all points between iStart and iStop
  //  for i:=iStop-iStart-2 downto 0 do
    for i:=iStop-1 downto iStart+1 do
      Delete(i);
  finally
    Find.Destroy;
  end;
end;

procedure TEzAvailabilityProfile.SetRules(Value: TEzCalendarRules);
begin
  FDefaultAllocation := 1.0;
  Clear;
  if FCalendarRules<>nil then
    FCalendarRules.DeleteLink(FRuleLink);
  FCalendarRules := Value;
  FCalendarRules.AddLink(FRuleLink);
end;

function TEzAvailabilityProfile.SumInterval(Start, Stop: TDateTime; Delta: Double; AllowNegative: Boolean): Boolean;
begin
  Result := SumInterval(DatetimeToEzDatetime(Start), DatetimeToEzDatetime(Stop), Delta, AllowNegative);
end;

function TEzAvailabilityProfile.SumInterval(Start, Stop: TEzDateTime; Delta: Double; AllowNegative: Boolean): Boolean;
begin
  Result := InternalSumInterval(Start, Stop, Delta, AllowNegative, False);
end;

function TEzAvailabilityProfile.ValidateDate(Value: TEzDateTime): Boolean;
var
  ts: TEzTimespan;

begin
  Result := False;
  if not RequiresPrepare then Exit; // Nothing to do

	if FPreparedRange.Start = Unprepared then
	begin
    ts.Start := Value;
    ts.Stop := Value+FPrepareBlockSize;
		PrepareDateRange(ts);
		Result := true;
	end
	else if Value < FPreparedRange.Start then
	begin
    ts.Start := min(Value, FPreparedRange.Start-FPrepareBlockSize);
    ts.Stop := FPreparedRange.Start;
		PrepareDateRange(ts);
		Result := true;
	end
	else if Value >= FPreparedRange.Stop then
	begin
    ts.Start := FPreparedRange.Stop;
    ts.Stop := max(Value, FPreparedRange.Stop+FPrepareBlockSize);
		PrepareDateRange(ts);
		Result := true;
	end;
end;

{
function TEzAvailabilityProfile.ValidateSchedule(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray): TEzScheduleResults;
var
  i_int, i_point: Integer;
  Interval, prep: TEzTimeSpan;
  point, nextpoint: TEzAvailabilityPoint;

begin
  Result := [];
  if Intervals.Count = 0 then Exit;

  i_int:=0;
  Interval := Intervals[0];

  prep.Start := Interval.Start;
  prep.Stop := Intervals[Intervals.Count-1].Stop;
	PrepareDateRange(prep);

  if not IndexOf(i_point, Interval.Start) and (i_point > 0) then
    dec(i_point);
  point := Items[i_point];

  if i_point < Count-1 then
    nextpoint := Items[i_point+1] else
    nextpoint := TEzAvailabilityPoint.Create(END_OF_TIME, 0, []);

  while (i_int<Intervals.Count) and (Result = []) do
  try
    if point.Units < Data.UnitsRequired then
      //
      // Inaddequate availability, schedule fails for this resource,
      // try to reschedule starting with this resource.
      //
    begin
      include(Result, srValidateReschedule);
      if Data.Direction = sdForwards then
        Data.Start := nextpoint.DateValue else
        Data.Start := point.DateValue;
    end

    else
        //
        // if this resource may not switch between tasks, then intervals must match
        // exactly.
        //
    if  not Data.ResourceAllowsSwithing and (
        //
        // For any succeeding interval, start date must match intervals start
        //
        ((i_int>0) and (point.DateValue <> Interval.Start))
        or
        //
        // if we have more intervals to check, then enddate must match
        // intervals end date.
        //
        ((i_int<Intervals.Count-1) and (nextpoint.DateValue <> Interval.Stop)))

    then
      include(Result, srValidateReschedule);

    if Result = [] then
    begin
      if Nextpoint.Datevalue > Interval.Stop then
      begin
        // we are done
        include(Result, srScheduledOK);
        Exit;
      end;

      if Nextpoint.Datevalue <= Interval.Stop then
      begin
        inc(i_int);
        if i_int < Intervals.Count then
          Interval := Intervals[i_int]
        else
        begin
          // we are done
          include(Result, srScheduledOK);
          Exit;
        end;
      end;

      if Nextpoint.DateValue < Interval.Start then
      begin
        inc(i_point);
        point := nextpoint;
        while (i_point<Count-1) and (point.Units <= 0) and (point.DateValue<Interval.Start) do
        begin
          inc(i_point);
          point := Items[i_point];
          nextpoint := Items[i_point+1];
        end;

        if point.DateValue>Interval.Start then
          //
          // The next interval for this resource lies after the scheduled
          // interval.
          //
        begin
          if Data.AllResourcesAllowSwithing then
            //
            // If the profile allows task switching, we will reschedule starting
            // with this resource and then verify the result against the other
            // resources.
            //
          begin
            include(Result, srValidateReschedule);
            if Data.Direction = sdForwards then
              Data.Start := Intervals[0].Start else
              Data.Start := Intervals[0].Stop;
          end
          else
            //
            // We are still going to reschedule, however now we start at
            // current date.
            //
          begin
            include(Result, srValidateReschedule);
            Data.Start := point.DateValue;
          end;
        end
        else if (point.DateValue<Interval.Start) and not Data.ResourceAllowsSwithing then
          //
          // The next interval for this resource lies before the (next) scheduled
          // interval. Tell the scheduler to reschedule this task starting
          // at next interval start.
          //
        begin
          include(Result, srValidateResourceMayNotSwitch);
          Data.Start := Interval.Start;
        end;
      end;
    end;
  finally
    if nextpoint.DateValue = END_OF_TIME then
      nextpoint.Destroy;
  end;
end;
}
{
function TEzAvailabilityProfile.ValidateSchedule(var Data: TEzCalendarScheduleData; Intervals: TEzTimespanArray): TEzScheduleResults;
var
  TempProfile: TEzAvailabilityProfile;
  Interval: TEzTimeSpan;
  i_int, i_point: Integer;
  point: TEzAvailabilityPoint;

begin
  Interval.Start := Intervals[0].Start;
  Interval.Stop := Intervals[Intervals.Count-1].Stop;
  PrepareDateRange(Interval);

  if Data.Direction = sdForwards then
  begin
    IndexOf(i_point, Interval.Stop);
    if (i_point>0) then
      dec(i_point);

    point := Items[i_point];

    while (i_point>=0) and (point.DateValue>=Interval.Start) do
    begin
      if not Data.ResourceAllowsSwithing  and (afIsAllocated in point.Flags) then
      begin
        Assert(i_point<Count-1);
        Data.Start := Items[i_point+1].DateValue;
        Result := [srValidateResourceMayNotSwitch];
        Exit;
      end;

      if afNoScheduleAcross in point.Flags then
      begin
        Assert(i_point<Count-1);
        Data.Start := Items[i_point+1].DateValue;
        Result := [srValidateResourceMayNotSwitch];
        Exit;
      end;
      dec(i_point);
      if i_point>=0 then
        point := Items[i_point];
    end;
  end
  else
  begin
    if not IndexOf(i_point, Interval.Start) and (i_point>0) then
      dec(i_point);

    point := Items[i_point];

    while (i_point<Count) and (point.DateValue<Interval.Stop) do
    begin
      if not Data.ResourceAllowsSwithing  and (afIsAllocated in point.Flags) then
      begin
        Data.Start := point.DateValue;
        Result := [srValidateResourceMayNotSwitch];
        Exit;
      end;

      if afNoScheduleAcross in point.Flags then
      begin
        Data.Start := point.DateValue;
        Result := [srValidateResourceMayNotSwitch];
        Exit;
      end;
      inc(i_point);
      if i_point<Count then
        point := Items[i_point];
    end;
  end;

  TempProfile := TEzAvailabilityProfile.Create(0);
  try
    TempProfile.AddSource(Self, False);

    if Data.Direction = sdForwards then
    begin
      for i_int:=Intervals.Count-1 downto 0 do
      begin
        Interval := Intervals[i_int];
        if TempProfile.AllocateInterval(Interval.Start, Interval.Stop, Data.UnitsRequired, True) then
        begin
          Data.Start := Interval.Stop;
          Result := [srValidateReschedule];
          Exit;
        end;
      end;
    end
    else // Data.Direction = sdBackwards
    begin
      for i_int:=0 to Intervals.Count-1 do
      begin
        Interval := Intervals[i_int];
        if TempProfile.AllocateInterval(Interval.Start, Interval.Stop, Data.UnitsRequired, True) then
        begin
          Data.Start := Interval.Start;
          Result := [srValidateReschedule];
          Exit;
        end;
      end;
    end;
    Result := [srScheduledOK];
  finally
    TempProfile.Destroy;
  end;
end;
}
//= End of TEzAvailabilityProfile --------------------------------------------=

//=----------------------------------------------------------------------------=

constructor TEzCalendarRule.Create;
begin
  inherited;
  RangeStart := 0;
  RangeEnd := 0;
  Count := 1;
  Occurrences := 0;
end;

//= End of TEzCalendarPanelDataLink ---------------------------------------------------=

initialization
begin
  with CalendarLocalization do
  begin
    WorkinghourText := SWorkingHourText;
    WorkingText := SWorkingText;
    NonWorkingText := SNonWorkingText;
  end;
end;

end.
