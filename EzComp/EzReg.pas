unit EzReg;

{$I Ez.inc}

interface

uses SysUtils, Classes, Graphics, Windows, Dialogs

{$IFDEF EZ_D2010}
  , EzDateTime, ToolsApi
{$ENDIF}

{$IFNDEF EZ_D6}
  , DsgnIntf;
{$ENDIF}

{$IFDEF EZ_D6}
  , DesignEditors, DesignIntf, VCLEditors;
{$ENDIF}

type
   TSelectiveComponentProperty = class(TComponentProperty)
   public
       procedure Edit; override;
   end;

// Copy from DBReg.pas
  TDBStringProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValueList(List: TStrings); virtual;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TEzDatalinkFieldProperty = class(TDBStringProperty)
  public
    procedure GetValueList(List: TStrings); override;
  end;

  TEzSchedulerFieldProperty = class(TDBStringProperty)
  public
    procedure GetValueList(List: TStrings); override;
  end;

  TGanttBarFieldProperty = class(TDBStringProperty)
    procedure GetValueList(List: TStrings); override;
  end;

  TGanttChartFieldProperty = class(TDBStringProperty)
  public
    procedure GetValueList(List: TStrings); override;
  end;

  TEzGanttLineMarkersLinesProperty = class(TClassProperty)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

  TEzTreeColumnSchemesProperty = class(TClassProperty)
  public
    procedure Edit; override;
    function  GetAttributes: TPropertyAttributes; override;
    function  GetValue: string; override;
  end;

{$IFDEF EZ_D2010}
  TEzPlanIT_Visualizer = class(
    TInterfacedObject,
    IOTADebuggerVisualizer,
    IOTADebuggerVisualizerValueReplacer,
    IOTAThreadNotifier)

  protected
    FExpression: string;
    FNotifierIndex: Integer;
    FCompleted: Boolean;
    FDeferredResult: string;
    FDeferredError: Boolean;

    function Evaluate(const Expression: string) : string;

    // IOTAThreadNotifier
    procedure AfterSave;
    procedure BeforeSave;
    procedure Destroyed;
    procedure Modified;
    procedure ThreadNotify(Reason: TOTANotifyReason);
    { IOTAThreadNotifier }
    procedure EvaluateComplete(const ExprStr: string; const ResultStr: string;
      CanModify: Boolean; ResultAddress: Cardinal; ResultSize: Cardinal;
      ReturnCode: Integer); overload;

    procedure EvaluteComplete(const ExprStr, ResultStr: string; CanModify: Boolean;
      ResultAddress, ResultSize: LongWord; ReturnCode: Integer); overload;
    procedure ModifyComplete(const ExprStr, ResultStr: string; ReturnCode: Integer);

  public
    function GetReplacementValue(
      const Expression: string;
      const TypeName: string;
      const EvalResult: string): string;
    procedure GetSupportedType(
      Index: Integer;
      var TypeName: string;
      var AllDescendents: Boolean);
    function GetSupportedTypeCount: Integer;
    function GetVisualizerDescription: string;
    function GetVisualizerIdentifier: string;
    function GetVisualizerName: string;
  end;

{$ENDIF}


  procedure Register;

{$IFDEF EZ_D2010}
var
  _EzPlanIT_Visualizer: IOTADebuggerVisualizer;
{$ENDIF}

implementation

{$R EzComp.RES}

uses  Db, DBGrids, Forms, Controls, TypInfo,
      EzDBGrid, EzGantt, EzDataSet, EzGraph,
      EzScheduler, EzTimebar,
      EzCalendarControl, EzPrinter, Math, EzDsDsgn, EzPainting,
      DSDesign, DsnDbCst, ColnEdit, EzSplash, EzEpDsgn, ImgList, EzScmDsg;

{ Utility Functions }

function GetPropertyValue(Instance: TPersistent; const PropName: string): TPersistent;
var
  PropInfo: PPropInfo;
begin
  Result := nil;
  PropInfo := TypInfo.GetPropInfo(Instance.ClassInfo, PropName);
  if (PropInfo <> nil) and (PropInfo^.PropType^.Kind = tkClass) then
    Result := TObject(GetOrdProp(Instance, PropInfo)) as TPersistent;
end;

procedure TSelectiveComponentProperty.Edit;
begin
  if GetOrdValue = 0 then
    inherited Edit
  else
    { does not change focus if component is on another form }
    Designer.SelectComponent(TComponent(GetOrdValue));
end;

{ TDBStringProperty }

function TDBStringProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paMultiSelect];
end;

procedure TDBStringProperty.GetValueList(List: TStrings);
begin
end;

procedure TDBStringProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
  Values: TStringList;
begin
  Values := TStringList.Create;
  try
    GetValueList(Values);
    for I := 0 to Values.Count - 1 do Proc(Values[I]);
  finally
    Values.Free;
  end;
end;

{ TProgressbarProperty: EzGanttbar.ProgressBar }

type
  TProgressbarProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  function TProgressbarProperty.GetAttributes: TPropertyAttributes;
  begin
    Result := [paValueList, paSortList, paRevertable];
  end;

  procedure TProgressbarProperty.GetValues(Proc: TGetStrProc);
  var
    i: Integer;
    bar: TEzGanttbar;

  begin
    bar := GetComponent(0) as TEzGanttbar;

    with bar.Collection as TEzGanttBars do
      for i:=0 to Count-1 do
        if (Items[i] <> bar) then
          Proc(Items[i].Name);
  end;

{ TEzProgresslineAttachToBarProperty: TEzProgresslineSettings.AttachToBar }

type
  TEzProgresslineAttachToBarProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  function TEzProgresslineAttachToBarProperty.GetAttributes: TPropertyAttributes;
  begin
    Result := [paValueList, paSortList, paRevertable];
  end;

  procedure TEzProgresslineAttachToBarProperty.GetValues(Proc: TGetStrProc);
  var
    i: Integer;
    Gantt: TCustomEzGanttChart;

  begin
    Gantt := (GetComponent(0) as TEzProgresslineSettings).GanttChart;
    with Gantt.GanttBars do
      for i:=0 to Count-1 do
        Proc(Items[i].DisplayName);
  end;

{ TEzGanttBarDatalinkNameProperty: EzGanttbar.Visibility.EzDatalinkName }
type
  TEzGanttBarDatalinkNameProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  function TEzGanttBarDatalinkNameProperty.GetAttributes: TPropertyAttributes;
  begin
    Result := [paValueList];
  end;

  procedure TEzGanttBarDatalinkNameProperty.GetValues(Proc: TGetStrProc);
  var
    i: Integer;
    GB: TEzGanttbar;
    ds: TDataset;

  begin
    GB := (GetComponent(0) as TEzGanttBarVisibility).GanttBar;
    if not Assigned(GB) or
       not Assigned(GB.Collection) or
       not Assigned((GB.Collection as TEzGanttBars).GanttChart) or
       not Assigned((GB.Collection as TEzGanttBars).GanttChart.DataSource) or
       not Assigned((GB.Collection as TEzGanttBars).GanttChart.DataSource.Dataset)
    then
      Exit;

    ds := (GB.Collection as TEzGanttBars).GanttChart.DataSource.Dataset;
    with ds as TCustomEzDataset do
      if not SelfReferencing then
        for i:=0 to EzDatalinks.Count-1 do
          Proc(EzDatalinks[i].DisplayName);
  end;

procedure TEzGanttLineMarkersLinesProperty.Edit;
begin
  ShowCollectionEditorClass(Designer, TCollectionEditor,
      (GetComponent(0) as TEzGanttLineMarkersHelper).Owner, TEzGanttLineMarkers(GetOrdValue), 'MyCollection');
end;

function TEzGanttLineMarkersLinesProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paReadOnly, paDialog];
end;

procedure TEzTreeColumnSchemesProperty.Edit;
begin
  ShowEzSchemeDesigner(Designer, GetComponent(0) as TEzTreeGridColumn);
end;

function TEzTreeColumnSchemesProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paReadOnly, paDialog];
end;

function TEzTreeColumnSchemesProperty.GetValue: string;
var
  Col: TEzTreeGridColumn;
  i: Integer;

begin
  Col := GetComponent(0) as TEzTreeGridColumn;
  Result := '(';
  for i:=0 to Col.Schemes.Count-1 do
    if Result = '(' then
      Result := Result + IntToStr(Integer(Col.Schemes[i])) else
      Result := Result + ',' + IntToStr(Integer(Col.Schemes[i]));
  Result := Result + ')';
end;

{ TDataFieldProperty }

procedure TEzDatalinkFieldProperty.GetValueList(List: TStrings);
begin
  if (GetComponent(0) as TEzDatalink).DetailDataset <> nil then
    (GetComponent(0) as TEzDatalink).DetailDataset.GetFieldNames(List);
end;

procedure TEzSchedulerFieldProperty.GetValueList(List: TStrings);
var
  DataSource: TDataSource;
begin
  DataSource := GetPropertyValue(GetComponent(0), 'DataSource') as TDataSource;
  if (DataSource <> nil) and (DataSource.DataSet <> nil) then
    DataSource.DataSet.GetFieldNames(List);
end;

procedure TGanttBarFieldProperty.GetValueList(List: TStrings);
var
  Gantt: TCustomEzGanttChart;
  DataSource: TDataSource;
begin
  Gantt := (GetComponent(0) as EzGantt.TEzGanttBar).GanttChart;
  if (Gantt = nil) then Exit;
  DataSource := GetPropertyValue(Gantt, 'DataSource') as TDataSource;
  if (DataSource <> nil) and (DataSource.DataSet <> nil) then
    DataSource.DataSet.GetFieldNames(List);
end;

procedure TGanttChartFieldProperty.GetValueList(List: TStrings);
var
  Gantt: TCustomEzGanttChart;
  DataSource: TDataSource;
begin
  if GetComponent(0) is TPredecessorLine then
    Gantt := (GetComponent(0) as TPredecessorLine).GanttChart
  else if GetComponent(0) is TEzProgresslineSettings then
    Gantt := (GetComponent(0) as TEzProgresslineSettings).GanttChart
  else
    Exit;

  DataSource := GetPropertyValue(Gantt, 'DataSource') as TDataSource;
  if (DataSource <> nil) and (DataSource.DataSet <> nil) then
    DataSource.DataSet.GetFieldNames(List);
end;

//=--------------------------------------------------------------=
//=--------------------------------------------------------------=
//=--------------------------------------------------------------=


{Helper functions for handling endpoint editors }
  procedure GetValues(List: TEzEndPointList; Proc: TGetStrProc);
  var
    i: integer;

  begin
{
    if Assigned(List) and Assigned(List.Images) then
      for i:=-1 to List.Images.Count-1 do
        Proc(IntToStr(i));
}
    if Assigned(List) then
      for i:=-1 to List.Points.Count-1 do
        Proc(IntToStr(i));
  end;

  procedure ListMeasureHeight(List: TEzEndPointList; const Value: string; ACanvas: TCanvas; var AHeight: Integer);
  begin
    if Assigned(List) and Assigned(List.Images) then
      AHeight := List.Images.Height + 4 else
      AHeight := 15;
  end;

  procedure ListMeasureWidth(List: TEzEndPointList; const Value: string; ACanvas: TCanvas; var AWidth: Integer);
  begin
    if Assigned(List) and Assigned(List.Images) then
        AWidth := List.Images.Width + 4;

    inc(AWidth, ACanvas.TextWidth('EndPoint ' + Value));
  end;

  procedure ListDrawValue(List: TEzEndPointList; const Value: string; ACanvas: TCanvas; const ARect: TRect; ASelected: Boolean);
  var
    vOldPenColor: TColor;
    Rect: TRect;
    H: Integer;

  begin
    with ACanvas do
    begin
      Rect := ARect;

      // save off things
      vOldPenColor := Pen.Color;

      // frame things
      Pen.Color := Brush.Color;
      Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);

      if Value <> '-1' then
      begin
        H := List.Images.Height;
        List.Images.Draw(
              ACanvas,
              Rect.Left+2, Rect.Top+((Rect.Bottom - Rect.Top) div 2) - (H div 2),
              List.Points[StrToInt(Value)].ImageIndex
          );

        inc(Rect.Left, List.Images.Width+4);
        H := TextHeight('E');
        TextRect(Rect, Rect.Left, Rect.Top+((Rect.Bottom - Rect.Top) div 2) - (H div 2), 'Endpoint ' + Value);
      end
      else
      begin
        H := TextHeight('E');
        TextRect(Rect, Rect.Left, Rect.Top+((Rect.Bottom - Rect.Top) div 2) - (H div 2), '<none>');
      end;

      Pen.Color := vOldPenColor;
    end;
  end;

{$IFNDEF EZ_VD4}
{$IFNDEF EZ_VCB4}

  //
  // EndPoint property editor
  //
type
{$IFDEF EZ_VD5} {Delphi 5 only}
  TEndPointImageIndexProperty = class(TIntegerProperty)
{$ELSE} {Delphi 6 and up only}
  TEndPointImageIndexProperty = class(TEnumProperty, ICustomPropertyListDrawing)
{$ENDIF}
  public
    function  GetEndPointList: TEzEndPointList;
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure ListMeasureHeight(const Value: string; ACanvas: TCanvas; var AHeight: Integer); {$IFDEF EZ_VD5}override;{$ENDIF}
    procedure ListMeasureWidth(const Value: string; ACanvas: TCanvas; var AWidth: Integer); {$IFDEF EZ_VD5}override;{$ENDIF}
    procedure ListDrawValue(const Value: string; ACanvas: TCanvas; const ARect: TRect; ASelected: Boolean); {$IFDEF EZ_VD5}override;{$ENDIF}
{$IFDEF EZ_VD5} {Delphi 5 and up only}
    procedure PropDrawValue(ACanvas: TCanvas; const ARect: TRect; ASelected: Boolean); override;
{$ENDIF}
  end;

  function  TEndPointImageIndexProperty.GetEndPointList: TEzEndPointList;
  begin
    if GetComponent(0) is TGanttBarEndPoint then
      Result := (GetComponent(0) as TGanttBarEndPoint).GanttBar.GanttChart.EndPoints

    else if GetComponent(0) is TPredecessorLine then
      Result := (GetComponent(0) as TPredecessorLine).GanttChart.EndPoints

    else
      Result := nil;
  end;

  procedure TEndPointImageIndexProperty.GetValues(Proc: TGetStrProc);
  begin
    EzReg.GetValues(GetEndPointList, Proc);
  end;

  function TEndPointImageIndexProperty.GetAttributes: TPropertyAttributes;
  begin
    Result := [paValueList];
  end;

  procedure TEndPointImageIndexProperty.ListMeasureHeight(const Value: string;
    ACanvas: TCanvas; var AHeight: Integer);
  begin
    EzReg.ListMeasureHeight(GetEndPointList, Value, ACanvas, AHeight);
  end;

  procedure TEndPointImageIndexProperty.ListMeasureWidth(const Value: string;
    ACanvas: TCanvas; var AWidth: Integer);
  begin
    EzReg.ListMeasureWidth(GetEndPointList, Value, ACanvas, AWidth);
  end;

{$IFDEF EZ_VD5}
  procedure TEndPointImageIndexProperty.PropDrawValue(ACanvas: TCanvas; const ARect: TRect;
      ASelected: Boolean);
  begin
    if GetVisualValue <> '-1' then
      EzReg.ListDrawValue(GetEndPointList, GetVisualValue, ACanvas, ARect, ASelected)
    else
      inherited PropDrawValue(ACanvas, ARect, ASelected);
  end;
{$ENDIF}

  procedure TEndPointImageIndexProperty.ListDrawValue(const Value: string; ACanvas: TCanvas; const ARect: TRect; ASelected: Boolean);
  begin
    EzReg.ListDrawValue(GetEndPointList, Value, ACanvas, ARect, ASelected);
  end;

  //
  // Image index property editor
  //
type
{$IFDEF EZ_VD5} {Delphi 5 only}
  TImageIndexProperty = class(TIntegerProperty)
{$ELSE} {Delphi 6 only}
  TImageIndexProperty = class(TEnumProperty, ICustomPropertyListDrawing)
{$ENDIF}
  public
    function  GetImageList: TCustomImageList;
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure ListMeasureHeight(const Value: string; ACanvas: TCanvas; var AHeight: Integer); {$IFDEF EZ_VD5}override;{$ENDIF}
    procedure ListMeasureWidth(const Value: string; ACanvas: TCanvas; var AWidth: Integer); {$IFDEF EZ_VD5}override;{$ENDIF}
    procedure ListDrawValue(const Value: string; ACanvas: TCanvas; const ARect: TRect; ASelected: Boolean); {$IFDEF EZ_VD5}override;{$ENDIF}
{$IFDEF EZ_VD5} {Delphi 5 only}
    procedure PropDrawValue(ACanvas: TCanvas; const ARect: TRect; ASelected: Boolean); override;
{$ENDIF}
  end;

  function TImageIndexProperty.GetImageList: TCustomImageList;
  begin
    if GetComponent(0) is TEzEndPoint then
      Result := ((GetComponent(0) as TEzEndPoint).Collection as TEzEndPoints).EndPointList.Images
    else if GetComponent(0) is TEzExpandButtons then
      Result := ((((GetComponent(0) as TEzExpandButtons).Scheme).Collection as TEzSchemes).SchemeCollection as TEzTreeGridSchemes).ButtonImages
    else
      Result := nil;
  end;

  procedure TImageIndexProperty.GetValues(Proc: TGetStrProc);
  var
    ImageList: TCustomImageList;
    i: Integer;

  begin
    ImageList := GetImageList;
    if Assigned(ImageList) then
      for i:=-1 to ImageList.Count-1 do
        Proc(IntToStr(i));
  end;

  function TImageIndexProperty.GetAttributes: TPropertyAttributes;
  begin
    Result := [paValueList];
  end;

  procedure TImageIndexProperty.ListMeasureHeight(const Value: string;
    ACanvas: TCanvas; var AHeight: Integer);
  var
    ImageList: TCustomImageList;
  begin
    ImageList := GetImageList;
    if Assigned(ImageList) then
      AHeight := ImageList.Height + 4 else
      AHeight := 15;
  end;

  procedure TImageIndexProperty.ListMeasureWidth(const Value: string;
    ACanvas: TCanvas; var AWidth: Integer);
  var
    ImageList: TCustomImageList;
  begin
    ImageList := GetImageList;
    if Assigned(ImageList) then
      AWidth := ImageList.Width + 4;
    inc(AWidth, ACanvas.TextWidth('Image ' + Value));
  end;

{$IFDEF EZ_VD5}
  procedure TImageIndexProperty.PropDrawValue(ACanvas: TCanvas; const ARect: TRect;
      ASelected: Boolean);
  begin
    if GetVisualValue <> '-1' then
      ListDrawValue(GetVisualValue, ACanvas, ARect, ASelected)
    else
      inherited PropDrawValue(ACanvas, ARect, ASelected);
  end;
{$ENDIF}

  procedure TImageIndexProperty.ListDrawValue(const Value: string; ACanvas: TCanvas; const ARect: TRect; ASelected: Boolean);
  var
    vOldPenColor: TColor;
    Rect: TRect;
    H: Integer;
    ImageList: TCustomImageList;

  begin
    with ACanvas do
    begin
      Rect := ARect;

      // save off things
      vOldPenColor := Pen.Color;

      // frame things
      Pen.Color := Brush.Color;
      Rectangle(Rect.Left, Rect.Top, Rect.Right, Rect.Bottom);

      ImageList := GetImageList;
      if Assigned(ImageList) then
      begin
        if Value <> '-1' then
        begin
          H := ImageList.Height;
          ImageList.Draw(ACanvas, Rect.Left+2, Rect.Top+((Rect.Bottom - Rect.Top) div 2) - (H div 2), StrToInt(Value));
          inc(Rect.Left, ImageList.Width+4);
          H := TextHeight('E');
          TextRect(Rect, Rect.Left, Rect.Top+((Rect.Bottom - Rect.Top) div 2) - (H div 2), 'Image ' + Value);
        end
        else
        begin
          H := TextHeight('E');
          TextRect(Rect, Rect.Left, Rect.Top+((Rect.Bottom - Rect.Top) div 2) - (H div 2), '<none>');
        end;
      end;

      Pen.Color := vOldPenColor;
    end;
  end;

{$ENDIF}
{$ENDIF}

type
  TEzDataSetEditor = class(TComponentEditor)
  protected
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

  procedure TEzDataSetEditor.ExecuteVerb(Index: Integer);
  begin
    case index of
      0: ShowEzDatasetDesigner(Designer, TEzDataset(Component));
{$IFDEF EZ_D5}
      1: ShowFieldsEditor(Designer, TDataSet(Component), TDSDesigner);
{$ELSE}
      1: ShowDatasetDesigner(Designer, TDataSet(Component));
{$ENDIF}
      2: ShowAboutDialog;
    end;
  end;

  function TEzDataSetEditor.GetVerb(Index: Integer): string;
  begin
    case Index of
      0: Result := 'EzDataset designer...';
      1: Result := 'Fields editor...';
      2: Result := 'About...';
    end;
  end;

  function TEzDataSetEditor.GetVerbCount: Integer;
  begin
    Result := 3;
  end;

type
  TEzEndPointListEditor = class(TComponentEditor)
  protected
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

  procedure TEzEndPointListEditor.ExecuteVerb(Index: Integer);
  begin
    ShowEzEndPointDesigner(Designer, TEzEndPointList(Component));
  end;

  function TEzEndPointListEditor.GetVerb(Index: Integer): string;
  begin
    Result := 'Endpoint designer ...';
  end;

  function TEzEndPointListEditor.GetVerbCount: Integer;
  begin
    Result := 1;
  end;

type
  //
  // Editor for Scheme property of TEzCoolGridColumn class
  //
  TEzCoolColumnSchemesProperty = class(TIntegerProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

  function TEzCoolColumnSchemesProperty.GetAttributes: TPropertyAttributes;
  begin
    Result := [paValueList];
  end;

  procedure TEzCoolColumnSchemesProperty.GetValues(Proc: TGetStrProc);
  var
    Col: TEzCoolGridColumn;
    i: Integer;

  begin
    Col := GetComponent(0) as TEzCoolGridColumn;

    Proc('-1 - no scheme');

    if Assigned(Col.Collection) then
      with Col.Collection as TDBGridColumns do
        if Assigned(Grid) then
          with Grid as TCustomEzDBCoolGrid do
            if Assigned(SchemeCollection) then
              with SchemeCollection.Schemes do
                for i:=0 to Count-1 do
                  Proc(IntToStr(i) + ' - ' + Items[i].Name);
  end;

  procedure TEzCoolColumnSchemesProperty.SetValue(const Value: string);
  var
    Col: TEzCoolGridColumn;

  begin
    Col := GetComponent(0) as TEzCoolGridColumn;

    if Pos(' ', Value) <> 0 then
      Col.Scheme := StrToInt(Copy(Value, 0, Pos(' ', Value)-1)) else
      Col.Scheme := StrToInt(Value);
    Modified;
  end;

type
  //
  // Editor for Scheme property of TEzColumnTitle class
  //
  TEzColumnTitleSchemesProperty = class(TIntegerProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

  function TEzColumnTitleSchemesProperty.GetAttributes: TPropertyAttributes;
  begin
    Result := [paValueList];
  end;

  procedure TEzColumnTitleSchemesProperty.GetValues(Proc: TGetStrProc);
  var
    Title: TEzColumnTitle;
    i: Integer;

  begin
    Title := GetComponent(0) as TEzColumnTitle;

    Proc('-1 - no scheme');

    if Assigned(Title.Column) and Assigned(Title.Column.Collection) then
      with Title.Column.Collection as TDBGridColumns do
        if Assigned(Grid) then
          with Grid as TCustomEzDBCoolGrid do
            if Assigned(SchemeCollection) then
              with SchemeCollection.Schemes do
                for i:=0 to Count-1 do
                  Proc(IntToStr(i) + ' - ' + Items[i].Name);
  end;

  procedure TEzColumnTitleSchemesProperty.SetValue(const Value: string);
  var
    Title: TEzColumnTitle;

  begin
    Title := GetComponent(0) as TEzColumnTitle;

    if Pos(' ', Value) <> 0 then
      Title.Scheme := StrToInt(Copy(Value, 0, Pos(' ', Value)-1)) else
      Title.Scheme := StrToInt(Value);
    Modified;
  end;

procedure Register;
{$IFDEF EZ_D2010}
var
   LServices: IOTADebuggerServices;
{$ENDIF}

begin
  RegisterComponents
  (
    'EzPlan-IT', [TEzDataSet, TEzDBCoolGrid, TEzDBTreeGrid, TEzGanttChart,
                  TEzScheduler, TEzGraph, TEzCalendarPanel, TEzPrinter,
                  TEzPrintPreview, TEzEndPointList, TEzCoolGridSchemes,
                  TEzTreeGridSchemes]
  );

  // Selection editor....
  RegisterPropertyEditor(TypeInfo(TComponent), nil, '', TSelectiveComponentProperty);

  // TEzDataset field editors
  RegisterPropertyEditor(TypeInfo(string), TEzDatalink, 'KeyField', TEzDatalinkFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzDatalink, 'ParentRefField', TEzDatalinkFieldProperty);

  // TEzGanttBar field editors
  RegisterPropertyEditor(TypeInfo(string), TEzGanttBar, 'StartField', TGanttBarFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzGanttBar, 'StopField', TGanttBarFieldProperty);

  // TEzGanttChart field editors
  RegisterPropertyEditor(TypeInfo(string), TPredecessorLine, 'StartField', TGanttChartFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TPredecessorLine, 'EndField', TGanttChartFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzProgresslineSettings, 'ProgresslineField', TGanttChartFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzProgresslineSettings, 'AttachToBar', TEzProgresslineAttachToBarProperty);

  // TEzGanttChart  TEzGanttLineMarkers editor
  RegisterPropertyEditor(TypeInfo(TEzGanttLineMarkers), TEzGanttLineMarkersHelper, '', TEzGanttLineMarkersLinesProperty);


  // TEzScheduler field editors
  RegisterPropertyEditor(TypeInfo(string), TEzScheduler, 'StartField', TEzSchedulerFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzScheduler, 'StopField', TEzSchedulerFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzScheduler, 'DurationField', TEzSchedulerFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzScheduler, 'ConstraintField', TEzSchedulerFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzScheduler, 'ConstraintDateField', TEzSchedulerFieldProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzScheduler, 'PriorityField', TEzSchedulerFieldProperty);

  // TEzGanttbar.ProgressBar property
  RegisterPropertyEditor(TypeInfo(string), TEzGanttBar, 'ProgressBar', TProgressbarProperty);
  RegisterPropertyEditor(TypeInfo(string), TEzGanttBarVisibility, 'EzDatalinkName', TEzGanttBarDatalinkNameProperty);

  // TEzDBTreeGrid.TEzTreeGridColumn.Schemes editor
  RegisterPropertyEditor(TypeInfo(TSchemeList), nil, '', TEzTreeColumnSchemesProperty);

  // TEzDBCoolGrid.TEzCoolGridColumn.Scheme editor
  RegisterPropertyEditor(TypeInfo(TSchemeIndex), TEzCoolGridColumn, '', TEzCoolColumnSchemesProperty);

  // TEzColumnTitle.Scheme editor
  RegisterPropertyEditor(TypeInfo(TSchemeIndex), TEzColumnTitle, '', TEzColumnTitleSchemesProperty);

{$IFNDEF EZ_VD4}
{$IFNDEF EZ_VCB4}
  RegisterPropertyEditor(TypeInfo(TImageIndex), TEzEndPoint, '', TImageIndexProperty);
  RegisterPropertyEditor(TypeInfo(TImageIndex), TEzExpandButtons, '', TImageIndexProperty);
  RegisterPropertyEditor(TypeInfo(TEndPointIndex), TGanttBarEndPoint, '', TEndPointImageIndexProperty);
  RegisterPropertyEditor(TypeInfo(TEndPointIndex), TPredecessorLine, '', TEndPointImageIndexProperty);
{$ENDIF}
{$ENDIF}

  RegisterComponentEditor(TEzDataset, TEzDataSetEditor);
  RegisterComponentEditor(TEzEndPointList, TEzEndPointListEditor);

{$IFDEF EZ_D2010}
  if Supports(BorlandIDEServices, IOTADebuggerServices, LServices) then
  begin
    _EzPlanIT_Visualizer := TEzPlanIT_Visualizer.Create;
    LServices.RegisterDebugVisualizer(_EzPlanIT_Visualizer);
  end;
{$ENDIF}
end;

{$IFDEF EZ_D2010}
procedure UnRegisterVisualizer;
var
  LServices: IOTADebuggerServices;
begin
  if Supports(BorlandIDEServices, IOTADebuggerServices, LServices) then
  begin
    if _EzPlanIT_Visualizer <> nil then
    begin
      LServices.UnregisterDebugVisualizer(_EzPlanIT_Visualizer);
      _EzPlanIT_Visualizer := nil;
    end;
  end;
end;
{$ENDIF}

{$IFDEF EZ_D2010}
function TEzPlanIT_Visualizer.GetReplacementValue(
      const Expression: string;
      const TypeName: string;
      const EvalResult: string): string;

    // Expression = (40505432314884, 40505432314884)
    // Expression = (Start:40505432314884; Stop:40505432314884)
  function ParseTimespan : string;
  var
    i: Int64;
    p0: Integer;
    p1: Integer;
  begin
    p0 := 1;
    while not CharInSet(EvalResult[p0], ['0'..'9']) do
      inc(p0);

    p1 := p0;
    while CharInSet(EvalResult[p0], ['0'..'9']) do
      inc(p0);

    i := StrToInt64(Copy(EvalResult, p1, p0 - p1));
    Result := DateTimeToStr(EzDateTimeToDateTime(i));

    inc(p0);
    while not CharInSet(EvalResult[p0], ['0'..'9']) do
      inc(p0);
    p1 := p0;
    while CharInSet(EvalResult[p0], ['0'..'9']) do
      inc(p0);

    i := StrToInt64(Copy(EvalResult, p1, p0 - p1));
    Result := Result + ' - ' + DateTimeToStr(EzDateTimeToDateTime(i));
  end;

begin
  if (TypeName = 'TEzTimespan') then
    // Expression = (40505432314884, 40505432314884)
    // Expression = (Start:40505432314884; Stop:40505432314884)
    Result := ParseTimespan else
    Result := EvalResult;
end;

procedure TEzPlanIT_Visualizer.GetSupportedType(
  Index: Integer;
  var TypeName: string;
  var AllDescendents: Boolean);
begin
  AllDescendents := False;
  case Index of
    0: TypeName := 'TEzTimespan';
  end
end;

function TEzPlanIT_Visualizer.GetSupportedTypeCount: Integer;
begin
  Result := 1;
end;

function TEzPlanIT_Visualizer.GetVisualizerDescription: string;
begin
  Result := 'Displays the textual representation of EzPlan-IT date time types';
end;

function TEzPlanIT_Visualizer.GetVisualizerIdentifier: string;
begin
  Result := ClassName;
end;

function TEzPlanIT_Visualizer.GetVisualizerName: string;
begin
  Result := 'EzPlan-IT visualizer';
end;

procedure TEzPlanIT_Visualizer.AfterSave;
begin

end;

procedure TEzPlanIT_Visualizer.BeforeSave;
begin

end;

procedure TEzPlanIT_Visualizer.Destroyed;
begin

end;

procedure TEzPlanIT_Visualizer.Modified;
begin

end;

procedure TEzPlanIT_Visualizer.ModifyComplete(const ExprStr,
  ResultStr: string; ReturnCode: Integer);
begin

end;

procedure TEzPlanIT_Visualizer.ThreadNotify(Reason: TOTANotifyReason);
begin

end;

procedure TEzPlanIT_Visualizer.EvaluteComplete(const ExprStr,
  ResultStr: string; CanModify: Boolean; ResultAddress, ResultSize: LongWord;
  ReturnCode: Integer);
begin
  FCompleted := True;
  FDeferredResult := ResultStr;
  FDeferredError := ReturnCode <> 0;
end;

{ IOTAThreadNotifier }
procedure TEzPlanIT_Visualizer.EvaluateComplete(const ExprStr: string; const ResultStr: string;
  CanModify: Boolean; ResultAddress: Cardinal; ResultSize: Cardinal;
  ReturnCode: Integer);
begin
  FCompleted := True;
  FDeferredResult := ResultStr;
  FDeferredError := ReturnCode <> 0;
end;

function TEzPlanIT_Visualizer.Evaluate(const Expression: string) : string;
var
  CurProcess: IOTAProcess;
  CurThread: IOTAThread;
  ResultStr: array[0..4095] of Char;
  CanModify: Boolean;
  ResultAddr, ResultSize, ResultVal: LongWord;
  EvalRes: TOTAEvaluateResult;
  DebugSvcs: IOTADebuggerServices;
begin
  Result := '';
  if Supports(BorlandIDEServices, IOTADebuggerServices, DebugSvcs) then
    CurProcess := DebugSvcs.CurrentProcess;
  if CurProcess <> nil then
  begin
    CurThread := CurProcess.CurrentThread;
    if CurThread <> nil then
    begin
      EvalRes := CurThread.Evaluate(  Expression,
                                      @ResultStr,
                                      Length(ResultStr),
                                      CanModify,
                                      eseAll,
                                      '',
                                      ResultAddr,
                                      ResultSize,
                                      ResultVal,
                                      '',
                                      0);

      case EvalRes of
        erOK: Result := ResultStr;
        erDeferred:
          begin
            FCompleted := False;
            FDeferredResult := '';
            FDeferredError := False;
            FNotifierIndex := CurThread.AddNotifier(Self);
            while not FCompleted do
              DebugSvcs.ProcessDebugEvents;
            CurThread.RemoveNotifier(FNotifierIndex);
            FNotifierIndex := -1;
            if not FDeferredError then
            begin
              if FDeferredResult <> '' then
                Result := FDeferredResult
              else
                Result := ResultStr;
            end;
          end;
        erBusy:
          begin
            DebugSvcs.ProcessDebugEvents;
            Result := Evaluate(Expression);
          end;
      end;
    end;
  end;
end;

initialization
finalization
  UnRegisterVisualizer;
{$ENDIF}

end.

