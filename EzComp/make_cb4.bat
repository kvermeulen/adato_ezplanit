@echo off
set SAVEPATH=%PATH%
set PATH=D:\PROGRA~1\BORLAND\CBUILDER4\BIN;%PATH%
..\UpdateRcVersion EzPlanIT_CB4.rc %1

if %2 == DEMO goto :DEMO
if %2 == REG goto :REG
goto :End

:DEMO
make -B -fEzPlanIT_CB4_Demo.bpk
goto :END

:REG
make -B -fEzPlanIT_CB4.bpk
goto :End

:END
set PATH=%SAVEPATH%

