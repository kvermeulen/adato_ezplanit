unit EzDBGrid;

{$I Ez.inc}

interface

uses Windows, Graphics, Classes, stdctrls, db, EzDataset, sysutils,
     Messages, Controls, ActiveX, ImgList, EzPainting, DBCtrls, Contnrs,
     Grids, DBGrids {$IFDEF TNT_UNICODE}, TntDBGrids, TntDB{$ENDIF};

const
  PicklistFields: set of TFieldType = [ftSmallint, ftInteger, ftWord, ftBCD, ftFloat];

  // Timer id's
  tmrDragDropScroll = 3;
  CellHitMargin = 2;

type
  EEzGridError = class(Exception);

  TVerticalAlign = (tvaTop, tvaCenter, tvaBottom);
  TEzTextFlag = (tfWordBreak, tfEndEllipses, tfPathEllipses, tfHidePrefix, tfSingleLine, tfNoPrefix);
  TEzTextFlags = set of TEzTextFlag;

  TWMPrint = packed record
    Msg: Cardinal;
    DC: HDC;
    Flags: Cardinal;
    Result: Integer;
  end;

  TWMPrintClient = TWMPrint;

  // Forward declaration
  TCustomEzDBCoolGrid = class;
  TCustomEzDBTreeGrid = class;
  TEzTreeGridColumn = class;
  TEzCoolGridSchemes = class;
  TEzTreeGridScheme = class;
  TEzTreeGridSchemes = class;
  TEzSchemesLink = class;

  TPiColumnOption = (pcoDisplayParent, pcoDisplayChild, pcoUseColumnFont, pcoUseColumnColor);
  TPiColumnOptions = set of TPiColumnOption;

  TEzSchemeOption = (
    coUseColumnFont,
    coUseColumnColor,
    coUseColumnAlignment
    );

  TEzSchemeOptions = set of TEzSchemeOption;
  TEzTreeSchemeOption = (tsUseHierarchyColors);
  TEzTreeSchemeOptions = set of TEzTreeSchemeOption;

  TEzTreeColumnOption = (tcoDisplayParent, tcoDisplayChild);
  TEzTreeColumnOptions = set of TEzTreeColumnOption;

  // Obsolete
  TEzColumnIndentStyle = (
      isNone,       // Don't indent the column, don't paint expand/collapse buttons
      isNoButtons,  // Do indent the column but don't paint expand/collapse buttons
      isPlusMinus,  // Do indent the column and paint +/- style expand/collapse buttons.
                    // The user can click these buttons to expand or callapse a parent node.
      isTriangles   // Do indent the column and paint triangle style expand/collapse buttons
                    // The user can click these buttons to expand or callapse a parent node.
    );

  //
  // TEzCustomColumnScheme is the base class for column Scheme's.
  // A Scheme defines the settings for a column like color, indentation,
  // padding and so on.
  //
  TSchemeIndex = type Integer;
  TSchemeList = TList;
  TCustomEzScheme = class(TCollectionItem)
  private
    FColspan: Integer;
    FCellPadding: Integer;
    FTextAlignment: TAlignment;
    FTextIndent: Integer;
    FTextFlags: TEzTextFlags;
    FColor, FAltColor: TColor;
    FOptions: TEzSchemeOptions;
    FVerticalAlign: TVerticalAlign;
    FName: string;
    FFont: TFont;
    FAltFont: TFont;

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   Assign(Source: TPersistent); override;

  protected
    function  GetDisplayName: string; override;
    procedure SetAltColor(Value: TColor);
    procedure SetAltFont(Value: TFont);
    procedure SetCellPadding(Value: Integer);
    procedure SetColor(Value: TColor);
    procedure SetColspan(Value: Integer);
    procedure SetFont(Value: TFont);
    procedure SetTextAlignment(Value: TAlignment);
    procedure SetTextFlags(Value: TEzTextFlags);
    procedure SetTextIndent(Value: Integer);
    procedure SetVerticalAlign(V: TVerticalAlign);

  public
    property AltColor: TColor read FAltColor write SetAltColor default clWhite;
    property AltFont: TFont read FAltFont write SetAltFont;
    property CellPadding: Integer read FCellPadding write SetCellPadding default 2;
    property Color: TColor read FColor write SetColor default clWhite;
    property Colspan: Integer read FColspan write SetColspan default 1;
    property Font: TFont read FFont write SetFont;
    property Options: TEzSchemeOptions read FOptions write FOptions default [];
    property TextAlignment: TAlignment read FTextAlignment write SetTextAlignment default taLeftJustify;
    property TextFlags: TEzTextFlags read FTextFlags write SetTextFlags default [tfEndEllipses, tfSingleLine, tfNoPrefix];
    property TextIndent: Integer read FTextIndent write SetTextIndent default 5;
    property VerticalAlign: TVerticalAlign read FVerticalAlign write SetVerticalAlign default tvaCenter;

  published
    property Name: string read FName write FName;
  end;
  TEzSchemeClass = class of TCustomEzScheme;

  //
  // TEzColumnScheme is implements column Scheme's used for TEzDbCoolGrid
  // components;
  //
  PEzCoolGridScheme = ^TEzCoolGridScheme;
  TEzCoolGridScheme = class(TCustomEzScheme)
  published
//    property AltColor;
//    property AltFont;
    property CellPadding;
    property Color;
    property Colspan;
    property Font;
    property Options;
    property TextAlignment;
    property TextFlags;
    property TextIndent;
    property VerticalAlign;
  end;

  THierarchyButtonImages = array[0..2] of TImageIndex;
  TEzExpandButtons = class(TPersistent)
  private
    FImageIndexes: THierarchyButtonImages;
    FScheme: TEzTreeGridScheme;

  protected
    function  GetImage(Index: Integer) : TImageIndex;
    procedure SetImage(Index: Integer; ImageIndex: TImageIndex);

  public
    constructor Create(AScheme: TEzTreeGridScheme);
    procedure Assign(Source: TPersistent); override;

    property Scheme: TEzTreeGridScheme read FScheme;

  published
    property Expand: TImageIndex index 0 read GetImage write SetImage default -1;
    property Collapse: TImageIndex index 1 read GetImage write SetImage default -1;
    property Grayed: TImageIndex index 2 read GetImage write SetImage default -1;
  end;

  PEzTreeGridScheme = ^TEzTreeGridScheme;
  TEzTreeGridScheme = class(TCustomEzScheme)
  private
    FIndent: Integer;
    FDataVisible: Boolean;
    FHierarchyCell: Boolean;
    FTreeOptions: TEzTreeSchemeOptions;
    FExpandButtons: TEzExpandButtons;

  protected
    procedure SetIndent(Value: Integer);
    procedure SetDataVisible(Value: Boolean);
    procedure SetHierarchyCell(Value: Boolean);
    procedure SetTreeOptions(Value: TEzTreeSchemeOptions);

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;

    procedure Assign(Source: TPersistent); override;

  published
//  property AltColor;
//  property AltFont;
    property CellPadding;
    property Color;
    property Colspan;
    property Font;
    property Options;
    property TextAlignment;
    property TextFlags;
    property TextIndent;
    property VerticalAlign;

    property ExpandButtons: TEzExpandButtons read FExpandButtons write FExpandButtons;
    property Indent: Integer read FIndent write SetIndent default 10;
    property DataVisible: Boolean read FDataVisible write SetDataVisible default true;
    property HierarchyCell: Boolean read FHierarchyCell write SetHierarchyCell default false;
    property TreeOptions: TEzTreeSchemeOptions read FTreeOptions write SetTreeOptions default [tsUseHierarchyColors];
  end;

  //
  // Collection of Scheme objects
  //
  TEzSchemes = class(TOwnedCollection)
  protected
    function  GetScheme(Index: Integer): TCustomEzScheme;
    function  GetSchemeCollection: TEzCoolGridSchemes;
    procedure SetScheme(Index: Integer; Value: TCustomEzScheme);
    procedure Update(Item: TCollectionItem); override;

  public
    constructor Create(AOwner: TPersistent; AClass: TEzSchemeClass);
    destructor  Destroy; override;
    function    Add: TCustomEzScheme;

    property SchemeCollection: TEzCoolGridSchemes read GetSchemeCollection;
    property Items[Index: Integer]: TCustomEzScheme read GetScheme write SetScheme; default;
  end;

  TEzGridScrollBarOptions = class(TPersistent)
  private
    FAlwaysVisible: TScrollStyle;
    FOwner: TCustomEzDBCoolGrid;
    FScrollBars: TScrollStyle;                   // used to hide or show vertical and/or horizontal scrollbar
    procedure SetAlwaysVisible(Value: TScrollStyle);
    procedure SetScrollBars(Value: TScrollStyle);

  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(AOwner: TCustomEzDBCoolGrid);
    procedure Assign(Source: TPersistent); override;
  published
    property AlwaysVisible: TScrollStyle read FAlwaysVisible write SetAlwaysVisible default ssNone;
    property ScrollBars: TScrollStyle read FScrollbars write SetScrollBars default ssBoth;
  end;

  TEzCoolGridSchemes = class(TComponent)
  private
    FSchemes: TEzSchemes;
    FLinks: TObjectList;

  protected
    procedure AddLink(ALink: TEzSchemesLink);
    procedure RemoveLink(ALink: TEzSchemesLink);
    procedure NotifyLinks; virtual;

    function  GetSchemeClass: TEzSchemeClass; virtual;
    procedure SetSchemes(Value: TEzSchemes);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

  published
    property Schemes: TEzSchemes read FSchemes write SetSchemes;
  end;

  TEzSchemesLink = class(TPersistent)
  private
    FGrid: TCustomEzDBCoolGrid;
    FSchemes: TEzCoolGridSchemes;

  protected
    procedure Changed; virtual;
    procedure SetSchemes(Value: TEzCoolGridSchemes);

  public
    constructor Create(AOwner: TCustomEzDBCoolGrid);
    destructor Destroy; override;

    property Schemes: TEzCoolGridSchemes read FSchemes write SetSchemes;
    property Grid: TCustomEzDBCoolGrid read FGrid;
  end;

  TEzTreeGridSchemes = class(TEzCoolGridSchemes)
  private
    FButtonImages: TCustomImageList;
    FImageChangeLink: TChangeLink;

  protected
    function  GetSchemeClass: TEzSchemeClass; override;
    procedure ImageListChange(Sender: TObject);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetButtonImages(Value: TCustomImageList);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

//    procedure Assign(Source: TPersistent); override;
  published
    property ButtonImages: TCustomImageList read FButtonImages write SetButtonImages;
  end;

{$IFDEF TNT_UNICODE}
  TEzColumnTitle = class(TTntColumnTitle)
{$ELSE}
  TEzColumnTitle = class(TColumnTitle)
{$ENDIF}
  private
{
    FVAlign: TVerticalAlign;
    FTextFlags: TEzTextFlags;
}
    FSchemeIndex: TSchemeIndex;

  protected
{
    procedure SetVAlign(Value: TVerticalAlign);
    procedure SetTextFlags(Value: TEzTextFlags);
}
    procedure SetScheme(Value: TSchemeIndex);

  public
    constructor Create(Column: TColumn);
    procedure Assign(Source: TPersistent); override;

  published
{
    property VerticalAlignment: TVerticalAlign read FVAlign write SetVAlign;
    property TextFlags: TEzTextFlags read FTextFlags write SetTextFlags default [tfSingleLine, tfNoPrefix];
}
    property Scheme: TSchemeIndex read FSchemeIndex write SetScheme default -1;
  end;

{$IFDEF TNT_UNICODE}
  TCustomEzColumn = class(TTntColumn)
{$ELSE}
  TCustomEzColumn = class(TColumn)
{$ENDIF}
  private
    FSchemes: TSchemeList;
    FVisibleIndex: Integer;

  protected
    function  CreateTitle: TColumnTitle; override;
    function  GetTitle: TEzColumnTitle;
    procedure SetSchemes(Value: TSchemeList);
    procedure SetTitle(Value: TEzColumnTitle);

    // Property save and restore methods
    procedure DefineProperties(Filer: TFiler); override;
    function  HasScheme: Boolean;
    procedure ReadSchemeList(Reader: TReader);
    procedure WriteSchemeList(Writer: TWriter);

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure Assign(Source: TPersistent); override;

  public
    property Schemes: TSchemeList read FSchemes write SetSchemes stored false;
    property Title: TEzColumnTitle read GetTitle write SetTitle;
    property VisibleIndex: Integer read FVisibleIndex;
  end;

  //
  // Column class for TEzDbCoolGrid's
  //
  TEzCoolGridColumn = class(TCustomEzColumn)
  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;

  protected
    function  GetScheme: TSchemeIndex;
    procedure SetScheme(Value: TSchemeIndex);

  published
    property Scheme: TSchemeIndex read GetScheme write SetScheme stored False;
  end;

  //
  // Column class for TEzDbTreeGrid's
  //
  TEzTreeGridColumn = class(TCustomEzColumn)
  private

  protected

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;

    function  GetSchemeIndex(Level: Integer): TSchemeIndex; overload;
    function  GetSchemeIndex(Node: TNodeType): TSchemeIndex; overload;
    procedure SetSchemeIndex(Level: Integer; Value: TSchemeIndex); overload;
    procedure SetSchemeIndex(Node: TNodeType; Value: TSchemeIndex); overload;


  published
    property Schemes;
  end;

  // Maintained for backward compatibility only
  TPiColumn = class(TEzTreeGridColumn)
  private
    FOptions: TPiColumnOptions;

  public
    property Options: TPiColumnOptions read FOptions write FOptions default [];

  end;
  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=

  TEditStyle = (esSimple, esEllipsis, esPickList, esDataList);
  TPopupListbox = class;

{$IFDEF TNT_UNICODE}
  TEzInplaceEdit = class(TTntDBGridInplaceEdit)
{$ELSE}
  TEzInplaceEdit = class(TInplaceEdit)
{$ENDIF}
  private
    FButtonWidth: Integer;
    FDataList: TDBLookupListBox;
    FPickList: TPopupListbox;
    FActiveList: TWinControl;
    FLookupSource: TDatasource;
    FEditStyle: TEditStyle;
    FListVisible: Boolean;
    FTracking: Boolean;
    FPressed: Boolean;
    procedure ListMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SetEditStyle(Value: TEditStyle);
    procedure StopTracking;
    procedure TrackButton(X,Y: Integer);
    procedure CMCancelMode(var Message: TCMCancelMode); message CM_CancelMode;
    procedure WMCancelMode(var Message: TMessage); message WM_CancelMode;
    procedure WMKillFocus(var Message: TMessage); message WM_KillFocus;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message wm_LButtonDblClk;
    procedure WMPaint(var Message: TWMPaint); message wm_Paint;
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SetCursor;
    function OverButton(const P: TPoint): Boolean;
    function ButtonRect: TRect;
  protected
    procedure BoundsChanged; override;
    procedure CloseUp(Accept: Boolean); {$IFDEF TNT_UNICODE}override;{$ENDIF}
    procedure DoDropDownKeys(var Key: Word; Shift: TShiftState); {$IFDEF TNT_UNICODE}override;{$ENDIF}
    procedure DropDown; {$IFDEF TNT_UNICODE}override;{$ENDIF}
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure PaintWindow(DC: HDC); override;
    procedure UpdateContents; override;
    procedure WndProc(var Message: TMessage); override;
    property  EditStyle: TEditStyle read FEditStyle write SetEditStyle;
    property  ActiveList: TWinControl read FActiveList write FActiveList;
    property  DataList: TDBLookupListBox read FDataList;
    property  PickList: TPopupListbox read FPickList;
  public
    constructor Create(Owner: TComponent); override;
  end;

{ TPopupListbox }

  TPopupListbox = class(TCustomListbox)
  private
    FSearchText: String;
    FSearchTickCount: Longint;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  end;

{$IFDEF TNT_UNICODE}
  TEzGridDataLink = class(TTntGridDataLink)
{$ELSE}
  TEzGridDataLink = class(TGridDataLink)
{$ENDIF}
  private
    FSlaveMode: Boolean;
    FGrid: TCustomEzDBCoolGrid;

  protected
    {$IFDEF XE2}
    procedure DataEvent(Event: TDataEvent; Info: NativeInt); override;
    {$ELSE}
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    {$ENDIF}
    procedure DataSetChanged; override;

//    procedure LayoutChanged; override;
    procedure SetBufferCount(Value: Integer); override;
    procedure SetSlaveMode(Value: Boolean);

  public
    constructor Create(AGrid: TCustomDBGrid);
    property Grid: TCustomEzDBCoolGrid read FGrid;
    property SlaveMode: Boolean read FSlaveMode write SetSlaveMode default False;
  end;

  TGetColumnSchemeEvent = procedure (Sender: TCustomEzDBCoolGrid; AColumn: TCustomEzColumn; ARow: Longint; AState: TGridDrawState; var Scheme: TCustomEzScheme) of object;

  TCustomEzDBCoolGrid = class(TCustomDBGrid)
  private
    FHeaderPrinted: Boolean;
    FFixedCols: Integer;
    FFixedEditableCols: Integer;
    SavedActiveRecord: Integer;
    FTitlebarHeight: Integer;
    FSchemesLink: TEzSchemesLink;
    FScrollbarOptions: TEzGridScrollBarOptions;
    FColumnStyle: TEzButtonStyle;
    FDefaultRowHeight: Integer;
    FSkipInvalidate: Boolean;
    FTopLeftChanging: Boolean;
    FGridLineColor: TColor;

    FOnDrawTitleCell: TDrawColumnCellEvent;
    FOnGetColumnScheme: TGetColumnSchemeEvent;

  protected
    function  CanEditModify: Boolean; override;
    function  CanEditShow: Boolean; override;

    procedure ChangeGridOrientation(RightToLeftOrientation: Boolean);
    function  CreateEditor: TInplaceEdit; override;
    function  CreateDataLink: TGridDataLink; {$IFDEF EZ_D6} override; {$ELSE} virtual; {$ENDIF}
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure ColEnter; override;
    procedure ColExit; override;
    procedure DataChanged; virtual;
    procedure DefineProperties(Filer: TFiler); override;
    procedure DrawCell(AColumn: TCustomEzColumn; AScheme: TCustomEzScheme; ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); reintroduce; virtual;
    procedure DrawTitleCell(const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState); virtual;
{$IFDEF TNT_UNICODE}
    function  GetEditText(ACol, ARow: Longint): WideString; override;
{$ELSE}
    function  GetEditText(ACol, ARow: Longint): string; override;
{$ENDIF}
    function  GetFixedCols: Integer;
    function  GetFixedEditableCols: Integer;
    function  GetSchemeCollection: TEzCoolGridSchemes;
    function  GetSlaveMode: Boolean;
    function  GetTitleOffset: Integer;
    function  InternalGetColumnScheme(AColumn: TCustomEzColumn; ARow: Longint; AState: TGridDrawState): TCustomEzScheme; virtual;
    function  InternalGetTitleScheme(AColumn: TCustomEzColumn): TCustomEzScheme; virtual;
    procedure ColWidthsChanged; override;
    procedure RowHeightsChanged; override;
    procedure GridRectToScreenRect(GridRect: TGridRect; var ScreenRect: TRect; IncludeLine: Boolean);
    procedure InternalSetFixedCols; virtual;
    function  IsActiveControl: Boolean;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure LinkActive(Value: Boolean); override;
    procedure Loaded; override;
    procedure Paint; override;
    procedure Scroll(Distance: Integer); override;
    procedure SchemesUpdated; virtual;
    function  SelectCell(ACol, ARow: Longint): Boolean; override;
    procedure SetColumnAttributes; override;
    procedure SetColumnStyle(S: TEzButtonStyle);
    procedure SetDefaultRowHeight(H: Integer);
    procedure SetFixedCols(Value: Integer);
    procedure SetFixedEditableCols(Value: Integer);
    procedure SetGridLineColor(Value: TColor);
    procedure SetOptions(Value: TDBGridOptions);
    procedure SetSchemeCollection(Value: TEzCoolGridSchemes);
    procedure SetScrollBarOptions(Value: TEzGridScrollBarOptions);
    procedure SetSlaveMode(Value: Boolean);
    procedure SetTitlebarHeight(Value: Integer);
    procedure SkipProperty(Reader: TReader);
    procedure TopLeftChanged; override;
    procedure UpdateActive; virtual;
    procedure UpdateHorizontalScrollBar; virtual;
    procedure UpdateVerticalScrollBar; virtual;
    procedure UpdateRowCount; virtual;
    procedure ValidateColumn; virtual;
    function  VirtualRowCount: Integer;
    procedure WMHScroll(var Msg: TWMHScroll); message WM_HSCROLL;
    procedure WMKillFocus(var Message: TMessage); message WM_KillFocus;
    procedure WMSetFocus(var Message: TMessage); message WM_SetFocus;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure WMPrint(var Message: TWMPrint); message WM_PRINT;
    procedure WMPrintClient(var Message: TWMPrintClient); message WM_PRINTCLIENT;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    function  CellRect(ACol, ARow: Longint): TRect; reintroduce;
    function  CreateColumns: TDBGridColumns; override;
    function  GetColumnScheme(AColumn: TCustomEzColumn; ARow: Longint; AState: TGridDrawState): TCustomEzScheme; virtual;
    procedure Invalidate; override;
    function  RepairColumnIndex(ACol, ARow: Integer) : Integer; overload;
    function  RepairColumnIndex(ACol, ARow: Integer; var ColScheme: TCustomEzScheme) : Integer; overload;
    function  SpannedCellRect(ACol, ARow: Longint): TRect; virtual;

    property Col;
    property ColWidths;
    property Columns stored False; //StoreColumns;
    property ColumnStyle: TEzButtonStyle read FColumnStyle write SetColumnStyle default btsThinButton;
    property DataLink;
    property DefaultRowHeight: integer read FDefaultRowHeight write SetDefaultRowHeight default 17;
    property FixedRows;
    property FixedCols: Integer read GetFixedCols write SetFixedCols default 0;
    property FixedEditableCols: Integer read GetFixedEditableCols write SetFixedEditableCols default 0;
    property GridLineColor: TColor read FGridLineColor write SetGridLineColor default clSilver;
    property GridLineWidth;
    property LeftCol;
    property Row;
    property RowCount;
    property RowHeights;
    property DefaultDrawing;
    property Options write SetOptions;
    property ScrollbarOptions: TEzGridScrollBarOptions read FScrollbarOptions write SetScrollbarOptions;
    property SchemeCollection: TEzCoolGridSchemes read GetSchemeCollection write SetSchemeCollection;
    property SlaveMode: Boolean read GetSlaveMode write SetSlaveMode default false;
    property TitlebarHeight: Integer read FTitlebarHeight write SetTitlebarHeight
                default 40;

    property OnDrawTitleCell: TDrawColumnCellEvent read FOnDrawTitleCell write FOnDrawTitleCell;
    property OnGetColumnScheme: TGetColumnSchemeEvent read FOnGetColumnScheme write FOnGetColumnScheme;
  end;

  TEzGridHitPosition = (
    // No hit found, cursor in empty area of grid
    hpNowhere,
    // On a row displaying record data
    hpOnRow,
    // On cell holding data (hpOnCell can only occur in combinatiopn with hpOnRow)
    hpOnCell,
    // On the top border of a cell. Accuracy is determined by global HitMargin value.
    hpOnCellTopBorder,
    // On the bottom border of a cell. Accuracy is determined by global HitMargin value.
    hpOnCellBottomBorder,
    // On a expand or collapse image displayed in the cell
    hpOnExpandButton,
    // On a fixed cell in the titlebar of the grid
    hpOnTitlebar,
    // On an indicator cell of the grid
    hpOnIndicator
  );
  TEzGridHitPositions = set of TEzGridHitPosition;

  // structure used to store information about a position in the Gantt chart
  PEzGridHitInfo = ^TEzGridHitInfo;
  TEzGridHitInfo = record
    // Location where hit took place
    HitWhere: TPoint;
    // Column/Row coordinate of where hit took place (x=column, y=row)
    Coordinate: TGridCoord;
    // Node associated with row in grid
    HitNode: TRecordNode;
    // Bounding rectangle for cell at HitRow/HitColumn
    HitCellRect: TRect;
    // Type of hit
    HitPositions: TEzGridHitPositions;
  end;

  // Indicates which drag operations should be handled by the tree grid
  TEzGridDragOperation = (
    // No drag operation supported and/or active
    doNoOperation,
    // Allow tasks to move to another parent using drag and drop
    doInsertAsChild,
    // The task will be inserted before drop target.
    doInsertTask,
    // The task will be inserted after drop target.
    doInsertTaskAfter,
    // Indent task
    doIndentTask,
    // Outdent task
    doOutdentTask,
    // Drag and drop handled by owner
    doOwnerDrag
  );
  TEzGridDragOperations = set of TEzGridDragOperation;

  // Indicates which drag operations should be handled by the tree grid
  TEzGridDragSupportFlag = (
    // Allow tasks to move to another parent using drag and drop
    dsAllowChangeParent,
    // The order of tasks can be changed using drag-and-drop
    dsReorderTask,
    // Drag and drop handled by owner
    dsOwnerDrag
  );
  TEzGridDragSupportFlags = set of TEzGridDragSupportFlag;

  TEzGridDragEvent = procedure(Sender: TCustomEzDBCoolGrid; Source: TObject; State: TDragState;
                                    Pt: TPoint; HitInfo: TEzGridHitInfo;
                                    var Operations: TEzGridDragOperations;
                                    var Accept: Boolean) of object;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TCustomEzDBTreeGrid = class(TCustomEzDBCoolGrid)
  private
    FDragObject: TDragControlObject;
    FDragThreshold: Integer;
    FDragSupport: TEzGridDragSupportFlags;
    // Hit information from where mouse was pressed
    FMouseDownHitInfo: TEzGridHitInfo;
    FDragStartInfo: TEzGridHitInfo;
    FDropTargetInfo: TEzGridHitInfo;
    FDragEffect: TEzGridDragOperation;
    FTreelevelSchemes: TList; // Used by FillTreecell method

    FOnDragOver: TEzGridDragEvent;
    FOnDragDrop: TEzGridDragEvent;

  protected
    function  CanEditModify: Boolean; override;
    function  CanEditShow: Boolean; override;
    procedure ClearDragEffects;
    procedure DefineProperties(Filer: TFiler); override;
    function  GetSchemeCollection: TEzTreeGridSchemes;
    procedure SkipGridLevels(Reader: TReader);
    procedure DrawCell(AColumn: TCustomEzColumn; AScheme: TCustomEzScheme; ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure FillTreeCell(
      TreeColumn: TEzTreeGridColumn;
      TreeScheme: TEzTreeGridScheme;
      ACanvas: TCanvas; ACol, ARow: Integer; CellLevel, NextLevel: SmallInt;
      var ARect: TRect; HighLight: Boolean; AState: TGridDrawState);
    procedure LinkActive(Value: Boolean); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure RunTimer(idTimer, Interval: integer);
    procedure SetSchemeCollection(Value: TEzTreeGridSchemes);
    procedure StopTimer(idTimer: integer);
    procedure WMTimer(var Msg: TWMTimer); message WM_TIMER;


    // =--------- Drag and drop functions --------------=
    procedure CMDrag(var Message: TCMDrag); message CM_DRAG;
    procedure DragOver(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: LongInt); reintroduce; virtual;
    procedure DragDrop(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: LongInt); reintroduce; virtual;

    procedure DoDragOver(Source: TObject; State: TDragState;
                         Pt: TPoint; HitInfo: TEzGridHitInfo;
                         var Operations: TEzGridDragOperations;
                         var Accept: Boolean);

    procedure DoDragDrop(Source: TObject; State: TDragState;
                         Pt: TPoint; HitInfo: TEzGridHitInfo;
                         var Operations: TEzGridDragOperations;
                         var Accept: Boolean);
    procedure DoEndDrag(Target: TObject; X, Y: Integer); override;
    procedure DoStartDrag(var DragObject: TDragObject); override;
    // =--------- End of Drag and drop functions --------------=

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    function  CreateColumns: TDBGridColumns; override;
    procedure DragPositionInfo(Source: TObject; DragMessage: TDragMessage;
                          X, Y: Integer;
                          var HitInfo: TEzGridHitInfo;
                          var Operations: TEzGridDragOperations);
    function  GetColumnScheme(AColumn: TCustomEzColumn; ARow: Longint; AState: TGridDrawState): TCustomEzScheme; override;
    procedure GetHitTestInfoAt(X, Y: Integer; var HitInfo: TEzGridHitInfo); virtual;

    property Columns stored False; //StoreColumns;
    property DragSupport: TEzGridDragSupportFlags read FDragSupport write FDragSupport
              default [];

    property DragStartInfo: TEzGridHitInfo read FDragStartInfo;
    property DropTargetInfo: TEzGridHitInfo read FDropTargetInfo write FDropTargetInfo;

    property SchemeCollection: TEzTreeGridSchemes read GetSchemeCollection write SetSchemeCollection;
    property OnDragOver: TEzGridDragEvent read FOnDragOver write FOnDragOver;
    property OnDragDrop: TEzGridDragEvent read FOnDragDrop write FOnDragDrop;

  published

  end;

  TEzDBTreeGrid = class(TCustomEzDBTreeGrid)
  public
    property Canvas;
    property IndicatorOffset;
    property SelectedRows;

  published
    property Align;
    property Anchors;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Columns stored False; //StoreColumns;
    property ColumnStyle;
    property Constraints;
    property Cursor;
    property DataSource;
    property DefaultDrawing;
    property DefaultRowHeight;
    property DragCursor;
    property DragKind;
//    property DragMode;
    property DragSupport;
    property Enabled;
    property Font;
    property FixedCols;
    property FixedEditableCols;
    property GridLineColor;
    property GridLineWidth;
    property ImeMode;
    property ImeName;
    property Options;
    property ParentBiDiMode;
    property ParentColor;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
//    property ScrollBars;
    property ScrollbarOptions;
    property SlaveMode;
    property TabOrder;
    property SchemeCollection;
    property TitlebarHeight;
    property Visible;

    property OnCanResize;
    property OnCellClick;
    property OnColEnter;
    property OnColExit;
    property OnColumnMoved;
    property OnDrawColumnCell;
    property OnDrawTitleCell;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEditButtonClick;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetColumnScheme;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnTitleClick;
  end;

  TEzDBCoolGrid = class(TCustomEzDBCoolGrid)
  public
    property Canvas;
    property IndicatorOffset;
    property SelectedRows;

  published
    property Align;
    property Anchors;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Columns stored False; //StoreColumns;
    property ColumnStyle;
    property Constraints;
    property Cursor;
    property DataSource;
    property DefaultDrawing;
    property DefaultRowHeight;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property FixedCols;
    property FixedEditableCols;
    property GridLineColor;
    property GridLineWidth;
    property ImeMode;
    property ImeName;
    property Options;
    property ParentBiDiMode;
    property ParentColor;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property SchemeCollection;
//    property ScrollBars;
    property ScrollbarOptions;
    property SlaveMode;
    property TabOrder;
    property TitlebarHeight;
    property Visible;

    property OnCanResize;
    property OnCellClick;
    property OnColEnter;
    property OnColExit;
    property OnColumnMoved;
    property OnDrawColumnCell;
    property OnDrawTitleCell;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEditButtonClick;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetColumnScheme;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnTitleClick;
  end;

  // For backward compatibility only
  TEzDBGrid = class(TEzDBTreeGrid)
  end;

var
  UserCount: Integer;
  Indicators: TImageList;

  procedure UsesBitmap;
  procedure ReleaseBitmap;


implementation

uses EzStrings_3, Math, Forms, EzMasks, Dialogs, TypInfo, EzGantt
     {$IFDEF EZ_D6}, Variants{$ENDIF};

{$R EzDBGrid.RES}

procedure EzGridError(const Message: string; Grid: TCustomEzDBCoolGrid = nil);
begin
  if Assigned(Grid) then
    raise EEzGridError.Create(Format('%s: %s', [Grid.Name, Message])) else
    raise EEzGridError.Create(Message);
end;

function CalcFontHeight(Font: TFont): Integer;
var
  Canvas: TCanvas;
  Dc:HDC;
begin
  Canvas := TCanvas.Create;
  Dc := GetWindowDC(Application.Handle);
  Canvas.Handle := Dc;
  try
    Canvas.Font.Assign(Font);
    Result := Canvas.TextHeight('H');
  finally
    Canvas.Handle := 0;
    ReleaseDC(Application.Handle, Dc);
    Canvas.Destroy;
  end;
end;

procedure KillMessage(Wnd: HWnd; Msg: Integer);
// Delete the requested message from the queue, but throw back
// any WM_QUIT msgs that PeekMessage may also return
var
  M: TMsg;
begin
  M.Message := 0;
  if PeekMessage(M, Wnd, Msg, Msg, pm_Remove) and (M.Message = WM_QUIT) then
    PostQuitMessage(M.wparam);
end;

function TextFlagsToWord(Flags: TEzTextFlags): DWORD;
begin
  Result := 0;
  if tfWordBreak in Flags then
    Result := Result or DT_WORDBREAK;
  if tfEndEllipses in Flags then
    Result := Result or DT_END_ELLIPSIS;
  if tfPathEllipses in Flags then
    Result := Result or DT_PATH_ELLIPSIS;
  if tfHidePrefix in Flags then
    Result := Result or DT_HIDEPREFIX;
  if tfSingleLine in Flags then
    Result := Result or DT_SINGLELINE;
  if tfNoPrefix in Flags then
    Result := Result or DT_NOPREFIX;
end;
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzColumnTitle.Create(Column: TColumn);
begin
  inherited Create(Column);
  FSchemeIndex := -1;
{
  FVAlign := tvaTop;
  FTextFlags := [tfSingleLine, tfNoPrefix];
}
end;

procedure TEzColumnTitle.Assign(Source: TPersistent);
begin
  if Source is TEzColumnTitle then
  begin
    FSchemeIndex := TEzColumnTitle(Source).Scheme;
{
    FVAlign := TEzColumnTitle(Source).VerticalAlignment;
    FTextFlags := TEzColumnTitle(Source).TextFlags;
}
  end;
  inherited;
end;

{
procedure TEzColumnTitle.SetVAlign(Value: TVerticalAlign);
begin
  if Value <> FVAlign then
  begin
    FVAlign := Value;
    (Column as TCustomEzColumn).Changed(False);
  end;
end;

procedure TEzColumnTitle.SetTextFlags(Value: TEzTextFlags);
begin
  if Value <> FTextFlags then
  begin
    FTextFlags := Value;
    (Column as TCustomEzColumn).Changed(False);
  end;
end;
}

procedure TEzColumnTitle.SetScheme(Value: TSchemeIndex);
begin
  if Value <> FSchemeIndex then
  begin
    FSchemeIndex := Value;
    (Column as TCustomEzColumn).Changed(False);
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TCustomEzColumn.Create(Collection: TCollection);
begin
  inherited;
  FSchemes := TSchemelist.Create;
end;

destructor TCustomEzColumn.Destroy;
begin
  inherited;
  FSchemes.Destroy;
end;

procedure TCustomEzColumn.Assign(Source: TPersistent);
var
  i: Integer;
begin
  if Source is TCustomEzColumn then
  begin
    FSchemes.Count := TCustomEzColumn(Source).Schemes.Count;
    for i:=0 to FSchemes.Count-1 do
      FSchemes[i] := TCustomEzColumn(Source).Schemes[i];
  end;

  inherited Assign(Source);
end;

function TCustomEzColumn.CreateTitle: TColumnTitle;
begin
  Result := TEzColumnTitle.Create(Self);
end;

function TCustomEzColumn.GetTitle: TEzColumnTitle;
begin
  Result := TEzColumnTitle(inherited Title);
end;

procedure TCustomEzColumn.SetSchemes(Value: TSchemeList);
var
  i: Integer;
begin
  FSchemes.Count := Value.Count;
  for i:=0 to Value.Count-1 do
    FSchemes[i] := Value[i];
  Changed(false);
end;

procedure TCustomEzColumn.SetTitle(Value: TEzColumnTitle);
begin
  inherited Title := Value;
end;

procedure TCustomEzColumn.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('SchemeList', ReadSchemeList, WriteSchemeList, HasScheme);
end;

function TCustomEzColumn.HasScheme: Boolean;
var
  i: Integer;

begin
  i:=0;
  Result := False;
  while not Result and (i<FSchemes.Count) do
  begin
    if Integer(FSchemes[i]) <> -1 then
      Result := True;
    inc(i);
  end;
end;

procedure TCustomEzColumn.ReadSchemeList(Reader: TReader);
begin
  Reader.ReadListBegin;
  FSchemes.Clear;
  while not Reader.EndOfList do
    FSchemes.Add(Pointer(Reader.ReadInteger));
  Reader.ReadListEnd;
end;

procedure TCustomEzColumn.WriteSchemeList(Writer: TWriter);
var
  i: integer;

begin
  Writer.WriteListBegin;
  for i:=0 to FSchemes.Count-1 do
    Writer.WriteInteger(Integer(FSchemes[i]));
  Writer.WriteListEnd;
end;


//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzCoolGridColumn.Create(Collection: TCollection);
begin
  inherited;
  FSchemes.Add(Pointer(-1));
end;

destructor TEzCoolGridColumn.Destroy;
begin
  inherited;
end;

function TEzCoolGridColumn.GetScheme: TSchemeIndex;
begin
  Result := TSchemeIndex(FSchemes[0]);
end;

procedure TEzCoolGridColumn.SetScheme(Value: TSchemeIndex);
begin
  FSchemes[0] := Pointer(Value);
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TCustomEzScheme.Create(Collection: TCollection);
begin
  inherited;
  FCellPadding := 2;
  FColspan := 1;
  FColor := clWhite;
  FAltCOlor := clWhite;
  FFont := TFont.Create;
  FAltFont := TFont.Create;
  FOptions := [];
  FTextIndent := 5;
  FVerticalAlign := tvaCenter;
  FTextFlags := [tfEndEllipses, tfSingleLine, tfNoPrefix];
  FTextAlignment := taLeftJustify;
end;

destructor TCustomEzScheme.Destroy;
begin
  inherited;
  FFont.Destroy;
  FAltFont.Destroy;
end;

procedure TCustomEzScheme.Assign(Source: TPersistent);
var
  src: TCustomEzScheme;

begin
  if Source is TCustomEzScheme then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      src := Source as TCustomEzScheme;
      FAltColor := src.AltColor;
      FAltFont.Assign(src.AltFont);
      FCellPadding := src.FCellPadding;
      FColspan := src.Colspan;
      FColor := src.Color;
      FFont.Assign(src.Font);
      FOptions := src.Options;
      FTextAlignment := src.TextAlignment;
      FTextIndent := src.TextIndent;
      FTextFlags := src.TextFlags;
      FVerticalAlign := src.VerticalAlign;
      FName := src.FName;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end else
    inherited;
end;

function TCustomEzScheme.GetDisplayName: string;
begin
  if FName = '' then
    Result := 'Scheme ' + IntToStr(Index) else
    Result := FName;
end;

procedure TCustomEzScheme.SetAltColor(Value: TColor);
begin
  if Value <> FAltColor then
  begin
    FAltColor := Value;
    Changed(False);
  end;
end;

procedure TCustomEzScheme.SetAltFont(Value: TFont);
begin
  AltFont.Assign(Value);
  Changed(False);
end;

procedure TCustomEzScheme.SetCellPadding(Value: Integer);
begin
  if Value <> FCellPadding then
  begin
    FCellPadding := Value;
    Changed(False);
  end;
end;

procedure TCustomEzScheme.SetColor(Value: TColor);
begin
  if Value <> FColor then
  begin
    FColor := Value;
    Changed(False);
  end;
end;

procedure TCustomEzScheme.SetFont(Value: TFont);
begin
  FFont.Assign(Value);
  Changed(False);
end;

procedure TCustomEzScheme.SetColspan(Value: Integer);
begin
  if Value <> FColspan then
  begin
    FColspan := Value;
    Changed(False);
  end;
end;

procedure TCustomEzScheme.SetTextAlignment(Value: TAlignment);
begin
  if Value <> FTextAlignment then
  begin
    FTextAlignment := Value;
    Changed(false);
  end;
end;

procedure TCustomEzScheme.SetTextFlags(Value: TEzTextFlags);
begin
  if Value <> FTextFlags then
  begin
    FTextFlags := Value;
    Changed(false);
  end;
end;

procedure TCustomEzScheme.SetTextIndent(Value: Integer);
begin
  if Value <> FTextIndent then
  begin
    FTextIndent := Value;
    Changed(false);
  end;
end;

procedure TCustomEzScheme.SetVerticalAlign(V: TVerticalAlign);
begin
  if V <> FVerticalAlign then
  begin
    FVerticalAlign := V;
    Changed(false);
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzExpandButtons.Create(AScheme: TEzTreeGridScheme);
begin
//  inherited Create;
  FScheme := AScheme;
  FImageIndexes[0] := -1;
  FImageIndexes[1] := -1;
  FImageIndexes[2] := -1;
end;

procedure TEzExpandButtons.Assign(Source: TPersistent);
begin
  if Source is TEzExpandButtons then
    FImageIndexes := (Source as TEzExpandButtons).FImageIndexes else
    inherited;
end;

function TEzExpandButtons.GetImage(Index: Integer) : TImageIndex;
begin
  Result := FImageIndexes[Index];
end;

procedure TEzExpandButtons.SetImage(Index: Integer; ImageIndex: TImageIndex);
begin
  if FImageIndexes[Index] <> ImageIndex then
  begin
    FImageIndexes[Index] := ImageIndex;
    if Assigned(FScheme) then FScheme.Changed(false);
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzTreeGridScheme.Create(Collection: TCollection);
begin
  inherited;
  FExpandButtons := TEzExpandButtons.Create(Self);
  FIndent := 10;
  FTextIndent := 5;
  FDataVisible := True;
  FHierarchyCell := False;
  FTreeOptions := [tsUseHierarchyColors];
end;

destructor TEzTreeGridScheme.Destroy;
begin
  inherited;
  FExpandButtons.Destroy;
end;

procedure TEzTreeGridScheme.Assign(Source: TPersistent);
begin
  if Source is TEzTreeGridScheme then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      FExpandButtons.Assign(TEzTreeGridScheme(Source).ExpandButtons);
      FIndent := TEzTreeGridScheme(Source).Indent;
      FDataVisible := TEzTreeGridScheme(Source).DataVisible;
      FHierarchyCell := TEzTreeGridScheme(Source).HierarchyCell;
      FTreeOptions := TEzTreeGridScheme(Source).TreeOptions;
      inherited;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end else
    inherited;
end;

procedure TEzTreeGridScheme.SetIndent(Value: Integer);
begin
  if Value <> FIndent then
  begin
    FIndent := Value;
    Changed(false);
  end;
end;

procedure TEzTreeGridScheme.SetDataVisible(Value: Boolean);
begin
  if Value <> FDataVisible then
  begin
    FDataVisible := Value;
    Changed(false);
  end;
end;

procedure TEzTreeGridScheme.SetHierarchyCell(Value: Boolean);
begin
  if Value <> FHierarchyCell then
  begin
    FHierarchyCell := Value;
    Changed(false);
  end;
end;

procedure TEzTreeGridScheme.SetTreeOptions(Value: TEzTreeSchemeOptions);
begin
  if Value <> FTreeOptions then
  begin
    FTreeOptions := Value;
    Changed(false);
  end;
end;


//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzSchemes.Create(AOwner: TPersistent; AClass: TEzSchemeClass);
begin
  inherited Create(AOwner, AClass);
end;

destructor TEzSchemes.Destroy;
begin
  inherited;
end;

function TEzSchemes.Add: TCustomEzScheme;
begin
  Result := TCustomEzScheme(inherited Add);
end;

function TEzSchemes.GetScheme(Index: Integer): TCustomEzScheme;
begin
  Result := TCustomEzScheme(inherited Items[Index]);
end;

function TEzSchemes.GetSchemeCollection: TEzCoolGridSchemes;
begin
  Result := (inherited GetOwner) as TEzCoolGridSchemes;
end;

procedure TEzSchemes.SetScheme(Index: Integer; Value: TCustomEzScheme);
begin
  TCustomEzScheme(inherited Items[Index]).Assign(Value);
end;

procedure TEzSchemes.Update(Item: TCollectionItem);
begin
  SchemeCollection.NotifyLinks;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
// TEzGridScrollBarOptions
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=

constructor TEzGridScrollBarOptions.Create(AOwner: TCustomEzDBCoolGrid);
begin
  inherited Create;
  FOwner := AOwner;
  FScrollBars := ssBoth;
  FAlwaysVisible := ssNone;
end;

procedure TEzGridScrollBarOptions.SetAlwaysVisible(Value: TScrollStyle);

begin
  if FAlwaysVisible <> Value then
  begin
    FAlwaysVisible := Value;
    if not (csLoading in FOwner.ComponentState) and FOwner.HandleAllocated then
      FOwner.RecreateWnd;
  end;
end;

procedure TEzGridScrollBarOptions.SetScrollBars(Value: TScrollStyle);
begin
  if FScrollbars <> Value then
  begin
    FScrollBars := Value;
    if not (csLoading in FOwner.ComponentState) and FOwner.HandleAllocated then
      FOwner.RecreateWnd;
  end;
end;

function TEzGridScrollBarOptions.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

procedure TEzGridScrollBarOptions.Assign(Source: TPersistent);
begin
  if Source is TEzGridScrollBarOptions then
  begin
    FScrollBars := TEzGridScrollBarOptions(Source).ScrollBars;
    FAlwaysVisible := TEzGridScrollBarOptions(Source).AlwaysVisible;
  end
  else
    inherited;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzCoolGridSchemes.Create(AOwner: TComponent);
begin
  inherited;
  FSchemes := TEzSchemes.Create(Self, GetSchemeClass);
  FLinks := TObjectList.Create;
  FLinks.OwnsObjects := False;
end;

destructor TEzCoolGridSchemes.Destroy;
var
  i: Integer;

begin
  for i:=0 to FLinks.Count-1 do
    TEzSchemesLink(FLinks[i]).Schemes := nil;

  inherited;
  FSchemes.Destroy;
  FLinks.Destroy;
end;
{
procedure TEzCoolGridSchemes.Assign(Source: TPersistent);
begin
  if Source is TEzCoolGridSchemes then
  begin
    FSchemes.BeginUpdate;
    try
      FSchemes.Assign((Source as TEzCoolGridSchemes).FSchemes);
    finally
      FSchemes.EndUpdate;
    end;
  end else
    inherited;
end;
}

procedure TEzCoolGridSchemes.AddLink(ALink: TEzSchemesLink);
begin
  FLinks.Add(ALink)
end;

procedure TEzCoolGridSchemes.RemoveLink(ALink: TEzSchemesLink);
begin
  FLinks.Remove(ALink);
end;

procedure TEzCoolGridSchemes.NotifyLinks;
var
  i: Integer;
begin
  for i:=0 to FLinks.Count-1 do
    TEzSchemesLink(FLinks[i]).Changed;
end;

function TEzCoolGridSchemes.GetSchemeClass: TEzSchemeClass;
begin
  Result := TEzCoolGridScheme;
end;

procedure TEzCoolGridSchemes.SetSchemes(Value: TEzSchemes);
begin
  FSchemes.Assign(Value);
end;

//=---------------------------------------------------------------------------=
//
// TEzSchemesLink
//
//=---------------------------------------------------------------------------=
constructor TEzSchemesLink.Create(AOwner: TCustomEzDBCoolGrid);
begin
  inherited Create;
  FGrid := AOwner;
end;

destructor TEzSchemesLink.Destroy;
begin
  inherited;
  Schemes := nil;
end;

procedure TEzSchemesLink.Changed;
begin
  FGrid.SchemesUpdated;
end;

procedure TEzSchemesLink.SetSchemes(Value: TEzCoolGridSchemes);
begin
  if Value<>FSchemes then
  begin
    if FSchemes<>nil then
      FSchemes.RemoveLink(Self);
    FSchemes:=Value;
    if FSchemes<>nil then
      FSchemes.AddLink(Self);
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzTreeGridSchemes.Create(AOwner: TComponent);
begin
  inherited;
  FImageChangeLink := TChangeLink.Create;
  FImageChangeLink.OnChange := ImageListChange;
end;

destructor  TEzTreeGridSchemes.Destroy;
begin
  inherited;
  FButtonImages := nil;
  FImageChangeLink.Destroy;
end;

{
procedure TEzTreeGridSchemes.Assign(Source: TPersistent);
begin
  if Source is TEzTreeGridSchemes then
    ButtonImages := (Source as TEzTreeGridSchemes).ButtonImages;
  inherited;
end;
}

function TEzTreeGridSchemes.GetSchemeClass: TEzSchemeClass;
begin
  Result := TEzTreeGridScheme;
end;

procedure TEzTreeGridSchemes.ImageListChange(Sender: TObject);
begin
//  if Sender = ButtonImages then Invalidate;
end;

procedure TEzTreeGridSchemes.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (AComponent = FButtonImages) then
  begin
    FButtonImages.UnRegisterChanges(FImageChangeLink);
    FButtonImages := nil;
  end;
end;

procedure TEzTreeGridSchemes.SetButtonImages(Value: TCustomImageList);
begin
  if FButtonImages <> Value then
  begin
//    if FButtonImages <> nil then FButtonImages.UnRegisterChanges(FImageChangeLink);
    FButtonImages := Value;
//    if FButtonImages <> nil then FButtonImages.RegisterChanges(FImageChangeLink);
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzTreeGridColumn.Create(Collection: TCollection);
begin
  inherited;
end;

destructor TEzTreeGridColumn.Destroy;
begin
  inherited;
end;

function TEzTreeGridColumn.GetSchemeIndex(Level: Integer) : TSchemeIndex;
begin
  if Level < FSchemes.Count then
    Result := TSchemeIndex(FSchemes[Level]) else
    Result := -1;
end;

function TEzTreeGridColumn.GetSchemeIndex(Node: TNodeType): TSchemeIndex;
begin
  Result := GetSchemeIndex(Ord(Node));
end;

procedure TEzTreeGridColumn.SetSchemeIndex(Level: Integer; Value: TSchemeIndex);
var
  i, c: Integer;

begin
  if Level >= FSchemes.Count then
  begin
    c:=FSchemes.Count;
    FSchemes.Count := Level+1;
    // Initialize new members to -1
    for i:=c to Level do
      FSchemes[i] := Pointer(-1);
  end;

  if TSchemeIndex(FSchemes[Level]) <> Value then
  begin
    FSchemes[Level] := Pointer(Value);
    Changed(False);
  end;
end;

procedure TEzTreeGridColumn.SetSchemeIndex(Node: TNodeType; Value: TSchemeIndex);
begin
  SetSchemeIndex(Ord(Node), Value);
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
procedure TPopupListBox.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or WS_BORDER;
    ExStyle := WS_EX_TOOLWINDOW or WS_EX_TOPMOST;
    AddBiDiModeExStyle(ExStyle);
    WindowClass.Style := CS_SAVEBITS;
  end;
end;

procedure TPopupListbox.CreateWnd;
begin
  inherited CreateWnd;
  Windows.SetParent(Handle, 0);
  CallWindowProc(DefWndProc, Handle, wm_SetFocus, 0, 0);
end;

procedure TPopupListbox.Keypress(var Key: Char);
var
  TickCount: Integer;
begin
  case Key of
    #8, #27: FSearchText := '';
    #32..#255:
      begin
        TickCount := GetTickCount;
        if TickCount - FSearchTickCount > 2000 then FSearchText := '';
        FSearchTickCount := TickCount;
        if Length(FSearchText) < 32 then FSearchText := FSearchText + Key;
        SendMessage(Handle, LB_SelectString, WORD(-1), Longint(PChar(FSearchText)));
        Key := #0;
      end;
  end;
  inherited Keypress(Key);
end;

procedure TPopupListbox.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  TEzInPlaceEdit(Owner).CloseUp((X >= 0) and (Y >= 0) and
      (X < Width) and (Y < Height));
end;


constructor TEzInplaceEdit.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  FLookupSource := TDataSource.Create(Self);
  FButtonWidth := GetSystemMetrics(SM_CXVSCROLL);
  FEditStyle := esSimple;
end;

procedure TEzInplaceEdit.BoundsChanged;
var
  R: TRect;
  Column: TCustomEzColumn;
  Scheme: TCustomEzScheme;
  i, EffectiveLineWidth, n: Integer;

begin
  with TCustomEzDBCoolGrid(Grid) do
  begin
    Column := Columns[SelectedIndex] as TCustomEzColumn;
    Scheme := InternalGetColumnScheme(Column, Row, [gdSelected]);
    if (Scheme<>nil) and (Scheme.Colspan > 1) then
    begin
      if (dgColLines in Options) then
        EffectiveLineWidth := GridLineWidth else
        EffectiveLineWidth := 0;
      R := CellRect(Col, Row);
      n := Scheme.Colspan-1;
      i := Col;
      while n>0 do
      begin
        inc(i);
        if ColWidths[i]>0 then
        begin
          inc(R.Right, ColWidths[i]+EffectiveLineWidth);
          dec(n);
        end;
      end;

      with R do
        SetWindowPos(Self.Handle, HWND_TOP, Left, Top, (Right - Left) + 1, Bottom - Top,
          SWP_SHOWWINDOW or SWP_NOREDRAW);
    end;
  end;

  SetRect(R, 2, 2, Width - 2, Height);
  if FEditStyle <> esSimple then
    if not TCustomDBGrid(Owner).UseRightToLeftAlignment then
      Dec(R.Right, FButtonWidth) else
      Inc(R.Left, FButtonWidth - 2);
  SendMessage(Handle, EM_SETRECTNP, 0, LongInt(@R));
  SendMessage(Handle, EM_SCROLLCARET, 0, 0);
  if SysLocale.FarEast then
    SetImeCompositionWindow(Font, R.Left, R.Top);
end;

procedure TEzInplaceEdit.CloseUp(Accept: Boolean);
var
  MasterField: TField;
  ListValue: Variant;
begin
  if FListVisible then
  begin
    if GetCapture <> 0 then SendMessage(GetCapture, WM_CANCELMODE, 0, 0);
    if FActiveList = FDataList then
      ListValue := FDataList.KeyValue
    else
      if FPickList.ItemIndex <> -1 then
        ListValue := FPickList.Items[FPicklist.ItemIndex];
    SetWindowPos(FActiveList.Handle, 0, 0, 0, 0, 0, SWP_NOZORDER or
      SWP_NOMOVE or SWP_NOSIZE or SWP_NOACTIVATE or SWP_HIDEWINDOW);
    FListVisible := False;
    if Assigned(FDataList) then
      FDataList.ListSource := nil;
    FLookupSource.Dataset := nil;
    Invalidate;
    if Accept then
      if FActiveList = FDataList then
        with TCustomEzDBCoolGrid(Grid), Columns[SelectedIndex].Field do
        begin
          MasterField := DataSet.FieldByName(KeyFields);
          if MasterField.CanModify and DataLink.Edit then
            MasterField.Value := ListValue;
        end
      else
        if (not VarIsNull(ListValue)) and EditCanModify then
          with TCustomEzDBCoolGrid(Grid), Columns[SelectedIndex].Field do
            Text := ListValue;
  end;
end;

procedure TEzInplaceEdit.DoDropDownKeys(var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_UP, VK_DOWN:
      if ssAlt in Shift then
      begin
        if FListVisible then CloseUp(True) else DropDown;
        Key := 0;
      end;
    VK_RETURN, VK_ESCAPE:
      if FListVisible and not (ssAlt in Shift) then
      begin
        CloseUp(Key = VK_RETURN);
        Key := 0;
      end;
  end;
end;

procedure TEzInplaceEdit.DropDown;
var
  P: TPoint;
  I,J,Y: Integer;
  Column: TColumn;
begin
  if not FListVisible and Assigned(FActiveList) then
  begin
    FActiveList.Width := Width;
    with TCustomEzDBCoolGrid(Grid) do
      Column := Columns[SelectedIndex];
    if FActiveList = FDataList then
    with Column.Field do
    begin
      FDataList.Color := Color;
      FDataList.Font := Font;
      FDataList.RowCount := Column.DropDownRows;
      FLookupSource.DataSet := LookupDataSet;
      FDataList.KeyField := LookupKeyFields;
      FDataList.ListField := LookupResultField;
      FDataList.ListSource := FLookupSource;
      FDataList.KeyValue := DataSet.FieldByName(KeyFields).Value;
{      J := Column.DefaultWidth;
      if J > FDataList.ClientWidth then
        FDataList.ClientWidth := J;
}    end
    else
    begin
      FPickList.Color := Color;
      FPickList.Font := Font;
      FPickList.Items := Column.Picklist;
      if FPickList.Items.Count >= Integer(Column.DropDownRows) then
        FPickList.Height := Integer(Column.DropDownRows) * FPickList.ItemHeight + 4
      else
        FPickList.Height := FPickList.Items.Count * FPickList.ItemHeight + 4;
      if Column.Field.IsNull then
        FPickList.ItemIndex := -1
      else
        FPickList.ItemIndex := FPickList.Items.IndexOf(Column.Field.Text);
      J := FPickList.ClientWidth;
      for I := 0 to FPickList.Items.Count - 1 do
      begin
        Y := FPickList.Canvas.TextWidth(FPickList.Items[I]);
        if Y > J then J := Y;
      end;
      FPickList.ClientWidth := J;
    end;
    P := Parent.ClientToScreen(Point(Left, Top));
    Y := P.Y + Height;
    if Y + FActiveList.Height > Screen.Height then Y := P.Y - FActiveList.Height;
    SetWindowPos(FActiveList.Handle, HWND_TOP, P.X, Y, 0, 0,
      SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
    FListVisible := True;
    Invalidate;
    Windows.SetFocus(Handle);
  end;
end;

type
  TWinControlCracker = class(TWinControl) end;

procedure TEzInplaceEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (EditStyle = esEllipsis) and (Key = VK_RETURN) and (Shift = [ssCtrl]) then
  begin
    TCustomEzDBCoolGrid(Grid).EditButtonClick;
    KillMessage(Handle, WM_CHAR);
  end
  else
    inherited KeyDown(Key, Shift);
end;

procedure TEzInplaceEdit.ListMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
    CloseUp(PtInRect(FActiveList.ClientRect, Point(X, Y)));
end;

procedure TEzInplaceEdit.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if (Button = mbLeft) and (FEditStyle <> esSimple) and
    OverButton(Point(X,Y)) then
  begin
    if FListVisible then
      CloseUp(False)
    else
    begin
      MouseCapture := True;
      FTracking := True;
      TrackButton(X, Y);
      if Assigned(FActiveList) then
        DropDown;
    end;
  end;
  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TEzInplaceEdit.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  ListPos: TPoint;
  MousePos: TSmallPoint;
begin
  if FTracking then
  begin
    TrackButton(X, Y);
    if FListVisible then
    begin
      ListPos := FActiveList.ScreenToClient(ClientToScreen(Point(X, Y)));
      if PtInRect(FActiveList.ClientRect, ListPos) then
      begin
        StopTracking;
        MousePos := PointToSmallPoint(ListPos);
        SendMessage(FActiveList.Handle, WM_LBUTTONDOWN, 0, Integer(MousePos));
        Exit;
      end;
    end;
  end;
  inherited MouseMove(Shift, X, Y);
end;

procedure TEzInplaceEdit.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
  WasPressed: Boolean;
begin
  WasPressed := FPressed;
  StopTracking;
  if (Button = mbLeft) and (FEditStyle = esEllipsis) and WasPressed then
    TCustomEzDBCoolGrid(Grid).EditButtonClick;
  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TEzInplaceEdit.PaintWindow(DC: HDC);
var
  R: TRect;
  Flags: Integer;
  W, X, Y: Integer;
begin
  if FEditStyle <> esSimple then
  begin
    R := ButtonRect;
    Flags := 0;
    if FEditStyle in [esDataList, esPickList] then
    begin
      if FActiveList = nil then
        Flags := DFCS_INACTIVE
      else if FPressed then
        Flags := DFCS_FLAT or DFCS_PUSHED;
      DrawFrameControl(DC, R, DFC_SCROLL, Flags or DFCS_SCROLLCOMBOBOX);
    end
    else   { esEllipsis }
    begin
      if FPressed then Flags := BF_FLAT;
      DrawEdge(DC, R, EDGE_RAISED, BF_RECT or BF_MIDDLE or Flags);
      X := R.Left + ((R.Right - R.Left) shr 1) - 1 + Ord(FPressed);
      Y := R.Top + ((R.Bottom - R.Top) shr 1) - 1 + Ord(FPressed);
      W := FButtonWidth shr 3;
      if W = 0 then W := 1;
      PatBlt(DC, X, Y, W, W, BLACKNESS);
      PatBlt(DC, X - (W * 2), Y, W, W, BLACKNESS);
      PatBlt(DC, X + (W * 2), Y, W, W, BLACKNESS);
    end;
    ExcludeClipRect(DC, R.Left, R.Top, R.Right, R.Bottom);
  end;
  inherited PaintWindow(DC);
end;

procedure TEzInplaceEdit.SetEditStyle(Value: TEditStyle);
begin
  if Value = FEditStyle then Exit;
  FEditStyle := Value;
  case Value of
    esPickList:
      begin
        if FPickList = nil then
        begin
          FPickList := TPopupListbox.Create(Self);
          FPickList.Visible := False;
          FPickList.Parent := Self;
          FPickList.OnMouseUp := ListMouseUp;
          FPickList.IntegralHeight := True;
          FPickList.ItemHeight := 11;
        end;
        FActiveList := FPickList;
      end;
    esDataList:
      begin
        if FDataList = nil then
        begin
          FDataList := TPopupDataList.Create(Self);
          FDataList.Visible := False;
          FDataList.Parent := Self;
          FDataList.OnMouseUp := ListMouseUp;
        end;
        FActiveList := FDataList;
      end;
  else  { cbsNone, cbsEllipsis, or read only field }
    FActiveList := nil;
  end;
  with TCustomEzDBCoolGrid(Grid) do
    Self.ReadOnly := Columns[SelectedIndex].ReadOnly;
  Repaint;
end;

procedure TEzInplaceEdit.StopTracking;
begin
  if FTracking then
  begin
    TrackButton(-1, -1);
    FTracking := False;
    MouseCapture := False;
  end;
end;

procedure TEzInplaceEdit.TrackButton(X,Y: Integer);
var
  NewState: Boolean;
  R: TRect;
begin
  R := ButtonRect;
  NewState := PtInRect(R, Point(X, Y));
  if FPressed <> NewState then
  begin
    FPressed := NewState;
    InvalidateRect(Handle, @R, False);
  end;
end;

procedure TEzInplaceEdit.UpdateContents;
var
  Column: TColumn;
  NewStyle: TEditStyle;
  MasterField: TField;
begin
  with TCustomEzDBCoolGrid(Grid) do
    Column := Columns[SelectedIndex];
  NewStyle := esSimple;
  case Column.ButtonStyle of
   cbsEllipsis: NewStyle := esEllipsis;
   cbsAuto:
     if Assigned(Column.Field) then
     with Column.Field do
     begin
       { Show the dropdown button only if the field is editable }
       if FieldKind = fkLookup then
       begin
         MasterField := Dataset.FieldByName(KeyFields);
         { Column.DefaultReadonly will always be True for a lookup field.
           Test if Column.ReadOnly has been assigned a value of True }
         if Assigned(MasterField) and MasterField.CanModify and
           not ((cvReadOnly in Column.AssignedValues) and Column.ReadOnly) then
           with TCustomEzDBCoolGrid(Grid) do
             if not ReadOnly and DataLink.Active and not Datalink.ReadOnly then
               NewStyle := esDataList
       end
       else
       if Assigned(Column.Picklist) and (Column.PickList.Count > 0) and
         not Column.Readonly then
         NewStyle := esPickList
       else if DataType in [ftDataset, ftReference] then
         NewStyle := esEllipsis;
     end;
  end;
  EditStyle := NewStyle;
  inherited UpdateContents;
  Font.Assign(Column.Font);
  ImeMode := Column.ImeMode;
  ImeName := Column.ImeName;
end;

procedure TEzInplaceEdit.CMCancelMode(var Message: TCMCancelMode);
begin
  if (Message.Sender <> Self) and (Message.Sender <> FActiveList) then
    CloseUp(False);
end;

procedure TEzInplaceEdit.WMCancelMode(var Message: TMessage);
begin
  StopTracking;
  inherited;
end;

procedure TEzInplaceEdit.WMKillFocus(var Message: TMessage);
begin
  if not SysLocale.FarEast then inherited
  else
  begin
    ImeName := Screen.DefaultIme;
    ImeMode := imDontCare;
    inherited;
    if HWND(Message.WParam) <> TCustomDBGrid(Grid).Handle then
      ActivateKeyboardLayout(Screen.DefaultKbLayout, KLF_ACTIVATE);
  end;
  CloseUp(False);
end;

function TEzInplaceEdit.ButtonRect: TRect;
begin
  if not TCustomDBGrid(Owner).UseRightToLeftAlignment then
    Result := Rect(Width - FButtonWidth, 0, Width, Height)
  else
    Result := Rect(0, 0, FButtonWidth, Height);
end;

function TEzInplaceEdit.OverButton(const P: TPoint): Boolean;
begin
  Result := PtInRect(ButtonRect, P);
end;

procedure TEzInplaceEdit.WMLButtonDblClk(var Message: TWMLButtonDblClk);
begin
  with Message do
  if (FEditStyle <> esSimple) and OverButton(Point(XPos, YPos)) then
    Exit;
  inherited;
end;

procedure TEzInplaceEdit.WMPaint(var Message: TWMPaint);
begin
  PaintHandler(Message);
end;

procedure TEzInplaceEdit.WMSetCursor(var Message: TWMSetCursor);
var
  P: TPoint;
begin
  GetCursorPos(P);
  P := ScreenToClient(P);
  if (FEditStyle <> esSimple) and OverButton(P) then
    Windows.SetCursor(LoadCursor(0, idc_Arrow))
  else
    inherited;
end;

procedure TEzInplaceEdit.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    wm_KeyDown, wm_SysKeyDown, wm_Char:
      if EditStyle in [esPickList, esDataList] then
      with TWMKey(Message) do
      begin
        DoDropDownKeys(CharCode, KeyDataToShiftState(KeyData));
        if (CharCode <> 0) and FListVisible then
        begin
          with TMessage(Message) do
            SendMessage(FActiveList.Handle, Msg, WParam, LParam);
          Exit;
        end;
      end
  end;
  inherited;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
procedure UsesBitmap;
begin
  if UserCount = 0 then
  begin
    Indicators := TImageList.Create(nil);
    Indicators.Height := 11;
    Indicators.Width := 6;

    Indicators.ResourceLoad(rtBitmap, 'EZDBARROW', clOlive);
    Indicators.ResourceLoad(rtBitmap, 'EZDBEDIT', clWhite);
    Indicators.ResourceLoad(rtBitmap, 'EZDBINSERT', clWhite);
    Indicators.ResourceLoad(rtBitmap, 'EZDBMULTIDOT', clWhite);
    Indicators.ResourceLoad(rtBitmap, 'EZDBMULTIARROW', clWhite);
  end;
  Inc(UserCount);
end;

procedure ReleaseBitmap;
begin
  Dec(UserCount);
  if UserCount = 0 then
  begin
    Indicators.Free;
  end;
end;

constructor TEzGridDataLink.Create(AGrid: TCustomDBGrid);
begin
  inherited;
  FGrid := TCustomEzDBCoolGrid(AGrid);
  FSlaveMode := False;
end;

{$IFDEF XE2}
procedure TEzGridDataLink.DataEvent(Event: TDataEvent; Info: NativeInt);
{$ELSE}
procedure TEzGridDataLink.DataEvent(Event: TDataEvent; Info: Longint);
{$ENDIF}
begin
  if SlaveMode and Active then
  begin
    if Event = deGetMaxPosition then
      with DataSet as TCustomEzDataset do
        MaxPosition := min(MaxPosition, TCustomEzDBCoolGrid(Grid).VirtualRowCount)

    else if Event = deBufferCountChanged then
    begin
      if BufferCount <> Info then
        DataSetChanged;
    end
    else
      inherited;
  end
  else
    inherited;
end;

procedure TEzGridDataLink.DataSetChanged;
begin
  FGrid.DataChanged;
end;

procedure TEzGridDataLink.SetBufferCount(Value: Integer);
var
  c: integer;

begin
  if SlaveMode then
    with DataSet as TCustomEzDataset do
    begin
      UpdateMaxPosition;
      c := MaxPosition;
      if c <> BufferCount then
      begin
        inherited SetBufferCount(c);
        NotifyBufferCountChanged(c);
      end;
    end
  else
    inherited;
end;

procedure TEzGridDataLink.SetSlaveMode(Value: Boolean);
begin
  if Value <> FSlaveMode then
  begin
    FSlaveMode := Value;
    if Active then
      Dataset.Resync([]);
  end;
end;


//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
//=----------------------------------------------------------------------------=
type
  PIntArray = ^TIntArray;
  TIntArray = array[0..MaxCustomExtents] of Integer;

function PointInGridRect(Col, Row: Longint; const Rect: TGridRect): Boolean;
begin
  Result := (Col >= Rect.Left) and (Col <= Rect.Right) and (Row >= Rect.Top)
    and (Row <= Rect.Bottom);
end;

{$IFDEF XE2}
  {$IF NOT DEFINED(CLR)}
  procedure FillDWord(var Dest; Count, Value: Integer);
  {$IFDEF PUREPASCAL}
  {$POINTERMATH ON}

  var
    I: Integer;
    P: PInteger;
  begin
    P := PInteger(@Dest);
    for I := 0 to Count - 1 do
      P[I] := Value;
  end;
  {$POINTERMATH OFF}
  {$ELSE !PUREPASCAL}
  {$IFDEF CPUX86}
  asm
    XCHG  EDX, ECX
    PUSH  EDI
    MOV   EDI, EAX
    MOV   EAX, EDX
    REP   STOSD
    POP   EDI
  end;
  {$ENDIF CPUX86}
  {$ENDIF !PUREPASCAL}
  {$IFEND}

  { StackAlloc allocates a 'small' block of memory from the stack by
    decrementing SP.  This provides the allocation speed of a local variable,
    but the runtime size flexibility of heap allocated memory.  }


  {$IF NOT DEFINED(CLR)}
  function StackAlloc(Size: Integer): Pointer; {$IFNDEF PUREPASCAL} register; {$ENDIF}
  {$IFDEF PUREPASCAL}
  begin
    GetMem(Result, Size);
  end;
  {$ELSE !PUREPASCAL}
  {$IFDEF CPUX86}
  asm
    POP   ECX          { return address }
    MOV   EDX, ESP
    ADD   EAX, 3
    AND   EAX, not 3   // round up to keep ESP dword aligned
    CMP   EAX, 4092
    JLE   @@2
  @@1:
    SUB   ESP, 4092
    PUSH  EAX          { make sure we touch guard page, to grow stack }
    SUB   EAX, 4096
    JNS   @@1
    ADD   EAX, 4096
  @@2:
    SUB   ESP, EAX
    MOV   EAX, ESP     { function result = low memory address of block }
    PUSH  EDX          { save original SP, for cleanup }
    MOV   EDX, ESP
    SUB   EDX, 4
    PUSH  EDX          { save current SP, for sanity check  (sp = [sp]) }
    PUSH  ECX          { return to caller }
  end;
  {$ENDIF CPUX86}
  {$ENDIF !PUREPASCAL}
  {$IFEND}

  { StackFree pops the memory allocated by StackAlloc off the stack.
  - Calling StackFree is optional - SP will be restored when the calling routine
    exits, but it's a good idea to free the stack allocated memory ASAP anyway.
  - StackFree must be called in the same stack context as StackAlloc - not in
    a subroutine or finally block.
  - Multiple StackFree calls must occur in reverse order of their corresponding
    StackAlloc calls.
  - Built-in sanity checks guarantee that an improper call to StackFree will not
    corrupt the stack. Worst case is that the stack block is not released until
    the calling routine exits. }


  {$IF NOT DEFINED(CLR)}
  procedure StackFree(P: Pointer); {$IFNDEF PUREPASCAL}register; {$ENDIF}
  {$IFDEF PUREPASCAL}
  begin
    FreeMem(P);
  end;
  {$ELSE !PUREPASCAL}
  {$IFDEF CPUX86}
  asm
    POP   ECX                     { return address }
    MOV   EDX, DWORD PTR [ESP]
    SUB   EAX, 8
    CMP   EDX, ESP                { sanity check #1 (SP = [SP]) }
    JNE   @@1
    CMP   EDX, EAX                { sanity check #2 (P = this stack block) }
    JNE   @@1
    MOV   ESP, DWORD PTR [ESP+4]  { restore previous SP  }
  @@1:
    PUSH  ECX                     { return to caller }
  end;
  {$ENDIF CPUX86}
  {$ENDIF !PUREPASCAL}
  {$IFEND}
{$ELSE}
  procedure FillDWord(var Dest; Count, Value: Integer); register;
  asm
    XCHG  EDX, ECX
    PUSH  EDI
    MOV   EDI, EAX
    MOV   EAX, EDX
    REP   STOSD
    POP   EDI
  end;

  { StackAlloc allocates a 'small' block of memory from the stack by
    decrementing SP.  This provides the allocation speed of a local variable,
    but the runtime size flexibility of heap allocated memory.  }
  function StackAlloc(Size: Integer): Pointer; register;
  asm
    POP   ECX          { return address }
    MOV   EDX, ESP
    ADD   EAX, 3
    AND   EAX, not 3   // round up to keep ESP dword aligned
    CMP   EAX, 4092
    JLE   @@2
  @@1:
    SUB   ESP, 4092
    PUSH  EAX          { make sure we touch guard page, to grow stack }
    SUB   EAX, 4096
    JNS   @@1
    ADD   EAX, 4096
  @@2:
    SUB   ESP, EAX
    MOV   EAX, ESP     { function result = low memory address of block }
    PUSH  EDX          { save original SP, for cleanup }
    MOV   EDX, ESP
    SUB   EDX, 4
    PUSH  EDX          { save current SP, for sanity check  (sp = [sp]) }
    PUSH  ECX          { return to caller }
  end;

  { StackFree pops the memory allocated by StackAlloc off the stack.
  - Calling StackFree is optional - SP will be restored when the calling routine
    exits, but it's a good idea to free the stack allocated memory ASAP anyway.
  - StackFree must be called in the same stack context as StackAlloc - not in
    a subroutine or finally block.
  - Multiple StackFree calls must occur in reverse order of their corresponding
    StackAlloc calls.
  - Built-in sanity checks guarantee that an improper call to StackFree will not
    corrupt the stack. Worst case is that the stack block is not released until
    the calling routine exits. }
  procedure StackFree(P: Pointer); register;
  asm
    POP   ECX                     { return address }
    MOV   EDX, DWORD PTR [ESP]
    SUB   EAX, 8
    CMP   EDX, ESP                { sanity check #1 (SP = [SP]) }
    JNE   @@1
    CMP   EDX, EAX                { sanity check #2 (P = this stack block) }
    JNE   @@1
    MOV   ESP, DWORD PTR [ESP+4]  { restore previous SP  }
  @@1:
    PUSH  ECX                     { return to caller }
  end;
{$ENDIF}

procedure TCustomEzDBCoolGrid.GridRectToScreenRect(GridRect: TGridRect;
  var ScreenRect: TRect; IncludeLine: Boolean);

  function LinePos(const AxisInfo: TGridAxisDrawInfo; Line: Integer): Integer;
  var
    Start, I: Longint;
  begin
    with AxisInfo do
    begin
      Result := 0;
      if Line < FixedCellCount then
        Start := 0
      else
      begin
        if Line >= FirstGridCell then
          Result := FixedBoundary;
        Start := FirstGridCell;
      end;
      for I := Start to Line - 1 do
      begin
        Inc(Result, GetExtent(I) + EffectiveLineWidth);
        if Result > GridExtent then
        begin
          Result := 0;
          Exit;
        end;
      end;
    end;
  end;

  function CalcAxis(const AxisInfo: TGridAxisDrawInfo;
    GridRectMin, GridRectMax: Integer;
    var ScreenRectMin, ScreenRectMax: Integer): Boolean;
  begin
    Result := False;
    with AxisInfo do
    begin
      if (GridRectMin >= FixedCellCount) and (GridRectMin < FirstGridCell) then
        if GridRectMax < FirstGridCell then
        begin
          FillChar(ScreenRect, SizeOf(ScreenRect), 0); { erase partial results }
          Exit;
        end
        else
          GridRectMin := FirstGridCell;
      if GridRectMax > LastFullVisibleCell then
      begin
        GridRectMax := LastFullVisibleCell;
        if GridRectMax < GridCellCount - 1 then Inc(GridRectMax);
        if LinePos(AxisInfo, GridRectMax) = 0 then
          Dec(GridRectMax);
      end;

      ScreenRectMin := LinePos(AxisInfo, GridRectMin);
      ScreenRectMax := LinePos(AxisInfo, GridRectMax);
      if ScreenRectMax = 0 then
        ScreenRectMax := ScreenRectMin + GetExtent(GridRectMin)
      else
        Inc(ScreenRectMax, GetExtent(GridRectMax));
      if ScreenRectMax > GridExtent then
        ScreenRectMax := GridExtent;
      if IncludeLine then Inc(ScreenRectMax, EffectiveLineWidth);
    end;
    Result := True;
  end;

var
  DrawInfo: TGridDrawInfo;
  Hold: Integer;
begin
  FillChar(ScreenRect, SizeOf(ScreenRect), 0);
  if (GridRect.Left > GridRect.Right) or (GridRect.Top > GridRect.Bottom) then
    Exit;
  CalcDrawInfo(DrawInfo);
  with DrawInfo do
  begin
    if GridRect.Left > Horz.LastFullVisibleCell + 1 then Exit;
    if GridRect.Top > Vert.LastFullVisibleCell + 1 then Exit;

    if CalcAxis(Horz, GridRect.Left, GridRect.Right, ScreenRect.Left,
      ScreenRect.Right) then
    begin
      CalcAxis(Vert, GridRect.Top, GridRect.Bottom, ScreenRect.Top,
        ScreenRect.Bottom);
    end;
  end;
  if UseRightToLeftAlignment and (Canvas.CanvasOrientation = coLeftToRight) then
  begin
    Hold := ScreenRect.Left;
    ScreenRect.Left := ClientWidth - ScreenRect.Right;
    ScreenRect.Right := ClientWidth - Hold;
  end;
end;

function TCustomEzDBCoolGrid.IsActiveControl: Boolean;
var
  H: Hwnd;
  ParentForm: TCustomForm;
begin
  Result := False;
  ParentForm := GetParentForm(Self);
  if Assigned(ParentForm) then
  begin
    if (ParentForm.ActiveControl = Self) then
      Result := True
  end
  else
  begin
    H := GetFocus;
    while IsWindow(H) and (Result = False) do
    begin
      if H = WindowHandle then
        Result := True
      else
        H := GetParent(H);
    end;
  end;
end;

procedure TCustomEzDBCoolGrid.Paint;
type
  PointArray = array of TPoint;

var
  LineColor: TColor;
  DrawInfo: TGridDrawInfo;
  DrawColumn: TCustomEzColumn;
  DrawScheme: TCustomEzScheme;
  Sel: TGridRect;
  UpdateRect: TRect;
{$IF DEFINED(CLR)}
  PointsList: PointArray;
  StrokeList: array of DWORD;
  I: Integer;
{$ELSE}
  PointsList: PIntArray;
  StrokeList: PIntArray;
{$IFEND}
  MaxStroke: Integer;
  FrameFlags1, FrameFlags2: DWORD;

  procedure DrawLines(DoHorz, DoVert: Boolean; Col, Row: Longint;
    const CellBounds: array of Integer; OnColor, OffColor: TColor);

  { Cellbounds is 4 integers: StartX, StartY, StopX, StopY
    Horizontal lines:  MajorIndex = 0
    Vertical lines:    MajorIndex = 1 }

  const
    FlatPenStyle = PS_Geometric or PS_Solid or PS_EndCap_Flat or PS_Join_Miter;

    procedure DrawAxisLines(const AxisInfo: TGridAxisDrawInfo;
      Cell, MajorIndex: Integer; UseOnColor: Boolean);
    var
      Line: Integer;
      LogBrush: TLOGBRUSH;
      Index: Integer;
{$IF DEFINED(CLR)}
      Points: PointArray;
{$ELSE}
      Points: PIntArray;
{$IFEND}
      StopMajor, StartMinor, StopMinor, StopIndex: Integer;
      LineIncr: Integer;
    begin
      with Canvas, AxisInfo do
      begin
        if EffectiveLineWidth <> 0 then
        begin
          Pen.Width := GridLineWidth;
          if UseOnColor then
            Pen.Color := OnColor else
            Pen.Color := OffColor;

          if Pen.Width > 1 then
          begin
            LogBrush.lbStyle := BS_Solid;
            LogBrush.lbColor := Pen.Color;
            LogBrush.lbHatch := 0;
            Pen.Handle := ExtCreatePen(FlatPenStyle, Pen.Width, LogBrush, 0, nil);
          end;

          Points := PointsList;

          Line := CellBounds[MajorIndex] + EffectiveLineWidth shr 1 +
            GetExtent(Cell);
          //!!! ??? Line needs to be incremented for RightToLeftAlignment ???
          if UseRightToLeftAlignment and (MajorIndex = 0) then Inc(Line);
          StartMinor := CellBounds[MajorIndex xor 1];
          StopMinor := CellBounds[2 + (MajorIndex xor 1)];
          StopMajor := CellBounds[2 + MajorIndex] + EffectiveLineWidth;

{$IF DEFINED(CLR)}
          StopIndex := MaxStroke * 2;
{$ELSE}
          StopIndex := MaxStroke * 4;
{$IFEND}
          Index := 0;
          repeat
{$IF DEFINED(CLR)}
            if MajorIndex <> 0 then
            begin
              Points[Index].Y := Line;
              Points[Index].X := StartMinor;
            end else
            begin
              Points[Index].X := Line;
              Points[Index].Y := StartMinor;
            end;
            Inc(Index);
            if MajorIndex <> 0 then
            begin
              Points[Index].Y := Line;
              Points[Index].X := StopMinor;
            end else
            begin
              Points[Index].X := Line;
              Points[Index].Y := StopMinor;
            end;
            Inc(Index);
{$ELSE}
            Points^[Index + MajorIndex] := Line;         { MoveTo }
            Points^[Index + (MajorIndex xor 1)] := StartMinor;
            Inc(Index, 2);
            Points^[Index + MajorIndex] := Line;         { LineTo }
            Points^[Index + (MajorIndex xor 1)] := StopMinor;
            Inc(Index, 2);
{$IFEND}

            // Skip hidden columns/rows.  We don't have stroke slots for them
            // A column/row with an extent of -EffectiveLineWidth is hidden
            repeat
              Inc(Cell);
              LineIncr := AxisInfo.GetExtent(Cell) + EffectiveLineWidth;
            until (LineIncr > 0) or (Cell > LastFullVisibleCell);
            Inc(Line, LineIncr);
          until (Line > StopMajor) or (Cell > LastFullVisibleCell) or (Index > StopIndex);
{$IF DEFINED(CLR)}
          { 2 points per line -> Index div 2 }
          PolyPolyLine(Canvas.Handle, Points, StrokeList, Index shr 1);
{$ELSE}
           { 2 integers per point, 2 points per line -> Index div 4 }
          PolyPolyLine(Canvas.Handle, Points^, StrokeList^, Index shr 2);
{$IFEND}
        end;
      end;
    end;

  begin
    if (CellBounds[0] = CellBounds[2]) or (CellBounds[1] = CellBounds[3]) then Exit;
    if not DoHorz then
    begin
      DrawAxisLines(DrawInfo.Vert, Row, 1, DoHorz);
      DrawAxisLines(DrawInfo.Horz, Col, 0, DoVert);
    end
    else
    begin
      DrawAxisLines(DrawInfo.Horz, Col, 0, DoVert);
      DrawAxisLines(DrawInfo.Vert, Row, 1, DoHorz);
    end;
  end;

  procedure DrawCells(ACol, ARow: Longint; StartX, StartY, StopX, StopY: Integer;
    Color: TColor; IncludeDrawState: TGridDrawState);
  var
    CurCol, CurRow, ColSpan, i, n: Longint;
    Where, TempRect: TRect;
    DrawState: TGridDrawState;
    Focused, ColumnWasPainted: Boolean;

  begin
    CurRow := ARow;
    Where.Top := StartY;
    if Datalink.Active then
      SavedActiveRecord := Datalink.ActiveRecord else
      SavedActiveRecord := -1;

    try
      while (Where.Top < StopY) and (CurRow < RowCount) do
      begin
        Where.Left := StartX;
        Where.Bottom := Where.Top + RowHeights[CurRow];

        if (CurRow>=FixedRows) and Datalink.Active then
          Datalink.ActiveRecord := CurRow-FixedRows;

        CurCol := ACol;
        // Go to first visible column!
//        while (CurCol < ColCount) and (ColWidths[CurCol]<=0) do
//          inc(CurCol);

        if (CurRow>=FixedRows) and (CurCol>=IndicatorOffset) then
        begin
          // Test if this column is overlayed by a column located to the left.
          // If so, then we actually need to paint the column located at the
          // left and not the column indicated by CurCol.
          i := RepairColumnIndex(CurCol, CurRow);
          if i<>CurCol then
            // This CurCul is overlapped by column i
          begin
            for n:=CurCol-1 downto i do
              if ColWidths[n]>0 then
                dec(Where.Left, ColWidths[n]+DrawInfo.Horz.EffectiveLineWidth);
            CurCol := i;
          end;
        end;

        while (Where.Left < StopX) and (CurCol < ColCount) do
        begin
          ColSpan := 1;
          DrawColumn := nil;
          DrawScheme := nil;

          if (CurCol>=IndicatorOffset) then
          begin
            DrawColumn := TCustomEzColumn(Columns[CurCol-IndicatorOffset]);
            Where.Right := Where.Left + ColWidths[CurCol];

            if CurRow>=FixedRows then
            begin
              DrawState := [];
              DrawScheme := InternalGetColumnScheme(DrawColumn, CurRow, DrawState);
              if Assigned(DrawScheme) and (DrawScheme.Colspan>1) then
              begin
                ColSpan := min(DrawScheme.Colspan, ColCount);
                n:=ColSpan-1;
                i:=CurCol;
                while n>0 do
                begin
                  inc(i);
                  if ColWidths[i]>0 then
                  begin
                    inc(Where.Right, ColWidths[i]+DrawInfo.Horz.EffectiveLineWidth);
                    dec(n);
                  end;
                end;
              end;
            end else
              // A fixed, titlebar, cell is drawn
              DrawScheme := InternalGetTitleScheme(DrawColumn);
          end else
            Where.Right := Where.Left + ColWidths[0];

          //
          // This column was painted as a result of a previous call to DrawCells
          //
          ColumnWasPainted := (Where.Left < StartX) and (StartX > Canvas.ClipRect.Left);

          if not ColumnWasPainted and (Where.Right > Where.Left) and
             (EzIsPrinting or RectVisible(Canvas.Handle, Where))
          then
          begin
            DrawState := IncludeDrawState;
            Focused := IsActiveControl;
            if Focused and (CurRow = Row) and ((dgRowSelect in Options) or (CurCol = Col)) then
              Include(DrawState, gdFocused);
            if ((dgRowSelect in Options) and (CurRow = Row)) or PointInGridRect(CurCol, CurRow, Sel) then
              Include(DrawState, gdSelected);
            if not (gdFocused in DrawState) or not (dgEditing in Options) or
              not EditorMode or (csDesigning in ComponentState) then
            begin
              DrawCell(DrawColumn, DrawScheme, CurCol, CurRow, Where, DrawState);

              if DefaultDrawing and (gdFixed in DrawState) and Ctl3D and
                ((FrameFlags1 or FrameFlags2) <> 0) then
              begin
                TempRect := Where;
                if (FrameFlags1 and BF_RIGHT) = 0 then
                  Inc(TempRect.Right, DrawInfo.Horz.EffectiveLineWidth)
                else if (FrameFlags1 and BF_BOTTOM) = 0 then
                  Inc(TempRect.Bottom, DrawInfo.Vert.EffectiveLineWidth);
                DrawEdge(Canvas.Handle, TempRect, BDR_RAISEDINNER, FrameFlags1);
                DrawEdge(Canvas.Handle, TempRect, BDR_RAISEDINNER, FrameFlags2);
              end;
            end;
          end;

          Where.Left := Where.Right + DrawInfo.Horz.EffectiveLineWidth;

          if (Where.Left < StopX) and (CurCol < ColCount) then
          begin
            // Jump to next visible column
            n:=ColSpan;
            while n>0 do
            begin
              inc(CurCol);
              if ColWidths[CurCol]>0 then
                dec(n);
            end;
          end;
        end;
        Where.Top := Where.Bottom + DrawInfo.Vert.EffectiveLineWidth;
        Inc(CurRow);
      end;
    finally
      if Datalink.Active then
        Datalink.ActiveRecord := SavedActiveRecord;
    end;
  end;

begin
  if UseRightToLeftAlignment then ChangeGridOrientation(True);

  UpdateRect := Canvas.ClipRect;
  CalcDrawInfo(DrawInfo);

  with DrawInfo do
  begin
    if (Horz.EffectiveLineWidth > 0) or (Vert.EffectiveLineWidth > 0) then
    begin
      { Draw the grid line in the four areas (fixed, fixed), (variable, fixed),
        (fixed, variable) and (variable, variable) }
      LineColor := FGridLineColor;
      MaxStroke := Max(Horz.LastFullVisibleCell - LeftCol + FixedCols,
                        Vert.LastFullVisibleCell - TopRow + FixedRows) + 3;

{$IF DEFINED(CLR)}
      SetLength(PointsList, MaxStroke * 2); // two points per stroke
      SetLength(StrokeList, MaxStroke);
      for I := 0 to MaxStroke - 1 do
        StrokeList[I] := 2;
{$ELSE}
      PointsList := StackAlloc(MaxStroke * sizeof(TPoint) * 2);
      StrokeList := StackAlloc(MaxStroke * sizeof(Integer));
      FillDWord(StrokeList^, MaxStroke, 2);
{$IFEND}

      if ColorToRGB(Color) = clSilver then LineColor := clGray;

      DrawLines(True, True,
        0, 0, [0, 0, Horz.FixedBoundary, Vert.FixedBoundary], clBlack, FixedColor);

      DrawLines(True, True,
        LeftCol, 0, [Horz.FixedBoundary, 0, Horz.GridBoundary,
        Vert.FixedBoundary], clBlack, FixedColor);

      if dgIndicator in Options then
      begin
        DrawLines(True, True,
          0, TopRow, [0, Vert.FixedBoundary, IndicatorWidth,
          Vert.GridBoundary], clBlack, FixedColor);

        DrawLines(True, True,
          1, TopRow, [IndicatorWidth+1, Vert.FixedBoundary, Horz.FixedBoundary,
          Vert.GridBoundary], LineColor, FixedColor);
      end
      else
        DrawLines(True, True,
          0, TopRow, [0, Vert.FixedBoundary, Horz.FixedBoundary,
          Vert.GridBoundary], LineColor, FixedColor);

      DrawLines(dgRowLines in Options, dgColLines in Options, LeftCol,
        TopRow, [Horz.FixedBoundary, Vert.FixedBoundary, Horz.GridBoundary,
        Vert.GridBoundary], LineColor, Color);

{$IF DEFINED(CLR)}
      SetLength(StrokeList, 0);
      SetLength(PointsList, 0);
{$ELSE}
      StackFree(StrokeList);
      StackFree(PointsList);
{$IFEND}
    end;

    { Draw the cells in the four areas }
    Sel := Selection;
    FrameFlags1 := 0;
    FrameFlags2 := 0;

    // Draw fixed header cells
    DrawCells(0, 0, 0, 0, Horz.FixedBoundary, Vert.FixedBoundary, FixedColor,
      [gdFixed]);

    // Draw normal header cells
    DrawCells(LeftCol, 0, Horz.FixedBoundary {- FColOffset}, 0, Horz.GridBoundary,  //!! clip
      Vert.FixedBoundary, FixedColor, [gdFixed]);

    // Draw fixed columns
    DrawCells(0, TopRow, 0, Vert.FixedBoundary, Horz.FixedBoundary,
      Vert.GridBoundary, FixedColor, [gdFixed]);

    // Protect indicator column from being painted over
    ExcludeClipRect(Canvas.Handle, 0, 0, Horz.FixedBoundary, Vert.GridBoundary);

    // Draw normal columns
    DrawCells(LeftCol, TopRow, Horz.FixedBoundary {- FColOffset},                   //!! clip
      Vert.FixedBoundary, Horz.GridBoundary, Vert.GridBoundary, Color, []);

    { Fill in area not occupied by cells }
    if Horz.GridBoundary < Horz.GridExtent then
    begin
      Canvas.Brush.Color := Color;
      Canvas.FillRect(Rect(Horz.GridBoundary, 0, Horz.GridExtent, Vert.GridBoundary));
    end;
    if Vert.GridBoundary < Vert.GridExtent then
    begin
      Canvas.Brush.Color := Color;
      Canvas.FillRect(Rect(0, Vert.GridBoundary, Horz.GridExtent, Vert.GridExtent));
    end;
  end;

  if UseRightToLeftAlignment then ChangeGridOrientation(False);
end;

{$IFNDEF EZ_D6}
type
  //
  // Special grid component which has an public DataLink property.
  //
  // We use this component to override the DataLink value.
  //
  {$HINTS OFF}
  TWriteDatalinkGrid = class(TCustomGrid)
  private
    FIndicators: TImageList; FTitleFont: TFont; FReadOnly: Boolean; FOriginalImeName: TImeName;
    FOriginalImeMode: TImeMode; FUserChange: Boolean; FIsESCKey: Boolean; FLayoutFromDataset: Boolean;
    FOptions: TDBGridOptions; FTitleOffset, FIndicatorOffset: Byte; FUpdateLock: Byte;
    FLayoutLock: Byte; FInColExit: Boolean; FDefaultDrawing: Boolean; FSelfChangingTitleFont: Boolean;
    FSelecting: Boolean; FSelRow: Integer;
    FDataLink: TGridDataLink;
  end;
  {$HINTS ON}
{$ENDIF}

constructor TCustomEzDBCoolGrid.Create(AOwner: TComponent);
{$IFNDEF EZ_D6}
var
  NewLink: TGridDataLink;
{$ENDIF}

begin
  inherited;
  UsesBitmap;
  FGridLineColor := clSilver;
  FTopLeftChanging := False;
  FSkipInvalidate := False;
  FFixedCols := 0;
  FFixedEditableCols := 0;
  FColumnStyle := btsThinButton;
  Scrollbars := ssNone;
  FScrollbarOptions := TEzGridScrollBarOptions.Create(Self);
  FTitlebarHeight := 40;
  FDefaultRowHeight := 17;
  FSchemesLink := TEzSchemesLink.Create(Self);

{$IFDEF EZ_D2010}
  // We do not support themed drawing, therefore we need to set
  // FInternalColor ourselves
  Self.FInternalColor := clWindow;
{$ENDIF}

{$IFNDEF EZ_D6}
  // Override FDataLink
  DataLink.Destroy;
  NewLink := CreateDataLink;
  TWriteDatalinkGrid(Self).FDatalink := NewLink;
  if TGridDataLink(Datalink) <> TGridDataLink(NewLink) then
    ShowMessage('Failed');
{$ENDIF}
end;

destructor TCustomEzDBCoolGrid.Destroy;
begin
  inherited;
  ReleaseBitmap;
  FSchemesLink.Free;
  FScrollBarOptions.Free;
end;

function TCustomEzDBCoolGrid.CanEditModify: Boolean;
begin
  Result := not Columns[SelectedIndex].ReadOnly;

  if not Result then Exit;

  if DataSource.DataSet is TCustomEzDataset then
    Result := (DataSource.Dataset as TCustomEzDataset).FieldCanModify(Columns[SelectedIndex].Field) else
    Result := DataSource.Dataset.CanModify;

  if Result then
  begin
    Datalink.Edit;
    Result := Datalink.Editing;
    if Result then Datalink.Modified;
  end;
end;

function TCustomEzDBCoolGrid.CanEditShow: Boolean;
begin
  Result := (SelectedIndex < Columns.Count) and
            inherited CanEditShow and
            // not Columns[SelectedIndex].ReadOnly and
            DataSource.Dataset.CanModify;

  if EditorMode and not Result then
    EditorMode := False;
end;

procedure TCustomEzDBCoolGrid.ChangeGridOrientation(RightToLeftOrientation: Boolean);
var
  Org: TPoint;

begin
  if RightToLeftOrientation Then
  Begin
    Org:=Point(ClientWidth,0);
    SetMapMode(Canvas.Handle,MM_ANISOTROPIC);
    SetWindowOrgEx(Canvas.Handle,Org.X,0,Nil);
    SetViewportExtEx(Canvas.Handle,Org.X,Org.Y,Nil);
    SetWindowExtEx(Canvas.Handle,-Org.X,Org.Y,Nil);
  end
  else
  Begin
    Org:=Point(ClientWidth,0);
    SetMapMode(Canvas.Handle,MM_ANISOTROPIC);
    SetWindowOrgEx(Canvas.Handle,0,0,Nil);
    SetViewportExtEx(Canvas.Handle,Org.X,Org.Y,Nil);
    SetWindowExtEx(Canvas.Handle,Org.X,Org.Y,Nil);
  end;
end;

function TCustomEzDBCoolGrid.CellRect(ACol, ARow: Longint): TRect;
begin
  Result := inherited CellRect(ACol, ARow);
end;

function TCustomEzDBCoolGrid.CreateColumns: TDBGridColumns;
begin
{$IFDEF TNT_UNICODE}
  Result := TTntDBGridColumns.Create(Self, TEzCoolGridColumn);
{$ELSE}
  Result := TDBGridColumns.Create(Self, TEzCoolGridColumn);
{$ENDIF}
end;

function TCustomEzDBCoolGrid.CreateEditor: TInplaceEdit;
begin
  Result := TEzInplaceEdit.Create(Self);
end;

function TCustomEzDBCoolGrid.CreateDataLink: TGridDataLink;
begin
  Result := TEzGridDatalink.Create(Self);
end;

procedure TCustomEzDBCoolGrid.CreateParams(var Params: TCreateParams);
const
  ScrollBar: array[TScrollStyle] of Cardinal = (0, WS_HSCROLL, 0 {WS_VSCROLL}, WS_HSCROLL or WS_VSCROLL);

begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or (WS_CLIPCHILDREN + WS_CLIPSIBLINGS) or ScrollBar[ScrollBarOptions.FScrollBars];

    WindowClass.style := CS_DBLCLKS;
    if BorderStyle = bsSingle then
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
        Style := Style or WS_BORDER;
  end;
end;

procedure TCustomEzDBCoolGrid.CreateWnd;
var
  SI: TScrollInfo;

begin
  inherited;

  if HandleAllocated and (ScrollbarOptions.AlwaysVisible<>ssNone) then
  begin
    SI.cbSize := sizeof(SI);
    SI.fMask := SIF_ALL;
    SI.nMin := 1;
    SI.nMax := 10;
    SI.nPage := 0;
    SI.nPos := 0;
    if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssVertical]) then
      SetScrollInfo(Self.Handle, SB_VERT, SI, True);
    if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssHorizontal]) then
      SetScrollInfo(Self.Handle, SB_HORZ, SI, True);
  end;
end;

procedure TCustomEzDBCoolGrid.ColEnter;
begin
  ValidateColumn;
  inherited;
  UpdateHorizontalScrollbar;
end;

procedure TCustomEzDBCoolGrid.ColExit;
var
  R: TRect;

begin
  R := SpannedCellRect(Col, Row);
  if not IsRectEmpty(R) then
    InvalidateRect(Handle, @R, True);
  inherited;
end;

procedure TCustomEzDBCoolGrid.DataChanged;
begin
  if not HandleAllocated then Exit;
  UpdateRowCount;
  UpdateVerticalScrollBar;
  UpdateActive;
  InvalidateEditor;
  ValidateRect(Handle, nil);
  Invalidate;
end;

procedure TCustomEzDBCoolGrid.DrawCell(AColumn: TCustomEzColumn; AScheme: TCustomEzScheme; ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
const
  DT_Flags : array[TAlignment] of Cardinal = (DT_LEFT, DT_RIGHT, DT_CENTER);
  VTextFlags: array[TVerticalAlign] of Cardinal = (DT_TOP, DT_VCENTER, DT_BOTTOM);

var
  Field: TField;
  CellText: WideString;
  ImageIndex, OldActive: Integer;
  MultiSelected, Highlight: Boolean;
  DrawRect, RCopy: TRect;
  TextState: TEzTextState;
  Flags: Cardinal;

  function IsMultiSelected: Boolean;
  var
    Index, OldActive: integer;

  begin
    Result := (dgMultiSelect in Options) and (ARow >= FixedRows);
    if Result then
    begin
      OldActive := DataLink.ActiveRecord;
      try
        Datalink.ActiveRecord := ARow-FixedRows;
        Result := SelectedRows.Find(Datalink.Datasource.Dataset.Bookmark, Index);
      finally
        Datalink.ActiveRecord := OldActive;
      end;
    end;
  end;

  function GetValuefromPicklist: WideString;
  var
    tmp: Integer;
  begin
    with AColumn.Picklist do
    begin
      tmp := Field.AsInteger;
      if (tmp >= 0) and (tmp < Count) then
        Result := Strings[tmp] else
        Result := '';
    end;
  end;

begin
  CellText := '';

  if (ACol < IndicatorOffset) then

  // Indicator column

  begin
    DrawRect := ARect;
    Canvas.Brush.Color := FixedColor;
    Canvas.Pen.Color := clBlack;

    if FColumnStyle = btsXPButton then
    begin
      if dgColLines in Options then inc(DrawRect.Right, GridLineWidth);
      if dgRowLines in Options then inc(DrawRect.Bottom, GridLineWidth);
    end;

    EzRenderButton(Canvas, DrawRect, FColumnStyle, bstNormal, 0);

    if Assigned(DataLink) and DataLink.Active then
    begin
      MultiSelected := IsMultiSelected;
      if (ARow-FixedRows = SavedActiveRecord) or MultiSelected then
      begin
        ImageIndex := 0;
        if DataLink.DataSet <> nil then
          case DataLink.DataSet.State of
            dsEdit: ImageIndex := 1;
            dsInsert: ImageIndex := 2;
            dsBrowse:
              if MultiSelected then
                if (ARow <> SavedActiveRecord) then
                  ImageIndex := 3
                else
                  ImageIndex := 4;  // multiselected and current row
          end;

        Indicators.Draw(Canvas,
          DrawRect.Left + (DrawRect.Right - DrawRect.Left) div 2 - Indicators.Width div 2,
          DrawRect.Top + (DrawRect.Bottom - DrawRect.Top) div 2 - Indicators.Height div 2,
          ImageIndex, True);
      end;
    end;
  end
  else if (ARow < FixedRows) then

  // Header cell

  begin
    DrawRect := ARect;

    if DefaultDrawing then
    begin
      Canvas.Pen.Width := 1;
      if FColumnStyle = btsXPButton then
      begin
        if dgColLines in Options then inc(DrawRect.Right, GridLineWidth);
        if dgRowLines in Options then inc(DrawRect.Bottom, GridLineWidth);
      end
      else
      begin
        if Assigned(AScheme) and not (coUseColumnColor in AScheme.Options) then
          Canvas.Brush.Color := AScheme.Color else
          Canvas.Brush.Color := AColumn.Title.Color;
        Canvas.Pen.Color := clBlack;
      end;

      if Assigned(AScheme) and not (coUseColumnFont in AScheme.Options) then
        Canvas.Font := AScheme.Font else
        Canvas.Font := AColumn.Title.Font;

      if Assigned(AScheme) then
      begin
        if not (coUseColumnAlignment in AScheme.Options) then
          Flags := DT_Flags[AScheme.TextAlignment] or
                   VTextFlags[AScheme.VerticalAlign] or
                   TextFlagsToWord(AScheme.TextFlags)
        else
          Flags := DT_Flags[AColumn.Title.Alignment] or
                   VTextFlags[AScheme.VerticalAlign] or
                   TextFlagsToWord(AScheme.TextFlags)
      end
      else
      begin
        Flags := DT_Flags[AColumn.Title.Alignment];
      end;

      EzRenderTextRect(Canvas, DrawRect, AColumn.Title.Caption, FColumnStyle, bstNormal, 0, Flags);
    end; // if DefaultDrawing

    // Call event handler
    DrawTitleCell(ARect, ACol, AColumn, AState);
  end
  else if (DataLink = nil) or not DataLink.Active then
    //
    // Paint empty datacell when no data is available
    //
  begin
    Canvas.Brush.Color := Color;
    Canvas.FillRect(ARect);

    if DefaultDrawing and not (csDesigning in ComponentState) and
      (gdFocused in AState) and
      ([dgEditing, dgAlwaysShowEditor] * Options <> [dgEditing, dgAlwaysShowEditor]) and
      not (dgRowSelect in Options) and
      IsActiveControl
    then
      DrawFocusRect(Canvas.Handle, ARect)
  end

  else
  begin
    Field := AColumn.Field;

    if not AColumn.Showing then
    begin
      Canvas.Brush.Color := Color;
      Canvas.FillRect(ARect);
    end

    else

      //
      // Draw normal cell containing data
      //
    with Datalink.Dataset do
    begin
      OldActive := DataLink.ActiveRecord;
      try
        DataLink.ActiveRecord := ARow-FixedRows;

        if DefaultDrawing or (csDesigning in ComponentState) then
        begin
          if Assigned(Field) then
            if (Field.DataType in PicklistFields) and (AColumn.Picklist.Count > 0) then
              CellText := GetValueFromPicklist else
              CellText := GetWideDisplayText(Field);

          HighLight := HighlightCell(ACol, ARow, CellText, AState);

          with Canvas do
          begin
            if Assigned(AScheme) and not (coUseColumnFont in AScheme.Options) then
              Font := AScheme.Font else
              Font := AColumn.Font;

            if HighLight then
            begin
              if gdFocused in AState then
              begin
                Brush.Color := clHighlight;
                Font.Color := clHighlightText;
              end
              else
              begin
                Brush.Color := clInactiveBorder;
              end;
            end
            else if Assigned(AScheme) and not (coUseColumnColor in AScheme.Options) then
              Brush.Color := AScheme.Color else
              Brush.Color := AColumn.Color;

            DrawRect := ARect;
            FillRect(DrawRect);

            if Length(CellText)>0 then
            begin
              if not Enabled then
                TextState := tsDisabled
              else if HighLight and (gdFocused in AState) then
                TextState := tsHighLight
              else
                TextState := tsNormal;

              RCopy := DrawRect;

              if Assigned(AScheme) then
              begin
                InflateRect(RCopy, -AScheme.CellPadding, 0);
                inc(RCopy.Left, AScheme.TextIndent);
                if not (coUseColumnAlignment in AScheme.Options) then
                  Flags := DT_Flags[AScheme.TextAlignment] or
                           VTextFlags[AScheme.VerticalAlign] or
                           TextFlagsToWord(AScheme.TextFlags)
                else
                  Flags := DT_Flags[AColumn.Alignment] or
                           VTextFlags[AScheme.VerticalAlign] or
                           TextFlagsToWord(AScheme.TextFlags);
              end else
              begin
                Flags := DT_Flags[AColumn.Alignment];
                InflateRect(RCopy, -2, 0);
              end;

              EzRenderText(Canvas, RCopy, CellText, TextState, Flags);
            end;

            if not (csDesigning in ComponentState) and
               HighLight and
               IsActiveControl and
               not (dgRowSelect in Options)
            then
              Windows.DrawFocusRect(Canvas.Handle, DrawRect)
          end;
        end;

        // Call event handler
        DrawColumnCell(ARect, ACol, AColumn, AState);

      finally
        DataLink.ActiveRecord := OldActive;
      end;
    end;
  end;

//  Canvas.Pen.Color := clBlue;
//  Canvas.MoveTo(ARect.Left, ARect.Top);
//  Canvas.LineTo(ARect.Right, ARect.Bottom);

end;

procedure TCustomEzDBCoolGrid.DrawTitleCell(const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Assigned(OnDrawTitleCell) then
    OnDrawTitleCell(Self, Rect, DataCol, Column, State);
end;

procedure TCustomEzDBCoolGrid.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('CellPadding', SkipProperty, nil, false);
end;

procedure TCustomEzDBCoolGrid.Invalidate;
begin
//  if not FSkipInvalidate then
    inherited Invalidate;
//  FSkipInvalidate := False;
end;

function TCustomEzDBCoolGrid.RepairColumnIndex(ACol, ARow: Integer; var ColScheme: TCustomEzScheme) : Integer;
var
  Distance: Integer;
  GridCol: TCustomEzColumn;
  Scheme: TCustomEzScheme;

begin
  ColScheme := nil;
  Result := ACol;

  if ACol<IndicatorOffset then Exit;
  dec(ACol);
  Distance:=1;

  while ACol>0 do
  begin
    if ColWidths[ACol]>0 then
    begin
      inc(Distance);
      GridCol := TCustomEzColumn(Columns[ACol-IndicatorOffset]);
      Scheme := InternalGetColumnScheme(GridCol, ARow, []);
      if Assigned(Scheme) and (Scheme.Colspan>=Distance) then
      begin
        ColScheme := Scheme;
        Result := ACol;
        break;
      end;
    end;
    dec(ACol);
  end;
end;

function TCustomEzDBCoolGrid.RepairColumnIndex(ACol, ARow: Integer) : Integer;
var
  DummyScheme: TCustomEzScheme;

begin
  Result := RepairColumnIndex(ACol, ARow, DummyScheme);
end;

{
procedure TCustomEzDBCoolGrid.Scroll(Distance: Integer);
begin
  inherited;
  ValidateColumn;
  UpdateScrollBar;
end;
}

procedure TCustomEzDBCoolGrid.Scroll(Distance: Integer);
var
  OldRect, NewRect: TRect;
  RowHeight: Integer;
begin
  if not HandleAllocated then Exit;
  OldRect := BoxRect(0, Row, ColCount - 1, Row);
  if (DataLink.ActiveRecord >= RowCount - GetTitleOffset) then UpdateRowCount;
  UpdateVerticalScrollBar;
  UpdateActive;
  NewRect := BoxRect(0, Row, ColCount - 1, Row);
  ValidateRect(Handle, @OldRect);
  InvalidateRect(Handle, @OldRect, False);
  InvalidateRect(Handle, @NewRect, False);
  if Distance <> 0 then
  begin
    HideEditor;
    try
      if Abs(Distance) > VisibleRowCount then
      begin
        Invalidate;
        Exit;
      end
      else
      begin
        RowHeight := DefaultRowHeight;
        if dgRowLines in Options then Inc(RowHeight, GridLineWidth);
        if dgIndicator in Options then
        begin
          OldRect := BoxRect(0, GetTitleOffset+Row, ColCount - 1, GetTitleOffset+Row);
          InvalidateRect(Handle, @OldRect, False);
        end;
        NewRect := BoxRect(0, GetTitleOffset, ColCount - 1, 1000);
        ScrollWindowEx(Handle, 0, -RowHeight * Distance, @NewRect, @NewRect,
          0, nil, SW_Invalidate);
        if dgIndicator in Options then
        begin
          NewRect := BoxRect(0, Row, ColCount - 1, Row);
          InvalidateRect(Handle, @NewRect, False);
        end;
      end;
    finally
      if dgAlwaysShowEditor in Options then ShowEditor;
    end;
  end;
  ValidateColumn;
  if UpdateLock = 0 then Update;
end;

procedure TCustomEzDBCoolGrid.SchemesUpdated;
begin
  Invalidate;
end;

function TCustomEzDBCoolGrid.SelectCell(ACol, ARow: Longint): Boolean;
begin
  if ((dgIndicator in Options) and (ACol <= FixedCols)) or
     (not (dgIndicator in Options) and (ACol < FixedCols))
  then
    Result := False else
    Result := inherited SelectCell(ACol, ARow);
end;

function TCustomEzDBCoolGrid.SpannedCellRect(ACol, ARow: Longint): TRect;
var
  ColSpan, RawCol, lw: Integer;
  GridCol: TCustomEzColumn;
  ColScheme: TCustomEzScheme;

begin
  Result := Rect(-1,-1,-1,-1);

  GridCol := TCustomEzColumn(Columns[ACol-IndicatorOffset]);
  ColScheme := InternalGetColumnScheme(GridCol, ARow, []);
  if Assigned(ColScheme) and (ColScheme.Colspan>1) then
  begin
    if dgColLines in Options then
      lw := GridLineWidth else
      lw := 0;

    Result := CellRect(ACol, ARow);

    ColSpan := ColScheme.ColSpan;
    RawCol := ACol+1;
    while (ColSpan>0) and (RawCol < ColCount) do
    begin
      if ColWidths[RawCol] > 0 then
      begin
        dec(ColSpan);
        inc(Result.Right, ColWidths[RawCol]+lw);
      end;
      inc(RawCol);
    end;
  end;
end;

procedure TCustomEzDBCoolGrid.TopLeftChanged;
var
  fc: Integer;
  Current: TGridCoord;

begin
  if FTopLeftChanging then Exit;
  FTopLeftChanging := True;

  try
    fc := FFixedCols + FFixedEditableCols+IndicatorOffset;

    // Update inherited FixedCols value when moving into a fixed column
    if (Col < fc) and (fc <= inherited FixedCols+IndicatorOffset) then
    begin
      Current.Y := Row;
      inherited FixedCols := Col;
      Row := Current.Y;
    end

    // Restore inherited FixedCols value when moving out of a fixed column
    // into a reguslar column.
    else if (LeftCol >= fc) and (fc <> inherited FixedCols) then
    begin
      Current.Y := Row;
      Current.X := Col;
      inherited FixedCols := fc;
      Row := Current.Y;
      Col := max(Current.X, fc);
    end;
    inherited;
  finally
    FTopLeftChanging := False;
  end;

end;

procedure TCustomEzDBCoolGrid.ColWidthsChanged;
begin
  inherited;
  UpdateHorizontalScrollbar;
end;

procedure TCustomEzDBCoolGrid.RowHeightsChanged;
begin
  inherited;
  SetTitlebarHeight(FTitlebarHeight);
  if inherited DefaultRowHeight <> FDefaultRowHeight then
    inherited DefaultRowHeight := FDefaultRowHeight;
end;

function TCustomEzDBCoolGrid.GetColumnScheme(AColumn: TCustomEzColumn;
  ARow: Longint; AState: TGridDrawState): TCustomEzScheme;
var
  Col: TEzCoolGridColumn;

begin
  Col := AColumn as TEzCoolGridColumn;
  if (ARow >= FixedRows) and (SchemeCollection<>nil) and (Col.Scheme<>-1) and (Col.Scheme < SchemeCollection.Schemes.Count) then
    Result := SchemeCollection.Schemes[Col.Scheme] else
    Result := nil;
end;

{$IFDEF TNT_UNICODE}
function TCustomEzDBCoolGrid.GetEditText(ACol, ARow: Longint): WideString;
{$ELSE}
function TCustomEzDBCoolGrid.GetEditText(ACol, ARow: Longint): string;
{$ENDIF}
var
  x: integer;
begin
  Result := inherited GetEditText(ACol, ARow);
  with Columns[ACol-IndicatorOffset] do
    if (Length(Result)>0) and
       Assigned(Field) and
      (Field.DataType in PicklistFields) and
      (Picklist.Count > 0)
    then
    begin
      try
        x := StrToInt(Result);
        if (x>=0) and (x<Picklist.Count) then
          Result := Picklist[x];
      except
        Result := '';
      end;
    end;
end;

function TCustomEzDBCoolGrid.GetFixedCols: Integer;
begin
  Result := FFixedCols;
end;

function TCustomEzDBCoolGrid.GetFixedEditableCols: Integer;
begin
  Result := FFixedEditableCols;
end;

function TCustomEzDBCoolGrid.GetSchemeCollection: TEzCoolGridSchemes;
begin
  Result := FSchemesLink.FSchemes;
end;

function TCustomEzDBCoolGrid.GetSlaveMode: Boolean;
begin
  Result := (Datalink as TEzGridDatalink).SlaveMode;
end;

function TCustomEzDBCoolGrid.GetTitleOffset: Integer;
begin
  if dgTitles in Options then
    Result := 1 else
    Result := 0;
end;

function TCustomEzDBCoolGrid.InternalGetColumnScheme(AColumn: TCustomEzColumn; ARow: Longint; AState: TGridDrawState): TCustomEzScheme;
begin
  Result := nil;
  if Assigned(FOnGetColumnScheme) then
    FOnGetColumnScheme(Self, AColumn, ARow, AState, Result) else
    Result := GetColumnScheme(AColumn, ARow, AState);
end;

function TCustomEzDBCoolGrid.InternalGetTitleScheme(AColumn: TCustomEzColumn): TCustomEzScheme;
var
  Index: Integer;

begin
  Result := nil;
  if Assigned(FOnGetColumnScheme) then
    FOnGetColumnScheme(Self, AColumn, -1, [gdFixed], Result)
  else
  begin
    Index := AColumn.Title.Scheme;
    if (SchemeCollection<>nil) and (Index<>-1) and (Index<SchemeCollection.Schemes.Count) then
      Result := SchemeCollection.Schemes[Index];
  end;
end;

procedure TCustomEzDBCoolGrid.KeyDown(var Key: Word; Shift: TShiftState);
var
  RawCol, ColSpan: Integer;
  Scheme: TCustomEzScheme;

begin
  case Key of
    VK_HOME:
      if Shift = [] then
      begin
        RawCol := FixedCols;
        if dgIndicator in Options then
          inc(RawCol, IndicatorOffset);

        if RawCol <> Col then
        begin
          ColExit;
          Col := RawCol;
          ColEnter;
        end;

        Key := 0;
      end;
    VK_RIGHT:
    begin
      Key := 0;
      RawCol := Col;
      Scheme := InternalGetColumnScheme(TCustomEzColumn(Columns[RawCol-IndicatorOffset]), Row, []);
      if not assigned(Scheme) then
        ColSpan := 1 else
        ColSpan := Scheme.Colspan;

      while (RawCol < ColCount) and (ColSpan > 0) do
      begin
        inc(RawCol);
        if (ColWidths[RawCol] > 0) then
          dec(ColSpan);
      end;

      if RawCol >= ColCount then Exit;
      ColExit;
      Col := RawCol;
      ColEnter;
    end;
  end;
  inherited;
end;

procedure TCustomEzDBCoolGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Cell: TGridCoord;

begin
  // KV: 6 nov 2006
  // save data when users clicks in the empty area of the grid
  Cell := MouseCoord(X, Y);
  if (Cell.X<0) and EditorMode then
    (Datalink as TEzGridDataLink).UpdateData;

  if not Focused then
    //
    // We do not want to start editing when the control receives focus.
    //
  begin
    Windows.SetFocus(Handle);

    inherited;
    if not (dgAlwaysShowEditor in Options) then
      EditorMode := False;
  end else
    inherited;
end;

procedure TCustomEzDBCoolGrid.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  AField: TField;

begin
  if (FGridState=gsColMoving) and not (dgRowSelect in Options) then
  begin
    AField := SelectedField;
    inherited;
    SelectedField := AField;
  end else
    inherited;
end;

procedure TCustomEzDBCoolGrid.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
  begin
    if (AComponent = SchemeCollection) then
      SchemeCollection := nil;
  end;
end;

procedure TCustomEzDBCoolGrid.LinkActive(Value: Boolean);
begin
  InternalSetFixedCols;
  inherited;
  UpdateVerticalScrollBar;
end;

procedure TCustomEzDBCoolGrid.Loaded;
begin
  inherited;
end;

procedure TCustomEzDBCoolGrid.SetColumnAttributes;
begin
  InternalSetFixedCols;
  inherited;
end;

procedure TCustomEzDBCoolGrid.SetColumnStyle(S: TEzButtonStyle);
begin
  if FColumnStyle <> S then
  begin
    FColumnStyle := S;
    Invalidate;
  end;
end;

procedure TCustomEzDBCoolGrid.SetDefaultRowHeight(H: Integer);
begin
  FDefaultRowHeight := H;
  RowHeightsChanged;
end;

procedure TCustomEzDBCoolGrid.SetFixedCols(Value: Integer);
begin
  if FFixedCols <> Value then
  begin
    FFixedCols := Value;
    InternalSetFixedCols;
  end;
end;

procedure TCustomEzDBCoolGrid.SetFixedEditableCols(Value: Integer);
begin
  if FFixedEditableCols <> Value then
  begin
    FFixedEditableCols := Value;
    InternalSetFixedCols;
  end;
end;

procedure TCustomEzDBCoolGrid.SetGridLineColor(Value: TColor);
begin
  if FGridLineColor <> Value then
  begin
    FGridLineColor := Value;
    Invalidate;
  end;
end;

procedure TCustomEzDBCoolGrid.SetSlaveMode(Value: Boolean);
begin
  (Datalink as TEzGridDatalink).SlaveMode := Value;
end;

procedure TCustomEzDBCoolGrid.InternalSetFixedCols;
var
  Value: Integer;
  Current: TGridCoord;

begin
  if csLoading in ComponentState then Exit;

  Value := FFixedCols + FFixedEditableCols;
  if (dgIndicator in Options) then
    inc(Value);

{
  if (inherited FixedCols <> Value) and (DataSource <> nil) and
    (DataSource.DataSet <> nil) and DataSource.DataSet.Active and
    (Value < Columns.Count) and (Value < ColCount)
}
  if (inherited FixedCols <> Value) and (Value < ColCount) then
  begin
    Current.Y := Row;
    Current.X := Col;
    inherited FixedCols := Value;
    Row := Current.Y;
    Col := max(Current.X, Value);
  end;
end;

procedure TCustomEzDBCoolGrid.SetOptions(Value: TDBGridOptions);
var
  HadTitle: Boolean;
begin
  HadTitle := dgTitles in Options;
  inherited Options := Value;
  if dgRowSelect in Options then FFixedEditableCols:=0;
  if not HadTitle and (dgTitles in Options) then
    RowHeights[0] := FTitlebarHeight;
end;

procedure TCustomEzDBCoolGrid.SetSchemeCollection(Value: TEzCoolGridSchemes);
begin
  if SchemeCollection <> Value then
  begin
    FSchemesLink.Schemes := Value;
    Invalidate;
  end;
end;

procedure TCustomEzDBCoolGrid.SetScrollBarOptions(Value: TEzGridScrollBarOptions);
begin
  FScrollBarOptions.Assign(Value);
end;

procedure TCustomEzDBCoolGrid.SetTitlebarHeight(Value: Integer);
begin
  FTitlebarHeight := Value;

  if (dgTitles in Options) and (RowHeights[0] <> Value) then
    RowHeights[0] := FTitlebarHeight;
end;

procedure TCustomEzDBCoolGrid.SkipProperty(Reader: TReader);
  //
  // Skip obsolete properties
  //
begin
{$IFDEF EZ_D6}
  Reader.SkipValue;
{$ELSE}
  Reader.ReadInteger;
{$ENDIF}

end;

procedure TCustomEzDBCoolGrid.UpdateRowCount;
var
  OldRowCount: Integer;
begin
  OldRowCount := RowCount;
  if RowCount <= GetTitleOffset then RowCount := GetTitleOffset + 1;
  FixedRows := GetTitleOffset;
  with DataLink do
    if not Active or (RecordCount = 0) or not HandleAllocated then
      RowCount := 1 + GetTitleOffset
    else
    begin
      RowCount := 1000;
      DataLink.BufferCount := VisibleRowCount;
      RowCount := RecordCount + GetTitleOffset;
      if dgRowSelect in Options then TopRow := FixedRows;
      UpdateActive;
    end;
  if OldRowCount <> RowCount then Invalidate;
end;

procedure TCustomEzDBCoolGrid.UpdateActive;
var
  NewRow: Integer;

begin
  if Datalink.Active and HandleAllocated and not (csLoading in ComponentState) then
  begin
    NewRow := Datalink.ActiveRecord + GetTitleOffset;
    if Row <> NewRow then
    begin
      if not (dgAlwaysShowEditor in Options) then HideEditor;
      MoveColRow(Col, NewRow, False, False);
      InvalidateEditor;
    end;
{
    Field := SelectedField;
    if Assigned(Field) and (Field.Text <> FEditText) then
      InvalidateEditor;
}
  end;
end;

procedure TCustomEzDBCoolGrid.UpdateHorizontalScrollBar;
var
  DrawInfo: TGridDrawInfo;
  ScrollInfo: TScrollInfo;

begin
  if (ScrollbarOptions.ScrollBars in [ssBoth, ssHorizontal]) and HandleAllocated then
  begin
    CalcDrawInfo(DrawInfo);
    with DrawInfo do
    begin
      ScrollInfo.cbSize := sizeof(ScrollInfo);
      ScrollInfo.fMask := SIF_ALL;
      if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssHorizontal]) then
        ScrollInfo.fMask := ScrollInfo.fMask or SIF_DISABLENOSCROLL;

      if (Horz.FirstGridCell<>Horz.FixedCellCount) or (Horz.GridCellCount>Horz.LastFullVisibleCell+1) then
      begin
        ScrollInfo.nMin := IndicatorOffset+FixedCols;
        ScrollInfo.nMax := Horz.GridCellCount-1;
        ScrollInfo.nPage := 1;
        if dgRowSelect in Options  then
          ScrollInfo.nPos := LeftCol else
          ScrollInfo.nPos := Col;
      end
      else
      begin
        ScrollInfo.nMin := 0;
        ScrollInfo.nMax := 0;
        ScrollInfo.nPage := 1;
        ScrollInfo.nPos := 0;
      end;
      SetScrollInfo(Self.Handle, SB_HORZ, ScrollInfo, True);
    end;
  end;
end;

procedure TCustomEzDBCoolGrid.UpdateVerticalScrollBar;
var
  SIOld, SINew: TScrollInfo;
  rc: DWORD;

begin
  if Datalink.Active and HandleAllocated and (ScrollbarOptions.ScrollBars in [ssBoth, ssVertical]) then
  begin
    with Datalink.DataSet do
    begin
      SIOld.cbSize := sizeof(SIOld);
      SIOld.fMask := SIF_ALL;

      GetScrollInfo(Self.Handle, SB_VERT, SIOld);
      SINew := SIOld;

      if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssVertical]) then
        SINew.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
        SINew.fMask := SIF_ALL;

      if IsSequenced then
      begin
        SINew.nMin := 1;
        SINew.nPage := VirtualRowCount; //Self.VisibleRowCount;
        rc := RecordCount;

        if rc<=SINew.nPage then // Hide scrollbar
        begin
          SINew.nMax := 1;
          SINew.nMin := 1;
          SINew.nPage := 1;
          SINew.nPos := 0;
        end
        else
        begin
          SINew.nMax := Integer(rc + SINew.nPage - 1);
          if State in [dsInactive, dsBrowse, dsEdit] then
            SINew.nPos := RecNo;
        end;
      end
      else
      begin
        SINew.nMin := 0;
        SINew.nPage := 0;
        SINew.nMax := 4;
        if DataLink.BOF then
          SINew.nPos := 0
        else if DataLink.EOF then
          SINew.nPos := 4
        else
          SINew.nPos := 2;
      end;
      if (SINew.nMin <> SIOld.nMin) or (SINew.nMax <> SIOld.nMax) or
        (SINew.nPage <> SIOld.nPage) or (SINew.nPos <> SIOld.nPos)
      then
        SetScrollInfo(Self.Handle, SB_VERT, SINew, True);
    end;
  end
  else
  begin
    if (ScrollbarOptions.AlwaysVisible in [ssBoth, ssVertical]) then
      SINew.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
      SINew.fMask := SIF_ALL;

    SINew.nMax := 1;
    SINew.nMin := 1;
    SINew.nPage := 1;
    SINew.nPos := 1;
    SetScrollInfo(Self.Handle, SB_VERT, SINew, True);
  end;
end;

procedure TCustomEzDBCoolGrid.ValidateColumn;
var
  RCol: LongInt;
  lw, n: Integer;
  R: TRect;
  ColScheme: TCustomEzScheme;

begin
  RCol := RepairColumnIndex(Col, Row, ColScheme);

  if Assigned(ColScheme) and (ColScheme.ColSpan>1) then

    //
    // Jumping into a column spanning multiple cells.
    // - Move to the initial column
    // - Invalidate rectangle of cell + spanned cells to the right
    //

  begin
    if dgColLines in Options then
      lw := GridLineWidth else
      lw := 0;

    // Because RepairColumnIndex has been called, FDrawScheme
    // contains the scheme to be used with the column.
    R := CellRect(RCol, Row);
    // Extend right side of rectangle
    for n:=RCol+1 to RCol+ColScheme.ColSpan-1 do
      inc(R.Right, ColWidths[n]+lw);

    InvalidateRect(Handle, @R, True);

    if RCol <> Col then
      Col := RCol;
  end;
end;

function TCustomEzDBCoolGrid.VirtualRowCount: Integer;
var
  di: TGridDrawInfo;
  P: Integer;

begin
  CalcDrawInfo(di);
  Result := di.Vert.LastFullVisibleCell;
  P := di.Vert.FullVisBoundary;
  inc(P, di.Vert.GetExtent(Result+1) + di.Vert.EffectiveLineWidth);
  while P <= di.Vert.GridExtent do
  begin
    inc(Result);
    inc(P, di.Vert.GetExtent(Result+1) + di.Vert.EffectiveLineWidth);
  end;
end;

procedure TCustomEzDBCoolGrid.WMHScroll(var Msg: TWMHScroll);
var
  DrawInfo: TGridDrawInfo;
  RawCol: Integer;

  procedure NeedDrawInfo;
  begin
    CalcDrawInfo(DrawInfo);
  end;

begin
  (Datalink as TEzGridDataLink).UpdateData;

  NeedDrawInfo;

  RawCol := Col;

  case Msg.ScrollCode of
    SB_LINEUP, SB_PAGEUP:
      if dgRowSelect in Options then
        LeftCol:=max(FixedCols+IndicatorOffset, LeftCol-1) else
        RawCol:=max(FixedCols+IndicatorOffset, Col-1);
    SB_LINEDOWN, SB_PAGEDOWN:
      if dgRowSelect in Options then
        LeftCol:=min(DrawInfo.Horz.GridCellCount-1, LeftCol+1) else
        RawCol:=min(DrawInfo.Horz.GridCellCount-1, Col+1);
    SB_THUMBTRACK, SB_THUMBPOSITION:
      if dgRowSelect in Options then
        LeftCol:=min(DrawInfo.Horz.GridCellCount-1, max(FixedCols+IndicatorOffset, Msg.Pos)) else
        RawCol:=min(DrawInfo.Horz.GridCellCount-1, max(FixedCols+IndicatorOffset, Msg.Pos));
    SB_BOTTOM:
      if dgRowSelect in Options then
        LeftCol:=DrawInfo.Horz.GridCellCount-1 else
        RawCol:=DrawInfo.Horz.GridCellCount-1;
    SB_TOP:
      if dgRowSelect in Options then
        LeftCol:=FixedCols+IndicatorOffset else
        RawCol:=FixedCols+IndicatorOffset;
  end;

  if (RawCol >= ColCount) or (RawCol = Col) then
    Exit;

  ColExit;
  Col := RawCol;
  ColEnter;

  UpdateHorizontalScrollBar;
end;

procedure TCustomEzDBCoolGrid.WMKillFocus(var Message: TMessage);
var
  R: TRect;
begin
  inherited;
  if dgRowSelect in Options then
  begin
    R  := CellRect(0, Row);
    R.Left := 0;
    R.Right := Width;
  end else
    R := SpannedCellRect(Col, Row);

  if not IsRectEmpty(R) then
    InvalidateRect(Handle, @R, True);
end;

procedure TCustomEzDBCoolGrid.WMSetFocus(var Message: TMessage);
var
  R: TRect;
begin
  inherited;

  if dgRowSelect in Options then
  begin
    R  := CellRect(0, Row);
    R.Left := 0;
    R.Right := Width;
  end else
    R := SpannedCellRect(Col, Row);

  if not IsRectEmpty(R) then
    InvalidateRect(Handle, @R, True);
end;

procedure TCustomEzDBCoolGrid.WMSize(var Msg: TWMSize);
begin
  inherited;
  UpdateHorizontalScrollbar;
  UpdateVerticalScrollbar;
end;

procedure TCustomEzDBCoolGrid.WMPrint(var Message: TWMPrint);
var
  Rgn: HRgn;
  R: TRect;

begin
  // Draw only if the window is visible or visibility is not required.
  if ((Message.Flags and PRF_CHECKVISIBLE) = 0) or IsWindowVisible(Handle) then
  begin
    EzBeginPrint;

    try
      R := ClientRect;

      if (BorderStyle = bsSingle) then
        OffsetRect(R, 2, 2);

      LPtoDP(Message.DC, R, 2);
      Rgn := CreateRectRgnIndirect(R);
      SelectClipRgn(Message.DC, Rgn);
      PaintTo(Message.DC, 0, 0);
      SelectClipRgn(Message.DC, 0);
      DeleteObject(Rgn);

      FHeaderPrinted := True;
    finally
      EzEndPrint;
    end;
  end;
  inherited;
  FHeaderPrinted := false;
end;

procedure TCustomEzDBCoolGrid.WMPrintClient(var Message: TWMPrintClient);
var
  Rgn: HRgn;
  R: TRect;
  DrawInfo: TGridDrawInfo;
  hOffset, vOffset: integer;

begin
  hOffset := 0;
  vOffset := 0;
  // Draw only if the window is visible or visibility is not required.
  if ((Message.Flags and PRF_CHECKVISIBLE) = 0) or IsWindowVisible(Handle) then
  begin
    EzBeginPrint;
    try
      CalcDrawInfo(DrawInfo);
      SetRect(R, 0, DrawInfo.Vert.FixedBoundary, ClientWidth, ClientHeight);
      if (BorderStyle = bsSingle) then
        OffsetRect(R, 2, 2);

      if not FHeaderPrinted then
        vOffset := -DrawInfo.Vert.FixedBoundary;

      if ((Message.Flags and PRF_NONCLIENT) <> 0) then
      begin
        dec(vOffset, 2);
        dec(hOffset, 2);
      end;

      LPtoDP(Message.DC, R, 2);
      Rgn := CreateRectRgnIndirect(R);
      SelectClipRgn(Message.DC, Rgn);
      PaintTo(Message.DC, hOffset, vOffset);
      SelectClipRgn(Message.DC, 0);
      DeleteObject(Rgn);
    finally
      EzEndPrint;
    end;
  end;
end;

//=----------------------------------------------------------------------------=
//
// Implementation of TCustomEzDBTreeGrid.
//
// TCustomEzDBTreeGrid is a tree grid designed to work with TEzDataset's
//
//=----------------------------------------------------------------------------=
constructor TCustomEzDBTreeGrid.Create(AOwner: TComponent);
begin
  inherited;
  ControlStyle := ControlStyle - [csSetCaption] + [csCaptureMouse, csOpaque, csReplicatable];

  FTreelevelSchemes := TList.Create;
  FDragSupport := [];
  FDragEffect := doNoOperation;
  FDragThreshold := 5;
end;

destructor TCustomEzDBTreeGrid.Destroy;
begin
  inherited;
  FTreelevelSchemes.Destroy;
end;

function TCustomEzDBTreeGrid.CanEditModify: Boolean;
begin
  Result := not Columns[SelectedIndex].ReadOnly and
            Assigned(Columns[SelectedIndex].Field) and
           (DataSource.Dataset as TCustomEzDataset).FieldCanModify(Columns[SelectedIndex].Field);

  if Result then
  begin
    Datalink.Edit;
    Result := Datalink.Editing;
    if Result then Datalink.Modified;
  end;
end;

function TCustomEzDBTreeGrid.CanEditShow: Boolean;
begin
  Result := inherited CanEditShow and
            Assigned(Columns[SelectedIndex].Field) and
            (DataSource.Dataset as TCustomEzDataset).FieldCanModify(Columns[SelectedIndex].Field);
end;

function TCustomEzDBTreeGrid.CreateColumns: TDBGridColumns;
begin
{$IFDEF TNT_UNICODE}
  Result := TTntDBGridColumns.Create(Self, TEzTreeGridColumn);
{$ELSE}
  Result := TDBGridColumns.Create(Self, TEzTreeGridColumn);
{$ENDIF}
end;

procedure TCustomEzDBTreeGrid.ClearDragEffects;
var
  L: Integer;
begin
  if FDragEffect <> doNoOperation then
  begin
    with Canvas do
    begin
      Pen.Color := clBlue;
      Pen.Style := psSolid;
      Pen.Mode := pmNotXor;
      Pen.Width := 2;

      if dgIndicator in Options then
        L:=ColWidths[0] else
        L:=0;

      case FDragEffect of
        doInsertAsChild:
        begin
          Pen.Color := $E0E0E0;
          Brush.Color := $E0E0E0;
          Rectangle(L, FDropTargetInfo.HitCellRect.Top, GridWidth, FDropTargetInfo.HitCellRect.Bottom);
        end;

        doInsertTask:
        begin
          MoveTo(L, FDropTargetInfo.HitCellRect.Top-1);
          LineTo(GridWidth, FDropTargetInfo.HitCellRect.Top-1);
        end;

        doInsertTaskAfter:
        begin
          MoveTo(L, FDropTargetInfo.HitCellRect.Bottom);
          LineTo(GridWidth, FDropTargetInfo.HitCellRect.Bottom);
        end;
      end;
      FDragEffect := doNoOperation;
      Pen.Mode := pmCopy;
    end;
  end;
end;

procedure TCustomEzDBTreeGrid.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('GridLevels', SkipGridLevels, nil, false);
end;

function TCustomEzDBTreeGrid.GetSchemeCollection: TEzTreeGridSchemes;
begin
  Result := TEzTreeGridSchemes(FSchemesLink.Schemes);
end;

procedure TCustomEzDBTreeGrid.SkipGridLevels(Reader: TReader);
{$IFNDEF EZ_D6}
var
  C: TCollection;
{$ENDIF}
begin
  ShowMessage(
    'GridLevels are no longer part of the TEzDbTreeGrid component and are replaced by schemes.' + #10#13 +
    'Schemes allow you to add a different looks to each cell of your grid.' + #10#13 +
    'To start using schemes, do the following:' + #10#13 +
    '- Add a TEzTreeGridSchemes component to your form' + #10#13 +
    '- Assign it to TEzDbTreeGrid.SchemeCollection' + #10#13 +
    '- Add one or more schemes to the TEzTreeGridSchemes.Schemes collection' + #10#13 +
    '- Assign schemes to the columns of your TEzDbTreeGrid''s by pressing ''...'' on TEzDbTreeGrid.Columns.Schemes.'
    );
{$IFDEF EZ_D6}
  Reader.SkipValue;
{$ELSE}
  C := TCollection.Create(TCollectionItem);
  try
    Reader.ReadCollection(C);
    C.Destroy;
  except
  end;
{$ENDIF}
end;

procedure TCustomEzDBTreeGrid.DrawCell(AColumn: TCustomEzColumn; AScheme: TCustomEzScheme; ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
const
  DT_Flags : array[TAlignment] of Cardinal = (DT_LEFT, DT_RIGHT, DT_CENTER);
  VTextFlags: array[TVerticalAlign] of Cardinal = (DT_TOP, DT_VCENTER, DT_BOTTOM);
  DEFAULT_FLAGS = DT_SINGLELINE or DT_NOPREFIX or DT_END_ELLIPSIS;

var
  TreeColumn: TEzTreeGridColumn;
  TreeScheme: TEzTreeGridScheme;
  CellText: WideString;
  ImageIndex, TaskLevel, NextLevel: Integer;
  Highlight, HasChilds, IsExpanded: Boolean;
  DrawRect, RCopy: TRect;
  Field: TField;
  TextState: TEzTextState;
  Flags: Cardinal;

  function IsMultiSelected: Boolean;
  var
    Index, OldActive: integer;

  begin
    Result := (dgMultiSelect in Options) and (ARow >= FixedRows);
    if Result then
    begin
      OldActive := DataLink.ActiveRecord;
      try
        Datalink.ActiveRecord := ARow-FixedRows;
        Result := SelectedRows.Find(Datalink.Datasource.Dataset.Bookmark, Index);
      finally
        Datalink.ActiveRecord := OldActive;
      end;
    end;
  end;

  function GetValuefromPicklist: WideString;
  var
    tmp: Integer;
  begin
    with TreeColumn.Picklist do
    begin
      tmp := Field.AsInteger;
      if (tmp >= 0) and (tmp < Count) then
        Result := Strings[tmp] else
        Result := '';
    end;
  end;

begin
  IsExpanded := false;
  CellText := '';

  if (ACol < IndicatorOffset) or (ARow < FixedRows) or (DataLink = nil) or
     not DataLink.Active or not (DataLink.Dataset is TCustomEzDataset)
  then
    inherited

  else
  begin
    TreeColumn := AColumn as TEzTreeGridColumn;
    TreeScheme := AScheme as TEzTreeGridScheme;
    Field := TreeColumn.Field;

    with Datalink.DataSet as TCustomEzDataset do
    begin
      if DefaultDrawing or (csDesigning in ComponentState) then
      begin
        if Assigned(Field) and Field.Visible then
          if TreeColumn.Visible or ((SavedActiveRecord = DataLink.ActiveRecord) and (State = dsInsert)) then
            if (Field.DataType in PicklistFields) and (TreeColumn.Picklist.Count > 0) then
              CellText := GetValueFromPicklist else
              CellText := GetWideDisplayText(Field);

        HighLight := HighlightCell(ACol, ARow, CellText, AState);

        TaskLevel := max(0, Level);
        HasChilds := HasChildren;
        if HasChilds then
          IsExpanded := Expanded;

        // Painting last row in the grid
        if ARow=DataLink.RecordCount then
          NextLevel := 0

        else if dgTitles in Self.Options then
        begin
          DataLink.ActiveRecord := ARow;
          NextLevel := Level;
          DataLink.ActiveRecord := ARow-1;
        end

        else
        begin
          DataLink.ActiveRecord := ARow+1;
          NextLevel := Level;
          DataLink.ActiveRecord := ARow;
        end;
{
        if (State=dsInsert) and (ARow=SavedActiveRecord) then
          //
          // Next record is in insert state
          //
        begin
          DataLink.ActiveRecord := SavedActiveRecord;
          NextLevel := Level;
          DataLink.ActiveRecord := ARow-1;
        end
        else
        begin
          Node := RecordNode;
          if Node<>nil then
          begin
            Node := ActiveCursor.NextNode(Node);
            if Node <> ActiveCursor.EndNode then
              NextLevel := ActiveCursor.Level(Node) else
              NextLevel := 0;
          end
          else
            //
            // row is in insert state
            //
          begin
            if ARow=DataLink.RecordCount then
              NextLevel := 0
            else
            begin
              NextLevel := TaskLevel;
              if (InsertPosition=ipInsertAsChild) then
                inc(NextLevel);
            end;
          end;
        end;
 }
        with Canvas do
        begin
          if Assigned(TreeScheme) and not (coUseColumnFont in TreeScheme.Options) then
            Font := TreeScheme.Font else
            Font := AColumn.Font;

          if HighLight then
          begin
            if gdFocused in AState then
            begin
              Brush.Color := clHighlight;
              Font.Color := clHighlightText;
            end
            else
            begin
              Brush.Color := clInactiveBorder;
            end;
          end
          else if Assigned(TreeScheme) and not (coUseColumnColor in TreeScheme.Options) then
            Brush.Color := TreeScheme.Color
          else
            Brush.Color := AColumn.Color;

          DrawRect := ARect;

          if Assigned(TreeScheme) and TreeScheme.HierarchyCell then
          begin
            FillTreeCell(TreeColumn, TreeScheme, Canvas, ACol, ARow, TaskLevel, NextLevel, DrawRect, HighLight, AState);

            if Assigned(SchemeCollection.ButtonImages) then
              //
              // Paint expand bitmaps
              //
            begin
              if not HasChilds then
                ImageIndex := TreeScheme.FExpandButtons.Grayed
              else if IsExpanded then
                ImageIndex := TreeScheme.FExpandButtons.Collapse
              else
                ImageIndex := TreeScheme.FExpandButtons.Expand;

              if (ImageIndex <> -1) and (ImageIndex < SchemeCollection.ButtonImages.Count) then
                SchemeCollection.ButtonImages.Draw(Canvas,
                  DrawRect.Left + TreeScheme.CellPadding,
                  DrawRect.Top + (DrawRect.Bottom - DrawRect.Top) div 2 - SchemeCollection.ButtonImages.Height div 2,
                  ImageIndex, True);
            end;
          end else
            FillRect(DrawRect);

          if Length(CellText)>0 then
          begin
            if not Enabled then
              TextState := tsDisabled
            else if HighLight and (gdFocused in AState) then
              TextState := tsHighLight
            else
              TextState := tsNormal;

            RCopy := DrawRect;
            if Assigned(TreeScheme) then
            begin
              InflateRect(RCopy, -TreeScheme.CellPadding, 0);
              inc(RCopy.Left, TreeScheme.TextIndent);
            end else
              InflateRect(RCopy, -2, 0);

            Flags := DT_Flags[TreeColumn.Alignment] or DEFAULT_FLAGS or DT_VCENTER;
            EzRenderText(Canvas, RCopy, CellText, TextState, Flags);
          end;

          if not (csDesigning in ComponentState) and
            (gdFocused in AState) and
            ([dgEditing, dgAlwaysShowEditor] * Self.Options <> [dgEditing, dgAlwaysShowEditor]) and
            not (dgRowSelect in Self.Options) and
            IsActiveControl
          then
            DrawFocusRect(DrawRect);
        end;
      end;

      // Call event handler
      DrawColumnCell(ARect, ACol, TreeColumn, AState);
    end;
  end
end;

procedure TCustomEzDBTreeGrid.FillTreeCell(
  TreeColumn: TEzTreeGridColumn;
  TreeScheme: TEzTreeGridScheme;
  ACanvas: TCanvas;
  ACol, ARow: Integer;
  CellLevel, NextLevel: SmallInt;
  var ARect: TRect; HighLight: Boolean; AState: TGridDrawState);
var
  c, Indent:integer;
  DrawRect: TRect;

  procedure GetLevelSchemes;
  var
    lvl: Integer;

  begin
    with TCustomEzDataset(Datalink.DataSet) do
    begin
      StartDirectAccess([cpShowAll], True);
      try
        FTreelevelSchemes.Count := max(CellLevel+2, FTreelevelSchemes.Count);

        lvl := CellLevel;
        // On inserted record?
        if (ARow-1=SavedActiveRecord) and (State=dsInsert) then
        begin
          ActiveCursor.CurrentNode := InsertNode;
          FTreelevelSchemes[lvl] := TreeScheme;
          dec(lvl);
          if (InsertPosition<>ipInsertAsChild) then
            // Next scheme to load is parent's scheme
            ActiveCursor.MoveParent;
        end
        else
        begin
          ActiveCursor.CurrentNode := RecordNode;
          FTreelevelSchemes[lvl] := TreeScheme;
          // Next scheme to load is parent's scheme
          ActiveCursor.MoveParent;
          dec(lvl);
        end;

        // Add higher level schemes
        while not ActiveCursor.Bof do
        begin
          FTreelevelSchemes[lvl] := TEzTreeGridScheme(InternalGetColumnScheme(TreeColumn, -1, []));
          dec(lvl);
          ActiveCursor.MoveParent;
        end;
      finally
        EndDirectAccess;
      end;
    end;
  end;

begin
  ACanvas.Pen.Color := clLtGray;
  DrawRect := ARect;

  FTreelevelSchemes.Clear;

  // By default, we remove the gridline
  if dgRowLines in Options then
    inc(DrawRect.Bottom, GridLineWidth);

{
  if NextLevel > CellLevel then
    GetLevelSchemes;
}

  if (NextLevel > CellLevel) or (CellLevel>0) {and (tsUseHierarchyColors in TreeScheme.TreeOptions)} then
    GetLevelSchemes else
    FTreelevelSchemes.Add(TreeScheme);

  c:=0;
  while c <= CellLevel do
  begin
    if c > 0 then
      // Draw vertical grid line
    begin
      ACanvas.MoveTo(DrawRect.Left, DrawRect.Top);
      ACanvas.LineTo(DrawRect.Left, DrawRect.Bottom);
      inc(DrawRect.Left);
    end;

    if Assigned(FTreelevelSchemes[c]) then
      Indent := TEzTreeGridScheme(FTreelevelSchemes[c]).Indent else
      Indent := 0;

    if c = CellLevel then
    begin
      if not HighLight then
        ACanvas.Brush.Color := TreeScheme.Color

      else if gdFocused in AState then
        ACanvas.Brush.Color := clHighLight

      else
       ACanvas.Brush.Color := clInactiveBorder;

      // We might have to draw this cell in two steps because in the first part
      // the gridline must be removed.
      if (NextLevel > CellLevel) and (dgRowLines in Options) then
      begin
        DrawRect.Right := DrawRect.Left + Indent;
        ACanvas.FillRect(DrawRect);
        dec(DrawRect.Bottom, GridLineWidth);
      end;
      DrawRect.Right := ARect.Right;
    end
    else
    begin
      if Assigned(FTreelevelSchemes[c]) then
        ACanvas.Brush.Color := TEzTreeGridScheme(FTreelevelSchemes[c]).Color else
        ACanvas.Brush.Color := TreeColumn.Color;

      DrawRect.Right := DrawRect.Left + Indent;
    end;

    // Do not remove gridline for the next levels
    if C = NextLevel then
    begin
      if (dgRowLines in Options) then
        dec(DrawRect.Bottom, GridLineWidth);
      NextLevel := -1;
    end;

    ACanvas.FillRect(DrawRect);

    if c < CellLevel then
      DrawRect.Left := DrawRect.Right;

    inc(c);
  end;

  ARect.Left := DrawRect.Left;
end;

{
procedure TCustomEzDBTreeGrid.FillTreeCell(
  ACanvas: TCanvas;
  Level, NextLevel: SmallInt;
  var ARect: TRect; CellColor: TColor; UseLevelColors, HighLight: Boolean);
var
  C:integer;
  DrawRect: TRect;
begin
  ACanvas.Pen.Color := clLtGray;

  DrawRect := ARect;

  // By default, we remove the gridline
  if dgRowLines in Options then
    inc(DrawRect.Bottom, GridLineWidth);

  c:=0;
  while c <= Level do
  begin
    if c > 0 then
      // Draw vertical grid line
    begin
      ACanvas.MoveTo(DrawRect.Left, DrawRect.Top);
      ACanvas.LineTo(DrawRect.Left, DrawRect.Bottom);
      inc(DrawRect.Left);
    end;

    if HighLight and (c=Level) or not UseLevelColors then
      ACanvas.Brush.Color := CellColor else
      ACanvas.Brush.Color := GridLevels[C].LevelColor;

    if C = Level then
    begin
      // We might have to draw this cell in two steps because in the first part
      // the gridline must be removed.
      if (NextLevel > Level) and (dgRowLines in Options) then
      begin
        DrawRect.Right := DrawRect.Left + GridLevels[C].Indent;
        ACanvas.FillRect(DrawRect);
        dec(DrawRect.Bottom, GridLineWidth);
      end;
      DrawRect.Right := ARect.Right;
    end
    else
      DrawRect.Right := DrawRect.Left + GridLevels[C].Indent;

    // Do not remove gridline for the next levels
    if C = NextLevel then
    begin
      dec(DrawRect.Bottom, GridLineWidth);
      NextLevel := -1;
    end;

    ACanvas.FillRect(DrawRect);

    if c <> Level then
      DrawRect.Left := DrawRect.Right;

    inc(c);
  end;

  ARect.Left := DrawRect.Left;
end;
}
function TCustomEzDBTreeGrid.GetColumnScheme(AColumn: TCustomEzColumn;
  ARow: Longint; AState: TGridDrawState) : TCustomEzScheme;
var
  Col: TEzTreeGridColumn;
  Scheme: TSchemeIndex;

begin
  Col := AColumn as TEzTreeGridColumn;
  Result := nil;

  if Assigned(Datalink.DataSet) and Assigned(SchemeCollection) and
     Datalink.DataSet.Active and not Datalink.DataSet.IsEmpty
  then
  begin
    with TCustomEzDataset(Datalink.DataSet) do
      if SelfReferencing then
        Scheme := Col.GetSchemeIndex(NodeType) else
        Scheme := Col.GetSchemeIndex(Level);

    if (Scheme <> -1) and (Scheme < SchemeCollection.Schemes.Count) then
      Result := SchemeCollection.Schemes[Scheme];
  end;
end;

procedure TCustomEzDBTreeGrid.GetHitTestInfoAt(X, Y: Integer; var HitInfo: TEzGridHitInfo);
var
  ARow: Integer;
  ACol: Integer;
  OldActive: Integer;
  GridLineWidth: Integer;
  GridColumn: TEzTreeGridColumn;
  Scheme: TEzTreeGridScheme;
  ExpandRect: TRect;

begin
  ZeroMemory(@HitInfo, SizeOf(HitInfo));
  HitInfo.HitWhere := Point(X,Y);
  HitInfo.Coordinate := MouseCoord(X, Y);
  if HitInfo.Coordinate.X = -1 then
  begin
    Include(HitInfo.HitPositions, hpNowhere);
    Exit;
  end;

  HitInfo.HitCellRect :=
    CellRect(HitInfo.Coordinate.X, HitInfo.Coordinate.Y);

  if HitInfo.Coordinate.Y < FixedRows then
  begin
    Include(HitInfo.HitPositions, hpOnTitlebar);
    Exit;
  end;

  if HitInfo.Coordinate.X < IndicatorOffset then
  begin
    Include(HitInfo.HitPositions, hpOnIndicator);
    Exit;
  end;

  with HitInfo.HitCellRect do
    if PtInRect(Rect(Left, Top, Right, Top+CellHitMargin+1), HitInfo.HitWhere) then
      Include(HitInfo.HitPositions, hpOnCellTopBorder)
    else if PtInRect(Rect(Left, Bottom-CellHitMargin, Right, Bottom+1), HitInfo.HitWhere) then
      Include(HitInfo.HitPositions, hpOnCellBottomBorder)
    else
      Include(HitInfo.HitPositions, hpOnCell);

  ARow := HitInfo.Coordinate.Y-FixedRows;

  if Datalink.DataSet.Active then
  begin
    OldActive := 0;
    with Datalink.DataSet as TEzDataset do
    try
      OldActive := DataLink.ActiveRecord;
      DataLink.ActiveRecord := ARow;
      HitInfo.HitNode := RecordNode;
      Include(HitInfo.HitPositions, hpOnRow);

      if Assigned(SchemeCollection) and Assigned(SchemeCollection.ButtonImages) then
      begin
        ACol := HitInfo.Coordinate.X-IndicatorOffset;

        GridColumn := Columns[ACol] as TEzTreeGridColumn;
        Scheme := TEzTreeGridScheme(InternalGetColumnScheme(GridColumn, HitInfo.Coordinate.Y, []));

        if Assigned(Scheme) and Scheme.HierarchyCell then
        begin
          if ((State <> dsInsert) or (ARow <> OldActive)) and HasChildren then
          begin
            GridLineWidth := 1; // Fixed value for now!
            ExpandRect := HitInfo.HitCellRect;
            StartDirectAccess([cpShowAll], True);
            try
              ActiveCursor.MoveParent;
              while not ActiveCursor.Bof do
              begin
                Inc(ExpandRect.Left, GridLineWidth + TEzTreeGridScheme(InternalGetColumnScheme(GridColumn, -1, [])).Indent);
                ActiveCursor.MoveParent;
              end;
            finally
              EndDirectAccess;
            end;

            Inc(ExpandRect.Left,2);
            ExpandRect.Right := ExpandRect.Left+SchemeCollection.ButtonImages.Width;
            if PtInRect(ExpandRect, HitInfo.HitWhere) then
              Include(HitInfo.HitPositions, hpOnExpandButton);
          end;
        end;
      end;
    finally
      DataLink.ActiveRecord := OldActive;
    end;
  end;
end;

procedure TCustomEzDBTreeGrid.LinkActive(Value: Boolean);
begin
  if Value and (DataSource <> nil) and
    (DataSource.DataSet <> nil) and not (DataSource.DataSet is TEzDataSet)
  then
    EzGridError(SInvalidDataset, Self);

  inherited;
end;


procedure TCustomEzDBTreeGrid.MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer);

  procedure UpdateDatasetCursor;
  var
    Move: Integer;
  begin
    // Let dataset follow mouse cursor
    Move := FMouseDownHitInfo.Coordinate.Y-FixedRows-Datalink.ActiveRecord;
    if Move <> 0 then
      Datalink.Dataset.MoveBy(Move);
  end;

begin
  if (csDesigning in ComponentState) or (Datalink = nil) or not Datalink.Active then
  begin
    inherited;
    Exit;
  end;

  GetHitTestInfoAt(X, Y, FMouseDownHitInfo);

  if ([hpOnExpandButton, hpOnRow] * FMouseDownHitInfo.HitPositions) = [hpOnExpandButton, hpOnRow] then
    //
    // Expand button clicked, do not call 'inherited' to prevent editor from showing.
    //
    with Datalink.DataSet as TEzDataset do
    begin
      UpdateDatasetCursor;
      Expanded := not Expanded;
    end
  else
    inherited;

  // TDBGrid does not call OnMouseDown event so we do it ourselves.
  if Assigned(OnMouseDown) then OnMouseDown(Self, Button, Shift, X, Y);
end;

procedure TCustomEzDBTreeGrid.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  if not Dragging and (FDragSupport <> []) and (ssLeft in Shift) and
     (hpOnRow in FMouseDownHitInfo.HitPositions) and
     ((abs(FMouseDownHitInfo.HitWhere.X - X) > FDragThreshold) or (abs(FMouseDownHitInfo.HitWhere.Y - Y) > FDragThreshold))
  then
  begin
    FDragStartInfo := FMouseDownHitInfo;
    BeginDrag(true, 0);
  end else
    inherited;
end;

procedure TCustomEzDBTreeGrid.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (AComponent = SchemeCollection) then
    SchemeCollection := nil;
end;

procedure TCustomEzDBTreeGrid.RunTimer(idTimer, Interval: integer);
begin
  if not (csDesigning in ComponentState) and HandleAllocated then
    SetTimer(Handle, idTimer, Interval, nil);
end;

procedure TCustomEzDBTreeGrid.SetSchemeCollection(Value: TEzTreeGridSchemes);
begin
  if SchemeCollection <> Value then
  begin
    FSchemesLink.Schemes := Value;
    Invalidate;
  end;
end;

procedure TCustomEzDBTreeGrid.StopTimer(idTimer: integer);
begin
  if HandleAllocated and not (csDesigning in ComponentState) then
    KillTimer(Handle, idTimer);
end;

procedure TCustomEzDBTreeGrid.WMTimer(var Msg: TWMTimer);

  procedure DoDragDropScroll;
  var
    Pos: TPoint;
  begin
    if Dragging then
    begin
      Pos := ScreenToClient(Mouse.CursorPos);

      if ((dgTitles in Options) and (Pos.y>0) and (Pos.y<FTitlebarHeight)) or
         (not (dgTitles in Options) and (Pos.y<=4) and (Pos.y>-2))
      then
      begin
        Datalink.ActiveRecord := 0;
        Datalink.Dataset.MoveBy(-1);
      end
      else if (Pos.y>=ClientRect.Bottom-1) and (Pos.y<ClientRect.Bottom+2) then
      begin
        Datalink.ActiveRecord := Datalink.RecordCount-1;
        Datalink.Dataset.MoveBy(1);
      end;
    end;
  end;

begin
  if Msg.TimerID = tmrDragDropScroll then
    DoDragDropScroll else
    inherited;
end;


//=------------------ Drag and drop functions ----------------=
procedure TCustomEzDBTreeGrid.CMDrag(var Message: TCMDrag);
var
  effect: LongInt;
  S: TObject;

begin
  effect := 0;
  with Message, DragRec^ do
  begin
    if Source is TBaseDragControlObject then
      S := (Source as TBaseDragControlObject).Control else
      S := Source;

    case DragMessage of
      dmDragEnter, dmDragLeave, dmDragMove:
      begin
        DragOver(S, Pos.X, Pos.Y, DragMessage, effect);
        Message.Result := effect;
      end;

      dmDragDrop:
      begin
        DragDrop(S, Pos.X, Pos.Y, DragMessage, effect);
        Message.Result := effect;
      end;

      dmFindTarget:
      begin
        Result := Integer(ControlAtPos(ScreenToClient(Pos), False));
        if Result = 0 then
          Result := Integer(Self);
      end;
    end;
  end;
end;

procedure TCustomEzDBTreeGrid.DragOver(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: Longint);
var
  Accept: Boolean;
  Operations: TEzGridDragOperations;
  L: Integer;
begin
  ClearDragEffects;

  Effect := DROPEFFECT_NONE;
  DragCursor := crDrag;

  if (Source = Self) then
  begin
    // Gather information about current drag position
    DragPositionInfo(Source, DragMessage, X, Y, FDropTargetInfo, Operations);
    Accept := Operations <> [];
  end else
    Accept := false;

  // Call event handler
  DoDragOver(Source, TDragState(DragMessage), Point(X, Y), FDropTargetInfo, Operations, Accept);

  if Accept then
  begin
    Effect := Effect or DROPEFFECT_COPY or DROPEFFECT_MOVE;

    if DragMessage <> dmDragLeave then
    with Canvas do
    begin
      Pen.Color := clBlue;
      Pen.Style := psSolid;
      Pen.Mode := pmNotXor;
      Pen.Width := 2;

      if dgIndicator in Options then
        L:=ColWidths[0] else
        L:=0;

      if doInsertTask in Operations then
      begin
        FDragEffect := doInsertTask;
        DragCursor := crInsertBefore;
        MoveTo(L, FDropTargetInfo.HitCellRect.Top-1);
        LineTo(GridWidth, FDropTargetInfo.HitCellRect.Top-1);
      end
      else if doInsertTaskAfter in Operations then
      begin
        FDragEffect := doInsertTaskAfter;
        DragCursor := crInsertAfter;
        MoveTo(L, FDropTargetInfo.HitCellRect.Bottom);
        LineTo(GridWidth, FDropTargetInfo.HitCellRect.Bottom);
      end
      else if doInsertAsChild in Operations then
        //
        // doInsertAsChild
        //
      begin
        FDragEffect := doInsertAsChild;
        DragCursor := crInsertAsChild;
        Pen.Color := $E0E0E0;
        Brush.Color := $E0E0E0;
        Rectangle(L, FDropTargetInfo.HitCellRect.Top, GridWidth, FDropTargetInfo.HitCellRect.Bottom);
      end;
      Pen.Mode := pmCopy;
    end;
  end;
end;

procedure TCustomEzDBTreeGrid.DragDrop(Source: TObject; X, Y: integer; DragMessage: TDragMessage; var Effect: LongInt);

// Handles dmDragDrop message:
// Calculates HitInformation and then links or moves tasks

var
  Accept: Boolean;
  Operations: TEzGridDragOperations;

begin
  Effect := DROPEFFECT_NONE;
  ClearDragEffects;

  // Gather information about drop position
  DragPositionInfo(Source, DragMessage, X, Y, FDropTargetInfo, Operations);

  Accept := (Source = Self) and (Operations <> []);

  // Call event handler
  DoDragDrop(Source, TDragState(DragMessage), Point(X, Y), FDropTargetInfo, Operations, Accept);

  if Accept then
    with (Datalink.Dataset as TEzDataset) do
    begin
      if doInsertTask in Operations then
      begin
        MoveRecord(FDragStartInfo.HitNode, FDropTargetInfo.HitNode, ipInsertBefore);
        RecordNode := FDragStartInfo.HitNode;
      end
      else if doInsertTaskAfter in Operations then
      begin
        MoveRecord(FDragStartInfo.HitNode, FDropTargetInfo.HitNode, ipInsertAfter);
        RecordNode := FDragStartInfo.HitNode;
      end

      else if doInsertAsChild in Operations then
      begin
        MoveRecord(FDragStartInfo.HitNode, FDropTargetInfo.HitNode, ipInsertAsChild);
        RecordNode := FDragStartInfo.HitNode;
      end;
    end;
end;

procedure TCustomEzDBTreeGrid.DragPositionInfo(
      Source: TObject;
      DragMessage: TDragMessage;
      X, Y: Integer;
      var HitInfo: TEzGridHitInfo;
      var Operations: TEzGridDragOperations);

var
  Pos: TPoint;

  procedure TryMoveTask;
  begin
    with (Datalink.Dataset as TEzDataset) do
      if (dsAllowChangeParent in DragSupport) then
      begin
        if (Source <> Self) then
          Include(Operations, doInsertAsChild)

        else if (HitInfo.HitNode <> FDragStartInfo.HitNode) then
        begin
          if
            // A parent cannot be dropped on one of its childs
            ActiveCursor.NodeHasParent(HitInfo.HitNode, FDragStartInfo.HitNode) or
            // A child cannot be dragged onto its parent.
            (FDragStartInfo.HitNode.Parent = HitInfo.HitNode)
          then
            Exit;

          Include(Operations, doInsertAsChild);
        end;
        {
        else
          //
          // Hovering over the same record ==> test for indent, outdent
          //
        begin
          if FDropTargetInfo.HitWhere.X > FDragStartInfo.HitWhere.X+5 then
            Include(Operations, doIndentTask);
        end;
        }
      end;
  end;

  procedure TryMoveLast;
  var
    LastNode: TRecordNode;

  begin
    with (Datalink.Dataset as TEzDataset) do
      if (dsReorderTask in DragSupport) and not ActiveCursor.IsEmpty then
      begin
        // Move task beneeth last record as a root record
        LastNode := ActiveCursor.EndNode.Prev;
        if (FDragStartInfo.HitNode <> LastNode) then
        begin
          Include(Operations, doInsertTaskAfter);
          HitInfo.HitPositions := [hpNoWhere, hpOnRow, hpOnCellBottomBorder];
          HitInfo.HitNode := LastNode;
        end;
      end;
  end;

  procedure TryReorderTask;
  var
    AdjacentNode: TRecordNode;

  begin
    with (Datalink.Dataset as TEzDataset) do
      if Source <> Self then
      begin
        if hpOnCellTopBorder in HitInfo.HitPositions then
          Include(Operations, doInsertTask)

        else if HitInfo.HitNode.Child <> nil then
          Include(Operations, doInsertAsChild)

        else
          Include(Operations, doInsertTaskAfter);
      end
      else if (HitInfo.HitNode <> FDragStartInfo.HitNode) then
      begin
        if ActiveCursor.NodeHasParent(HitInfo.HitNode, FDragStartInfo.HitNode) then
          // A parent cannot be dropped on one of its childs
          Exit;

        if (HitInfo.HitNode.Parent <> FDragStartInfo.HitNode.Parent) and
           // A child cannot be dragged onto its parent.
           (FDragStartInfo.HitNode.Parent <> HitInfo.HitNode)
         then
          Include(Operations, doInsertAsChild);

        if hpOnCellTopBorder in HitInfo.HitPositions then
        begin
          if HitInfo.HitNode.Prev = FDragStartInfo.HitNode then
            // Cannot insert a task before the next task because node
            // is already in this position.
            Exit;

          AdjacentNode := ActiveCursor.PrevNode(HitInfo.HitNode);
        end
        else
        begin
          if HitInfo.HitNode.Next = FDragStartInfo.HitNode then
            // Cannot insert a task after the previous task because node
            // is already in this position.
            Exit;

          AdjacentNode := ActiveCursor.NextNode(HitInfo.HitNode);
        end;

        if AdjacentNode = ActiveCursor.TopNode then
          //
          // Hovering over top border of the first record.
          // Insert can only take place before the first record.
          //
          Include(Operations, doInsertTask)

        else if AdjacentNode.Parent = HitInfo.HitNode.Parent then
          //
          // Nodes share the same parent, Insert can only take place between the
          // two nodes.
          //
        begin
          Include(Operations, doInsertTaskAfter);
          if hpOnCellTopBorder in HitInfo.HitPositions then
          begin
            dec(HitInfo.Coordinate.Y);
            HitInfo.HitCellRect :=
              CellRect(HitInfo.Coordinate.X, HitInfo.Coordinate.Y);
            HitInfo.HitNode := AdjacentNode;
          end;
        end
        else
          //
          // Nodes do not share the same parent.
          // 1:  T1
          //        T2
          //
          // 2:     T2
          //     T3
          //
        begin
          if hpOnCellTopBorder in HitInfo.HitPositions then
            Include(Operations, doInsertTask)
          else
          if (hpOnCellBottomBorder in HitInfo.HitPositions) and
             (FDragStartInfo.HitNode.Parent <> HitInfo.HitNode)
          then
            Include(Operations, doInsertTaskAfter);
        end;
      end;
  end;

begin
  Operations := [];

  if (DragSupport * [dsAllowChangeParent, dsReorderTask] <> []) then
  begin
    Pos := ScreenToClient(Point(X,Y));
    GetHitTestInfoAt(Pos.X, Pos.Y, HitInfo);

    if hpNowhere in HitInfo.HitPositions then
      TryMoveLast

    else if hpOnRow in HitInfo.HitPositions then
    begin
      if (HitInfo.HitPositions * [hpOnCellTopBorder, hpOnCellBottomBorder] <> []) and
         (dsReorderTask in DragSupport)
      then
        TryReorderTask;

      if HitInfo.HitPositions * [hpOnCell, hpOnIndicator] <> [] then
        TryMoveTask;
    end;
  end;
end;

procedure TCustomEzDBTreeGrid.DoDragOver(
      Source: TObject; State: TDragState;
      Pt: TPoint; HitInfo: TEzGridHitInfo;
      var Operations: TEzGridDragOperations;
      var Accept: Boolean);

  // Handle OnDragOver event

begin
  if Assigned(FOnDragOver) then
    FOnDragOver(Self, Source, State, Pt, HitInfo, Operations, Accept);
end;

procedure TCustomEzDBTreeGrid.DoDragDrop(
      Source: TObject; State: TDragState;
      Pt: TPoint; HitInfo: TEzGridHitInfo;
      var Operations: TEzGridDragOperations;
      var Accept: Boolean);

  // Handle OnDragDrop event

begin
  if Assigned(FOnDragDrop) then
    FOnDragDrop(Self, Source, State, Pt, HitInfo, Operations, Accept);
end;

procedure TCustomEzDBTreeGrid.DoEndDrag(Target: TObject; X, Y: Integer);
begin
  StopTimer(tmrDragDropScroll);
  inherited DoEndDrag(Target, X, Y);

  if (FDragObject<>nil) and (FDragObject.Control=Self) then
    FreeAndNil(FDragObject);
end;

procedure TCustomEzDBTreeGrid.DoStartDrag(var DragObject: TDragObject);
begin
  inherited DoStartDrag(DragObject);

  if Datalink.Dataset.State in [dsEdit, dsInsert] then Datalink.Dataset.Post;
  EditorMode := False;

  if DragObject = nil then
  begin
    FDragObject := TDragControlObject.Create(Self);
    DragObject := FDragObject;
  end;

  RunTimer(tmrDragDropScroll, 100);
end;
//=------------------ End of Drag and drop functions ----------------=

end.
