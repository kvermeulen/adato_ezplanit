unit EzDataset;

{$I Ez.inc}
{$R-} {turn off range checking}

interface

uses
  Classes,
  SysUtils, Db, Dialogs, Math,
  EzParser,
  EzDB,
  EzVirtualDataset
{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
;
{$ENDIF}

const
  NO_REF = -10;
  ROOT_REF = -11;
  PARENT_REF = -12;
  deGetMaxPosition = TDataEvent(Ord(High(TDataEvent))+1);
  deBufferCountChanged = TDataEvent(Ord(deGetMaxPosition)+1);

type
  EEzDBCursorError = class(Exception);
  EFormulaError = class(Exception);
  EEzDatasetError = class(Exception);

  TCustomEzDataset = class;
  TEzDataset = class;
  PRecordNode = ^TRecordNode;
  TRecordNode = class;
  TEzDBCursor = class;
  TEzDataLink = class;
  TEzDataLinks = class;

  PBookmark = ^TBookmark;

  PRecInfo = ^TRecInfo;
  TRecInfo = record
    Bookmark: TRecordNode;
    BookmarkFlag: TBookmarkFlag;
    Level: Integer;
  end;

  TEzDBState = (
    esFastSort,         // Used while sorting to indicate that fast sorting is enabled,
                        // that is no formulas need te be evaluated
    esDefaultCursorLoading, // Flag indicating that the default cursor is loading
    esCursorLoading,    // Flag indicating that a cursor is loading it's data
    esResyncPending     // A resync was requested but is not performed yet
  );
  TEzDBStates = set of TEzDBState;

  TEzDBOption = (
    // Delete all child records when a parent is deleted
    eoCascadeDelete,
    // If set, TEzDataset expects that the dataset's to which it
    // is connected are configured with a master-detail relation
    // (like two TTable datasets connected using the MasterSource and
    // MasterFields properties).
    // TEzDataset will access the detail datasets differently depeding
    // on the state of this flag. When the datasets have a MD relation,
    // TEzDataset will relocate the master table before relocation
    // the detail table because otherwise the child record might not be
    // in view. If the detail tables have no MD relation, TEzDataset can
    // freely move the detail table without worrying about the master
    // table.
    // You should only use this option if you have no other choice than
    // connecting to tables having a MD between them.
    eoUseMDRelations,
    // This flags tells TEzDataset to verify that a child record has
    // a valid value stored in it's parent reference field before
    // adding the record to the default cursor.
    // If this flags is set, and the child record has no value
    // stored in it's parent field, an exception will be raised.
    // If this flag is not set and the child record has no value
    // stored in it's parent field, then the Default cursor will
    // decide what to do (see TEzDbCursor.Options).
    eoParentKeysRequired,
    // This option tells TEzDataset that it should generate
    // a unique key value when the detail dataset does not provide
    // one.
    // TEzDataset requires that newly created records are given a
    // unique key when they are inserted in the detail dataset.
    // However not all data access components return a unique
    // key value right away. For example TClientDataset first
    // stores inserted records in memory before actually inserting
    // them into the database. As long as the updates are not
    // saved to the database, identifier fields have no value.
    // This will not work for TEzDataset.
    // In such case, you can ask TEzDataset to generate temporarily
    // key values by setting this flag.
    eoGenerateKeys,
    // Set this flag to activate an extra verification step whenever
    // the cursor is moved using TEzDataset.RecordNode or
    // TEzDataset.ActiveCursor.CurrentNode.
    // This flag can be used for debugging purposes in cases where
    // you expect that the dataset is moved to an invalid
    // cursor position.
    eoCheckCursorPosition
  );
  TEzDBOptions = set of TEzDBOption;

  TEzInsertPosition = (ipUndefined, ipInsertBefore, ipInsertAfter, ipInsertAsChild, ipAppend);

  TNodeFlag = (nfExpanded, nfNew, nfAutoCreated, nfVirtual, nfShadowAfter, nfShadowBefore);
  TNodeFlags = set of TNodeFlag;

  {
    TNodeKey is the same as task_key which is used by EzPlan-IT scheduler.
  }
  PNodeKey = ^TNodeKey;
  TNodeKey = packed record
    Level: Integer;
    Value: OleVariant;
  end;

  TTwinPosition = (tpBefore, tpAfter);

  TRecordNodeTwin = packed record
    Owner: TRecordNode;
    Position: TTwinPosition;
    Twin: TRecordNode;
  end;

  TRecordNodeTwins = class(TList)
  private

  protected
    function  Get(Index: Integer): TRecordNodeTwin;
    procedure Put(Index: Integer; Value: TRecordNodeTwin);

  public
    constructor Create(AOwner: TRecordNode);
    destructor  Destroy; override;

    function Add(Value: TRecordNodeTwin): Integer;

    property Items[Index: Integer]: TRecordNodeTwin read Get write Put;
  end;

  TRecordNode = class(TObject)
  private
    FTwinsAfter: TRecordNodeTwins;
    FTwinsBefore: TRecordNodeTwins;

  public
    Next: TRecordNode;
    Child: TRecordNode;
    Prev: TRecordNode;
    Parent: TRecordNode;
    Key: TNodeKey;
    Flags: TNodeFlags;

    RecNo: Integer;
    Data: Pointer;

    constructor Create;

    property TwinsAfter: TRecordNodeTwins read FTwinsAfter;
    property TwinsBefore: TRecordNodeTwins read FTwinsBefore;
  end;

  //
  // RecordnodeArray maintains a sorted array of TRecordNode objects.
  // This array is used by TEzDbCursor for locating TRecordNode objects quickly.
  //
  TRecordnodeArray = class(TObject)
  private
    FMemory: Pointer;           { Pointer to item buffer }
    FCapacity: Integer;         { The allocated size of the array }
    FCount: Integer;            { Count of items in use }
    FGrowBy: Integer;

    function  GetIndexPtr(index: Integer): Pointer;
    procedure SetCount(NewCount: Integer);

  protected
    procedure InternalHandleException;
    procedure SetCapacity(NewCapacity: Integer); virtual;
    function  GetNode(index: Integer) : TRecordNode;
    function  ValidIndex(Index: Integer): Boolean;
    procedure PutNode(index: Integer; Node: TRecordNode);

  public
    constructor Create(itemcount, Grow: Integer); virtual;
    destructor Destroy; override;

    procedure Add(Node: TRecordnode);
    procedure Clear;
    procedure Delete(Index: Integer);
    procedure DeleteKey(Key: TNodeKey);
    function  FindNodeByKey(Key: TNodeKey): TRecordNode;
    function  FindKey(var Index: Integer; Key: TNodeKey): Boolean;

    property Count: Integer read FCount write SetCount;
    property Nodes[Index: Integer]: TRecordNode read GetNode write PutNode; default;
  end;

  //
  // TNodeType identifies the type of node within a TEzDbCursor.
  //
  TNodeType = (
      ntSingle,   // A node with no parent(s) and no child(s)
      ntRoot,     // A node with no parent(s) and one or more child(s)
      ntParent,   // a node with one or more parent(s) and one or more child(s)
      ntChild     // a node with one or more parent(s) and no child(s)
  );
  TNodeTypes = set of TNodeType;

  TCursorOption = (
      // Create a parent node when a node is added for which the
      // parent does not (yet) exists.
      cpAutoCreateParents,
      // Ignore the node when it is added and the parent does
      // not (yet) exists.
      cpIgnore,
      // Show all nodes when walking through the cursor, regardless
      // of expanded state of node.
      cpShowAll,
      // Cursor is sorted on IndexFields
      cpSorted,
      // Cursor is filtered
      cpFiltered,
      // Added nodes are expanded by default
      cpDefExpanded,
      // Expanded state is inherited from DefaultCursor when this
      // secondary cursor is loaded.
      cpInheritsExpanded,
      // Expanded state is inherited from DefaultCursor when this
      // secondary cursor is loaded.
      cpSortCaseSensitive
  );
  TCursorOptions = set of TCursorOption;

  TSortDirection = (sdDescending, sdAscending);

  TFilterNodeEvent = procedure(Cursor: TEzDBCursor; Node: TRecordNode; var Accept: Boolean) of object;

  TEzDBCursor=class(TCollectionItem)
  private
    FTopNode: TRecordNode;
    FEndNode: TRecordNode;
    FIndexFieldNames: string;
    FCursorName: string;
    FCurrentNode: TRecordNode;
    FOptions: TCursorOptions;
    FRecordCount: Integer;
    FRecNoValid: Boolean;
    FIsClearing: Boolean;
    FSortedKeys: TRecordnodeArray; { Stores recordnodes sorted on key }
    FSortDirection: TSortDirection;

  protected
    function  CompareNodes(N1, N2: TRecordNode): Integer; virtual;
    function  CreateNode(Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode; virtual;
    procedure FreeNode(var Node: TRecordNode);
    function  GetActive: Boolean;
    function  GetAbsCount: Integer;
    function  GetRecNo: Integer;
    function  GetBof: Boolean;
    function  GetChildCount: Integer;
    function  GetDisplayName: string; override;
    function  GetEof: Boolean;
    function  GetExpanded: Boolean;
    function  GetDataset: TCustomEzDataset;
    function  GetKey: TNodeKey;
    function  GetParentKey: TNodeKey;
    function  GetRecordCount: Integer;
    procedure InitRecordNumbers;
    procedure InternalAddNode(Position, NewNode: TRecordNode);

    procedure ReIndexNode(Node: TRecordNode);
    procedure SetCurrentNode(Node: TRecordNode);
    procedure SetCursorName(Name: string);
    procedure SetExpanded(Value: Boolean);
    procedure SetIndexFieldNames(Value: string);
    procedure SetKey(Key: TNodeKey);
    procedure SetParentKey(Key: TNodeKey);
    procedure SetOptions(O: TCursorOptions);
    procedure SetSortDirection(Value: TSortDirection);
    procedure SetRecNo(Value: Integer);

  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function  AbsParent: TRecordNode; overload;
    function  AbsParent(Node: TRecordNode): TRecordNode; overload;

    // Insert / add node functions
    function  Add(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode; overload;
    function  Add(Parent, Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode; overload;
    function  AddChild(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
    function  AddChildFirst(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
    function  Insert(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;

    function  Advance(Node: TRecordNode; Distance: Integer) : TRecordNode;
    function  BookmarkValid(Bookmark: TRecordNode) : Boolean;
    function  Childs(Node: TRecordNode): Integer;
    procedure Clear;
    procedure CloseCursor;
    procedure CutOffNode(Node: TRecordNode);
    procedure Delete;
    procedure DeleteNode(Node: TRecordNode);
    function  Distance(Node1, Node2: TRecordNode): Integer;
    function  Exists(Key: TNodeKey) : Boolean; overload;
    function  Exists(Node: TRecordNode) : Boolean; overload;
    function  FindNode(Key: TNodeKey): TRecordNode;
    function  FindNearest(Key: TNodeKey): TRecordNode;
    function  GetParent(Node: TRecordNode): TRecordNode;
    function  HasChildren: Boolean; overload;
    function  HasChildren(Node: TRecordNode): Boolean; overload;
    function  HasParent: Boolean; overload;
    function  HasParent(Node: TRecordNode): Boolean; overload;
    function  IsEmpty: Boolean;
    function  Level: Integer; overload;
    function  Level(Node: TRecordNode): integer; overload;

    function  NextNode(Node: TRecordNode): TRecordNode;
    procedure MakeNodeVisible;
    procedure MoveNode(Node, Location: TRecordNode; Position: TEzInsertPosition);
    function  NodeHasTwin(Node: TRecordNode): TNodeFlags;
    function  PrevNode(Node: TRecordNode): TRecordNode;
    function  IsDefaultCursor: Boolean;
    function  IsExpanded: Boolean;  overload;
    function  IsExpanded(Node: TRecordNode): Boolean; overload;
    function  IsVisible: Boolean;
    function  IsNodeVisible(Node: TRecordNode): Boolean;

    procedure MoveFirst;
    procedure MoveNext;
    procedure MoveNextSibling;
    procedure MoveLast;
    procedure MoveParent;
    procedure MovePrevious;
    procedure MovePrevSibling;
    procedure MoveToKey(Value: TNodeKey);
    function  NodeHasParent(Node, Parent: TRecordNode) : Boolean;
    function  NodeRecNo(Node: TRecordNode): LongInt;
    function  NodeAtIndex(Value: Integer): TRecordNode;
    function  NodeType(Node:TRecordNode): TNodeType; overload;
    function  NodeType: TNodeType; overload;
    procedure OpenCursor;
    procedure UpdateKey(CurrentKey, NewKey: TNodeKey);
    procedure UpdateParent(NewParent, Node: TRecordNode);
    function  VisibleChilds(Node: TRecordNode): Integer;

    property Active: Boolean read GetActive;
    property AbsCount: Integer read GetAbsCount;
    property Bof: Boolean read GetBof;
    property ChildCount: Integer read GetChildCount;
    property CurrentNode: TRecordNode read FCurrentNode write SetCurrentNode;
    property Dataset: TCustomEzDataset read GetDataset;
    property EndNode: TRecordNode read FEndNode;
    property Eof: Boolean read GetEof;
    property Expanded: Boolean read GetExpanded write SetExpanded;
    property Key: TNodeKey read GetKey write SetKey;
    property ParentKey: TNodeKey read GetParentKey write SetParentKey;
    property RecNo: Integer read GetRecNo write SetRecNo;
    property RecNoValid: Boolean read FRecNoValid;
    property RecordCount: Integer read GetRecordCount;
    property SortedKeys: TRecordNodeArray read FSortedKeys;
    property TopNode: TRecordNode read FTopNode;

  published
    property IndexFieldNames: string read FIndexFieldNames write SetIndexFieldNames;
    property CursorName: string read FCursorName write SetCursorName;
    property Options: TCursorOptions read FOptions write SetOptions default [];
    property SortDirection: TSortDirection read FSortDirection write SetSortDirection default sdAscending;
  end;

  TEzDBCursors = class(TCollection)
  private
    FDataset: TCustomEzDataset;

  protected
    function GetOwner: TPersistent; override;
    function GetEzDBCursor(Index: Integer): TEzDBCursor;
    procedure SetEzDBCursor(Index: Integer; Value: TEzDBCursor);

  public
    constructor Create(Dataset: TCustomEzDataset; ColumnClass: TCollectionItemClass);
    function  Add: TEzDBCursor;
    procedure EndUpdate; override;
    property Items[Index: Integer]: TEzDBCursor read GetEzDBCursor write SetEzDBCursor; default;
    property Dataset: TCustomEzDataset read FDataset;
  end;

  PEzFieldLink = ^TEzFieldLink;
  TEzFieldLink = class(TObject)
  private
    FFieldName: string;     // FieldName of field to which this link belongs.
                            // i.e. the name of the field in the TEzDataset object.
    FLink: string;          // Link information: either a field name or a formula.
    FParentLevel: integer;  // Used internally by TEzDataset to store the
                            // TEzDatalink level in case this links requires data
                            // from another datalink to which this link belongs.
    FField: TField;         // Reference to the TField object in the detail table.

  protected
    procedure ReadFieldLink(Reader: TReader);
    procedure WriteFieldLink(Writer: TWriter);

  public
    constructor Create(AName: string);

    property Field: TField read FField write FField;
    property FieldName: string read FFieldName write FFieldName;
    property Link: string read FLink write FLink;
    property ParentLevel: integer read FParentLevel write FParentLevel;
  end;

  TEzFieldLinks = class(TPersistent)
  private
    FMemory: Pointer;   // Pointer to item buffer }
    FCapacity: Integer;         // The allocated size of the array }
    FCount: Integer;            // Count of items in use }
    FUpdated: Boolean;          // Indicates whether links have be checked
                                // against dataset fields
    FDatalink: TEzDatalink;     // Owner of this links object

    function  GetIndexPtr(index: Integer): Pointer;

  protected
    procedure InternalHandleException;
    procedure SetCapacity(NewCapacity: Integer); virtual;
    function  GetCount: integer;
    function  GetEzFieldLinks(FieldName: string) : TEzFieldLink;
    function  GetItems(index: integer) : TEzFieldLink;
    function  ValidIndex(Index: Integer): Boolean;
    procedure PutEzFieldLinks(FieldName: string; ALink: TEzFieldLink);
    procedure PutItems(index: integer; ALink: TEzFieldLink);

  public
    constructor Create(Link: TEzDatalink; itemcount: Integer); virtual;
    destructor Destroy; override;

    procedure Add(ALink: TEzFieldLink); overload;
    function  Add(const FieldName: string) : TEzFieldLink; overload;
    procedure Assign(Source: TPersistent); override;
    procedure Clear;
    procedure Delete(Index: Integer);
    function  FindLink(var Index: Integer; const FieldName: string): Boolean;
    procedure Update;

    property Count: Integer read GetCount;
    property FieldLinks[FieldName: string]: TEzFieldLink read GetEzFieldLinks write PutEzFieldLinks; default;
    property Items[index: integer]: TEzFieldLink read GetItems write PutItems;
    property Updated: Boolean read FUpdated write FUpdated;
  end;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TGetVariableEvent = procedure (Dataset: TDataset; Variable: string;
                                var Result: Variant) of object;
  TCompareNodesEvent = procedure (Dataset: TDataset;
                                  Cursor: TEzDBCursor;
                                  N1, N2: TRecordNode;
                                  var Result: Integer) of object;
  TLocateRecordEvent = procedure(Datalink: TEzDatalink; const Key: TNodeKey) of object;
  TGenerateKeyEvent = procedure(Datalink: TEzDatalink; var Value: Variant) of object;
  TMoveRecordEvent = procedure (Dataset: TDataset; Node, Location: TRecordNode; Position: TEzInsertPosition) of object;
  TExpandRecordEvent = procedure (Dataset: TDataset; Node: TRecordNode) of object;

  // Used for tracking recursive formula's
  PCalculateField = ^TCalculateField;
  TCalculateField = record
    CalcRecord: TRecordNode;
    Expression: string;
  end;

  TEzDatasetParser = class(TEzParser)
  private
    Dataset: TCustomEzDataset;
  protected
    function  DoGetIdentValue(ctype: TCalcType; const S: String; var Value: Double) : Boolean; override;
    function  DoGetStrIdentValue(ctype: TCalcType; const S: String; var Value: string) : Boolean; override;
    procedure DoGetRangeValues(const sRange, sVariable: string; ValueType: TValueType; ParamList: TStringList); override;
    function  DoUserFunction(const Func: string; Params: TStringList) : Double; override;
    function  DoStrUserFunction(const Func: string; Params: TStringList) : string; override;
  public
    constructor Create(D: TCustomEzDataset); reintroduce;

  end;

  TEzDetailDatalink = class(TDataLink)
  private
    FEzDatalink: TEzDatalink;

  protected
    procedure ActiveChanged; override;
    procedure DataSetChanged; override;

  public
    constructor Create(EzDataLink: TEzDatalink);
  end;

  { TEzBlobStream }
  TEzBlobStream = class(TMemoryStream)
  private
    FField: TBlobField;
    FDataSet: TCustomEzDataSet;
    FBuffer: TRecBuf;
    FFieldNo: Integer;
    FModified: Boolean;
    FData: Variant;
    FFieldData: Variant;
  protected
    procedure ReadBlobData;
    function Realloc(var NewCapacity: Longint): Pointer; override;
  public
    constructor Create(Field: TBlobField; Mode: TBlobStreamMode);
    destructor Destroy; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    procedure Truncate;
  end;

  { TEzDataLink }
  TEzDataLink = class(TCollectionItem)
  private
    FDatalink: TEzDetailDatalink;
    FKeyField: string;
    FParentRefField: string;
    FFieldLinks: array[TNodeType] of TEzFieldLinks;
    FSyncedWith: TRecordNode;
    FCanExpand: Boolean;

  protected
    procedure   DefineProperties(Filer: TFiler); override;
    function    GetActiveFieldLinks: TEzFieldLinks;
    function    GetDisplayName: string; override;
    function    GetDataSource: TDataSource;
    function    GetDetailDataset: TDataset;
    function    GetEzDataset: TEzDataset;
    function    GetFieldLinks(NodeType: TNodeType): TEzFieldLinks;
    // Needed for BCB support
    function    GetFieldLinkIndex(IntType: Integer): TEzFieldLinks;
    function    GetOnRelocateDataset: TLocateRecordEvent;
    procedure   SetDataSource(Value: TDataSource);
    procedure   LinkActiveChanged(Active: Boolean); virtual;
    procedure   LinkChanged; virtual;
    procedure   ReadSelectedFieldLinks(Reader: TReader; Links: TEzFieldLinks);
    procedure   ReadMDFieldLinks(Reader: TReader);
    procedure   ReadSingleNodeFields(Reader: TReader);
    procedure   ReadRootNodeFields(Reader: TReader);
    procedure   ReadParentNodeFields(Reader: TReader);
    procedure   ReadChildNodeFields(Reader: TReader);
    procedure   SetOnRelocateDataset(Event: TLocateRecordEvent);
    procedure   WriteSelectedFieldLinks(Writer: TWriter; Links: TEzFieldLinks);
    procedure   WriteMDFieldLinks(Writer: TWriter);
    procedure   WriteSingleNodeFields(Writer: TWriter);
    procedure   WriteRootNodeFields(Writer: TWriter);
    procedure   WriteParentNodeFields(Writer: TWriter);
    procedure   WriteChildNodeFields(Writer: TWriter);

  public
    procedure   Assign(Source: TPersistent); override;
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   RelocateDataset(Node: TRecordNode);

    property ActiveFieldLinks: TEzFieldLinks read GetActiveFieldLinks;
    property DetailDataset: TDataset read GetDetailDataset;
    property EzDataset: TEzDataset read GetEzDataset;
    property FieldLinks[NodeType: TNodeType]: TEzFieldLinks read GetFieldLinks;
    property MDFields: TEzFieldLinks index ntSingle read GetFieldLinkIndex;
    property SingleNodeFields: TEzFieldLinks index ntSingle read GetFieldLinkIndex;
    property RootNodeFields: TEzFieldLinks index ntRoot read GetFieldLinkIndex;
    property ParentNodeFields: TEzFieldLinks index ntParent read GetFieldLinkIndex;
    property ChildNodeFields: TEzFieldLinks index ntChild read GetFieldLinkIndex;
    property SyncedWith: TRecordNode read FSyncedWith;

    property OnRelocateDataset: TLocateRecordEvent read GetOnRelocateDataset write SetOnRelocateDataset;

  published
    property CanExpand: Boolean read FCanExpand write FCanExpand default True;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property KeyField: string read FKeyField write FKeyField;
    property ParentRefField: string read FParentRefField write FParentRefField;
  end;

  TEzDataLinks = class(TCollection)
  private
    FEzDataset: TCustomEzDataset;

  protected
    procedure FieldListChanged;
    function  GetOwner: TPersistent; override;
    function  GetEzDatalink(Index: Integer): TEzDatalink;
    procedure SetEzDatalink(Index: Integer; Value: TEzDatalink);
    procedure Update(Item: TCollectionItem); override;

  public
    constructor Create(EzDataset:TCustomEzDataset; ColumnClass: TCollectionItemClass);
    function  Add: TEzDatalink;
    function  FindDatalink(Dataset: TDataset): TEzDatalink;
    procedure DisableDetailDatasets;
    procedure EnableDetailDatasets;

    property Items[Index: Integer]: TEzDatalink read GetEzDatalink write SetEzDatalink; default;
    property EzDataset: TCustomEzDataset read FEzDataset;
  end;

  PDBState = ^TDBState;
  TDBState = record
    Key: TNodeKey;
    CursorOptions: TCursorOptions;
    NodeFlags: TNodeFlags;
    UseDefaultCursor: Boolean;
  end;

  TCustomEzDataset = class(TDataSet, IUnknown)
  private
    FOpening: Boolean;
    FEzStates: TEzDBStates;
    FActiveCursorIndex: Integer;
    FMaxPosition: Integer;
    FCursors: TEzDBCursors;
    FUseDefaultCursor: Boolean;
    FDirectAccessStack: TList;
    FRecBufSize: Integer;
    FOptions: TEzDBOptions;
    FIndexFields: string;
    FParser: TEzDatasetParser;
    FEzDataLinks: TEzDataLinks;
    FCalculateList: TList;
    FGeneratedKeys: TList;
    FFilterBuffer: TRecBuf;
    FModifiedFields: TList;
    // FOldValueBuffer: TRecordBuffer;
    {$IFDEF XE3}
    FReserved: Pointer;
    {$ENDIF}
    FInsertNode: TRecordNode;
    FInsertPosition: TEzInsertPosition;

    FAfterExpandRecord: TExpandRecordEvent;
    FBeforeExpandRecord: TExpandRecordEvent;
    FAfterMoveRecord: TMoveRecordEvent;
    FBeforeMoveRecord: TMoveRecordEvent;
    FOnCompareNodes: TCompareNodesEvent;
    FOnGetVariableValue: TGetVariableEvent;
    FOnGenerateKey: TGenerateKeyEvent;
    FOnRelocateDataLink: TLocateRecordEvent;
    FOnUserFunction: TCalcOnUserFunc;
    FOnStrUserFunction: TCalcOnStrUserFunc;

  protected
    function  AllocRecordBuffer: TRecordBuffer; override;
    procedure CheckCursor;
    procedure CloseCursor; override;
{$IFDEF XE2}
    procedure DataEvent(Event: TDataEvent; Info: NativeInt); override;
{$ELSE}
    procedure DataEvent(Event: TDataEvent; Info: Integer); override;
{$ENDIF}
    procedure DatalinksChanged; virtual;
    procedure DoAfterOpen; override;
    procedure DoOnNewRecord; override;
    function  DoGetIdentValue(ctype: TCalcType; const S: String; var Value: Double) : Boolean;
    function  DoGetStrIdentValue(ctype: TCalcType; const S: String; var Value: string) : Boolean;
    procedure DoGetRangeValues(const sRange, sVariable: string; ValueType: TValueType; ParamList: TStringList);
    procedure DoGetVariableValue(const Variable: string; var Value: Variant); virtual;
    procedure FreeRecordBuffer(var Buffer: TRecordBuffer); override;
    function  GenerateKeyValue(DataLink: TEzDatalink; Level: Integer): Variant; virtual;
    function  GetActiveRecBuf(var RecBuf: TRecBuf): Boolean;
    function  GetActiveCursor: TEzDBCursor;
    function  GetActiveDatalink: TEzDatalink;
    procedure GetBookmarkData(Buffer: TRecBuf; Data: TBookmark); override;
    function  GetBookmarkFlag(Buffer: TRecBuf): TBookmarkFlag; override;
    function  GetCanModify: Boolean; override;
    function  GetDefaultCursor: TEzDBCursor;
    function  GetDirectAccess: Boolean;
    function  GetExpanded: Boolean;
    function  GetActiveDataset: TDataset;
    function  GetMasterDataSet: TDataset;
    function  GetMaxLevel: integer;
    function  GetRecordNode: TRecordNode;
    function  GetTopNode: TRecordNode;
    function  GetTopRecNo: Integer;
    function  GetNodeKey: TNodeKey;
    function  GetNodeType: TNodeType;
    function  GetParent: TNodeKey;
    function  GetFields : TFields;
    function  GetRecNo: Integer; override;
    function  GetRecord(Buffer: TRecBuf; GetMode: TGetMode; DoCheck: Boolean): TGetResult; override;
    function  GetRecordCount: Integer; override;
    function  GetRecordSize: Word; override;
    function  GetSelfReferencing: Boolean;
    function  GetUseDefaultCursor: Boolean;

    procedure InternalAddRecord(Buffer: TRecBuf; Append: Boolean); override;
    procedure InternalCancel; override;
    procedure InternalClose; override;
    procedure InternalCreateFields; virtual;
    procedure InternalDelete; override;
    procedure InternalEdit; override;
    procedure InternalFirst; override;
    procedure InternalInsert; override;
    function  InternalGetRecord(Buffer: TRecBuf; GetMode: TGetMode; DoCheck: Boolean): TGetResult;
    function  InternalGetFieldData(Node: TRecordNode; FieldName: string) : Variant;
    procedure InternalGotoBookmark(Bookmark: {$IFDEF EZ_D2009}Pointer{$ELSE}TBookmark{$ENDIF}); override;
    procedure InternalHandleException; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalInitRecord(Buffer: TRecordBuffer); override;
    procedure InternalLast; override;
    procedure InternalOpen; override;
    procedure InternalPost; override;
    procedure InternalRefresh; override;
    procedure InternalSetToRecord(Buffer: TRecordBuffer); override;
    { actual impl. of SetFieldData, required to support both Delphi and CPP compile }
    procedure InternalSetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean);
    procedure InternalSyncDataset(Syncwith: TRecordNode; SyncMasterLinks: Boolean);
    function  IsCursorOpen: Boolean; override;
    procedure LayoutFieldMap;
    function  LocateRecord(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions; SyncCursor: Boolean): Boolean;
    procedure Loaded; override;
    procedure LoadDefaultCursorData;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure OpenCursor(InfoQuery: Boolean); override;
    procedure SetActiveCursorIndex(Index: Integer);
    procedure SetBookmarkFlag(Buffer: TRecBuf; Value: TBookmarkFlag); override;
    procedure SetBookmarkData(Buffer: TRecBuf; Data: TBookmark); override;
    procedure SetCursors(C: TEzDBCursors);
    procedure SetEzDataLinks(Value: TEzDataLinks);

{$IFNDEF EZ_D5}
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
{$ELSE}
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
    procedure SetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean); override;
{$ENDIF}
{$IFDEF XE3}
    procedure SetFieldData(Field: TField; Buffer: TValueBuffer); overload; override;
    procedure SetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean); overload; override;
{$ENDIF}

    procedure SetExpanded(Value: Boolean);
    procedure SetFiltered(Value: Boolean); override;
    procedure SetRecNo(Value: Integer); override;
    procedure SetTopNode(Node: TRecordNode);
    procedure SetTopRecNo(Value: Integer);
    procedure SetNodeKey(Value: TNodeKey);
    procedure SetRecordNode(Node: TRecordNode);
    procedure SetParent(Key: TNodeKey);
    procedure UpdateRecordSetPosition(Buffer: TRecBuf);

    {$IFDEF XE3}
    property Reserved: Pointer read FReserved write FReserved;
    {$ENDIF}
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    // Node handling functions
    function  AddNode(Key: TNodeKey; Location: TRecordNode; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
    function  AddChildNode(Key: TNodeKey; Parent: TRecordNode; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
    function  CompareNodes(N1, N2: TRecordNode): Integer; virtual;
    procedure DeleteNode(Node: TRecordNode); overload;
    procedure DeleteNode(Key: TNodeKey); overload;
    procedure CollapseAll;
    function  CursorHasRecord: Boolean;
    function  InsertRecordNode(Key: TNodeKey; Location: TRecordNode; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
    function  InternalCompareNodes(N1, N2: TRecordNode): Integer;
    function  FindKey(Key: TNodeKey) : TRecordNode;
    function  FixupChildRecords(Dataset: TDataset; Parent: Variant): Boolean;
    procedure FixupGeneratedKey(Dataset: TDataset; GeneratedKey, NewKey: Variant);
    procedure FreeBookmark(Bookmark: TBookmark); override;
    procedure ExpandAll;
    function  LocateNode(Node: TRecordNode) : Boolean; overload;
    function  LocateNode(Key: TNodeKey) : Boolean; overload;
    function  MapDefaultFields: Boolean;
    procedure MoveNode(Node, Parent: TRecordNode);
    procedure MoveRecord(Node, Location: TRecordNode; Position: TEzInsertPosition);
    function  KeyExists(Key: TNodeKey) : Boolean;
    // end of node handling functions

    procedure Add;
    procedure AddChild;
    function  BookmarkValid(Bookmark: TBookmark): Boolean; override;
    function  CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer; override;
    function  CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream; override;
    procedure EndDirectAccess;
    function  FieldCanModify(Field: TField) : Boolean; virtual;
    function  GetBlobFieldData(FieldNo: Integer; var Buffer: TBlobByteData): Integer; override;
    function  GetFieldClass(FieldType: TFieldType): TFieldClass; override;

{$IFDEF XE10_UP}
    function GetFieldData(Field: TField; var Buffer: TValueBuffer): Boolean; overload; override;
    function GetFieldData(Field: TField; var Buffer: TValueBuffer; NativeFormat: Boolean): Boolean; overload; override;
    function GetFieldData(FieldNo: Integer; var Buffer: TValueBuffer): Boolean; overload; override;
{$ELSE}
  {$IFDEF XE3}
      function GetFieldData(Field: TField; Buffer: TValueBuffer): Boolean; overload; override;
      function GetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean): Boolean; overload; override;
      function GetFieldData(FieldNo: Integer; Buffer: TValueBuffer): Boolean; overload; override;
  {$ENDIF}
{$ENDIF}

{$IFNDEF EZ_D5}
    function GetFieldData(Field: TField; Buffer: Pointer): Boolean; overload; override;
{$ELSE}
    function  GetFieldData(Field: TField; Buffer: Pointer): Boolean; override;
    function  GetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean): Boolean; override;
{$ENDIF}
    function  Locate(const KeyFields: string; const KeyValues: Variant;
              Options: TLocateOptions): Boolean; override;
    function  Lookup(const KeyFields: string; const KeyValues: Variant;
              const ResultFields: string): Variant; override;
    procedure LoadCursorData(Cursor: TEzDBCursor);
    function  UpdateStatus: TUpdateStatus; override;
    function  HasChildren: Boolean;
    function  HasParentRecord: Boolean;
    procedure InsertChild; virtual;
    function  Level: SmallInt;
    procedure NotifyBufferCountChanged(Count: Integer);
    procedure Resync(Mode: TResyncMode); override;
    procedure StartDirectAccess(AddOptions: TCursorOptions = []; SwitchToDefaultCursor: Boolean = False);
    procedure SyncDataset;
    procedure SyncCursor;
    procedure UpdateMaxPosition;

    property ActiveCursor: TEzDBCursor read GetActiveCursor;
    property ActiveCursorIndex: Integer read FActiveCursorIndex write SetActiveCursorIndex default 0;
    property ActiveDatalink: TEzDatalink read GetActiveDatalink;
    property DefaultCursor: TEzDBCursor read GetDefaultCursor;
    property DirectAccess: Boolean read GetDirectAccess;
    property Cursors: TEzDBCursors read FCursors write SetCursors;
    property Expanded: Boolean read GetExpanded write SetExpanded;
    property GeneratedKeys: TList read FGeneratedKeys;
    property InsertNode: TRecordNode read FInsertNode write FInsertNode;
    property InsertPosition: TEzInsertPosition read FInsertPosition write FInsertPosition;
    property MasterDataSet: TDataSet read GetMasterDataSet;
    property MaxPosition: Integer read FMaxPosition write FMaxPosition;
    property ActiveDataset: TDataset read GetActiveDataset;
    property MaxLevel: integer read GetMaxLevel;
    property EzDataLinks: TEzDataLinks read FEzDataLinks write SetEzDataLinks;
    property Options: TEzDBOptions read FOptions write FOptions
                      default [eoCascadeDelete, eoParentKeysRequired];
    property Parser: TEzDatasetParser read FParser;
    property RecordNode: TRecordNode read GetRecordNode write SetRecordNode;
    property TopNode: TRecordNode read GetTopNode write SetTopNode;
    property TopRecNo: Integer read GetTopRecNo write SetTopRecNo;
    property NodeKey: TNodeKey read GetNodeKey write SetNodeKey;
    property NodeType: TNodeType read GetNodeType;
    property Parent: TNodeKey read GetParent write SetParent;
    property SelfReferencing: Boolean read GetSelfReferencing;
    property EzStates: TEzDBStates read FEzStates;
    property UseDefaultCursor: Boolean read GetUseDefaultCursor;
    property OnGetVariableValue: TGetVariableEvent read FOnGetVariableValue write FOnGetVariableValue;
    property OnCompareNodes: TCompareNodesEvent read FOnCompareNodes write FOnCompareNodes;
    property OnGenerateKey: TGenerateKeyEvent read FOnGenerateKey write FOnGenerateKey;
    property AfterExpandRecord: TExpandRecordEvent read FAfterExpandRecord write FAfterExpandRecord;
    property BeforeExpandRecord: TExpandRecordEvent read FBeforeExpandRecord write FBeforeExpandRecord;
    property AfterMoveRecord: TMoveRecordEvent read FAfterMoveRecord write FAfterMoveRecord;
    property BeforeMoveRecord: TMoveRecordEvent read FBeforeMoveRecord write FBeforeMoveRecord;
    property OnRelocateDataLink: TLocateRecordEvent read FOnRelocateDataLink write FOnRelocateDataLink;
    property OnUserFunction: TCalcOnUserFunc read FOnUserFunction write FOnUserFunction;
    property OnStrUserFunction: TCalcOnStrUserFunc read FOnStrUserFunction write FOnStrUserFunction;
  end;

{ TEzCursor }

  TEzDataset = class(TCustomEzDataset)
  private
  protected
  public
  published
    property Active;
    property ActiveCursorIndex;
    property BufferCount;
    property Cursors;
    property EzDataLinks;
    property Options;
    property Filtered;
    property OnFilterRecord;
    property OnGetVariableValue;
    property OnCompareNodes;
    property OnGenerateKey;
    property OnRelocateDataLink;
    property OnUserFunction;
    property OnStrUserFunction;    

    property AfterExpandRecord;
    property BeforeExpandRecord;
    property BeforeOpen;
    property AfterOpen;
    property BeforeClose;
    property AfterClose;
    property BeforeInsert;
    property AfterInsert;
    property BeforeEdit;
    property AfterEdit;
    property AfterMoveRecord;
    property BeforeMoveRecord;
    property BeforePost;
    property AfterPost;
    property BeforeCancel;
    property AfterCancel;
    property BeforeDelete;
    property AfterDelete;
    property BeforeScroll;
    property AfterScroll;
{$IFDEF EZ_D5}
    property BeforeRefresh;
    property AfterRefresh;
{$ENDIF}
    property OnCalcFields;
    property OnDeleteError;
    property OnEditError;
    property OnNewRecord;
    property OnPostError;
   end;

  PVariantList = ^TVariantList;
  TVariantList = array[0..0] of OleVariant;

  function  CompareNodeKeys(Key1, Key2: TNodeKey): Integer;
  function  NodeKeyToStr(Key: TNodeKey): string;
  function  StrToNodeKey(s: string) : TNodeKey;
  function  CopyField(AOwner: TComponent; ADataset: TDataset; Source: TField) : TField;
  procedure CopyProperties(SrcObj, DestObj: TObject);
  function  VarSafeCompare(const V1, V2: Variant; CaseSensitive: Boolean): integer;
  function  VarSafeIsNull(const V: Variant): Boolean;
{$IFNDEF TNT_UNICODE}
  function GetWideDisplayText(Field: TField): WideString;
{$ENDIF}

implementation

uses EzStrings_3, TypInfo, Forms, ActiveX, Windows, ComObj, DbConsts, DbCommon, EzSplash
{$IFDEF EZ_D2006}
  , WideStrUtils
{$ENDIF}
{$IFDEF EZ_D6}
  , SqlTimSt
{$ENDIF}
{$IFDEF EZ_D2007}
  , FMTBcd
{$ENDIF}
  ;

const
  bfNA = TBookmarkFlag(Ord(High(TBookmarkFlag)) + 1);
  dsSort = TDataSetState(Ord(High(TDataSetState)) + 1);
  dsEvaluateFormula = TDataSetState(Ord(High(TDataSetState)) + 2);

{$IFDEF EZ_VCB4}
  { these field types do not exist in CPP 4.0 }
  ftGuid = TFieldType(-1);
  ftInterface = TFieldType(-1);
  ftVariant = TFieldType(-1);
  ftDispatch = TFieldType(-1);
{$ENDIF}

{$IFDEF DEMO}
var
  SplashDidShow: Boolean;
{$ENDIF}

procedure EzDbCursorError(const Message: string; Cursor: TEzDbCursor = nil);
begin
  if Assigned(Cursor) and (Cursor.CursorName <> '') then
    raise EEzDBCursorError.Create(Format('%s: %s', [Cursor.CursorName, Message])) else
    raise EEzDBCursorError.Create(Message);
end;

procedure EzDatasetError(const Message: string; Dataset: TCustomEzDataset = nil);
begin
  if Assigned(Dataset) then
    raise EEzDatasetError.Create(Format('%s: %s', [Dataset.Name, Message])) else
    raise EEzDatasetError.Create(Message);
end;

function CompareNodeKeys(Key1, Key2: TNodeKey) : Integer;
begin
  if Key1.Level < Key2.Level then
    Result := -1

  else if Key1.Level > Key2.Level then
    Result := 1

  else
    Result := VarSafeCompare(Key1.Value, Key2.Value, True);
end;

function NodeKeyToStr(Key: TNodeKey): string;

  function VarSafeString(const V: Variant) : string;
  begin
    if VarIsNull(V) then
      Result := 'Null' else
      Result := string(V);
  end;

var
  i, h: integer;

begin
  Result := IntToStr(Key.Level);
  if VarIsArray(Key.Value) then
  begin
    h := VarArrayHighBound(Key.Value, 1);
    i:=0;
    while (i<=h) do
    begin
      Result := Result + ',' + VarSafeString(Key.Value[i]);
      inc(i);
    end;
  end
  else
    Result := Result + ',' + VarSafeString(Key.Value);
end;

function StrToNodeKey(s: string) : TNodeKey;
var
  p: integer;
begin
  p := Pos(',', s);
  Result.Level := StrToInt(copy(s, 1, p-1));
  Result.Value := copy(s, p+1, MaxLong);
end;

procedure CopyProperties(SrcObj, DestObj: TObject);
var
  PropList: pPropList;
  PropInfo: pPropInfo;
  count, i: Integer;
  Value: Variant;
  PropName: string;
  ClassSrcObj, ClassDestObj: TObject;
begin
  // Here we'll get the count of the given properties, ...
  Count := GetPropList(SrcObj.ClassInfo, tkProperties, nil);

  // ...and create room for the PropList,...
  GetMem(PropList, Count * SizeOf(PPropInfo));
  try
    // ...get the Proplist-Data,...
    GetPropList(SrcObj.ClassInfo, tkProperties, PropList);
    // ...and write the property-names into the StringList
    for i := 0 to Count - 1 do
    begin
      PropInfo := Proplist[i];
      PropName := string(PropInfo.Name);
      if (PropInfo.SetProc <> nil) and
         (PropName <> 'Name') and
         (PropName <> 'FieldKind') and
         (PropName <> 'DataSet') then
      begin
        if PropInfo.PropType^.Kind = tkClass then
        begin
          ClassSrcObj := GetObjectProp(SrcObj, PropName);
          ClassDestObj := GetObjectProp(DestObj, PropName);
          if (ClassSrcObj<>nil) and (ClassDestObj<>nil) then
            CopyProperties(ClassSrcObj, ClassDestObj)
        end
        else
        begin
          Value := GetPropValue(SrcObj, PropName);
          SetPropValue(DestObj, PropName, Value);
        end;
      end;
    end;
  finally
    FreeMem(PropList);
  end;
end;

function CopyField(AOwner: TComponent; ADataset: TDataset; Source: TField) : TField;
var
  FieldClassType: TFieldClass;
begin
  FieldClassType := TFieldClass(Source.ClassType);
  if FieldClassType = nil then DatabaseErrorFmt(SUnknownFieldType, [ADataset.Name]);
  Result := FieldClassType.Create(AOwner);
  try
    if ADataset <> nil then
      Result.Name := ADataset.Name + StringReplace(Source.FieldName, ' ', '_', [rfReplaceAll])
    else
      Result.Name := StringReplace(Source.FieldName, ' ', '_', [rfReplaceAll]);

    CopyProperties(Source, Result);
    Result.DataSet := ADataset;

//    Result.FieldName := FieldName;
//    Result.SetFieldType(DataType);
//    Result.FieldKind := FieldKind;
//    Result.DataSet := ADataset;
//    CopyFieldSettings(Source, Result);

  except
    Result.Free;
    raise;
  end;
end;

function FieldListCheckSum(DataSet: TDataset): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to DataSet.Fields.Count - 1 do
    Result := Result + (Integer(Dataset.Fields[I]) shr (I mod 16));
end;

function VarSafeCompare(const V1, V2: Variant; CaseSensitive: Boolean): integer;
var
  i, h: integer;
{$IFDEF EZ_D6}
  T: TVarType;
{$ENDIF}

begin
  if VarSafeIsNull(V1) then
  begin
    if VarSafeIsNull(V2) then
      Result := 0 else
      Result := -1;
    Exit;
  end;

  if VarSafeIsNull(V2) then
  begin
    Result := 1;
    Exit;
  end;

  if not VarIsArray(V1) then
  begin
    if TVarData(V1).VType = VT_DECIMAL then
    begin
{$IFDEF XE2}
      if LargeInt(V1) < LargeInt(V2) then
        Result := -1
      else if LargeInt(V1) > LargeInt(V2) then
        Result := 1
      else
        Result := 0;
{$ELSE}
  {$IFDEF EZ_D5}
        if Decimal(V1).lo64 < Decimal(V2).lo64 then
  {$ELSE}
        if Integer(V1) < Integer(V2) then
  {$ENDIF}
          Result := -1 else

  {$IFDEF EZ_D5}
        if Decimal(V1).lo64 > Decimal(V2).lo64 then
  {$ELSE}
        if Integer(V1) > Integer(V2) then
  {$ENDIF}
          Result := 1 else
          Result := 0;
{$ENDIF}
    end
    else
    begin

{$IFDEF EZ_D6}
      // Rely on standard variant compare
      T := VarType(V1);
      if (T=varOleStr) or (T=varStrArg) or (T=varString) then
      begin
        if CaseSensitive then
          Result := WideCompareStr(V1, V2) else
          Result := WideCompareText(V1, V2);
      end
      else
{$ENDIF}
      begin
        if V1 < V2 then
          Result := -1
        else if V1 > V2 then
          Result := 1
        else
          Result := 0;
      end;
    end;
  end
  else
  begin
    h := VarArrayHighBound(V1, 1);
    i:=0;
    Result := 0;
    while (i<=h) and (Result = 0) do
    begin
      Result := VarSafeCompare(V1[i], V2[i], CaseSensitive);
      Inc(i);
    end;
  end;
end;

function VarSafeIsNull(const V: Variant): Boolean;
var
  i, h: integer;

begin
  if not VarIsArray(V) then
    Result := VarIsNull(V)
  else
  begin
    h := VarArrayHighBound(V, 1);
    i:=0;
    Result := true;
     while (i<=h) and Result do
    begin
      Result := VarIsNull(V[i]);
      Inc(i);
    end;
  end;
end;

{$IFNDEF TNT_UNICODE}
function VarToWideStrDef(const V: Variant; const ADefault: WideString): WideString;
begin
  if not VarIsNull(V) then
    Result := V
  else
    Result := ADefault;
end;

function VarToWideStr(const V: Variant): WideString;
const
  // This is the value returned when a NULL is converted into a string.  Other
  //  environments return 'NULL' instead of Delphi's default of an empty string.
  NullAsStringValue: string = '';

begin
  Result := VarToWideStrDef(V, NullAsStringValue);
end;

function GetAsWideString(Field: TField): WideString;
begin
  if (Field.ClassType = TMemoField{TNT-ALLOW TMemoField}) then
    Result := VarToWideStr(Field.AsVariant) { works for NexusDB BLOB Wide }
  else
{$IFDEF WIDESTRINGSUPPORTED}
    Result := Field.AsWideString;
{$ELSE}
    Result := Field.AsString;
{$ENDIF}
end;

function GetWideDisplayText(Field: TField): WideString;
var
  WideField: IWideStringField;

begin
  if Field.GetInterface(IWideStringField, WideField) then
    Result := WideField.WideDisplayText
  else if (Field is TWideStringField{TNT-ALLOW TWideStringField})
  and (not Assigned(Field.OnGetText)) then
    Result := GetAsWideString(Field)
  else
    Result := Field.DisplayText{TNT-ALLOW DisplayText};
end;
{$ENDIF}

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=

constructor TRecordNodeTwins.Create(AOwner: TRecordNode);
begin
end;

destructor TRecordNodeTwins.Destroy;
begin
  inherited;
end;

function TRecordNodeTwins.Add(Value: TRecordNodeTwin): Integer;
begin
  Result := inherited Add(nil {Value});
end;

function  TRecordNodeTwins.Get(Index: Integer): TRecordNodeTwin;
begin
//  inherited;
end;

procedure TRecordNodeTwins.Put(Index: Integer; Value: TRecordNodeTwin);
begin
//  inherited;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TRecordNode.Create;
begin
  Next := nil;
  Child := nil;
  Prev := nil;
  Parent := nil;
  Key.Level := -1;
  Key.Value := Null;
  Flags := [];
  RecNo := -1;
  Data := nil;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TRecordnodeArray.Create(itemcount, Grow: Integer);
begin
  inherited Create;
  FMemory := nil;
  FCapacity := 0;
  FCount := 0;
  FGrowBy := Grow;
  SetCapacity(itemcount);
end;

destructor TRecordnodeArray.Destroy;
begin
  if (FMemory <> nil) then
    Clear;
  inherited Destroy;
end;

procedure TRecordnodeArray.Add(Node: TRecordnode);
var
  Pos: Integer;
begin
  if FCount+1 >= FCapacity then
    SetCapacity(FCapacity+FGrowBy);

  FindKey(Pos, Node.Key);

  if (Pos < FCount) then
  begin
    try
      MoveMemory(GetIndexPtr(Pos+1), GetIndexPtr(Pos), (FCount - Pos) * SizeOf(TRecordNode));
    except
      InternalHandleException;
    end;
  end;
  TRecordNode(GetIndexPtr(Pos)^) := Node;
  Inc(FCount);
end;

procedure TRecordnodeArray.Clear;
begin
  FCount := 0;
  FCapacity := 0;
  ReallocMem(FMemory, 0);
end;

procedure TRecordnodeArray.Delete(Index: Integer);
begin
  { We are removing only one item. }
  if ValidIndex(index) then
  begin
    Dec(FCount);
    if (Index < FCount) then
    begin
      try
        MoveMemory(GetIndexPtr(Index), GetIndexPtr(Index + 1), (FCount - Index) * SizeOf(TRecordNode));
      except
      end;
    end;
  end;
end;

procedure TRecordnodeArray.DeleteKey(Key: TNodeKey);
var
  i: integer;
begin
  if FindKey(i, Key) then
    Delete(i);
end;

function TRecordnodeArray.FindNodeByKey(Key: TNodeKey): TRecordNode;
var
  i: integer;
begin
  result := nil;
  if FindKey(i, key) then
    result := GetNode(i);
end;

function TRecordnodeArray.FindKey(var Index: Integer; Key: TNodeKey): Boolean;
var
  L, H, I, C: Integer;

begin
  Result := False;
  L := 0;
  H := Count - 1;
  while (L <= H) do
  begin
    I := (L + H) shr 1;
    C := CompareNodeKeys(GetNode(I).Key, Key);
    if (C < 0) then
      L := I + 1
    else
    begin
      H := I - 1;
      if (C = 0) then
        Result := True;
    end;
  end;
  Index := L;
end;

function TRecordnodeArray.GetIndexPtr(index: Integer): Pointer;
begin
  Result := nil;
  if ValidIndex(index) then
    Result := Ptr(LongInt(FMemory) + (index*SizeOf(TRecordNode)));
end;

function TRecordnodeArray.GetNode(index: Integer) : TRecordNode;
begin
  Result := TRecordNode(GetIndexPtr(index)^);
end;

procedure TRecordnodeArray.InternalHandleException;
begin
  Clear;
  raise Exception.Create('general error in TRecordnodeArray.');
end;

procedure TRecordnodeArray.PutNode(index: Integer; Node: TRecordNode);
begin
  if ValidIndex(index) then
    TRecordNode(GetIndexPtr(index)^) := Node;
end;

procedure TRecordnodeArray.SetCapacity(NewCapacity: Integer);
begin
  if (NewCapacity < FCount) or (NewCapacity > MaxListSize) then
    raise Exception.Create('ArrayIndexError');
  if (NewCapacity > FCapacity) then
  begin
    ReallocMem(FMemory, NewCapacity * SizeOf(TRecordNode));
    // Do not use GetIndexPtr here because index would be invalid
    FillChar(Pointer(Integer(FMemory) + FCapacity * SizeOf(TRecordNode))^, (NewCapacity - FCapacity) * SizeOf(TRecordNode), 0);
    FCapacity := NewCapacity;
  end;
end;

procedure TRecordnodeArray.SetCount(NewCount: Integer);
begin
  if (NewCount < 0) or (NewCount > MaxListSize) then
    raise Exception.Create('ArrayIndexError');
  if (NewCount > FCapacity) then
    SetCapacity(NewCount);
  if (NewCount > FCount) then
    // Do not use GetIndexPtr here because index would be invalid
    FillMemory(Ptr(LongInt(FMemory) + (FCount*SizeOf(TRecordNode))), (NewCount - FCount) * SizeOf(TRecordNode), 0);
  FCount := NewCount;
end;

function TRecordnodeArray.ValidIndex(Index: Integer): Boolean;
begin
  Result := True;
  if (Index < 0) or (Index > FCount) then
  begin
    raise Exception.Create('ArrayIndexError');
    Result := False;
  end
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzDBCursor.Create(Collection: TCollection);
begin
  inherited;
  FIsClearing := False;
  FSortedKeys := TRecordnodeArray.Create(100,100);
  FSortDirection := sdAscending;

  FTopNode := TRecordNode.Create;
  FTopNode.Key.Level := -1;
  FTopNode.Key.Value := Null;

  FEndNode := TRecordNode.Create;
  FEndNode.Key.Level := -1;
  FEndNode.Key.Value := Null;

  FTopNode.Next := FEndNode;
  FEndNode.Prev := FTopNode;

  FCurrentNode := FTopNode;
  FOptions := [];
  FRecordCount := 0;
  FRecNoValid := false;
end;

destructor TEzDBCursor.Destroy;
begin
  inherited;
  Clear;
  FTopNode.Destroy;
  FEndNode.Destroy;
  FSortedKeys.Destroy;
end;

function TEzDBCursor.AbsParent: TRecordNode;
begin
  Result := AbsParent(FCurrentNode);
end;

function TEzDBCursor.AbsParent(Node: TRecordNode): TRecordNode;
begin
  Result := Node;
  while Result.Parent <> nil do Result := Result.Parent;
end;

function TEzDBCursor.Add(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
begin
  Result := CreateNode(Key, Flags, AllowExpanded);
  try
    InternalAddNode(Node, Result);
  except
    FreeNode(Result);
    raise;
  end;
  FSortedKeys.Add(Result);
  if IsNodeVisible(Result) then begin
    Inc(FRecordCount);
    FRecNoValid := False;
  end;
  Exclude(Result.Flags, nfNew);
end;

function TEzDBCursor.Add(Parent, Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
var
  P: TRecordNode;

begin
  Result := nil;
  if VarIsNull(Key.Value) then raise Exception.Create(SNoKey);

  // Check for automatically created records.
  // If previously inserted node had a parent key for which a node did not
  // exist, then that node might have been automatically added to the tree.
  // Now, if we are adding the node for which we have created such a node,
  // then we have to update the previously created node instead adding
  // another one.
  if (cpAutoCreateParents in Options) then
  begin
    Result := FindNode(Key);
    if (Result <> FEndNode) then
    begin
      // A node with the same key already exists (i.e. was auto created on a previous insert)
      // Reinsert the parent
      Exclude(Result.Flags, nfAutoCreated);

      // Node already exists, if parent value is different from stored value,
      // then we need to move the node to it's new location.
      if not VarIsNull(Parent.Value) then
      begin
        P := FindNode(Parent);
        if P = FEndNode then
        begin
          P := Add(nil, Parent, Flags, AllowExpanded);
          Include(P.Flags, nfAutoCreated);
        end;
        UpdateParent(P, Result);
      end
      else if cpSorted in FOptions then           
        ReIndexNode(Result);

      Exit;
    end;
  end;

  if not VarIsNull(Parent.Value) and not VarIsEmpty(Parent.Value) then
  begin
    // Check if the parent actualy exists!
    P := FindNode(Parent);
    if P = FEndNode then
    begin
      if (cpAutoCreateParents in FOptions) then
      begin
        P := Add(nil, Parent, Flags, AllowExpanded);
        Include(P.Flags, nfAutoCreated);
      end
      else if cpIgnore in FOptions then
        Exit
      else
        EzDbCursorError(SParentNotFound, Self);
    end;
    Result := AddChild(P, Key, Flags, AllowExpanded);
  end
  else
    Result := Add(nil, Key, Flags, AllowExpanded);
end;

function TEzDBCursor.AddChild(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
begin
  if Node.Child = nil then
  begin
    Result := CreateNode(Key, Flags, AllowExpanded);
    Node.Child := Result;
    Result.Parent := Node;

    FSortedKeys.Add(Result);
    if IsNodeVisible(Result) then
    begin
      Inc(FRecordCount);
      FRecNoValid := False;
    end;
    Exclude(Result.Flags, nfNew);
  end
  else
    Result := Add(Node.Child, Key, Flags, AllowExpanded);
end;

function TEzDBCursor.AddChildFirst(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
begin
  if Node.Child = nil then
    Result := AddChild(Node, Key, Flags, AllowExpanded) else
    Result := Insert(Node.Child, Key, Flags, AllowExpanded);
end;

function TEzDBCursor.Insert(Node: TRecordNode; Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;

  procedure InsertNodeInTree;
  var
    N: TRecordNode;

  begin
    if Node.Prev <> nil then
    begin
      Result.Parent := Node.Parent;
      N := Node.Prev;
      Result.Next := Node;
      Node.Prev := Result;
      if N <> nil then
      begin
        Result.Prev := N;
        N.Next := Result;
      end;
    end

    // Special case: Insert new node as the first child of parent
    else
    begin
      Result.Next := Node;
      Node.Prev := Result;
      if Node.Parent <> nil then begin
        Result.Parent := Node.Parent;
        Result.Parent.Child := Result;
      end;
    end;
  end;

begin
  try
    Result := CreateNode(Key, Flags, AllowExpanded);

    if cpSorted in FOptions then
      InternalAddNode(Node, Result)
    else
    begin
      if (Node = nil) or (Node = FTopNode) then
        Node := NextNode(FTopNode);
      InsertNodeInTree;
    end;

  except
    FreeNode(Result);
    raise;
  end;

  FSortedKeys.Add(Result);
  if IsNodeVisible(Result) then begin
    Inc(FRecordCount);
    FRecNoValid := false;
  end;
  Exclude(Result.Flags, nfNew);
end;

function TEzDBCursor.Advance(Node: TRecordNode; Distance: Integer) : TRecordNode;
begin
  Result := Node;
  if Distance>0 then
    while (Result <> FEndNode) and (Distance>0) do
    begin
      Result := NextNode(Result);
      Dec(Distance);
    end
  else
    while (Result <> FTopNode) and (Distance<0) do
    begin
      Result := PrevNode(Result);
      Inc(Distance);
    end;
end;

procedure TEzDBCursor.Assign(Source: TPersistent);
begin
  if Source is TEzDBCursor then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      FIndexFieldNames := TEzDBCursor(Source).FIndexFieldNames;
      FCursorName := TEzDBCursor(Source).FCursorName;
      FOptions := TEzDBCursor(Source).FOptions;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TEzDBCursor.BookmarkValid(Bookmark: TRecordNode) : Boolean;
var
  N: TRecordNode;
begin
  N := NextNode(FTopNode);
  while (N <> FEndNode) and (N<>Bookmark) do
    N := NextNode(N);
  Result := N<>FEndNode;
end;

procedure TEzDBCursor.Clear;
var
  N, P: TRecordNode;
begin
  FIsClearing := True;
  try
    // Clear sorted keys at once for speed
    FSortedKeys.Clear;

    N := NextNode(FTopNode);
    while N <> FEndNode do
    begin
      P := N.Next;
      DeleteNode(N);
      N := P;
    end;
    FCurrentNode := FTopNode;
    FRecordCount := 0;
    FRecNoValid := False;
  finally
    FIsClearing := False;
  end;
end;

procedure TEzDBCursor.CloseCursor;
begin
  if Active then
  begin
    if Index=0 {default cursor closing} then
      Dataset.Close else
      Dataset.ActiveCursorIndex := 0;
    Clear;
  end;
end;


function TEzDBCursor.Childs(Node: TRecordNode): Integer;
var
  N: TRecordNode;
  O: TCursorOptions;

begin
  if Node.Child = nil then
  begin
    Result := 0;
    Exit;
  end;

  O := FOptions;
  FOptions := O + [cpShowAll];
  try
    Result := 0;
    N := Node.Child;
    while NodeHasParent(N, Node) do
    begin
      N := NextNode(N);
      Inc(Result);
    end;
  finally
    FOptions := O;
  end;
end;

function TEzDBCursor.CompareNodes(N1, N2: TRecordNode): Integer;
begin
  Result := Dataset.CompareNodes(N1, N2);
end;

function TEzDBCursor.CreateNode(Key: TNodeKey; Flags: TNodeFlags; AllowExpanded: Boolean) : TRecordNode;
begin
  Result := TRecordNode.Create;
  Result.Key := Key;
  if cpInheritsExpanded in FOptions then
    Result.Flags := Flags + [nfNew]
  else if AllowExpanded and (cpDefExpanded in FOptions) then
    Result.Flags := Flags + [nfExpanded, nfNew] else
    Result.Flags := Flags - [nfExpanded] + [nfNew];
end;

procedure TEzDBCursor.CutOffNode(Node: TRecordNode);
begin
  if not FIsClearing and IsNodeVisible(Node) then
  begin
    dec(FRecordCount, VisibleChilds(Node)+1);
    FRecNoValid := False;
  end;

  if Node = FCurrentNode then
    FCurrentNode := PrevNode(Node);
  // Remove node from tree
  if Node.Prev <> nil then
    Node.Prev.Next := Node.Next;
  if Node.Next <> nil then
    Node.Next.Prev := Node.Prev;
  // Is this the first child node of a branche?
  if (Node.Parent <> nil) and (Node.Prev = nil) then
    Node.Parent.Child := Node.Next;
  Node.Parent := nil;
  Node.Next := nil;
  Node.Prev := nil;
//  Dec(FRecordCount);
//  FRecNoValid := False;
end;

procedure TEzDBCursor.FreeNode(var Node: TRecordNode);
begin
  Node.Free;
  Node := nil;
end;

function TEzDBCursor.GetChildCount: Integer;
begin
  Result := Childs(FCurrentNode);
end;

function TEzDBCursor.GetDisplayName: string;
begin
  if FCursorName = '' then
    Result := 'Cursor' + IntToStr(Index) else
    Result := FCursorName;
end;

function TEzDBCursor.Exists(Key: TNodeKey) : Boolean;
begin
  Result := FindNode(Key) <> FEndNode;
end;

function TEzDBCursor.Exists(Node: TRecordNode) : Boolean;
var
  nd: TRecordNode;

begin
  while Assigned(Node.Parent) do
    Node := Node.Parent;

  nd := FTopNode;
  while (nd <> Node) and Assigned(nd.Next) do
    nd := nd.Next;

  Result := nd = Node;
end;

function TEzDBCursor.FindNearest(Key: TNodeKey): TRecordNode;
var
  i: Integer;

begin
  if not (Eof or Bof) and (CompareNodeKeys(FCurrentNode.Key, Key) = 0) then
  begin
    Result := FCurrentNode;
    Exit;
  end;

  with FSortedKeys do
  begin
    if Count = 0 then
    begin
      Result := EndNode;
      Exit;
    end;
    FindKey(i, key);
    if i < 0 then
      Result := Nodes[0]
    else if i >= Count then
      Result := Nodes[Count-1]
    else
      Result := Nodes[i];
  end;
end;

function TEzDBCursor.FindNode(Key: TNodeKey): TRecordNode;
begin
  if not (Eof or Bof) and (CompareNodeKeys(FCurrentNode.Key, Key) = 0) then
  begin
    Result := FCurrentNode;
    Exit;
  end;

  Result := FSortedKeys.FindNodeByKey(Key);
  if Result = nil then
    Result := FEndNode;
end;

function TEzDBCursor.GetActive: Boolean;
begin
  Result := (Dataset<>nil) and Dataset.Active and
            ((Index=0) or (Dataset.ActiveCursorIndex=Index));
end;

function TEzDBCursor.GetAbsCount: Integer;
begin
  Result := FSortedKeys.Count;
end;

function TEzDBCursor.GetRecNo: Integer;
begin
  if not FRecNoValid then
    InitRecordNumbers;
  Result := FCurrentNode.RecNo;
end;

function TEzDBCursor.GetParent(Node: TRecordNode): TRecordNode;
begin
  if Node.Parent = nil then
    Result := FTopNode else
    Result := Node.Parent;
end;

function TEzDBCursor.HasChildren: Boolean;
begin
  Result := HasChildren(FCurrentNode);
end;

function TEzDBCursor.HasChildren(Node: TRecordNode): Boolean;
begin
  Result := Node.Child <> nil;
end;

function TEzDBCursor.HasParent: Boolean;
begin
  Result := HasParent(FCurrentNode);
end;

function TEzDBCursor.HasParent(Node: TRecordNode): Boolean;
begin
  Result := Node.Parent <> nil;
end;

function TEzDBCursor.GetParentKey: TNodeKey;
var
  N: TRecordNode;
begin
  N := GetParent(FCurrentNode);
  if N <> FTopNode then
    Result := N.Key else
    Result := FTopNode.Key;
end;

function TEzDBCursor.NodeHasParent(Node, Parent: TRecordNode): Boolean;
begin
  Result := (Node <> nil) and (Node.Parent = Parent);

  while (Result = false) and (Node <> nil) do
    if Node.Parent = Parent then
      Result := true else
      Node := Node.Parent;
end;

procedure TEzDBCursor.Delete;
var
  Node: TRecordNode;

begin
  if Eof or Bof then EzDbCursorError(SInvalidNode, Self);
  Node := FCurrentNode;
  FCurrentNode := NextNode(FCurrentNode);
  DeleteNode(Node);
  if Eof and (FCurrentNode.Prev <> TopNode) then
    FCurrentNode := PrevNode(FCurrentNode);
end;

procedure TEzDBCursor.DeleteNode(Node: TRecordNode);
var
  N, P: TRecordNode;
begin
  CutOffNode(Node);

  N := Node;
  while N <> nil do
  begin
    while (N.Child <> nil) or (N.Next <> nil) do
    begin
      while N.Child <> nil do N := N.Child;
      while N.Next <> nil do N := N.Next;
    end;

    P := N;
    if N.Prev <> nil then
    begin
      N := N.Prev;
      N.Next := nil;
    end
    else if N.Parent <> nil then
    begin
      N := N.Parent;
      N.Child := nil;
    end
    else
      N := nil;

    FSortedKeys.DeleteKey(P.Key);
    FreeNode(P);
{
    Dec(FRecordCount);
    FRecNoValid := False;
}
  end;
end;

function TEzDBCursor.Distance(Node1, Node2: TRecordNode): Integer;
var
  N: TRecordNode;

  function GoOnce: Integer;
  begin
    Result := 0;
    N := Node1;
    while (N <> FEndNode) and (N <> Node2) do
    begin
      N := NextNode(N);
      Inc(Result);
    end;
    if (N = FEndNode) and (Node2 <> FEndNode) then Result := MaxInt;
  end;

begin
  Result := GoOnce;
  if Result = MaxInt then
  begin
    N := Node1;
    Node1 := Node2;
    Node2 := N;
    Result := GoOnce;
    if Result <> MaxInt then Result := -Result;
  end;
end;

function TEzDBCursor.GetRecordCount: Integer;
begin
  Result := FRecordCount;
end;

function TEzDBCursor.GetBof: Boolean;
begin
  Result := FCurrentNode = FTopNode;
end;

function TEzDBCursor.GetEof: Boolean;
begin
  Result := FCurrentNode = FEndNode;
end;

function TEzDBCursor.GetExpanded: Boolean;
begin
  Result := nfExpanded in FCurrentNode.Flags;
end;

function TEzDBCursor.GetDataset: TCustomEzDataset;
begin
  if Assigned(Collection) then
    Result := TEzDBCursors(Collection).Dataset else
    Result := nil;
end;

function TEzDBCursor.GetKey: TNodeKey;
begin
  Result := FCurrentNode.Key
end;

procedure TEzDBCursor.InitRecordNumbers;
var
  N: TRecordNode;
  I: Integer;
  SavedOptions: TCursorOptions;
begin
  SavedOptions := FOptions;
  try
    FOptions := SavedOptions - [cpShowAll];
    N := NextNode(FTopNode);
    I := 1;
    while (N <> FEndNode) do
    begin
      N.RecNo := I;
      Inc(I);
      N := NextNode(N);
    end;
  finally
    FOptions := SavedOptions;
  end;
  FRecNoValid := true;
end;

procedure TEzDBCursor.InternalAddNode(Position, NewNode: TRecordNode);
var
  N: TRecordNode;

  function Advance(N: TRecordNode; C: Integer) : TRecordNode;
  begin
    while C>0 do begin N := N.Next; Dec(C); end;
    while C<0 do begin N := N.Prev; Inc(C); end;
    Result := N;
  end;

  function CalcDistance(Node: TRecordNode) : Integer;
  begin
    Result := 1;
    while Node.Next <> nil do
    begin
      Node := Node.Next;
      Inc(Result);
    end;
    if Node = FEndNode then
      Dec(Result);
  end;

  function FindPos(NewNode: TRecordNode): TRecordNode;
  const
    Sort: array[TSortDirection] of integer = (-1, 1);

  var
    N: TRecordNode;
    L, H, I, C, Q: Integer;

  begin
    Result := Position;
    L := 0;
    H := CalcDistance(Position)-1;
    if H < 0 then Exit;
    N := Position;
    Q := 0;

    while (L <= H) do
    begin
      I := (L + H) shr 1;
      N := Advance(N, I-Q);
      Q:=I;
      C := Sort[FSortDirection]*CompareNodes(N, NewNode);
      if (C <= 0) then
        L := I + 1 else
        H := I - 1;
    end;

    Result := Advance(N, L-Q);
  end;

  procedure AddNodetoTree;
  begin
    NewNode.Parent := Position.Parent;
    N := Position.Next;
    NewNode.Prev := Position;
    Position.Next := NewNode;
    if N <> nil then
    begin
      NewNode.Next := N;
      N.Prev := NewNode;
    end;
  end;

  procedure InsertNodeInTree;
  begin
    if Position.Prev <> nil then
    begin
      NewNode.Parent := Position.Parent;
      N := Position.Prev;
      NewNode.Next := Position;
      Position.Prev := NewNode;
      if N <> nil then
      begin
        NewNode.Prev := N;
        N.Next := NewNode;
      end;
    end
    else // Special case: Insert new node as the first child of parent
    begin
      NewNode.Next := Position;
      Position.Prev := NewNode;
      NewNode.Parent := Position.Parent;
      NewNode.Parent.Child := NewNode;
    end;
  end;
var
  Saved: TRecordNode;

begin
  if nfShadowBefore in NewNode.Flags then
  begin
    Assert(Position<>nil);
    InsertNodeInTree;
  end
  else if nfShadowAfter in NewNode.Flags then
  begin
    Assert(Position<>nil);
    AddNodeToTree;
  end
  else if cpSorted in Options then
  begin
    // Set position on the first node of the branche
    if (Position = nil) or (Position.Parent = nil) then
      Position := FTopNode.Next else
      Position := Position.Parent.Child;

    Saved := Position;
    Position := FindPos(NewNode);
    if Position = nil then
      //
      // Adds node as last node of branche
      //
    begin
      Position := Saved;
      while Position.Next <> nil do
        Position := Position.Next;
      AddNodetoTree;
    end
    else
      InsertNodeInTree;
  end
  else
  begin
    if (Position = nil) or (Position.Parent = nil) then
      Position := FEndNode.Prev else
      while Position.Next <> nil do
        Position := Position.Next;

    AddNodeToTree;
  end;
end;

function TEzDBCursor.IsDefaultCursor: Boolean;
begin
  Result := Index = 0;
end;

function TEzDBCursor.IsExpanded: Boolean;
begin
  Result := Expanded;
end;

function TEzDBCursor.IsExpanded(Node: TRecordNode): Boolean;
begin
  Result := nfExpanded in Node.Flags;
end;

function TEzDBCursor.IsNodeVisible(Node: TRecordNode): Boolean;
begin
  Node := GetParent(Node);
  while (Node <> FTopNode) and (nfExpanded in Node.Flags) do
    Node := GetParent(Node);

  Result := Node = FTopNode;
end;

function TEzDBCursor.IsVisible: Boolean;
begin
  Result := IsNodeVisible(FCurrentNode);
end;

function TEzDBCursor.IsEmpty: Boolean;
begin
  Result := (FTopNode.Next = FEndNode) and (FTopNode.Child = nil);
end;

function TEzDBCursor.Level: Integer;
begin
  Result := Level(FCurrentNode);
end;

function TEzDBCursor.Level(Node: TRecordNode): integer;
begin
  Result := -1;
  if Node = nil then Exit;

  while Node <> FTopNode do
  begin
    Node := GetParent(Node);
    Inc(Result);
  end
end;

function TEzDBCursor.NodeRecNo(Node: TRecordNode): LongInt;
begin
  if not FRecNoValid then
    InitRecordNumbers;

  if Assigned(Node) then
    Result := Node.RecNo else
    Result := -1;
end;

function TEzDBCursor.NodeAtIndex(Value: Integer): TRecordNode;
var
  N: TRecordNode;
  SavedOptions: TCursorOptions;

begin
  if not FRecNoValid then
    InitRecordNumbers;

  SavedOptions := FOptions;
  try
    FOptions := SavedOptions - [cpShowAll];
    N := NextNode(FTopNode);
    while (N <> FEndNode) and (Value>0) do
    begin
      dec(Value);
      N := NextNode(N);
    end;
    Result := N;
  finally
    FOptions := SavedOptions;
  end;
end;

procedure TEzDBCursor.MoveFirst;
begin
  FCurrentNode := NextNode(FTopNode);
end;

procedure TEzDBCursor.MoveNext;
begin
  FCurrentNode := NextNode(FCurrentNode);
end;

procedure TEzDBCursor.MoveNextSibling;
begin
  if FCurrentNode.Next <> nil then
    FCurrentNode := FCurrentNode.Next
  else
    FCurrentNode := FEndNode;
end;

procedure TEzDBCursor.MoveLast;
begin
  FCurrentNode := PrevNode(FEndNode);
  if FCurrentNode = FTopNode then
    FCurrentNode := FEndNode;
end;

procedure TEzDBCursor.MoveParent;
begin
  FCurrentNode := GetParent(FCurrentNode);
end;

procedure TEzDBCursor.MovePrevious;
begin
  FCurrentNode := PrevNode(FCurrentNode);
end;

procedure TEzDBCursor.MovePrevSibling;
begin
  if FCurrentNode.Prev <> nil then
    FCurrentNode := FCurrentNode.Prev else
    FCurrentNode := FTopNode;
end;

procedure TEzDBCursor.MoveToKey(Value: TNodeKey);
begin
  FCurrentNode := FindNode(Value);
end;

procedure TEzDBCursor.MakeNodeVisible;
var
  bkm: TRecordNode;
begin
  bkm := CurrentNode;
  MoveParent;
  while not Bof do
  begin
    Expanded := true;
    MoveParent;
  end;
  CurrentNode := bkm;
end;

procedure TEzDBCursor.MoveNode(Node, Location: TRecordNode; Position: TEzInsertPosition);
begin
  if Node=Location then
    EzDbCursorError(SCannotMoveToItself, Self);

  CutOffNode(Node);

  if Position = ipInsertBefore then
  begin
    Node.Parent := Location.Parent; // Might be nil!
    Node.Prev := Location.Prev;     // Might be nil!
    Node.Next := Location;
    if Location.Prev <> nil then
      Location.Prev.Next := Node
    else if (Location.Parent <> nil) then
      Location.Parent.Child := Node;
    Location.Prev := Node;
  end
  else if Position = ipInsertAfter then
  begin
    Node.Parent := Location.Parent;
    Node.Prev := Location;
    Node.Next := Location.Next;
    if Location.Next <> nil then
      Location.Next.Prev := Node;
    Location.Next := Node;
  end;

  if IsNodeVisible(Node) then
    inc(FRecordCount, VisibleChilds(Node)+1);
end;

function TEzDBCursor.NodeHasTwin(Node: TRecordNode): TNodeFlags;
begin
  Result := [];
  if (Node.Next<>nil) and (nfShadowAfter in Node.Next.Flags) then
    Include(Result, nfShadowAfter);
  if (Node.Prev<>nil) and (nfShadowBefore in Node.Prev.Flags) then
    Include(Result, nfShadowBefore);
end;

function TEzDBCursor.NextNode(Node: TRecordNode): TRecordNode;
begin
  if ((nfExpanded in Node.Flags) or (cpShowAll in FOptions)) and (Node.Child <> nil) then
    Node := Node.Child

  else if (Node.Next <> nil) then
    Node := Node.Next
  else
  begin
    Node := GetParent(Node);
    while (Node <> FTopNode) and (Node.Next = nil) do Node := GetParent(Node);
    if Node = FTopNode then
      Node := FEndNode else
      Node := Node.Next;
  end;
  Result := Node;
end;

function TEzDBCursor.NodeType(Node:TRecordNode): TNodeType;
const
  Types : array[Boolean, Boolean] of TNodeType = ((ntSingle, ntRoot),(ntChild, ntParent));

begin
  Result := Types[HasParent(Node), HasChildren(Node)];
end;

function TEzDBCursor.NodeType: TNodeType;
begin
  Result := NodeType(FCurrentNode);
end;

function TEzDBCursor.PrevNode(Node: TRecordNode): TRecordNode;
begin
  Result := FTopNode;
  if Node = FTopNode then Exit;

  // Node is the first node of a branche, return it's parent.
  if (Node.Prev = nil) then
      Node := Node.Parent
  else
  begin
    Node := Node.Prev;
    if Node <> FTopNode then
    begin
      // Need to locate last visible node of current node.
      while ((nfExpanded in Node.Flags) or (cpShowAll in FOptions)) and
            (Node.Child <> nil) do
      begin
        Node := Node.Child;
        while Node.Next <> nil do Node := Node.Next;
      end;
    end;
  end;
  Result := Node;
end;

procedure TEzDBCursor.OpenCursor;
begin
  if Index=0 {Opening default cursor} then
  begin
    if not Dataset.FOpening then
      Dataset.Open
  end else
    Dataset.ActiveCursorIndex := Index;
end;

procedure TEzDBCursor.ReIndexNode(Node: TRecordNode);
var
  Parent: TRecordNode;
  IsCurrent: Boolean;

begin
  IsCurrent := Node = FCurrentNode;

  Parent := Node.Parent;

  if (Parent <> nil) and (Parent.Child = Node) and (Node.Next = nil) then Exit;

  CutOffNode(Node); // FRecordCount updated as well

  if Parent = nil then
    InternalAddNode(nil, Node) else
    InternalAddNode(Parent.Child, Node);

  if IsNodeVisible(Node) then
    inc(FRecordCount, VisibleChilds(Node)+1);

  if IsCurrent then
    FCurrentNode := Node;
end;

procedure TEzDBCursor.SetCurrentNode(Node: TRecordNode);
var
  n: TRecordNode;

begin
  if Node = FCurrentNode then
    Exit;

  if (Node = nil) or (Node = FEndNode) or (Node = FTopNode) then
  begin
    FCurrentNode := FEndNode;
    Exit;
  end;

  if (Dataset<>nil) and (eoCheckCursorPosition in Dataset.Options) then
  begin
    n := FindNode(Node.Key);
    if (n <> Node) then
      EzDbCursorError(SInvalidCursorPosition, Self);
  end;

  FCurrentNode := Node;
end;

procedure TEzDBCursor.SetCursorName(Name: string);
begin
  if IsDefaultCursor then Name := SDefaultCursorName;
  FCursorName := Name;
  Changed(False);
end;

procedure TEzDBCursor.SetExpanded(Value: Boolean);
begin
  if Eof or Bof or not HasChildren or
    (Value and (nfExpanded in FCurrentNode.Flags)) or
    (not Value and not (nfExpanded in FCurrentNode.Flags)) then Exit;

  if (FRecordCount > 0) and (nfExpanded in FCurrentNode.Flags) and IsNodeVisible(FCurrentNode) then
    Dec(FRecordCount, VisibleChilds(FCurrentNode));

  if Value then
    Include(FCurrentNode.Flags, nfExpanded) else
    Exclude(FCurrentNode.Flags, nfExpanded);

  if (nfExpanded in FCurrentNode.Flags) and IsNodeVisible(FCurrentNode) then
    Inc(FRecordCount, VisibleChilds(FCurrentNode));

  FRecNoValid := False;
end;

procedure TEzDBCursor.SetIndexFieldNames(Value: string);
begin
  if FIndexFieldNames<>Value then
  begin
    FIndexFieldNames := Value;
    if Active then
    begin
      CloseCursor;
      OpenCursor;
    end;
    Changed(False);
  end;
end;

procedure TEzDBCursor.SetKey(Key: TNodeKey);
begin
  FCurrentNode := FindNode(Key);
end;

procedure TEzDBCursor.SetParentKey(Key: TNodeKey);
var
  N: TRecordNode;
begin
  N := nil;
  if not VarIsNull(Key.Value) then
  begin
    N := FindNode(Key);
    if N = FEndNode then
      EzDbCursorError(SParentNotFound, Self);

  end;
  UpdateParent(N, FCurrentNode);
end;

procedure TEzDBCursor.SetOptions(O: TCursorOptions);
begin
  if O=FOptions then Exit;

  if (Index = 0) and (cpFiltered in O) then
    EzDbCursorError(SDefCursorCantFilter, Self);

  // Cannot have both cpAutoCreateParents and cpIgnore
  if (cpAutoCreateParents in O) and not (cpAutoCreateParents in FOptions) then
    O := O - [cpIgnore]
  else if (cpIgnore in O) and not (cpIgnore in FOptions) then
    O := O - [cpAutoCreateParents];

  if Active and
     (((FOptions * [cpSorted]) <> (O * [cpSorted])) or
     ((FOptions * [cpFiltered]) <> (O * [cpFiltered])))
  then
  begin
    CloseCursor;
    FOptions := O;
    OpenCursor;
  end else
    FOptions := O;
end;

procedure TEzDBCursor.SetSortDirection(Value: TSortDirection);
begin
  if Value<>FSortDirection then
  begin
    FSortDirection:=Value;
    if Active then
    begin
      CloseCursor;
      OpenCursor;
    end;
    Changed(False);
  end;
end;

procedure TEzDBCursor.SetRecNo(Value: Integer);
begin
  FCurrentNode := Advance(FTopNode, Value);
end;

procedure TEzDBCursor.UpdateKey(CurrentKey, NewKey: TNodeKey);
var
  r: TRecordNode;

begin
  r := FindNode(CurrentKey);
  if r = FEndNode then EzDbCursorError(SKeyNotFound, Self);
  FSortedKeys.DeleteKey(CurrentKey);
  r.Key := NewKey;
  FSortedKeys.Add(r);
end;

procedure TEzDBCursor.UpdateParent(NewParent, Node: TRecordNode);
var
  Position: TRecordNode;
  IsCurrent: Boolean;

begin
  if Node.Parent = NewParent then Exit;
  if Node = NewParent then EzDbCursorError(SCircularParent, Self);

  IsCurrent := FCurrentNode = Node;

  Position := AbsParent(Node);
  CutOffNode(Node); // Updates FRecordCount as well

  if NewParent = nil then
    InternalAddNode(Position, Node)

  else if NewParent.Child = nil then
  begin
    NewParent.Child := Node;
    Node.Parent := NewParent;
  end

  else
    InternalAddNode(NewParent.Child, Node);

  if IsNodeVisible(Node) then
    inc(FRecordCount, VisibleChilds(Node)+1);

  if IsCurrent then
    FCurrentNode := Node;
end;

function TEzDBCursor.VisibleChilds(Node: TRecordNode): Integer;
var
  N: TRecordNode;
  O: TCursorOptions;

begin
  if not (nfExpanded in Node.Flags) or (Node.Child = nil) then
  begin
    Result := 0;
    Exit;
  end;

  O := FOptions;
  FOptions := O - [cpShowAll];
  try
    Result := 0;
    N := Node.Child;
    while NodeHasParent(N, Node) do begin
      N := NextNode(N);
      Inc(Result);
    end;
  finally
    FOptions := O;
  end;
end;

//=---------------------------------------------------------------------------=
{ TEzBlobStream }
//=---------------------------------------------------------------------------=
constructor TEzBlobStream.Create(Field: TBlobField; Mode: TBlobStreamMode);
begin
  FField := Field;
  FFieldNo := FField.FieldNo - 1;
  FDataSet := FField.DataSet as TCustomEzDataSet;
  FFieldData := Null;
  FData := Null;
  if not FDataSet.GetActiveRecBuf(FBuffer) then Exit;
  if Mode <> bmRead then
  begin
    if FField.ReadOnly then
      DatabaseErrorFmt(SFieldReadOnly, [FField.DisplayName], FDataSet);
    if not (FDataSet.State in [dsEdit, dsInsert]) then
      DatabaseError(SNotEditing, FDataSet);
  end;
  if Mode = bmWrite then Truncate
  else ReadBlobData;
end;

destructor TEzBlobStream.Destroy;
begin
  if FModified then
  try
    FDataSet.SetFieldData(FField, @FData);
    FField.Modified := True;
    FDataSet.DataEvent(deFieldChange, Longint(FField));
  except
    Application.HandleException(Self);
  end;
  inherited Destroy;
end;

procedure TEzBlobStream.ReadBlobData;
var
  S: AnsiString;
begin
{$IFNDEF EZ_D5}
  FDataSet.GetFieldData(FField, @FFieldData);
{$ELSE}
  FDataSet.GetFieldData(FField, @FFieldData, True);
{$ENDIF}

{$IFDEF EZ_D2010}
  if not VarIsNull(FFieldData) then
  begin
    if VarType(FFieldData) = varOleStr then
    begin
      if FField.BlobType = ftWideMemo then
        Size := Length(WideString(FFieldData)) * sizeof(widechar)
      else
      begin
        { Convert OleStr into a pascal string (format used by TBlobField) }
        S := AnsiString(FFieldData);
        FFieldData := S;
        Size := Length(S);
      end;
    end else
      Size := VarArrayHighBound(FFieldData, 1) + 1;
    FFieldData := Null;
  end;
{$ELSE}
  if not VarIsNull(FFieldData) then
  begin
    if VarType(FFieldData) = varOleStr then
    begin
      { Convert OleStr into a pascal string (format used by TBlobField) }
      FFieldData := string(FFieldData);
      Size := Length(FFieldData);
    end else
      Size := VarArrayHighBound(FFieldData, 1) + 1;
    FFieldData := Null;
  end;
{$ENDIF}
end;

{$IFDEF EZ_D2010}
function TEzBlobStream.Realloc(var NewCapacity: Longint): Pointer;

  procedure VarAlloc(var V: Variant; Field: TFieldType);
  var
    W: WideString;
    S: AnsiString;
  begin
    if Field = ftMemo then
    begin
      if not VarIsNull(V) then
        S := AnsiString(V);
      SetLength(S, NewCapacity);
      V := S;
    end else
    if Field = ftWideMemo then
    begin
      if not VarIsNull(V) then
        W := WideString(V);
      SetLength(W, NewCapacity div 2);
      V := W;
    end else
    begin
      if VarIsClear(V) or VarIsNull(V) then
        V := VarArrayCreate([0, NewCapacity-1], varByte) else
        VarArrayRedim(V, NewCapacity-1);
    end;
  end;

begin
  Result := Memory;
  if NewCapacity <> Capacity then
  begin
    if VarIsArray(FData) then VarArrayUnlock(FData);
    if NewCapacity = 0 then
    begin
      FData := Null;
      Result := nil;
    end else
    begin
      if VarIsNull(FFieldData) then
        VarAlloc(FData, FField.DataType) else
        FData := FFieldData;
      if VarIsArray(FData) then
        Result := VarArrayLock(FData) else
        Result := TVarData(FData).VString;
    end;
  end;
end;
{$ELSE}
function TEzBlobStream.Realloc(var NewCapacity: Longint): Pointer;

  procedure VarAlloc(var V: Variant; StrField: Boolean);
  var
    S: string;
  begin
    if StrField then
    begin
      if not VarIsNull(V) then S := string(V);
      SetLength(S, NewCapacity);
      V := S;
    end else
    begin
      if VarIsEmpty(V) or VarIsNull(V) then
        V := VarArrayCreate([0, NewCapacity-1], varByte) else
        VarArrayRedim(V, NewCapacity-1);
    end;
  end;

begin
  Result := Memory;
  if NewCapacity <> Capacity then
  begin
    if VarIsArray(FData) then VarArrayUnlock(FData);
    if NewCapacity = 0 then
    begin
      FData := Null;
      Result := nil;
    end else
    begin
      if VarIsNull(FFieldData) then
        VarAlloc(FData, FField.DataType = ftMemo) else
        FData := FFieldData;
      if VarIsArray(FData) then
        Result := VarArrayLock(FData) else
        Result := TVarData(FData).VString;
    end;
  end;
end;
{$ENDIF}

function TEzBlobStream.Write(const Buffer; Count: Longint): Longint;
begin
  Result := inherited Write(Buffer, Count);
  FModified := True;
end;

procedure TEzBlobStream.Truncate;
begin
  Clear;
  FModified := True;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzDBCursors.Create(Dataset: TCustomEzDataset; ColumnClass: TCollectionItemClass);
begin
  FDataset := Dataset;
  inherited Create(ColumnClass);
end;

function TEzDBCursors.Add: TEzDBCursor;
begin
  Result := TEzDBCursor(inherited Add);
end;

procedure TEzDBCursors.EndUpdate;
begin
  inherited;
  if (UpdateCount = 0) and (Count = 0) or
     ((Count > 0) and (TEzDBCursor(Items[0]).CursorName <> SDefaultCursorName))
  then
    TEzDBCursor(Insert(0)).FCursorName := SDefaultCursorName;
end;

function TEzDBCursors.GetOwner: TPersistent;
begin
  Result := FDataset;
end;

function TEzDBCursors.GetEzDBCursor(Index: Integer): TEzDBCursor;
begin
  Result := TEzDBCursor(inherited Items[Index]);
end;

procedure TEzDBCursors.SetEzDBCursor(Index: Integer; Value: TEzDBCursor);
begin
  Items[Index].Assign(Value);
end;

//=---------------------------------------------------------------------------=
// TEzDatasetParser implementation
//=---------------------------------------------------------------------------=
constructor TEzDatasetParser.Create(D: TCustomEzDataset);
begin
  inherited Create(nil);
  Dataset := D;
  CaseSensitive := false;
end;

function TEzDatasetParser.DoGetIdentValue(ctype: TCalcType; const S: String; var Value: Double) : Boolean;
begin
  Result := Dataset.DoGetIdentValue(ctype, S, Value);
end;

function  TEzDatasetParser.DoGetStrIdentValue(ctype: TCalcType; const S: String; var Value: string) : Boolean;
begin
  Result := Dataset.DoGetStrIdentValue(ctype, S, Value);
end;

procedure TEzDatasetParser.DoGetRangeValues(const sRange, sVariable: string; ValueType: TValueType; ParamList: TStringList);
begin
  Dataset.DoGetRangeValues(sRange, sVariable, ValueType, ParamList);
end;

function TEzDatasetParser.DoUserFunction(const Func: string; Params: TStringList) : Double;
var
  Handled: Boolean;

begin
  Handled := False;
  if Assigned(Dataset.FOnUserFunction) then
    Dataset.FOnUserFunction(Dataset, Func, Params, Result, Handled);
  if not Handled then
    Result := inherited DoUserFunction(Func, Params);
end;

function TEzDatasetParser.DoStrUserFunction(const Func: string; Params: TStringList) : string;
var
  Handled: Boolean;

begin
  Handled := False;
  if Assigned(Dataset.FOnStrUserFunction) then
    Dataset.FOnStrUserFunction(Dataset, Func, Params, Result, Handled);
  if not Handled then
    Result := inherited DoStrUserFunction(Func, Params);
end;


//=---------------------------------------------------------------------------=
// TEzDatalinkDatalink implementation
//=---------------------------------------------------------------------------=
constructor TEzDetailDatalink.Create(EzDatalink: TEzDatalink);
begin
  inherited Create;
  FEzDatalink := EzDatalink;
end;

procedure TEzDetailDatalink.ActiveChanged;
begin
  inherited;
  FEzDatalink.LinkActiveChanged(Active);
end;

procedure TEzDetailDatalink.DataSetChanged;
begin
  inherited;
  FEzDatalink.LinkChanged;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=

procedure TEzDataLink.Assign(Source: TPersistent);
var
  lnk: TEzDataLink;
begin
  if Source is TEzDatalink then
  begin
    lnk := Source as TEzDatalink;
    DataSource := lnk.DataSource;
    FKeyField := lnk.FKeyField;
    FParentRefField := lnk.FParentRefField;
    FCanExpand := lnk.FCanExpand;

    if (Collection as TEzDatalinks).EzDataset.SelfReferencing then
    begin
      FFieldLinks[ntSingle].Assign(lnk.FFieldLinks[ntSingle]);
      FFieldLinks[ntRoot].Assign(lnk.FFieldLinks[ntRoot]);
      FFieldLinks[ntParent].Assign(lnk.FFieldLinks[ntParent]);
      FFieldLinks[ntChild].Assign(lnk.FFieldLinks[ntChild]);
    end else
      MDFields.Assign(lnk.MDFields);

  end else
    inherited Assign(Source);
end;

constructor TEzDataLink.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  FDatalink := TEzDetailDatalink.Create(Self);

  FFieldLinks[ntSingle] := TEzFieldLinks.Create(Self, 0);
  FFieldLinks[ntRoot] := TEzFieldLinks.Create(Self, 0);
  FFieldLinks[ntParent] := TEzFieldLinks.Create(Self, 0);
  FFieldLinks[ntChild] := TEzFieldLinks.Create(Self, 0);
  FSyncedWith := nil;
  FCanExpand := True;
end;

destructor TEzDataLink.Destroy;
begin
  inherited;
  FDatalink.Destroy;

  FFieldLinks[ntSingle].Destroy;
  FFieldLinks[ntRoot].Destroy;
  FFieldLinks[ntParent].Destroy;
  FFieldLinks[ntChild].Destroy;
end;

procedure TEzDataLink.LinkActiveChanged(Active: Boolean);
begin
  if not Active then
    (Collection as TEzDataLinks).EzDataset.Close;
end;

procedure TEzDataLink.LinkChanged;
begin
  FSyncedWith := nil;
end;

procedure TEzDataLink.DefineProperties(Filer: TFiler);
begin
  inherited;
  if (Collection as TEzDataLinks).EzDataset.SelfReferencing then
  begin
    Filer.DefineProperty('SingleNodeFields', ReadSingleNodeFields, WriteSingleNodeFields, true);
    Filer.DefineProperty('RootNodeFields', ReadRootNodeFields, WriteRootNodeFields, true);
    Filer.DefineProperty('ParentNodeFields', ReadParentNodeFields, WriteParentNodeFields, true);
    Filer.DefineProperty('ChildNodeFields', ReadChildNodeFields, WriteChildNodeFields, true);
  end
  else
    Filer.DefineProperty('FieldLinks', ReadMDFieldLinks, WriteMDFieldLinks, true);
end;

procedure TEzDataLink.ReadSelectedFieldLinks(Reader: TReader; Links: TEzFieldLinks);
var
  ALink: TEzFieldLink;
begin
  Links.Clear;

  Reader.ReadlistBegin;
  while not Reader.EndOfList do
  begin
    ALink := TEzFieldLink.Create('');
    ALink.ReadFieldLink(Reader);
    Links.Add(ALink);
  end;
  Reader.ReadlistEnd;
end;

procedure TEzDataLink.ReadMDFieldLinks(Reader: TReader);
begin
  ReadSelectedFieldLinks(Reader, MDFields);
end;

procedure TEzDataLink.ReadSingleNodeFields(Reader: TReader);
begin
  ReadSelectedFieldLinks(Reader, SingleNodeFields);
end;

procedure TEzDataLink.ReadRootNodeFields(Reader: TReader);
begin
  ReadSelectedFieldLinks(Reader, RootNodeFields);
end;

procedure TEzDataLink.ReadParentNodeFields(Reader: TReader);
begin
  ReadSelectedFieldLinks(Reader, ParentNodeFields);
end;

procedure TEzDataLink.ReadChildNodeFields(Reader: TReader);
begin
  ReadSelectedFieldLinks(Reader, ChildNodeFields);
end;

procedure TEzDataLink.SetOnRelocateDataset(Event: TLocateRecordEvent);
begin
  if Assigned(Collection) then
    (Collection as TEzDataLinks).EzDataset.OnRelocateDataLink := Event;
end;

procedure TEzDataLink.WriteSelectedFieldLinks(Writer: TWriter; Links: TEzFieldLinks);
var
  sl: TStringList;

  function IsDefaultMap(ALink: TEzFieldLink) : Boolean;
  begin
    if Assigned(DetailDataset) then
    begin
      if sl.IndexOf(ALink.FieldName) <> -1 then
        Result := ALink.Link = ALink.FieldName else
        Result := ALink.Link = '';
    end else
      Result := ALink.Link = '';
  end;

var
  i: integer;

begin
  sl := TStringList.Create;
  try
    if Assigned(DetailDataset) then
      DetailDataset.GetFieldNames(sl);

    Writer.WriteListBegin;
    with Links do
      for i:=0 to Count-1 do
        if not IsDefaultMap(Items[i]) then
          Items[i].WriteFieldLink(Writer);
    Writer.WriteListEnd;
  finally
    sl.Destroy;
  end;
end;

procedure TEzDataLink.WriteMDFieldLinks(Writer: TWriter);
begin
  WriteSelectedFieldLinks(Writer, MDFields);
end;

procedure TEzDataLink.WriteSingleNodeFields(Writer: TWriter);
begin
  WriteSelectedFieldLinks(Writer, SingleNodeFields);
end;

procedure TEzDataLink.WriteRootNodeFields(Writer: TWriter);
begin
  WriteSelectedFieldLinks(Writer, RootNodeFields);
end;

procedure TEzDataLink.WriteParentNodeFields(Writer: TWriter);
begin
  WriteSelectedFieldLinks(Writer, ParentNodeFields);
end;

procedure TEzDataLink.WriteChildNodeFields(Writer: TWriter);
begin
  WriteSelectedFieldLinks(Writer, ChildNodeFields);
end;

function TEzDataLink.GetActiveFieldLinks: TEzFieldLinks;
begin
  Result := nil;
end;

function TEzDataLink.GetDisplayName: string;
begin
  if Assigned(DataSource) then
    Result := DataSource.Name else
    Result := inherited GetDisplayName;
end;

procedure TEzDataLink.RelocateDataset(Node: TRecordNode);
begin
  if FSyncedWith = Node then Exit;

  FSyncedWith := nil;

  if Assigned(Collection) and Assigned((Collection as TEzDatalinks).EzDataset.OnRelocateDataLink) then
  begin
    (Collection as TEzDatalinks).EzDataset.OnRelocateDataLink(Self, Node.Key);
    FSyncedWith := Node;
  end
  else
    with DetailDataset do
    {
      KV: 20-2-2006
      Code removed, bookmarks are not used anyway
      if Assigned(Node.Key.Bookmark) then
      begin
        try
          GotoBookmark(Node.Key.Bookmark);
        except
          ShowMessage('Bookmark not found');
        end;
        FSyncedWith := Node;
      end
      else
    }
      begin
        if Locate(KeyField, Node.Key.Value, []) then
          FSyncedWith := Node else
          EzDatasetError(Format(SLocateFailed, [DisplayName]), EzDataset);
      end;
end;

function TEzDataLink.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

function TEzDataLink.GetDetailDataset: TDataset;
begin
  Result := FDatalink.Dataset;
end;

function TEzDataLink.GetEzDataset: TEzDataset;
begin
  if Assigned(Collection) then
    Result := (Collection as TEzDataLinks).EzDataset as TEzDataset else
    Result := nil;
end;

function TEzDataLink.GetFieldLinks(NodeType: TNodeType): TEzFieldLinks;
begin
  Result := nil;

  if (Collection as TEzDataLinks).EzDataset.SelfReferencing then
    Result := FFieldLinks[NodeType]
  else if (NodeType = ntSingle) then
    Result := FFieldLinks[ntSingle]
  else
    EzDatasetError(SLinkindexOutOfRange, (Collection as TEzDataLinks).EzDataset);
end;

function TEzDataLink.GetFieldLinkIndex(IntType: Integer): TEzFieldLinks;
begin
  Result := GetFieldLinks(TNodeType(IntType));
end;

function TEzDataLink.GetOnRelocateDataset: TLocateRecordEvent;
begin
  if Assigned(Collection) then
    Result := (Collection as TEzDataLinks).EzDataset.OnRelocateDataLink else
    Result := nil;
end;

procedure TEzDataLink.SetDataSource(Value: TDataSource);
begin
  if Value = FDatalink.Datasource then Exit;
  FDataLink.DataSource := Value;
  if Value <> nil then Value.FreeNotification((Collection as TEzDataLinks).EzDataset);
end;

function TEzDataLinks.Add: TEzDatalink;
begin
  Result := TEzDatalink(inherited Add);
  if Count > 1 then
    Items[0].ParentRefField := '';
end;

constructor TEzDataLinks.Create(EzDataset:TCustomEzDataset; ColumnClass: TCollectionItemClass);
begin
  FEzDataset := EzDataset;
  inherited Create(ColumnClass);
end;

procedure TEzDataLinks.FieldListChanged;
var
  i: integer;
begin
  for i:=0 to Count-1 do
    with GetEzDatalink(i) do
      if EzDataset.SelfReferencing then
      begin
        FieldLinks[ntSingle].Updated := false;
        FieldLinks[ntRoot].Updated := false;
        FieldLinks[ntParent].Updated := false;
        FieldLinks[ntChild].Updated := false;
    end else
      MDFields.Updated := false;
end;

procedure TEzDataLinks.DisableDetailDatasets;
var
  i: integer;

begin
  for i:=0 to Count-1 do
    Items[i].DetailDataset.DisableControls;
end;

procedure TEzDataLinks.EnableDetailDatasets;
var
  i: integer;

begin
  for i:=0 to Count-1 do
    Items[i].DetailDataset.EnableControls;
end;

function TEzDataLinks.FindDatalink(Dataset: TDataset): TEzDatalink;
var
  i: integer;
begin
  i:=0;
  Result := nil;
  while (i<Count) and (Result = nil) do
    if GetEzDatalink(i).DetailDataset = Dataset then
      Result := GetEzDatalink(i) else
      inc(i);
end;

function TEzDataLinks.GetOwner: TPersistent;
begin
  Result := FEzDataset;
end;

function TEzDataLinks.GetEzDatalink(Index: Integer): TEzDatalink;
begin
  if (Index < 0) or (Index >= Count) then
    EzDatasetError(Format(SInvalidDatalinkIndex, [Index]), EzDataset);
  Result := TEzDatalink(inherited Items[Index]);
end;

procedure TEzDataLinks.SetEzDatalink(Index: Integer; Value: TEzDatalink);
begin
  Items[Index].Assign(Value);
end;

procedure TEzDataLinks.Update(Item: TCollectionItem);
begin
  if (FEzDataset = nil) or (csLoading in FEzDataset.ComponentState) then Exit;
  FEzDataset.DatalinksChanged;
end;


//=---------------------------------------------------------------------------=
// TEzFieldLink
//=---------------------------------------------------------------------------=
constructor TEzFieldLink.Create(AName: string);
begin
  FieldName := AName;
end;

procedure TEzFieldLink.ReadFieldLink(Reader: TReader);
begin
  Reader.ReadListBegin;
  FieldName := Reader.ReadString;
  Link := Reader.ReadString;
  Reader.ReadListEnd;
end;

procedure TEzFieldLink.WriteFieldLink(Writer: TWriter);
begin
  Writer.WriteListBegin;
  Writer.WriteString(FieldName);
  Writer.WriteString(Link);
  Writer.WriteListEnd;
end;

//=---------------------------------------------------------------------------=
// TEzFieldLinks
//=---------------------------------------------------------------------------=
constructor TEzFieldLinks.Create(Link: TEzDatalink; itemcount: Integer);
begin
  inherited Create;
  FDataLink := Link;
  FUpdated := false;  
  FMemory := nil;
  FCapacity := 0;
  FCount := 0;
  SetCapacity(itemcount);
end;

destructor TEzFieldLinks.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TEzFieldLinks.Add(ALink: TEzFieldLink);
var
  Pos: Integer;

begin
  if FCount = FCapacity then SetCapacity(FCapacity+1);

  if FindLink(Pos, ALink.FieldName) then
    //
    // A link with given name already exists ==> ignore duplicates
    //
  begin
    PEzFieldLink(GetIndexPtr(Pos))^.Free;
    TEzFieldLink(GetIndexPtr(Pos)^) := ALink;
    Exit;
  end;

  if Pos < FCount then
  try
    inc(FCount);
    MoveMemory(GetIndexPtr(Pos+1), GetIndexPtr(Pos), (FCount-1-Pos) * SizeOf(TEzFieldLink));
  except
    InternalHandleException;
  end else
    Inc(FCount);

  TEzFieldLink(GetIndexPtr(Pos)^) := ALink;
end;

function TEzFieldLinks.Add(const FieldName: string) : TEzFieldLink;
begin
  Result := TEzFieldLink.Create(FieldName);
  try
    Add(Result);
  except
    Result.Destroy;
    raise;
  end;
end;

procedure TEzFieldLinks.Assign(Source: TPersistent);
var
  S: TEzFieldLinks;
  i: integer;

begin
  if Source is TEzFieldLinks then
  begin
    S := Source as TEzFieldLinks;
    Clear;
    Updated := true;
    S.Update;

    for i:=0 to S.Count-1 do
      with Add(S.Items[i].FieldName) do
        Link := S.Items[i].Link;

    Updated := false;
  end else
    inherited;
end;

procedure TEzFieldLinks.Clear;
var
  i: integer;
begin
  if FCount > 0 then
  begin
    for i:=0 to FCount-1 do
      TEzFieldLink(GetIndexPtr(i)^).Free;
    FCount := 0;
    SetCapacity(0);
  end;
end;

procedure TEzFieldLinks.Delete(Index: Integer);
begin
  { We are removing only one item. }
  if ValidIndex(index) then
  begin
    Items[Index].Free;
    TEzFieldLink(GetIndexPtr(Index)^) := nil;
    if (FCount > 1) and (Index < FCount) then
    begin
      try
        MoveMemory(GetIndexPtr(Index), GetIndexPtr(Index + 1), (FCount-1-Index) * SizeOf(TEzFieldLink));
      except
      end;
    end;
    Dec(FCount);
  end;
end;

function TEzFieldLinks.FindLink(var Index: Integer; const FieldName: string): Boolean;
var
  L, H, I, C: Integer;

begin
  if not Updated then Update;
  Result := False;
  L := 0;
  H := FCount - 1;
  while (L <= H) do
  begin
    I := (L + H) shr 1;
    C := CompareText(GetItems(I).FieldName, FieldName);
    if (C < 0) then
      L := I + 1
    else
    begin
      H := I - 1;
      if (C = 0) then
        Result := True;
    end;
  end;
  Index := L;
end;

function TEzFieldLinks.GetCount: integer;
begin
  if not Updated then Update;
  Result := FCount;
end;

function TEzFieldLinks.GetIndexPtr(index: Integer): Pointer;
begin
  Result := nil;
  if ValidIndex(index) then
    Result := Ptr(LongInt(FMemory) + (index*SizeOf(TEzFieldLink)));
end;

function TEzFieldLinks.GetEzFieldLinks(FieldName: string) : TEzFieldLink;
var
  i: integer;

begin
  if not Updated then Update;
  if FindLink(i, FieldName) then
    Result := TEzFieldLink(GetIndexPtr(i)^) else
    Result := nil;
end;

function TEzFieldLinks.GetItems(index: integer) : TEzFieldLink;
begin
  if not Updated then Update;
  Result := TEzFieldLink(GetIndexPtr(index)^);
end;

procedure TEzFieldLinks.InternalHandleException;
begin
  Clear;
  raise Exception.Create('general error in TEzFieldLinks.');
end;

procedure TEzFieldLinks.PutEzFieldLinks(FieldName: string; ALink: TEzFieldLink);
var
  i: integer;

begin
  if FindLink(i, FieldName) then
  begin
    Delete(i);
    Add(Alink);
  end;
end;

procedure TEzFieldLinks.PutItems(index: integer; ALink: TEzFieldLink);
begin
  if ValidIndex(index) then
  begin
    Delete(index);
    Add(ALink);
  end;
end;

procedure TEzFieldLinks.SetCapacity(NewCapacity: Integer);
begin
  ReallocMem(FMemory, NewCapacity * SizeOf(TEzFieldLink));
  FCapacity := NewCapacity;
end;

function TEzFieldLinks.ValidIndex(Index: Integer): Boolean;
begin
  Result := True;
  if (Index < 0) or (Index >= FCount) then
    raise Exception.Create('ArrayIndexError');
end;

procedure TEzFieldLinks.Update;
var
  i, n: integer;
  Name: string;
  ds: TCustomEzDataset;
  Names, DetailNames: TStringList;

begin
  if not Assigned(FDataLink.Collection) then Exit;
  ds := (FDataLink.Collection as TEzDatalinks).EzDataset;
  if ([csDestroying, csLoading] * ds.ComponentState) <> [] then exit;

  FUpdated := true; // Prevents recursive calling of Update
  Names := TStringList.Create;
  DetailNames  := TStringList.Create;

//  with FDataLink do
  try
    ds.GetFieldNames(Names);
    if Assigned(FDataLink.DetailDataset) then
      FDataLink.DetailDataset.GetFieldNames(DetailNames);

    for n:=0 to Names.Count-1 do
    begin
      Name := Names[n];
      if not FindLink(i, Name) then
        if DetailNames.IndexOf(Name) <> -1 then
          Add(Name).Link := Name else
          Add(Name);
    end;

    if Count <> Names.Count then
    begin
      n:=0;
      while n < Count do
        if Names.IndexOf(Items[n].FieldName) = -1 then
          Delete(n) else
          inc(n);
    end;
  finally
    Names.Destroy;
    DetailNames.Destroy;
  end;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TCustomEzDataset.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FEzStates := [];
  FOptions := [eoCascadeDelete, eoParentKeysRequired];
  FActiveCursorIndex := 0;
  FUseDefaultCursor := False;
  FDirectAccessStack := TList.Create;
//  FOldValueBuffer := nil;
  FFilterBuffer := 0;
  FInsertPosition := ipUndefined;
  FCursors := TEzDBCursors.Create(Self, TEzDBCursor);
  FCursors.Add.FCursorName := SDefaultCursorName;
  FModifiedFields := TList.Create;
  FEzDataLinks:= TEzDataLinks.Create(Self, TEzDatalink);
  FCalculateList := TList.Create;
  FParser := TEzDatasetParser.Create(Self);
  FGeneratedKeys := TList.Create;
end;

//=---------------------------------------------------------------------------=
// Node handling functions
//=---------------------------------------------------------------------------=
function TCustomEzDataset.AddNode(Key: TNodeKey; Location: TRecordNode; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
begin
  CheckActive;

  if ActiveCursor = DefaultCursor then
    Result := DefaultCursor.Add(Location, Key, Flags, AllowExpanded) else
    begin
      if Assigned(Location) then
        DefaultCursor.Add(DefaultCursor.FindNode(Location.Key), Key, Flags, AllowExpanded) else
        DefaultCursor.Add(nil, Key, Flags, AllowExpanded);
      Result := ActiveCursor.Add(Location, Key, Flags, AllowExpanded);
    end;
end;

function TCustomEzDataset.AddChildNode(Key: TNodeKey; Parent: TRecordNode; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
begin
  CheckActive;

  if ActiveCursor = DefaultCursor then
    Result := DefaultCursor.AddChild(Parent, Key, Flags, AllowExpanded) else
    begin
      if Assigned(Parent) then
        DefaultCursor.AddChild(DefaultCursor.FindNode(Parent.Key), Key, Flags, AllowExpanded) else
        DefaultCursor.AddChild(nil, Key, Flags, AllowExpanded);
      Result := ActiveCursor.AddChild(Parent, Key, Flags, AllowExpanded);
    end;
end;

procedure TCustomEzDataset.UpdateMaxPosition;
begin
  MaxPosition := MaxInt;
  DataEvent(deGetMaxPosition, 0);
  if MaxPosition = MaxInt then
    MaxPosition := -1;
end;

procedure TCustomEzDataset.DeleteNode(Node: TRecordNode);
begin
  DeleteNode(Node.Key);
end;

procedure TCustomEzDataset.DeleteNode(Key: TNodeKey);
var
  Node: TRecordNode;

begin
  CheckActive;
  if ActiveCursor <> DefaultCursor then
  begin
    Node := ActiveCursor.FindNode(Key);
    if Node <> ActiveCursor.FEndNode then
    begin
      if ActiveCursor.CurrentNode = Node then
        ActiveCursor.MovePrevious;
      ActiveCursor.DeleteNode(Node);
    end;
  end;

  Node := DefaultCursor.FindNode(Key);
  if Node <> DefaultCursor.FEndNode then
  begin
    if DefaultCursor.CurrentNode = Node then
      DefaultCursor.MovePrevious;
    DefaultCursor.DeleteNode(Node);
  end;

//  Code removed: Resyncing conflicts with InternalDelete method.
//  if not DirectAccess then
//    Resync([]);
end;

function TCustomEzDataset.InsertRecordNode(Key: TNodeKey; Location: TRecordNode; Flags: TNodeFlags; AllowExpanded: Boolean = true) : TRecordNode;
begin
  CheckActive;

  if ActiveCursor = DefaultCursor then
    Result := DefaultCursor.Insert(Location, Key, Flags, AllowExpanded) else
    begin
      if Assigned(Location) then
        DefaultCursor.Insert(DefaultCursor.FindNode(Location.Key), Key, Flags, AllowExpanded) else
        DefaultCursor.Insert(nil, Key, Flags, AllowExpanded);
      Result := ActiveCursor.Insert(Location, Key, Flags, AllowExpanded);
    end;
end;

function TCustomEzDataset.FindKey(Key: TNodeKey): TRecordNode;
begin
  CheckActive;
  Result := ActiveCursor.FindNode(Key);
end;

function TCustomEzDataset.LocateNode(Node: TRecordNode) : Boolean;
var
  RecBuf: TRecordBuffer;
  i: Integer;

begin
  CheckActive;

  Result := True;
  if DirectAccess then
  begin
    if ActiveCursor.CurrentNode = Node then Exit;
    Result := ActiveCursor.FindNode(Node.Key) <> ActiveCursor.EndNode;
    if not Result then Exit;
    ActiveCursor.CurrentNode := Node;
  end
  else
  begin
    CheckBrowseMode;

    // Try to locate Node in current buffers.
    // This prevents needless scrolling when locating a record that is
    // already visible.
    i:=0;
    while (i < BufferCount) do
    begin
      RecBuf := TRecordBuffer(Buffers[i]);
      if (PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent) and
         (PRecInfo(RecBuf)^.Bookmark = Node)
      then
      begin
        MoveBy(i-ActiveRecord);
        Exit;
      end;
      inc(i);
    end;

    // Node not found.
    Result := ActiveCursor.FindNode(Node.Key) <> ActiveCursor.EndNode;
    if not Result then Exit;
    ActiveCursor.CurrentNode := Node;
    ActiveCursor.MakeNodeVisible;
    Resync([]);
  end;
end;

function TCustomEzDataset.LocateNode(Key: TNodeKey) : Boolean;
var
  RecBuf: TRecBuf;
  Node: TRecordNode;

begin
  CheckActive;
  UpdateCursorPos;
  if CompareNodeKeys(ActiveCursor.Key, Key) = 0 then
  begin
    Result := True;
    Exit;
  end;

  Node := ActiveCursor.FindNode(Key);
  Result := Node <> ActiveCursor.EndNode;
  if not Result then Exit;

  if DirectAccess then
    ActiveCursor.CurrentNode := Node

  else if GetActiveRecBuf(RecBuf) then
    if (PRecInfo(RecBuf)^.BookmarkFlag = bfInserted) or
       (PRecInfo(RecBuf)^.Bookmark <> Node)
    then
    begin
      ActiveCursor.CurrentNode := Node;
      ActiveCursor.MakeNodeVisible;
      Resync([rmCenter]);
    end;
end;

function TCustomEzDataset.MapDefaultFields: Boolean;

  procedure MapFields(DLink: TEzDatalink; FLinks: TEzFieldLinks);
  var
    f: integer;
    Names: TStringList;

  begin
    with DLink do
    begin
      if not Assigned(DetailDataset) then Exit;
      Names := TStringList.Create;

      try
        DetailDataset.GetFieldNames(Names);

        for f:=0 to FLinks.Count-1 do
          with FLinks.Items[f] do
            if Link = '' then
              if Names.IndexOf(FieldName) <> -1 then
              begin
                Result := true;
                Link := FieldName;
              end;
      finally
        Names.Destroy;
      end;
    end;
  end;

var
  l: integer;

begin
  Result := false;
  if SelfReferencing then
  begin
    MapFields(EzDataLinks[0], EzDataLinks[0].SingleNodeFields);
    MapFields(EzDataLinks[0], EzDataLinks[0].RootNodeFields);
    MapFields(EzDataLinks[0], EzDataLinks[0].ParentNodeFields);
    MapFields(EzDataLinks[0], EzDataLinks[0].ChildNodeFields);
  end
  else
  begin
    for l := 0 to EzDataLinks.Count-1 do
      MapFields(EzDataLinks[l], EzDataLinks[l].MDFields);
  end;
end;

procedure TCustomEzDataset.MoveNode(Node, Parent: TRecordNode);
begin
  CheckActive;

  if FActiveCursorIndex = 0 then
    DefaultCursor.UpdateParent(Parent, Node)
  else
  begin
    with DefaultCursor do
      if Parent <> nil then
        UpdateParent(FindNode(Parent.Key), FindNode(Node.Key)) else
        UpdateParent(nil, FindNode(Node.Key));

    ActiveCursor.UpdateParent(Parent, Node);
  end;
end;

procedure TCustomEzDataset.MoveRecord(Node, Location: TRecordNode; Position: TEzInsertPosition);
var
  NullKey: TNodeKey;

begin
  CheckActive;

  if Location = nil then
  begin
    Location := ActiveCursor.EndNode;
    Position := ipInsertBefore;
  end;

  if Assigned(FBeforeMoveRecord) then
    FBeforeMoveRecord(Self, Node, Location, Position);

  StartDirectAccess([cpShowAll]);
  try
    if (Position = ipInsertAsChild) then
    begin
      RecordNode := Node;
      Parent := Location.Key;
    end
    else if (Node.Parent <> Location.Parent) then
    begin
      RecordNode := Node;
      if Location.Parent = nil then
      begin
        NullKey.Value := Null;
        NullKey.Level := 0;
        Parent := NullKey;
      end
      else
        Parent := Location.Parent.Key;
    end else
      ActiveCursor.MoveNode(Node, Location, Position);

  finally
    EndDirectAccess;
  end;
  // Refresh current view without scrolling records.
  SetTopNode(TopNode);

  if Assigned(FAfterMoveRecord) then
    FAfterMoveRecord(Self, Node, Location, Position);
end;

function TCustomEzDataset.KeyExists(Key: TNodeKey) : Boolean;
begin
  CheckActive;

  Result := ActiveCursor.FindNode(Key) <> ActiveCursor.EndNode;
end;

//=---------------------------------------------------------------------------=
// End of node handling functions
//=---------------------------------------------------------------------------=
{$IFDEF XE2}
procedure TCustomEzDataset.DataEvent(Event: TDataEvent; Info: NativeInt);
{$ELSE}
procedure TCustomEzDataset.DataEvent(Event: TDataEvent; Info: Integer);
{$ENDIF}
begin
  case Event of
    deLayoutChange:
    begin
      if Active and Assigned(Reserved) and
        (FieldListCheckSum(Self) <> Integer(Reserved)) then
        Reserved := nil;
    end;

    deUpdateState:
      if State=dsInactive then
        FParser.ClearCache;

    deFieldListChange:
      if not (csLoading in ComponentState) then
        EzDatalinks.FieldListChanged;

  end;
  inherited;
end;

procedure TCustomEzDataset.DatalinksChanged;
begin
  DataEvent(deLayoutChange, 0);
end;

procedure TCustomEzDataset.CollapseAll;
var
  cpSaved: TCursorOptions;
  bkCursor: TRecordNode;

begin
  CheckActive;

  with ActiveCursor do
  begin
    SyncCursor;
    bkCursor := CurrentNode;
    cpSaved := Options;
    Options := cpSaved + [cpShowAll];

    try
      MoveFirst;
      while not Eof do
      begin
        Expanded := false;
        MoveNext;
      end;
    finally
      CurrentNode := bkCursor;
      MakeNodeVisible;
      Options := cpSaved;
      Refresh;
    end;
  end;
end;

procedure TCustomEzDataset.EndDirectAccess;
var
  P: PDBState;
  Node: TRecordNode;

begin
  if FDirectAccessStack.Count = 0 then Exit;
  P := FDirectAccessStack.Last;

  ActiveCursor.Options := P^.CursorOptions;
  FUseDefaultCursor := P^.UseDefaultCursor;

  // Restore position
  Node := ActiveCursor.FindNode(P^.Key);

  // Node can't be found, probably deleted during direct access
  if Node<>ActiveCursor.EndNode then
  begin
    ActiveCursor.Key := P^.Key;

    // KV: 30 Jan 2008
    // Next line dissabled. Why on earth do I reset the flags
    // of the current node????
    // ActiveCursor.CurrentNode.Flags := P^.NodeFlags;
  end else
    ActiveCursor.MoveFirst;

  Finalize(P^);
  Dispose(P);
  FDirectAccessStack.Delete(FDirectAccessStack.Count-1);

  if (FDirectAccessStack.Count = 0) then
  begin
    CursorPosChanged;
    if (esResyncPending in FEzStates) then
      Resync([]);
  end;
end;

procedure TCustomEzDataset.ExpandAll;
var
  cpSaved: TCursorOptions;
  bkCursor: TRecordNode;

begin
  CheckActive;

  with ActiveCursor do
  begin
    bkCursor := CurrentNode;
    cpSaved := Options;
    Options := cpSaved + [cpShowAll];

    try
      MoveFirst;
      while not Eof do
      begin
        if SelfReferencing or EzDatalinks[Level].CanExpand then
          Expanded := true;
        MoveNext;
      end;
    finally
      CurrentNode := bkCursor;
      Options := cpSaved;
      Refresh;
    end;
  end;
end;

destructor TCustomEzDataset.Destroy;
begin
  Destroying;
  inherited Destroy;
  FDirectAccessStack.Destroy;
  FModifiedFields.Destroy;
  FEzDataLinks.Destroy;
  FCursors.Destroy;
  FCalculateList.Destroy;
  FParser.Destroy;
  FGeneratedKeys.Destroy;
end;

procedure TCustomEzDataset.LoadCursorData(Cursor: TEzDBCursor);
var
  bkCursor: TRecordNode;
  cpSaved: TCursorOptions;
  Accept, DoFilter, SkippedParent: Boolean;
  SaveState: TDataSetState;
  GetMode: TGetMode;
  GetResult: TGetResult;
  SkippedParents: array of TRecordNode;
  X,I: Integer;

begin
  if (cpSorted in Cursor.Options) and (Cursor.IndexFieldNames <> '') then
    FIndexFields := Cursor.IndexFieldNames else
    Cursor.FOptions := Cursor.FOptions - [cpSorted];

  if DefaultCursor.IsEmpty then Exit;
  if not (eoUseMDRelations in Options) then
    EzDatalinks.DisableDetailDatasets;
  DisableControls;
  cpSaved := DefaultCursor.Options;
  Include(FEzStates, esCursorLoading);

  try
    try
      with DefaultCursor do
      begin
        Options := cpSaved + [cpShowAll];

        // Copy nodes from DefaultCursor into activated cursor
        MoveFirst;
        GetResult := grOK;
        DoFilter := (FActiveCursorIndex <> 0) and
                    (cpFiltered in FCursors[FActiveCursorIndex].Options) and
                    Assigned(OnFilterRecord);

        while GetResult = grOK do
        begin
          SkippedParent := False;

          if DoFilter then
          begin
            GetMode := gmCurrent;
            FFilterBuffer := TempBuffer;
            SaveState := SetTempState(dsFilter);
            try
              repeat
                Accept := True;
                GetResult := InternalGetRecord(FFilterBuffer, GetMode, False);

                if GetResult = grOK then
                  OnFilterRecord(Self, Accept);

                // If we skip this record and this record is a parent then
                // we may have to re-insert this parent later when one of
                // it's childs is visible.
                if not Accept and DefaultCursor.HasChildren then
                  SkippedParent := True;

                // if we go again, then fetch next record
                if not Accept then
                  GetMode := gmNext;

              until Accept or (GetResult <> grOK);
            except
              Application.HandleException(Self);
            end;
            RestoreState(SaveState);
          end;

          if GetResult = grOK then
          begin
            // Remember current cursor position, because adding a node to the new
            // cursor might update position of DefaultCursor.
            bkCursor := CurrentNode;

            // Insert all parents we have skipped
            if SkippedParent then
            begin
              MoveParent;

              SetLength(SkippedParents, Level+1);
              X:=0;

              while not Bof and (Cursor.FindNode(Key) = Cursor.EndNode) do
              begin
                SkippedParents[X] := CurrentNode;
                Inc(X);
                MoveParent;
              end;

              // Insert skipped parents in reverse order.
              for I:=X-1 downto 0 do
              begin
                CurrentNode := SkippedParents[I];
                Cursor.Add(ParentKey, Key, CurrentNode.Flags);
              end;

              // Return to saved position
              CurrentNode := bkCursor;
            end;

            // Copy node from DefaultCursor into Cursor.
            // Cursor's bookmark points to DefaultCursor.CurrentNode
            Cursor.Add(ParentKey, Key, CurrentNode.Flags);
            // Return to saved position
            CurrentNode := bkCursor;
            MoveNext;
            if Eof then
              GetResult := grEof;
          end;
        end;
      end;
    except
      raise;
    end;
    Cursor.MoveFirst;
  finally
    Exclude(FEzStates, esCursorLoading);
    DefaultCursor.Options := cpSaved;
    EnableControls;
    if not (eoUseMDRelations in Options) then
      EzDatalinks.EnableDetailDatasets;
  end;
end;

procedure TCustomEzDataset.Loaded;
begin
  inherited;
end;

procedure TCustomEzDataset.LoadDefaultCursorData;
var
  NodeKey: TNodeKey;
  Bkm: {$IFDEF EZ_D2009}TBookmark{$ELSE}TBookmarkStr{$ENDIF};

  procedure LoadDatalink(ALevel: Integer);
  var
    nkRecord, nkParent: TNodeKey;

  begin
    with FEzDataLinks[ALevel] do
    begin
      nkParent.Value := Null;

      if SelfReferencing then
      begin
        nkRecord.Level := -1;
        nkParent.Level := -1;
      end
      else
      begin
        nkRecord.Level := ALevel;
        nkParent.Level := ALevel-1;
      end;

      // DetailDataset.DisableControls;
      with DetailDataset do
      try
        First;
        if Eof then Exit;
        // Test whether Keys between this level and the
        // higher level are compatible
        if (ALevel>0) or SelfReferencing then
        begin
          with FEzDataLinks[max(ALevel-1, 0)] do
            nkRecord.Value := DetailDataset.FieldValues[KeyField];

          nkParent.Value := DetailDataset.FieldValues[ParentRefField];
          if (VarIsArray(nkRecord.Value) <> VarIsArray(nkParent.Value)) or
             (VarIsArray(nkRecord.Value) and (VarArrayHighBound(nkRecord.Value, 1) <> VarArrayHighBound(nkParent.Value, 1)))
          then
            EzDatasetError(SIncompatibleKeys, Self);
        end;

        while not Eof do
        begin
          nkRecord.Value := DetailDataset.FieldValues[KeyField];
          if ParentRefField <> '' then
          begin
            nkParent.Value := DetailDataset.FieldValues[ParentRefField];
            if VarSafeIsNull(nkParent.Value) then
              nkParent.Value := Null;
          end;

          if (ALevel > 0) and VarIsNull(nkParent.Value) and (eoParentKeysRequired in Options) then
          begin
            try
              EzDatasetError(Format(SParentRequired, [string(nkRecord.Value)]), Self);
            except
              EzDatasetError(Format(SParentRequired, ['unknown key']), Self);
            end;
          end
          else
          begin
            Bkm := Bookmark;
            DefaultCursor.CurrentNode := DefaultCursor.Add(nkParent, nkRecord, [], CanExpand);
            Bookmark := Bkm;
          end;

          Next;
        end;
      finally
        // EnableControls;
      end;
    end;
  end;

  procedure AddRecord(ALevel: Integer; ParentNode: TRecordnode);
  var
    KeyField: TField;

  begin
    with FEzDataLinks[ALevel].DetailDataset do
    begin
      KeyField := FieldByName(FEzDataLinks[ALevel].KeyField);
      while not Eof do
      begin
        NodeKey.Level := ALevel;
        NodeKey.Value := KeyField.Value;

        with DefaultCursor do
          if Assigned(ParentNode) then
            CurrentNode := AddChild(ParentNode, NodeKey, []) else
            CurrentNode := Add(nil, NodeKey, []);

        // Load detail records for current record
        if ALevel < FEzDataLinks.Count-1 then
          AddRecord(ALevel+1, DefaultCursor.CurrentNode);

        Next;
      end;
    end;
  end;

var
  i: integer;

begin
  NodeKey.Level := 0;

  if not (eoUseMDRelations in Options)  then
    EzDatalinks.DisableDetailDatasets;

  include(FEzStates, esDefaultCursorLoading);
  try
    if (cpSorted in DefaultCursor.Options) and (DefaultCursor.IndexFieldNames <> '') then
      FIndexFields := DefaultCursor.IndexFieldNames else
      DefaultCursor.FOptions := DefaultCursor.FOptions - [cpSorted];

    // Load set of keys from Master table if:
    // * Data is stored in a self referencing table in which case
    //   we only need to load data from the first table.
    // * We are not required to honour master / detail relations set
    //   between tables.
    if SelfReferencing or not (eoUseMDRelations in Options) then
      LoadDatalink(0);

    if not SelfReferencing then
      // Continue loading other tables..
      if not (eoUseMDRelations in Options) then
        for i := 1 to FEzDataLinks.Count-1 do
          LoadDatalink(i)
      else
      //
      // eoUseMDRelations is set!
      // Load data by using Master / detail relations set between tables.
      //
      begin
        MasterDataset.First;
        AddRecord(0, nil);
      end;
  finally
    exclude(FEzStates, esDefaultCursorLoading);
    if not (eoUseMDRelations in Options)  then
      EzDatalinks.EnableDetailDatasets;
  end;
end;

procedure TCustomEzDataset.Notification(AComponent: TComponent; Operation: TOperation);
var
  i: integer;
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (AComponent = DataSource) then
    for i:=0 to EzDataLinks.Count-1 do
      if EzDataLinks[i].DataSource = AComponent then
        EzDataLinks[i].DataSource := nil;
end;

procedure TCustomEzDataset.InternalOpen;
var
  i: integer;

begin
{$IFDEF DEMO}
  if not SplashDidShow then
  begin
    ShowAboutDialog;
    SplashDidShow := true;
  end;
{$ENDIF}

  // Make sure IsCursorOpen returns True
  FOpening := True;

  if not Assigned(MasterDataSet) then
    DatabaseError(SNoDataSet, Self);

  BookmarkSize := SizeOf(TNodeKey);

  for i:=0 to FEzDataLinks.Count-1 do begin
    if FEzDataLinks[i].DetailDataset = Self then
      EzDatasetError(SRecursiveDataset, Self);
    FEzDataLinks[i].DetailDataset.Open;
  end;

  FieldDefs.Updated := False;
  FieldDefs.Update;

  if DefaultFields then CreateFields;
  LayoutFieldMap;

  Reserved := Pointer(FieldListCheckSum(Self));
  BindFields(True);
  FRecBufSize := SizeOf(TRecInfo) + (Fields.Count * SizeOf(Variant));
end;

procedure TCustomEzDataset.InternalClose;
var
  i: integer;
  pn: PNodeKey;

begin
//  if FOldValueBuffer <> nil then
//  begin
//    FreeRecordBuffer(FOldValueBuffer);
//    FOldValueBuffer := nil;
//  end;

  for i:=0 to FGeneratedKeys.Count-1 do
  begin
    pn := FGeneratedKeys[i];
    Finalize(pn^);
    Dispose(pn);
  end;
  FGeneratedKeys.Clear;

  BindFields(False);
  if DefaultFields then DestroyFields;
end;

procedure TCustomEzDataset.CheckCursor;
begin
  if not IsCursorOpen then DatabaseError(SNoActiveCursor, Self);
end;

procedure TCustomEzDataset.CloseCursor;
begin
  if (FActiveCursorIndex <> 0) then
    ActiveCursor.Clear;
  DefaultCursor.Clear;
  inherited;
end;

//=---------------------------------------------------------------------------=
//
// Insert + Append methods
//
//=---------------------------------------------------------------------------=
procedure TCustomEzDataset.Add;

  //
  // Add a record below current record.
  // If current record is a child then the new record will become a
  // child of the same parent.
  //

begin
  CheckBrowseMode;

  FInsertPosition := ipInsertAfter;
  FInsertNode := RecordNode;

  if HasChildren and Expanded then
    // Move to last child
    MoveBy(ActiveCursor.VisibleChilds(RecordNode));

  if not IsEmpty and (MoveBy(1) = 1) then
    Insert else
    Append;
end;

procedure TCustomEzDataset.AddChild;

  //
  // Add a record as a child of the current record.
  //

begin
  CheckBrowseMode;

  if IsEmpty or (not SelfReferencing and (Level = MaxLevel)) then
    EzDatasetError(SCannotInsertChild, Self);

  if DirectAccess then
  begin
    // KV: 4 april 2006 next line dissabled
    // ActiveCursor.Expanded := True;
    FInsertPosition := ipInsertAsChild;
    FInsertNode := ActiveCursor.CurrentNode;
    Insert;
  end
  else if HasChildren then
  begin
    if not Expanded then
      Expanded := True;
    MoveBy(1);
    Insert;
  end
  else
  begin
    FInsertPosition := ipInsertAsChild;
    FInsertNode := RecordNode;
    if (MoveBy(1) = 1) then
      Insert else
      Append;
  end;
end;

procedure TCustomEzDataset.InternalInsert;
var
  RecBuf: TRecBuf;

begin
  GetActiveRecBuf(RecBuf);
  case PRecInfo(RecBuf)^.BookmarkFlag of
    bfInserted, bfCurrent:
    begin
      if FInsertPosition = ipUndefined then
      begin
        FInsertPosition := ipInsertBefore;
        FInsertNode := PRecInfo(RecBuf)^.Bookmark;
      end;
      SetBookmarkFlag(RecBuf, bfInserted);
    end;
    bfEof:
    begin
      if FInsertPosition = ipUndefined then
      begin
        FInsertPosition := ipAppend;
        FInsertNode := nil;
      end;
    end;
    else
    begin
      FInsertPosition := ipInsertBefore;
      FInsertNode := nil;
    end;
  end;

  inherited;
end;

procedure TCustomEzDataset.DoAfterOpen;
begin
  FOpening := False;
  inherited;
end;

procedure TCustomEzDataset.DoOnNewRecord;
var
  lvl, i, n: Integer;
  FieldLinks: TEzFieldLinks;
  ParentField: string;
  Fields: TList;

begin
  FModifiedFields.Clear;
//
//  if FOldValueBuffer = nil then
//    FOldValueBuffer := AllocRecordBuffer;
//
//  InitRecord(FOldValueBuffer);

  if FInsertNode <> nil then
    //
    // Try to fill the parent reference field with the parent key
    //
  begin
    if SelfReferencing then
    begin
      lvl := 0;
      FieldLinks := EzDataLinks[0].FieldLinks[ActiveCursor.NodeType];
    end
    else
    begin
      lvl := ActiveCursor.Level(FInsertNode);
      if FInsertPosition = ipInsertAsChild then
        inc(lvl);
      FieldLinks := EzDataLinks[lvl].MDFields;
    end;

    ParentField := FEzDataLinks[lvl].ParentRefField;
    if ParentField <> '' then
    begin
      Fields := TList.Create;
      try
        FEzDataLinks[lvl].DetailDataset.GetFieldList(Fields, ParentField);
        i:=0;
        while (i<FieldLinks.Count) do
        begin
          if Fields.Count > 1 then
          begin
            for n:=0 to Fields.Count-1 do
              if (FieldLinks.Items[i].Link = TField(Fields[n]).FieldName) then
              begin
                if FInsertPosition = ipInsertAsChild then
                  FieldValues[FieldLinks.Items[i].FieldName] := FInsertNode.Key.Value[n]
                else if FInsertNode.Parent <> nil then
                  FieldValues[FieldLinks.Items[i].FieldName] := FInsertNode.Parent.Key.Value[n]
              end;
          end
          else if (FieldLinks.Items[i].Link = TField(Fields[0]).FieldName) then
          begin
            if FInsertPosition = ipInsertAsChild then
              FieldValues[FieldLinks.Items[i].FieldName] := FInsertNode.Key.Value
            else if FInsertNode.Parent <> nil then
              FieldValues[FieldLinks.Items[i].FieldName] := FInsertNode.Parent.Key.Value
          end;
          inc(i);
        end;
      finally
        Fields.Free;
      end;
    end;
  end;

  inherited DoOnNewRecord;
end;


procedure TCustomEzDataset.InsertChild;
begin
  AddChild;
end;
//=---------------------------------------------------------------------------=
//
// End of Insert + Append methods
//
//=---------------------------------------------------------------------------=
function TCustomEzDataset.DoGetIdentValue(ctype: TCalcType; const S: String; var Value: Double) : Boolean;
var
  Field: TField;
  V: Variant;

begin
  Field := FindField(S);
  Value := 0.0;
  Result := True;

  if Field = nil then // Must be a variable...
  begin
    DoGetVariableValue(S, V);
    Value := double(V);
  end
  else
  begin
    if Field.DataType = ftBoolean then
      Value := 0+Ord(Field.AsBoolean) else
      Value := Field.AsFloat;
  end;
end;

function TCustomEzDataset.DoGetStrIdentValue(ctype: TCalcType; const S: String; var Value: string) : Boolean;
var
  Field: TField;
  V: Variant;

begin
  Field := FindField(S);
  Value := '';
  Result := False;

  if Field = nil then // Must be a variable...
  begin
    DoGetVariableValue(S, V);
    if not VarIsNull(Result) then
    begin
      Value :=  string(V);
      Result := True;
    end;
  end
  else
  begin
    Result := True;
    Value := Field.AsString;
  end;
end;

procedure TCustomEzDataset.DoGetRangeValues(const sRange, sVariable: string; ValueType: TValueType; ParamList: TStringList);
var
  Field: TField;
  Node, Stop: TRecordNode;
  IgnoreNulls: Boolean;
  sFunc: string;
  P: Integer;

  procedure GetThisValue;
  var
    V: Variant;

  begin
    if Field = nil then // Must be a variable...
    begin
      DoGetVariableValue(sVariable, V);
      if V = Null then
      begin
        if IgnoreNulls then Exit;
        raise EFormulaError.Create(Format(SNoValue, [sVariable]));
      end;
      if ValueType = vtNummeric then
        ParamList.Add(FloatToStr(double(V))) else
        ParamList.Add(VarToStr(V));
    end

    else if IgnoreNulls and Field.IsNull then
      Exit
    else if Field.DataType = ftBoolean then
      ParamList.Add(IntToStr(Integer(Field.AsBoolean)))
    else if ValueType = vtNummeric then
      ParamList.Add(FloatToStr(Field.AsFloat))
    else
      ParamList.Add('''' + Field.AsString + '''');
  end;

begin
  Field := FindField(sVariable);

  P := Pos('NOTNULL', sRange);
  if P=0 then
  begin
    sFunc := sRange;
    IgnoreNulls := False;
  end
  else
  begin
    IgnoreNulls := True;
    sFunc := Copy(sRange, 1, P-1);
  end;

  if sFunc = 'THIS' then
    GetThisValue

  else if sFunc = 'CHILDS' then
  begin
    with DefaultCursor do
      if HasChildren then
      begin
        Node := CurrentNode;
        if Assigned(Node.Next) then
          Stop := Node.Next else
          begin
            Stop := Node.Parent;
            while not Assigned(Stop.Next) do
              Stop := Stop.Parent;
            Stop := Stop.Next;
          end;

        try
          MoveNext;
          while CurrentNode <> Stop do
          begin
            if not HasChildren then
              GetThisValue;
            MoveNext;
          end;
        finally
          CurrentNode := Node;
        end;
      end;
  end
  else if sFunc = 'PARENT' then
  begin
    Node := DefaultCursor.CurrentNode;
    try
      DefaultCursor.MoveParent;
      if not DefaultCursor.Bof then
        GetThisValue;
    finally
      DefaultCursor.CurrentNode := Node;
    end;
  end
  else if sFunc = 'PARENTS' then
  begin
    Node := DefaultCursor.CurrentNode;
    try
      DefaultCursor.MoveParent;
      while not DefaultCursor.Bof do
      begin
        GetThisValue;
        DefaultCursor.MoveParent;
      end;
    finally
      DefaultCursor.CurrentNode := Node;
    end;
  end;
end;

procedure TCustomEzDataset.DoGetVariableValue(const Variable: string; var Value: Variant);
var
  n: TRecordNode;
begin
  n := ActiveCursor.CurrentNode;
  try
    if Assigned(FOnGetVariableValue) then FOnGetVariableValue(Self, Variable, Value);
  finally
    ActiveCursor.CurrentNode := n;
  end;
end;

function TCustomEzDataset.IsCursorOpen: Boolean;
begin
  Result := FOpening or (State<>dsInactive);
end;

function TCustomEzDataset.HasParentRecord: Boolean;
var
  RecBuf: TRecBuf;

begin
  CheckActive;
  Result := false;

  if DirectAccess then
    Result := ActiveCursor.HasParent

  else if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := ActiveCursor.HasParent(PRecInfo(RecBuf)^.Bookmark);
end;

function TCustomEzDataset.HasChildren: Boolean;
var
  RecBuf: TRecBuf;

begin
  CheckActive;
  Result := false;

  if DirectAccess then
    Result := ActiveCursor.HasChildren

  else if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := ActiveCursor.HasChildren(PRecInfo(RecBuf)^.Bookmark);
end;

function TCustomEzDataset.Level: SmallInt;
var
  RecBuf: TRecBuf;

begin
  CheckActive;
  Result := -1;

  if DirectAccess then
  begin
    Result := ActiveCursor.Level;
    {
    KV: 29/8/2006
        Code dissabled,
        When in direct access mode, there is no way to determine
        if we are at the 'insert' node position. Therefore this code
        always fails.
    // When at insert node, then increase Level
    if (State = dsInsert) and
       (FInsertPosition = ipInsertAsChild) and
       (FInsertNode = ActiveCursor.CurrentNode)
    then
      inc(Result);
    }
  end
  else if GetActiveRecBuf(RecBuf) then
  begin
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := ActiveCursor.Level(PRecInfo(RecBuf)^.Bookmark)

    else if FInsertNode <> nil then
    begin
      Result := ActiveCursor.Level(FInsertNode);
      if FInsertPosition = ipInsertAsChild then
        inc(Result);

    end else
      Result := 0;
  end;
end;

procedure TCustomEzDataset.InternalInitFieldDefs;
var
  FieldDef: TFieldDef;

  procedure LoadFromFields(Fields: TFields; FieldDefs: TFieldDefs);
  var
    i: integer;
    F: TField;
  begin
    for I := 0 to Fields.Count - 1 do
    begin
      F := Fields[I];
      with F do
        if FieldDefs.IndexOf(FieldName) = -1 then
        begin
          FieldDef := FieldDefs.AddFieldDef;
          FieldDef.Name := FieldName;
          FieldDef.DataType := DataType;
          FieldDef.Size := Size;
          if Required then
            FieldDef.Attributes := [faRequired];
          if ReadOnly then
            FieldDef.Attributes := FieldDef.Attributes + [faReadonly];
          if (DataType = ftBCD) and (F is TBCDField) then
            FieldDef.Precision := TBCDField(F).Precision;
          if F is TObjectField then
            LoadFromFields(TObjectField(F).Fields, FieldDef.ChildDefs);
        end;
    end;
  end;

  procedure LoadFromFieldDefs(Source: TFieldDefs; FieldDefs: TFieldDefs);
  var
    i: integer;
  begin
    for i := 0 to Source.Count - 1 do
      if FieldDefs.IndexOf(Source[i].Name) = -1 then
      begin
        FieldDef := FieldDefs.AddFieldDef;
        FieldDef.Assign(Source[i]);
      end;
  end;

begin
  if not Assigned(MasterDataSet) then
    EzDatasetError(SNoDataSet, Self);
  if MasterDataSet = Self then EzDatasetError(SRecursiveDataset, Self);

  FieldDefs.Clear;

  // We only load fields from master dataset
  if MasterDataset.Fields.Count>0 then
    LoadFromFields(MasterDataset.Fields, FieldDefs) else
    begin
      MasterDataset.FieldDefs.Update;
      LoadFromFieldDefs(MasterDataset.FieldDefs, FieldDefs);
    end;

  // Make sure that persistent fields are all added to the fieldefs list
  if not DefaultFields then
    LoadFromFields(Fields, FieldDefs);
end;

function TCustomEzDataset.CreateBlobStream(Field: TField;
  Mode: TBlobStreamMode): TStream;
begin
  Result := TEzBlobStream.Create(Field as TBlobField, Mode);
end;

function TCustomEzDataset.InternalCompareNodes(N1, N2: TRecordNode): Integer;
const
  RetCodes: array[Boolean, Boolean] of ShortInt = ((0, 1),(-1, 2));

var
  Key1, Key2: Variant;
  SaveState: TDataSetState;

  // Returns true if valid data is returned.
  function GetIndexFieldValues(N: TRecordNode; var Ret: Variant): Boolean;
  begin
    if nfNew in N.Flags then
    begin
      with PRecInfo(TempBuffer)^ do
      begin
        Bookmark := N;
        BookmarkFlag := bfInserted;
        Level := N.Key.Level;
      end;
      Finalize(PVariantList(TempBuffer+SizeOf(TRecInfo))^, Fields.Count);

      // KV: 2 maart 2007
      // In case of a new node, use DefaultCursor to get data
      DefaultCursor.CurrentNode := DefaultCursor.FindNode(N.Key);

      { TODO :
Call to GetCalcFields was removed, however I have to test this
with a dataset having calculated fields! }
      // GetCalcFields(TempBuffer);
    end
    else
    begin
      if (esDefaultCursorLoading in FEzStates) or (ActiveCursorIndex=0) then
        DefaultCursor.CurrentNode := N else
        DefaultCursor.CurrentNode := DefaultCursor.FindNode(N.Key);

      InternalGetRecord(TempBuffer, gmCurrent, True);
    end;

    Ret := FieldValues[FIndexFields];
    Result := not (varType(Ret) in [varEmpty, varNull]);
  end;

var
  v1, v2, v3, v4: Variant;

begin
  SaveState := SetTempState(dsSort);
  try
    Result := RetCodes[GetIndexFieldValues(N1, Key1), GetIndexFieldValues(N2, Key2)];

    if VarIsArray(Key1) then
    begin
      v1 := Key1[0];
      v2 := Key1[1];
      GetIndexFieldValues(N1, Key1);
      v3 := Key2[0];
      v4 := Key2[1];
      GetIndexFieldValues(N2, Key2);

      if N1=nil then;
      if N2=nil then;
      if VarIsNull(v1) or VarIsNull(v2) or VarIsNull(v3) or VarIsNull(v4) then;
    end;

    if Result = 2 then
      Result := VarSafeCompare(Key1, Key2,
          // Do not use ActiveCursor because that would return DefaultCursor
          cpSortCaseSensitive in FCursors[FActiveCursorIndex].Options);
  finally
    RestoreState(SaveState);
  end;
end;

function TCustomEzDataset.CompareNodes(N1, N2: TRecordNode): Integer;
begin
  if Assigned(FOnCompareNodes) then
  begin
    Result := 0;
    FOnCompareNodes(Self, ActiveCursor, N1, N2, Result);
  end else
    Result := InternalCompareNodes(N1, N2);
end;

// This method creates the field objects by copying them
// from the wrapped dataset.
procedure TCustomEzDataset.InternalCreateFields;
var
  I: Integer;
begin
  for I := 0 to MasterDataSet.Fields.Count-1 do
    CopyField(Self.Owner, Self, MasterDataSet.Fields[I]);
end;

procedure TCustomEzDataset.InternalHandleException;
begin
  Application.HandleException(Self);
end;

procedure TCustomEzDataset.InternalGotoBookmark(Bookmark: {$IFDEF EZ_D2009}Pointer{$ELSE}TBookmark{$ENDIF});
begin
  ActiveCursor.Key := PNodeKey(Bookmark)^;
end;

procedure TCustomEzDataset.InternalSyncDataset(Syncwith: TRecordNode; SyncMasterLinks: Boolean);
var
  depth, lvl, i: integer;
  n: TRecordNode;

begin
  if SelfReferencing then
    depth := 0

  else if nfNew in Syncwith.Flags then
    depth := Syncwith.Key.Level
  else
    depth := ActiveCursor.Level(Syncwith);

  if (depth = 0) or not SyncMasterLinks then
    FEzDataLinks[depth].RelocateDataset(Syncwith)

  // We have to update cursor position for every dataset till current level
  else
    for lvl:=0 to depth do begin
      n := Syncwith;
      for i:=(depth-lvl) downto 1 do
        n := n.Parent;
      FEzDataLinks[lvl].RelocateDataset(n);
    end;
end;

procedure TCustomEzDataset.InternalSetToRecord(Buffer: TRecordBuffer);
begin
  if not DirectAccess and (PRecInfo(Buffer)^.BookmarkFlag in [bfCurrent, bfInserted]) then
    ActiveCursor.CurrentNode := PRecInfo(Buffer)^.Bookmark;
end;

procedure TCustomEzDataset.GetBookmarkData(Buffer: TRecBuf; Data: TBookmark);
begin
  Initialize(PNodeKey(Data)^);
  if DirectAccess then
    PNodeKey(Data)^ := ActiveCursor.CurrentNode.Key else
    PNodeKey(Data)^ := PRecInfo(Buffer).Bookmark.Key;
end;

function TCustomEzDataset.GetBookmarkFlag(Buffer: TRecBuf): TBookmarkFlag;
begin
  Result := PRecInfo(Buffer)^.BookmarkFlag;
end;

procedure TCustomEzDataset.SetActiveCursorIndex(Index: Integer);
begin
  if Index <> FActiveCursorIndex then
  try
    if not Active then
      FActiveCursorIndex := Index

    else
    try
      // Close active cursor
      if FActiveCursorIndex<>0 then
      begin
        FCursors[FActiveCursorIndex].Clear;
        FActiveCursorIndex := 0;
      end;

      // Open new cursor
      if Index <> 0 then
      begin
        if Index > FCursors.Count-1 then
          EzDatasetError(SInvalidCursorIndex, Self);

        with FCursors[Index] do
        begin
          FActiveCursorIndex := Index;
          LoadCursorData(FCursors[Index]);
        end;
      end;

    except
      FActiveCursorIndex := 0;
      raise;
    end;

  finally
    if State = dsBrowse then
    begin
      ActiveCursor.MoveFirst;
      Resync([]);
    end;
  end;
end;

procedure TCustomEzDataset.SetBookmarkFlag(Buffer: TRecBuf; Value: TBookmarkFlag);
begin
  PRecInfo(Buffer)^.BookmarkFlag := Value;
end;

procedure TCustomEzDataset.SetBookmarkData(Buffer: TRecBuf; Data: TBookmark);
begin
  if Assigned(Data) then
    PRecInfo(Buffer)^.Bookmark := ActiveCursor.FindNode(PNodeKey(Data)^) else
    PRecInfo(Buffer)^.BookmarkFlag := bfNA;
end;

procedure TCustomEzDataset.SetCursors(C: TEzDBCursors);
begin
  FCursors.Assign(C);
end;

function TCustomEzDataset.BookmarkValid(Bookmark: TBookmark): Boolean;
begin
  CheckCursor;
  if Assigned(Bookmark) then
    Result := ActiveCursor.Exists(PNodeKey(Bookmark)^) else
    Result := False;
end;

function TCustomEzDataset.CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer;
const
  RetCodes: array[Boolean, Boolean] of ShortInt = ((2, -1),(1, 0));

begin
  Result := RetCodes[Bookmark1 = nil, Bookmark2 = nil];
  if Result = 2 then
  try
    Result := CompareNodeKeys(PNodeKey(Bookmark1)^, PNodeKey(Bookmark2)^);
  except
    Result := 0;
  end;
end;

{ Record Functions }

function TCustomEzDataset.GetRecordSize: Word;
begin
  Result := FRecBufSize;
end;

function TCustomEzDataset.GetSelfReferencing: Boolean;
begin
  if FEzDataLinks.Count = 1 then
    Result := FEzDataLinks[0].ParentRefField <> ''
  else
    Result := False;
end;

function TCustomEzDataset.GetUseDefaultCursor: Boolean;
begin
  Result := FUseDefaultCursor;
end;

function TCustomEzDataset.GetFieldClass(FieldType: TFieldType): TFieldClass;
begin
  Result := inherited GetFieldClass(FieldType);
end;

function TCustomEzDataset.GetActiveDataSet: TDataset;
begin
  Result := ActiveDatalink.DetailDataset;
end;

function TCustomEzDataset.GetMasterDataSet: TDataset;
begin
  if FEzDataLinks.Count > 0 then
    Result := FEzDataLinks[0].DetailDataset else
    Result := nil;
end;

function TCustomEzDataset.GetMaxLevel: integer;
begin
  if SelfReferencing then
    Result := -1 else
    Result := FEzDataLinks.Count-1;
end;

function TCustomEzDataset.GetExpanded: Boolean;
var
  RecBuf: TRecBuf;
begin
  CheckActive;
  Result := false;

  if DirectAccess then
    Result := ActiveCursor.Expanded

  else if GetActiveRecBuf(RecBuf) then
    Result := nfExpanded in PRecInfo(RecBuf)^.Bookmark.Flags;
end;

function TCustomEzDataset.GetRecordNode: TRecordNode;
var
  RecBuf: TRecBuf;
begin
  CheckActive;
  Result := nil;

  if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := PRecInfo(RecBuf)^.Bookmark;
end;

function TCustomEzDataset.GetTopNode: TRecordNode;
var
  RecBuf: TRecBuf;
begin
  CheckActive;
  Result := nil;

  if not IsEmpty then
  begin
    RecBuf := Buffers[0];
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := PRecInfo(RecBuf)^.Bookmark;
  end;
end;

function TCustomEzDataset.GetTopRecNo: Integer;
var
  N: TRecordNode;

begin
  N := TopNode;
  if N=nil then
    Result := -1 else
    Result := ActiveCursor.NodeRecNo(N);
end;

function TCustomEzDataset.GetNodeKey: TNodeKey;
var
  RecBuf: TRecBuf;
begin
  CheckActive;
  Result.Level := -1;
  Result.Value := NULL;

  if DirectAccess then
    Result := ActiveCursor.Key

  else if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := PRecInfo(RecBuf)^.Bookmark.Key;
end;

function TCustomEzDataset.GetNodeType: TNodeType;
var
  RecBuf: TRecBuf;
begin
  CheckActive;
  Result := ntSingle;
  if DirectAccess then
    Result := ActiveCursor.NodeType

  else if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := ActiveCursor.NodeType(PRecInfo(RecBuf)^.Bookmark);
end;

function TCustomEzDataset.GetParent: TNodeKey;
var
  RecBuf: TRecBuf;

begin
  CheckActive;
  Result.Level := -1;
  Result.Value := NULL;

  if DirectAccess then
    Result := ActiveCursor.ParentKey

  else if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := ActiveCursor.GetParent(PRecInfo(RecBuf)^.Bookmark).Key;
end;

function TCustomEzDataset.AllocRecordBuffer: TRecordBuffer;
begin
  if not (csDestroying in ComponentState) then
  begin
    Result := AllocMem(FRecBufSize);
    Initialize(PVariantList(Result+SizeOf(TRecInfo))^, Fields.Count);
  end else
    Result := nil;
end;

function TCustomEzDataset.FixupChildRecords(Dataset: TDataset; Parent: Variant): Boolean;
var
  lvl: integer;
  Node: TRecordNode;
  nk: TNodeKey;

begin
  Result := False;
  if SelfReferencing then
    lvl := -1 else
    lvl := EzDataLinks.FindDatalink(Dataset).Index;

  nk.Level := lvl;
  nk.Value := Parent;

  Node := DefaultCursor.FindNode(nk);
  if Node = DefaultCursor.EndNode then EzDatasetError(SKeyNotFound, Self);

  if Assigned(Node.Child) then
  begin
    Node := Node.Child;
    with EzDataLinks[lvl+1 {will also work with self referencing!}] do
      while Assigned(Node) do
      begin
        InternalSyncDataset(Node, eoUseMDRelations in Options);

        if VarSafeCompare(DetailDataset[ParentRefField], Parent, True) <> 0 then
        begin
          Result := True;
          DetailDataset.Edit;
          DetailDataset[ParentRefField] := Parent;
          DetailDataset.Post;
        end;
        Node := Node.Next;
      end;
    end;
end;

procedure TCustomEzDataset.FixupGeneratedKey(Dataset: TDataset; GeneratedKey, NewKey: Variant);
var
  i, lvl: integer;
  pn: PNodeKey;
  nk: TNodeKey;
  Found: Boolean;

begin
  CheckActive;
  if FGeneratedKeys.Count = 0 then Exit;

  i:=0;
  pn := nil;
  if SelfReferencing then
    lvl := -1 else
    lvl := EzDataLinks.FindDatalink(Dataset).Index;

  Found := False;
  while not Found and (i<FGeneratedKeys.Count) do
  begin
    pn := FGeneratedKeys[i];
    if (pn^.Level = lvl) and (pn^.Value = GeneratedKey) then
      Found := true else
      inc(i);
  end;

  if Found then
  begin
    nk.Level := lvl;
    nk.Value := NewKey;
    ActiveCursor.UpdateKey(pn^, nk);
    if ActiveCursorIndex <> 0 then DefaultCursor.UpdateKey(pn^, nk);
    Finalize(pn^);
    Dispose(pn);
    FGeneratedKeys.Delete(i);
  end;
end;

procedure TCustomEzDataset.FreeBookmark(Bookmark: TBookmark);
begin
  Finalize(PNodeKey(Bookmark)^);
  inherited;
end;

procedure TCustomEzDataset.FreeRecordBuffer(var Buffer: TRecordBuffer);
begin
  if Buffer<>nil then
  begin
    Finalize(PVariantList(Buffer+SizeOf(TRecInfo))^, Fields.Count);
    FreeMem(Buffer);
  end;
end;

function TCustomEzDataset.GetActiveCursor: TEzDBCursor;
begin
  if (FActiveCursorIndex = 0) or (State = dsSort) or
     (State = dsEvaluateFormula) or (esCursorLoading in FEzStates) or
     UseDefaultCursor
  then
    Result := DefaultCursor
  else
    Result := FCursors[FActiveCursorIndex];
end;

function TCustomEzDataset.GetActiveDatalink: TEzDatalink;
var
  lvl: integer;
begin
  if IsEmpty then
    lvl := 0

  else if (State = dsInsert) then
  begin
    if SelfReferencing or (FInsertNode = nil) then
      lvl := 0 else
      begin
        lvl := ActiveCursor.Level(FInsertNode);
        if (FInsertPosition = ipInsertAsChild) and (lvl < MaxLevel) then
          Inc(lvl);
      end;
  end

  else if SelfReferencing then
    lvl := 0  else
    begin
      lvl := Level;
      if lvl = -1 then lvl := 0;
    end;

  Result  := FEzDataLinks[lvl];
end;

function TCustomEzDataset.CursorHasRecord: Boolean;
var
  RecBuf: TRecBuf;

begin
  CheckActive;
  Result := false;

  if DirectAccess then
    Result := ActiveCursor.CurrentNode.Flags * [nfAutoCreated, nfVirtual] = []

  else if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := PRecInfo(RecBuf)^.Bookmark.Flags * [nfAutoCreated, nfVirtual] = [];
end;

function TCustomEzDataset.GenerateKeyValue(Datalink: TEzDataLink; Level: Integer): Variant;
const
  IntegerTypes = [ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime, ftAutoInc, ftLargeint];

var
  MaxKey: Variant;
  Index: Integer;
  nk: TNodeKey;

begin
  Result := Null;

  if (Pos(';', Datalink.KeyField) = 0) and (Datalink.DetailDataset.FieldByName(Datalink.KeyField).DataType in IntegerTypes) then
  begin
    nk.Level := Level;
    nk.Value := MaxInt;

    if DefaultCursor.FSortedKeys.Count = 0 then
      Result := 1 else
      begin
        DefaultCursor.FSortedKeys.FindKey(Index, nk);
        dec(Index);
        MaxKey := DefaultCursor.FSortedKeys.Nodes[Index].Key.Value;
        try
          VarCast(Result, MaxKey, varInteger);
          Result := Result + 1;
        except
        end;
      end;
  end;

  if Assigned(FOnGenerateKey) then
    FOnGenerateKey(DataLink, Result);

  if VarSafeIsNull(Result) then
    EzDatasetError(SCannotGenerate, Self);
end;

function TCustomEzDataset.GetActiveRecBuf(var RecBuf: TRecBuf): Boolean;
begin
  case State of
    dsBlockRead,
    dsBrowse:
      if IsEmpty or ((BookmarkSize = 0) and ActiveCursor.EOF) then
        RecBuf := 0 else
        RecBuf := ActiveBuffer;
    dsEdit, dsInsert, dsNewValue, dsOldValue: RecBuf := ActiveBuffer;
    dsCalcFields,
    dsInternalCalc:
        RecBuf := CalcBuffer;
    dsFilter: RecBuf := FFilterBuffer;
  else
    if State=dsSort then {Avoids Case label outside range warning}
      RecBuf := TempBuffer else
      RecBuf := 0;
  end;
  Result := RecBuf <> 0;
end;

function TCustomEzDataset.GetDefaultCursor: TEzDBCursor;
begin
  Result := FCursors[0];
end;

function  TCustomEzDataset.GetDirectAccess: Boolean;
begin
  Result := FDirectAccessStack.Count > 0;
end;

function TCustomEzDataset.GetRecord(Buffer: TRecBuf; GetMode: TGetMode;
  DoCheck: Boolean): TGetResult;
var
  Accept: Boolean;
  SaveState: TDataSetState;
begin
  if Filtered and not (cpShowAll in ActiveCursor.Options) and Assigned(OnFilterRecord)
  then
  begin
    FFilterBuffer := Buffer;
    SaveState := SetTempState(dsFilter);
    try
      repeat
        Accept := True;
        Result := InternalGetRecord(Buffer, GetMode, DoCheck);
        if Result = grOK then
        begin
          OnFilterRecord(Self, Accept);
          if not Accept and (GetMode = gmCurrent) then
            Result := grError;
        end;
      until Accept or (Result <> grOK);
    except
      Application.HandleException(Self);
      Result := grError;
    end;
    RestoreState(SaveState);
  end else
    Result := InternalGetRecord(Buffer, GetMode, DoCheck)
end;

function TCustomEzDataset.InternalGetRecord(Buffer: TRecBuf; GetMode: TGetMode;
  DoCheck: Boolean): TGetResult;
var
  crs: TEzDbCursor;

begin
  try
    Result := grOK;
    crs := ActiveCursor;
    case GetMode of
      gmNext:
        begin
          if not crs.EOF then crs.MoveNext;
          if crs.EOF then Result := grEOF;
        end;
      gmPrior:
        begin
          if not crs.BOF then crs.MovePrevious;
          if crs.BOF then Result := grBOF;
        end;
      gmCurrent:
        begin
          if crs.BOF then Result := grBOF;
          if crs.EOF then Result := grEOF;
        end;
    end;

    // if position is tree is valid,
    // then position FDataSet on the same record and load its data.
    if Result = grOK then
    begin
      with PRecInfo(Buffer)^ do
      begin
        Bookmark := crs.CurrentNode;
        BookmarkFlag := bfCurrent;
        Level := crs.Level;
      end;
      Finalize(PVariantList(Buffer+SizeOf(TRecInfo))^, Fields.Count);
      GetCalcFields(Buffer);
    end;

  except
    if DoCheck then raise;
    Result := grError;
  end;
end;

procedure TCustomEzDataset.InternalInitRecord(Buffer: TRecordBuffer);
var
  I: Integer;
begin
  PRecInfo(Buffer)^.Level := -1;
  for I := 0 to Fields.Count - 1 do
    PVariantList(Buffer+SizeOf(TRecInfo))[I] := Null;
end;

procedure TCustomEzDataset.UpdateRecordSetPosition(Buffer: TRecBuf);
begin
  if (State <> dsCalcFields) and (ActiveCursor.CurrentNode <> PRecInfo(Buffer)^.Bookmark) then
  begin
    InternalSetToRecord(Buffer);
//    CursorPosChanged;
  end;
end;

function TCustomEzDataset.UpdateStatus: TUpdateStatus;
var
  RecBuf: TRecBuf;
  OK: Boolean;
begin
  OK := GetActiveRecBuf(RecBuf);
  if OK then
  begin
    UpdateRecordSetPosition(RecBuf);
    Result := ActiveDataset.UpdateStatus;
  end
  else
    Result := inherited UpdateStatus;
end;

{ Field Data }

function TCustomEzDataset.GetBlobFieldData(FieldNo: Integer;
  var Buffer: TBlobByteData): Integer;
begin
  Result := ActiveDataset.GetBlobFieldData(FieldNo, Buffer);
end;

{$IFDEF EZ_D5}
function TCustomEzDataset.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
begin
  Result := GetFieldData(Field, Buffer, True);
end;

function TCustomEzDataset.GetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean): Boolean;
{$ELSE}
function TCustomEzDataset.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
{$ENDIF}
var
  RecBuf: TRecBuf;
  Data: OleVariant;

  procedure CurrToBuffer(const C: Currency);
  begin
    if NativeFormat then
      DataConvert(Field, @C, Buffer, True) else
      Currency(Buffer^) := C;
  end;

  procedure VarToBuffer;
  begin
    with tagVariant(Data) do
      case Field.DataType of
        ftGuid, ftFixedChar, ftString:
          begin
            PAnsiChar(Buffer)[Field.Size] := #0;
            WideCharToMultiByte(0, 0, bStrVal, SysStringLen(bStrVal)+1,
              Buffer, Field.Size, nil, nil);
          end;
        ftFixedWideChar, ftWideString:
          WStrCopy(Buffer, bstrVal);
        ftSmallint:
          if vt = VT_UI1 then
            SmallInt(Buffer^) := Byte(cVal) else
            SmallInt(Buffer^) := iVal;
        ftWord:
          if vt = VT_UI1 then
            Word(Buffer^) := bVal else
            Word(Buffer^) := uiVal;
        ftAutoInc, ftInteger:
          Integer(Buffer^) := Data;
        ftFloat, ftCurrency:
          if vt = VT_R8 then
            Double(Buffer^) := dblVal else
            Double(Buffer^) := Data;
        ftFMTBCD:
          TBcd(Buffer^) := VarToBcd(Data);
        ftBCD:
          if vt = VT_CY then
            CurrToBuffer(cyVal) else
            CurrToBuffer(Data);
        ftBoolean:
          WordBool(Buffer^) := vbool;
        ftDate, ftTime, ftDateTime:
          if NativeFormat then
            DataConvert(Field, @date, Buffer, True) else
            TOleDate(Buffer^) := date;
        ftBytes, ftVarBytes:
          if NativeFormat then
            DataConvert(Field, @Data, Buffer, True) else
            OleVariant(Buffer^) := Data;
        ftInterface: IUnknown(Buffer^) := Data;
        ftIDispatch: IDispatch(Buffer^) := Data;
        ftLargeInt:
          if PDecimal(@Data)^.sign > 0 then
            LargeInt(Buffer^):=-1*PDecimal(@Data)^.Lo64
          else
            LargeInt(Buffer^):=PDecimal(@Data)^.Lo64;
        ftBlob..ftTypedBinary, ftVariant, ftWideMemo: OleVariant(Buffer^) := Data;
      else
        DatabaseErrorFmt(SUsupportedFieldType, [FieldTypeNames[Field.DataType],
          Field.DisplayName]);
      end;
  end;

  procedure RefreshBuffers;
  begin
    Reserved := Pointer(FieldListCheckSum(Self));
    UpdateCursorPos;
    Resync([]);
  end;

  function DataToInt64: Int64;
  begin
    if PDecimal(@Data)^.sign > 0 then
      Result := -1 * PDecimal(@Data)^.Lo64
    else
      Result := PDecimal(@Data)^.Lo64;
  end;

begin
  try
      // 14-12-2016 Code dissabled
      // Newer versions of Delphi hanlde 'old' and 'new' values in TField...

//    // KV: 19 june 2006
//    // (State<>dsSort) added to condition because sort value are read
//    // from TempBuffer and not directly from linked dataset
//    if DirectAccess and (State<>dsSort) and not (State in [dsOldValue, dsNewValue]) then
//      Data := InternalGetFieldData(ActiveCursor.CurrentNode, Field.FieldName)
//    else
//    begin
//      if (State = dsOldValue) and (FModifiedFields.IndexOf(Field) <> -1) then
//        //
//        // Requesting the old value of an modified field
//        //
//      begin
//        Result := True;
//        RecBuf := FOldValueBuffer;
//      end
//      else if (State=dsNewValue) and (FModifiedFields.IndexOf(Field) = -1) then
//        //
//        // New value requested, but no new value present
//        //
//      begin
//        Data := Null;
//        Result := False;
//        Exit;
//      end
//      else
//        Result := GetActiveRecBuf(RecBuf);

    if DirectAccess and (State<>dsSort) and not (State in [dsOldValue, dsNewValue]) then
      Data := InternalGetFieldData(ActiveCursor.CurrentNode, Field.FieldName)

    else
    begin
      Result := GetActiveRecBuf(RecBuf);
      if not Result then Exit;
      if not Assigned(Reserved) then RefreshBuffers;
      Data := PVariantList(RecBuf+SizeOf(TRecInfo))^[Field.Index];

      // if data hasn't been loaded yet, then get data from
      // dataset.
      if VarIsEmpty(Data) then
      begin
        Data := InternalGetFieldData(PRecInfo(RecBuf)^.Bookmark, Field.FieldName);
        if VarIsEmpty(Data) then Data := Null;
        PVariantList(RecBuf+SizeOf(TRecInfo))[Field.Index] := Data;
      end;
    end;
    Result := not VarIsNull(Data);
    if Result and (Buffer <> nil) then
      VarToBuffer;
  except
    on E: EFormulaError do
    begin
      Close;
      raise;
    end;
  end;
end;

function TCustomEzDataset.InternalGetFieldData(Node: TRecordNode; FieldName: string) : Variant;
var
  lvl: integer;
  Field: TField;
  FieldLinks: TEzFieldLinks;
  FieldLink: TEzFieldLink;

  procedure CheckRecursion(const Formula: string);
  var
   x: Integer;
   B2: TRecordNode;
  begin
    B2 := DefaultCursor.CurrentNode;
    for x:=0 to FCalculateList.Count-1 do
      with PCalculateField(FCalculateList[x])^ do
        if (CalcRecord = B2) and (Expression = Formula) then
          EzDatasetError(Format(SRecursiveFormula, [Formula]), Self);
  end;

  function Evaluate(const Formula: string): Variant;
  const
    NummericFieldTypes = [ftSmallint, ftAutoInc, ftInteger, ftWord, ftBoolean,
      ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime, {$IFDEF EZ_D6}ftTimeStamp,{$ENDIF} ftLargeInt];
    StringFieldTypes = [ftString, ftMemo, ftFmtMemo, ftFixedChar, {$IFDEF EZ_D5}ftVariant,{$ENDIF} ftWideString];

  var
    P: PCalculateField;
    SaveState: TDataSetState;
    SavedModified: Boolean;
    SavedOptions: TCursorOptions;

  begin
    // Because we are using the default cursor while evaluating a formula,
    // the DefaultCursor must be synchronized.
    if (FActiveCursorIndex <> 0) and (State <> dsEvaluateFormula) then
      DefaultCursor.Key := Node.Key else
      DefaultCursor.CurrentNode := Node;

    // A call to CursorPosChanged is required here!
    CursorPosChanged;

    CheckRecursion(Formula);

    // Prepare dataset
    SavedModified := Modified;
    SaveState := SetTempState(dsEvaluateFormula);
    SavedOptions := DefaultCursor.FOptions;
    include(DefaultCursor.FOptions, cpShowAll);

    New(P);
    P^.CalcRecord := Node;
    P^.Expression := Formula;
    FCalculateList.Add(P);

    FDirectAccessStack.Add(nil); // fake direct db access

    try
      FParser.ExpressionKey := NodeKeyToStr(Node.Key);
      FParser.Expression := Formula;

      try
        if Field.DataType in NummericFieldTypes then
          Result := FParser.Result
        else if Field.DataType in StringFieldTypes then
          Result := FParser.StrResult
        else
          DatabaseErrorFmt(SCantUseFormulaWithFieldType, [FieldTypeNames[Field.DataType]], Self);

        if FParser.ResultWasNA then
          Result := Null;
      except
        on E:Exception do
          raise EFormulaError.Create(Format(SInvalidFormula, [Formula, E.Message]));
      end;
    finally
      RestoreState(SaveState);
      SetModified(SavedModified);
      DefaultCursor.FOptions := SavedOptions;
      FDirectAccessStack.Delete(FDirectAccessStack.Count-1);
      FCalculateList.Delete(FCalculateList.IndexOf(P));
      Dispose(P);
    end;
  end;

  function SyncParentDataset(L: integer): Boolean;
  var
    n: TRecordNode;
    i: integer;

  begin
    Result := true;
    n := Node;
    for i:=lvl-1 downto L do
      n := n.Parent;

    if n.Flags * [nfAutoCreated, nfVirtual] = [] then
      InternalSyncDataset(n, eoUseMDRelations in Options) else
      Result := false;
  end;

  function Convert(const Value: Variant) : Variant;
  begin
    if VarIsNull(Value) then
      Result := Value
    else
      case Field.DataType of
        ftGuid, ftFixedChar, ftString:
          Result := VarToStr(Value);
      else
        Result := Value;
      end;
//        begin
//          PAnsiChar(Buffer)[Field.Size] := #0;
//          WideCharToMultiByte(0, 0, bStrVal, SysStringLen(bStrVal)+1,
//            Buffer, Field.Size, nil, nil);
//        end;
//      ftFixedWideChar, ftWideString:
//        WStrCopy(Buffer, bstrVal);
//      ftSmallint:
//        if vt = VT_UI1 then
//          SmallInt(Buffer^) := Byte(cVal) else
//          SmallInt(Buffer^) := iVal;
//      ftWord:
//        if vt = VT_UI1 then
//          Word(Buffer^) := bVal else
//          Word(Buffer^) := uiVal;
//      ftAutoInc, ftInteger:
//        Integer(Buffer^) := Data;
//      ftFloat, ftCurrency:
//        if vt = VT_R8 then
//          Double(Buffer^) := dblVal else
//          Double(Buffer^) := Data;
//      ftFMTBCD:
//        TBcd(Buffer^) := VarToBcd(Data);
//      ftBCD:
//        if vt = VT_CY then
//          CurrToBuffer(cyVal) else
//          CurrToBuffer(Data);
//      ftBoolean:
//        WordBool(Buffer^) := vbool;
//      ftDate, ftTime, ftDateTime:
//        if NativeFormat then
//          DataConvert(Field, @date, Buffer, True) else
//          TOleDate(Buffer^) := date;
//      ftBytes, ftVarBytes:
//        if NativeFormat then
//          DataConvert(Field, @Data, Buffer, True) else
//          OleVariant(Buffer^) := Data;
//      ftInterface: IUnknown(Buffer^) := Data;
//      ftIDispatch: IDispatch(Buffer^) := Data;
//      ftLargeInt:
//        if PDecimal(@Data)^.sign > 0 then
//          LargeInt(Buffer^):=-1*PDecimal(@Data)^.Lo64
//        else
//          LargeInt(Buffer^):=PDecimal(@Data)^.Lo64;
//      ftBlob..ftTypedBinary, ftVariant, ftWideMemo: OleVariant(Buffer^) := Data;
//    else
//      DatabaseErrorFmt(SUsupportedFieldType, [FieldTypeNames[Field.DataType],
//        Field.DisplayName]);
//    end;
  end;

var
  NodeType: TNodeType;

begin
  Result := Null;
  Field := FieldByName(FieldName);

  if SelfReferencing then
  begin
    lvl := 0;
    // KV: 2 maart 2007
    // Next two lines added:
    // when sorting DefaultCursor.CurrentNode holds record to read from
    if (State = dsSort) and (nfNew in Node.Flags) then
      NodeType := DefaultCursor.NodeType else
      NodeType := ActiveCursor.NodeType(Node);

    FieldLinks := EzDataLinks[0].FieldLinks[NodeType];
  end
  else
  begin
    // KV: 19 june 2006
    // Next two lines added:
    // when a node is new, level cannot be retrieved
    // using ActiveCursor.Level(Node), use Node.Key.Level instead.
    if nfNew in Node.Flags then
      lvl := Node.Key.Level else
      lvl := ActiveCursor.Level(Node);

    FieldLinks := EzDataLinks[lvl].MDFields;
  end;

  FieldLink := FieldLinks[FieldName];

  if Assigned(FieldLink.Field) then
    //
    // Get value from field object
    //
  begin
    // Check if a value is requested from a higher level node.
    case FieldLink.FParentLevel of
      NO_REF:
        if Node.Flags * [nfAutoCreated, nfVirtual] = [] then
        begin
          InternalSyncDataset(Node, eoUseMDRelations in FOptions);
          Result := Convert(FieldLink.Field.Value);
        end;
      ROOT_REF:
        try
          FDirectAccessStack.Add(nil); // fake direct db access
          Node := ActiveCursor.AbsParent(Node);
          Result := InternalGetFieldData(Node, FieldName);
        finally
          FDirectAccessStack.Delete(FDirectAccessStack.Count-1);
        end;
      PARENT_REF:
        try
          FDirectAccessStack.Add(nil); // fake direct db access
          Node := ActiveCursor.GetParent(Node);
          Result := InternalGetFieldData(Node, FieldName);
        finally
          FDirectAccessStack.Delete(FDirectAccessStack.Count-1);
        end;
      else

        //
        // gets field data for absolute ParentLevel values (like when ParentLevel=1)
        //
        if (FieldLink.FParentLevel <> lvl) then
        begin
          if SyncParentDataset(FieldLink.FParentLevel) then
            Result := FieldLink.Field.Value;
        end
        else if Node.Flags * [nfAutoCreated, nfVirtual] = [] then
        begin
          InternalSyncDataset(Node, eoUseMDRelations in FOptions);
          Result := FieldLink.Field.Value;
        end;
    end;
  end
  else if FieldLink.Link <> '' then
    //
    // Need to evaluate a formula
    //
  begin
    Result := Evaluate(FieldLink.Link);
  end;
end;

{$IFDEF XE3}
function TCustomEzDataset.GetFieldData(Field: TField; {$IFDEF XE10_UP}var{$ENDIF} Buffer: TValueBuffer): Boolean;
begin
  Result := GetFieldData(Field, Buffer, True);
end;

function TCustomEzDataset.GetFieldData(FieldNo: Integer; {$IFDEF XE10_UP}var{$ENDIF} Buffer: TValueBuffer): Boolean;
begin
  Result := GetFieldData(FieldByNumber(FieldNo), Buffer);
end;

function TCustomEzDataset.GetFieldData(Field: TField; {$IFDEF XE10_UP}var{$ENDIF} Buffer: TValueBuffer; NativeFormat: Boolean): Boolean;
var
  RecBuf: TRecBuf;
  Data: OleVariant;

  procedure CurrToBuffer(const C: Currency);
  var
    LBuff: TValueBuffer;
  begin
    if NativeFormat then
    begin
      SetLength(LBuff, SizeOf(Currency));
      TDBBitConverter.UnsafeFrom<Currency>(C, LBuff);
      DataConvert(Field, LBuff, Buffer, True)
    end
    else
      TDBBitConverter.UnsafeFrom<Currency>(C, Buffer);
  end;

  procedure VarToBuffer;
  var
    dt: TDateTime;
    TempBuff: TValueBuffer;
    PData: Pointer;
    sqlt: TSQLTimeStamp;

  begin
    case Field.DataType of
      ftGuid, ftFixedChar, ftString:
        begin
          PAnsiChar(Buffer)[Field.Size] := #0;
          WideCharToMultiByte(0, 0, tagVariant(Data).bStrVal, SysStringLen(tagVariant(Data).bStrVal)+1,
            @Buffer[0], Field.Size, nil, nil);
        end;
      ftFixedWideChar, ftWideString:
        begin
          TempBuff := TEncoding.Unicode.GetBytes(tagVariant(Data).bstrVal);
          SetLength(TempBuff, Length(TempBuff) + SizeOf(Char));
          TempBuff[Length(TempBuff) - 2] := 0;
          TempBuff[Length(TempBuff) - 1] := 0;
          Move(TempBuff[0], Buffer[0], Length(TempBuff));
        end;
      ftSmallint:
        if tagVariant(Data).vt = VT_UI1 then
          TDBBitConverter.UnsafeFrom<SmallInt>(Byte(tagVariant(Data).cVal), Buffer)
        else
          TDBBitConverter.UnsafeFrom<SmallInt>(tagVariant(Data).iVal, Buffer);
      ftWord:
        if tagVariant(Data).vt = VT_UI1 then
          TDBBitConverter.UnsafeFrom<Word>(tagVariant(Data).bVal, Buffer)
        else
          TDBBitConverter.UnsafeFrom<Word>(tagVariant(Data).uiVal, Buffer);
      ftAutoInc, ftInteger:
        TDBBitConverter.UnsafeFrom<Integer>(Data, Buffer);
      ftFloat, ftCurrency:
        if tagVariant(Data).vt = VT_R8 then
          TDBBitConverter.UnsafeFrom<Double>(tagVariant(Data).dblVal, Buffer)
        else
          TDBBitConverter.UnsafeFrom<Double>(Data, Buffer);
      ftFMTBCD:
        TDBBitConverter.UnsafeFrom<TBcd>(VarToBcd(Data), Buffer);
      ftBCD:
        if tagVariant(Data).vt = VT_CY then
          CurrToBuffer(tagVariant(Data).cyVal)
        else
          CurrToBuffer(Data);
      ftBoolean:
        TDBBitConverter.UnsafeFrom<WordBool>(tagVariant(Data).vbool, Buffer);
      ftDate, ftTime, ftDateTime:
        if NativeFormat then
        begin
          SetLength(TempBuff, SizeOf(Double));
          TDBBitConverter.UnsafeFrom<Double>(data, TempBuff);
          DataConvert(Field, TempBuff, Buffer, True);
        end else
          TDBBitConverter.UnsafeFrom<Double>(tagVariant(Data).date, Buffer);
      ftTimeStamp:
      begin
        sqlt := VarToSQLTimeStamp(Data);
        TDBBitConverter.UnsafeFrom<TSqlTimeStamp>(sqlt, Buffer);

//        dt := SQLTimeStampToDateTime(sqlt);
//        SetLength(TempBuff, SizeOf(Double));
//        TDBBitConverter.UnsafeFrom<Double>(dt, TempBuff);
//        DataConvert(Field, TempBuff, Buffer, True);
        // TDBBitConverter.UnsafeFrom<Double>(dt, Buffer);
      end;

      ftBytes, ftVarBytes:
        if NativeFormat then
        begin
          PData := VarArrayLock(Data);
          try
            DataConvert(Field, BytesOf(PData, VarArrayHighBound(Data, 1) - VarArrayLowBound(Data, 1) + 1), Buffer, True);
          finally
            VarArrayUnlock(Data);
          end;
        end
        else
        begin
          if VarIsArray(Data) then
            SetLength(Buffer, VarArrayHighBound(Data, 1) + 1);
          TDBBitConverter.UnsafeFromVariant(Data, Buffer);
        end;
      ftInterface:
        begin
          TempBuff := BytesOf(@Data, SizeOf(IUnknown));
          Move(TempBuff[0], Buffer[0], SizeOf(IUnknown));
        end;
      ftIDispatch:
        begin
          TempBuff := BytesOf(@Data, SizeOf(IDispatch));
          Move(TempBuff[0], Buffer[0], SizeOf(IDispatch));
        end;
      ftLargeInt:
      begin
        if PDecimal(@Data)^.sign > 0 then
          TDBBitConverter.UnsafeFrom<Int64>(-1*PDecimal(@Data)^.Lo64, Buffer)
        else
          TDBBitConverter.UnsafeFrom<Int64>(PDecimal(@Data)^.Lo64, Buffer);
      end;
      ftBlob..ftTypedBinary, ftVariant, ftWideMemo:
      begin
        SetLength(TempBuff, SizeOf(Variant));
        PVariant(TempBuff)^ := Data;
        Move(TempBuff[0], Buffer[0], SizeOf(Variant));
      end;
        // TDBBitConverter.UnsafeFromVariant(Data, Buffer);
    else
      DatabaseErrorFmt(SUsupportedFieldType, [FieldTypeNames[Field.DataType],
        Field.DisplayName]);
    end;
  end;

  procedure RefreshBuffers;
  begin
    Reserved := Pointer(FieldListCheckSum(Self));
    UpdateCursorPos;
    Resync([]);
  end;

  function DataToInt64: Int64;
  begin
    if PDecimal(@Data)^.sign > 0 then
      Result := -1 * PDecimal(@Data)^.Lo64
    else
      Result := PDecimal(@Data)^.Lo64;
  end;

begin
  try
      // 14-12-2016 Code dissabled
      // Newer versions of Delphi hanlde 'old' and 'new' values in TField...

//    // KV: 19 june 2006
//    // (State<>dsSort) added to condition because sort value are read
//    // from TempBuffer and not directly from linked dataset
//    if DirectAccess and (State<>dsSort) and not (State in [dsOldValue, dsNewValue]) then
//      Data := InternalGetFieldData(ActiveCursor.CurrentNode, Field.FieldName)
//    else
//    begin
//      if (State = dsOldValue) and (FModifiedFields.IndexOf(Field) <> -1) then
//        //
//        // Requesting the old value of an modified field
//        //
//      begin
//        Result := True;
//        RecBuf := FOldValueBuffer;
//      end
//      else if (State=dsNewValue) and (FModifiedFields.IndexOf(Field) = -1) then
//        //
//        // New value requested, but no new value present
//        //
//      begin
//        Data := Null;
//        Result := False;
//        Exit;
//      end
//      else
//        Result := GetActiveRecBuf(RecBuf);

    if DirectAccess and (State<>dsSort) and not (State in [dsOldValue, dsNewValue]) then
      Data := InternalGetFieldData(ActiveCursor.CurrentNode, Field.FieldName)
    else
    begin
      Result := GetActiveRecBuf(RecBuf);
      if not Result then Exit;
      if not Assigned(Reserved) then RefreshBuffers;
      Data := PVariantList(RecBuf+SizeOf(TRecInfo))^[Field.Index];

      // if data hasn't been loaded yet, then get data from
      // dataset.
      if VarIsEmpty(Data) then
      begin
        Data := InternalGetFieldData(PRecInfo(RecBuf)^.Bookmark, Field.FieldName);
        if VarIsEmpty(Data) then Data := Null;
        PVariantList(RecBuf+SizeOf(TRecInfo))[Field.Index] := Data;
      end;
    end;
    Result := not VarIsNull(Data);
    if Result and (Buffer <> nil) then
      VarToBuffer;
  except
    on E: EFormulaError do
    begin
      Close;
      raise;
    end;
  end;
end;

procedure TCustomEzDataset.SetFieldData(Field: TField; Buffer: TValueBuffer);
begin
  InternalSetFieldData(Field, Buffer, True);
end;

procedure TCustomEzDataset.SetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean);
begin
  InternalSetFieldData(Field, Buffer, nativeFormat);
end;

{$ENDIF}

procedure TCustomEzDataset.SetEzDataLinks(Value: TEzDataLinks);
begin
  FEzDataLinks.Assign(Value);
end;

procedure TCustomEzDataset.SetExpanded(Value: Boolean);
var
  lvl: Integer;

begin
  if Assigned(FBeforeExpandRecord) then
    FBeforeExpandRecord(Self, ActiveCursor.CurrentNode);

  if DirectAccess then
    ActiveCursor.Expanded := Value
  else
  begin
    CheckActive;
    UpdateRecordSetPosition(ActiveBuffer);
    if ActiveCursor.Expanded <> Value then
    begin
      if not SelfReferencing then
      begin
        lvl := Level;
        if not FEzDatalinks[lvl].CanExpand then
          Value := False;
        if ActiveCursor.Expanded = Value then Exit;
      end;
      ActiveCursor.Expanded := Value;
      Refresh;
    end;
  end;
  if Assigned(FAfterExpandRecord) then
    FAfterExpandRecord(Self, ActiveCursor.CurrentNode);
end;

procedure TCustomEzDataset.SetRecordNode(Node: TRecordNode);
begin
  if not LocateNode(Node) then
    EzDatasetError(SNodeNotFound, Self);
end;

procedure TCustomEzDataset.SetNodeKey(Value: TNodeKey);
begin
  if not LocateNode(Value) then
    EzDatasetError(SKeyNotFound, Self);
end;

procedure TCustomEzDataset.SetParent(Key: TNodeKey);
var
  i: integer;
  AField: TField;
  LinkField: TEzFieldLink;
  Links: TEzFieldLinks;

begin
  CheckActive;
  if not SelfReferencing and ((Level <> Key.Level+1) or ((Level>0) and VarIsNull(Key.Value))) then
    EzDatasetError(SConnotMove, Self);

  with ActiveDatalink do
  begin
    if SelfReferencing then
      Links := FieldLinks[ActiveCursor.NodeType] else
      Links := MDFields;

    i:=0;
    while (i < Fields.Count) do
    begin
      AField := TField(Fields[i]);
      LinkField := Links[AField.FieldName];
      if LowerCase(LinkField.Link)=LowerCase(ParentRefField) then
      begin
        Edit;
        AField.Value := Key.Value;
        Post;
        Exit; // Break away
      end;
      inc(i);
    end;

    EzDatasetError(Format(SConnotMoveNoLink, [ParentRefField]), Self);

  end;
end;

procedure TCustomEzDataset.SetFieldData(Field: TField; Buffer: Pointer);
begin
  InternalSetFieldData(Field, Buffer, True);
end;

{$IFDEF EZ_D5}
procedure TCustomEzDataset.SetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean);
begin
  InternalSetFieldData(Field, Buffer, NativeFormat);
end;
{$ENDIF}

procedure TCustomEzDataset.InternalSetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean);

{$IFDEF EZ_D2009}
  procedure BufferToVar(var Data: OleVariant);
  begin
    case Field.DataType of
      ftString, ftFixedChar, ftGuid:
        Data := AnsiString(PAnsiChar(Buffer));
      ftWideString, ftFixedWideChar:
        Data := WideString(PWideChar(Buffer));
      ftAutoInc, ftInteger:
        Data := LongInt(Buffer^);
      ftSmallInt:
        Data := SmallInt(Buffer^);
      ftWord:
        Data := Word(Buffer^);
      ftBoolean:
        Data := WordBool(Buffer^);
      ftFloat, ftCurrency:
        Data := Double(Buffer^);
      ftBlob, ftMemo, ftGraphic, ftVariant, ftWideMemo:
        Data := Variant(Buffer^);
      ftInterface:
        Data := IUnknown(Buffer^);
      ftIDispatch:
        Data := IDispatch(Buffer^);
      ftDate, ftTime, ftDateTime:
        if NativeFormat then
          DataConvert(Field, Buffer, @TVarData(Data).VDate, False) else
          Data := TDateTime(Buffer^);
      ftTimeStamp:
        Data := SQLTimeStampToDateTime(TSQLTimeStamp(Buffer^));
{$IFDEF EZ_D2010}
      ftTimeStampOffset:
        Data := SQLTimeStampOffsetToDateTime(TSQLTimeStampOffset(Buffer^));
{$ENDIF}
      ftFMTBCD:
        Data := VarFMTBcdCreate(TBcd(Buffer^));
      ftBCD:
        if NativeFormat then
          DataConvert(Field, Buffer, @TVarData(Data).VCurrency, False) else
          Data := Currency(Buffer^);
      ftBytes, ftVarBytes:
        if NativeFormat then
          DataConvert(Field, Buffer, @Data, False) else
          Data := OleVariant(Buffer^);
      ftLargeInt:
        Data := LargeInt(Buffer^);
      else
        DatabaseErrorFmt(SUsupportedFieldType, [FieldTypeNames[Field.DataType],
          Field.DisplayName]);
    end;
  end;
{$ELSE}
  procedure BufferToVar(var Data: Variant);
{$IFNDEF EZ_D5}
  var
    TimeStamp: TTimeStamp;
{$ENDIF}

  begin
    case Field.DataType of
{$IFDEF EZ_D5}
      ftGuid:
        Data := WideString(PChar(Buffer));
      ftInterface:
        Data := IUnknown(Buffer^);
      ftIDispatch:
        Data := IDispatch(Buffer^);
      ftVariant:
        Data := Variant(Buffer^);
{$ENDIF}

      ftString, ftFixedChar:
        Data := WideString(PChar(Buffer));

{$IFDEF EZ_D2006}
      ftWideString, ftFixedWideChar:
        Data := WideString(PWideChar(Buffer));
{$ELSE}
      ftWideString:
        Data := WideString(Buffer^);
{$ENDIF}

      ftAutoInc, ftInteger:
        Data := LongInt(Buffer^);
      ftSmallInt:
        Data := SmallInt(Buffer^);
      ftWord:
        Data := Word(Buffer^);
      ftBoolean:
        Data := WordBool(Buffer^);
      ftFloat, ftCurrency:
        Data := Double(Buffer^);
      ftBlob, ftMemo, ftGraphic:
        Data := Variant(Buffer^);
{$IFDEF EZ_D5}
      ftDate, ftTime, ftDateTime:
        if NativeFormat then
          DataConvert(Field, Buffer, @TVarData(Data).VDate, False) else
          Data := TDateTime(Buffer^);
{$ELSE}
      ftDate:
      begin
        TimeStamp.Time := 0;
        TimeStamp.Date := TDateTimeRec(Buffer^).Date;
        Data := TimeStampToDateTime(TimeStamp);
      end;
      ftTime:
      begin
        TimeStamp.Time := TDateTimeRec(Buffer^).Time;
        TimeStamp.Date := DateDelta;
        Data := TimeStampToDateTime(TimeStamp);
      end;
      ftDateTime:
        Data := TimeStampToDateTime(MSecsToTimeStamp(TDateTimeRec(Buffer^).DateTime));
{$ENDIF}

{$IFDEF EZ_D6}
      ftTimeStamp:
        Data := SQLTimeStampToDateTime(TSQLTimeStamp(Buffer^));
{$ENDIF}
{$IFDEF EZ_D2010}
      ftTimeStampOffset:
        Data := SQLTimeStampOffsetToDateTime(TSQLTimeStampOffset(Buffer^));
{$ENDIF}

      ftBCD:
{$IFDEF EZ_D5}
        if NativeFormat then
          DataConvert(Field, Buffer, @TVarData(Data).VCurrency, False) else
          Data := Currency(Buffer^);
{$ELSE}
        Data := Currency(Buffer^);
{$ENDIF}
      ftBytes, ftVarBytes:
{$IFDEF EZ_D5}
        if NativeFormat then
          DataConvert(Field, Buffer, @Data, False) else
          Data := Variant(Buffer^);
{$ELSE}
        Data := Variant(Buffer^);
{$ENDIF}

      ftLargeInt:
{$IFDEF EZ_D5}
        begin
          TVarData(Data).VType := VT_DECIMAL;
          Decimal(Data).Lo64 := Int64(Buffer^);
        end;
{$ELSE}
          Data := Integer(Buffer^);
{$ENDIF}
      else
        DatabaseErrorFmt(SUnsupportedFieldType, [FieldTypeNames[Field.DataType],
          Field.DisplayName], Self);
    end;
  end;
{$ENDIF}

var
  Data: {$IFDEF EZ_D2009}OleVariant{$ELSE}Variant{$ENDIF};
  RecBuf: TRecBuf;

begin
  with Field do
  begin
    if not (State in dsWriteModes) then DatabaseError(SNotEditing, Self);
    GetActiveRecBuf(RecBuf);

    if FieldNo > 0 then
    begin
      if not (State in [dsSetKey, dsFilter]) and not FieldCanModify(Field) then
        DatabaseErrorFmt(SFieldReadOnly, [DisplayName], Self);

      Validate(Buffer);

      if FModifiedFields.IndexOf(Field) = -1 then
      begin
          // Newer versions of Delphi handle old values in a different way
          // calling Field.Value will actually reset the new field value back to the old value!!
//        PVariantList(FOldValueBuffer+SizeOf(TRecInfo))[Field.Index] := Field.Value;
        FModifiedFields.Add(Field);
      end;
    end;

    if Buffer = nil then
      Data := Null else
      BufferToVar(Data);

    PVariantList(RecBuf+SizeOf(TRecInfo))[Field.Index] := Data;

    if not (State in [dsCalcFields, dsInternalCalc, dsFilter, dsNewValue]) then
    begin
      DataEvent(deFieldChange, Longint(Field));

      // Special behaviour. If a calculated field is edited then we set
      // Modified flag here.
      if not Modified then
        SetModified(True);
    end;
  end;
end;


{ Record Navigation / Editing }

procedure TCustomEzDataset.InternalFirst;
begin
  ActiveCursor.FCurrentNode := ActiveCursor.FTopNode;
end;

procedure TCustomEzDataset.InternalLast;
begin
  ActiveCursor.FCurrentNode := ActiveCursor.FEndNode;
end;

function TCustomEzDataset.GetCanModify: Boolean;
begin
  if (InsertPosition = ipInsertAsChild) and not SelfReferencing then
    Result := EzDatalinks[min(ActiveCursor.Level+1, MaxLevel)].DataSource.Dataset.Canmodify else
    Result := ActiveDataset.CanModify;
end;

function TCustomEzDataset.FieldCanModify(Field: TField) : Boolean;
var
  lvl: integer;
  Links: TEzFieldLinks;

begin
  if (Field.FieldKind = fkLookup) then
  begin
    Result := true;
    Exit;
  end;

  Result := (IsEmpty or (State = dsInsert) or ((State <> dsInsert) and CursorHasRecord)) and
            Field.CanModify;

  if not Result then Exit;

  //
  // Locate TEzFieldLinks associated with current record
  //
  if (State = dsInsert) then
  begin
    if SelfReferencing then
    begin
      if (FInsertNode = nil) or
        ((FInsertPosition = ipInsertBefore) and (FInsertNode.Parent = nil))
      then
        Links := EzDatalinks[0].SingleNodeFields else
        Links := EzDatalinks[0].ChildNodeFields;
    end
    else
    begin
      if (FInsertNode = nil) then
        lvl := 0
      else
      begin
        lvl := ActiveCursor.Level(FInsertNode);
        if (FInsertPosition = ipInsertAsChild) and (lvl < MaxLevel) then
          inc(lvl);
      end;
      Links := EzDatalinks[lvl].MDFields;
    end;
  end
  // Dataset in edit mode !
  else if SelfReferencing then
  begin
    UpdateCursorPos;
    Links := EzDatalinks[0].FieldLinks[ActiveCursor.NodeType]
  end
  else
  begin
    lvl := Level;
    if lvl = -1 then lvl := 0;
    Links := EzDatalinks[lvl].MDFields;
  end;

  with Links[Field.FieldName] do
    if (Field = nil) {or (FParentLevel <> NO_REF)} then
      Result := false else // Calculated field
      Result := not Field.ReadOnly;
end;

function TCustomEzDataset.GetFields : TFields;
begin
  Result := inherited Fields;
end;

procedure TCustomEzDataset.InternalAddRecord(Buffer: TRecBuf; Append: Boolean);
begin
  if Append then SetBookmarkFlag(Buffer, bfEOF);
  InternalPost;
end;

procedure TCustomEzDataset.InternalEdit;
var
  RecBuf: TRecordBuffer;

begin
  FModifiedFields.Clear;
//
// if FOldValueBuffer = nil then
//    FOldValueBuffer := AllocRecordBuffer else
//    Finalize(PVariantList(FOldValueBuffer+SizeOf(TRecInfo))^, Fields.Count);
//
//  if GetActiveRecBuf(RecBuf) then
//    PRecInfo(FOldValueBuffer)^ := PRecInfo(RecBuf)^;
end;

procedure TCustomEzDataset.InternalPost;
var
  Node: TRecordNode;
  nk: TNodeKey;
  ReIndex, ParentUpdated: Boolean;
  lvl: integer;
  FieldData: PVariantList;
  InsertDS: TDataset;
  ParentValue: Variant;
  FLinks: TEzFieldLinks;
  ReIndexNodes: TList;

  procedure UpdateData;
  var
    I, l: Integer;
    ModField: TField;
    LinkField: TEzFieldLink;
    IndexNode: TRecordNode;

  begin
    with FEzDataLinks[lvl] do
      for I:=0 to FModifiedFields.Count - 1 do
      begin
        ModField := TField(FModifiedFields[I]);
        LinkField := FLinks[ModField.FieldName];

        if LinkField.Field = nil then
          EzDatasetError(Format(SCalcfieldUpdated, [LinkField.FFieldName]), Self);

        if not (LinkField.Field.Dataset.State in [dsEdit, dsInsert]) then
          LinkField.Field.Dataset.Edit;

        LinkField.Field.Value := FieldData[ModField.Index];

        if (LinkField.Field.Dataset.State=dsEdit) and (Pos(lowercase(LinkField.FLink), lowercase(KeyField)) <> 0) then
          EzDatasetError(SKeyCannotChange, Self);

        // if we have a sorted cursor, and modified field is part of the
        // index, then we need to reindex this node later on.
        if Assigned(ReIndexNodes) and
           (cpSorted in ActiveCursor.Options) and
           (Pos(lowercase(ModField.FieldName), lowercase(ActiveCursor.IndexFieldNames)) <> 0)
        then
        begin
          case LinkField.ParentLevel of
            NO_REF:   IndexNode := ActiveCursor.CurrentNode;
            ROOT_REF: IndexNode := ActiveCursor.AbsParent;
            PARENT_REF: IndexNode := ActiveCursor.GetParent(ActiveCursor.CurrentNode);
            else
            begin
              IndexNode := ActiveCursor.CurrentNode;
              if (LinkField.FParentLevel <> lvl) then
                for l:=ActiveCursor.Level-1 downto LinkField.FParentLevel do
                  IndexNode := IndexNode.Parent;
            end;
          end;

          if ReIndexNodes.IndexOf(IndexNode) = -1 then
            ReIndexNodes.Add(IndexNode);
        end;
      end;
  end;

  //
  // Check if parent value has changed and if the new parent has the
  // same level as the current parent. We currently do no support
  // moving records from one table into another.
  function ParentUpdateCheck: Boolean;
  var
    i: integer;
    modField: TField;
    LinkField: TEzFieldLink;
    NodeCheck: TRecordNode;

  begin
    Result := false;
    with FEzDataLinks[lvl] do
    begin
      i:=0;
      while not Result and (i < FModifiedFields.Count) do
      begin
        ModField := TField(FModifiedFields[i]);
        LinkField := FLinks[ModField.FieldName];
        with LinkField do
        begin
          if (Link = ParentRefField) and
             (Field.Value <> FieldData[ModField.Index])
          then
          begin
            Result := true;
            Node := nil;
            nk.Value := FieldData[ModField.Index];

            if not VarIsNull(nk.Value) then
            begin
              if SelfReferencing then
                nk.Level := -1
              else if lvl > 0 then
                nk.Level := lvl-1
              else
                EzDatasetError(SParentCantChange, Self);

              Node := ActiveCursor.FindNode(nk);
              if Node = ActiveCursor.EndNode then
                EzDatasetError(SParentNotFound, Self)

              else
              begin
                NodeCheck := Node;
                while NodeCheck <> ActiveCursor.TopNode do
                begin
                  if NodeCheck = ActiveCursor.CurrentNode then
                    EzDbCursorError(SCircularParent, ActiveCursor);
                  NodeCheck := ActiveCursor.GetParent(NodeCheck);
                end;
              end;
            end;
          end;
          inc(i);
        end;
      end;
    end;
  end;

  procedure ApplyEdit;
    //
    // Handles editing of records
    //
  var
    i: integer;

  begin
    ReIndexNodes := TList.Create;
    try
      FieldData := PVariantList(ActiveBuffer + SizeOf(TRecInfo));

      if SelfReferencing then
      begin
        lvl := 0;
        FLinks := EzDataLinks[0].FieldLinks[ActiveCursor.NodeType];
      end
      else
      begin
        lvl := ActiveCursor.Level;
        FLinks := EzDataLinks[lvl].MDFields;
      end;

      InternalSyncDataset(ActiveCursor.CurrentNode, True);
      ParentUpdated := ParentUpdateCheck;

      // Copy modified fields to detail dataset
      UpdateData;

      // Post changes for every modified dataset
      for i:=0 to FEzDataLinks.Count-1 do
        with FEzDataLinks[i] do
          if DetailDataset.State = dsEdit then
            DetailDataset.Post;

      //
      // Update position of node in cursor.
      //
      if ParentUpdated then
      begin
        MoveNode(ActiveCursor.CurrentNode, Node);
        if not DirectAccess then
          ActiveCursor.MakeNodeVisible;
      end
      else
        for i:=0 to ReIndexNodes.Count-1 do
          ActiveCursor.ReIndexNode(TRecordNode(ReIndexNodes[i]));
    finally
      ReIndexNodes.Destroy;
    end;
  end;

  procedure AppendRecord;
    //
    // Handles creation of new records
    //
  var
    pn: PNodeKey;

  begin
    FieldData := PVariantList(ActiveBuffer + SizeOf(TRecInfo));
    lvl := 0;
    if SelfReferencing then
    begin
      if (FInsertNode = nil) or
        ((FInsertPosition = ipInsertBefore) and (FInsertNode.Parent = nil))
      then
        FLinks := EzDatalinks[0].SingleNodeFields else
        FLinks := EzDatalinks[0].ChildNodeFields;
    end
    else
    begin
      if (FInsertNode <> nil) then
      begin
        lvl := ActiveCursor.Level(FInsertNode);

        if (FInsertPosition = ipInsertAsChild) and (lvl < MaxLevel) then
          inc(lvl);
      end;

      FLinks := EzDatalinks[lvl].MDFields;
    end;

    with FEzDataLinks[lvl] do
    begin
      InsertDS := DetailDataset;

      if Assigned(FInsertNode) then
        // All higher level datasets must be in sync
        // before we insert new record.
        InternalSyncDataset(FInsertNode, True);

      InsertDS.Append;
      ReIndex := True; // Avoid checking
      UpdateData;

      // did user enter value for the parent column
      if ParentRefField <> '' then
        ParentValue := InsertDS.FieldValues[ParentRefField];

      if not VarSafeIsNull(ParentValue)
         and
         (
           (FInsertPosition in [ipInsertAfter, ipInsertBefore])
           or
           (FInsertNode = nil)
           or
           (
             (FInsertPosition = ipInsertAsChild) and
             (VarSafeCompare(FInsertNode.Key.Value, ParentValue, True)<>0)
           )
           or
           (
             (FInsertPosition <> ipInsertAsChild) and
             (FInsertNode.Parent <> nil) and
             (VarSafeCompare(FInsertNode.Parent.Key.Value, ParentValue, True)<>0)
           )
         )
      then
        //
        // the user updated the value for the parent record.
        // Therefore we need to reconsider positioning.
        //
      begin
        if SelfReferencing then
          nk.level := -1 else
          nk.Level := lvl-1;

        nk.value := ParentValue;
        FInsertNode := ActiveCursor.FindNode(nk);
        if FInsertNode = ActiveCursor.EndNode then
          EzDatasetError(SParentNotFound, Self);
        FInsertPosition := ipInsertAsChild;
      end

      else if FInsertNode <> nil then
      begin
        if FInsertPosition = ipInsertAsChild then
          InsertDS.FieldValues[ParentRefField] := FInsertNode.Key.Value

        else if FInsertNode.Parent <> nil then
          InsertDS.FieldValues[ParentRefField] := FInsertNode.Parent.Key.Value;
      end
      // Copy Parent values from higer level datasets
      else if (lvl > 0) then
        // ForceResync must have been called
        InsertDS.FieldValues[ParentRefField] :=
          FEzDataLinks[lvl-1].DetailDataset.FieldValues[FEzDataLinks[lvl-1].KeyField];

      if SelfReferencing then
        nk.Level := -1 else
        nk.Level := lvl;

      if eoGenerateKeys in Options then
        //
        // Generate keys in advance
        //
      begin
        nk.Value := GenerateKeyValue(FEzDataLinks[lvl], nk.Level);
        InsertDS.FieldValues[KeyField] := nk.Value;
        InsertDS.Post;
        New(pn);
        pn^ := nk;
        FGeneratedKeys.Add(pn);
      end
      else
      begin
        InsertDS.Post;
        nk.Value := InsertDS.FieldValues[KeyField];
        if VarSafeIsNull(nk.Value) or VarIsEmpty(nk.Value) then
          EzDatasetError(SKeyValueRequired, Self);
      end;

      LinkChanged;

      // Add node to cursor
      if FInsertNode <> nil then
      begin
        case FInsertPosition of
          ipInsertAsChild:
            Node := AddChildNode(nk, FInsertNode, [], CanExpand);
          ipInsertAfter:
            if (FInsertNode.Next = nil) then
              Node := AddNode(nk, FInsertNode, [], CanExpand) else
              Node := InsertRecordNode(nk, FInsertNode.Next, [], CanExpand);
          else
            Node := InsertRecordNode(nk, FInsertNode, [], CanExpand);
        end;
      end else
        Node := AddNode(nk, nil, [], CanExpand);

      //
      // Need to make new node visible and the active record
      //
      ActiveCursor.CurrentNode := Node;
      if not DirectAccess then
        ActiveCursor.MakeNodeVisible;
      CursorposChanged;
      FInsertPosition := ipUndefined;
    end;
  end;

begin
  ParentValue := Null;
  InsertDS := nil;
  ReIndexNodes := nil;
  FParser.ClearCache;

  try
    if State = dsEdit then
      ApplyEdit else
      AppendRecord;
  except
    on E: Exception do
    begin
      if Active then
      begin
        CursorPosChanged;
        if Assigned(InsertDS) then InsertDS.Cancel;
      end;
      DatabaseError(E.Message, Self);
    end;
  end;
end;

procedure TCustomEzDataset.InternalRefresh;
begin
  inherited;
  FParser.ClearCache;  
end;

procedure TCustomEzDataset.InternalDelete;
var
  nRecords, C, lvl: Integer;
  cpSaved: TCursorOptions;
  Deleting, NextNode: TRecordNode;

  procedure RemoveGeneratedKey(Key: TNodeKey);
  var
    i: Integer;
    Found: Boolean;
    pn: PNodeKey;

  begin
    i:=0;
    Found := False;
    pn := nil;
    while not Found and (i < FGeneratedKeys.Count) do
    begin
      pn := FGeneratedKeys[i];
      Found := (pn^.Level = Key.Level) and (pn^.Value = Key.Value);
      inc(i);
    end;

    if Found then
    begin
      Dispose(pn);
      FGeneratedKeys.Delete(i);
    end;
  end;

begin
  try
    FParser.ClearCache;

    if ActiveCursorIndex <> 0 then
      DefaultCursor.Key := ActiveCursor.Key;

    if eoCascadeDelete in FOptions then
    begin
      // Need access to all records (both visible and invisible)
      cpSaved := DefaultCursor.Options;
      DefaultCursor.Options := cpSaved + [cpShowAll];
      try
        nRecords := DefaultCursor.ChildCount;

        // Move cursor to last child
        for C:=0 to nRecords-1 do
          DefaultCursor.MoveNext;

        Deleting := DefaultCursor.CurrentNode;
        NextNode := nil;
        while nRecords >= 0 do
        begin
          if Deleting.Flags * [nfAutoCreated, nfVirtual] = [] then
          begin
            if SelfReferencing then
              lvl := 0 else
              lvl := DefaultCursor.Level(Deleting);

            with FEzDataLinks[lvl] do
            begin
              InternalSyncDataset(Deleting, eoUseMDRelations in Options);
              DetailDataset.Delete;
              NextNode := DefaultCursor.PrevNode(Deleting);
              DeleteNode(Deleting);
            end;
          end;
          Dec(nRecords);
          Deleting := NextNode;
        end;
      finally
        DefaultCursor.Options := cpSaved;
        while not DefaultCursor.IsVisible do
          DefaultCursor.MovePrevious;
      end;
    end
    else if CursorHasRecord then
    begin
      InternalSyncDataset(RecordNode, eoUseMDRelations in FOptions);
      ActiveDataset.Delete;
      DeleteNode(RecordNode);
    end;
  except
    on E: Exception do
    begin
      ActiveDataset.Cancel;
      DatabaseError(E.Message, Self);
    end;
  end;
end;

procedure TCustomEzDataset.InternalCancel;
begin
  FInsertPosition := ipUndefined;
  ActiveDataset.Cancel;
end;

{ Lookup and Locate }

function TCustomEzDataset.Lookup(const KeyFields: string; const KeyValues: Variant;
  const ResultFields: string): Variant;
begin
  Result := Null;
  if LocateRecord(KeyFields, KeyValues, [], False) then
  begin
    SetTempState(dsCalcFields);
    try
      CalculateFields(TempBuffer);
      Result := FieldValues[ResultFields];
    finally
      RestoreState(dsBrowse);
    end;
  end;
end;

procedure TCustomEzDataset.NotifyBufferCountChanged(Count: Integer);
begin
  DataEvent(deBufferCountChanged, Count);
end;

procedure TCustomEzDataset.OpenCursor(InfoQuery: Boolean);
begin
  inherited;

  if not InfoQuery then
  begin
    if not Assigned(MasterDataSet) then
      EzDatasetError(SNoDataSet, Self);
    if MasterDataSet = Self then
      EzDatasetError(SRecursiveDataset, Self);
    MasterDataSet.Open;

    //
    // Initialize internal buffers.
    // Buffers are required when a sorted cursor is loaded
    //
    UpdateBufferCount;
    LoadDefaultCursorData;

    if FActiveCursorIndex <> 0 then
      LoadCursorData(FCursors[FActiveCursorIndex]);

    // Update cursor position to a valid location (first) and
    // initialize the buffers.
    InternalFirst;
    GetNextRecord;
    GetNextRecords;
  end;
end;

procedure TCustomEzDataset.Resync(Mode: TResyncMode);
begin
  if not DirectAccess then begin
    Exclude(FEzStates, esResyncPending);
    inherited;
  end else
    Include(FEzStates, esResyncPending);
end;

procedure TCustomEzDataset.StartDirectAccess(AddOptions: TCursorOptions = []; SwitchToDefaultCursor: Boolean = False);
var
  P: PDBState;

begin
  try
    if (FDirectAccessStack.Count = 0) and not IsEmpty then
      SyncCursor;

    New(P);
    Initialize(P^);

    P^.UseDefaultCursor := FUseDefaultCursor;
    P^.Key := ActiveCursor.Key;
    P^.NodeFlags := ActiveCursor.CurrentNode.Flags;

    if SwitchToDefaultCursor <> FUseDefaultCursor then
    begin
      if ActiveCursorIndex <> 0 then
      begin
        if SwitchToDefaultCursor then
          DefaultCursor.Key := Cursors[ActiveCursorIndex].Key else
          Cursors[ActiveCursorIndex].Key := DefaultCursor.Key;
      end;
      FUseDefaultCursor := SwitchToDefaultCursor;
    end;

    P^.CursorOptions := ActiveCursor.Options;
    ActiveCursor.Options := ActiveCursor.Options + AddOptions;

    FDirectAccessStack.Add(P);
  except
    raise;
  end;
end;

procedure TCustomEzDataset.SyncDataset;
begin
  if DirectAccess then
    InternalSyncDataset(ActiveCursor.CurrentNode, eoUseMDRelations in FOptions) else
    InternalSyncDataset(RecordNode, eoUseMDRelations in FOptions);
end;

procedure TCustomEzDataset.SyncCursor;
begin
  if State = dsCalcFields then
    InternalSetToRecord(CalcBuffer)
    {
      29-3-2001: KV
      Code replaced. UpdateRecordSetPosition checks for
      State = dsCalcFields so nothing would happen. By calling
      InternalSetToRecord(CalcBuffer) we know for sure the cursor
      position is updated.
        UpdateRecordSetPosition(CalcBuffer)
    }
  else
    UpdateRecordSetPosition(ActiveBuffer);
end;

procedure TCustomEzDataset.LayoutFieldMap;
var
  dotPos: integer;
  iLink: integer;

  procedure MapParentField(FieldLink: TEzFieldLink);

    //
    // Locate DataSource + Field given Link = 'dsTask.idTask'
    //

  var
    dsName: string;
    lvl: integer;

  begin
    with FieldLink do
    begin
      dsName := lowercase(Copy(Link, 1, dotPos-1));

      if dsName = 'root' then
      begin
        FParentLevel := ROOT_REF;
        lvl := 0;
      end
      else if dsName = 'parent' then
      begin
        FParentLevel := PARENT_REF;
        lvl := iLink;
      end
      else
      begin
        // Locate parent datalink using name of datasource
        lvl:=0;
        while (lvl <= iLink) and (lowercase(EzDataLinks[lvl].DataSource.Name) <> dsName) do
          Inc(lvl);

        FParentLevel := lvl;
      end;

      if lvl <= iLink then
        FField := EzDataLinks[lvl].DetailDataset.FindField(Copy(Link, dotPos+1, MaxInt));

      // did we succeed
      if  (lvl > iLink) or (FField = nil) then
        EzDatasetError(Format(SInvalidFieldLink, [Link]), Self);
    end;
  end;

  procedure LayoutFields(Links: TEzFieldLinks);
    //
    // This function maps the fields for a self referencing dataset.
    //
  var
    i: integer;
    s: string;

  begin
    with EzDataLinks[0].DetailDataset do // we can safely use master dataset
      for i:=0 to Links.Count-1 do
        with Links.Items[i] do
        begin
          FField := nil;
          FParentLevel := NO_REF;

          if Link <> '' then
          begin
              dotPos := Pos('.', Link);
              if dotPos <> 0 then
              begin
                FField := FindField(Copy(Link, dotPos+1, MaxInt));
                if FField <> nil then
                begin
                  s := lowercase(Copy(Link, 1, dotPos-1));
                  if s = 'root' then
                    FParentLevel := ROOT_REF
                  else if s = 'parent' then
                    FParentLevel := PARENT_REF
                  else
                    EzDatasetError(Format(SInvalidFieldLink, [Link]), Self);
                end
              end
              else
                FField := FindField(Link);

              //
              // FField can still be nil in which case we assume
              // Link contains a formula
              //

          end;
        end;
  end;

var
  iField: integer;

begin
  if SelfReferencing then
  begin
    with EzDataLinks[0] do
    begin
      LayoutFields(SingleNodeFields);
      LayoutFields(RootNodeFields);
      LayoutFields(ParentNodeFields);
      LayoutFields(ChildNodeFields);
    end;
  end
  else
  begin
    iLink := 0;
    while iLink < EzDataLinks.Count do
      with EzDataLinks[iLink] do
      begin
        for iField:=0 to MDFields.Count-1 do
          with MDFields.Items[iField] do
          begin
            FField := nil;
            FParentLevel := NO_REF;

            if Link <> '' then
            begin
              dotPos := Pos('.', Link);
              if dotPos <> 0 then
                MapParentField(MDFields.Items[iField]) else
                FField := DetailDataset.FindField(Link);

              //
              // FField can still be nil in which case we assume
              // Link contains a formula
              //
            end;
          end;
        inc(iLink);
      end;
  end;
end;

function TCustomEzDataset.Locate(const KeyFields: string;
  const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  DoBeforeScroll;
  Result := LocateRecord(KeyFields, KeyValues, Options, True);
  if Result then
  begin
    ActiveCursor.MakeNodeVisible;
    Resync([rmExact, rmCenter]);
    DoAfterScroll;
  end;
end;

function TCustomEzDataset.LocateRecord(const KeyFields: string;
  const KeyValues: Variant; Options: TLocateOptions;
  SyncCursor: Boolean): Boolean;
var
  Buffer: TRecordBuffer;
  i: Integer;
  ds: TDataset;
  nk: TNodeKey;

begin
  CheckBrowseMode;
  UpdateCursorPos;
  CursorPosChanged;
  Result := false;
  try
    if SelfReferencing then
    begin
      Result := MasterDataset.Locate(KeyFields, KeyValues, Options);
      if Result and SyncCursor then
      begin
        nk.Level := -1;
        nk.Value := MasterDataset.FieldValues[EzDataLinks[0].KeyField];
        ActiveCursor.MoveToKey(nk);
      end;
    end
    else
    begin
      nk.Level := 0;
      ds := nil;
      while (nk.Level <= Maxlevel) and (Result = false) do
      begin
        ds := EzDataLinks[nk.Level].DetailDataset;
        Result := ds.Locate(KeyFields, KeyValues, Options);
        if not Result then
          Inc(nk.Level);
      end;

      if Result and SyncCursor then
      begin
        nk.Value := ds.FieldValues[EzDataLinks[nk.level].KeyField];
        ActiveCursor.MoveToKey(nk);
      end;
    end;

    if Result and not SyncCursor then
    begin
      Buffer := TRecordBuffer(TempBuffer);

      { For lookups, read all field values into the temp buffer }
      for I := 0 to Self.Fields.Count - 1 do
        with Self.Fields[I] do
          if FieldKind = fkData then
            PVariantList(Buffer+SizeOf(TRecInfo))[Index] := ActiveDataset.Fields[FieldNo-1].Value;
    end;
  except
    Result := False;
  end;
end;

{ Indexes }

function TCustomEzDataset.GetRecordCount: Longint;
begin
  CheckActive;
  Result := ActiveCursor.RecordCount;
end;

function TCustomEzDataset.GetRecNo: Longint;
var
  RecBuf: TRecBuf;

begin
  CheckActive;                    
  Result := -1;

  if DirectAccess then
    Result := ActiveCursor.RecNo

  else if GetActiveRecBuf(RecBuf) then
    if PRecInfo(RecBuf)^.BookmarkFlag = bfCurrent then
      Result := ActiveCursor.NodeRecNo(PRecInfo(RecBuf)^.Bookmark);
end;

procedure TCustomEzDataset.SetFiltered(Value: Boolean);
begin
  if Value <> Filtered then
  begin
    inherited SetFiltered(Value);
    if Active then
    begin
      ActiveCursor.MoveFirst;
      Resync([]);
    end;
  end;
end;

procedure TCustomEzDataset.SetRecNo(Value: Integer);
begin
  if RecNo <> Value then
  begin
    DoBeforeScroll;
    ActiveCursor.RecNo := Value;
    Resync([rmCenter]);
    DoAfterScroll;
  end;
end;

procedure TCustomEzDataset.SetTopRecNo(Value: Integer);
begin
  if TopRecNo<>Value then
    SetTopNode(ActiveCursor.NodeAtIndex(Value-1));
end;

procedure TCustomEzDataset.SetTopNode(Node: TRecordNode);
begin
  ClearBuffers;

  ActiveCursor.CurrentNode := Node;

  if GetRecord(Buffers[0], gmCurrent, True) = grOK then
    //
    // Only fetch next records when Eof and Bof are false  
    //
  begin
    ActivateBuffers;
    GetNextRecords;
  end;
  DataEvent(deDataSetChange, 0);
end;

end.



