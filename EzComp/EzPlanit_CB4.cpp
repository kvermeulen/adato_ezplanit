//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("EzPlanIT_CB4.res");
USEUNIT("EzScheduler.pas");
USEFORMNS("EzCalendar.pas", Ezcalendar, EzCalendar);
USEUNIT("EzDataset.pas");
USEUNIT("EzDBGrid.pas");
USEUNIT("EzGantt.pas");
USEUNIT("EzGraph.pas");
USEUNIT("EzReg.pas");
USEUNIT("ExMasks.pas");
USEUNIT("EZPILib_TLB.pas");
USERES("EZPILib_TLB.dcr");
USEFORMNS("EzDsDsgn.pas", Ezdsdsgn, EzDatasetDesigner);
USEPACKAGE("vclx40.bpi");
USEPACKAGE("vcldb40.bpi");
USEPACKAGE("vcl40.bpi");
USEPACKAGE("dclstd40.bpi");
USEPACKAGE("dcldb40.bpi");
USEFORMNS("EzPreview.pas", Ezpreview, frmPrintPreview);
USEUNIT("EzPrinter.pas");
USEPACKAGE("EzBasic_CB4.bpi");
USEFORMNS("EzEpDsgn.pas", Ezepdsgn, EzEndPointDesigner);
USEFORMNS("Splash.pas", Splash, frmSplash);
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------
//   Package source.
//---------------------------------------------------------------------------
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void*)
{
  return 1;
}
//---------------------------------------------------------------------------
