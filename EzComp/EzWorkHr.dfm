object frmSetWorkingHours: TfrmSetWorkingHours
  Left = 222
  Top = 114
  Caption = 'Working hours'
  ClientHeight = 168
  ClientWidth = 257
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 227
    Height = 39
    Caption = 
      'Enter the daily working periods in the grid below.'#13#10'You can add ' +
      'as many intervals as you like.'#13#10'Make sure that intervals do not ' +
      'overlap.'
    WordWrap = True
  end
  object GdWorkingHours: TStringGrid
    Left = 8
    Top = 64
    Width = 169
    Height = 97
    ColCount = 2
    DefaultRowHeight = 18
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
    ScrollBars = ssNone
    TabOrder = 0
    OnSelectCell = GdWorkingHoursSelectCell
    ColWidths = (
      85
      79)
  end
  object btnOK: TButton
    Left = 184
    Top = 136
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object btnCancel: TButton
    Left = 183
    Top = 105
    Width = 75
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
