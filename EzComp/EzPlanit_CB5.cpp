//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("EzPlanIT_CB5.res");
USEPACKAGE("vcl50.bpi");
USEFORMNS("EzCalendar.pas", Ezcalendar, EzCalendar);
USEUNIT("EzDataset.pas");
USEUNIT("EzDBGrid.pas");
USEUNIT("EzGantt.pas");
USEUNIT("EzGraph.pas");
USEUNIT("EzScheduler.pas");
USEPACKAGE("Vcldb50.bpi");
USEPACKAGE("EzBasic_CB5.bpi");
USEPACKAGE("dcldb50.bpi");
USEFORMNS("EzPreview.pas", Ezpreview, frmPrintPreview);
USEUNIT("EzPrinter.pas");
USEPACKAGE("dsnide50.bpi");
USEFORMNS("EzWorkHr.pas", Ezworkhr, frmSetWorkingHours);
USEFORMNS("EzRuleEd.pas", Ezruleed, frmRule);
USEPACKAGE("EzSpecials_CB5.bpi");
//---------------------------------------------------------------------------
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Package source.
//---------------------------------------------------------------------------

#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void*)
{
        return 1;
}
//---------------------------------------------------------------------------
