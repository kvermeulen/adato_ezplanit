object dmProjectData: TdmProjectData
  OldCreateOrder = False
  Height = 640
  Width = 607
  object cnnProject: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=\\DrL' +
      'inux\development\Delphi\EzPlanIT\Demos\EzProject\EzProject.mdb;M' +
      'ode=Share Deny None;Persist Security Info=False;Jet OLEDB:System' +
      ' database="";Jet OLEDB:Registry Path="";Jet OLEDB:Database Passw' +
      'ord="";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1' +
      ';Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Trans' +
      'actions=1;Jet OLEDB:New Database Password="";Jet OLEDB:Create Sy' +
      'stem Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:D' +
      'on'#39't Copy Locale on Compact=False;Jet OLEDB:Compact Without Repl' +
      'ica Repair=False;Jet OLEDB:SFP=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    AfterConnect = cnnProjectAfterConnect
    BeforeConnect = cnnProjectBeforeConnect
    Left = 40
    Top = 24
  end
  object qrProjects: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    BeforeDelete = qrProjectsBeforeDelete
    IndexFieldNames = 'prDescription'
    TableName = 'Projects'
    Left = 40
    Top = 88
    object qrProjectsidProject: TAutoIncField
      DisplayLabel = 'Project #'
      FieldName = 'idProject'
    end
    object qrProjectsprDescription: TWideStringField
      DisplayLabel = 'Description'
      FieldName = 'prDescription'
      Size = 50
    end
    object qrProjectsprStart: TDateTimeField
      DisplayLabel = 'Project start date'
      FieldName = 'prStart'
    end
    object qrProjectsprStop: TDateTimeField
      DisplayLabel = 'Project end date'
      FieldName = 'prStop'
    end
  end
  object dsProjects: TDataSource
    DataSet = qrProjects
    Left = 152
    Top = 88
  end
  object dsConstraintNames: TDataSource
    DataSet = qrConstraintNames
    Left = 152
    Top = 216
  end
  object qrConstraintNames: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    IndexName = 'PrimaryKey'
    TableName = 'ConstraintNames'
    Left = 40
    Top = 216
  end
  object qrPriorities: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    IndexName = 'idPriority'
    TableName = 'Priorities'
    Left = 40
    Top = 264
  end
  object dsPriorities: TDataSource
    DataSet = qrPriorities
    Left = 152
    Top = 264
  end
  object qrRelations: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    IndexName = 'idPriority'
    TableName = 'Relations'
    Left = 40
    Top = 312
  end
  object dsRelations: TDataSource
    DataSet = qrRelations
    Left = 152
    Top = 312
  end
  object qrResources: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    BeforeDelete = qrResourcesBeforeDelete
    TableName = 'Resources'
    Left = 40
    Top = 136
    object qrResourcesidResource: TAutoIncField
      FieldName = 'idResource'
    end
    object qrResourcesrsName: TWideStringField
      DisplayLabel = 'Name'
      FieldName = 'rsName'
      Size = 50
    end
    object qrResourcesrsAllowTaskSwitching: TBooleanField
      FieldName = 'rsAllowTaskSwitching'
    end
  end
  object dsResources: TDataSource
    DataSet = qrResources
    Left = 152
    Top = 136
  end
  object qrIdentity: TADOQuery
    Connection = cnnProject
    Parameters = <>
    SQL.Strings = (
      'select @@identity')
    Left = 40
    Top = 384
  end
  object qrStatusNames: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    IndexName = 'PrimaryKey'
    TableName = 'StatusNames'
    Left = 40
    Top = 440
  end
  object dsStatusNames: TDataSource
    DataSet = qrStatusNames
    Left = 152
    Top = 440
  end
  object qrVersion: TADOQuery
    Connection = cnnProject
    Parameters = <>
    SQL.Strings = (
      'select * from Version')
    Left = 152
    Top = 24
  end
  object qrCapacities: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    BeforeDelete = qrResourcesBeforeDelete
    IndexFieldNames = 'caDescription'
    TableName = 'Capacities'
    Left = 40
    Top = 496
    object qrCapacitiesidCapacity: TAutoIncField
      FieldName = 'idCapacity'
      ReadOnly = True
    end
    object qrCapacitiescaDescription: TWideStringField
      FieldName = 'caDescription'
      Size = 50
    end
    object qrCapacitiescaMergeProfiles: TBooleanField
      FieldName = 'caMergeProfiles'
    end
  end
  object dsCapacities: TDataSource
    DataSet = qrCapacities
    Left = 152
    Top = 496
  end
  object qrResourceCapacities: TADOQuery
    Connection = cnnProject
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idResource'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select * from ResourceCapacities '
      'where idResource=:idResource')
    Left = 256
    Top = 136
    object qrResourceCapacitiesidResource: TIntegerField
      FieldName = 'idResource'
    end
    object qrResourceCapacitiesidCapacity: TIntegerField
      FieldName = 'idCapacity'
    end
  end
  object qrResourceOperators: TADOQuery
    Connection = cnnProject
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idResource'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select Type, idOperator, AllocationUnits from Operators '
      'where idResource = :idResource')
    Left = 368
    Top = 136
    object qrResourceOperatorsType: TWideStringField
      FieldName = 'Type'
      FixedChar = True
      Size = 1
    end
    object qrResourceOperatorsidOperator: TIntegerField
      FieldName = 'idOperator'
    end
    object qrResourceOperatorsAllocationUnits: TFloatField
      FieldName = 'AllocationUnits'
    end
  end
  object qrResourceTypes: TADOTable
    Connection = cnnProject
    CursorType = ctStatic
    IndexName = 'PrimaryKey'
    TableName = 'TaskResourceTypes'
    Left = 248
    Top = 216
  end
  object dsResourceTypes: TDataSource
    DataSet = qrResourceTypes
    Left = 360
    Top = 216
  end
end
