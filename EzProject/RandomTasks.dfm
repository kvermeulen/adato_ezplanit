object frmRandomTasks: TfrmRandomTasks
  Left = 548
  Top = 227
  Width = 371
  Height = 177
  Caption = 'Create random tasks'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 14
    Width = 282
    Height = 13
    Caption = 'How many tasks (parents+childs) do you want to be created'
  end
  object Label2: TLabel
    Left = 10
    Top = 88
    Width = 41
    Height = 13
    Caption = 'Progress'
  end
  object Label3: TLabel
    Left = 8
    Top = 67
    Width = 43
    Height = 13
    Caption = 'Duration:'
  end
  object lbCount: TLabel
    Left = 67
    Top = 47
    Width = 69
    Height = 13
    AutoSize = False
  end
  object lbDuration: TLabel
    Left = 67
    Top = 67
    Width = 69
    Height = 13
    AutoSize = False
  end
  object Label5: TLabel
    Left = 17
    Top = 47
    Width = 34
    Height = 13
    Caption = 'Task #'
  end
  object edCount: TEdit
    Left = 296
    Top = 10
    Width = 41
    Height = 21
    TabOrder = 0
    Text = '5'
  end
  object pbProgress: TProgressBar
    Left = 67
    Top = 86
    Width = 286
    Height = 16
    TabOrder = 2
  end
  object btnCreate: TButton
    Left = 200
    Top = 112
    Width = 75
    Height = 25
    Caption = 'C&reate'
    TabOrder = 3
    OnClick = btnCreateClick
  end
  object btnClose: TButton
    Left = 280
    Top = 112
    Width = 75
    Height = 25
    Caption = '&Close'
    ModalResult = 2
    TabOrder = 4
  end
  object udCount: TUpDown
    Left = 337
    Top = 10
    Width = 15
    Height = 21
    Associate = edCount
    Min = 1
    Position = 5
    TabOrder = 1
  end
end
