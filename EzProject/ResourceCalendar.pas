unit ResourceCalendar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DesignPanel, EzCalendar, StdCtrls, Db, ADODB, ExtCtrls, EzArrays,
  EzCalendarControl;

resourceString
  SRuleNotFound = 'Failed to locate record for updating.';

type
  TfrmResourceCalendar = class(TForm)
    Panel1: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Calendar: TEzCalendarPanel;
    procedure CalendarRuleChanged(Sender: TEzCalendarPanel; Event: TEzListNotification; Rule: TEzCalendarRule);
  private
    { Private declarations }

  protected
    FCalendarID: Integer;
    FCalendarType: string;

  public
    procedure Open;

    // Type of calendar to edit
    // i.e. 'RESOURCE' for resource calendar
    //      'PROJECT' for project calendar
    property CalendarType: string read FCalendarType write FCalendarType;

    // ID of calendar (Resource id or Project id)
    property CalendarID: Integer read FCalendarID write FCalendarID;
  end;

implementation

uses ProjectData, Main;

{$R *.DFM}

procedure TfrmResourceCalendar.Open;
begin
  Calendar.BeginUpdate;
  try
    LoadResourceRules(Calendar.Rules, dmProjectData.cnnProject, CalendarType, FCalendarID);
  finally
    Calendar.EndUpdate;
  end;

  Calendar.Date := Trunc(Now);
end;

procedure TfrmResourceCalendar.CalendarRuleChanged(
  Sender: TEzCalendarPanel; Event: TEzListNotification; Rule: TEzCalendarRule);
const
  sAddSql = 'insert into CalendarRules(crCalendarType, crID, crStart, crStop, crIsWorking, crCount, crRangeStart, crRangeEnd, crOccurrences) ' +
            'values(''%s'', %d, ''%s'', ''%s'', %d, %d, ''%s'', ''%s'', %d)';
  sDeleteSql = 'delete from CalendarRules where idCalendarRule=%d';

  function SqlDate(D: TDateTime): string;
  begin
    Result := FormatDateTime('yyyy/mm/dd hh:mm:ss', D);
  end;
var
  s: string;

begin
  if Calendar.UpdateCount>0 then Exit;
  case Event of
    elnAdded:
    begin
      s := Format(sAddSql, [CalendarType, CalendarID, SqlDate(Rule.Start), SqlDate(Rule.Stop), Integer(Rule.IntervalType=itWorking), Rule.Count, SqlDate(Rule.RangeStart), SqlDate(Rule.RangeEnd), Rule.Occurrences]);
      dmProjectData.cnnProject.Execute(s);
      Rule.Tag := dmProjectData.cnnProject.Execute('select @@identity').Fields[0].Value;
    end;

    elnDeleted:
      dmProjectData.cnnProject.Execute(Format(sDeleteSql, [Rule.Tag]));
  end;
end;

end.

