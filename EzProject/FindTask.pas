unit FindTask;

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, EzDBGrid, ExtCtrls, DB, DBCLient;

type
  TfrmFindTask = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    edFilter: TEdit;
    dsTasks: TDataSource;
    Label1: TLabel;                                        
    btnCancel: TButton;
    btnOK: TButton;
    Panel3: TPanel;
    EzDBCoolGrid1: TEzDBCoolGrid;
    cdsFindTask: TClientDataSet;
    procedure edFilterChange(Sender: TObject);
    procedure cdsFindTaskFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private

  protected
    procedure SetDataset(Value:TClientDataset);

  public
    procedure RecaptureDataset;
    procedure ReleaseDataset;

    property Dataset: TClientDataset write SetDataset;

  end;

var
  frmFindTask: TfrmFindTask;

implementation

{$R *.dfm}

{$IFDEF EZ_D6}
uses Variants;
{$ENDIF}

procedure TfrmFindTask.RecaptureDataset;
begin
{
  FDataset.IndexName := 'ixAlphabetical';
  FDataset.Filtered := True;
  FDataset.Bookmark := Bkm;
  FDataset.EnableControls;
}
end;

procedure TfrmFindTask.ReleaseDataset;
begin
{
  Bkm := FDataset.Bookmark;
  FDataset.Filtered := False;
  FDataset.IndexName := 'ixidTask';
  FDataset.DisableControls;
}
end;

procedure TfrmFindTask.SetDataset(Value:TClientDataset);
begin
  if Value <> cdsFindTask.CloneSource then
  begin
    if Value <> nil then
    begin
      cdsFindTask.CloneCursor(Value, false, True);
      cdsFindTask.IndexName := 'ixAlphabetical';
      cdsFindTask.OnFilterRecord := cdsFindTaskFilterRecord;
    end else
      cdsFindTask.Close;
  end;
end;

procedure TfrmFindTask.edFilterChange(Sender: TObject);
begin
  cdsFindTask.Filtered := False;
  cdsFindTask.Filtered := True;
end;

procedure TfrmFindTask.cdsFindTaskFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  if edFilter.Text <> '' then
    Accept := Pos(lowercase(edFilter.Text), lowercase(Dataset.FieldByName('taDescription').AsString)) <> 0 else
    Accept := True;
end;

end.
