unit GanttChart;

{$I Ez.inc}

interface

uses Windows, Classes, Contnrs, Graphics, Forms, Controls, StdCtrls, Db,
  EzDataSet,
  Main, EzGantt, Grids, DBGrids, EzDBGrid, DBTables, ComCtrls, ToolWin,
  ExtCtrls, EzCalendar, EzScheduler, ComObj, ActiveX, Mask, DBCtrls,
  EzGraph, ImgList, EzColorCombobox, EzTimeBar, Buttons, Menus,
  ADODB, Provider, DBClient, ActnList, DelayedIdle, EzDurationField, EzArrays,
  EzDateTime, System.Actions;

resourcestring
  SApplyChanges = 'Changes have not been saved, save data before closing?';
  SResourceNoTask = 'Can only add resources when a task is selected.';
  SPredecessorNoTask = 'Can only add predecessors when a task is selected.';
  SConfirmDeleteTask = 'Delete task ''%s'' and all its children?';
  SNone = 'Default sorting';

const
  NORMAL_PRIORITY=2;

type
  TfrmGanttChart = class;

  PFixedKey = ^TFixedKey;
  TFixedKey = record
    OldKey, NewKey: Variant;
  end;

  TResourceGraphProperties = class
  public
    ResourceID: Integer;
    Line: TEzGraphLine;
    Color: TColor;
    Filled: Boolean;
    GraphType: Integer;
  end;

  TfrmGanttChart = class(TForm)
    dsTasks: TDataSource;
    pnlBottom: TPanel;
    ezdsTaskTree: TEzDataset;
    Scheduler: TEzScheduler;
    pcBottomPane: TPageControl;
    shScheduleMessages: TTabSheet;
    ImageList1: TImageList;
    lvScheduleMessages: TListView;
    dsTasksSource: TDataSource;
    imgEndPoints: TImageList;
    eplEndPoints: TEzEndPointList;
    qrTasks: TADODataSet;
    qrTaskResources: TADODataSet;
    dsTaskResources: TDataSource;
    dsTaskPredecessors: TDataSource;
    cdsTasks: TClientDataSet;
    dspTaskProvider: TDataSetProvider;
    dspResources: TDataSetProvider;
    dspPredecessors: TDataSetProvider;
    cdsTaskResources: TClientDataSet;
    cdsTaskPredecessors: TClientDataSet;
    cdsTaskResourcestridResource: TIntegerField;
    cdsTaskResourcestrAssigned: TFloatField;
    cdsTaskPredecessorstpidPredecessor: TIntegerField;
    cdsTaskPredecessorstpidRelation: TIntegerField;
    cdsTaskPredecessorstpLag: TFloatField;
    cdsTaskResourcestridTask: TIntegerField;
    cdsTaskPredecessorstpidTask: TIntegerField;
    cdsTasksidTask: TAutoIncField;
    cdsTaskstaConstraintDate: TDateTimeField;
    cdsTaskstaDescription: TWideStringField;
    cdsTaskstaidConstraint: TIntegerField;
    cdsTaskstaidParent: TIntegerField;
    cdsTaskstaidProject: TIntegerField;
    cdsTaskstaidPriority: TIntegerField;
    cdsTaskstaProgress: TIntegerField;
    cdsTaskstaStart: TDateTimeField;
    cdsTaskstaStop: TDateTimeField;
    cdsTaskstaWork: TFloatField;
    cdsTaskResourceslkResourceName: TStringField;
    cdsTaskPredecessorslkRelation: TStringField;
    shReconcileErrors: TTabSheet;
    lvReconcileErrors: TListView;
    Panel1: TPanel;
    btnReconcileTask: TSpeedButton;
    Panel2: TPanel;
    btnScrollToTask: TSpeedButton;
    qrTaskPredecessors: TADODataSet;
    ActionList1: TActionList;
    acShowGraph: TAction;
    acStackGraph: TAction;
    acRefresh: TAction;
    acScrollToReconcileError: TAction;
    acScrollToScheduleError: TAction;
    acScheduleErrorInfo: TAction;
    SpeedButton1: TSpeedButton;
    cdsTaskPredecessorslkPredecessor: TStringField;
    shTaskFields: TTabSheet;
    pnlTaskDescription: TPanel;
    edTaskDescription: TDBEdit;
    acApplyChanges: TAction;
    acCancelChanges: TAction;
    acSelectParent: TAction;
    acEditStartDate: TAction;
    acEditStopdate: TAction;
    PopupMenu1: TPopupMenu;
    cdsTaskstaMemo: TMemoField;
    acEditConstraintDate: TAction;
    shGraphProps: TTabSheet;
    Panel3: TPanel;
    lvGraphResources: TListView;
    New1: TMenuItem;
    Addchild1: TMenuItem;
    Delete1: TMenuItem;
    cdsTaskstaLocked: TBooleanField;
    acRemoveParent: TAction;
    ilButtonImages: TImageList;
    scGridSchemes: TEzTreeGridSchemes;
    cdsTaskstaidStatus: TIntegerField;
    DelayedIdle: TEzDelayedIdle;
    pcTaskDetail: TPageControl;
    tsTaskGeneral: TTabSheet;
    Label3: TLabel;
    edWork: TDBEdit;
    Label4: TLabel;
    edProgress: TDBEdit;
    udProgress: TUpDown;
    Label11: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    tsTaskConstraint: TTabSheet;
    Label9: TLabel;
    cbConstraint: TDBLookupComboBox;
    Label10: TLabel;
    edConstraintDate: TDBEdit;
    SpeedButton8: TSpeedButton;
    tsTaskDates: TTabSheet;
    Label5: TLabel;
    edStart: TDBEdit;
    Label6: TLabel;
    edStop: TDBEdit;
    btnStartDate: TSpeedButton;
    btnStopDate: TSpeedButton;
    tsTaskMemo: TTabSheet;
    DBMemo1: TDBMemo;
    Label12: TLabel;
    edActualStart: TDBEdit;
    SpeedButton5: TSpeedButton;
    lbScheduleStop: TLabel;
    edScheduleStop: TDBEdit;
    SpeedButton6: TSpeedButton;
    acEditActualStart: TAction;
    acEditActualStop: TAction;
    Label2: TLabel;
    tsTaskResources: TTabSheet;
    tsTaskPredecessors: TTabSheet;
    Label8: TLabel;
    cbTaskStatus: TDBLookupComboBox;
    DBEdit3: TDBEdit;
    chkLocked: TDBCheckBox;
    cdsTaskstaActualStart: TDateTimeField;
    cdsTaskstaActualStop: TDateTimeField;
    tsDebugInfo: TTabSheet;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit1: TDBEdit;
    Label15: TLabel;
    DBEdit2: TDBEdit;
    Label16: TLabel;
    DBEdit4: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    RecordStatusImages: TImageList;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    cdsTaskResourcesClone: TClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    FloatField1: TFloatField;
    StringField1: TStringField;
    cdsTaskPredecessorsClone: TClientDataSet;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StringField2: TStringField;
    IntegerField5: TIntegerField;
    StringField3: TStringField;
    FloatField2: TFloatField;
    pnlTop: TPanel;
    EzGrid: TEzDBTreeGrid;
    GanttChartSplitter: TSplitter;
    pnlGanttChart: TPanel;
    EzTimebar: TEzTimebar;
    EzGantt: TEzGanttChart;
    TaskDetailSplitter: TSplitter;
    pnlGraph: TPanel;
    ResourceGraphs: TEzGraph;
    pnlFillGraph: TPanel;
    GraphSplitter: TSplitter;
    cdsTaskstaOrder: TIntegerField;
    cdsTaskstaExpanded: TBooleanField;
    DBEdit6: TDBEdit;
    Label1: TLabel;
    Panel4: TPanel;
    SpeedButton7: TSpeedButton;
    SpeedButton10: TSpeedButton;
    acAddPredecessor: TAction;
    acDeletePredecessor: TAction;
    acAddResource: TAction;
    acDeleteResource: TAction;
    Panel5: TPanel;
    SpeedButton11: TSpeedButton;
    SpeedButton12: TSpeedButton;
    CoolBar1: TCoolBar;
    ToolBar3: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    qrProject: TADODataSet;
    qrProjectprActiveScale: TWideStringField;
    qrProjectprDescription: TWideStringField;
    qrTasksidTask: TAutoIncField;
    qrTaskstaConstraintDate: TDateTimeField;
    qrTaskstaDescription: TWideStringField;
    qrTaskstaidConstraint: TIntegerField;
    qrTaskstaidParent: TIntegerField;
    qrTaskstaidProject: TIntegerField;
    qrTaskstaidPriority: TIntegerField;
    qrTaskstaidStatus: TIntegerField;
    qrTaskstaProgress: TIntegerField;
    qrTaskstaStart: TDateTimeField;
    qrTaskstaActualStart: TDateTimeField;
    qrTaskstaStop: TDateTimeField;
    qrTaskstaActualStop: TDateTimeField;
    qrTaskstaWork: TFloatField;
    qrTaskstaMemo: TMemoField;
    qrTaskstaLocked: TBooleanField;
    qrTaskstaOrder: TIntegerField;
    qrTaskstaExpanded: TBooleanField;
    btnIndicateConstraintDate: TSpeedButton;
    acIndicateConstraintDate: TAction;
    ezdsTaskTreeidTask: TAutoIncField;
    ezdsTaskTreetaidProject: TIntegerField;
    ezdsTaskTreetaidParent: TIntegerField;
    ezdsTaskTreetaDescription: TWideStringField;
    ezdsTaskTreetaWork: TEzDurationField;
    ezdsTaskTreeTaskDuration: TEzDurationField;
    ezdsTaskTreetaProgress: TIntegerField;
    ezdsTaskTreetaStart: TDateTimeField;
    ezdsTaskTreetaActualStart: TDateTimeField;
    ezdsTaskTreetaStop: TDateTimeField;
    ezdsTaskTreetaActualStop: TDateTimeField;
    ezdsTaskTreetaidStatus: TIntegerField;
    ezdsTaskTreetaidPriority: TIntegerField;
    ezdsTaskTreelkPriority: TStringField;
    ezdsTaskTreetaidConstraint: TIntegerField;
    ezdsTaskTreelkConstraint: TStringField;
    ezdsTaskTreetaConstraintDate: TDateTimeField;
    ezdsTaskTreelkParent: TStringField;
    ezdsTaskTreetaMemo: TMemoField;
    ezdsTaskTreetaLocked: TBooleanField;
    ezdsTaskTreeIsLocked: TBooleanField;
    ezdsTaskTreeScheduleStart: TDateTimeField;
    ezdsTaskTreeScheduleStop: TDateTimeField;
    ezdsTaskTreeScheduleConstraint: TIntegerField;
    ezdsTaskTreeScheduleConstraintDate: TDateTimeField;
    ezdsTaskTreetaOrder: TIntegerField;
    ezdsTaskTreetaExpanded: TBooleanField;
    tsCapacity: TTabSheet;
    Label7: TLabel;
    DBLookupComboBox2: TDBLookupComboBox;
    qrTaskstaCapacity: TIntegerField;
    cdsTaskstaCapacity: TIntegerField;
    ezdsTaskTreetaCapacity: TIntegerField;
    ezdsTaskTreelkCapacity: TStringField;
    SpeedButton2: TSpeedButton;
    acRemoveCapacity: TAction;
    grTaskResources: TDBGrid;
    cdsTaskResourcestrType: TIntegerField;
    cdsTaskResourcesClonetrType: TIntegerField;
    cdsTaskResourceslkResourceType: TStringField;
    cdsTaskResourcesClonelkResourceType: TStringField;
    grTaskPredecessors: TDBGrid;
    SingleTaskScheduler: TEzScheduler;
    chkGraphShowing: TCheckBox;
    pnlGraphProperties: TPanel;
    Label19: TLabel;
    Label18: TLabel;
    GraphType: TComboBox;
    chkFilled: TCheckBox;
    cbLineColor: TEzColorComboBox;
    procedure acIndicateConstraintDateUpdate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure acIndicateConstraintDateExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qrTasksNewRecord(DataSet: TDataSet);
    procedure ezdsTaskTreeGetVariableValue(Dataset: TDataSet;
      Variable: String; var Result: Variant);
    procedure FormActivate(Sender: TObject);
    procedure SmallintField2SetText(Sender: TField; const Text: String);
    procedure EzGanttGetPredecessor(Sender: TObject;
      var Predecessor: TPredecessor);
    procedure EzGanttDrawEnd(Sender: TObject);
    procedure EzGanttDrawBegin(Sender: TObject;
      var bEraseBackground: Boolean);
    procedure ezdsTaskTreeFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure cbLineColorChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EzGanttStartbarDrag(Sender: TObject; X, Y: Integer; BarInfo: TScreenbar;
      DragType: TGanttBarDragType; var CanDrag: Boolean);
    procedure EzGanttLinkTasks(Sender: TObject; Predecessor,
      Successor: TNodeKey; Relation: TEzPredecessorRelation);
    procedure ezdsTaskTreeBeforeScroll(DataSet: TDataSet);
    procedure ezdsTaskTreeRelocateDataLink(Datalink: TEzDataLink;
      const Key: TNodeKey);
    procedure cdsTaskResourcesNewRecord(DataSet: TDataSet);
    procedure cdsTaskPredecessorsNewRecord(DataSet: TDataSet);
    procedure cdsTaskPredecessorsNeedInvalidate(DataSet: TDataSet);
    procedure cdsTaskResourcesNeedRefresh(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acShowGraphUpdate(Sender: TObject);
    procedure acShowGraphExecute(Sender: TObject);
    procedure lvGraphResourcesChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure cdsTasksBeforeDelete(DataSet: TDataSet);
    procedure acScrollToReconcileErrorExecute(Sender: TObject);
    procedure acScrollToReconcileErrorUpdate(Sender: TObject);
    procedure acScrollToScheduleErrorExecute(Sender: TObject);
    procedure acScrollToScheduleErrorUpdate(Sender: TObject);
    procedure acScheduleErrorInfoExecute(Sender: TObject);
    procedure acScheduleErrorInfoUpdate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EzGanttGetBarProps(Sender: TObject;
      const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
    procedure acApplyChangesExecute(Sender: TObject);
    procedure acApplyChangesUpdate(Sender: TObject);
    procedure acCancelChangesExecute(Sender: TObject);
    procedure acCancelChangesUpdate(Sender: TObject);
    procedure acEditStartDateExecute(Sender: TObject);
    procedure acEditStartDateUpdate(Sender: TObject);
    procedure ezdsTaskTreelkParentGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure acEditStopdateExecute(Sender: TObject);
    procedure acEditStopdateUpdate(Sender: TObject);
    procedure udProgressClick(Sender: TObject; Button: TUDBtnType);
    procedure EzGanttGetHint(Sender: TCustomEzGanttChart;
      HitInfo: TEzGanttHitInfo; var HintText: String);
    procedure udProgressChangingEx(Sender: TObject;
      var AllowChange: Boolean; NewValue: Smallint;
      Direction: TUpDownDirection);
    procedure acEditConstraintDateExecute(Sender: TObject);
    procedure acEditConstraintDateUpdate(Sender: TObject);
    procedure acRemoveParentExecute(Sender: TObject);
    procedure acRemoveParentUpdate(Sender: TObject);
    procedure cdsTaskResourcesBeforeInsert(DataSet: TDataSet);
    procedure cdsTaskPredecessorsBeforeInsert(DataSet: TDataSet);
    procedure acEditActualStartExecute(Sender: TObject);
    procedure acEditActualStartUpdate(Sender: TObject);
    procedure acEditActualStopExecute(Sender: TObject);
    procedure acEditActualStopUpdate(Sender: TObject);
    procedure ezdsTaskTreeBeforePost(DataSet: TDataSet);
    procedure EzGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ezdsTaskTreetaidConstraintChange(Sender: TField);
    procedure cdsTaskResourcesAfterOpen(DataSet: TDataSet);
    procedure cdsTaskPredecessorsAfterOpen(DataSet: TDataSet);
    procedure DelayedIdleDelayedIdle(Sender: TObject;
      var NewState: TEzIdleState);
    procedure EzGanttEndbarDrag(Sender: TObject; X, Y: Integer;
      BarInfo: TScreenbar; DragType: TGanttBarDragType;
      var UpdateDataset: Boolean);
    procedure ezdsTaskTreeBeforeDelete(DataSet: TDataSet);
    procedure pnlGraphResize(Sender: TObject);
    procedure GanttChartSplitterMoved(Sender: TObject);
    procedure ezdsTaskTreeAfterMoveRecord(Dataset: TDataSet; Node,
      Location: TRecordNode; Position: TEzInsertPosition);
    procedure EzGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ezdsTaskTreeAfterPost(DataSet: TDataSet);
    procedure acAddPredecessorExecute(Sender: TObject);
    procedure acDeletePredecessorExecute(Sender: TObject);
    procedure acDeletePredecessorUpdate(Sender: TObject);
    procedure acAddResourceExecute(Sender: TObject);
    procedure acDeleteResourceExecute(Sender: TObject);
    procedure acDeleteResourceUpdate(Sender: TObject);
    procedure ezdsTaskTreeBeforeInsert(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acSetScaleYearMonthExecute(Sender: TObject);
    procedure acSetScaleWeekDayExecute(Sender: TObject);
    procedure acTimebarOptionsExecute(Sender: TObject);
    procedure EzGanttOverGanttChart(Sender: TCustomEzGanttChart; HitInfo: TEzGanttHitInfo);
    procedure ezdsTaskTreeAfterExpandRecord(Dataset: TDataSet;
      Node: TRecordNode);
    procedure ezdsTaskTreeAfterOpen(DataSet: TDataSet);
    procedure EzGanttSelectPredecessorLine(Sender: TObject;
      Predecessor: TPredecessor; var CanSelect: Boolean);
    procedure EzGanttMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EzGanttMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cdsTaskPredecessorsAfterPost(DataSet: TDataSet);
    procedure EzGanttDragDrop(Sender: TCustomEzGanttChart; Source: TObject;
      State: TDragState; Pt: TPoint; HitInfo: TEzGanttHitInfo;
      var Operations: TEzGanttDragOperations;
      var Relation: TEzPredecessorRelation; var Accept: Boolean);
    procedure EzTimebarLayoutChange(Sender: TObject);
    procedure acRemoveCapacityExecute(Sender: TObject);
    procedure chkFilledClick(Sender: TObject);
    procedure EzGanttStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure EzGridColExit(Sender: TObject);
    procedure GraphTypeChange(Sender: TObject);
    procedure ResourceGraphsGetHint(Sender: TEzGraph; Location: TPoint; Line,
        PointIndex: Integer; SelectionKind: TSelectionKind; var Hint: string);
    procedure SchedulerGetResourceAllocationProfile(Task: TEzScheduleTask;
        Assignment: TEzResourceAssignment; Resource: TEzResource; var Profile:
        TEzAvailabilityProfile);
    procedure SchedulerGetResourceScheduleProfile(Task: TEzScheduleTask; Interval:
        TEzScheduleInterval; Assignment: TEzResourceAssignment; Resource:
        TEzResource);

  private
    FResourceFilter: Integer;
    FAutoTaskScroll: Boolean;
    FAutoSchedule: Boolean;
    FIsSelectingConstraintDate: Boolean;
    FDesc: string;
    FPrevSelected: TListItem;
    FDeleting, ezDatasetScrolled: Boolean;
    FixupKeys: TList;
    UpdateCount: Integer;
    SavedGraphHeight: Integer;
    DatasetUpdateCount: Integer;
    FMouseTickCount: DWord;
    FResourceGraphProperties: TObjectList;
    FTaskInserted: Boolean;
    FInsertNode: TRecordNode;
    FInsertPosition: TEzInsertPosition;
    FProfilesAllocated: TEzObjectList;

    // Variables related to local (=scheduling a single task) schedule
    RunLocalScheduler: Boolean;
    FRescheduleTask: Boolean;
    FScheduleStartDate: TDatetime;
    FProjectProfile: TEzAvailabilityProfile;

    procedure LoadProjectResources;
    procedure LoadResourceOperators(Resource: TEzResource);
    procedure LoadResourceScheduleData(Resource: TEzResource);
    procedure CalculateRunningTotal(Resource: TEzResource;  ALine: TEzGraphLine);
    procedure CalculateResourceAllocation(Resource: TEzResource;
      ALine: TEzGraphLine);
    procedure CalculateResourceAllocationStretched(Resource: TEzResource;
      ALine: TEzGraphLine);
    procedure CalculateResourceLoad(Resource: TEzResource; ALine: TEzGraphLine);
    procedure LoadGraphData(Graph: TResourceGraphProperties);

  protected
    procedure AttachGraphs;
    procedure BeginUpdate;
    procedure EndUpdate;
    function  CalcResourceCount: Double;
    procedure ClearFixupKeys;
    procedure ClearScheduleMessages;
    procedure DetachGraphs;
    function  GetActive: Boolean;
    function  GetIdProject: Integer;
    function  GetDate(Index: Integer): TDateTime;
    function  GetGraphProperties(idResource: Integer) : TResourceGraphProperties;
    function  GetProjectProfile: TEzAvailabilityProfile;
    function  GetResourceProfile(ResourceKey: Integer): TEzAvailabilityProfile;

    function  get_IsSorted: Boolean;
    procedure SetDetailTableRange;
    procedure ShowGraphPropertiesPage;
    procedure SetActive(Value: Boolean);
    procedure SetIdProject(Value: Integer);
    procedure SetDescription(Value : string);
    procedure SetResourceFilter(Value: Integer);
    procedure SetDate(Index: Integer; Value: TDateTime);

    procedure ReorderBranch(Dataset: TEzDataset);
    procedure UpdateRecordOrderValue(Dataset: TDataSet; Node: TRecordNode);

    //
    // methods involved with scheduling a single task and all related tasks
    //
    function  GetStartDateChecked(Task: TEzScheduleTask): TDateTime;
    procedure LoadChildren(Node: TRecordNode; DoLoadPredecessors, DoLoadSuccessors: Boolean);
    function  LoadParent(Node: TRecordNode; DoLoadPredecessors, DoLoadSuccessors: Boolean): TEzScheduleTask;
    function  LoadTask(Node, ParentNode: TRecordNode; DoLoadParent, DoLoadChildren, DoLoadPredecessors, DoLoadSuccessors: Boolean): TEzScheduleTask;
    procedure LoadPredecessors(Task: TEzScheduleTask; ParentNode: TRecordNode);
    procedure LoadScheduleMessages;
    procedure LoadSuccessors(Task: TEzScheduleTask; ParentNode: TRecordNode);
    procedure LoadResourceCapacities(Resource: TEzResource);
    procedure RescheduleFromTask(TaskToSchedule: TRecordNode);
    procedure OverrideConstraint(Task: TEzScheduleTask);

{$IFDEF EZ_D6}
    procedure dspTaskProviderUpdateError(Sender: TObject;
      DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
    procedure dspTaskProviderAfterUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind);
{$ELSE}
    procedure dspTaskProviderUpdateError(Sender: TObject;
      DataSet: TClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
    procedure dspTaskProviderAfterUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TClientDataSet;
      UpdateKind: TUpdateKind);
{$ENDIF}

  public
    FIsOpening: Boolean;
    FIsUpdating: Boolean;

    procedure AddTask;
    procedure CancelTask;
    procedure CalcDateBounds(var LowerBound, UpperBound: TDateTime);

    procedure DeleteTask;
    procedure AddChildTask;
    procedure StartTask;
    procedure StopTask;
    procedure IndentTask;
    procedure OutdentTask;
    procedure UpdateDatabase;
    function  DataChanged: Boolean;
    procedure MaximizeGrid;
    procedure MinimizeGrid;
    procedure Reschedule;
    procedure ScrollToTask;
    procedure SelectCursor(Index: Integer);

    property  AutoSchedule: Boolean read FAutoSchedule write FAutoSchedule;
    property  AutoTaskScroll: Boolean read FAutoTaskScroll write FAutoTaskScroll;
    property  Active: Boolean read GetActive write SetActive;
    property  Description: string read FDesc write SetDescription;
    property  idProject: Integer read GetIdProject write SetIdProject;
    property  IsSorted: Boolean read get_IsSorted;

    property  CurrentDate: TDateTime index 0 read GetDate write SetDate;
    property  ProjectStart: TDateTime index 1 read GetDate write SetDate;
    property  ProjectStop: TDateTime index 2 read GetDate write SetDate;
    property  ResourceFilter: Integer read FResourceFilter write SetResourceFilter;
  end;

  TCustomAvailabilityProfile = class(TEzAvailabilityProfile)
  protected
    _SourceProfile: TEzAvailabilityProfile;
  public
    constructor Create(Source: TEzAvailabilityProfile); reintroduce;

    function  AllocateInterval(Start, Stop: TEzDateTime; Units: Double; AllowNegative: Boolean): Boolean; override;
  end;

implementation

{$R *.DFM}
uses Dialogs, SysUtils, Math, EzMasks, EzPainting, ProjectData, Messages,
 {$IFDEF EZ_D6} Variants, {$ENDIF} FindTask, StartPage,
  Capacity;

//=----------------------------------------------------------------------------=

procedure TfrmGanttChart.FormCreate(Sender: TObject);
var
  fld: TField;
begin
{$IFDEF DEBUG}
  tsDebugInfo.TabVisible := True;
{$ELSE}
  tsDebugInfo.TabVisible := False;
{$ENDIF}
  Height := 350;
  Width := 650;
  FIsUpdating := False;

  pcTaskDetail.ActivePage := tsTaskGeneral;

  // Assign events OnUpdateError and AfterUpdateRecord manually.
  // This is necesary because the implementations of these events
  // changed with newer versions of Delphi.
  dspTaskProvider.OnUpdateError := dspTaskProviderUpdateError;
  dspResources.OnUpdateError := dspTaskProviderUpdateError;
  dspPredecessors.OnUpdateError := dspTaskProviderUpdateError;
  dspTaskProvider.AfterUpdateRecord := dspTaskProviderAfterUpdateRecord;

  //
  // qrTasks is a query object used for accessing tasks belonging
  // to the project given by ProjectKey. Because this Dataset object
  // is wrapped by the TEzDataSet component we don't rely on any events
  // sent by qrTasks. We can therefore safely disable it.
  //
  FAutoTaskScroll := True;
  FAutoSchedule := False;
  ezDatasetScrolled := True;
  RunLocalScheduler := True;
  FIsSelectingConstraintDate := False;

  // Hide tabs that are not used
  shScheduleMessages.TabVisible := False;
  shReconcileErrors.TabVisible := False;

  // Hide resource graph by default.
  SavedGraphHeight := pnlGraph.Height;
  pnlGraph.Height := 0;

  // By default, 'Task detail data' page is active.
  pcBottomPane.ActivePage := shTaskFields;

  FDeleting := False;
  FixupKeys := TList.Create;

  // Setup the timebar
  //
  // We set scalestorage to point to the 'master' scale storage
  // located on the main form. This way, all windows having a
  // timscale use the same setup
  EzTimebar.ScaleStorage := MainForm.tssGlobalScales;
  EzTimebar.Date := Now;
  EzGantt.MinDate := Now;
  EzGantt.MaxDate := Now;

  FResourceGraphProperties := TObjectList.Create;

//  EzGantt.Overlapping.Mode := omUseTwinRows;
end;

procedure TfrmGanttChart.FormDestroy(Sender: TObject);
begin
  ClearFixupKeys;
  FixupKeys.Destroy;

  ClearScheduleMessages;

  // Release project calendar
  FProjectProfile.Free;

  // release any temporarily profiles
  FProfilesAllocated.Free;

  FResourceGraphProperties.Free;
end;

function TfrmGanttChart.DataChanged: Boolean;
begin
  Result := (cdsTasks.ChangeCount > 0) or
            (cdsTaskResources.ChangeCount > 0) or
            (cdsTaskPredecessors.ChangeCount > 0);
end;

function TfrmGanttChart.GetActive: Boolean;
begin
  Result := ezdsTaskTree.Active;
end;

function TfrmGanttChart.GetIdProject: Integer;
begin
  Result := qrTasks.Parameters.ParamValues['idProject'];
end;

function TfrmGanttChart.GetDate(Index: Integer) : TDateTime;
begin
  case Index of
    0: Result := EzTimebar.Date;
    1: Result := EzGantt.MinDate;
    2: Result := EzGantt.MaxDate;
    else
      Result := 0;
  end;
end;

procedure TfrmGanttChart.SetActive(Value: Boolean);
var
  i: Integer;

begin
  if Value then
  begin
    if idProject = 0 then
      raise Exception.Create('Project key must be set');

    qrProject.Parameters.ParamValues['idProject'] := idProject;
    qrTasks.Parameters.ParamValues['idProject'] := idProject;
    qrTaskResources.Parameters.ParamValues['idProject'] := idProject;
    qrTaskPredecessors.Parameters.ParamValues['idProject'] := idProject;

    qrProject.Open;
    if qrProject.FieldByName('prActiveScale').AsString<>'' then
    begin
      i:=0;
      while (i<EzTimebar.ScaleStorage.Scales.Count) and
            (EzTimebar.ScaleStorage.Scales[i].Name<>
              qrProject.FieldByName('prActiveScale').AsString) do
        inc(i);
      if (i<EzTimebar.ScaleStorage.Scales.Count) then
        EzTimebar.ScaleIndex := i;
    end;

    ezdsTaskTree.Open;
    cdsTaskResources.Open;
    cdsTaskPredecessors.Open;
  end
  else
  begin
    qrProject.Edit;
    qrProject['prActiveScale'] := EzTimebar.ScaleStorage.Scales[EzTimebar.ScaleIndex].Name;
    qrProject.Post;
    ezdsTaskTree.Close;
    cdsTaskResources.Close;
    cdsTaskPredecessors.Close;
  end;
end;

procedure TfrmGanttChart.SetIdProject(Value: Integer);
begin
  qrTasks.Parameters.ParamValues['idProject'] := Value;
end;

procedure TfrmGanttChart.SetDescription(Value : string);
begin
  FDesc := Value;
  Caption := 'Project: ''' + Value + '''';
end;

procedure TfrmGanttChart.SetResourceFilter(Value: Integer);
var
  Index: Integer;
begin
  // Default cursor cannot be filtered
  if ezdsTaskTree.ActiveCursorIndex = 0 then
    Value := -1;

  if FResourceFilter <> Value then
  begin
    FResourceFilter := Value;

    if FResourceFilter <> -1 then
    begin
      // if cursor was already filtered, then close and reopen cursor to reload data
      if cpFiltered in ezdsTaskTree.ActiveCursor.Options then
      begin
        Index := ezdsTaskTree.ActiveCursorIndex;
        ezdsTaskTree.ActiveCursorIndex := 0; // Close
        ezdsTaskTree.ActiveCursorIndex := Index; // Open
      end
      else
        ezdsTaskTree.ActiveCursor.Options := ezdsTaskTree.ActiveCursor.Options + [cpFiltered]
    end
    else
      ezdsTaskTree.ActiveCursor.Options := ezdsTaskTree.ActiveCursor.Options - [cpFiltered];
  end;
end;

procedure TfrmGanttChart.SelectCursor(Index: Integer);
var
  CurrentCursor: Integer;
begin
  with ezdsTaskTree do
    if Index <> ActiveCursorIndex then
    begin
      CurrentCursor := ActiveCursorIndex;
      ActiveCursorIndex := 0;

      // Remove any filtering from current cursor
      Cursors[CurrentCursor].Options := Cursors[CurrentCursor].Options - [cpFiltered];
      ActiveCursorIndex := Index;
    end;
end;

procedure TfrmGanttChart.SetDate(Index: Integer; Value: TDateTime);
begin
  case Index of
    0: EzTimebar.Date := Value;
    1:
    begin
      EzGantt.MinDate := Value;
      EzTimebar.MinDate := Value;
    end;
    2:
    begin
      EzGantt.MaxDate := Value;
      EzTimebar.MaxDate := Value;
    end;
  end;
end;

//
// This procedure scrolls the Gantt chart in such a way that the taskbar,
// drawn for the current record, will be visible. This method is called
// automatically when AutoTaskScroll is set to True.
//
procedure TfrmGanttChart.ScrollToTask;
var
  Start, Stop, EndDate, Current: TDateTime;
begin
  if not ezdsTaskTree.FieldByName('TASTART').IsNull then
    Start := ezdsTaskTree.FieldByName('TASTART').AsDateTime else
    Start := 0;

  if not ezdsTaskTree.FieldByName('TASTOP').IsNull then
    Stop := ezdsTaskTree.FieldByName('TASTOP').AsDateTime else
    Stop := 0;

  with EzGantt.Timebar do
  begin
    Current := Date;
    EndDate := X2DateTime(Width);

    if (Start <> 0) and (Start < Current) then
      Date := Start
    else if (Stop <> 0) and (Stop > EndDate) then
    begin
      if Start-Current < Stop-EndDate then
        Date := Start else
        Date := AddTicks(MajorScale, MajorCount, Date + Stop-EndDate, 1);
    end;
  end;
end;

procedure TfrmGanttChart.qrTasksNewRecord(DataSet: TDataSet);
begin
  // Make sure that all required fields have a value stored
  with DataSet as TEzDataset do
  begin
    FieldByName('taidProject').AsInteger := idProject;
    FieldByName('taidConstraint').AsInteger := Ord(cnASAP);
    FieldByName('taidPriority').AsInteger := NORMAL_PRIORITY;
    FieldByName('taidStatus').AsInteger := STATUS_NONE;
    FieldByName('taOrder').AsInteger := 0;
    FInsertPosition := InsertPosition;
    FInsertNode := InsertNode;
  end;
end;

procedure TfrmGanttChart.BeginUpdate;
begin
  inc(UpdateCount);
end;

procedure TfrmGanttChart.EndUpdate;
begin
  if UpdateCount > 0 then
    dec(UpdateCount);
end;

//
// This function calculates the total number of resources working
// on the task given by TaskKey. This value is required to calculate
// the duration a task has:
//
// Duration := working hours / total number of resources.
//
function TfrmGanttChart.CalcResourceCount : Double;
var
  idTask: Integer;

begin
  Result := 0;
  try
    ezdsTaskTree.SyncCursor;

    while not ezdsTaskTree.ActiveCursor.Bof do
    begin
      idTask := ezdsTaskTree.ActiveCursor.Key.Value;
      cdsTaskResourcesClone.SetRange([idTask], [idTask]);
      // KV: 13-12-2004
      // Sometimes Eof was true, even when there were one or more
      // resources assigned. Calling first fixes this problem.
      cdsTaskResourcesClone.First;
      while not cdsTaskResourcesClone.Eof do
      begin
        Result := Result + cdsTaskResourcesClone.FieldByName('trAssigned').AsFloat;
        cdsTaskResourcesClone.Next;
      end;

      //
      // Goto parent record
      //
      ezdsTaskTree.ActiveCursor.MoveParent;
    end;
  finally
    // Avoid divide by 0 errors and negative resource assignments
    if (Result <= 0) then Result := 1;
  end;
end;

procedure TfrmGanttChart.ClearFixupKeys;
var
  i: integer;

begin
  while FixupKeys.Count > 0 do
  begin
    i := FixupKeys.Count-1;
    Finalize(PFixedKey(FixupKeys[i])^);
    Dispose(FixupKeys[i]);
    FixupKeys.Delete(i);
  end;
end;

//
// Event handler for TEzDataset's formula parser.
// Whenever a variable is required that isn't recognized by the
// formula parser, then this event handler gets called in a attempt to
// retrieve the value for the variable given.
//
// In this demo, the field DURATION is calculated as :TAWORK/:RC
// where TAWORK is a field taken from the dataset holding the total
// working hours
// and :RC is the number of resources working on a certain task.
// Because the field 'RC' is not part of the dataset, we have to supply it's value
// using this event.
//
procedure TfrmGanttChart.ezdsTaskTreeGetVariableValue(Dataset: TDataSet;
  Variable: String; var Result: Variant);
begin
  if Variable = 'RC' then // RC = Resource count
    Result := CalcResourceCount;
end;

procedure TfrmGanttChart.MaximizeGrid;
begin
  pnlGanttChart.Width := 100;
end;

procedure TfrmGanttChart.MinimizeGrid;
begin
  pnlGanttChart.Width := ClientWidth - 100;
end;

procedure TfrmGanttChart.Reschedule;
var
  Task: TEzScheduleTask;
  Int: TEzScheduleInterval;

  procedure AddResources;
  var
    R: TEzResourceAssignment;

  begin
    cdsTaskResources.SetRange([Integer(Task.Key.Value)], [Integer(Task.Key.Value)]);
    cdsTaskResources.First;

    while not cdsTaskResources.Eof do
    begin
      R := TEzResourceAssignment.Create;
      R.Key := cdsTaskResources['tridResource'];
      R.Assigned := cdsTaskResources.FieldByNAme('trAssigned').AsFloat;

      // The duration of the task is influenced by the assignment of this
      // resource
      R.AffectsDuration := True;
      Task.Resources.Add(R);
      cdsTaskResources.Next;
    end;
  end;

  procedure AddPredecessors;
  var
    P: TEzPredecessor;

  begin
    cdsTaskPredecessors.SetRange([Integer(Task.Key.Value)], [Integer(Task.Key.Value)]);

    while not cdsTaskPredecessors.Eof do
    begin
      P := TEzPredecessor.Create;
      P.Key.Level := -1;
      P.Key.Value := cdsTaskPredecessors['tpidPredecessor'];
      P.Relation := TEzPredecessorRelation(cdsTaskPredecessors.FieldByName('tpidRelation').AsInteger);
      P.Lag := cdsTaskPredecessors.FieldByName('tpLag').AsFloat;
      Task.Predecessors.Add(P);
      cdsTaskPredecessors.Next;
    end;
  end;

  procedure LoadScheduleData;
  var
    Duration: Double;
    idCapacity: Integer;
    CapacityAssignment: TEzCapacityAssignment;

  begin
    ezdsTaskTree.ActiveCursor.MoveFirst;
    while not ezdsTaskTree.ActiveCursor.Eof do
      //
      // Load all tasks into the scheduler
      //
    begin
      Task := TEzScheduleTask.Create;
      Task.Key := ezdsTaskTree.ActiveCursor.CurrentNode.Key;
      if ezdsTaskTree.ActiveCursor.CurrentNode.Parent <> nil then
        Task.Parent := ezdsTaskTree.ActiveCursor.CurrentNode.Parent.Key else
        Task.Parent.Value := Null;

      Task.Constraint := TEzConstraint(ezdsTaskTree.FieldByName('ScheduleConstraint').AsInteger);
      Task.ConstraintDate := ezdsTaskTree.FieldByName('ScheduleConstraintDate').AsDateTime;
      Task.Priority := ezdsTaskTree.FieldByName('taidPriority').AsInteger;

      // Capacity assignment?
      if not ezdsTaskTree.FieldByName('taCapacity').IsNull then
      begin
        CapacityAssignment := TEzCapacityAssignment.Create;
        idCapacity := ezdsTaskTree.FieldByName('taCapacity').AsInteger;
        CapacityAssignment.Key := idCapacity;
        CapacityAssignment.Assigned := 1.0;
        CapacityAssignment.MergeProfiles :=
          dmProjectData.qrCapacities.Lookup('idCapacity', idCapacity, 'caMergeProfiles');

        // The duration of the task is influenced by the assignment of this
        // capacity
        CapacityAssignment.AffectsDuration := False;

        // Do not other resources!
        Task.Resources.Add(CapacityAssignment);
      end else
        AddResources;

      AddPredecessors;

      Duration := ezdsTaskTree.FieldByName('taWork').AsFloat;
      if Duration>0 then
        // Add a schedule interval
      begin
        Int := TEzScheduleInterval.Create;
        Int.Start := ezdsTaskTree.FieldByName('ScheduleStart').AsDateTime;
        Int.Stop := ezdsTaskTree.FieldByName('ScheduleStop').AsDateTime;
        Int.Duration := Duration;
        Task.Intervals.Add(Int);
      end;

      Scheduler.AddTask(Task);
      ezdsTaskTree.ActiveCursor.MoveNext;
    end;
  end;

  procedure SaveData;
  var
    i_workInterval, i_assignment: Integer;
    WorkInterval: TEzWorkInterval;
    Assignment: TEzResourceAssignment;
    hasChildren: Boolean;
    s: string;
    sl: TStringList;

  begin
    RunLocalScheduler := False;
    try
      // Run through all tasks
      ezdsTaskTree.ActiveCursor.MoveFirst;
      while not ezdsTaskTree.ActiveCursor.Eof do
      begin
        hasChildren := ezdsTaskTree.ActiveCursor.HasChildren;

        // Get schedule data from scheduler
        Task := Scheduler.FindTask(ezdsTaskTree.ActiveCursor.Key);

        if not hasChildren and (srIntervaldataChanged in Task.Flags) then
          //
          // Task was moved by the scheduler
          //
        begin
          ezdsTaskTree.Edit;
          ezdsTaskTree['taStart'] := Task.Intervals[0].Start;
          ezdsTaskTree['taStop'] := Task.Intervals[0].Stop;
          ezdsTaskTree.Post;
        end;

        // Clean up 'automatically' assigned resources
        cdsTaskResources.SetRange([Integer(Task.Key.Value)], [Integer(Task.Key.Value)]);
        cdsTaskResources.First;
        while not cdsTaskResources.Eof do
        begin
          if cdsTaskResources['trType'] = 2 then
            cdsTaskResources.Delete else
            cdsTaskResources.Next;
        end;

        if Task.WorkIntervals <> nil then
        begin
          sl := TStringList.Create;

          // Save actual resources assigned to this task
          for i_workInterval := 0 to Task.WorkIntervals.Count - 1 do
          begin
            WorkInterval := Task.WorkIntervals[i_workInterval];

            // WorkInterval.ResourceAssigments holds the list of resources
            // assigned during this interval.
            //
            // If a task uses merged profiles then every interval
            // might have a different set of resources selected.
            //
            // If a task does not use merged profiles, each interval
            // uses the same resources and this list will be stored
            // in Task.WorkIntervals[0].ResourceAssigments.
            //
            if WorkInterval.ResourceAssigments <> nil then
            begin
              for i_assignment := 0 to WorkInterval.ResourceAssigments.Count - 1 do
              begin
                Assignment := WorkInterval.ResourceAssigments[i_assignment];

{$IFDEF DEBUG}
                s := string(Assignment.Key) + ' - ' + DateTimeToStr(EzDateTimeToDateTime(WorkInterval.TimeSpan.Start)) + ' > ' +
                                                      DateTimeToStr(EzDateTimeToDateTime(WorkInterval.TimeSpan.Stop));
                sl.Add(s);
{$ENDIF}

                if not cdsTaskResources.Locate('tridResource', Assignment.Key, []) then
                begin
                  cdsTaskResources.Insert;
                  cdsTaskResources['tridTask'] := Task.Key.Value;
                  cdsTaskResources['tridResource'] := Assignment.Key;
                  cdsTaskResources.FieldByNAme('trAssigned').AsFloat := Assignment.Assigned;
                  cdsTaskResources['trType'] := 2; // automatic
                  cdsTaskResources.Post;
                end;
              end;
            end;
          end;
{$IFDEF DEBUG}
          if sl.Text <> '' then
            ShowMessage('Debug info: ' + sl.Text);
{$ENDIF}
        end;

        ezdsTaskTree.ActiveCursor.MoveNext;
      end;
    finally
      RunLocalScheduler := True;
    end;
  end;

begin
  ezdsTaskTree.StartDirectAccess([cpShowAll], True);
  try
    cdsTaskPredecessors.DisableControls;
    cdsTaskResources.DisableControls;

    if ezdsTaskTree.State in [dsEdit, dsInsert] then
      ezdsTaskTree.Post;
    if cdsTaskPredecessors.State in [dsEdit, dsInsert] then
      cdsTaskPredecessors.Post;
    if cdsTaskResources.State in [dsEdit, dsInsert] then
      cdsTaskResources.Post;

    try
      // Because Scheduler.Reset will destroy the resource
      // availability graphs, we have to detach any graph first.
      DetachGraphs;

      // Prepare scheduler for next run
      Scheduler.Reset;

      // Scheduler.Reset releases all schedule profiles from previous run,
      // we can now release our profiles too
      if FProfilesAllocated = nil then
        FProfilesAllocated := TEzObjectList.Create(True {Owns objects}) else
        FProfilesAllocated.Clear;

      // Let the scheduler return detailed data about the
      // scheduled intervals of each task.
      // This data is returned in Task.WorkIntervals list.
      Scheduler.ReturnWorkIntervals := True;

      // Load all resources used in this project into the scheduler.
      LoadProjectResources;

      ClearScheduleMessages;

      // ProjectWindow holds the project schedule window.
      // That is the start and stop dates in which the project must be completed.
      Scheduler.ProjectWindowStart := ProjectStart;
      Scheduler.ProjectWindowStop := ProjectStop;
      Scheduler.ProjectWindowAbsoluteStart := ProjectStart-365;
      Scheduler.ProjectWindowAbsoluteStop := ProjectStop+365;
      Scheduler.SchedulebeyondScheduleWindow := True;

      // Set project calendar,
      // Create calendar with 24 hours availability
      Scheduler.ProjectProfile := GetProjectProfile;


      // Import data in scheduler component
      LoadScheduleData;

      //
      // Reschedule project
      //
      Scheduler.Reschedule;
      if srScheduledOK in Scheduler.ScheduleResults then
        SaveData;

      LoadScheduleMessages;

      // Reconnect graph lines with availability profiles calculated by the
      // scheduler.
      AttachGraphs;

      // Once the scheduler has run, graphs can be displayed. Therefore
      // enable the graph properties page.
      ShowGraphPropertiesPage;

    finally
      cdsTaskPredecessors.EnableControls;
      cdsTaskResources.EnableControls;
    end;
  finally
    ezdsTaskTree.EndDirectAccess;
  end;
end;

procedure TfrmGanttChart.EzGanttLinkTasks(Sender: TObject; Predecessor,
  Successor: TNodeKey; Relation: TEzPredecessorRelation);
begin
  with cdsTaskPredecessors do
  try
    Insert;
    FieldByName('tpidTask').AsInteger := Successor.Value;
    FieldByName('tpidPredecessor').AsInteger := Predecessor.Value;
    FieldByName('tpidRelation').AsInteger := Ord(Relation);
    Post;

    // EzGantt leaves the predecessor task selected,
    // Here we update the dataset cursor to select the successor instead.
    ezdsTaskTree.NodeKey := Successor;
    EzGantt.SelectedGanttBars.Clear;
    // Active predecessorline page
    pcTaskDetail.ActivePage := tsTaskPredecessors;
  except
    Cancel;
    raise;
  end;
end;

// Update contents of Cursor menu with currently available cursors.
// Cursors are defined in the TEzDataset component.
procedure TfrmGanttChart.FormActivate(Sender: TObject);
var
  X: Integer;
  MenuItem: TMenuItem;
begin
  // Update contents of sort menu with Cursors defined in the TEzDataset
  with MainForm.ppSortMenu do
  begin
    Items.Clear;

    for X:=0 to ezdsTaskTree.Cursors.Count-1 do
    begin
      MenuItem := TMenuItem.Create(MainForm.ppSortMenu);
      with MenuItem do
      begin
        if X = 0 then
          Caption := SNone else
          Caption := ezdsTaskTree.Cursors[X].CursorName;
        GroupIndex := 1;
        RadioItem := True;
        OnClick := MainForm.SortItemClick;

        if X = ezdsTaskTree.ActiveCursorIndex then
          Checked := True;
      end;
      Items.Add(MenuItem);
    end;
  end;

  MainForm.UpdateMarkerMenu(EzGantt);
end;

procedure TfrmGanttChart.SmallintField2SetText(Sender: TField;
  const Text: String);
begin
  Sender.AsInteger := EzGrid.Columns[EzGrid.SelectedIndex].Picklist.IndexOf(Text);
end;

procedure TfrmGanttChart.EzGanttGetPredecessor(Sender: TObject;
  var Predecessor: TPredecessor);

  //
  // Event fired by TEzGanttChart every time a new predecessor line
  // is requested. This event should return the data for the next
  // predecessor if there is one.
  // Otherwise SuccessorNode.Value must be set to Null
  //

begin
  with cdsTaskPredecessorsClone, Predecessor do
  begin
    if not Eof then
    begin
      SuccessorNode.Level := -1;
      SuccessorNode.Value := FieldValues['tpidTask'];
      PredecessorNode.Level := -1;
      PredecessorNode.Value := FieldValues['tpidPredecessor'];
      Relation := TEzPredecessorRelation(FieldByName('tpidRelation').AsInteger);
      Next;
    end
    else
      // No more predecessors, i.e. Stop condition
      SuccessorNode.Value := Null;
  end;
end;

procedure TfrmGanttChart.EzGanttDrawBegin(
  Sender: TObject;
  var bEraseBackground: Boolean);

var
  drwInfo: TGanttDrawInfo;
  R: TRect;
  Gantt: TEzGanttChart;
  Profile: TEzAvailabilityProfile;
  Point: TEzAvailabilityPoint;
  i_point: Integer;
  EndDate: TEzDateTime;
  timeBar: TEzTimeBar;

begin
  // This will also work when this method is called during printing
  Gantt := Sender as TEzGanttChart;
  Profile := GetProjectProfile;

  drwInfo := Gantt.DrawInfo;
  timeBar := Gantt.Timebar;

    //
    // Fill background using global calendar
    //

  // prevent GanttChart from erasing the background.
  bEraseBackground := false;

  // Erase background ourselves
  Gantt.Canvas.Brush.Style := bsSolid;

  // Erase area beneeth data rows
  Gantt.Canvas.Brush.Color := Gantt.RowColor;
  IntersectRect(R, drwInfo.UpdateRect, drwInfo.RowsRect);
  Gantt.Canvas.FillRect(R);

  // Erase unused area beneeth last data rows
  Gantt.Canvas.Brush.Color := Gantt.Color;
  IntersectRect(R, drwInfo.UpdateRect, drwInfo.ExcludedRect);
  Gantt.Canvas.FillRect(R);

  if Gantt.Timebar.MajorScale in [msDay, msHour, msMinute] then
  begin
    Gantt.Canvas.Brush.Color := $00E5E5E5; // a lite shade of gray

    // Make sure availability profile is prepared over the
    // requested interval
    Profile.PrepareDateRange(MakeTimespan(drwInfo.LeftDateExtend, drwInfo.RightDateExtend));

    // Locate datapoint in graph near LeftDateExtend
    if not Profile.IndexOf(i_point, drwInfo.LeftDateExtend) then
      dec(i_point);

    Point := Profile.Items[i_point];
    IntersectRect(R, drwInfo.UpdateRect, drwInfo.RowsRect);
    EndDate := 0;
    while EzDateTimeToDateTime(EndDate) < drwInfo.RightDateExtend do
    begin
      if i_point < Profile.Count-1 then
        EndDate := Profile.Items[i_point+1].DateValue else
        EndDate := END_OF_TIME;

      R.Right := timeBar.DateTime2X(min(drwInfo.RightDateExtend, EzDateTimeToDateTime(EndDate)));
      // hightlight non-working periods only
      if Point.Units = 0.0 then
        Gantt.Canvas.FillRect(R);
      R.Left := R.Right;

      if i_point < Profile.Count-1 then
      begin
        inc(i_point);
        Point := Profile.Items[i_point];
      end;
    end;
  end;

  // In case predecessor lines are drawn, then prepare predecessor query.
  if Gantt.PredecessorLine.Visible and cdsTaskPredecessorsClone.Active then
  begin
    cdsTaskPredecessorsClone.IndexName := 'ixidTask';
    cdsTaskPredecessorsClone.CancelRange;
    cdsTaskPredecessorsClone.First;
  end;
end;

procedure TfrmGanttChart.EzGanttDrawEnd(Sender: TObject);
begin
end;

{
  Event handler used for filtering recordset on resources.
  This event is fired from TEzDataset when a new cursor is activated
  and records are loaded into this cursor.

  When a resource filter is active, we test if this resource is assigned
  to the current record and set the value Accept accordingly.

  Because this event is called when a cursor is being loaded, we cannot
  access the dataset directly (using FieldByName('')) but the current
  record key can be retrieved from the cursor directly.
}
procedure TfrmGanttChart.ezdsTaskTreeFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var
  idTask: Integer;

begin
  if FResourceFilter <> -1 then
    with ezdsTaskTree do
    begin
      Accept := False;

      idTask := ActiveCursor.Key.Value;
      cdsTaskResourcesClone.SetRange([idTask], [idTask]);
      // KV: 13-12-2004
      // Sometimes Eof was true, even when there are one or more
      // resources assigned. Calling first fixes this problem.
      cdsTaskResourcesClone.First;

      while not Accept and not cdsTaskResourcesClone.Eof do
        if cdsTaskResourcesClone['tridResource'] = FResourceFilter then
          Accept := True else
          cdsTaskResourcesClone.Next;
    end;
end;

procedure TfrmGanttChart.SetDetailTableRange;
var
  idTask: Integer;

begin
  if not ezdsTaskTree.Active then Exit;

  if ezdsTaskTree.Eof or (ezdsTaskTree.State = dsInsert) then
    idTask := 0 else
    idTask := ezdsTaskTree.FieldByName('idTask').AsInteger;

  if (ActiveControl<>grTaskResources) then
  begin
    if (cdsTaskResources.State in [dsEdit, dsInsert]) then
      cdsTaskResources.Post;
    cdsTaskResources.SetRange([idTask], [idTask]);
  end;

  if (ActiveControl<>grTaskPredecessors) and grTaskPredecessors.Showing then
  begin
    if (cdsTaskPredecessors.State in [dsEdit, dsInsert]) then
    try
      cdsTaskPredecessors.Post;
    except
      ActiveControl:=grTaskPredecessors;
      raise;
    end;
    cdsTaskPredecessors.SetRange([idTask], [idTask]);

    // If a predecessorline is selected,
    // the try to move cursor of cdsTaskPredecessors to selected predecessor.
    if EzGantt.SelectedPredecessorLine<>nil then
      // Can't use FindKey here because of wrong index
      cdsTaskPredecessors.Locate('tpidPredecessor', EzGantt.SelectedPredecessorLine.Predecessor.PredecessorNode.Value, []);
  end;
end;

{
  When the graph is made visible, then the special property page is shown.

  This procedure loads the resources used in the current project and adds them
  to the listview (lvGraphResources) object placed on the propertypage.
  We use the general purpose query object to retrieve these names.
}
procedure TfrmGanttChart.ShowGraphPropertiesPage;
var
  Item: TListItem;
  idResource: Integer;

begin
  shGraphProps.Enabled := True;

  // No ListItem selected yet
  FPrevSelected := nil;

  // Load resource names into ListView
  cdsTaskResourcesClone.IndexName := 'ixResourceName';
  lvGraphResources.Items.Clear;
  with cdsTaskResourcesClone, lvGraphResources do
  try
    First;
    while not Eof do
    begin
      idResource := FieldByName('tridResource').AsInteger;
      Item := Items.Add;
      Item.Caption := FieldByName('lkResourceName').AsString;
      Item.Data := Ptr(idResource);

      if GetGraphProperties(idResource).Line.Visible then
        Item.ImageIndex := 0 else
        Item.ImageIndex := -1;

      Next;
      while not Eof and (FieldByName('tridResource').AsInteger = idResource) do
        Next;
    end;
  finally
    IndexName := 'ixidTask';
  end;
end;

function TfrmGanttChart.GetGraphProperties(idResource: Integer) : TResourceGraphProperties;
var
  GraphLine: TEzGraphLine;
  i: Integer;
  props: TResourceGraphProperties;

begin
  for i := 0 to FResourceGraphProperties.Count - 1 do
  begin
    props := TResourceGraphProperties(FResourceGraphProperties[i]);
    if props.ResourceID = idResource then
    begin
      Result := props;
      Exit;
    end;
  end;

  props := TResourceGraphProperties.Create;
  props.ResourceID := idResource;
  props.Filled := chkFilled.Checked;

  // Select next color
  cbLineColor.ItemIndex := cbLineColor.ItemIndex+1;

  props.Color := cbLineColor.SelectedColor;
  props.GraphType := GraphType.ItemIndex;

  FResourceGraphProperties.Add(props);

  GraphLine := ResourceGraphs.Lines.Add;

  if props.Filled then
  begin
    GraphLine.Pen.Style := psClear;
    GraphLine.Brush.Style := bsSolid;
    GraphLine.Brush.Color := props.Color;
  end
  else
  begin
    GraphLine.Pen.Style := psSolid;
    GraphLine.Pen.Color := props.Color;
    GraphLine.Brush.Style := bsClear;
  end;

  props.Line := GraphLine;

  Result := props;
end;

procedure TfrmGanttChart.CalculateRunningTotal(Resource: TEzResource; ALine: TEzGraphLine);
var
  A1: Double;
  A2: Double;
  ADate: TDateTime;
  Allocation: Double;
  NextDate: TDateTime;
begin
  ALine.Profile.Clear;
  ALine.Profile.ClearSources;

  ADate := TruncDate( EzTimeBar.MajorScale,
                      EzTimeBar.MajorCount,
                      EzTimeBar.MajorOffset,
                      ProjectStart,
                      EzTimeBar.MajorWeekStart);

  Allocation := 0;
  while ADate < ProjectStop do
  begin
    NextDate := AddTicks(EzTimeBar.MajorScale,
                      EzTimeBar.MajorCount,
                      ADate,
                      1);

    A1 := 24 * Resource.Availability.CalcWorkingHours(ADate, NextDate);
    A2 := 24 * Resource.ActualAvailability.CalcWorkingHours(ADate, NextDate);
    Allocation := Allocation + (A1 - A2);
//    Allocation := Resource.Availability.CalcWorkingHours(ADate, NextDate) -
//                  Resource.ActualAvailability.CalcWorkingHours(ADate, NextDate);

    ALine.Profile.SetInterval(ADate, NextDate, Allocation, []);
    ADate := NextDate;
  end;

  // Reset to 0 after project stop
  ALine.Profile.Add(ProjectStop, 0);
end;

procedure TfrmGanttChart.CalculateResourceLoad(Resource: TEzResource; ALine: TEzGraphLine);
var
  A1: Double;
  A2: Double;
  ADate: TDateTime;
  Allocation: Double;
  NextDate: TDateTime;
begin
  ALine.Profile.Clear;
  ALine.Profile.ClearSources;

  ADate := TruncDate( EzTimeBar.MajorScale,
                      EzTimeBar.MajorCount,
                      EzTimeBar.MajorOffset,
                      ProjectStart,
                      EzTimeBar.MajorWeekStart);

  while ADate < ProjectStop do
  begin
    NextDate := AddTicks(EzTimeBar.MajorScale,
                      EzTimeBar.MajorCount,
                      ADate,
                      1);

    A1 := 24 * Resource.Availability.CalcWorkingHours(ADate, NextDate);
    if A1 <> 0 then
    begin
      A2 := 24 * Resource.ActualAvailability.CalcWorkingHours(ADate, NextDate);
      Allocation := (A1 - A2) * 100 / A1;

      ALine.Profile.SetInterval(ADate, NextDate, Allocation, []);
    end;

    ADate := NextDate;
  end;
end;

procedure TfrmGanttChart.CalculateResourceAllocation(Resource: TEzResource; ALine: TEzGraphLine);
begin
  ALine.Profile.Clear;
  ALine.Profile.ClearSources;

  //
  // The resource allocation graph can be calculated by substracting the
  // actual resource availabiliy (this is the availability of a
  // resource after scheduling) from the original resource availability
  // (this is the availability before scheduling). Both profiles
  // are stored in the resource collection of the scheduler.
  // Be aware that the original resource availability is actualy the
  // same profile as returned by OnLoadResourceData.
  //
  ALine.Profile.Operator := opSubstract;
  ALine.Profile.AddSource(Resource.Availability, False);
  ALine.Profile.AddSource(Resource.ActualAvailability, False);
end;

procedure TfrmGanttChart.CalculateResourceAllocationStretched(Resource: TEzResource; ALine: TEzGraphLine);
var
  idTask: Integer;
  startDate: TDateTime;
  stopDate: TDateTime;
  IsUsed: Boolean;
  minDate: TDateTime;
  maxDate: TDateTime;

begin
  ALine.Profile.Clear;
  ALine.Profile.ClearSources;

  minDate := END_OF_TIME;
  maxDate := 0;

  // Run through all tasks
  // Check whether this resource is assigned to this task.
  // Update graph when it is....
  ezdsTaskTree.StartDirectAccess([cpShowAll], True);
  try
    ezdsTaskTree.ActiveCursor.MoveFirst;
    while not ezdsTaskTree.ActiveCursor.Eof do
    begin
      idTask := ezdsTaskTree.FieldByName('idTask').AsInteger;
      cdsTaskResourcesClone.SetRange([idTask], [idTask]);

      cdsTaskResourcesClone.First;
      IsUsed := False;
      while not cdsTaskResourcesClone.Eof do
      begin
        IsUsed := cdsTaskResourcesClone.FieldByName('tridResource').AsInteger = Resource.Key;
        if IsUsed then
          break;
        cdsTaskResourcesClone.Next;
      end;

      if IsUsed then
      begin
        startDate := Trunc(ezdsTaskTree.FieldByName('taStart').AsDatetime);
        stopDate := ezdsTaskTree.FieldByName('taStop').AsDatetime;
        if Frac(stopDate) > 0 then
          stopDate := Trunc(stopDate) + 1;
        ALine.Profile.SumInterval(  startDate,
                                    stopDate,
                                    1, // Show # of resources working.
                                    False);

        minDate := min(minDate, startDate);
        maxDate := max(maxDate, stopDate);
      end;

      ezdsTaskTree.ActiveCursor.MoveNext;
    end;

    // Remove all days from graph where availability = 0
    if maxDate <> 0 then
    begin
      startDate := trunc(minDate);
      while startDate < maxDate do
      begin
        if Resource.Availability.CalcWorkingHours(startDate, startDate + 1) = 0 then
          ALine.Profile.SetInterval(startDate, startDate + 1, 0, []);
        startDate := startDate + 1;
      end;
    end;
  finally
    ezdsTaskTree.EndDirectAccess;
  end;
end;


function  TfrmGanttChart.GetProjectProfile: TEzAvailabilityProfile;
var
  Rules: TEzCalendarRules;

begin
  if FProjectProfile=nil then
  begin
    //
    // Load calendar rules for this project
    //
    Rules := TEzCalendarRules.Create;
    LoadResourceRules(Rules, dmProjectData.cnnProject, 'PROJECT', idProject);

    //
    // Load availability profile for this resource
    //
    FProjectProfile := TEzAvailabilityProfile.Create(1);

    //
    // Assign calendar rules and availability profile to the destination profile.
    // The destination profile will merge the rules with the base availability
    // profile into one availability profile.
    //
    FProjectProfile.Rules := Rules;

    // Rules must be freed automatically
    FProjectProfile.OwnsRules := True;
  end;
  Result := FProjectProfile;
end;

function TfrmGanttChart.get_IsSorted: Boolean;
begin
  Result := ezdsTaskTree.ActiveCursorIndex <> 0;
end;

procedure TfrmGanttChart.acShowGraphUpdate(Sender: TObject);
var
  props: TResourceGraphProperties;
begin
  if lvGraphResources.Selected <> nil then
  begin
    acShowGraph.Enabled := True;
    acShowGraph.Checked := lvGraphResources.Selected.ImageIndex = 0;

    if acShowGraph.Checked then
    begin
      pnlGraphProperties.Enabled := True;
      props := GetGraphProperties(Integer(lvGraphResources.Selected.Data));
      cbLineColor.SelectedColor := props.Color;
      chkFilled.Checked := props.Filled;
      if not GraphType.Focused then
        GraphType.ItemIndex := props.GraphType;
    end else
      pnlGraphProperties.Enabled := False;
  end
  else
  begin
    acShowGraph.Enabled := False;
    pnlGraphProperties.Enabled := False;
  end;
end;

procedure TfrmGanttChart.LoadGraphData(Graph: TResourceGraphProperties);
var
  Resource: TEzResource;
begin
  // Get scheduled resource
  Resource := Scheduler.Resources.ResourceByKey(Graph.ResourceID);

  case GraphType.ItemIndex of
    0:
      CalculateResourceAllocation(Resource, Graph.Line);
    1:
      CalculateResourceAllocationStretched(Resource, Graph.Line);
    2:
      CalculateResourceLoad(Resource, Graph.Line);
    3:
      CalculateRunningTotal(Resource, Graph.Line);
  end;
end;

procedure TfrmGanttChart.acShowGraphExecute(Sender: TObject);
var
  idResource, i: Integer;
  props: TResourceGraphProperties;

begin
  idResource := Integer(lvGraphResources.Selected.Data);

  if lvGraphResources.Selected.ImageIndex = -1 {ImageIndex indicates visability} then
    //
    // display line
    //
  begin
    cbLineColor.Enabled := True;

    props := GetGraphProperties(idResource);

    if props.Line.Visible then
      Exit; // Line is already showing

    LoadGraphData(props);

    // Assign resources same group id so that when the graph is stacked
    // they are summed in the same group.
    props.Line.Group := 0;
    props.Line.Index := ResourceGraphs.Lines.Count-1; // Display line on top
    props.Line.Visible := True;  // Make line visible

    lvGraphResources.Selected.ImageIndex := 0;

    if pnlGraph.Height=0 then
    begin
      pnlGraph.Height := SavedGraphHeight;
      ResourceGraphs.Xax.Position := ResourceGraphs.Height-10;
    end;
  end
  else
  begin
    cbLineColor.Enabled := False;
    props := GetGraphProperties(idResource);
    props.Line.Visible := false;
    lvGraphResources.Selected.ImageIndex := -1;

    i:=0;
    while (i<ResourceGraphs.Lines.Count) and not ResourceGraphs.Lines[i].Visible do
      inc(i);

    if i=ResourceGraphs.Lines.Count then
      //
      // No lines visible, hide graph.
      //
    begin
      SavedGraphHeight := pnlGraph.Height;
      pnlGraph.Height := 0;
    end;
  end;
end;

procedure TfrmGanttChart.DetachGraphs;

  //
  // Disconnects any availability graph from it's sources.
  //

var
  i: integer;

begin
  for i:=0 to ResourceGraphs.Lines.Count-1 do
    ResourceGraphs.Lines[i].Profile.ClearSources;
end;

procedure TfrmGanttChart.AttachGraphs;

  //
  // Reconnects availability graph with it's sources.
  //

var
  i: integer;
  props: TResourceGraphProperties;

begin
  for i:=0 to FResourceGraphProperties.Count - 1 do
  begin
    props := TResourceGraphProperties(FResourceGraphProperties[i]);
    if props.Line.Visible then
      LoadGraphData(props);
  end;
end;

procedure TfrmGanttChart.lvGraphResourcesChange(Sender: TObject; Item: TListItem; Change: TItemChange);
  //
  // OnClick event of resource listview
  //
  // This handler moves selected resource graph on top any others
  //
begin
  with lvGraphResources do
    if Assigned(Selected) and (Selected.ImageIndex = 0) then
      GetGraphProperties(Integer(Selected.Data)).Line.Index := ResourceGraphs.Lines.Count-1;
end;

procedure TfrmGanttChart.cbLineColorChange(Sender: TObject);
  //
  //   Update the color of selected resource allocation graph.
  //
var
  props: TResourceGraphProperties;

begin
  props := GetGraphProperties(Integer(lvGraphResources.Selected.Data));
  props.Color := cbLineColor.SelectedColor;
  props.Line.Brush.Color := cbLineColor.SelectedColor;
end;

{
  This event is called whenever the user starts dragging within the Gantt chart.

  Sender:
    Refreence to the Sender of the event

  BarInfo:
    Pointer to a TScreenBar object holding all the information about the
    bar being dragged. If the user starts dragging in an empty area off
    the gantt chart and if gaoCanCreateBars is set in GanttChart.Options
    then a new bar will be added to the gantt and BarInfo is filled with
    default data.

  DragType:
    Type of dragging;
            bdBar:
              A complete bar is selected by the user and as a result
              the start and stop time of the bar are updated.
            bdBarDate:
              The right side of a bar is selected and as a result
              the stop time of the bar is updated.
            bdProgressBar:
              The right side of a progressbar is selected and
              as a result the progress of the current task is updated.
            bdAddProgressBar:
              A progressbar is being added to a bar not having a
              progressbar yet.
            bdMilestone:
              A screenmarker is selected and as a result the date field
              associated with this marker is updated.
            bdNewbar:
              The user started dragging in an empty area and as a result a new
              bar will be added.

  HiddenRecord:
    Boolean value indicating whether dragging occurs on a row displaying bars
    of hidden records. This occurs when a parent record is collapsed and
    both gaoShowHiddens and gaoAllowHiddenEdit are set in GanttChart.Options.
    In this situation the Gantt chart does not display a summary bar but
    instead displays all child bars of the records not visible on screen.

  CanDrag:
    Boolean value indicating whether dragging must continue. Set this value to
    false to cancel the drag operation.
}
procedure TfrmGanttChart.EzGanttStartbarDrag(Sender: TObject; X,Y: Integer;
  BarInfo: TScreenbar; DragType: TGanttBarDragType;
  var CanDrag: Boolean);
var
  Node: TRecordNode;

begin
  if (DragType = bdNewBar) then
  begin
    if ezdsTaskTree.IsEmpty then
    begin
      ezdsTaskTree.Insert;
      Exit;
    end;

    Node := EzGantt.RowNode(BarInfo.Row);
    if (Node<>nil) and (Node.Child<>nil) then
      //
      // Don't allow a ganttbar to be added on parent nodes
      //
      CanDrag := False

    else if not ezdsTaskTree.FieldByName('taStart').IsNull and not ezdsTaskTree.FieldByName('taStop').IsNull then
      //
      // Task already has a gantt bar, therefore a new bar cannot be added
      //
    begin
      if (ezdsTaskTree.State<>dsBrowse) then
        ezdsTaskTree.Post;

      // Insert a new task
      ezdsTaskTree.Insert;
    end;
  end;
end;

procedure TfrmGanttChart.ezdsTaskTreeBeforeScroll(DataSet: TDataSet);
begin
  ezDatasetScrolled := True;
end;

procedure TfrmGanttChart.ezdsTaskTreeRelocateDataLink(
  Datalink: TEzDataLink; const Key: TNodeKey);
begin
  cdsTasks.FindKey([integer(Key.Value)]);
end;

procedure TfrmGanttChart.cdsTaskResourcesBeforeInsert(DataSet: TDataSet);
begin
  if ezdsTaskTree.State = dsInsert then ezdsTaskTree.Post;
end;

procedure TfrmGanttChart.cdsTaskResourcesNewRecord(DataSet: TDataSet);
begin
  cdsTaskResources['trType'] := 1; // fixed
  
  if not VarIsNull(ezdsTaskTree['idTask']) then
  begin
    cdsTaskResources['tridTask'] := ezdsTaskTree['idTask'];
    cdsTaskResources['trAssigned'] := 1;
  end else
    raise Exception.Create(SResourceNoTask);
end;

procedure TfrmGanttChart.cdsTaskPredecessorsBeforeInsert(DataSet: TDataSet);
begin
  if ezdsTaskTree.State = dsInsert then ezdsTaskTree.Post;
end;

procedure TfrmGanttChart.cdsTaskPredecessorsNewRecord(DataSet: TDataSet);
begin
  if not VarIsNull(ezdsTaskTree['idTask']) then
  begin
    cdsTaskPredecessors['tpidTask'] := ezdsTaskTree['idTask'];
    cdsTaskPredecessors['tpidRelation'] := prFinishStart;
  end else
    raise Exception.Create(SPredecessorNoTask);
end;

procedure TfrmGanttChart.UpdateDatabase;
var
  pf: PFixedKey;
  i: integer;

  procedure UpdateDetailDatasets;
  begin
    //
    // Task dataset is a detail table for itself in a self referencing scenario
    //
    with cdsTasks do
    begin
      SetRange([integer(pf^.OldKey)], [integer(pf^.OldKey)]);
      while not Eof do
      begin
        Edit;
        FieldByName('taidParent').AsInteger := pf^.NewKey;
        Post;
        // Do not call Next since updating the Key value will remove the
        // record out of view
      end;
    end;

    with cdsTaskResources do
    begin
      SetRange([integer(pf^.OldKey)], [integer(pf^.OldKey)]);
      while not Eof do
      begin
        Edit;
        FieldByName('tridTask').AsInteger := pf^.NewKey;
        Post;
        // Do not call Next since updating the Key value will remove the
        // record out of view
      end;
    end;

    with cdsTaskPredecessors do
    begin
      //
      // Update task id
      //
      SetRange([integer(pf^.OldKey)], [integer(pf^.OldKey)]);
      while not Eof do
      begin
        Edit;
        FieldByName('tpidTask').AsInteger := pf^.NewKey;
        Post;
        // Do not call Next since updating the Key value will remove the
        // record out of view
      end;

      //
      // Update predecessor id
      //
      IndexName := 'ixidPredecessor';
      try
        SetRange([integer(pf^.OldKey)], [integer(pf^.OldKey)]);
        while not Eof do
        begin
          Edit;
          FieldByName('tpidPredecessor').AsInteger := pf^.NewKey;
          Post;
          // Do not call Next since updating the Key value will remove the
          // record out of view
        end;
      finally
        IndexName := 'ixidTask';
      end;
    end;
  end;

begin
  if not DataChanged then Exit;

  FIsUpdating := True;
  try
    lvReconcileErrors.Items.Clear;

    if cdsTasks.ApplyUpdates(-1) = 0 then
    begin
      // Refresh dataset to fetch updated key values
      cdsTasks.Refresh;

      if FixupKeys.Count > 0 then
      begin
        cdsTasks.IndexName := 'ixidParent';
        try
          for i:=0 to FixupKeys.Count-1 do
          begin
            pf := FixupKeys[i];
            if pf^.OldKey <> pf^.NewKey then
              UpdateDetailDatasets;
          end;
        finally
          cdsTasks.IndexName := 'ixidTask';
        end;

        ClearFixupKeys;

        // Also apply new updates
        if cdsTasks.ChangeCount > 0 then
          cdsTasks.ApplyUpdates(-1);
      end;

      if cdsTaskResources.ChangeCount > 0 then
        cdsTaskResources.ApplyUpdates(-1);
      if cdsTaskPredecessors.ChangeCount > 0 then
        cdsTaskPredecessors.ApplyUpdates(-1);

      ezdsTaskTree.Close;
      ezdsTaskTree.Open;
    end;

    if lvReconcileErrors.Items.Count > 0 then
    begin
      shReconcileErrors.TabVisible := True;
      pcBottomPane.ActivePageIndex := shReconcileErrors.PageIndex;
    end else
      shReconcileErrors.TabVisible := False;
  finally
    FIsUpdating := False;
  end;
end;

{$IFDEF EZ_D6}
procedure TfrmGanttChart.dspTaskProviderUpdateError(Sender: TObject;
  DataSet: TCustomClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
  var Response: TResolverResponse);
{$ELSE}
procedure TfrmGanttChart.dspTaskProviderUpdateError(Sender: TObject;
  DataSet: TClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
  var Response: TResolverResponse);
{$ENDIF}
var
  li: TListItem;
  ProvName: string;

begin
  li := lvReconcileErrors.Items.Add;
  li.ImageIndex := 3;
  ProvName := (Sender as TComponent).Name;
  if ProvName = 'dspTaskProvider' then
  begin
    li.SubItems.Add(DataSet.FieldByName('idTask').AsString);
    li.SubItems.Add('Tasks');
  end
  else if ProvName = 'dspResources' then
  begin
    li.SubItems.Add(DataSet.FieldByName('tridTask').AsString);
    li.SubItems.Add('Resources');
  end
  else if ProvName = 'dspPredecessors' then
  begin
    li.SubItems.Add(DataSet.FieldByName('tpidTask').AsString);
    li.SubItems.Add('Predecessors');
  end;

  li.SubItems.Add(E.Message);
end;

{$IFDEF EZ_D6}
procedure TfrmGanttChart.dspTaskProviderAfterUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind);
{$ELSE}
procedure TfrmGanttChart.dspTaskProviderAfterUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind);
{$ENDIF}
var
  pf: PFixedKey;

begin
  if UpdateKind = ukInsert then
  begin
    // Open 'select @@identity' query
    dmProjectData.qrIdentity.Open;
    New(pf);
    pf^.OldKey := DeltaDS['idTask'];
    pf^.NewKey := dmProjectData.qrIdentity.Fields[0].Value;
    FixupKeys.Add(pf);
    dmProjectData.qrIdentity.Close;
  end;
end;

procedure TfrmGanttChart.cdsTaskPredecessorsNeedInvalidate(DataSet: TDataSet);
begin
  EzGantt.Invalidate;
end;

procedure TfrmGanttChart.cdsTaskResourcesNeedRefresh(DataSet: TDataSet);
begin
  if not FDeleting then
    ezdsTaskTree.Refresh;
end;

procedure TfrmGanttChart.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if not DataChanged then
    CanClose := True
  else
  begin
    CanClose := False;
    case MessageDlg(SApplyChanges, mtConfirmation, mbYesNoCancel, 0) of
      idYes:
      begin
        UpdateDatabase;
        if not DataChanged then
          CanClose := True;
      end;
      idNo: CanClose := True;
    end;
  end;

  if CanClose then
    // Close tables
    Active:=False;
end;

procedure TfrmGanttChart.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmGanttChart.cdsTasksBeforeDelete(DataSet: TDataSet);
  //
  //  Delete detail records when master gets deleted
  //
var
  idTask: Integer;

begin
  FDeleting := True;
  try
    idTask := cdsTasks['idTask'];
    cdsTaskResources.SetRange([idTask], [idTask]);
    while not cdsTaskResources.Eof do
      cdsTaskResources.Delete;

    with cdsTaskPredecessors do
    begin
      //
      // Delete predecessors for this task
      //
      SetRange([idTask], [idTask]);
      while not Eof do
        Delete;

      //
      // Delete predecessors pointing to this task
      //
      IndexName := 'ixidPredecessor';
      try
        SetRange([idTask], [idTask]);
        while not Eof do Delete;
      finally
        IndexName := 'ixidTask';
      end;
    end;
  finally
    FDeleting := False;
  end;
end;

procedure TfrmGanttChart.acScrollToReconcileErrorExecute(Sender: TObject);
begin
  ezdsTaskTree.Locate('idTask', StrToInt(lvReconcileErrors.Selected.SubItems[0]), []);
end;

procedure TfrmGanttChart.acScrollToReconcileErrorUpdate(Sender: TObject);
begin
  acScrollToReconcileError.Enabled :=
    Assigned(lvReconcileErrors.Selected) and
    (lvReconcileErrors.Selected.SubItems[0] <> ''); 
end;

procedure TfrmGanttChart.acScrollToScheduleErrorExecute(Sender: TObject);
begin
  ezdsTaskTree.Locate('idTask', StrToInt(lvScheduleMessages.Selected.SubItems[0]), []);
end;

procedure TfrmGanttChart.acScrollToScheduleErrorUpdate(Sender: TObject);
begin
  acScrollToScheduleError.Enabled :=
    Assigned(lvScheduleMessages.Selected) and (lvScheduleMessages.Selected.SubItems[0] <> '--');
end;

procedure TfrmGanttChart.acScheduleErrorInfoExecute(Sender: TObject);
var
  AMessage: TEzScheduleMessage;
  MessageText: string;

  function GetBoundaryText(Boundary: TScheduleWindowBoundType; TaskId: Variant): string;
  begin
    case Boundary of
      // Date boundary is set from project start date
      btProjectStartDate:
        Result := 'project start date';

      // Date boundary is set from project end date
      btProjectEndDate:
        Result := 'project end date';

      // Date boundary is set from start date of indicated task
      btTaskStartDate:
        Result := Format('start date of task %d', [integer(TaskId)]);

      // Date boundary is set from end date of indicated task
      btTaskEndDate:
        Result := Format('end date of task %d', [integer(TaskId)]);

      // Date boundary is set from constraint date of indicated task
      btConstraintDate:
        Result := Format('constraint date of task %d', [integer(TaskId)]);

      // Date boundary is set from the start date of the schedule window
      btTaskWindowStartDate:
        Result := Format('start date of the task window of task %d', [integer(TaskId)]);

      // Date boundary is set from the end date of the schedule window
      btTaskWindowEndDate:
        Result := Format('end date of the task window of task %d', [integer(TaskId)]);
    end;
  end;

begin
  AMessage := lvScheduleMessages.Selected.SubItems.Objects[0] as TEzScheduleMessage;

  MessageText := Format('Scheduler was unable to schedule task: %d', [StrToInt(lvScheduleMessages.Selected.SubItems[0])]);
  MessageText := MessageText + #13#10 + #13#10;
  MessageText := MessageText + 'Message: ' + AMessage.MessageText;
  if not VarIsNull(AMessage.ResourceKey) then
  begin
    MessageText := MessageText + #13#10;
    MessageText := MessageText +
      Format('Resource: %s',
             [string(dmProjectData.qrResources.Lookup('idResource', AMessage.ResourceKey, 'rsName'))]);
  end;
  MessageText := MessageText + #13#10 + #13#10;
  MessageText := MessageText + 'Schedule window available for this task: ';
  MessageText := MessageText + #13#10;
  MessageText := MessageText +
      Format('   Start: %s (=%s)',
             [
              DateTimeToStr(AMessage.ScheduleWindowStart),
              GetBoundaryText(AMessage.StartBoundaryType, AMessage.ScheduleWindowStartTaskKey.Value)
             ]);
  MessageText := MessageText + #13#10;
  MessageText := MessageText +
      Format('   End: %s (=%s)',
             [
              DateTimeToStr(AMessage.ScheduleWindowEnd),
              GetBoundaryText(AMessage.EndBoundaryType, AMessage.ScheduleWindowEndTaskKey.Value)
             ]);

  MessageDlg(MessageText, mtInformation, [mbOK], 0);
end;

procedure TfrmGanttChart.acScheduleErrorInfoUpdate(Sender: TObject);
begin
  acScheduleErrorInfo.Enabled := Assigned(lvScheduleMessages.Selected);
end;

procedure TfrmGanttChart.EzGanttGetBarProps(Sender: TObject;
  const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
var
  idTask: Integer;
  Field: TField;

  function GetBarLabel: string;
  begin
    with cdsTaskResourcesClone do
    begin
      Field := FieldByName('lkResourceName');
      idTask := BarInfo.Node.Key.Value;
      SetRange([idTask], [idTask]);
      First;
      while not Eof do
      begin
        if Result = '' then
          Result := Field.AsString else
          Result := Result + ', ' + Field.AsString;
        Next;
      end;
    end;
  end;

begin
  if BarInfo.Node = nil {Inserted record} then Exit;

  with BarInfo.BarProps do
  begin
    if Name = 'Summary bar' then
    begin
      BarInfo.BarProps.TextLabel.Text := GetBarLabel;
    end
    else if Name = 'Task bar' then
    begin
      BarInfo.BarProps.TextLabel.Text := GetBarLabel;
    end
    else if Name = 'Actual bar' then
    begin
      case ezdsTaskTreetaidStatus.AsInteger of
        STATUS_NONE:
          BarFill.Color := clLtGray;
        STATUS_RUNNING:
          BarFill.Color := clLime;
      end;
    end;
  end;
end;

procedure TfrmGanttChart.acApplyChangesExecute(Sender: TObject);
begin
  ezdsTaskTree.Post;
end;

procedure TfrmGanttChart.acApplyChangesUpdate(Sender: TObject);
begin
  with ezdsTaskTree do
    acApplyChanges.Enabled := Active and (State in [dsEdit, dsInsert]);
end;

procedure TfrmGanttChart.acCancelChangesExecute(Sender: TObject);
begin
  ezdsTaskTree.Cancel;
end;

procedure TfrmGanttChart.acCancelChangesUpdate(Sender: TObject);
begin
  with ezdsTaskTree do
    acCancelChanges.Enabled := Active and (State in [dsEdit, dsInsert]);
end;

procedure TfrmGanttChart.AddTask;
begin
  ezdsTaskTree.Add;
  ActiveControl := edTaskDescription;
end;

procedure TfrmGanttChart.AddChildTask;
begin
  ezdsTaskTree.AddChild;
  ActiveControl := edTaskDescription;
end;

procedure TfrmGanttChart.DeleteTask;
begin
  ezdsTaskTree.Delete;
end;

procedure TfrmGanttChart.IndentTask;
begin
  with ezdsTaskTree do
    Parent := RecordNode.Prev.Key;
  UpdateRecordOrderValue(ezdsTaskTree, ezdsTaskTree.RecordNode);
end;

procedure TfrmGanttChart.OutdentTask;
var
  ParentNode: TNodeKey;

begin
  with ezdsTaskTree do
  begin
    if RecordNode.Parent.Parent <> nil then
      ParentNode := RecordNode.Parent.Parent.Key
    else
    begin
      ParentNode.Level := -1;
      ParentNode.Value := Null;
    end;
    Parent := ParentNode;
    UpdateRecordOrderValue(ezdsTaskTree, ezdsTaskTree.RecordNode);
  end;
end;

procedure TfrmGanttChart.StartTask;
begin
  with ezdsTaskTree do
  begin
    if not (State in [dsEdit, dsInsert]) then
    begin
      Edit;
      FieldByName('taidStatus').AsInteger := STATUS_RUNNING;
      Post;
    end else
      FieldByName('taidStatus').AsInteger := STATUS_RUNNING;
  end;
end;

procedure TfrmGanttChart.StopTask;
begin
  with ezdsTaskTree do
  begin
    if not (State in [dsEdit, dsInsert]) then
    begin
      Edit;
      FieldByName('taidStatus').AsInteger := STATUS_COMPLETED;
      Post;
    end else
      FieldByName('taidStatus').AsInteger := STATUS_COMPLETED;
  end;
end;

procedure TfrmGanttChart.CancelTask;
begin
  with ezdsTaskTree do
  begin
    if not (State in [dsEdit, dsInsert]) then
    begin
      Edit;
      FieldByName('taidStatus').AsInteger := STATUS_CANCELED;
      Post;
    end else
      FieldByName('taidStatus').AsInteger := STATUS_CANCELED;
  end;
end;

procedure TfrmGanttChart.acRemoveCapacityExecute(Sender: TObject);
begin
  if not (ezdsTaskTree.State in [dsEdit, dsInsert]) then
    ezdsTaskTree.Edit;

  ezdsTaskTree.FieldByName('taCapacity').Clear;
end;

procedure TfrmGanttChart.acRemoveParentExecute(Sender: TObject);
var
  DoPost: Boolean;

begin
  DoPost := False;
  if not (ezdsTaskTree.State in [dsEdit, dsInsert]) then
  begin
    ezdsTaskTree.Edit;
    DoPost := True;
  end;
  ezdsTaskTree.FieldByName('taidParent').Clear;
  if DoPost then ezdsTaskTree.Post;
end;

procedure TfrmGanttChart.acRemoveParentUpdate(Sender: TObject);
begin
  acRemoveParent.Enabled :=
    ezdsTaskTree.Active and not ezdsTaskTree.FieldByName('taidParent').IsNull;
end;

procedure TfrmGanttChart.acEditStartDateExecute(Sender: TObject);
begin
  EditDateTimeField(ezdsTaskTree.FieldByName('taStart'));
end;

procedure TfrmGanttChart.acEditStartDateUpdate(Sender: TObject);
begin
  with ezdsTaskTree do
    acEditStartDate.Enabled := FieldCanModify(FieldByName('taStart'));
end;

procedure TfrmGanttChart.ezdsTaskTreelkParentGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then
    Text := '<click ... to select a parent>' else
    Text := Sender.AsString;
end;

procedure TfrmGanttChart.acEditStopdateExecute(Sender: TObject);
begin
  EditDateTimeField(ezdsTaskTree.FieldByName('taStop'));
end;

procedure TfrmGanttChart.acEditStopdateUpdate(Sender: TObject);
begin
  with ezdsTaskTree do
    acEditStopdate.Enabled := FieldCanModify(FieldByName('taStop'));
  edStop.ReadOnly := not acEditStopdate.Enabled;
end;

procedure TfrmGanttChart.acIndicateConstraintDateExecute(Sender: TObject);
begin
  // Capture mouse
  SetCapture(Handle);
  FIsSelectingConstraintDate := True;
end;

procedure TfrmGanttChart.acIndicateConstraintDateUpdate(Sender: TObject);
begin
  acIndicateConstraintDate.Checked := FIsSelectingConstraintDate;
end;

procedure TfrmGanttChart.acEditConstraintDateExecute(Sender: TObject);
begin
  EditDateTimeField(ezdsTaskTree.FieldByName('taConstraintDate'));
end;

procedure TfrmGanttChart.acEditConstraintDateUpdate(Sender: TObject);
begin
  with ezdsTaskTree do
    acEditConstraintDate.Enabled := FieldCanModify(FieldByName('taConstraintDate'));
  edConstraintDate.ReadOnly := not acEditConstraintDate.Enabled;
end;

procedure TfrmGanttChart.acEditActualStartExecute(Sender: TObject);
begin
  EditDateTimeField(ezdsTaskTree.FieldByName('taActualStart'));
end;

procedure TfrmGanttChart.acEditActualStartUpdate(Sender: TObject);
begin
  with ezdsTaskTree do
    acEditActualStart.Enabled := FieldCanModify(FieldByName('taActualStart'));
  edActualStart.ReadOnly := not acEditActualStart.Enabled;
end;

procedure TfrmGanttChart.acEditActualStopExecute(Sender: TObject);
begin
  EditDateTimeField(ezdsTaskTree.FieldByName('taActualStop'));
end;

procedure TfrmGanttChart.acEditActualStopUpdate(Sender: TObject);
begin
  with ezdsTaskTree do
    acEditActualStop.Enabled := FieldCanModify(FieldByName('taActualStop'));
  edScheduleStop.ReadOnly := not acEditActualStop.Enabled;
end;

procedure TfrmGanttChart.EzGanttGetHint(Sender: TCustomEzGanttChart;
  HitInfo: TEzGanttHitInfo; var HintText: String);
begin
  if hpOnLineMarker in HitInfo.HitPositions then
    HintText := HitInfo.HitLineMarker.Name;
end;

// Up down support for progress editor
procedure TfrmGanttChart.udProgressClick(Sender: TObject; Button: TUDBtnType);
begin
  with edProgress.Field do
  begin
    if not (Dataset.State in [dsEdit, dsInsert]) then
      Dataset.Edit;
    AsInteger := udProgress.Position;
  end;
end;

procedure TfrmGanttChart.udProgressChangingEx(Sender: TObject;
  var AllowChange: Boolean; NewValue: Smallint;
  Direction: TUpDownDirection);
begin
  if UpdateCount > 0 then Exit;
  if edProgress.Field.AsInteger <> udProgress.Position then
  begin
    BeginUpdate;
    try
      AllowChange := False;
      udProgress.Position := edProgress.Field.AsInteger;
    finally
      EndUpdate;
    end;
  end;
end;

procedure TfrmGanttChart.DelayedIdleDelayedIdle(
  Sender: TObject;
  var NewState: TEzIdleState);
begin
  try
    SetDetailTableRange;
    if ezDatasetScrolled then
    begin
      ezDatasetScrolled := false;
      with ezdsTaskTree do
      begin
        if FieldByName('taidStatus').AsInteger = STATUS_RUNNING then
          lbScheduleStop.Caption := '&Estimated stop' else
          lbScheduleStop.Caption := 'Act&ual stop';

        edStart.ReadOnly := not FieldCanModify(FieldByName('taStart'));
        edWork.Enabled := not edStart.ReadOnly;
        cbTaskStatus.Enabled := edWork.Enabled;
      end;

{
      if GetCapture = 0 then
        ScrollToTask;
}
    end;
  except
  end;
end;

procedure TfrmGanttChart.ezdsTaskTreeBeforePost(DataSet: TDataSet);

  function FieldHasChanged(AField: TField): Boolean;
  begin
    Result := not VarIsNull(AField.NewValue) and (AField.OldValue<>AField.NewValue);
  end;

begin
  with ezdsTaskTree do
  begin
    // Remember if this is a new task (see afterpost event)
    FTaskInserted := State = dsInsert;

    if not HasChildren and (FieldByName('taWork').AsFloat=0) then
      //
      // If a task is a MileStone (i.e. duration = 0) then
      // the start and stop dates must always be equal to the constraint date.
      //
    begin
      FieldByName('taStart').Value := FieldByName('taConstraintDate').Value;
      FieldByName('taStop').Value := FieldByName('taConstraintDate').Value;
    end;

    // Update fields taActualStart and taActualStop according to the value of
    // taidStatus.
    if FieldByName('taidStatus').OldValue <> FieldByName('taidStatus').Value then
      case FieldByName('taidStatus').AsInteger of
        STATUS_NONE:
        begin
          FieldByName('taActualStart').Clear;
          FieldByName('taActualStop').Clear;
        end;
        STATUS_RUNNING:
        begin
          FieldByName('taActualStart').AsDateTime := Now;
          FieldByName('taActualStop').AsDateTime := Now +
              FieldByName('taStop').AsDateTime - FieldByName('taStart').AsDateTime;
        end;
        STATUS_COMPLETED:
          FieldByName('taActualStop').AsDateTime := Now;
        STATUS_CANCELED:
        begin
          if FieldByName('taActualStart').IsNull then
            FieldByName('taActualStart').AsDateTime := Now;
          FieldByName('taActualStop').AsDateTime := Now;
        end;
      end;

    FRescheduleTask :=
      RunLocalScheduler and
      (
        FTaskInserted or
        FieldHasChanged(FieldByName('taWork')) or
        FieldHasChanged(FieldByName('taStart')) or
        FieldHasChanged(FieldByName('taStop')) or
        FieldHasChanged(FieldByName('taidPriority')) or
        FieldHasChanged(FieldByName('taActualStart')) or
        FieldHasChanged(FieldByName('taActualStop')) or
        FieldHasChanged(FieldByName('taidParent')) or
        FieldHasChanged(FieldByName('taidStatus')) or
        FieldHasChanged(FieldByName('taidConstraint')) or
        FieldHasChanged(FieldByName('taConstraintDate')) or
        FieldHasChanged(FieldByName('talocked'))
      );
  end;
end;

procedure TfrmGanttChart.EzGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  TheGrid: TEzDbTreeGrid;
  Index: Integer;

begin
  // Paint image column
  if (DataCol = 1) then
  begin
    //
    // use Sender to access grid because this will also work during printing
    //
    TheGrid := Sender as TEzDbTreeGrid;

    //
    // Paint small lock when a task is locked
    //
    if ezdsTaskTree.FieldByName('IsLocked').AsBoolean then
        // TaskImages is a TImageList with images at the appropriete size
        RecordStatusImages.Draw(TheGrid.Canvas, Rect.Left+2, Rect.Top+2, 0);

    //
    // Paint small status bitmap for running, completed and terminated tasks.
    //
    case ezdsTaskTree.FieldByName('taidStatus').AsInteger of
      STATUS_RUNNING: Index := 1;
      STATUS_COMPLETED: Index := 2;
      STATUS_CANCELED: Index := 3;
      else Index := -1;
    end;

    if Index <> -1 then
      // TaskImages is a TImageList with images at the appropriete size
      RecordStatusImages.Draw(TheGrid.Canvas, Rect.Left+RecordStatusImages.Width+2, Rect.Top+2, Index);

  end;
end;

procedure TfrmGanttChart.ezdsTaskTreetaidConstraintChange(Sender: TField);
  //
  // Event handler for idConstraint field updates.
  //
  // Here we match the constraint date with the constraint set by the user.
  //
begin
  case TEzConstraint(Sender.AsInteger) of
    cnDNL, cnASAP, cnALAP, cnIgnore:
      ezdsTaskTree.FieldByName('taConstraintDate').Clear;
  end;
end;

procedure TfrmGanttChart.cdsTaskResourcesAfterOpen(DataSet: TDataSet);
begin
  cdsTaskResourcesClone.CloneCursor(cdsTaskResources, false, true);
end;

procedure TfrmGanttChart.cdsTaskPredecessorsAfterOpen(DataSet: TDataSet);
begin
  cdsTaskPredecessorsClone.CloneCursor(cdsTaskPredecessors, false, true);
end;

procedure TfrmGanttChart.EzGanttEndbarDrag(Sender: TObject; X, Y: Integer;
  BarInfo: TScreenbar; DragType: TGanttBarDragType;
  var UpdateDataset: Boolean);
var
  Profile: TEzAvailabilityProfile;
  FreeProfile: Boolean;
  ResourceUnits: Double; // Total resource assignment in Units
  WorkingHours: Double;

  procedure PrepareTaskProfile;
  var
    TaskID: Integer;
  begin
    with cdsTaskResourcesClone do
    begin
      TaskID := ezdsTaskTree['idTask'];
      SetRange([TaskID], [TaskID]);
      First;

      if Eof then
      begin
        // Task has no resources assigned, therefore
        // we use project's profile to calculate working hours
        Profile := GetProjectProfile;
      end
      else
      begin
        ResourceUnits := 0.0;
        FreeProfile := True;
        Profile := TEzAvailabilityProfile.Create(1);
        Profile.Operator := opAnd;
        while not Eof do
        begin
          ResourceUnits := ResourceUnits + FieldByName('trAssigned').AsFloat;
          Profile.AddSource(GetResourceProfile(FieldByName('tridResource').AsInteger));
          Next;
        end;
      end;
    end;
  end;

begin
  if DragType in [bdNewBar, bdBarDate, bdBarStartDate] then
  begin
    Profile := nil;
    FreeProfile := False;
    ResourceUnits := 1.0;

    if DragType=bdNewBar then
      // A new bar has no resources jet and should therefore
      // be scheduled agains project profile
      Profile := GetProjectProfile else
      PrepareTaskProfile;

    try
      if not (ezdsTaskTree.State in [dsEdit, dsInsert]) then
        ezdsTaskTree.Edit;

      WorkingHours := Profile.CalcWorkingHours(BarInfo.BarStart, BarInfo.BarStop) * ResourceUnits;
      ezdsTaskTree.FieldByName('taWork').AsFloat := WorkingHours;
      ezdsTaskTree.FieldByName('taStart').AsDateTime := BarInfo.BarStart;
      ezdsTaskTree.FieldByName('taStop').AsDateTime := BarInfo.BarStop;

      ezdsTaskTree.Post;

      UpdateDataset := False;

      if DragType = bdNewBar then
        ActiveControl := edTaskDescription;
    finally
      if FreeProfile then
        Profile.Free;
    end;
  end;
end;

procedure TfrmGanttChart.ezdsTaskTreeBeforeDelete(DataSet: TDataSet);
begin
  if MessageDlg(Format(SConfirmDeleteTask, [ezdsTaskTreetaDescription.AsString]), mtConfirmation, [mbOK, mbCancel], 0) <> idOK then
    Abort;
end;

procedure TfrmGanttChart.CalcDateBounds(var LowerBound, UpperBound: TDateTime);
const
  BigDate = MaxInt;

var
  i: integer;
  dtStart, dtStop: TDateTime;

begin
  LowerBound := BigDate;
  UpperBound := 0;

  ezdsTaskTree.StartDirectAccess([cpShowAll]);
  try
    with EzGantt, ezdsTaskTree.ActiveCursor do
    begin
      // Direct Db access;
      // control db cursor through EzDataset cursor.
      MoveFirst;

      while not Eof do
      begin
        for i:=0 to Ganttbars.Count-1 do
        begin
          if CheckGanttbarVisibility(Ganttbars[i], False) and
             GetGanttbarDates(Ganttbars[i], dtStart, dtStop)
          then
            if (dtStart <> 0) and (dtStop <> 0) then
            begin
              LowerBound := min(LowerBound, dtStart);
              UpperBound := max(UpperBound, dtStop);
            end;
        end;

        MoveNext;
      end;
    end;
  finally
    ezdsTaskTree.EndDirectAccess;
  end;

  if LowerBound <> BigDate then
    //
    // round dates
    //
    with EzTimebar do
    begin
      LowerBound := TruncDate(MajorScale, MajorCount, MajorOffset, LowerBound);
      if TruncDate(MajorScale, MajorCount, MajorOffset, UpperBound) <> UpperBound then
        UpperBound := TruncDate(MajorScale, MajorCount, MajorOffset, AddTicks(MajorScale, MajorCount, UpperBound, 1));
    end;
end;

function TfrmGanttChart.GetResourceProfile(ResourceKey: Integer): TEzAvailabilityProfile;
var
  Rules: TEzCalendarRules;
  BaseProfile: TEzAvailabilityProfile;

begin
  //
  // Load calendar rules for this resource
  //
  Rules := TEzCalendarRules.Create;
  LoadResourceRules(Rules, dmProjectData.cnnProject, 'RESOURCE', ResourceKey);

  //
  // Load availability profile for this resource
  //
  BaseProfile := TEzAvailabilityProfile.Create(1);
  LoadAvailability(BaseProfile, dmProjectData.cnnProject, ResourceKey);

  //
  // Create a destination profile
  //
  Result := TEzAvailabilityProfile.Create(0);

  //
  // Assign calendar rules and availability profile to the destination profile.
  // The destination profile will merge the rules with the base availability
  // profile into one availability profile.
  //
  Result.Rules := Rules;
  // Rules must be freed automatically
  Result.OwnsRules := True;
  Result.AddSource(BaseProfile);
  // Sources must be freed automatically
  Result.OwnsSources := True;
end;

procedure TfrmGanttChart.pnlGraphResize(Sender: TObject);
begin
  // Align Y-Ax of graph with left side of ganttchart
  pnlFillGraph.Width :=
    EzGrid.Width + GanttChartSplitter.Width-ResourceGraphs.Yax.Width;
end;

procedure TfrmGanttChart.GanttChartSplitterMoved(Sender: TObject);
begin
  pnlGraphResize(nil);
end;

procedure TfrmGanttChart.ezdsTaskTreeAfterMoveRecord(Dataset: TDataSet; Node,
  Location: TRecordNode; Position: TEzInsertPosition);
begin
  UpdateRecordOrderValue(Dataset, Node);
end;

procedure TfrmGanttChart.UpdateRecordOrderValue(Dataset: TDataSet; Node: TRecordNode);
var
  OrderField: TField;
  P1, P2: Integer;
  TheDataset: TEzDataset;

  procedure GetOrderValues;
  begin
    with TheDataset, ActiveCursor do
    begin
      CurrentNode := Node;

      if not ((CurrentNode.Prev=nil) or (CurrentNode.Prev=TopNode)) then
      begin
        MovePrevSibling;
        p1 := OrderField.AsInteger;
        CurrentNode := Node;
      end else
        P1 := 0;

      if not ((CurrentNode.Next=nil) or (CurrentNode.Next=EndNode)) then
      begin
        MoveNextSibling;
        p2 := OrderField.AsInteger;
        CurrentNode := Node;
      end else
        p2 := p1+200;
    end;
  end;

begin
  if DatasetUpdateCount>0 then Exit;
  inc(DatasetUpdateCount);
  try
    TheDataset := Dataset as TEzDataset;
    with TheDataset, ActiveCursor do
    begin
      OrderField := FieldByName('taOrder');

      StartDirectAccess;
      try
        GetOrderValues;
        if (abs(P2-P1)<2) or ((p1+(p2-p1) div 2)<0) then
        begin
          ReorderBranch(TheDataset);
          GetOrderValues;
        end;

        CurrentNode := Node;
        Edit;
        OrderField.AsInteger := p1+(p2-p1) div 2;
        Post;
      finally
        EndDirectAccess;
      end;
    end;
  finally
    dec(DatasetUpdateCount);
  end;
end;

procedure TfrmGanttChart.LoadChildren(Node: TRecordNode; DoLoadPredecessors, DoLoadSuccessors: Boolean);
var
  Children: TList;
  i: Integer;

begin
  Children := TList.Create;
  try
    with ezdsTaskTree do
    begin
      RecordNode := Node.Child;
      while not ActiveCursor.Eof do
      begin
        Children.Add(ActiveCursor.CurrentNode);
        ActiveCursor.MoveNextSibling;
      end;
    end;

    for i:=0 to Children.Count-1 do
      LoadTask(TRecordNode(Children[i]), Node, False {do not load parents}, True, DoLoadPredecessors, DoLoadSuccessors);
  finally
    Children.Destroy;
  end;
end;

function TfrmGanttChart.LoadParent(Node: TRecordNode; DoLoadPredecessors, DoLoadSuccessors: Boolean): TEzScheduleTask;
begin
  Result := LoadTask(Node.Parent, nil, True, True {Load children}, DoLoadPredecessors, DoLoadSuccessors);
  // Restore position
  ezdsTaskTree.RecordNode := Node;
end;

procedure TfrmGanttChart.OverrideConstraint(Task: TEzScheduleTask);
begin
  if (Task.Predecessors.Count=0) {and VarIsNull(Task.Parent.Value)} then
    case Task.Constraint of
      cnAsap:
        //
        // Add MSO constraint to fix task at the drop position
        //
      begin
        Task.Constraint := cnMSO;
        if Task.Intervals.Count>0 then
          Task.ConstraintDate := max(FScheduleStartDate, Task.Intervals[0].Start) else
          Task.ConstraintDate := FScheduleStartDate;
      end;

      cnSNET:
        //
        // Add MSO constraint to fix task at the drop position
        //
      begin
        Task.Constraint := cnMSO;
        if Task.Intervals.Count>0 then
          Task.ConstraintDate := max(FScheduleStartDate, max(Task.ConstraintDate, Task.Intervals[0].Start)) else
          Task.ConstraintDate := max(FScheduleStartDate, Task.ConstraintDate);
      end;
    end;
end;

function TfrmGanttChart.LoadTask(Node, ParentNode: TRecordNode; DoLoadParent, DoLoadChildren, DoLoadPredecessors, DoLoadSuccessors: Boolean): TEzScheduleTask;

  procedure LoadTaskIntervals(Task: TEzScheduleTask);
  var
    Int: TEzScheduleInterval;
    Duration: Double;

  begin
    with ezdsTaskTree do
    begin
      Duration := FieldByName('taWork').AsFloat;
      if Duration>0 then
        // Add a schedule interval
      begin
        Int := TEzScheduleInterval.Create;
        Int.Start := FieldByName('ScheduleStart').AsDateTime;
        Int.Stop := FieldByName('ScheduleStop').AsDateTime;
        Int.Duration := Duration;
        Task.Intervals.Add(Int);
      end;
    end;
  end;

  procedure LoadResources;
  var
    R: TEzResourceAssignment;

  begin
    with cdsTaskResourcesClone do
    begin
      SetRange([Integer(Node.Key.Value)], [Integer(Node.Key.Value)]);
      First;
      while not Eof do
      begin
        R := TEzResourceAssignment.Create;
        R.Key := FieldByName('tridResource').AsInteger;
        R.Assigned := FieldByName('trAssigned').AsFloat;
        Result.Resources.Add(R);
        Next;
      end;
    end;
  end;

begin
  Result := Scheduler.FindTask(Node.Key);
  if Result <>nil then Exit;

  with ezdsTaskTree do
  begin
    // Move to task
    RecordNode := Node;
    Result := TEzScheduleTask.Create;
    Result.Key := Node.Key;
    if ParentNode<>nil then Result.Parent := ParentNode.Key;

    // Add task to scheduler
    Scheduler.AddTask(Result);

    Result.Constraint := TEzConstraint(FieldByName('ScheduleConstraint').AsInteger);
    Result.ConstraintDate := FieldByName('ScheduleConstraintDate').AsDateTime;

    // Add intervals
    if not HasChildren then
      LoadTaskIntervals(Result);

    // Load tasks' resources
    LoadResources;

    if DoLoadParent and HasParentRecord {and (Scheduler.FindTask(Node.Parent.Key)=nil)} then
    begin
      Result.Parent := LoadParent(Node, DoLoadPredecessors, DoLoadSuccessors).Key;
      Scheduler.RemoveTask(Result);
      Scheduler.AddTask(Result);
    end;

    if DoLoadChildren and HasChildren then
      LoadChildren(Node, DoLoadPredecessors, DoLoadSuccessors);

    // Add predecessors
    if DoLoadPredecessors then
      LoadPredecessors(Result, ParentNode);

    // Add successors
    if DoLoadSuccessors then
      LoadSuccessors(Result, ParentNode);
  end;
end;

function TfrmGanttChart.GetStartDateChecked(Task: TEzScheduleTask): TDateTime;
begin
  if  Task.Intervals.Count>0 then
    Result := Task.Intervals[0].Start
  else
  begin
    // Reactivate direct access
    ezdsTaskTree.StartDirectAccess();
    try
      ezdsTaskTree.NodeKey := Task.Key;
      Result := ezdsTaskTree['taStart'];
    finally
      ezdsTaskTree.EndDirectAccess;
    end;
  end;
end;

procedure TfrmGanttChart.LoadPredecessors(Task: TEzScheduleTask; ParentNode: TRecordNode);
var
  Predecessors: TList; // List of TEzPredecessor objects
  Pred: TEzPredecessor;
  PredecessorTask: TEzScheduleTask;
  i: Integer;

begin
  with ezdsTaskTree do
  begin
    cdsTaskPredecessorsClone.IndexName := 'ixidTask';
    cdsTaskPredecessorsClone.SetRange([Task.Key.Value], [Task.Key.Value]);
    Predecessors := TList.Create;
    try
      while not cdsTaskPredecessorsClone.Eof do
      begin
        // Add predecessor relation to our temporary list
        Pred := TEzPredecessor.Create;
        Pred.Key.Level := -1;
        Pred.Key.Value := cdsTaskPredecessorsClone.FieldByName('tpidPredecessor').AsInteger;
        Pred.Relation := TEzPredecessorRelation(cdsTaskPredecessorsClone.FieldByName('tpidRelation').AsInteger);
        Pred.Lag := cdsTaskPredecessorsClone.FieldByName('tpLag').AsFloat;
        Predecessors.Add(Pred);
        cdsTaskPredecessorsClone.Next;
      end;

      for i:=0 to Predecessors.Count-1 do
      begin
        Pred := TEzPredecessor(Predecessors[i]);
        // Lookup predecessor task in the scheduler
        PredecessorTask := Scheduler.FindTask(Pred.Key);
        if PredecessorTask = nil then
          //
          // Predecessor task has not been loaded yet, load it now
          //
        begin
          PredecessorTask := LoadTask(ActiveCursor.FindNode(Pred.Key), ParentNode, True {Load parent}, True {load children}, True {do load predecessors of predecessors}, False {do not load successors});

          if PredecessorTask.Predecessors.Count=0 then
            //
            // If predecessor task has no predecessors itself, override
            // constraint to prevent the scheduler to reschedule this task
            //
          begin
            PredecessorTask.Constraint := cnMSO;
            PredecessorTask.ConstraintDate := GetStartDateChecked(PredecessorTask);
            FScheduleStartDate :=
              min(FScheduleStartDate, DateTimeToEzDateTime(PredecessorTask.ConstraintDate));
          end;
        end;

        // Add predecessor to the list of predecessors
        if Task.Predecessors.IndexOf(Pred.Key) = -1 then
          Task.Predecessors.Add(Pred) else
          // Predecessor already exist, destroy object
          Pred.Destroy;
      end;
    finally
      Predecessors.Destroy;
    end;
  end;
end;

procedure TfrmGanttChart.LoadResourceCapacities(Resource: TEzResource);
var
  Capacity: TEzCapacityListItem;
  key: Variant;

begin
  dmProjectData.qrResourceCapacities.Parameters.ParamValues['idResource'] := Resource.Key;
  dmProjectData.qrResourceCapacities.Open;
  while not dmProjectData.qrResourceCapacities.Eof do
  begin
    key := dmProjectData.qrResourceCapacities['idCapacity'];
    Capacity := TEzCapacityListItem.Create(key);
//    if (Resource.Key = 26) then
//    begin
//      if (key = 7) then
//        Capacity.SpeedFactor := 2.0;
//    end;

    Resource.Capacities.Add(Capacity);
    dmProjectData.qrResourceCapacities.Next;
  end;
  dmProjectData.qrResourceCapacities.Close;
end;

procedure TfrmGanttChart.LoadResourceOperators(Resource: TEzResource);
var
  OperatorType: Char;
  OperatorID: Integer;
  Units: Double;
  R: TEzResourceAssignment;
  C: TEzCapacityAssignment;

begin
  dmProjectData.qrResourceOperators.Parameters.ParamValues['idResource'] := Resource.Key;
  dmProjectData.qrResourceOperators.Open;

  while not dmProjectData.qrResourceOperators.Eof do
  begin
    OperatorType := dmProjectData.qrResourceOperators.FieldByName('Type').AsString[1];
    OperatorID := dmProjectData.qrResourceOperators.FieldByName('idOperator').AsInteger;
    Units := dmProjectData.qrResourceOperators.FieldByName('AllocationUnits').AsFloat;

    if OperatorType = 'C' then
    begin
      C := TEzCapacityAssignment.Create;
      C.Key := OperatorID;
      C.Assigned := Units;
      C.AffectsDuration := False;
      C.MergeProfiles :=
        dmProjectData.qrCapacities.Lookup('idCapacity', OperatorID, 'caMergeProfiles');

      Resource.Operators.Add(C);
    end
    else
    begin
      R := TEzResourceAssignment.Create;
      R.Key := OperatorID;
      R.Assigned := Units;
      R.AffectsDuration := False;
      Resource.Operators.Add(R);
    end;

    dmProjectData.qrResourceOperators.Next;
  end;
  dmProjectData.qrResourceOperators.Close;
end;

procedure TfrmGanttChart.LoadProjectResources;
var
  Resource: TEzResource;
  resId: Integer;

begin
  // This code can be optimised.
  //
  // Loading resources each time we reschedule is a time consuming businiss
  // so we should actually keep the resources in memory at all times.
  //
  // Also this code loads all resources, not only the resources used in this
  // project.
  Scheduler.Resources.Clear;
  dmProjectData.qrResources.First;

  while not dmProjectData.qrResources.Eof do
  begin
    resID := dmProjectData.qrResources['idResource'];
    Resource := TEzResource.Create(resId);
    // Scheduler 'owns' resource and will therefore release object
    Scheduler.Resources.Add(Resource);
    LoadResourceScheduleData(Resource);
    dmProjectData.qrResources.Next;
  end;
end;

procedure TfrmGanttChart.LoadSuccessors(Task: TEzScheduleTask; ParentNode: TRecordNode);
var
  Successors: TList; // List of TEzPredecessor objects
  Pred: TEzPredecessor;
  SuccessorTask: TEzScheduleTask;
  i: Integer;

begin
  with ezdsTaskTree do
  begin
    cdsTaskPredecessorsClone.IndexName := 'ixidPredecessor';
    cdsTaskPredecessorsClone.SetRange([Task.Key.Value], [Task.Key.Value]);
    Successors := TList.Create;
    try
      while not cdsTaskPredecessorsClone.Eof do
      begin
        // Add predecessor relation to task
        Pred := TEzPredecessor.Create;
        Pred.Key.Level := -1;
        Pred.Key.Value := cdsTaskPredecessorsClone.FieldByName('tpidTask').AsInteger;
        Pred.Relation := TEzPredecessorRelation(cdsTaskPredecessorsClone.FieldByName('tpidRelation').AsInteger);
        Pred.Lag := cdsTaskPredecessorsClone.FieldByName('tpLag').AsFloat;
        Successors.Add(Pred);
        cdsTaskPredecessorsClone.Next;
      end;

      for i:=0 to Successors.Count-1 do
      begin
        Pred := TEzPredecessor(Successors[i]);
        // Lookup successor task in the scheduler
        SuccessorTask := Scheduler.FindTask(Pred.Key);
        if SuccessorTask = nil then
          //
          // Successor task has not been loaded yet, load it now
          //
          SuccessorTask := LoadTask(ActiveCursor.FindNode(Pred.Key), ParentNode, True {Load parent}, True {load children}, False {do not load predecessors}, True {load successors of successor});

        // Add successor to the list of successor
        if SuccessorTask.Predecessors.IndexOf(Task.Key) = -1 then
        begin
          Pred.Key := Task.Key;
          SuccessorTask.Predecessors.Add(Pred);
        end else
          // Successor already exist, destroy object
          Pred.Destroy;

        // Delay load predecessors
        LoadPredecessors(SuccessorTask, ParentNode);
      end;
    finally
      Successors.Destroy;
    end;
  end;
end;

procedure TfrmGanttChart.ClearScheduleMessages;
var
  i: Integer;

begin
  for i:=0 to lvScheduleMessages.Items.Count-1 do
    lvScheduleMessages.Items[i].SubItems.Objects[0].Free;

  lvScheduleMessages.Items.Clear;
end;

procedure TfrmGanttChart.LoadScheduleMessages;
var
  i: Integer;
  AMessage, AMessageCopy: TEzScheduleMessage;
  li: TListItem;

begin
  // Display any/other schedule messages.
  for i:=0 to Scheduler.Messages.Count-1 do
  begin
    li := lvScheduleMessages.Items.Add;
    li.Caption := '';
    AMessage := Scheduler.Messages[i];
    li.SubItems.Add(AMessage.TaskKey.Value);
    li.SubItems.Add(AMessage.MessageText);

    // Keep a copy of the schedule error
    AMessageCopy := TEzScheduleMessage.Create;
    AMessageCopy.Assign(AMessage);
    li.SubItems.Objects[0] := AMessageCopy;
  end;

  if lvScheduleMessages.Items.Count>0 then
  begin
    shScheduleMessages.TabVisible := True;
    pcBottomPane.ActivePage := shScheduleMessages;
  end else
    shScheduleMessages.TabVisible := False;
end;

procedure TfrmGanttChart.RescheduleFromTask(TaskToSchedule: TRecordNode);
var
  Task: TEzScheduleTask;

  procedure SaveScheduleResult;
  begin
    with ezdsTaskTree do
    begin
      StartDirectAccess([cpShowAll]);
      try
        Task := nil;
        while SingleTaskScheduler.GetNextScheduledTask(Task) do
        begin
          RecordNode := ActiveCursor.FindNode(Task.Key);
          Edit;
          ezdsTaskTree['taStart'] := Task.Intervals[0].Start;
          ezdsTaskTree['taStop'] := Task.Intervals[0].Stop;
          Post;
        end;
      finally
        EndDirectAccess;
      end;
    end;
  end;

  procedure Prepare;
  begin
    ClearScheduleMessages;

    // Because Scheduler.Reset will destroy the resource
    // availability graphs, we have to detach any graph first.
    DetachGraphs;

    // Prepare scheduler, clear data from previous schedule
    SingleTaskScheduler.Reset;
    SingleTaskScheduler.SchedulebeyondScheduleWindow := True;

    // Window holds the project schedule window.
    // That is the start and stop dates in which the project must be completed.
    FScheduleStartDate := ProjectStart;
    SingleTaskScheduler.ProjectWindowStart := ProjectStart;
    SingleTaskScheduler.ProjectWindowStop := ProjectStop;
    SingleTaskScheduler.ProjectWindowAbsoluteStart := ProjectStart-365;
    SingleTaskScheduler.ProjectWindowAbsoluteStop := ProjectStop+365;

    //
    // Assign availability profiel to scheduler.
    // The availability profile determines the working and non-working periods
    // of resources.
    //
    SingleTaskScheduler.ProjectProfile := GetProjectProfile;

    //
    // Get start date for task TaskID
    //
    with ezdsTaskTree do
    begin
      StartDirectAccess([cpShowAll]);
      try
        // Load this task into the scheduler.
        // Loading this task will also load it's predecessors and successors.

        Task := LoadTask(TaskToSchedule, nil, True {Load parent}, True {load children}, True {Load predecessors}, True {Load successors});
        OverrideConstraint(Task);
{
        case Task.Constraint of
          cnAsap:
            if (Task.Predecessors.Count=0) then
              //
              // Add MSO constraint to fix task at the drop position
              //
            begin
              Task.Constraint := cnMSO;
              Task.ConstraintDate := max(Task.Intervals[0].Start, FScheduleStartDate);
            end;

          cnSNET:
            if Task.Predecessors.Count = 0 then
              //
              // Add MSO constraint to fix task at the drop position
              //
            begin
              Task.Constraint := cnMSO;
              Task.ConstraintDate := max(FScheduleStartDate, max(Task.ConstraintDate, Task.Intervals[0].Start));
            end;
        end;
}
      finally
        EndDirectAccess;
      end;
    end;
  end;

begin
  // Prevent recursive calls
  RunLocalScheduler := False;
  try
    Prepare;
    SingleTaskScheduler.Reschedule;
    if srScheduledOK in SingleTaskScheduler.ScheduleResults then
      //
      // Apply changes
      //
      SaveScheduleResult;

    // Load error messages, if any.
    LoadScheduleMessages;
  finally
    RunLocalScheduler := True;
  end;
end;

procedure TfrmGanttChart.ReorderBranch(Dataset: TEzDataset);
var
  OrderField: TField;
  p, i: Integer;
  CurrentOrder: TList;

begin
  with Dataset do
  begin
    OrderField := FieldByName('taOrder');
    StartDirectAccess([cpShowAll]);
    CurrentOrder := TList.Create;
    try
      if ActiveCursor.CurrentNode.Parent<>nil then
        // Move to first child of the parent of the current record
        ActiveCursor.CurrentNode := ActiveCursor.CurrentNode.Parent.Child else
        ActiveCursor.MoveFirst;

      // Remember current sortorder in branch
      while ActiveCursor.CurrentNode <> ActiveCursor.EndNode do
      begin
        CurrentOrder.Add(ActiveCursor.CurrentNode);
        ActiveCursor.CurrentNode := ActiveCursor.CurrentNode.Next;
      end;

      p := 100;
      for i:=0 to CurrentOrder.Count-1 do
      begin
        ActiveCursor.CurrentNode := TRecordNode(CurrentOrder[i]);
        Edit;
        OrderField.AsInteger := p;
        Post;
        inc(p, 100);
      end;
    finally
      EndDirectAccess;
      CurrentOrder.Destroy;
    end;
  end;
end;

procedure TfrmGanttChart.EzGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_INSERT then
    AddTask;
end;

procedure TfrmGanttChart.ezdsTaskTreeAfterPost(DataSet: TDataSet);
var
  CurrentPos: TRecordNode;

begin
  if DatasetUpdateCount>0 then Exit;

  // When a new task is inserted this task will have no value for field taOrder.
  // Therefore the task will be inserted as the last child of it's branch.
  // Here we move the task back to it's insert position. As a result,
  // event AfterMoveRecord will be fired which will update field taOrder
  // with the right value.
  if FTaskInserted then
  begin
    if not IsSorted and (FInsertPosition <> ipInsertAsChild) then
    begin
      CurrentPos := ezdsTaskTree.RecordNode;
      ezdsTaskTree.MoveRecord(CurrentPos, FInsertNode, FInsertPosition);
      ezdsTaskTree.RecordNode := CurrentPos;
    end else
      // Task is inserted in a sorted view.
      // Keep task in it's current position, however do update
      // value for field taOrder.
      UpdateRecordOrderValue(ezdsTaskTree, ezdsTaskTree.RecordNode);

    FTaskInserted := False;
  end;

  if FAutoSchedule and FRescheduleTask and not FIsUpdating then
    RescheduleFromTask(ezdsTaskTree.RecordNode);
end;

procedure TfrmGanttChart.acAddPredecessorExecute(Sender: TObject);
begin
  cdsTaskPredecessors.Append;
  ActiveControl := grTaskPredecessors;
end;

procedure TfrmGanttChart.acDeletePredecessorExecute(Sender: TObject);
begin
  cdsTaskPredecessors.Delete;
end;

procedure TfrmGanttChart.acDeletePredecessorUpdate(Sender: TObject);
begin
  acDeletePredecessor.Enabled := not cdsTaskPredecessors.Eof;
end;

procedure TfrmGanttChart.acAddresourceExecute(Sender: TObject);
begin
  cdsTaskResources.Append;
  ActiveControl := grTaskResources;
end;

procedure TfrmGanttChart.acDeleteResourceExecute(Sender: TObject);
begin
  cdsTaskResources.Delete;
end;

procedure TfrmGanttChart.acDeleteResourceUpdate(Sender: TObject);
begin
  acDeleteResource.Enabled := not cdsTaskResources.Eof;
end;

procedure TfrmGanttChart.ezdsTaskTreeBeforeInsert(DataSet: TDataSet);
begin
  pcBottomPane.ActivePage := shTaskFields;
  pcTaskDetail.ActivePage := tsTaskGeneral;
end;

procedure TfrmGanttChart.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  // Handle Control+O shortcut, this should be done by the action manager
  // but somehow it doesn't respond to Ctrl+O...
  if (ssCtrl in Shift) and
     ((Char(Key) = 'O') or (Char(Key) = 'o')) and
      MainForm.acOutdentTask.Enabled
  then
    OutdentTask;

  if ezdsTaskTree.State in [dsEdit, dsInsert] then
    case Key of
      VK_ESCAPE:
        ezdsTaskTree.Cancel;
      VK_RETURN:
        ezdsTaskTree.Post;
    end;
{
      begin
        Key := 0;
        PostMessage((Sender as TWinControl).Handle, WM_KEYDOWN, VK_TAB, 0);
      end;
}
end;

procedure TfrmGanttChart.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  ct: TWinControl;
  AControl: TControl;
  P: TPoint;
  H: HWND;
  ScreenPos: TPoint;
  Gantt: TEzGanttChart;

begin
  if not FIsSelectingConstraintDate then Exit;
  FIsSelectingConstraintDate := False;
  ReleaseCapture;

  if MainForm.ActiveMdiChild=nil then Exit;

  ScreenPos := ClientToScreen(Point(X,Y));
  P := MainForm.ActiveMdiChild.ScreenToClient(ScreenPos);
  H := ChildWindowFromPoint(MainForm.ActiveMdiChild.Handle, P);
  ct := FindControl(H);
  if ct=nil then Exit;
  

  repeat
    AControl := ct.ControlAtPos(ct.ScreenToClient(ScreenPos), True, True);
    if AControl is TWinControl then
      ct := AControl as TWinControl else
      ct := nil;
  until (ct is TEzGanttChart) or (ct=nil);

  if (ct is TEzGanttChart) then
  begin
    Gantt := ct as TEzGanttChart;
    P := ct.ScreenToClient(ScreenPos);
    ezdsTaskTree.Edit;
    ezdsTaskTree.FieldByName('taConstraintDate').AsDateTime :=
      Gantt.Timebar.X2DateTime(P.X);
    ezdsTaskTree.Post;
  end else
    ShowMessage('Please click on a gantt chart to indicate a position');
end;

procedure TfrmGanttChart.acSetScaleYearMonthExecute(Sender: TObject);
begin
  EzTimebar.ScaleIndex := 0;
end;

procedure TfrmGanttChart.acSetScaleWeekDayExecute(Sender: TObject);
begin
  EzTimebar.ScaleIndex := 1;
end;

procedure TfrmGanttChart.acTimebarOptionsExecute(Sender: TObject);
begin
  EzTimebar.OpenSetupDialog;
end;

procedure TfrmGanttChart.EzGanttOverGanttChart(Sender: TCustomEzGanttChart;
  HitInfo: TEzGanttHitInfo);
var
  Bar: TScreenbar;
  HintText: string;

begin
  if ezdsTaskTree.State <> dsBrowse then Exit;

  if (hpOnBar in HitInfo.HitPositions) then
  begin

    // Mouse pointer is located over the bar indicated
    // by EzGantt.DrawInfo[HitInfo.HitRow][HitInfo.HitBar]

    Bar := EzGantt.DrawInfo[EzGantt.ScreenRowToCacheIndex(HitInfo.HitRow)][HitInfo.HitBar];

    // Dataset's active record is the current record which is not nessesarily
    // the same record as the one having the mouse cursor. We therefore
    // have to update datasets' cursor position to match with the
    // record belonging to pBar.

    ezdsTaskTree.StartDirectAccess;
    try
      // Move cursor to detail record
      ezdsTaskTree.ActiveCursor.CurrentNode := Bar.Node;

      HintText :=   ezdsTaskTree.FieldByName('taDescription').AsString + #13#10 +
                    'ID: ' + ezdsTaskTree.FieldByName('idTask').AsString + #13#10 +
                    'Start: ' + ezdsTaskTree.FieldByName('taStart').AsString + #13#10 +
                    'Stop: ' + ezdsTaskTree.FieldByName('taStop').AsString;
    finally
      ezdsTaskTree.EndDirectAccess;
    end;
  end;
  EzGantt.Hint := HintText;
end;

procedure TfrmGanttChart.ezdsTaskTreeAfterOpen(DataSet: TDataSet);
begin
  ezdsTaskTree.ActiveCursor.MoveFirst;
  while not ezdsTaskTree.ActiveCursor.Eof do
  begin
    if ezdsTaskTree.ActiveCursor.HasChildren then
    begin
      cdsTasks.FindKey([ezdsTaskTree.ActiveCursor.CurrentNode.Key.Value]);
      ezdsTaskTree.ActiveCursor.Expanded := cdsTasks['taExpanded'];
    end;

    ezdsTaskTree.ActiveCursor.MoveNext;
  end;

  ezdsTaskTree.Resync([]);
end;

procedure TfrmGanttChart.ezdsTaskTreeAfterExpandRecord(Dataset: TDataSet; Node: TRecordNode);
begin
  if FIsOpening then Exit;
  if not (ezdsTaskTree.State in [dsEdit, dsInsert]) then
  begin
    ezdsTaskTree.Edit;
    ezdsTaskTree['taExpanded'] := ezdsTaskTree.Expanded;
    ezdsTaskTree.Post;
  end else
    ezdsTaskTree['taExpanded'] := ezdsTaskTree.Expanded;
end;

//
// Event handler for TEzGanttChart.SelectPredecessorline
// Here we select the predecessor relation on the 'predecessor' tab
//
procedure TfrmGanttChart.EzGanttSelectPredecessorLine(Sender: TObject;
  Predecessor: TPredecessor; var CanSelect: Boolean);
begin
  // Active predecessorline page
  pcTaskDetail.ActivePage := tsTaskPredecessors;

  // See also SetDetailTableRange,
  // because here cdsTaskPredecessors is synchronized with selected line
end;

procedure TfrmGanttChart.EzGanttMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // Scroll to task control:
  // We only scroll ganttchart to the active task when the user
  // clicks the ganttchart.
  FMouseTickCount := GetTickCount;
end;

procedure TfrmGanttChart.EzGanttMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // Scroll to task control:
  // We only scroll ganttchart to the active task when the user
  // clicks the ganttchart (max duration 200 ms)
  if GetTickCount-FMouseTickCount<200 then
    ScrollToTask;
end;

procedure TfrmGanttChart.cdsTaskPredecessorsAfterPost(DataSet: TDataSet);
begin
  if not FIsUpdating then
  begin
    if FAutoSchedule then
      RescheduleFromTask(ezdsTaskTree.RecordNode);
    EzGantt.Invalidate;
  end;
end;

procedure TfrmGanttChart.chkFilledClick(Sender: TObject);
var
  props: TResourceGraphProperties;

begin
  props := GetGraphProperties(Integer(lvGraphResources.Selected.Data));
  props.Filled := chkFilled.Checked;
  if props.Filled then
  begin
    props.Line.Brush.Style := bsSolid;
    props.Line.Brush.Color := props.Color;
    props.Line.Pen.Style := psClear;
  end
  else
  begin
    props.Line.Brush.Style := bsClear;
    props.Line.Pen.Style := psSolid;
    props.Line.Pen.Color := props.Color;
  end;
end;

procedure TfrmGanttChart.EzGanttDragDrop(Sender: TCustomEzGanttChart;
  Source: TObject; State: TDragState; Pt: TPoint; HitInfo: TEzGanttHitInfo;
  var Operations: TEzGanttDragOperations;
  var Relation: TEzPredecessorRelation; var Accept: Boolean);
var
  Delta: TDateTime;
  DragObject: TEzGanttDragObject;
  stop: TRecordNode;

begin
  // We do not want to interfere with linking two tasks
  if doLinkTasks in Operations then Exit;

  // Get the bar being dragged
  // TEzGanttDragObject holds all information about the bar being dragged
  DragObject := Source as TEzGanttDragObject;

  // Was a summary bar dragged?
  if (DragObject.Bar.Bar.Name = 'Summary bar') then
  begin
    // Prevent ganttchart from updating the DataSet
    Accept := False;

    // Start direct access mode
    ezdsTaskTree.StartDirectAccess([cpShowAll]);
    RunLocalScheduler := False;
    try
      // Move cursor to summary bar record
      ezdsTaskTree.ActiveCursor.CurrentNode := DragObject.Bar.BarNode;

      // Calculate how much the bar was moved
      Delta := DragObject.BarStart-ezdsTaskTree.FieldByName('taStart').AsDateTime;

      // Update all children, move them by Delta
      if (Delta<>0) and (ezdsTaskTree.ActiveCursor.HasChildren) then
      begin
        stop := ezdsTaskTree.ActiveCursor.CurrentNode.Next;

        ezdsTaskTree.ActiveCursor.CurrentNode := ezdsTaskTree.ActiveCursor.CurrentNode.Child;
        repeat
          ezdsTaskTree.Edit;
          // Update start field
          ezdsTaskTree.FieldByName('taStart').AsDateTime :=
            ezdsTaskTree.FieldByName('taStart').AsDateTime+Delta;
          // Update stop field
          ezdsTaskTree.FieldByName('taStop').AsDateTime :=
            ezdsTaskTree.FieldByName('taStop').AsDateTime+Delta;
          ezdsTaskTree.Post;

          ezdsTaskTree.ActiveCursor.MoveNext;

          if ezdsTaskTree.ActiveCursor.CurrentNode = stop then
            break;

        until false; // Go again
      end;
    finally
      RunLocalScheduler := True;
      ezdsTaskTree.EndDirectAccess;
    end;
  end;
end;

procedure TfrmGanttChart.EzGanttStartDrag(Sender: TObject; var DragObject:
    TDragObject);
begin
;
end;

procedure TfrmGanttChart.EzGridColExit(Sender: TObject);
begin
  if Sender = nil then;
end;

procedure TfrmGanttChart.EzTimebarLayoutChange(Sender: TObject);
begin
  EzGantt.ScrollbarOptions.LeftExtend := EzTimebar.Date;
  EzGantt.ScrollbarOptions.RightExtend := EzTimebar.X2DateTime(10*Width);
end;

procedure TfrmGanttChart.GraphTypeChange(Sender: TObject);
var
  props: TResourceGraphProperties;

begin
  props := GetGraphProperties(Integer(lvGraphResources.Selected.Data));
  if props.GraphType <> GraphType.ItemIndex then
  begin
    props.GraphType := GraphType.ItemIndex;
    LoadGraphData(props);
  end;
end;

procedure TfrmGanttChart.LoadResourceScheduleData(Resource: TEzResource);
  // Handle OnLoadResourceData
  // This event is fired by the scheduler whenever a new resource is
  // encoutered in a task. The event handler should return the
  // availability profile for the resource.

begin
  // Lookup resource name
  with dmProjectData.qrResources do
  begin
    Locate('idResource', Resource.Key, []);
    Resource.Name := FieldByName('rsName').AsString;
    Resource.AllowTaskSwitching := FieldByName('rsAllowTaskSwitching').AsBoolean;
  end;

  Resource.Availability := GetResourceProfile(Resource.Key);
  //
  // Resource owns availability profile. Therefore this profile will be freed
  // when the resource is freed.
  //

  LoadResourceCapacities(Resource);
  LoadResourceOperators(Resource);
end;

procedure TfrmGanttChart.ResourceGraphsGetHint(
  Sender: TEzGraph;
  Location: TPoint;
  Line, PointIndex: Integer;
  SelectionKind: TSelectionKind;
  var Hint: string);
var
  Allocation: Double;
  graphLine: TEzGraphLine;
  i: Integer;
  props: TResourceGraphProperties;
  Resource: TEzResource;
  Start: string;
  Stop: string;
begin
  if Line = -1 then
  begin
    Hint := DateTimeToStr(Sender.Timebar.X2DateTime(Location.X));
  end
  else
  begin
    graphLine := ResourceGraphs.Lines[Line];

    props := nil;
    i := 0;
    while i < FResourceGraphProperties.Count do
    begin
      props := TResourceGraphProperties(FResourceGraphProperties[i]);
      if props.Line = graphLine then
        break;
      inc(i);
    end;

    // Get scheduled resource
    Resource := Scheduler.Resources.ResourceByKey(props.ResourceID);

    case props.GraphType of
      0:
      begin
        Start := DateTimeToStr(graphLine.Profile[PointIndex].DateTime);
        if PointIndex + 1 < graphLine.Profile.Count then
          Stop := DateTimeToStr(graphLine.Profile[PointIndex + 1].DateTime) else
          Stop := 'n.a.';

        Allocation := graphLine.Profile[PointIndex].Units;
        Hint := Format( '%s' + #13#10 +
                        '  Resource: %s' + #13#10 +
                        '  Interval ''%s'' - ''%s''' + #13#10 +
                        '  Units: %f',
                        [
                          DateTimeToStr(Sender.Timebar.X2DateTime(Location.X)),
                          Resource.Name,
                          Start,
                          Stop,
                          Allocation
                        ]);
      end;

      1:
      begin
        Allocation := graphLine.Profile[PointIndex].Units;
        Hint := Format( '%s' + #13#10 +
                        '  Resource: %s' + #13#10 +
                        '  Resource load: %.0f%',
                        [
                          DateTimeToStr(Sender.Timebar.X2DateTime(Location.X)),
                          Resource.Name,
                          Allocation
                        ]);
      end;

      2:
      begin
        Allocation := graphLine.Profile[PointIndex].Units;
        Hint := Format( '%s' + #13#10 +
                        '  Resource: %s' + #13#10 +
                        '  Running total: %.0f hours',
                        [
                          DateTimeToStr(Sender.Timebar.X2DateTime(Location.X)),
                          Resource.Name,
                          Allocation
                        ]);
      end;
    end;

  end;
end;

// This event is raised when the scheduler needs the allocation profile
// for a given resource. The allocation profile keeps track of the allocated intervals
// for this resource. If Profile is set to nil, the  scheduler will use the default
// allocation profile (=Resource.ActualAvailability);
procedure TfrmGanttChart.SchedulerGetResourceAllocationProfile(
  Task: TEzScheduleTask;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource;
  var Profile: TEzAvailabilityProfile);
begin
{$IFDEF USE_CUSTOM_PROFILE}
  // TCustomAvailabilityProfile overrides method AllocateInterval.
  // This just a sample on how to respond to allocations from the scheduler
  Profile := TCustomAvailabilityProfile.Create(Resource.ActualAvailability);

  FProfilesAllocated.Add(Profile);
{$ENDIF}
end;

// This events allow you to return a customized schedule profile to the scheduler.
procedure TfrmGanttChart.SchedulerGetResourceScheduleProfile(
  Task: TEzScheduleTask;
  Interval: TEzScheduleInterval;
  Assignment: TEzResourceAssignment;
  Resource: TEzResource);
begin
{$IFDEF USE_CUSTOM_PROFILE}
  // Return a 24 hours profile
  Assignment.ScheduleProfile := TEzAvailabilityProfile.Create(1);
  Assignment.IgnoreOverallocations := True; // Do no generate error messages when allocating this schedule on the resource original's profile

  // We need to Free profile when we are done.
  // Profiles can be released when scheduler runs again or when window gets
  // detroyed
  FProfilesAllocated.Add(Assignment.ScheduleProfile);
{$ENDIF}
end;

{ TCustomAvailabilityProfile }

// The scheduler calls method AllocateInterval for every interval
// to be allocated;
function TCustomAvailabilityProfile.AllocateInterval(Start, Stop: TEzDateTime;
  Units: Double; AllowNegative: Boolean): Boolean;
begin
  // This is just a sample. Pass call to default resource profile.
  Result := _SourceProfile.AllocateInterval(Start, Stop, Units, AllowNegative);
end;

constructor TCustomAvailabilityProfile.Create(Source: TEzAvailabilityProfile);
begin
  inherited Create(1);
  _SourceProfile := Source;
end;

end.
