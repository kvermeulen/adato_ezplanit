unit DateTimeSelector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ComCtrls, ExtCtrls, Buttons;

resourcestring
  SHistoryDate = 'The selected date is in the past.'#13#10'continue anyway?';

type
  TfrmSelectDateTime = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    gbDate: TGroupBox;
    caDate: TMonthCalendar;
    gbTime: TGroupBox;
    edTime: TDateTimePicker;
    SpeedButton1: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    function Editdate(var ADate: TDatetime) : Boolean;
    function Execute(ADate: TDateTime): TDateTime;
  end;

var
  frmSelectDateTime: TfrmSelectDateTime;

implementation

{$R *.DFM}

function TfrmSelectDateTime.Editdate(var ADate: TDatetime) : boolean;
var
  Hour, Min, Sec, MSec: Word;

begin
  Result := false;

  if ADate = 0 then
    ADate := Date;

  DecodeTime(ADate, Hour, Min, Sec, MSec);
  caDate.Date := int(ADate);

  edTime.Time := ADate;

  if ShowModal = idOK then
  begin
    ADate := int(caDate.Date) + Frac(edTime.Time);
    Result := true;
  end;
end;

function TfrmSelectDateTime.Execute(ADate: TDateTime): TDateTime;
var
  Saved: TDateTime;

begin
  Saved := ADate;
  if Editdate(ADate) then
    Result := ADate else
    Result := Saved;
end;

procedure TfrmSelectDateTime.FormShow(Sender: TObject);
begin
  edTime.SetFocus;
end;

procedure TfrmSelectDateTime.SpeedButton1Click(Sender: TObject);
begin
  caDate.Date := int(Now);
  edTime.Time := Frac(Now);
end;

procedure TfrmSelectDateTime.FormClose(Sender: TObject;
  var Action: TCloseAction);

const
  ONE_MINUTE = 1/24/60;

var
  dt: TDateTime;

begin
  Action := caHide;

  if  ModalResult = idOK then
  begin
    dt := int(caDate.Date) + Frac(edTime.Time);
    if (dt < Now-10*ONE_MINUTE) and
       (MessageDlg(SHistoryDate, mtWarning, [mbYes, mbNo], 0) = mrNo)
    then
      Action := caNone;
  end;
end;

end.
