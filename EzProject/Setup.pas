unit Setup;

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DB, EzVirtualDataset, MSXML, Mask, DBCtrls, Grids, DBGrids,
  EzDBGrid;

const
  NumericTypes = [ftInteger, ftBoolean, ftDateTime, ftDate, ftTime];

type
  TfrmSetup = class(TForm)
    btnCancel: TButton;
    btnOK: TButton;
    Label1: TLabel;
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    XmlSetupDataset: TEzArrayDataset;
    XmlSetupDatasetDocumentStorage: TStringField;
    dsSetupData: TDataSource;
    DBEdit1: TDBEdit;
    XmlSetupDatasetScalesSetup: TMemoField;
    XmlSetupDatasetWindowPosition: TStringField;
    procedure Button1Click(Sender: TObject);
    procedure XmlSetupDatasetGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure XmlSetupDatasetGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure XmlSetupDatasetPostData(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure XmlSetupDatasetAfterPost(DataSet: TDataSet);
  private

  protected
    function  GetSetupString(Index: Integer): string;
    procedure SetSetupString(Index: Integer; Value: string);

  public
    XMLSetup: IXMLDOMDocument;

    property Database: string index 0 read GetSetupString;
    property ScalesSetup: string index 1 read GetSetupString write SetSetupString;
    property WindowPosition: string index 2 read GetSetupString write SetSetupString;
  end;

var
  frmSetup: TfrmSetup;

implementation

{$R *.DFM}

{$IFDEF EZ_D6}
uses Variants;
{$ENDIF}

function ForceChildNode(ANode: IXMLDOMNode; ChildName: string): IXMLDOMNode;
begin
  with ANode do
  begin
    Result := selectSingleNode(ChildName);
    if Result = nil then
    begin
      Result := ownerDocument.createElement(ChildName);
      appendChild(Result);
    end;
  end;
end;

procedure SaveRecordToNode(ADataset: TDataset; ANode: IXMLDOMNode);
var
  i: Integer;

begin
  for i:=0 to ADataset.Fields.Count-1 do
    ForceChildNode(ANode, ADataset.Fields[i].FieldName).text :=
      ADataset.Fields[i].AsString;
end;

function GetFieldValue(Field: TField; ANode: IXMLDOMNode): Variant;
begin
  if ANode <> nil then
    if (Field.DataType in NumericTypes) and (ANode.text = '') then
      Result := Null else
      Result := ANode.Text;
end;

procedure TfrmSetup.FormCreate(Sender: TObject);
var
  SetupFile: string;

begin
  SetupFile := ChangeFileExt(ParamStr(0), '.xml');
  XMLSetup := CoDOMDocument.Create;
  if FileExists(SetupFile) then
    XMLSetup.load(SetupFile) else
    XMLSetup.loadXml('<Settings><Database>.\EzProject.mdb</Database></Settings>');

  XmlSetupDataset.Open;
end;

procedure TfrmSetup.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = idOK then
  begin
    if XmlSetupDataset.State = dsEdit then
      XmlSetupDataset.Post else
      XMLSetup.Save(ChangeFileExt(ParamStr(0), '.xml'));
  end
  else
    // Undo changes
    XmlSetupDataset.Cancel;

  CanClose := True;
end;

function TfrmSetup.GetSetupString(Index: Integer): string;
begin
  Result := XmlSetupDataset.Fields[Index].AsString;
end;

procedure TfrmSetup.SetSetupString(Index: Integer; Value: string);
begin
  if not (XmlSetupDataset.State in [dsEdit, dsInsert]) then
    XmlSetupDataset.Edit;
  XmlSetupDataset.Fields[Index].AsString := Value;
  XmlSetupDataset.Post;
end;

procedure TfrmSetup.Button1Click(Sender: TObject);
begin
  with XmlSetupDataset do
  begin
    OpenDialog1.Filename := FieldByName('Database').AsString;
    if OpenDialog1.Execute then
    begin
      Edit;
      FieldByName('Database').AsString := OpenDialog1.Filename;
      Post;
    end;
  end;
end;

procedure TfrmSetup.XmlSetupDatasetGetFieldValue(
  Sender: TCustomEzArrayDataset; Field: TField; Index: Integer;
  var Value: Variant);
begin
  Value := GetFieldValue(Field, XMLSetup.documentElement.selectSingleNode(Field.FieldName));
end;

procedure TfrmSetup.XmlSetupDatasetGetRecordCount(
  Sender: TCustomEzArrayDataset; var Count: Integer);
begin
  Count := 1;
end;

procedure TfrmSetup.XmlSetupDatasetPostData(Sender: TCustomEzArrayDataset; Index: Integer);
begin
  SaveRecordToNode(Sender, XMLSetup.documentElement);
end;

procedure TfrmSetup.XmlSetupDatasetAfterPost(DataSet: TDataSet);
begin
  XMLSetup.Save(ChangeFileExt(ParamStr(0), '.xml'));
end;

end.
