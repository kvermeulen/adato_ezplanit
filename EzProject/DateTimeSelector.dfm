object frmSelectDateTime: TfrmSelectDateTime
  Left = 433
  Top = 291
  BorderStyle = bsDialog
  Caption = 'Select date time'
  ClientHeight = 217
  ClientWidth = 425
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnCancel: TButton
    Left = 320
    Top = 176
    Width = 75
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object btnOK: TButton
    Left = 240
    Top = 176
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object gbDate: TGroupBox
    Left = 8
    Top = 8
    Width = 217
    Height = 193
    Caption = 'Date'
    TabOrder = 0
    object caDate: TMonthCalendar
      Left = 12
      Top = 23
      Width = 197
      Height = 154
      Date = 37110.402959270830000000
      TabOrder = 0
    end
  end
  object gbTime: TGroupBox
    Left = 232
    Top = 8
    Width = 177
    Height = 153
    Caption = 'Time'
    TabOrder = 1
    object SpeedButton1: TSpeedButton
      Left = 38
      Top = 75
      Width = 101
      Height = 22
      Hint = 'Nu'
      Caption = 'Current time'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333000003333333333F77777FFF333333009999900
        3333333777777777FF33330998FFF899033333777333F3777FF33099FFFCFFF9
        903337773337333777F3309FFFFFFFCF9033377333F3337377FF098FF0FFFFFF
        890377F3373F3333377F09FFFF0FFFFFF90377F3F373FFFFF77F09FCFFF90000
        F90377F733377777377F09FFFFFFFFFFF90377F333333333377F098FFFFFFFFF
        890377FF3F33333F3773309FCFFFFFCF9033377F7333F37377F33099FFFCFFF9
        90333777FF37F3377733330998FCF899033333777FF7FF777333333009999900
        3333333777777777333333333000003333333333377777333333}
      NumGlyphs = 2
      OnClick = SpeedButton1Click
    end
    object edTime: TDateTimePicker
      Left = 39
      Top = 48
      Width = 101
      Height = 21
      Date = 37575.469232569440000000
      Time = 37575.469232569440000000
      Kind = dtkTime
      TabOrder = 0
    end
  end
end
