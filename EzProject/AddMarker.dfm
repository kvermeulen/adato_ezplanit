object frmAddLineMarker: TfrmAddLineMarker
  Left = 533
  Top = 299
  Caption = 'Add new line marker'
  ClientHeight = 188
  ClientWidth = 255
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnMouseDown = FormMouseDown
  OnMouseUp = FormMouseDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 173
    Height = 13
    Caption = 'Enter a name for the new line marker'
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 88
    Height = 13
    Caption = 'Location of marker'
  end
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 104
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333300333333C3333330FF033333C3303330FF033333C330030FF0
      333333C330F00FF03333333330FFFF03333333C330FFFF00033333C330FFFFFF
      0333333330FFFFF0333333C330FFFF03333333C330FFF033333333C330FF0333
      333333C330F03333333333C33003333333333333333333333333}
    OnClick = SpeedButton1Click
  end
  object Label3: TLabel
    Left = 40
    Top = 104
    Width = 191
    Height = 39
    Caption = 
      'Click here to use your mouse to indicate '#13#10'a position on the gan' +
      'tt chart.'#13#10'(this dialog will be hidden while doing so)'
  end
  object edName: TEdit
    Left = 8
    Top = 24
    Width = 249
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 138
    Top = 160
    Width = 75
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object btnOK: TButton
    Left = 50
    Top = 160
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object dtpDate: TDateTimePicker
    Left = 8
    Top = 72
    Width = 145
    Height = 21
    Date = 37433.437589039300000000
    Time = 37433.437589039300000000
    TabOrder = 1
  end
  object dtpTime: TDateTimePicker
    Left = 160
    Top = 72
    Width = 97
    Height = 21
    Date = 37433.437680497700000000
    Time = 37433.437680497700000000
    Kind = dtkTime
    TabOrder = 2
  end
end
