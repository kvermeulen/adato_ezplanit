unit Splash;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TfrmSplash = class(TForm)
    Image1: TImage;
    Memo1: TMemo;
    Label1: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    btnClose: TButton;
    lbVersion: TLabel;
    mmRegistered: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    Timer1: TTimer;
    Label7: TLabel;
    lbBuild: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  procedure ShowAboutDialog;

var
  frmSplash: TfrmSplash;

implementation

{$R *.DFM}

procedure GetVersionInfo(var Version, Build: string);
var
  Len: UINT;        // length of structs returned from API calls
  Ptr: Pointer;     // points to version info structures
  InfoSize: DWORD;  // size of info buffer
  Dummy: DWORD;     // stores 0 in call to GetFileVersionInfoSize
  FixedFileInfo: TVSFixedFileInfo;
  PInfoBuffer: PChar;

begin
  FillChar(FixedFileInfo, SizeOf(FixedFileInfo), 0);
  InfoSize := GetFileVersionInfoSize(PChar(ParamStr(0)), Dummy);
  if InfoSize > 0 then
  begin
    PInfoBuffer := StrAlloc(InfoSize);
    try
      if GetFileVersionInfo(PChar(ParamStr(0)), Dummy, InfoSize, PInfoBuffer) then
      begin
        // Get fixed file info & copy to own storage
        VerQueryValue(PInfoBuffer, '\', Ptr, Len);
        FixedFileInfo := PVSFixedFileInfo(Ptr)^;

        Version := Format('%d.%d.%d',
          [
            HiWord(FixedFileInfo.dwFileVersionMS),
            LoWord(FixedFileInfo.dwFileVersionMS),
            HiWord(FixedFileInfo.dwFileVersionLS)
          ]);
        Build := Format('%d', [LoWord(FixedFileInfo.dwFileVersionLS)]);
      end;
    finally
      StrDispose(PInfoBuffer);
    end;
  end;
end;

procedure ShowAboutDialog;
begin
  with TfrmSplash.Create(nil) do
  try
    Timer1.Enabled := False;
    ShowModal;
  finally
    Destroy;
  end;
end;

procedure TfrmSplash.FormCreate(Sender: TObject);
var
  Version, Build: string;
begin
{$IFDEF DEMO}
  mmRegistered.Visible := False;
  Timer1.Enabled := True;
{$ELSE}
  mmRegistered.Visible := True;
  Timer1.Enabled := False;
{$ENDIF}

  GetVersionInfo(Version, Build);
  lbVersion.Caption := Version;
  lbBuild.Caption := Build;
end;

procedure TfrmSplash.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  ShowModal;
end;

end.
