unit ResourceProperties;

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db, ADODB, CheckLst, EzVirtualDataset, EzDataset,
  Grids, DBGrids, EzDBGrid, DBClient, Provider;

resourcestring
  SConfirmDeleteCapacity = 'Are you sure capacity ''%s'' should be deleted?';

  
type
  TfrmResourceProps = class(TForm)
    Label3: TLabel;
    Label4: TLabel;
    DBText4: TDBText;
    edName: TDBEdit;
    btnCancel: TButton;
    btnOK: TButton;
    dsEditResource: TDataSource;
    DBCheckBox1: TDBCheckBox;
    qrResource: TADOQuery;
    qrResourceidResource: TAutoIncField;
    qrResourcersName: TWideStringField;
    qrResourcersAllowTaskSwitching: TBooleanField;
    lbCapacities: TCheckListBox;
    Label1: TLabel;
    btnAddCapacity: TButton;
    btnDeleteCapacity: TButton;
    qrResourceCapacities: TADOQuery;
    DataSource1: TDataSource;
    qrResourceCapacitiesidResource: TIntegerField;
    qrResourceCapacitiesidCapacity: TIntegerField;
    Label2: TLabel;
    Label5: TLabel;
    EzDBTreeGrid1: TEzDBTreeGrid;
    OperatorTree: TEzDataset;
    OperatorGroups: TEzArrayDataset;
    qrOperators: TADOQuery;
    dsOperatorAllocation: TDataSource;
    dsOperatorGroups: TDataSource;
    OperatorGroupsType: TStringField;
    OperatorGroupsDescription: TStringField;
    OperatorsTree: TDataSource;
    OperatorTreeType: TStringField;
    OperatorTreeDescription: TStringField;
    qrOperatorAllocation: TEzArrayDataset;
    qrOperatorAllocationType: TStringField;
    qrOperatorAllocationID: TIntegerField;
    qrOperatorAllocationDescription: TStringField;
    OperatorTreeID: TIntegerField;
    qrOperatorsType: TWideStringField;
    qrOperatorsID: TIntegerField;
    qrOperatorsDescription: TWideStringField;
    scGridSchemes: TEzTreeGridSchemes;
    qrOperatorAllocationAllocation: TStringField;
    OperatorTreeAllocation: TStringField;
    btnEditCapacity: TButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnAddCapacityClick(Sender: TObject);
    procedure btnDeleteCapacityClick(Sender: TObject);
    procedure lbCapacitiesClickCheck(Sender: TObject);
    procedure OperatorGroupsGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure OperatorGroupsGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure OperatorGroupsLocate(Sender: TCustomEzArrayDataset;
      const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions; var Index: Integer);
    procedure qrOperatorAllocationGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure qrOperatorAllocationLocate(Sender: TCustomEzArrayDataset;
      const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions; var Index: Integer);
    procedure qrOperatorAllocationGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure qrOperatorAllocationPostData(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure FormCreate(Sender: TObject);
    procedure OperatorTreeBeforeInsert(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEditCapacityClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
  protected
    AllocationUnits: TStringList;
    Modified: Boolean;
    function  GetResourceID: Integer;
    procedure SetResourceID(Value: Integer);
    procedure LoadCapacities;
    procedure LoadOperators;
    procedure SaveCapacities;
    procedure SaveOperators;

  public
    { Public declarations }
    property ResourceID: Integer read GetResourceID write SetResourceID;
  end;

var
  frmResourceProps: TfrmResourceProps;

implementation

uses
  ProjectData,
  Capacity
{$IFDEF EZ_D6}
  , Variants
{$ENDIF}
  ;


{$R *.DFM}

function TfrmResourceProps.GetResourceID: Integer;
begin
  Result := qrResource.Parameters.ParamValues['idResource'];
end;

procedure TfrmResourceProps.lbCapacitiesClickCheck(Sender: TObject);
begin
  Modified := True;
end;

procedure TfrmResourceProps.LoadCapacities;
var
  Checked: Boolean;
  Capacity: Integer;

begin
  Modified := False;

  lbCapacities.Items.Clear;

  if dmProjectData.qrCapacities.Active then
    dmProjectData.qrCapacities.Requery else
    dmProjectData.qrCapacities.Open;

  dmProjectData.qrCapacities.First;

  qrResourceCapacities.Parameters.ParamValues['idResource'] := ResourceID;
  qrResourceCapacities.Open;

  while not dmProjectData.qrCapacities.Eof do
  begin
    Capacity := dmProjectData.qrCapacities.FieldByName('idCapacity').AsInteger;
    Checked := qrResourceCapacities.Locate('idResource;idCapacity', VarArrayOf([ResourceID, Capacity]), []);
    lbCapacities.Items.AddObject(
      dmProjectData.qrCapacities.FieldByName('caDescription').AsString,
      TObject(Capacity));
    lbCapacities.Checked[lbCapacities.Items.Count - 1] := Checked;
    dmProjectData.qrCapacities.Next;
  end;

  qrResourceCapacities.Close;
end;

procedure TfrmResourceProps.LoadOperators;
var
  Key: string;

begin
  dmProjectData.qrResourceOperators.Parameters.ParamValues['idResource'] := ResourceID;
  dmProjectData.qrResourceOperators.Open;
  AllocationUnits.Clear;
  while not dmProjectData.qrResourceOperators.Eof do
  begin
    Key := Trim(dmProjectData.qrResourceOperators.FieldByName('Type').AsString) +
           dmProjectData.qrResourceOperators.FieldByName('idOperator').AsString;
    AllocationUnits.Values[Key] := dmProjectData.qrResourceOperators.FieldByName('AllocationUnits').AsString;
    dmProjectData.qrResourceOperators.Next;
  end;
  dmProjectData.qrResourceOperators.Close;

  qrOperators.Open;
  OperatorTree.Open;
end;

procedure TfrmResourceProps.OperatorGroupsGetFieldValue(
  Sender: TCustomEzArrayDataset; Field: TField; Index: Integer;
  var Value: Variant);
const
  OperatorGroups: array[0..1, 0..1] of string = (('C', 'Capacities'),('R', 'Resources'));
begin
  Value := OperatorGroups[Index, Field.Index];
end;

procedure TfrmResourceProps.OperatorGroupsGetRecordCount(
  Sender: TCustomEzArrayDataset; var Count: Integer);
begin
  Count := 2; // Capacities and Resources
end;

procedure TfrmResourceProps.OperatorGroupsLocate(Sender: TCustomEzArrayDataset;
  const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions;
  var Index: Integer);
begin
  if KeyValues = 'C' then
    Index := 0 else
    Index := 1;
end;

procedure TfrmResourceProps.OperatorTreeBeforeInsert(DataSet: TDataSet);
begin
  Abort;
end;

procedure TfrmResourceProps.qrOperatorAllocationGetFieldValue(
  Sender: TCustomEzArrayDataset; Field: TField; Index: Integer;
  var Value: Variant);
var
  Key: string;

begin
  qrOperators.RecNo := Index + 1;
  if Field.FieldName = 'Allocation' then
  begin
    Key := qrOperators.FieldByName('Type').AsString + qrOperators.FieldByName('ID').AsString;
    Value := AllocationUnits.Values[Key];
  end else
    Value := qrOperators[Field.FieldName];
end;

procedure TfrmResourceProps.qrOperatorAllocationGetRecordCount(
  Sender: TCustomEzArrayDataset; var Count: Integer);
begin
  Count := qrOperators.RecordCount;
end;

procedure TfrmResourceProps.qrOperatorAllocationLocate(
  Sender: TCustomEzArrayDataset; const KeyFields: string;
  const KeyValues: Variant; Options: TLocateOptions; var Index: Integer);
begin
  qrOperators.Locate(KeyFields, KeyValues, []);
  Index := qrOperators.RecNo-1;
end;

procedure TfrmResourceProps.qrOperatorAllocationPostData(
  Sender: TCustomEzArrayDataset; Index: Integer);
var
  Key, Value: string;
begin
  qrOperators.RecNo := Index + 1;
  Key := qrOperators.FieldByName('Type').AsString + qrOperators.FieldByName('ID').AsString;
  Value := AllocationUnits.values[Key];
  if Value <> Sender.FieldByName('Allocation').AsString then
  begin
    Value := Sender.FieldByName('Allocation').AsString;
    if StrToFloat(Value) = 0.0 then
      AllocationUnits.values[Key] := '' else
      AllocationUnits.values[Key] := Value;

    Modified := True;
  end;
end;

procedure TfrmResourceProps.SaveCapacities;
var
  I: Integer;
  Capacity: Integer;

begin
  dmProjectData.cnnProject.Execute('delete from ResourceCapacities where idResource=' + IntToStr(ResourceID));
  for I := 0 to lbCapacities.Items.Count - 1 do
  begin
    if lbCapacities.Checked[I] then
    begin
      Capacity := Integer(lbCapacities.Items.Objects[I]);
      dmProjectData.cnnProject.Execute(Format('insert into ResourceCapacities values(%d, %d)', [ResourceID, Capacity]));
    end;
  end;
end;

procedure TfrmResourceProps.SaveOperators;
var
  I: Integer;
  Key: string;
  Value: string;

begin
  dmProjectData.cnnProject.Execute('delete from Operators where idResource=' + IntToStr(ResourceID));
  for I := 0 to AllocationUnits.Count - 1 do
  begin
    Key := AllocationUnits.Names[I];
    // Do not use ValueFromIndex    
    Value := Copy(AllocationUnits[I], Length(Key) + 2, MaxInt);
    dmProjectData.cnnProject.Execute(
      Format('insert into Operators values(%d, ''%s'', ''%s'', ''%s'')',
      [
        ResourceID,
        Copy(Key, 2, MaxInt),
        Copy(Key, 1, 1),
        Value
      ]));
  end;
end;

procedure TfrmResourceProps.SetResourceID(Value: Integer);
begin
  qrResource.Parameters.ParamValues['idResource'] := Value;
end;

procedure TfrmResourceProps.btnAddCapacityClick(Sender: TObject);
begin
  TfrmCapacity.AddNew;
  LoadCapacities;
end;

procedure TfrmResourceProps.btnDeleteCapacityClick(Sender: TObject);
begin
  if MessageDlg(Format(SConfirmDeleteCapacity, [lbCapacities.Items[lbCapacities.ItemIndex]]), mtConfirmation, [mbOK, mbCancel], 0) = idOK then
  begin
    TfrmCapacity.Delete(Integer(lbCapacities.Items.Objects[lbCapacities.ItemIndex]));
    LoadCapacities;
  end;
end;

procedure TfrmResourceProps.btnEditCapacityClick(Sender: TObject);
begin
  if lbCapacities.ItemIndex >= 0 then
  begin
    TfrmCapacity.Edit(Integer(lbCapacities.Items.Objects[lbCapacities.ItemIndex]));
    LoadCapacities;
  end;
end;

procedure TfrmResourceProps.FormActivate(Sender: TObject);
begin
  if ResourceID=0 then
  begin
    Caption := 'Add resource';
    qrResource.Open;
    qrResource.Insert;
  end
  else
  begin
    Caption := 'Edit resource';
    qrResource.Open;
    qrResource.Edit;
  end;

  LoadCapacities();
  LoadOperators();
end;

procedure TfrmResourceProps.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  qrOperators.Close;
  OperatorTree.Close;
end;

procedure TfrmResourceProps.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = idOK then
  begin
    CanClose := False;
    if qrResource.Modified then
    begin
      qrResource.Post;
      dmProjectData.qrResources.Requery;
    end;

    if Modified then
    begin
      SaveCapacities;
      SaveOperators;
    end;

  end;

  qrResource.Close;
  CanClose := True;
end;

procedure TfrmResourceProps.FormCreate(Sender: TObject);
begin
  AllocationUnits := TStringList.Create;
end;

procedure TfrmResourceProps.FormDestroy(Sender: TObject);
begin
  AllocationUnits.Free;
end;

end.
