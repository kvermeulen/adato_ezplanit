object frmResourceProps: TfrmResourceProps
  Left = 467
  Top = 291
  Caption = 'Resource properties'
  ClientHeight = 394
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 36
    Top = 43
    Width = 31
    Height = 13
    Caption = 'name'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 56
    Top = 16
    Width = 11
    Height = 13
    Caption = 'id'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBText4: TDBText
    Left = 72
    Top = 16
    Width = 65
    Height = 17
    DataField = 'idResource'
    DataSource = dsEditResource
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 8
    Top = 111
    Width = 59
    Height = 13
    Caption = 'capacities'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 13
    Top = 223
    Width = 54
    Height = 13
    Caption = 'operators'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 357
    Top = 255
    Width = 198
    Height = 52
    Caption = 
      'Operators are the resources required to '#39'operate'#39' this equipment' +
      '. Operators will be scheduled with the resource they are assigne' +
      'd to.'
    WordWrap = True
  end
  object edName: TDBEdit
    Left = 72
    Top = 40
    Width = 273
    Height = 21
    DataField = 'rsName'
    DataSource = dsEditResource
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 310
    Top = 361
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object btnOK: TButton
    Left = 229
    Top = 361
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object DBCheckBox1: TDBCheckBox
    Left = 72
    Top = 72
    Width = 281
    Height = 17
    Caption = 'Resource may work on different tasks simultaneously'
    DataField = 'rsAllowTaskSwitching'
    DataSource = dsEditResource
    TabOrder = 1
    ValueChecked = 'True'
    ValueUnchecked = 'False'
  end
  object lbCapacities: TCheckListBox
    Left = 72
    Top = 111
    Width = 273
    Height = 106
    OnClickCheck = lbCapacitiesClickCheck
    ItemHeight = 13
    TabOrder = 4
  end
  object btnAddCapacity: TButton
    Left = 351
    Top = 111
    Width = 82
    Height = 16
    Caption = 'Add capacity'
    TabOrder = 2
    OnClick = btnAddCapacityClick
  end
  object btnDeleteCapacity: TButton
    Left = 350
    Top = 156
    Width = 83
    Height = 16
    Caption = 'Delete'
    TabOrder = 3
    OnClick = btnDeleteCapacityClick
  end
  object EzDBTreeGrid1: TEzDBTreeGrid
    Left = 73
    Top = 223
    Width = 272
    Height = 120
    DataSource = OperatorsTree
    Options = [dgEditing, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 7
    SchemeCollection = scGridSchemes
    TitlebarHeight = 17
    Columns = <
      item
        Expanded = False
        FieldName = 'Description'
        Width = 160
        Visible = True
        SchemeList = (
          0
          1)
      end
      item
        Expanded = False
        FieldName = 'Allocation'
        Visible = True
        SchemeList = (
          0
          1)
      end>
  end
  object btnEditCapacity: TButton
    Left = 351
    Top = 133
    Width = 82
    Height = 16
    Caption = 'Edit capacity'
    TabOrder = 8
    OnClick = btnEditCapacityClick
  end
  object dsEditResource: TDataSource
    DataSet = qrResource
    Left = 200
    Top = 128
  end
  object qrResource: TADOQuery
    Connection = dmProjectData.cnnProject
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idResource'
        Attributes = [paNullable]
        DataType = ftInteger
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from Resources where idResource=:idResource')
    Left = 168
    Top = 128
    object qrResourceidResource: TAutoIncField
      FieldName = 'idResource'
      ReadOnly = True
    end
    object qrResourcersName: TWideStringField
      FieldName = 'rsName'
      Size = 50
    end
    object qrResourcersAllowTaskSwitching: TBooleanField
      FieldName = 'rsAllowTaskSwitching'
    end
  end
  object qrResourceCapacities: TADOQuery
    Connection = dmProjectData.cnnProject
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idResource'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select * from ResourceCapacities '
      'where idResource=:idResource')
    Left = 168
    Top = 168
    object qrResourceCapacitiesidResource: TIntegerField
      FieldName = 'idResource'
    end
    object qrResourceCapacitiesidCapacity: TIntegerField
      FieldName = 'idCapacity'
    end
  end
  object DataSource1: TDataSource
    DataSet = qrResourceCapacities
    Left = 200
    Top = 168
  end
  object OperatorTree: TEzDataset
    Cursors = <
      item
        CursorName = 'DefaultCursor'
        Options = [cpDefExpanded]
      end>
    EzDataLinks = <
      item
        DataSource = dsOperatorGroups
        KeyField = 'Type'
        FieldLinks = ()
      end
      item
        DataSource = dsOperatorAllocation
        KeyField = 'Type;ID'
        ParentRefField = 'Type'
        FieldLinks = ()
      end>
    BeforeInsert = OperatorTreeBeforeInsert
    Left = 256
    Top = 240
    object OperatorTreeType: TStringField
      DisplayWidth = 1
      FieldName = 'Type'
      Size = 1
    end
    object OperatorTreeDescription: TStringField
      DisplayWidth = 50
      FieldName = 'Description'
      Size = 50
    end
    object OperatorTreeID: TIntegerField
      DisplayWidth = 10
      FieldName = 'ID'
    end
    object OperatorTreeAllocation: TStringField
      FieldName = 'Allocation'
      Size = 10
    end
  end
  object OperatorGroups: TEzArrayDataset
    ReadOnly = True
    OnGetFieldValue = OperatorGroupsGetFieldValue
    OnGetRecordCount = OperatorGroupsGetRecordCount
    OnLocate = OperatorGroupsLocate
    Left = 176
    Top = 240
    object OperatorGroupsType: TStringField
      FieldName = 'Type'
      Size = 1
    end
    object OperatorGroupsDescription: TStringField
      FieldName = 'Description'
      Size = 50
    end
  end
  object qrOperators: TADOQuery
    Connection = dmProjectData.cnnProject
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select '#39'C'#39' as Type, idCapacity as ID, caDescription as Descripti' +
        'on'
      'from Capacities'
      'union'
      'select '#39'R'#39' as Type, idResource, rsName'
      'from Resources'
      'order by Type, Description')
    Left = 112
    Top = 272
    object qrOperatorsType: TWideStringField
      FieldName = 'Type'
      Size = 255
    end
    object qrOperatorsID: TIntegerField
      FieldName = 'ID'
    end
    object qrOperatorsDescription: TWideStringField
      FieldName = 'Description'
      Size = 255
    end
  end
  object dsOperatorAllocation: TDataSource
    DataSet = qrOperatorAllocation
    Left = 208
    Top = 272
  end
  object dsOperatorGroups: TDataSource
    DataSet = OperatorGroups
    Left = 208
    Top = 240
  end
  object OperatorsTree: TDataSource
    DataSet = OperatorTree
    Left = 296
    Top = 240
  end
  object qrOperatorAllocation: TEzArrayDataset
    OnGetFieldValue = qrOperatorAllocationGetFieldValue
    OnGetRecordCount = qrOperatorAllocationGetRecordCount
    OnLocate = qrOperatorAllocationLocate
    OnPostData = qrOperatorAllocationPostData
    Left = 176
    Top = 272
    object qrOperatorAllocationType: TStringField
      FieldName = 'Type'
      ReadOnly = True
      Size = 1
    end
    object qrOperatorAllocationID: TIntegerField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qrOperatorAllocationDescription: TStringField
      FieldName = 'Description'
      ReadOnly = True
      Size = 50
    end
    object qrOperatorAllocationAllocation: TStringField
      FieldName = 'Allocation'
      Size = 10
    end
  end
  object scGridSchemes: TEzTreeGridSchemes
    Schemes = <
      item
        Name = 'Group header'
        Color = 16773864
        Colspan = 3
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        TextIndent = 80
        ExpandButtons.Expand = 0
        ExpandButtons.Collapse = 1
        Indent = 14
      end
      item
        Name = 'Child records'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        TextIndent = 0
      end>
    Left = 112
    Top = 240
  end
end
