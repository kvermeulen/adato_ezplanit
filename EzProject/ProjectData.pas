unit ProjectData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB;

type
  TdmProjectData = class(TDataModule)
    cnnProject: TADOConnection;
    qrProjects: TADOTable;
    dsProjects: TDataSource;
    qrProjectsidProject: TAutoIncField;
    qrProjectsprDescription: TWideStringField;
    qrProjectsprStart: TDateTimeField;
    qrProjectsprStop: TDateTimeField;
    dsConstraintNames: TDataSource;
    qrConstraintNames: TADOTable;
    qrPriorities: TADOTable;
    dsPriorities: TDataSource;
    qrRelations: TADOTable;
    dsRelations: TDataSource;
    qrResources: TADOTable;
    dsResources: TDataSource;
    qrResourcesidResource: TAutoIncField;
    qrResourcesrsName: TWideStringField;
    qrIdentity: TADOQuery;
    qrStatusNames: TADOTable;
    dsStatusNames: TDataSource;
    qrVersion: TADOQuery;
    qrResourcesrsAllowTaskSwitching: TBooleanField;
    qrCapacities: TADOTable;
    dsCapacities: TDataSource;
    qrCapacitiesidCapacity: TAutoIncField;
    qrCapacitiescaDescription: TWideStringField;
    qrResourceCapacities: TADOQuery;
    qrResourceCapacitiesidResource: TIntegerField;
    qrResourceCapacitiesidCapacity: TIntegerField;
    qrResourceOperators: TADOQuery;
    qrResourceOperatorsType: TWideStringField;
    qrResourceOperatorsidOperator: TIntegerField;
    qrResourceOperatorsAllocationUnits: TFloatField;
    qrCapacitiescaMergeProfiles: TBooleanField;
    qrResourceTypes: TADOTable;
    dsResourceTypes: TDataSource;
    procedure qrResourcesBeforeDelete(DataSet: TDataSet);
    procedure qrProjectsBeforeDelete(DataSet: TDataSet);
    procedure cnnProjectAfterConnect(Sender: TObject);
    procedure cnnProjectBeforeConnect(Sender: TObject);
  private

  protected
    procedure CheckDatabaseVersion;
    function  GetPropValue(Key: Integer): string;
    procedure SetPropValue(Key: Integer; Value: string);

  public
    procedure AddProject;
    procedure EditProject;
    procedure AddResource;
    procedure EditResource;
    procedure DeleteCapacity(idCapacity: Integer);

    property DataSource: string index 0 read GetPropValue write SetPropValue;
  end;

var
  dmProjectData: TdmProjectData;

implementation

uses Main, Setup, ProjectProperties, ResourceProperties;

{$R *.DFM}

//
// Returns the value of a variable stored in a ';' separated string
//
function GetValue(Parse, Value: string) : string;
var
  p1, p2: integer;
  Lower: string;

begin
  Result := '';
  Lower := lowercase(Parse);
  Value := lowercase(Value);
  p1 := Pos(Value + '=', Lower);
  if p1 <> 0 then
  begin
    Parse := copy(Parse, p1 + length(Value) + 1, MaxInt);
    Lower := lowercase(Parse);
    p2 := Pos(';', Lower);
    if p2 = 0 then
      p2 := length(Lower)+1;

    Result := copy(Parse, 1, p2-1);
  end;
end;

//
// Sets the value of a variable stored in a ';' separated string
//
function SetValue(Parse, Key, Value: string) : string;
var
  p1, p2: integer;
  Lower: string;

begin
  Result := '';
  Lower := lowercase(Parse);
  Key := lowercase(Key);
  p1 := Pos(Key + '=', Lower);
  if p1 <> 0 then
  begin
    P2 := P1 + Length(Key) + 2;
    while (P2 < length(Parse)) and (Parse[P2] <> ';') do
      Inc(P2);

    Result := Copy(Parse, 1, P1 + length(Key)) + Value + Copy(Parse, P2, MaxInt);
  end
  else if Parse[length(Parse)] = ';' then
    Result := Parse + Key + '=' + Value + ';'
  else
    Result := Parse + ';' + Key + '=' + Value;
end;

procedure TdmProjectData.AddProject;
begin
  frmProjectProps.ProjectID := 0;
  frmProjectProps.ShowModal;
end;

procedure TdmProjectData.EditProject;
begin
  frmProjectProps.ProjectID := qrProjects['idProject'];
  frmProjectProps.ShowModal;
end;

procedure TdmProjectData.AddResource;
begin
  frmResourceProps.ResourceID := 0;
  frmResourceProps.ShowModal;
end;

procedure TdmProjectData.EditResource;
begin
  frmResourceProps.ResourceID := qrResources['idResource'];
  frmResourceProps.ShowModal;
end;

procedure TdmProjectData.CheckDatabaseVersion;
var
  DidMessageShow: Boolean;

  procedure ShowUpdateMessage;
  begin
    if not DidMessageShow then
      ShowMessage('Your database needs to be updated. EzProject will now perform the required changes automatically');
    DidMessageShow := True;
  end;

  procedure Update_to_1_0;
  begin
    ShowUpdateMessage;
    cnnProject.Execute('create table Version (Version VARCHAR (8), PRIMARY KEY(Version))');
    cnnProject.Execute('alter table resources add rsAllowTaskSwitching bit');
    cnnProject.Execute('Update resources set rsAllowTaskSwitching=false');
    cnnProject.Execute('alter table tasks add taOrder int, taExpanded bit');
    cnnProject.Execute('Update tasks set taOrder=0, taExpanded=false');
    cnnProject.Execute('insert into Version values(''1.0'')');
  end;

  procedure Update_to_1_1;
  begin
    ShowUpdateMessage;
    cnnProject.Execute('alter table projects add prActiveScale varchar(50), prActiveResourceScale varchar(50)');
    cnnProject.Execute('delete from Version');
    cnnProject.Execute('insert into Version values(''1.1'')');
  end;

  procedure Update_to_1_2;
  begin
    ShowUpdateMessage;
    cnnProject.Execute('alter table CalendarRules add crCount int, crRangeStart datetime, crRangeEnd datetime, crOccurrences int');
    cnnProject.Execute('delete from Version');
    cnnProject.Execute('insert into Version values(''1.2'')');
  end;

  procedure Update_to_1_3;
  begin
    ShowUpdateMessage;
    try
      cnnProject.Execute('alter table CalendarRules DROP CONSTRAINT ixResourceDate');
    except
    end;
    cnnProject.Execute('alter table CalendarRules add crCalendarType varchar(10), crID int');
    cnnProject.Execute('update CalendarRules set crCalendarType=''RESOURCE'', crID=cridResource');
    cnnProject.Execute('create index ix_CalendarTypeID on CalendarRules(crCalendarType, crID, crStart)');
    cnnProject.Execute('alter table CalendarRules DROP Column cridResource');
    cnnProject.Execute('delete from Version');
    cnnProject.Execute('insert into Version values(''1.3'')');
  end;

  procedure Update_to_1_4;
  begin
    ShowUpdateMessage;
    cnnProject.Execute('create table Capacities(idCapacity counter not null primary key, caDescription varchar(50))');
    cnnProject.Execute('create table ResourceCapacities(idResource int, idCapacity int, PRIMARY KEY(idResource, idCapacity))');
    cnnProject.Execute('alter table Tasks add taCapacity int');
    cnnProject.Execute('delete from Version');
    cnnProject.Execute('insert into Version values(''1.4'')');
  end;

  procedure Update_to_1_5;
  begin
    ShowUpdateMessage;
    cnnProject.Execute('create table Operators(idResource int not null, idOperator int not null, Type char not null, AllocationUnits double, PRIMARY KEY(idResource, idOperator, Type))');
    cnnProject.Execute('delete from Version');
    cnnProject.Execute('insert into Version values(''1.5'')');
  end;

  procedure Update_to_1_6;
  begin
    ShowUpdateMessage;
    cnnProject.Execute('alter table Capacities add caMergeProfiles bit');
    cnnProject.Execute('delete from Version');
    cnnProject.Execute('insert into Version values(''1.6'')');
  end;

  procedure Update_to_1_7;
  begin
    ShowUpdateMessage;
    cnnProject.Execute('create table TaskResourceTypes(idType counter not null primary key, trtDescription varchar(50))');
    cnnProject.Execute('insert into TaskResourceTypes(trtDescription) values(''fixed'')');
    cnnProject.Execute('insert into TaskResourceTypes(trtDescription) values(''by capacity'')');    
    cnnProject.Execute('alter table TasksResources add trType int');
    cnnProject.Execute('update TasksResources set trType=1');
    cnnProject.Execute('create table TaskResourceIntervals(idTaskResourceInterval counter not null primary key, idTaskResource int not null, triStart DateTime, triStop DateTime)');
    cnnProject.Execute('create index ix_TaskResourceIntervals_TaskResource on TaskResourceIntervals(idTaskResource, triStart)');
    cnnProject.Execute('delete from Version');
    cnnProject.Execute('insert into Version values(''1.7'')');
  end;

var
  Tables: TStringlist;

begin
  Tables := TStringlist.Create;
  try
    cnnProject.GetTableNames(Tables, False);

    // Update database to version 1.0
    if Tables.IndexOf('Version') = -1 then
      Update_to_1_0;

    qrVersion.Open;
    if qrVersion['Version'] < '1.1' then
      Update_to_1_1;

    if qrVersion['Version'] < '1.2' then
      Update_to_1_2;

    if qrVersion['Version'] < '1.3' then
      Update_to_1_3;

    if qrVersion['Version'] < '1.4' then
      Update_to_1_4;

    if qrVersion['Version'] < '1.5' then
      Update_to_1_5;

    if qrVersion['Version'] < '1.6' then
      Update_to_1_6;

    if qrVersion['Version'] < '1.7' then
      Update_to_1_7;

      // Perform any other updates if required....

    qrVersion.Close;

    if DidMessageShow then
      ShowMessage('Your database is up-to-date');

  finally
    Tables.Destroy;
  end;
end;

function TdmProjectData.GetPropValue(Key: Integer): string;
begin
  case Key of
    0: Result := GetValue(cnnProject.ConnectionString, 'Data Source');
  end;
end;

procedure TdmProjectData.SetPropValue(Key: Integer; Value: string);
begin
  case Key of
    0: cnnProject.ConnectionString := SetValue(cnnProject.ConnectionString, 'Data Source', Value);
  end;
end;

procedure TdmProjectData.qrResourcesBeforeDelete(DataSet: TDataSet);
var
  idResource: Integer;

begin
  MainForm.CloseChildren;
  with TADOQuery.Create(nil) do
  try
    idResource := qrResources['idResource'];
    Connection := cnnProject;
    cnnProject.BeginTrans;
    try
      Sql.Add('delete from Availability where avidResource=' + IntToStr(idResource));
      ExecSql;
      Sql.Clear;
      Sql.Add('delete from CalendarRules where cridResource=' + IntToStr(idResource));
      ExecSql;
      Sql.Clear;
      Sql.Add('delete from TasksResources where tridResource=' + IntToStr(idResource));
      ExecSql;
      cnnProject.CommitTrans;
    except
      cnnProject.RollbackTrans;
      Abort;
    end;
  finally
    Destroy;
  end;
end;

procedure TdmProjectData.qrProjectsBeforeDelete(DataSet: TDataSet);
var
  idProject: Integer;

begin
  idProject := qrProjects['idProject'];
  MainForm.CloseProjectWindow(idProject);
  with TADOQuery.Create(nil) do
  try
    Connection := cnnProject;
    cnnProject.BeginTrans;
    try
      Sql.Add('delete from TasksPredecessors where tpidTask in (select idTask from Tasks where taidProject=' + IntToStr(idProject) + ')');
      ExecSql;
      Sql.Clear;
      Sql.Add('delete from TasksResources where tridTask in (select idTask from Tasks where taidProject=' + IntToStr(idProject) + ')');
      ExecSql;
      Sql.Clear;
      Sql.Add('delete from Tasks where taidProject=' + IntToStr(idProject));
      ExecSql;
      cnnProject.CommitTrans;
    except
      cnnProject.RollbackTrans;
      Abort;
    end;
  finally
    Destroy;
  end;
end;

procedure TdmProjectData.cnnProjectBeforeConnect(Sender: TObject);
begin
  if not Assigned(MainForm) then
    raise Exception.Create('Connection should be activated after the mainform is created. Please set cnnProject.Connected to false to solve this problem.');
  DataSource := frmSetup.Database;
end;

procedure TdmProjectData.DeleteCapacity(idCapacity: Integer);
begin
  cnnProject.Execute('update Tasks set taCapacity=null where taCapacity=' + IntToStr(idCapacity));
  cnnProject.Execute('delete from ResourceCapacities where idCapacity=' + IntToStr(idCapacity));
  cnnProject.Execute('delete from Capacities where idCapacity=' + IntToStr(idCapacity));
end;

procedure TdmProjectData.cnnProjectAfterConnect(Sender: TObject);
begin
  CheckDatabaseVersion;

  // Projects table
  qrProjects.Open;

  // Resources table
  qrResources.Open;

  // Constraint names table
  qrConstraintNames.Open;

  // Status names table
  qrStatusNames.Open;
end;

end.
