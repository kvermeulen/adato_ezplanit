unit RandomTasks;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, db, ExtCtrls;

type
  TfrmRandomTasks = class(TForm)
    Label1: TLabel;
    edCount: TEdit;
    pbProgress: TProgressBar;
    Label2: TLabel;
    btnCreate: TButton;
    btnClose: TButton;
    Label3: TLabel;
    lbCount: TLabel;
    udCount: TUpDown;
    lbDuration: TLabel;
    Label5: TLabel;
    procedure btnCreateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRandomTasks: TfrmRandomTasks;

implementation

uses Main, GanttChart, EzMasks, ProjectData;

{$R *.DFM}

procedure TfrmRandomTasks.btnCreateClick(Sender: TObject);
var
  x, nChilds, nChild, n, ID, TaskKey, ParentKey: Integer;
  dsResources: TDataset;
  ResourceIDs, Resources: TList;
  Start: TDateTime;

begin
  ResourceIDs := TList.Create;
  Resources := TList.Create;

  with dmProjectData.qrResources do
  begin
    First;
    while not Eof do
    begin
      ResourceIDs.Add(Ptr(FieldByName('idResource').AsInteger));
      Next;
    end;
  end;

  Randomize;

  dsResources := (MainForm.ActiveMDIChild as TfrmGanttChart).cdsTaskResources;
  MainForm.EzDataset.DisableControls;
  pbProgress.Max := udCount.Position;
  Start := Now;

  with MainForm.EzDataset do
  try
    x := 0;
    while x < udCount.Position do
    begin
      Last;
      AppendRecord([]);
      Edit;
      ParentKey := FieldByName('idTask').AsInteger;
      FieldByName('taDescription').AsString := 'Task ' + IntToStr(ParentKey);
      Post;
      Update;

      nChilds := Random(5);
      nChild := 0;
      while (nChild < nChilds) and (x < udCount.Position) do
      begin
        Last;
        AppendRecord([]);
        Edit;
        TaskKey := FieldByName('idTask').AsInteger;
        FieldByName('taWork').AsFloat := (Random(10)+1) / 24;
        FieldByName('taDescription').AsString := 'Sub ' + IntToStr(ParentKey) + '-' + IntToStr(TaskKey);
        FieldByName('taidParent').AsInteger := ParentKey;
        Post;
        Update;

        Resources.Clear;
        for n := 0 to Random(3)-1 do
        begin
          ID := Integer(ResourceIDs[Random(ResourceIDs.Count)]);
          if Resources.IndexOf(Ptr(ID)) = -1 then
            Resources.Add(Ptr(ID));
        end;

        for n := 0 to Resources.Count-1 do
        begin
          dsResources.Insert;
          dsResources.FieldByName('tridTask').AsInteger := TaskKey;
          dsResources.FieldByName('tridResource').AsInteger := Integer(Resources[n]);
          dsResources.FieldByName('trAssigned').AsFloat := 1.0;
          dsResources.Post;
        end;
        inc(nChild);
        inc(x);
      end;
      inc(x);
      pbProgress.Position := x;
      lbCount.Caption := IntToStr(x);
      lbDuration.Caption := FormatValue('mm:ss', Now - Start, ftFloat);
      Update;
    end;
  finally
    EnableControls;
    ResourceIDs.Destroy;
    Resources.Destroy;
  end;
end;

end.
