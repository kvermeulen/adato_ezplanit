//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORMNS("AddMarker.pas", Addmarker, frmAddLineMarker);
USEFORMNS("Capacity.pas", Capacity, frmCapacity);
USEFORMNS("DateTimeSelector.pas", Datetimeselector, frmSelectDateTime);
USEFORMNS("FindTask.pas", Findtask, frmFindTask);
USEFORMNS("GanttChart.pas", Ganttchart, frmGanttChart);
USEFORMNS("MAIN.PAS", Main, MainForm);
USEFORMNS("PrintForm.pas", Printform, frmPrint);
USEFORMNS("ProjectData.pas", Projectdata, dmProjectData); /* TDataModule: File Type */
USEFORMNS("ProjectProperties.pas", Projectproperties, frmProjectProps);
USEFORMNS("RandomTasks.pas", Randomtasks, frmRandomTasks);
USEFORMNS("ResourceAvailability.pas", Resourceavailability, frmAvailability);
USEFORMNS("ResourceCalendar.pas", Resourcecalendar, frmResourceCalendar);
USEFORMNS("ResourceGantt.pas", Resourcegantt, frmResourceGantt);
USEFORMNS("ResourceProperties.pas", Resourceproperties, frmResourceProps);
USEFORMNS("Setup.pas", Setup, frmSetup);
USEFORMNS("SnapSetup.pas", Snapsetup, frmSnap);
USEFORMNS("Splash.pas", Splash, frmSplash);
USEFORMNS("StartPage.pas", Startpage, frmStartpage);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
		SetApplicationMainFormOnTaskBar(Application, true);
		Application->Title = "EzPlan-IT demo application";
		Application->CreateForm(__classid(TdmProjectData), &dmProjectData);
		Application->CreateForm(__classid(TMainForm), &MainForm);
		Application->CreateForm(__classid(TfrmSelectDateTime), &frmSelectDateTime);
		Application->CreateForm(__classid(TfrmResourceProps), &frmResourceProps);
		Application->CreateForm(__classid(TfrmProjectProps), &frmProjectProps);
		Application->CreateForm(__classid(TfrmRandomTasks), &frmRandomTasks);
		Application->CreateForm(__classid(TfrmFindTask), &frmFindTask);
		Application->CreateForm(__classid(TfrmSplash), &frmSplash);
		Application->CreateForm(__classid(TfrmCapacity), &frmCapacity);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
