unit StartPage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, EzDBGrid, StdCtrls, ActnList, Buttons,
  EzSpeedButton, ExtCtrls;

type
  TfrmStartpage = class(TForm)
    ActionList1: TActionList;
    acProjectProperties: TAction;
    acAddProject: TAction;
    acDeleteProject: TAction;
    acOpenProject: TAction;
    Panel1: TPanel;
    EzSpeedButton1: TEzSpeedButton;
    EzSpeedButton2: TEzSpeedButton;
    EzSpeedButton3: TEzSpeedButton;
    EzSpeedButton4: TEzSpeedButton;
    EzDBCoolGrid1: TEzDBCoolGrid;
    Panel2: TPanel;
    EzSpeedButton5: TEzSpeedButton;
    EzSpeedButton6: TEzSpeedButton;
    EzSpeedButton7: TEzSpeedButton;
    EzSpeedButton8: TEzSpeedButton;
    EzDBCoolGrid2: TEzDBCoolGrid;
    acResourceProperties: TAction;
    acAddResource: TAction;
    acDeleteResource: TAction;
    acResourceCalendar: TAction;
    EzSpeedButton9: TEzSpeedButton;
    acResourceAvailability: TAction;
    Label1: TLabel;
    Label2: TLabel;
    acProjectCalendar: TAction;
    EzSpeedButton10: TEzSpeedButton;
    acProjectCalendar_II: TAction;
    procedure acAddProjectExecute(Sender: TObject);
    procedure acProjectPropertiesExecute(Sender: TObject);
    procedure acProjectPropertiesUpdate(Sender: TObject);
    procedure acOpenProjectExecute(Sender: TObject);
    procedure acDeleteProjectExecute(Sender: TObject);
    procedure acResourcePropertiesExecute(Sender: TObject);
    procedure acResourcePropertiesUpdate(Sender: TObject);
    procedure acAddResourceExecute(Sender: TObject);
    procedure acDeleteResourceExecute(Sender: TObject);
    procedure acResourceCalendarExecute(Sender: TObject);
    procedure acResourceAvailabilityExecute(Sender: TObject);
    procedure acProjectCalendarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStartPage: TfrmStartPage;
  
implementation

uses ProjectData, Main, GanttChart, ResourceCalendar,
     ResourceAvailability
{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
;
{$ENDIF}

{$R *.dfm}

procedure TfrmStartpage.acAddProjectExecute(Sender: TObject);
begin
  dmProjectData.AddProject;
end;

procedure TfrmStartpage.acProjectPropertiesExecute(Sender: TObject);
begin
  dmProjectData.EditProject;
end;

procedure TfrmStartpage.acProjectPropertiesUpdate(Sender: TObject);
begin
  with dmProjectData.qrProjects do
    acProjectProperties.Enabled := Active and not Eof;
  acOpenProject.Enabled := acProjectProperties.Enabled;
  acDeleteProject.Enabled := acProjectProperties.Enabled;
  acProjectCalendar.Enabled := acProjectProperties.Enabled;
  acProjectCalendar_II.Enabled := acProjectProperties.Enabled;
end;

procedure TfrmStartpage.acOpenProjectExecute(Sender: TObject);
begin
  with dmProjectData.qrProjects do
    MainForm.CreateView(
          TfrmGanttChart,
          FieldByName('idProject').AsInteger,
          FieldByName('prDescription').AsString,
          FieldByName('prStart').AsDateTime,
          FieldByName('prStop').AsDateTime,
          True);
end;

procedure TfrmStartpage.acDeleteProjectExecute(Sender: TObject);
begin
  if MessageDlg(
      Format(SConfirmDeleteProject, [dmProjectData.qrProjects.FieldByName('prDescription').AsString]),
      mtConfirmation, mbOKCancel, 0) = idOK
  then
    dmProjectData.qrProjects.Delete;
end;

procedure TfrmStartpage.acResourcePropertiesExecute(Sender: TObject);
begin
  dmProjectData.EditResource;
end;

procedure TfrmStartpage.acResourcePropertiesUpdate(Sender: TObject);
begin
  with dmProjectData.qrResources do
    acResourceProperties.Enabled := Active and not Eof;
  acResourceCalendar.Enabled := acResourceProperties.Enabled;
  acDeleteResource.Enabled := acResourceProperties.Enabled;
end;

procedure TfrmStartpage.acAddResourceExecute(Sender: TObject);
begin
  dmProjectData.AddResource;
end;

procedure TfrmStartpage.acDeleteResourceExecute(Sender: TObject);
begin
  if MessageDlg(
    Format(SConfirmDeleteResource, [dmProjectData.qrResources.FieldByName('rsName').AsString]),
                mtConfirmation, mbOKCancel, 0) = idOK then
    dmProjectData.qrResources.Delete;
end;

procedure TfrmStartpage.acResourceCalendarExecute(Sender: TObject);
begin
  with TfrmResourceCalendar.Create(Self) do
  try
    CalendarType := 'RESOURCE';
    CalendarID := dmProjectData.qrResources.FieldByName('idResource').AsInteger;
    Open;
    ShowModal;
    Close;
  finally
    Destroy;
  end;
end;

procedure TfrmStartpage.acResourceAvailabilityExecute(Sender: TObject);
begin
  with TfrmAvailability.Create(Self) do
  try
    idResource := dmProjectData.qrResources.FieldByName('idResource').AsInteger;
    Open;
    ShowModal;
  finally
    Destroy;
  end;
end;

procedure TfrmStartpage.acProjectCalendarExecute(Sender: TObject);
begin
  with TfrmResourceCalendar.Create(Self) do
  try
    CalendarType := 'PROJECT';
    CalendarID := dmProjectData.qrProjects.FieldByName('idProject').AsInteger;
    Open;
    ShowModal;
    Close;
  finally
    Destroy;
  end;
end;

end.
