object frmSnap: TfrmSnap
  Left = 491
  Top = 208
  Caption = 'Snap settings'
  ClientHeight = 180
  ClientWidth = 332
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 32
    Width = 54
    Height = 13
    Caption = 'Snap every'
  end
  object rgSnapScale: TRadioGroup
    Left = 96
    Top = 32
    Width = 153
    Height = 137
    Caption = 'Scale'
    Columns = 2
    Items.Strings = (
      'Years'
      'Quarters'
      'Months'
      'Weeks'
      'Days'
      'Hours'
      'Minutes')
    TabOrder = 3
  end
  object edSnapCount: TEdit
    Left = 24
    Top = 48
    Width = 41
    Height = 21
    TabOrder = 1
    Text = '0'
  end
  object udSnapCount: TUpDown
    Left = 65
    Top = 48
    Width = 15
    Height = 21
    Associate = edSnapCount
    TabOrder = 2
  end
  object chkSnapActive: TCheckBox
    Left = 24
    Top = 8
    Width = 209
    Height = 17
    Caption = 'Activate snap function for gantt chart'
    TabOrder = 0
  end
  object btnCancel: TButton
    Left = 256
    Top = 144
    Width = 75
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object btnOK: TButton
    Left = 256
    Top = 112
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 4
  end
end
