object frmSetup: TfrmSetup
  Left = 484
  Top = 248
  BorderStyle = bsDialog
  Caption = 'EzProject setup'
  ClientHeight = 83
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 80
    Height = 13
    Caption = 'Project database'
  end
  object btnCancel: TButton
    Left = 151
    Top = 53
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 0
  end
  object btnOK: TButton
    Left = 229
    Top = 53
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button1: TButton
    Left = 280
    Top = 24
    Width = 21
    Height = 21
    Caption = '...'
    TabOrder = 2
    OnClick = Button1Click
  end
  object DBEdit1: TDBEdit
    Left = 8
    Top = 24
    Width = 273
    Height = 21
    DataField = 'Database'
    DataSource = dsSetupData
    TabOrder = 3
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Ms Access DB (*.mdb)|*.mdb'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 8
    Top = 48
  end
  object XmlSetupDataset: TEzArrayDataset
    AfterPost = XmlSetupDatasetAfterPost
    OnGetFieldValue = XmlSetupDatasetGetFieldValue
    OnGetRecordCount = XmlSetupDatasetGetRecordCount
    OnPostData = XmlSetupDatasetPostData
    Left = 44
    Top = 48
    object XmlSetupDatasetDocumentStorage: TStringField
      FieldName = 'Database'
      Size = 1024
    end
    object XmlSetupDatasetScalesSetup: TMemoField
      FieldName = 'ScalesSetup'
      BlobType = ftMemo
    end
    object XmlSetupDatasetWindowPosition: TStringField
      FieldName = 'WindowPosition'
      Size = 12
    end
  end
  object dsSetupData: TDataSource
    DataSet = XmlSetupDataset
    Left = 80
    Top = 48
  end
end
