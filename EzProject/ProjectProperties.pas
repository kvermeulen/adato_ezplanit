unit ProjectProperties;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, DBCtrls, Db, ADODB;

resourcestring
  SNoStartDate = 'A start date is required.';
  SNoEndDate = 'A end date is required.';
  SInvalidDates = 'The start date must lie before the end date.';

type
  TfrmProjectProps = class(TForm)
    DBText1: TDBText;
    Label1: TLabel;
    edStartDate: TDBEdit;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    edEndDate: TDBEdit;
    SpeedButton2: TSpeedButton;
    dsEditProject: TDataSource;
    Label3: TLabel;
    btnCancel: TButton;
    btnOK: TButton;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    qrProject: TADOQuery;
    BitBtn1: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
  private
  protected
    function  GetProjectID: Integer;
    procedure SetProjectID(Value: Integer);
  public
    property ProjectID: Integer read GetProjectID write SetProjectID;
  end;

var
  frmProjectProps: TfrmProjectProps;

implementation

uses ProjectData, Main, StartPage;

{$R *.DFM}

procedure TfrmProjectProps.SpeedButton1Click(Sender: TObject);
begin
  EditDateTimeField(dsEditProject.Dataset.FieldByName('prStart'));
end;

procedure TfrmProjectProps.SpeedButton2Click(Sender: TObject);
begin
  EditDateTimeField(dsEditProject.Dataset.FieldByName('prStop'));
end;

procedure TfrmProjectProps.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if ModalResult = idOK then
  begin
    CanClose := False;
    if edStartDate.Field.IsNull then
      raise Exception.Create(SNoStartDate);
    if edEndDate.Field.IsNull then
      raise Exception.Create(SNoEndDate);
    if edStartDate.Field.AsDateTime >= edEndDate.Field.AsDateTime then
      raise Exception.Create(SInvalidDates);

    if qrProject.Modified then
    begin
      qrProject.Post;
      dmProjectData.qrProjects.Requery;
    end;
  end;

  qrProject.Close;
  CanClose := True;
end;

function TfrmProjectProps.GetProjectID: Integer;
begin
  Result := qrProject.Parameters.ParamValues['idProject'];
end;

procedure TfrmProjectProps.SetProjectID(Value: Integer);
begin
  qrProject.Parameters.ParamValues['idProject'] := Value;
end;

procedure TfrmProjectProps.FormActivate(Sender: TObject);
begin
  if ProjectID=0 then
  begin
    Caption := 'Add project';
    qrProject.Open;
    qrProject.Insert;
  end
  else
  begin
    Caption := 'Edit project';
    qrProject.Open;
    qrProject.Edit;
  end;
end;

end.
