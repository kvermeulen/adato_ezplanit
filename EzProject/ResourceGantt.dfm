object frmResourceGantt: TfrmResourceGantt
  Left = 211
  Top = 252
  Caption = 'Resourse usage'
  ClientHeight = 316
  ClientWidth = 681
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 281
    Top = 0
    Height = 316
  end
  object EzGrid: TEzDBTreeGrid
    Left = 0
    Top = 0
    Width = 281
    Height = 316
    Align = alLeft
    BorderStyle = bsNone
    ColumnStyle = btsXPButton
    DataSource = DataSource2
    FixedCols = 1
    ScrollbarOptions.ScrollBars = ssHorizontal
    TabOrder = 0
    SchemeCollection = EzTreeGridSchemes1
    TitlebarHeight = 28
    Columns = <
      item
        Expanded = False
        FieldName = 'idResource'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'rsName'
        Width = 144
        Visible = True
        SchemeList = (
          0
          0)
      end
      item
        Expanded = False
        FieldName = 'taStart'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'taStop'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 284
    Top = 0
    Width = 397
    Height = 316
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object EzTimebar: TEzTimebar
      Left = 0
      Top = 0
      Width = 397
      Height = 30
      AutoUpdateMajorSettings = True
      ButtonStyle = btsXPButton
      Date = 36861.000000000000000000
      ScaleStorage = MainForm.tssGlobalScales
      ScaleIndex = 1
    end
    object EzGantt: TEzGanttChart
      Left = 0
      Top = 30
      Width = 397
      Height = 286
      Align = alClient
      BorderStyle = bsNone
      Color = clWhite
      DataSource = DataSource2
      DragCursor = 3
      GridLinePen.Color = clSilver
      HatchOptions.HatchPen.Color = clBlue
      HatchOptions.HatchBrush.Color = clBlue
      HatchOptions.HatchBrush.Style = bsBDiagonal
      MinDate = 36526.000000000000000000
      MaxDate = 36891.000000000000000000
      Ganttbars = <
        item
          BarHeight = 5
          BarFill.Color = 8454143
          Name = 'Task bar'
          TextLabel.Font.Charset = DEFAULT_CHARSET
          TextLabel.Font.Color = clWindowText
          TextLabel.Font.Height = -11
          TextLabel.Font.Name = 'MS Sans Serif'
          TextLabel.Font.Style = []
          StartField = 'TASTART'
          StopField = 'TASTOP'
          Visibility.EzDatalinkName = 'dsTasks'
          Visibility.ShowFor = [sfNormal, sfHidden, sfExpanded, sfCollapsed]
        end>
      LineMarkers.Lines = <>
      Options = [gaoAllwaysShowHiddens, gaoHorzLines, gaoConfirmDelete]
      PredecessorLine.Font.Charset = DEFAULT_CHARSET
      PredecessorLine.Font.Color = clWindowText
      PredecessorLine.Font.Height = -11
      PredecessorLine.Font.Name = 'MS Sans Serif'
      PredecessorLine.Font.Style = []
      Progressline.Options = [poPaintOnTop]
      Progressline.ProgressLineDate = 38866.395327141200000000
      ParentColor = False
      ScrollbarOptions.LeftExtend = 36861.000000000000000000
      ScrollbarOptions.RightExtend = 36861.000000000000000000
      TabOrder = 1
      Timebar = EzTimebar
    end
  end
  object DataSource2: TDataSource
    DataSet = EzDataSet
    Left = 184
    Top = 8
  end
  object EzDataSet: TEzDataset
    Cursors = <
      item
        CursorName = 'DefaultCursor'
      end
      item
        IndexFieldNames = 'trResource'
        CursorName = 'Name'
        Options = [cpSorted]
      end
      item
        IndexFieldNames = 'taDescription;trResource'
        CursorName = 'Description'
        Options = [cpSorted]
      end
      item
        IndexFieldNames = 'taStart;trResource'
        CursorName = 'Start date'
        Options = [cpSorted]
      end>
    EzDataLinks = <
      item
        DataSource = dsResources
        KeyField = 'idResource'
        FieldLinks = ()
      end
      item
        DataSource = dsTasks
        KeyField = 'tridResource;idTask'
        ParentRefField = 'tridResource'
        FieldLinks = (
          (
            'rsName'
            'taDescription'))
      end>
    AfterScroll = EzDataSetAfterScroll
    Left = 152
    Top = 8
    object EzDataSetidResource: TAutoIncField
      DisplayLabel = 'Resource'
      FieldName = 'idResource'
    end
    object EzDataSetrsName: TWideStringField
      DisplayLabel = 'Description'
      FieldName = 'rsName'
      Size = 50
    end
    object EzDataSetidTask: TIntegerField
      FieldName = 'idTask'
    end
    object EzDataSettaDescription: TStringField
      FieldName = 'taDescription'
    end
    object EzDataSettaStart: TDateTimeField
      DisplayLabel = 'Start'
      FieldName = 'taStart'
    end
    object EzDataSettaStop: TDateTimeField
      DisplayLabel = 'Stop'
      FieldName = 'taStop'
    end
  end
  object dsResources: TDataSource
    DataSet = qrResources
    Left = 120
    Top = 8
  end
  object dsTasks: TDataSource
    DataSet = qrTasks
    Left = 120
    Top = 40
  end
  object qrResources: TADOQuery
    Connection = dmProjectData.cnnProject
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select idResource, rsName from Resources '
      'order by rsName')
    Left = 80
    Top = 8
  end
  object qrTasks: TADOQuery
    Connection = dmProjectData.cnnProject
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idProject'
        Attributes = [paNullable]
        DataType = ftFixedChar
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'select idTask, tridResource, taConstraintDate, taDescription, ta' +
        'idConstraint, '
      
        'taidParent, taidProject, taidPriority, taProgress, taStart, taSt' +
        'op, taWork'
      
        'from Tasks ta inner join TasksResources tr on ta.idTask=tr.tridT' +
        'ask'
      'where taidProject=:idProject')
    Left = 80
    Top = 40
  end
  object EzTreeGridSchemes1: TEzTreeGridSchemes
    Schemes = <
      item
        Name = 'Hierarchy cell'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        TextIndent = 12
        ExpandButtons.Expand = 0
        ExpandButtons.Collapse = 1
        ExpandButtons.Grayed = 2
        HierarchyCell = True
      end>
    ButtonImages = ilButtonImages
    Left = 96
    Top = 112
  end
  object ilButtonImages: TImageList
    Height = 9
    Width = 9
    Left = 64
    Top = 112
    Bitmap = {
      494C010103000400040009000900FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000240000000900000001002000000000001005
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000080808000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF008080800080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000080808000FFFFFF00000000000000
      0000000000000000000000000000FFFFFF008080800080808000FFFFFF000000
      000000000000000000000000000000000000FFFFFF0080808000FFFFFF00FFFF
      FF00C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0C000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000080808000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF008080800080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C0C0C000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008080800080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080008080800080808000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424D3E000000000000003E00000028000000240000000900000001000100
      00000000480000000000000000000000000000000000000000000000FFFFFF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000}
  end
end
