unit AddMarker;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ComCtrls, StdCtrls, EzGantt, extctrls;

type
  TfrmAddLineMarker = class(TForm)
    edName: TEdit;
    Label1: TLabel;
    Button1: TButton;
    btnOK: TButton;
    dtpDate: TDateTimePicker;
    dtpTime: TDateTimePicker;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    Label3: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
  private
    procedure StartMouseIndicator;

  public
    Gantt: TEzGanttChart;
    IndicatorActive: Boolean;

  end;

implementation

uses Main;

{$R *.DFM}

procedure TfrmAddLineMarker.SpeedButton1Click(Sender: TObject);
begin
  StartMouseIndicator;
end;

procedure TfrmAddLineMarker.FormMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  ct: TWinControl;
  AControl: TControl;
  dtDate: TDateTime;
  P: TPoint;
  H: HWND;
  ScreenPos: TPoint;

begin
  ReleaseCapture;
  Show;

  ScreenPos := ClientToScreen(Point(X,Y));
  P := MainForm.ActiveMdiChild.ScreenToClient(ScreenPos);
  H := ChildWindowFromPoint(MainForm.ActiveMdiChild.Handle, P);
  ct := FindControl(H);

  repeat
    AControl := ct.ControlAtPos(ct.ScreenToClient(ScreenPos), True, True);
    if AControl is TWinControl then
      ct := AControl as TWinControl else
      ct := nil;
  until (ct is TEzGanttChart) or (ct=nil);

  if (ct is TEzGanttChart) then
  begin
    Gantt := ct as TEzGanttChart;
    P := ct.ScreenToClient(ScreenPos);
    with Gantt do
      dtDate := Timebar.X2DateTime(P.X);

    dtpDate.Date := trunc(dtDate);
    dtpTime.Time := Frac(dtDate);
  end else
    ShowMessage('Please click on a gantt chart to indicate a position');
end;

procedure TfrmAddLineMarker.FormCreate(Sender: TObject);
begin
  dtpDate.Date := Trunc(Now);
  dtpTime.Time := 0;
end;

procedure TfrmAddLineMarker.StartMouseIndicator;
begin
  IndicatorActive := True;
  SetCapture(Handle);
  Hide;
end;

end.
