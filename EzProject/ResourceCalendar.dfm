object frmResourceCalendar: TfrmResourceCalendar
  Left = 222
  Top = 114
  BorderWidth = 4
  Caption = 'Resource calendar'
  ClientHeight = 338
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 304
    Width = 700
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object btnOK: TButton
      Left = 496
      Top = 7
      Width = 99
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 601
      Top = 6
      Width = 99
      Height = 25
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Calendar: TEzCalendarPanel
    Left = 0
    Top = 0
    Width = 700
    Height = 304
    Align = alClient
    BorderWidth = 0
    BorderStyle = bsNone
    CalendarColor = clWindow
    Date = 39414.000000000000000000
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    RulesColor = clWindow
    OldCreateOrder = False
    OnRuleChanged = CalendarRuleChanged
    IsControl = True
  end
end
