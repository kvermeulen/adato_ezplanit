unit PrintForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, EzTimeBar, EzGantt, Grids, DBGrids, EzDBGrid, ExtCtrls, StdCtrls,
  EzPrinter, EzDataset, ComCtrls, Printers, CheckLst, ImgList, Buttons, EzGraph,
  EzScheduler;

type
  TfrmPrint = class(TForm)
    EzPrinter: TEzPrinter;
    btnPreview: TButton;
    EzGanttChart1: TEzGanttChart;                       
    EzTimebar1: TEzTimebar;
    dsPrint: TDataSource;
    btnOK: TButton;                              
    gbPrinter: TGroupBox;
    cbPrinter: TComboBox;
    btnProperties: TButton;
    PrinterSetup: TPrinterSetupDialog;
    gbRange: TGroupBox;
    gbCopies: TGroupBox;
    btnCancel: TButton;
    rbAll: TRadioButton;
    rbPages: TRadioButton;
    edPages: TEdit;
    imgNoCollate: TImage;
    imgCollate: TImage;
    chkCollate: TCheckBox;
    Label5: TLabel;
    edCopies: TEdit;
    udCopies: TUpDown;
    Label6: TLabel;
    Label7: TLabel;
    EzDBTreeGrid1: TEzDBTreeGrid;
    gbPageSetup: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    lbHPages: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    lbVPages: TLabel;
    lbTotalPages: TLabel;
    LegendPanel: TPanel;
    Shape1: TShape;
    Image1: TImage;
    Label1: TLabel;
    Image2: TImage;
    Label2: TLabel;
    Image3: TImage;
    Label3: TLabel;
    Image4: TImage;
    Label4: TLabel;
    rbDoFit: TRadioButton;
    rbDoScale: TRadioButton;
    pnlDoFit: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    edHPages: TEdit;
    udHPages: TUpDown;
    edVPages: TEdit;
    udVPages: TUpDown;
    pnlDoScale: TPanel;
    Label15: TLabel;
    edScalePercentage: TEdit;
    udScalePercentage: TUpDown;
    pnlPrintProgress: TPanel;
    Label10: TLabel;
    lblPrintStep: TLabel;
    lblPrintProgress: TLabel;
    Button1: TButton;
    TitlePanel: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label18: TLabel;
    GraphPanel: TPanel;
    GraphFiller: TPanel;
    ResourceGraphs: TEzGraph;
    StatisticsPanel: TPanel;
    StatisticsFiller: TPanel;
    StatisticsTimebar: TEzTimebar;
    StatisticsScaleDefinition: TEzTimescaleStorage;
    GroupBox1: TGroupBox;
    chkPrintGrid: TCheckBox;
    chkPrintGantt: TCheckBox;
    chkPrintHeader: TCheckBox;
    chkPrintFooter: TCheckBox;
    chkPrintGraphs: TCheckBox;
    chkPrintStatistics: TCheckBox;
    chkPrintTitle: TCheckBox;
    chkPrintLegend: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    procedure btnPreviewClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnPropertiesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbPrinterChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chkCollateClick(Sender: TObject);
    procedure edPagesChange(Sender: TObject);
    procedure edScalePercentageChange(Sender: TObject);
    procedure edHPagesExit(Sender: TObject);
    procedure edVPagesExit(Sender: TObject);
    procedure EzPrinterPrintProgress(Sender: TEzPrinter; Step: TEzPrintProgressStep; Progress,
      MaxProgress: Integer; var Abort: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure chkPrintFooterClick(Sender: TObject);
    procedure chkPrintGanttClick(Sender: TObject);
    procedure chkPrintGraphsClick(Sender: TObject);
    procedure chkPrintGridClick(Sender: TObject);
    procedure chkPrintHeaderClick(Sender: TObject);
    procedure chkPrintLegendClick(Sender: TObject);
    procedure chkPrintStatisticsClick(Sender: TObject);
    procedure chkPrintTitleClick(Sender: TObject);
    procedure EzPrinterBeforePrintPage(Sender: TEzPrinter; Range: TPrintRange;
        const Page: TPageCoord);
    procedure EzPrinterPreparePage(Sender: TEzPrinter; Range: TPrintRange; const
        Page: TPageCoord);
    procedure StatisticsTimebarDrawCell(Sender: TEzTimebar; Band: TEzTimescaleBand; ARect:
        TRect; ADate: TDateTime);
    procedure udHPagesClick(Sender: TObject; Button: TUDBtnType);
    procedure udVPagesClick(Sender: TObject; Button: TUDBtnType);
  private
    AbortPrinting: Boolean;
    FUpdateCount: Integer;

    // Schedule engine used by the gantt window being printed
    // This engine is used to calculate and display statistical data
    // about the resources used by the current project
    FScheduler: TEzScheduler;

    procedure ResetPageSettings(var ScalePercentage, HPages, VPages: Integer);
    procedure UpdatePageSettings(Percentage, HPages, VPages: Integer);
    procedure ResetDefaultPageSettings;

  public
    property Scheduler: TEzScheduler read FScheduler write FScheduler;
  end;

implementation

uses Math, EzPreview, EzCalendar, EzDateTime;

{$R *.DFM}

procedure TfrmPrint.FormCreate(Sender: TObject);
begin
  FUpdateCount := 0;
  cbPrinter.Items.Assign(Printer.Printers);
  cbPrinter.ItemIndex := Printer.PrinterIndex;

  EzPrinter.PageControls[2].Scaled := True;
  EzPrinter.PageControls[3].Scaled := True;
end;

procedure TfrmPrint.btnPreviewClick(Sender: TObject);
begin
  EzPrinter.PrintDate := Int(Now);
  EzPrinter.Preview;
end;

procedure TfrmPrint.btnOKClick(Sender: TObject);
begin
  try
    AbortPrinting := False;
    pnlPrintProgress.Visible := True;
    Screen.Cursor := crHourGlass;

    EzPrinter.PrintDate := Int(Now);
    EzPrinter.Copies := udCopies.Position;
    EzPrinter.Collate := chkCollate.Checked;

    if rbPages.Checked then
      EzPrinter.Pages := edPages.Text else
      EzPrinter.Pages := '';

    EzPrinter.Print;
  finally
    pnlPrintProgress.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmPrint.ResetDefaultPageSettings;
var
  Scale, HPages, VPages: Integer;

begin
  if FUpdateCount>0 then Exit;
  inc(FUpdateCount);
  try
    Scale := 100;
    HPages := -1;
    VPages := -1;
    ResetPageSettings(Scale, HPages, VPages);

    udScalePercentage.Position := 100;
    lbHPages.Caption := IntToStr(HPages);
    udHPages.Position := HPages;
    lbVPages.Caption := IntToStr(VPages);
    udVPages.Position := VPages;
    lbTotalPages.Caption := IntToStr(HPages * VPages);
  finally
    dec(FUpdateCount);
  end;
end;

procedure TfrmPrint.ResetPageSettings(var ScalePercentage, HPages, VPages: Integer);
var
  DpiX, DpiY: integer;

begin
  EzPrinter.ScaleToFit.HPages := HPages;
  EzPrinter.ScaleToFit.VPages := VPages;
  EzPrinter.ScaleToFit.Scale := ScalePercentage;

  EzPrinter.PrintRange.Reset(true, true);

  DpiX := GetDeviceCaps(Printer.Handle, LOGPIXELSX);
  DpiY := GetDeviceCaps(Printer.Handle, LOGPIXELSY);

  EzPrinter.Units := euPixels; // Screen pixels !!!

  with EzPrinter.PageSettings do
  begin
    PageWidth := MulDiv(GetDeviceCaps(Printer.Handle, PHYSICALWIDTH), 96, DpiX);
    PageLength := MulDiv(GetDeviceCaps(Printer.Handle, PHYSICALHEIGHT), 96, DpiY);
  end;

  EzPrinter.CalcPrintSize(EzPrinter.PrintRange);

  HPages := EzPrinter.PrintRange.Pages.HPages;
  VPages := EzPrinter.PrintRange.Pages.VPages;
  ScalePercentage := EzPrinter.ScaleToFit.Scale;
end;

procedure TfrmPrint.UpdatePageSettings(Percentage, HPages, VPages: Integer);
begin
  if FUpdateCount>0 then Exit;
  inc(FUpdateCount);
  try
    ResetPageSettings(Percentage, HPages, VPages);
    lbHPages.Caption := IntToStr(HPages);
    lbVPages.Caption := IntToStr(VPages);
    lbTotalPages.Caption := IntToStr(HPages * VPages);

    udHPages.Position := HPages;
    udVPages.Position := VPages;
    udScalePercentage.Position := Percentage;
  finally
    dec(FUpdateCount);  
  end;
end;

procedure TfrmPrint.udHPagesClick(Sender: TObject; Button: TUDBtnType);
begin
  if FUpdateCount>0 then Exit;

  try
    rbDoFit.Checked := True;
    UpdatePageSettings(100, udHPages.Position, udVPages.Position);
  except
    on E: EEzPrinterError do
    begin
      udHPages.Position := StrToInt(lbHPages.Caption);
      UpdatePageSettings(100, udHPages.Position, udVPages.Position);
      raise;
    end;
  end;
end;

procedure TfrmPrint.udVPagesClick(Sender: TObject; Button: TUDBtnType);
begin
  if FUpdateCount>0 then Exit;

  try
    rbDoFit.Checked := True;
    UpdatePageSettings(100, udHPages.Position, udVPages.Position);
  except
    on E: EEzPrinterError do
    begin
      udVPages.Position := StrToInt(lbVPages.Caption);
      UpdatePageSettings(100, udHPages.Position, udVPages.Position);
      raise;
    end;
  end;
end;

procedure TfrmPrint.btnPropertiesClick(Sender: TObject);
begin
  if PrinterSetup.Execute then
    ResetDefaultPageSettings;
end;

procedure TfrmPrint.Button1Click(Sender: TObject);
begin
  AbortPrinting := True;
end;

procedure TfrmPrint.cbPrinterChange(Sender: TObject);

  procedure SwitchPrinter(const APrtName : string);
  var
    Device,
      Driver,
      Port: array[0..200] of Char;
    DevMode: THandle;
  begin
    with Printer do
    begin
      PrinterIndex := Printers.IndexOf(APrtName);
      GetPrinter(Device, Driver, Port, DevMode);
      SetPrinter(Device, Driver, Port, 0);
    end;
  end;

begin
  SwitchPrinter(cbPrinter.Text);
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.FormShow(Sender: TObject);
begin
  (dsPrint.Dataset as TEzDataset).ExpandAll;

  // Calculate printsize at notmal size
  rbDoScale.Checked := True;
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkCollateClick(Sender: TObject);
begin
  imgNoCollate.Visible := not chkCollate.Checked;
  imgCollate.Visible := chkCollate.Checked;
end;

procedure TfrmPrint.chkPrintFooterClick(Sender: TObject);
begin
  if chkPrintFooter.Checked then
    EzPrinter.FooterHeight := 10 else
    EzPrinter.FooterHeight := 0;
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkPrintGanttClick(Sender: TObject);
begin
  if chkPrintGantt.Checked then
    EzPrinter.PrintGantt := EzGanttChart1 else
    EzPrinter.PrintGantt := nil;

  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkPrintGraphsClick(Sender: TObject);
begin
  EzPrinter.PageControls[2].Visible := chkPrintGraphs.Checked;
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkPrintGridClick(Sender: TObject);
begin
  if chkPrintGrid.Checked then
    EzPrinter.PrintGrid := EzDBTreeGrid1 else
    EzPrinter.PrintGrid := nil;

  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkPrintHeaderClick(Sender: TObject);
begin
  if chkPrintHeader.Checked then
    EzPrinter.HeaderHeight := 10 else
    EzPrinter.HeaderHeight := 0;
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkPrintLegendClick(Sender: TObject);
begin
  EzPrinter.PageControls[1].Visible := chkPrintLegend.Checked;
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkPrintStatisticsClick(Sender: TObject);
begin
  EzPrinter.PageControls[3].Visible := chkPrintStatistics.Checked;
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.chkPrintTitleClick(Sender: TObject);
begin
  EzPrinter.PageControls[0].Visible := chkPrintTitle.Checked;
  ResetDefaultPageSettings;
end;

procedure TfrmPrint.edPagesChange(Sender: TObject);
begin
  rbPages.Checked := true;
end;

procedure TfrmPrint.edHPagesExit(Sender: TObject);
begin
  try
    rbDoFit.Checked := True;
    UpdatePageSettings(100, udHPages.Position, udVPages.Position);
  except
    // Reset page settings
    UpdatePageSettings(100, StrToInt(lbHPages.Caption), udVPages.Position);
    ActiveControl := edHPages;
    raise;
  end;
end;

procedure TfrmPrint.edVPagesExit(Sender: TObject);
begin
  try
    rbDoFit.Checked := True;
    UpdatePageSettings(100, udHPages.Position, udVPages.Position);
  except
    // Reset page settings
    UpdatePageSettings(100, udHPages.Position, StrToInt(lbVPages.Caption));
    ActiveControl := edVPages;
    raise;
  end;
end;

procedure TfrmPrint.EzPrinterPrintProgress(Sender: TEzPrinter; Step: TEzPrintProgressStep;
  Progress, MaxProgress: Integer; var Abort: Boolean);
begin
  if pnlPrintProgress.Visible then
  begin
    Application.ProcessMessages;

    if AbortPrinting then
    begin
      Abort := True;
      Exit;
    end;

    if Step = psInitialization then
      lblPrintStep.Caption := 'Initialization' else
      lblPrintStep.Caption := 'Printing'; // psPrinting

    if MaxProgress >= 0 then
      lblPrintProgress.Caption := Format('%d of %d', [Progress, MaxProgress]) else
      lblPrintProgress.Caption := Format('%d', [Progress]);

    pnlPrintProgress.Update;
  end;
end;

procedure TfrmPrint.edScalePercentageChange(Sender: TObject);
begin
  if FUpdateCount>0 then Exit;
  rbDoScale.Checked := True;
  UpdatePageSettings(udScalePercentage.Position, -1, -1);
end;

procedure TfrmPrint.EzPrinterBeforePrintPage(
  Sender: TEzPrinter;
  Range: TPrintRange;
  const Page: TPageCoord);
var
  Reserved: Integer;

begin
  Reserved := Range.GridWidth;

  if Range.ShowSeperator then
    inc(Reserved, EzPrinter.SeperatorWidth);

  if chkPrintGraphs.Checked then
  begin
    if (Page.HPage = 0) and (Page.VPage = 0) then
      //
      // When printing the first page, update the graph scaling.
      // The scaling is set over the full date range
      //
    begin
      ResourceGraphs.UpdateStepSize(Range.StartDate, Range.EndDate);
    end;

    // Align the graph with the gantt chart
    GraphFiller.Width := Reserved;

    // Reposition filler on the left
    GraphFiller.Left := 0;

    ResourceGraphs.Width := Range.TimebarWidth;
  end;

  if chkPrintStatistics.Checked then
  begin
    // Align the statistics timebar with the gantt
    StatisticsFiller.Width := Reserved;

    // Reposition filler on the left
    StatisticsFiller.Left := 0;

    // Align statistics bar with content
    StatisticsTimebar.Width := Range.TimebarWidth;
    StatisticsTimebar.Date := Range.PageStartDate;
  end;
end;

procedure TfrmPrint.EzPrinterPreparePage(
  Sender: TEzPrinter;
  Range: TPrintRange;
  const Page: TPageCoord);

var
  IsBottomPage: Boolean;
  IsTopPage: Boolean;
begin
  IsTopPage := Page.VPage = 0;
  IsBottomPage := Page.VPage = (Range.Pages.VPages - 1);

  // Title panel
  EzPrinter.PageControls[0].Visible := chkPrintTitle.Checked and IsTopPage;

  // Legend panel
  EzPrinter.PageControls[1].Visible := chkPrintLegend.Checked and IsBottomPage;

  // Graph panel
  EzPrinter.PageControls[2].Visible := chkPrintGraphs.Checked and IsBottomPage;

  // Statistics panel
  EzPrinter.PageControls[3].Visible := chkPrintStatistics.Checked and IsBottomPage;
end;

procedure TfrmPrint.StatisticsTimebarDrawCell(
  Sender: TEzTimebar;
  Band: TEzTimescaleBand;
  ARect: TRect;
  ADate: TDateTime);

var
  Hours: Extended;
  Stop: TDateTime;
  i: Integer;
  Resource: TEzResource;
  Text: string;

begin
  if (Scheduler = nil) or (Scheduler.Resources.Count = 0) then
    Exit;

  if Band.Index = 0 then
    // Available hours
  begin
    Stop := AddTicks(Sender.MajorScale, Sender.MajorCount, ADate, 1);

    Hours := 0.0;
    for i := 0 to Scheduler.Resources.Count - 1 do
    begin
      Resource := Scheduler.Resources[i];
      if Resource.ActualAvailability = nil then
        // Resource is not used in this schedule
        continue;

      Hours := Hours + Scheduler.Resources[i].Availability.CalcWorkingHours(ADate, Stop);
    end;

    Text := IntToStr(Trunc(Hours * 24));
    DrawTextW(  Sender.Canvas.Handle,
                PWideChar(Text),
                -1,
                ARect,
                DT_CENTER);
  end
  else
    // Allocated hours
  begin
    Stop := AddTicks(Sender.MajorScale, Sender.MajorCount, ADate, 1);

    Hours := 0.0;
    for i := 0 to Scheduler.Resources.Count - 1 do
    begin
      Resource := Scheduler.Resources[i];

      if Resource.ActualAvailability = nil then
        // Resource is not used in this schedule
        continue;

      Hours :=  Hours +
                Resource.Availability.CalcWorkingHours(ADate, Stop) -
                Resource.ActualAvailability.CalcWorkingHours(ADate, Stop);
    end;

    Text := IntToStr(Trunc(Hours * 24));
    DrawTextW(  Sender.Canvas.Handle,
                PWideChar(Text),
                -1,
                ARect,
                DT_CENTER);
  end;
end;

end.
