program EzProject_D4;

uses
  Forms,
  Projects in 'Projects.pas' {frmOpenProject},
  ResourceGantt in 'ResourceGantt.pas' {frmResourceGantt},
  Main in 'MAIN.PAS' {MainForm},
  GanttChart in 'GanttChart.pas' {frmGanttChart},
  EZPILib_TLB in '..\..\EzComp\EZPILib_TLB.pas',
  SnapSetup in 'SnapSetup.pas' {frmSnap},
  PrintForm in 'PrintForm.pas' {frmPrint},
  AddMarker in 'AddMarker.pas' {frmAddLineMarker},
  Data in 'Data.pas' {dmProjectData: TDataModule},
  DateTimeSelector in 'DateTimeSelector.pas' {frmSelectDateTime},
  Resources in 'Resources.pas' {frmResources},
  ResourceCalendar in 'ResourceCalendar.pas' {frmResourceCalendar},
  ResourceAvailability in 'ResourceAvailability.pas' {frmAvailability},
  ResourceProperties in 'ResourceProperties.pas' {frmResourceProps},
  ProjectProperties in 'ProjectProperties.pas' {frmProjectProps},
  FindTask in 'FindTask.pas' {frmFindTask};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'EzPlan-IT demo application';
  Application.CreateForm(TdmProjectData, dmProjectData);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TfrmSelectDateTime, frmSelectDateTime);
  Application.CreateForm(TfrmOpenProject, frmOpenProject);
  Application.CreateForm(TfrmResources, frmResources);
  Application.CreateForm(TfrmResourceProps, frmResourceProps);
  Application.CreateForm(TfrmProjectProps, frmProjectProps);
  Application.CreateForm(TfrmFindTask, frmFindTask);
  Application.Run;
end.
