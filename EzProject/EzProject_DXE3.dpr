program EzProject_DXE3;

uses
  Forms,
  MAIN in 'MAIN.PAS' {MainForm},
  GanttChart in 'GanttChart.pas' {frmGanttChart},
  SnapSetup in 'SnapSetup.pas' {frmSnap},
  PrintForm in 'PrintForm.pas' {frmPrint},
  AddMarker in 'AddMarker.pas' {frmAddLineMarker},
  ProjectData in 'ProjectData.pas' {dmProjectData: TDataModule},
  DateTimeSelector in 'DateTimeSelector.pas' {frmSelectDateTime},
  ResourceCalendar in 'ResourceCalendar.pas' {frmResourceCalendar},
  ResourceAvailability in 'ResourceAvailability.pas' {frmAvailability},
  ResourceProperties in 'ResourceProperties.pas' {frmResourceProps},
  ProjectProperties in 'ProjectProperties.pas' {frmProjectProps},
  RandomTasks in 'RandomTasks.pas' {frmRandomTasks},
  FindTask in 'FindTask.pas' {frmFindTask},
  Setup in 'Setup.pas' {frmSetup},
  Splash in 'Splash.pas' {frmSplash},
  StartPage in 'StartPage.pas' {frmStartpage},
  ResourceGantt in 'ResourceGantt.pas' {frmResourceGantt},
  Capacity in 'Capacity.pas' {frmCapacity};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'EzPlan-IT demo application';
  Application.CreateForm(TdmProjectData, dmProjectData);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TfrmSelectDateTime, frmSelectDateTime);
  Application.CreateForm(TfrmResourceProps, frmResourceProps);
  Application.CreateForm(TfrmProjectProps, frmProjectProps);
  Application.CreateForm(TfrmRandomTasks, frmRandomTasks);
  Application.CreateForm(TfrmFindTask, frmFindTask);
  Application.CreateForm(TfrmSplash, frmSplash);
  Application.CreateForm(TfrmCapacity, frmCapacity);
  Application.Run;
end.
