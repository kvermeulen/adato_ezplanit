unit Capacity;

{$I Ez.inc}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls
{$IFDEF EZ_D6}
  , Variants
{$ENDIF}
  ;



type
  TfrmCapacity = class(TForm)
    edName: TEdit;
    Label1: TLabel;
    rgProfileMerging: TRadioGroup;
    Button1: TButton;
    Button2: TButton;

  public
    class procedure AddNew;
    class procedure Edit(id: Integer);
    class procedure Delete(id: Integer);
  end;

var
  frmCapacity: TfrmCapacity;

implementation

uses ProjectData;

{$R *.dfm}

{ TfrmCapacity }

class procedure TfrmCapacity.AddNew;
begin
  if frmCapacity=nil then
    frmCapacity := TfrmCapacity.Create(Application);

  frmCapacity.edName.Text := '';
  frmCapacity.rgProfileMerging.ItemIndex := 0;
  if frmCapacity.ShowModal = idOK then
  begin
    dmProjectData.qrCapacities.Insert;
    dmProjectData.qrCapacities['caDescription'] := frmCapacity.edName.Text;
    if frmCapacity.rgProfileMerging.ItemIndex = 0 then
      dmProjectData.qrCapacities['caMergeProfiles'] := False else
      dmProjectData.qrCapacities['caMergeProfiles'] := True;
    dmProjectData.qrCapacities.Post;
  end;
end;

class procedure TfrmCapacity.Delete(id: Integer);
begin
  dmProjectData.DeleteCapacity(id);
end;

class procedure TfrmCapacity.Edit(id: Integer);
begin
  dmProjectData.qrCapacities.Locate('idCapacity', id, []);
  frmCapacity.edName.Text := dmProjectData.qrCapacities['caDescription'];
  if dmProjectData.qrCapacities['caMergeProfiles'] = True then
    frmCapacity.rgProfileMerging.ItemIndex := 1 else
    frmCapacity.rgProfileMerging.ItemIndex := 0;

  if frmCapacity.ShowModal = idOK then
  begin
    dmProjectData.qrCapacities.Edit;
    dmProjectData.qrCapacities['caDescription'] := frmCapacity.edName.Text;
    if frmCapacity.rgProfileMerging.ItemIndex = 0 then
      dmProjectData.qrCapacities['caMergeProfiles'] := False else
      dmProjectData.qrCapacities['caMergeProfiles'] := True;
    dmProjectData.qrCapacities.Post;
  end;
end;

end.
