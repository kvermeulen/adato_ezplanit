program EzProject_D5;

uses
  Forms,
  ResourceGantt in 'ResourceGantt.pas' {frmResourceGantt},
  Main in 'MAIN.PAS' {MainForm},
  GanttChart in 'GanttChart.pas' {frmGanttChart},
  SnapSetup in 'SnapSetup.pas' {frmSnap},
  PrintForm in 'PrintForm.pas' {frmPrint},
  AddMarker in 'AddMarker.pas' {frmAddLineMarker},
  DateTimeSelector in 'DateTimeSelector.pas' {frmSelectDateTime},
  ResourceCalendar in 'ResourceCalendar.pas' {frmResourceCalendar},
  ResourceAvailability in 'ResourceAvailability.pas' {frmAvailability},
  ResourceProperties in 'ResourceProperties.pas' {frmResourceProps},
  ProjectProperties in 'ProjectProperties.pas' {frmProjectProps},
  Setup in 'Setup.pas' {frmSetup},
  FindTask in 'FindTask.pas' {frmFindTask},
  Splash in 'Splash.pas' {frmSplash},
  StartPage in 'StartPage.pas' {frmStartpage},
  ProjectData in 'ProjectData.pas' {dmProjectData: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'EzPlan-IT demo application';
  Application.CreateForm(TdmProjectData, dmProjectData);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TfrmSelectDateTime, frmSelectDateTime);
  Application.CreateForm(TfrmResourceProps, frmResourceProps);
  Application.CreateForm(TfrmProjectProps, frmProjectProps);
  Application.CreateForm(TfrmFindTask, frmFindTask);
  Application.CreateForm(TfrmSetup, frmSetup);
  Application.CreateForm(TfrmSplash, frmSplash);
  Application.Run;
end.
