object frmFindTask: TfrmFindTask
  Left = 334
  Top = 279
  Width = 437
  Height = 236
  BorderStyle = bsSizeToolWin
  Caption = 'Select a task'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 429
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    Color = 16767163
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 97
      Height = 13
      Caption = 'Enter your filter here:'
    end
    object edFilter: TEdit
      Left = 112
      Top = 4
      Width = 161
      Height = 21
      TabOrder = 0
      OnChange = edFilterChange
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 172
    Width = 429
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    Color = 16767163
    TabOrder = 2
    object btnCancel: TButton
      Left = 336
      Top = 2
      Width = 75
      Height = 25
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object btnOK: TButton
      Left = 256
      Top = 2
      Width = 75
      Height = 25
      Caption = '&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 30
    Width = 429
    Height = 142
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 4
    Caption = 'Panel3'
    Color = 16773864
    TabOrder = 1
    object EzDBCoolGrid1: TEzDBCoolGrid
      Left = 4
      Top = 4
      Width = 421
      Height = 134
      Align = alClient
      BorderStyle = bsNone
      ColumnStyle = btsXPButton
      DataSource = dsTasks
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitlebarHeight = 17
      Columns = <
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'idTask'
          Title.Alignment = taRightJustify
          Title.Caption = '#'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'taDescription'
          Title.Caption = 'Description'
          Width = 325
          Visible = True
        end>
    end
  end
  object dsTasks: TDataSource
    DataSet = cdsFindTask
    Left = 56
    Top = 104
  end
  object cdsFindTask: TClientDataSet
    Aggregates = <>
    Filtered = True
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ixAlphabetical'
        Fields = 'taDescription'
      end>
    Params = <>
    StoreDefs = True
    Left = 24
    Top = 102
  end
end
