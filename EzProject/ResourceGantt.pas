unit ResourceGantt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, EzDataSet, Grids, DBGrids, EzDBGrid,
  EzGantt, ExtCtrls, EzTimeBar, ADODB, Db, ImgList;

type
  TfrmResourceGantt = class(TForm)
    DataSource2: TDataSource;
    EzGrid: TEzDBTreeGrid;
    EzDataSet: TEzDataSet;
    Splitter1: TSplitter;
    Panel1: TPanel;
    EzTimebar: TEzTimebar;
    EzGantt: TEzGanttChart;
    EzDataSetidTask: TIntegerField;
    EzDataSettaDescription: TStringField;
    EzDataSettaStart: TDateTimeField;
    EzDataSettaStop: TDateTimeField;
    dsResources: TDataSource;
    dsTasks: TDataSource;
    qrResources: TADOQuery;
    qrTasks: TADOQuery;
    EzDataSetidResource: TAutoIncField;
    EzDataSetrsName: TWideStringField;
    EzTreeGridSchemes1: TEzTreeGridSchemes;
    ilButtonImages: TImageList;
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EzDataSetAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure qrTasksCalcFields(DataSet: TDataSet);

  private
    FAutoTaskScroll: Boolean;
    FDesc: string;

  protected
    function  GetActive: Boolean;
    function  GetIdProject: Integer;
    function  GetStartDate: TDateTime;
    function  GetStopDate: TDateTime;
    procedure SetActive(Value: Boolean);
    procedure SetDescription(Value : string);
    procedure SetIdProject(Value: Integer);
    procedure SetStartDate(Value: TDateTime);
    procedure SetStopDate(Value: TDateTime);

  public
    procedure ScrollToTask;

    property  Active: Boolean read GetActive write SetActive;
    property  AutoTaskScroll: Boolean read FAutoTaskScroll write FAutoTaskScroll;
    property  idProject: Integer read GetIdProject write SetIdProject;
    property  Description: string read FDesc write SetDescription;
    property  ProjectStart: TDateTime read GetStartDate write SetStartDate;
    property  ProjectStop: TDateTime read GetStopDate write SetStopDate;

  end;


implementation

uses Main, Menus, ProjectData;

{$R *.DFM}


procedure TfrmResourceGantt.Button2Click(Sender: TObject);
begin
  EzDataset.Open;
end;

function TfrmResourceGantt.GetActive: Boolean;
begin
  Result := EzDataset.Active;
end;

function TfrmResourceGantt.GetIdProject: Integer;
begin
  Result := qrTasks.Parameters.ParamValues['idProject'];
end;

procedure TfrmResourceGantt.SetActive(Value: Boolean);
begin
  if Value then
  begin
    EzDataset.Open;
    ScrollToTask;
  end
  else
    EzDataset.Close;
end;

//
// This procedure scrolls the Gantt chart in such a way that the taskbar,
// drawn for the current record, will be visible. This method is called
// automatically when AutoTaskScroll is set to True.
//
procedure TfrmResourceGantt.ScrollToTask;
var
  Start, Stop, EndDate, Current: TDateTime;
begin
  if not EzDataSet.FieldByName('taStart').IsNull then
    Start := EzDataSet.FieldByName('taStart').AsDateTime
  else
    Start := 0;

  if not EzDataSet.FieldByName('taStop').IsNull then
    Stop := EzDataSet.FieldByName('taStop').AsDateTime
  else
    Stop := 0;

  Current := EzGantt.Timebar.Date;
  EndDate := EzGantt.Timebar.X2DateTime(EzGantt.Timebar.Width);

  if ((Start <> 0) and (Start < Current)) or (Stop > EndDate) then
    EzGantt.Timebar.Date := Start;
end;

procedure TfrmResourceGantt.SetDescription(Value : string);
begin
  FDesc := Value;
  Caption := 'Resource usage: ''' + Value + '''';
end;

procedure TfrmResourceGantt.SetIdProject(Value: Integer);
begin
  qrTasks.Parameters.ParamValues['idProject'] := Value;
end;

function  TfrmResourceGantt.GetStartDate: TDateTime;
begin
  Result := EzGantt.MinDate;
end;

function  TfrmResourceGantt.GetStopDate: TDateTime;
begin
  Result := EzGantt.MaxDate;
end;

procedure TfrmResourceGantt.SetStartDate(Value: TDateTime);
begin
  EzGantt.MinDate := Value;
end;

procedure TfrmResourceGantt.SetStopDate(Value: TDateTime);
begin
  EzGantt.MaxDate := Value;
end;

procedure TfrmResourceGantt.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Active := False;
  Action := caFree;
end;

procedure TfrmResourceGantt.EzDataSetAfterScroll(DataSet: TDataSet);
begin
  if FAutoTaskScroll then
    ScrollToTask;
end;

procedure TfrmResourceGantt.FormCreate(Sender: TObject);
begin
  FAutoTaskScroll := True;
  // Setup the timebar
  //
  // We set scalestorage to point to the 'master' scale storage
  // located on the main form. This way, all windows having a
  // timscale use the same setup
  EzTimebar.ScaleStorage := MainForm.tssGlobalScales;
end;

procedure TfrmResourceGantt.FormActivate(Sender: TObject);
var
  X: Integer;
  MenuItem: TMenuItem;
begin
  with MainForm.ppSortMenu do
  begin
    Items.Clear;
    for X:=0 to EzDataset.Cursors.Count-1 do
    begin
      MenuItem := TMenuItem.Create(MainForm.ppSortMenu);
      with MenuItem do
      begin
        Caption := EzDataset.Cursors[X].CursorName;
        GroupIndex := 1;
        RadioItem := True;
        OnClick := MainForm.SortItemClick;

        if X = EzDataset.ActiveCursorIndex then
          Checked := True;
      end;
      Items.Add(MenuItem);
    end;
  end;
end;

procedure TfrmResourceGantt.qrTasksCalcFields(DataSet: TDataSet);
begin
  with qrTasks do
    if FieldByName('trResource').IsNull then
      FieldByName('ResourceName').AsString := '<no resource>' else
      FieldByName('ResourceName').AsString := FieldByName('trResource').AsString;
end;

end.
