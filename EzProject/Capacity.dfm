object frmCapacity: TfrmCapacity
  Left = 0
  Top = 0
  Caption = 'Capacity'
  ClientHeight = 177
  ClientWidth = 354
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 12
    Width = 71
    Height = 13
    Caption = 'Capacity name'
  end
  object edName: TEdit
    Left = 9
    Top = 31
    Width = 337
    Height = 21
    TabOrder = 0
  end
  object rgProfileMerging: TRadioGroup
    Left = 9
    Top = 67
    Width = 337
    Height = 54
    Caption = 'Profile merging'
    Items.Strings = (
      'Schedule against individual resources '
      'Merge resource profiles when scheduling against this capacity')
    TabOrder = 1
  end
  object Button1: TButton
    Left = 99
    Top = 144
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Button2: TButton
    Left = 180
    Top = 144
    Width = 75
    Height = 25
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
