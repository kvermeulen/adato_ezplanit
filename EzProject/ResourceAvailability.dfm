object frmAvailability: TfrmAvailability
  Left = 284
  Top = 246
  Caption = 'Availability graph'
  ClientHeight = 333
  ClientWidth = 440
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnlGraph: TPanel
    Left = 0
    Top = 26
    Width = 440
    Height = 213
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    Caption = 'pnlGraph'
    TabOrder = 0
    object EzGraph: TEzGraph
      Left = 5
      Top = 46
      Width = 430
      Height = 162
      Color = 16773345
      ColorNegative = 16751932
      BorderStyle = bsNone
      Lines = <
        item
          Description = 'Rules line'
          Visible = True
          Pen.Color = clNone
          Pen.Style = psClear
          Brush.Color = 7055683
          Tag = 0
        end
        item
          Description = 'Availability line'
          Visible = True
          CanEdit = True
          Pen.Color = clRed
          Brush.Style = bsClear
          Tag = 0
        end>
      MinDate = 36892.000000000000000000
      MaxDate = 37472.500917974500000000
      OnChanged = EzGraphChanged
      ScrollbarOptions.ScrollBars = ssHorizontal
      Xax.Position = 139
      Yax.Color = clBtnFace
      Yax.Font.Charset = DEFAULT_CHARSET
      Yax.Font.Color = clBlue
      Yax.Font.Height = -11
      Yax.Font.Name = 'MS Sans Serif'
      Yax.Font.Style = [fsBold]
      Yax.SnapSize = 0.250000000000000000
      Yax.Spacing = 25
      Yax.StepScaling = ssContinues
      Timebar = EzTimebar
      GridPen.Color = 13423280
      GridPen.Style = psDot
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
    object Panel2: TPanel
      Left = 5
      Top = 5
      Width = 430
      Height = 41
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel2'
      TabOrder = 1
      object EzTimebar: TEzTimebar
        Left = 20
        Top = 0
        Width = 410
        Height = 41
        AutoUpdateMajorSettings = True
        ButtonStyle = btsXPButton
        Align = alClient
        Date = 36892.000000000000000000
        ScaleStorage = MainForm.tssGlobalScales
        ScaleIndex = 1
        OnDateChange = EzTimebarDateChange
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 20
        Height = 41
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 239
    Width = 440
    Height = 94
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 8
    TabOrder = 1
    object btnOK: TButton
      Left = 357
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 357
      Top = 36
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object Panel7: TPanel
      Left = 24
      Top = 8
      Width = 305
      Height = 81
      BevelOuter = bvNone
      Color = clInfoBk
      TabOrder = 2
      object Label1: TLabel
        Left = 8
        Top = 6
        Width = 289
        Height = 65
        Caption = 
          '- Green bars indicate working periods of resource(s).'#13#10'- Red lin' +
          'e indicates availability profile for resource(s)'#13#10'- Drag line up' +
          ' or down to modify the number of resource units '#13#10'  available fo' +
          'r that particular interval.'#13#10'- Use shift + drag to select part o' +
          'f the availability profile line.'
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 440
    Height = 26
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object btnScrollPrev: TSpeedButton
      Left = 152
      Top = 2
      Width = 23
      Height = 22
      Hint = 'Move to previous point'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF3333333333333744333333333333F773333333333337
        44473333333333F777F3333333333744444333333333F7733733333333374444
        4433333333F77333733333333744444447333333F7733337F333333744444444
        433333F77333333733333744444444443333377FFFFFFF7FFFFF999999999999
        9999733777777777777333CCCCCCCCCC33333773FF333373F3333333CCCCCCCC
        C333333773FF3337F333333333CCCCCCC33333333773FF373F3333333333CCCC
        CC333333333773FF73F33333333333CCCCC3333333333773F7F3333333333333
        CCC333333333333777FF33333333333333CC3333333333333773}
      NumGlyphs = 2
      OnClick = btnScrollPrevClick
    end
    object btnScrollNext: TSpeedButton
      Left = 176
      Top = 2
      Width = 23
      Height = 22
      Hint = 'Move to next point'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333FF3333333333333447333333333333377FFF33333333333744473333333
        333337773FF3333333333444447333333333373F773FF3333333334444447333
        33333373F3773FF3333333744444447333333337F333773FF333333444444444
        733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
        999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
        33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
        333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
        33333777333333333333CC333333333333337733333333333333}
      NumGlyphs = 2
      OnClick = btnScrollNextClick
    end
    object dtpDateTime: TDateTimePicker
      Left = 28
      Top = 2
      Width = 113
      Height = 21
      Date = 38188.431899525470000000
      Time = 38188.431899525470000000
      TabOrder = 0
      OnChange = dtpDateTimeChange
    end
    object chkSnap: TCheckBox
      Left = 216
      Top = 5
      Width = 89
      Height = 17
      Caption = 'Snap to date'
      TabOrder = 1
      OnClick = chkSnapClick
    end
  end
end
