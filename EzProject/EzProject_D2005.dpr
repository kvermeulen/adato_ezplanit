program EzProject_D2005;

{%File 'ModelSupport\SnapSetup\SnapSetup.txvpck'}
{%File 'ModelSupport\FindTask\FindTask.txvpck'}
{%File 'ModelSupport\ProjectData\ProjectData.txvpck'}
{%File 'ModelSupport\AddMarker\AddMarker.txvpck'}
{%File 'ModelSupport\ResourceProperties\ResourceProperties.txvpck'}
{%File 'ModelSupport\PrintForm\PrintForm.txvpck'}
{%File 'ModelSupport\Splash\Splash.txvpck'}
{%File 'ModelSupport\GanttChart\GanttChart.txvpck'}
{%File 'ModelSupport\StartPage\StartPage.txvpck'}
{%File 'ModelSupport\DateTimeSelector\DateTimeSelector.txvpck'}
{%File 'ModelSupport\Main\Main.txvpck'}
{%File 'ModelSupport\ResourceAvailability\ResourceAvailability.txvpck'}
{%File 'ModelSupport\Setup\Setup.txvpck'}
{%File 'ModelSupport\ResourceGantt\ResourceGantt.txvpck'}
{%File 'ModelSupport\RandomTasks\RandomTasks.txvpck'}
{%File 'ModelSupport\ResourceCalendar\ResourceCalendar.txvpck'}
{%File 'ModelSupport\ProjectProperties\ProjectProperties.txvpck'}
{%File 'ModelSupport\default.txvpck'}

uses
  Forms,
  ResourceGantt in 'ResourceGantt.pas' {frmResourceGantt},
  MAIN in 'MAIN.PAS' {MainForm},
  GanttChart in 'GanttChart.pas' {frmGanttChart},
  SnapSetup in 'SnapSetup.pas' {frmSnap},
  PrintForm in 'PrintForm.pas' {frmPrint},
  AddMarker in 'AddMarker.pas' {frmAddLineMarker},
  ProjectData in 'ProjectData.pas' {dmProjectData: TDataModule},
  DateTimeSelector in 'DateTimeSelector.pas' {frmSelectDateTime},
  ResourceCalendar in 'ResourceCalendar.pas' {frmResourceCalendar},
  ResourceAvailability in 'ResourceAvailability.pas' {frmAvailability},
  ResourceProperties in 'ResourceProperties.pas' {frmResourceProps},
  ProjectProperties in 'ProjectProperties.pas' {frmProjectProps},
  RandomTasks in 'RandomTasks.pas' {frmRandomTasks},
  FindTask in 'FindTask.pas' {frmFindTask},
  Setup in 'Setup.pas' {frmSetup},
  Splash in 'Splash.pas' {frmSplash},
  StartPage in 'StartPage.pas' {frmStartpage};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'EzPlan-IT demo application';
  Application.CreateForm(TdmProjectData, dmProjectData);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TfrmSelectDateTime, frmSelectDateTime);
  Application.CreateForm(TfrmResourceProps, frmResourceProps);
  Application.CreateForm(TfrmProjectProps, frmProjectProps);
  Application.CreateForm(TfrmRandomTasks, frmRandomTasks);
  Application.CreateForm(TfrmFindTask, frmFindTask);
  Application.CreateForm(TfrmSetup, frmSetup);
  Application.CreateForm(TfrmSplash, frmSplash);
  Application.Run;
end.
