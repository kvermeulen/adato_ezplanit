unit EzParser;
{
  TEzParser is based on TKAParser, a formula
  parser designed by Vijainder K Thakur.

  Besides the TKAParser Vijainder also delivers
  a 'plug and play web converence' tool.

  You can visit his website at: http://kaorg.sawf.org
}

(*
  Syntax:
  0xABCD, 0ABCDh, $ABCD - Hex number
  0b0101, 01010b,       - Binary number
  90`15`2               - Degree
   Operators by priorities:
    {7  } () (BRACES)
    {  6} ** (POWER),
    {  5} ~ (INVERSE), ! (NOT),
    {  4} * (MUL), / (DIV), % (MOD), %% (PERSENT),
    {  3} + (ADD), - (SUB),
    {  2} < (LT), <= (LE), == (EQ), <> != (NE), >= (GE), > (GT),
    {  1} | (OR), ^ (XOR), & (AND),


  Single parameter double functions supported. ROUND, TRUNC, INT, FRAC,
  SIN, COS, TAN, ATAN, LN, EXP, SIGN, SGN, XSGN
  These functions take a double value as a parameter and return a double
  value.

  Single Parameter string functions supported. TRIM, LTRIM, RTRIM
  Thess functions take a string value as a parameter and return a string
  value

  Special functions supported. IF, EMPTY, LEN, AND, OR, CONCATENATE, CONCAT, REPL,
  LEFT, RIGHT, SUBSTR, COMPARE, ISNA, ROUND, NOT, EVALUATE
  Special functions use either a single parameter of type other than
  their return type, or work with multiple parameters).


*)

// Use of String in TNamedVar Structure
// Variables and functions are case sensitive

{$I Ez.inc}

interface

uses SysUtils, Classes, Windows, EzArrays;

const
  MaxDbl: Double = 1.7976931348623158e+308;

type
  TToken = (
    { } tkEOF, tkERROR, tkASSIGN,
    {7} tkLBRACE, tkRBRACE, tkNUMBER, tkIDENT, tkSEMICOLON,
    {6} tkPOW,
    {5} tkINV, tkNOT,
    {4} tkMUL, tkDIV, tkMOD, tkPER,
    {3} tkADD, tkSUB,
    {2} tkLT, tkLE, tkEQ, tkNE, tkGE, tkGT,
    {1} tkOR, tkXOR, tkAND, tkString
  );

  ECalculate = class(Exception);
  ECircular = class(Exception);
  TCalcType = (ctGetValue, ctSetValue, ctFunction);
  TExpType = (etString, etDouble, etBoolean, etUnknown);
  TValueType = (vtNummeric, vtString); 

  TCalcNoIdentEvent = procedure (Sender: TObject; ctype: TCalcType;
    const S: String; var Value: Double; var Handled: Boolean) of Object;
  TCalcNoStrIdentEvent = procedure (Sender: TObject; ctype: TCalcType;
    const S: String; var StrValue: String; var Handled: Boolean) of Object;
  TCalcGetRangeValuesEvent = procedure (Sender: TObject;
    const sRange, sVariable: string; ValueType: TValueType; ParamList: TStringList) of object;
  TCalcOnUserFunc = procedure (Sender: TObject; const Func: string; Params: TStringList;
    var Value: Double; var Handled: Boolean) of Object;
  TCalcOnStrUserFunc = procedure (Sender: TObject; const Func: string; Params: TStringList;
    var Value: string; var Handled: Boolean) of Object;

  TCalcCBProc =  function(ctype: TCalcType; const S: String;
      var Value: Double): Boolean of Object;
  TCalcCBStrProc = function(ctype: TCalcType; const S: String;
      var Value: String): Boolean of Object;

  PEvalResult = ^TEvalResult;
  TEvalResult = record
    Val,
    Res: Double;
    LogOp: String;
  end;

  PNamedVar = ^TNamedVar;
  TNamedVar = record
    Value: Double;
    Name: String;
    IsNA: Boolean;
    Dep: TList;
  end;

  // Cache mechanism
  PExpVal = ^TExpVal;
  TExpVal = record
    Key: string;
    Exp: string;
    Val: Double;
    StrVal: string;
    WasNA: Boolean;
  end;

  TExpressionCache = class(TEzBaseArray)
  protected
    function  CompareItems(var Item1, Item2): Integer; override;
    procedure CopyItems(PSource, PDest: Pointer; numItems: Integer); override;
    procedure FreeItems(PItems: Pointer; numItems: Integer); override;
    function  GetExpressions(index: Integer) : TExpVal;
    procedure PutExpressions(index: Integer; E: TExpVal);

  public

    property Expressions[Index: Integer]: TExpVal read GetExpressions write PutExpressions; default;
  end;

  PContext = ^TContext;
  TContext = record
    // For saving and restoring context
    OldPtr: PChar;
    OldLN: Integer;
    OldExp,
    OldSValue: String;
    OldToken: TToken;
    OldComplexResult: Boolean;
    OldKey: string;
  end;

  PNamedStrVar = ^TNamedStrVar;
  TNamedStrVar = record
    Value,
    Name: String;
    IsNA: Boolean;
    Dep: TList;
  end;

  TEzParser = class;

  TFormula = class
  private
    FExp,
    FName: String;
    FIsNA: Boolean;
    FValue: Double;
    FStrValue: String;
    FValid: Boolean;
    FEvaluated: Boolean;
    FCalc: TEzParser;
    // A parser can be dependent on both, double as well as
    // string variables.
    FDep,
    FStrDep,
    FFormDep: TList;
    function GetValue: Double;
    function GetStrValue: String;
  public
    constructor Create(Calc: TEzParser);
    destructor Destroy; override;
    property Exp: String read FExp write FExp;
    property Name: String read FName write FName;
    property IsNA: Boolean read FIsNA write FIsNA;
    property Value: Double read GetValue;
    property StrValue: String read GetStrValue;
  end;

  TEzParser = class(TComponent)
  private
    {$ifdef CALCTEST}
    T: TextFile;
    Line: Integer;
    {$endif}
    {$ifdef friendlymode}
    T2: TextFile;
    {$endif}
    ptr: PChar;
    lineno: Word;
    fvalue: Double;
    svalue : String;
    token: TToken;
    CalcProc: TCalcCBProc;
    StrCalcProc: TCalcCBStrProc;
    FMaxExpCache: Word;
    FRecFormula: TList;
    FRecording: Boolean;
    FOnNoIdent: TCalcNoIdentEvent;
    FOnNoStrIdent: TCalcNoStrIdentEvent;
    FOnGetRangeValues: TCalcGetRangeValuesEvent;
    FOnStrUserFunc: TCalcOnStrUserFunc;
    FOnUserFunc: TCalcOnUserFunc;
    FExpression, FExpressionKey: String;
    FVars,
    FStrVars,
    FFormulae: TList;
    FIsComplexResult: Boolean;
    FExpCache: TExpressionCache;
    FEvalList: TList;
    FDefaultStrVar,
    FDefaultVar,
    FDummyCalc: Boolean;
    FDefaultStrVarAs: String;
    FDefaultVarAs: Double;
    FResultWasString: Boolean;
    FResultWasNA: Boolean;
    FCaseSensitive: Boolean;
    FCacheResults: Boolean;

    // property access methods
    function  GetResult: Double;
    function GetStrResult: String;
    function _GetVar(const Name: String): Double; overload;
    function _GetVar(const Name: String; var Value: Double) : Boolean; overload;
    function GetVar(const Name: String): Double;
    function _GetStrVar(const Name: String): String;
    function GetStrVar(const Name: String): String;
    procedure SetVar(const Name: String; value: Double);
    procedure SetStrVar(const Name, value: String);
    function GetVarCount: Integer;
    function GetStrVarCount: Integer;
    function GetFormula(const Name: String): String;
    function GetFormulaeCount: Integer;
    procedure SetFormula(const Name, Value: String);
    procedure RaiseError(const Msg: String);
    function tofloat(B: Boolean): Double;
    // Lexical parser functions
    procedure lex;
    procedure start(var R: Double);
    procedure term (var R: Double);
    procedure expr6(var R: Double);
    procedure expr5(var R: Double);
    procedure expr4(var R: Double);
    procedure expr3(var R: Double);
    procedure expr2(var R: Double);
    procedure expr1(var R: Double);
    procedure ExpStr(var R: String);
    procedure StrTerm(var R: String);
    procedure StrStart(var R: String);
    function SaveContext: PContext;
    procedure RestoreContext(P: PContext);
    function ExecFunc(FuncName: String; ParamList: TStringList): Double;
    function ExecStrFunc(FuncName: String; ParamList: TStringList): String;
    function IsInternalFunction(FN: String): Boolean;
    function IsRangeFunction(FN: String): Boolean;
    function IsSpecialFunction(FN: String): Boolean;

    procedure RecordVar(const Name: String);
    procedure RecordStrVar(const Name: String);
    procedure DelFormDep(Formula: TFormula);
    // Sledgehammer deletes for use from Destroy only!
    procedure DeleteVars;
    procedure DeleteStrVars;
    procedure DeleteFormulae;
    // Formula and variable access methods
    function GetFormulaObj(const Name: String; var Index: Integer): TFormula; overload;
    function GetFormulaObj(const Name: String; var Value: Double) : Boolean; overload;
    function GetVarObj(const Name: String; var Index: Integer): PNamedVar;
    function GetStrVarObj(const Name: String; var Index: Integer): PNamedStrVar;
    function GetEvalVal(Eval: Double): Double;
    procedure DeleteEvals;
  protected
    // Default identifier handling for float calculations
    function DefCalcProc(ctype: TCalcType; const S: String;
      var V: Double): Boolean; virtual;
    // Default identifier handling for string calculations
    function DefStrCalcProc(ctype: TCalcType; const S: String;
      var V: String): Boolean; virtual;

    function  DoStrUserFunction(const Func: string; Params: TStringList) : string; virtual;
    function  DoUserFunction(const Func: string; Params: TStringList) : Double; virtual;
    function  DoGetIdentValue(ctype: TCalcType; const S: String): Double; overload;
    function  DoGetIdentValue(ctype: TCalcType; const S: String; var Value: Double) : Boolean; overload; virtual;
    function  DoGetStrIdentValue(ctype: TCalcType; const S: String): string; overload;
    function  DoGetStrIdentValue(ctype: TCalcType; const S: String; var Value: string) : Boolean; overload; virtual;
    procedure DoGetRangeValues(const sRange, sVariable: string; ValueType: TValueType; ParamList: TStringList); virtual;
    procedure GetRange(ParamList: TStringList);
    procedure GetStrRange(ParamList: TStringList);
    function IdentValue(ctype: TCalcType;
              const Name: String; var Res: Double): Boolean; virtual;
    function IdentStrValue(ctype: TCalcType;
              const Name: String; var Res: String): Boolean; virtual;
    procedure ParseFuncParams(const Func: string; ParamList: TStringList);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure ClearCache;
    function  NameOfVar(Index: Word): String;
    function  NameOfStrVar(Index: Word): String;
    function NameOfFormula(Index: Word): String;
    function GetFormulaValue(const Name: String): Double;
    function GetFormulaStrValue(const Name: String): String;
    procedure DeleteVar(const Name: String);
    procedure DeleteStrVar(const Name: String);
    procedure DeleteFormula(const Name: String);
    procedure SetVarNA(const Name: String; Val: Boolean);
    procedure SetStrVarNA(const Name: String; Val: Boolean);
    procedure SetFormulaNA(const Name: String; Val: Boolean);
    function IsVarNA(const Name: String): Boolean;
    function IsStrVarNA(const Name: String): Boolean;
    function IsFormulaNA(const Name: String): Boolean;
    // function GetExpType(Exp: String): TExpType;
    // String calculations
    function StrCalculate(const Formula: String;
      var R: String; Proc: TCalcCBStrProc): Boolean;
    // Float calculations
    function Calculate(const Formula: String;
      var R: Double; Proc: TCalcCBProc): Boolean;
    {$ifdef CALCTEST}
    procedure GetFormDep(const Name: String; Dep: TStringList);
    {$endif}
    // Added in support of the new Evaluate function vk:10/03/99
    procedure AddEvaluation(LogOp: String; Val, Res: Double);
    procedure RemoveEvaluation(LogOp: String; Val: Double);
    // End new
    procedure GetFormulaVarDep(const Name: String; Dep: TStringList);
    procedure GetFormulaStrVarDep(const Name: String; Dep: TStringList);
    procedure GetFormulaFormDep(const Name: String; Dep: TStringList);
    procedure GetVarDep(const Name: String; Dep: TStringList);
    procedure GetStrVarDep(const Name: String; Dep: TStringList);
    property Vars[const Name: String]: Double Read GetVar Write SetVar;
    property StrVars[const Name: String]: String Read GetStrVar Write SetStrVar;
    property Formula[const Name: String]: String read GetFormula write SetFormula;
    property VarCount: Integer read GetVarCount;
    property StrVarCount: Integer read GetStrVarCount;
    property FormulaeCount: Integer read GetFormulaeCount;
    property Result: Double Read GetResult;
    property StrResult: String Read GetStrResult;
  published
    // Properties
    property CaseSensitive: Boolean read FCaseSensitive write FCaseSensitive default true;
    property CacheResults: Boolean read FCacheResults write FCacheResults default true;
    property Expression: String Read FExpression Write FExpression;
    property ExpressionKey: String Read FExpressionKey Write FExpressionKey;
    property OnNoIdent: TCalcNoIdentEvent read FOnNoIdent write FOnNoIdent;
    property OnNoStrIdent: TCalcNoStrIdentEvent read FOnNoStrIdent
      write FOnNoStrIdent;
    property OnGetRangeValues: TCalcGetRangeValuesEvent read FOnGetRangeValues
      write FOnGetRangeValues;
    property OnStrUserFunc: TCalcOnStrUserFunc read FOnStrUserFunc
      write FOnStrUserFunc;
    property OnUserFunc: TCalcOnUserFunc read FOnUserFunc
      write FOnUserFunc;
    property DefaultVar: Boolean read FDefaultVar write FDefaultVar
      default False;
    property DefaultStrVar: Boolean read FDefaultStrVar
      write FDefaultStrVar default False;
    property DefaultVarAs: Double read FDefaultVarAs
      write FDefaultVarAs;
    property DefaultStrVarAs: String read FDefaultStrVarAs
      write FDefaultStrVarAs;
    property MaxExpCache: Word read FMaxExpCache
      write FMaxExpCache default 1000;
    property ResultWasNA: Boolean read FResultWasNA;      
    property ResultWasString: Boolean read FResultWasString write FResultWasString;

  end;

var
  FSpecialFunctions: TStringList;
  FInternalFunctions: TStringList;
  FRangeFunctions: TStringList;

// Utility math functions
function fmod(x, y: extended): extended;
function power(x, y: Double): Double;
function DegreeToStr(Angle: Extended): String;
function StrToDegree(const S: String): Extended;

implementation

uses
  EzStrings_2,
  Math
{$IFNDEF EZ_D2009}
  , CharInSetFunc
{$ENDIF}
  ;

function fmod(x, y: extended): extended;
begin
  Result := x - Int(x / y) * y;
end;

function power(x, y: Double): Double;
begin
  if (x = 0)
  then power := 1.0
  else power := Exp(Ln(x)*y);
end;

function DegreeToStr(Angle: extended): String;
var
  ang, min, sec: LongInt;
begin
  Result := '';
  if Abs(Angle) < 1E-20 then Angle := 0.;
  if Angle < 0 then begin
    Result := '-';
    Angle := -Angle;
  end;
  Angle := Angle * 180.0 / Pi; ang := Trunc(Angle+5E-10);
  Angle := (Angle - ang) * 60; min := Trunc(Angle+5E-10);
  Angle := (Angle - min) * 60; sec := Trunc(Angle+5E-10);
  Result := Result + IntToStr(ang)+'`';
  if min <> 0 then Result := Result + IntToStr(min);
  if sec <> 0 then Result := Result + '`' + IntToStr(sec);
end;

function StrToDegree(const S: String): Extended;
var
  ptr: PChar;
  frac, sign: Extended;
begin
  ptr := PChar(S);
  sign := 1;
  if CharInSet(ptr^, ['-', '+']) then begin
    if (ptr^ = '-') then sign := -sign;
    Inc(ptr);
  end;
  if not CharInSet(ptr^, ['0', '9'])
    then EConvertERROR.CreateFmt(SInvalidDegree, [S]);
  frac := 0;
  while CharInSet(ptr^, ['0'..'9']) do begin
    frac := frac * 10 + (Ord(ptr^) - Ord('0'));
    Inc(ptr);
  end;
  Result := frac;
  if (ptr^ = '`') then begin
  Result := Result * Pi / 180.0;
  Inc(ptr); frac := 0;
  while CharInSet(ptr^, ['0'..'9']) do begin
    frac := frac * 10 + (Ord(ptr^) - Ord('0'));
    Inc(ptr);
  end;
  Result := Result + (frac * Pi / 180.0 / 60);
  if (ptr^ = '`') then begin
  Inc(ptr); frac := 0;
  while CharInSet(ptr^, ['0'..'9']) do begin
    frac := frac * 10 + (Ord(ptr^) - Ord('0'));
    Inc(ptr);
  end;
  Result := Result + (frac * Pi / 180.0 / 60 / 60);
  end;
  end;
  Result := Sign * fmod(Result, 2*Pi);
end;

{-------------------------- TExpressionCache ----------------------- }
function TExpressionCache.CompareItems(var Item1, Item2): Integer;
var
  i1: TExpVal absolute Item1;
  i2: TExpVal absolute Item2;

begin
  if i1.Key < i2.Key then
    Result := -1
  else if i1.Key > i2.Key then
    Result := 1
  else if i1.Exp < i2.Exp then
    Result := -1
  else if i1.Exp > i2.Exp then
    Result := 1
  else
    Result := 0;
end;

procedure TExpressionCache.CopyItems(PSource, PDest: Pointer; numItems: Integer);
begin
  if numItems > 1 then
    Exit;
  PExpVal(PDest)^ := PExpVal(PSource)^;
end;

procedure TExpressionCache.FreeItems(PItems: Pointer; numItems: Integer);
begin
  Finalize(PExpVal(PItems)^, numItems);
end;

function TExpressionCache.GetExpressions(index: Integer) : TExpVal;
begin
  GetItem(index, Result);
end;

procedure TExpressionCache.PutExpressions(index: Integer; E: TExpVal);
begin
  inherited PutItem(index, E);
end;

{
function CompareExpressions(P: PExpVal; const Key, Exp: string) : integer;
begin
  if P^.Key = Key then
  begin
    if P^.Exp < Exp then
      Result := -1
    else if P^.Exp > Exp then
      Result := 1
    else
      Result := 0;
  end
  else
  begin
    if P^.Key < Key then
      Result := -1 else
      Result := 1;
  end;
end;

constructor TExpressionCache.Create(itemcount, Grow: Integer);
begin
  inherited Create;
  FMemory := nil;
  FCapacity := 0;
  FCount := 0;
  FGrowBy := Grow;
  SetCapacity(itemcount);
end;

destructor TExpressionCache.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TExpressionCache.Add(E: TExpVal);
begin
  Add(E.Key, E.Exp, E.WasNA, E.Val);
end;

procedure TExpressionCache.Add(const Key, Exp: string; WasNA: Boolean; const Val: Double);
var
  Pos: Integer;
  P: PExpVal;

begin
  if FCount = FCapacity then SetCapacity(FCapacity+FGrowBy);

  FindExp(Pos, Key, Exp);

  if Pos < FCount then
  try
    inc(FCount);
    MoveMemory(GetIndexPtr(Pos+1), GetIndexPtr(Pos), (FCount - 1 - Pos) * SizeOf(TExpVal));
  except
    InternalHandleException;
  end else
    Inc(FCount);

  P := GetIndexPtr(Pos);
  Pointer(P^.Key) := nil;
  Pointer(P^.Exp) := nil;
  P^.Key := Key;
  P^.Exp := Exp;
  P^.Val := Val;
  P^.WasNA := WasNA;
end;

function TExpressionCache.FindExp(var Index: Integer; Key, Exp: string): Boolean;
var
  L, H, I, C: Integer;

begin
  Result := False;
  L := 0;
  H := Count - 1;
  while (L <= H) do
  begin
    I := (L + H) shr 1;
    C := CompareExpressions(GetIndexPtr(I), Key, Exp);
    if (C < 0) then
      L := I + 1
    else
    begin
      H := I - 1;
      if (C = 0) then
        Result := True;
    end;
  end;
  Index := L;
end;

function TExpressionCache.GetIndexPtr(index: Integer): Pointer;
begin
  Result := nil;
  if ValidIndex(index) then
    Result := Ptr(LongInt(FMemory) + (index*SizeOf(TExpVal)));
end;

function TExpressionCache.GetExpressions(index: Integer) : TExpVal;
begin
  Result := TExpVal(GetIndexPtr(index)^);
end;

procedure TExpressionCache.InternalHandleException;
begin
  Clear;
  raise Exception.Create('general error in TExpressionCache.');
end;

procedure TExpressionCache.FreeItems(PItems: Pointer; numItems: Integer);
begin
  Finalize(PItems, numItems);
end;

procedure TExpressionCache.PutExpressions(index: Integer; E: TExpVal);
begin
  if ValidIndex(index) then
  begin
    Delete(index);
    Add(E);
  end;
end;

procedure TExpressionCache.SetCapacity(NewCapacity: Integer);
begin
  ReallocMem(FMemory, NewCapacity * SizeOf(TExpVal));
  FCapacity := NewCapacity;
end;

procedure TExpressionCache.SetCount(NewCount: Integer);
begin
  if (NewCount < 0) or (NewCount > MaxListSize) then
    raise Exception.Create('ArrayIndexError');
  if (NewCount > FCapacity) then
    SetCapacity(NewCount);
  if (NewCount > FCount) then
    FillMemory(Ptr(LongInt(FMemory) + (FCount*SizeOf(PExpVal))), (NewCount - FCount) * SizeOf(PExpVal), 0);
  FCount := NewCount;
end;

function TExpressionCache.ValidIndex(Index: Integer): Boolean;
begin
  Result := True;
  if (Index < 0) or (Index >= FCount) then
  begin
    raise Exception.Create('ArrayIndexError');
    Result := False;
  end
end;
}

{-------------------------- TFormula ----------------------- }

constructor TFormula.Create(Calc: TEzParser);
begin
  inherited Create;
  FDep := TList.Create;
  FStrDep := TList.Create;
  FFormDep := TList.Create;
  FCalc := Calc;
end;

destructor TFormula.Destroy;
begin
  FDep.Clear;
  FDep.Free;
  FStrDep.Free;
  FFormDep.Clear;
  FFormDep.Free;
  inherited Destroy;
end;

function TFormula.GetValue: Double;
var
  SavedContext: PContext;
  WasRecording: Boolean;
  I: Integer;
begin
  if FValid then
  begin
    if FCalc.FRecording then
    begin
      // Add this formula as a dependency of the formula(e) being parsed.
      for I := 0 to FCalc.FRecFormula.Count - 1 do
        TFormula(FCalc.FRecFormula[I]).FFormDep.Add(Self);
      // Add the formula's dependencies to those of the formulae
      // being evaluated
      for I := 0 to FDep.Count - 1 do
        FCalc.RecordVar(PNamedVar(FDep[I]).Name);
      for I := 0 to FStrDep.Count - 1 do
        FCalc.RecordStrVar(PNamedStrVar(FStrDep[I]).Name);
    end;
    Result := FValue;
  end
  else
  begin
    if not FEvaluated then
    begin
      WasRecording := FCalc.FRecording;
      // if this formula participates in another formula's expression
      // add it to the formula dependency list of the other formula
      if WasRecording then
      begin
        for I := 0 to FCalc.FRecFormula.Count - 1 do
        begin
          // Modification to preclude recursion vkt:01/04/00
          // Check to see if the formula is already in the FRecFormula list
          if FCalc.FRecFormula[I] = Self then
            raise ECircular.Create(SCircularDependency + ': ' + Name);
          // End Modification
          TFormula(FCalc.FRecFormula[I]).FFormDep.Add(Self);
        end;
      end;
      FCalc.FRecording := True;
      FCalc.FRecFormula.Add(Self);
      SavedContext := FCalc.SaveContext;
      FCalc.FExpression := FExp;
      FValue := FCalc.Result;
      FEvaluated := True;
      FCalc.RestoreContext(SavedContext);
      FValid := True;
      Result := FValue;
      if not WasRecording then
        FCalc.FRecording := False;
      FCalc.FRecFormula.Delete(FCalc.FRecFormula.Count - 1);
    end
    else
    begin
      SavedContext := FCalc.SaveContext;
      FCalc.FExpression := FExp;
      FValue := FCalc.Result;
      FValid := True;
      FCalc.RestoreContext(SavedContext);
      Result := FValue;
 //    if FCalc.FDoRecalc then
 //    FCalc.FDoRecalc := False;
    end;
  end;
end;

function TFormula.GetStrValue: String;
var
  SavedContext: PContext;
  WasRecording: Boolean;
  I: Integer;
begin
  if FValid then
  begin
    if FCalc.FRecording then
    begin
      // Add this formula as a dependency of the formula(e) being parsed.
      for I := 0 to FCalc.FRecFormula.Count - 1 do
        TFormula(FCalc.FRecFormula[I]).FFormDep.Add(Self);
      // Add the formula's dependencies to those of the formulae
      // being evaluated
      for I := 0 to FDep.Count - 1 do
        FCalc.RecordVar(PNamedVar(FDep[I]).Name);
      for I := 0 to FStrDep.Count - 1 do
        FCalc.RecordStrVar(PNamedStrVar(FStrDep[I]).Name);
    end;
    Result := FStrValue;
  end
  else
  begin
    if not FEvaluated then
    begin
      WasRecording := FCalc.FRecording;
      // if this formula participates in another formula's expression
      // add it to the formula dependency list of the other formula
      if WasRecording then
      begin
        for I := 0 to FCalc.FRecFormula.Count - 1 do
        begin
          // Modification to preclude recursion vkt:01/04/00
          // Check to see if the formula is already in the FRecFormula list
          if FCalc.FRecFormula[I] = Self then
            raise ECircular.Create(SCircularDependency + ': ' + Name);
          // End Modification
          TFormula(FCalc.FRecFormula[I]).FFormDep.Add(Self);
        end;
      end;
      FCalc.FRecording := True;
      FCalc.FRecFormula.Add(Self);
      SavedContext := FCalc.SaveContext;
      FCalc.FExpression := FExp;
      FStrValue := FCalc.StrResult;
      FCalc.RestoreContext(SavedContext);
      Result := FStrValue;
      FValid := True;
      FEvaluated := True;
      if not WasRecording then
        FCalc.FRecording := False;
      FCalc.FRecFormula.Delete(FCalc.FRecFormula.Count - 1);
    end
    else
    begin
      SavedContext := FCalc.SaveContext;
      FCalc.FExpression := FExp;
      FStrValue := FCalc.StrResult;
      FValid := True;
      FCalc.RestoreContext(SavedContext);
      Result := FStrValue;
    end;
  end;
end;


{------------------------- TEzParser ----------------------- }

procedure TEzParser.ClearCache;
begin
  FExpCache.Clear;
end;

procedure TEzParser.GetRange(ParamList: TStringList);
var
  slTemp: TStringList;
  x, P1, P2: integer;
  s, Temp: string;
  SavedContext: PContext;

begin
  SavedContext := SaveContext;
  slTemp := TStringList.Create;
  x := 0;

  try
    while x < ParamList.Count do
    begin
      s := ParamList[x];

      // is parameter a function (i.e. max(x,y,z) or childs(x))
      P1 := Pos('(', s);
      if P1 <> 0 then
      begin
        Temp := UpperCase(Trim(Copy(s, 1, P1-1)));
        // range function encoutered
        if IsRangeFunction(Temp) then
        begin
          P2 := Pos(')', s);
          if P2 = 0 then
            RaiseError(SBraceError+' "'+s+'"');
          slTemp.Clear;
          DoGetRangeValues(Temp, Trim(Copy(s, P1+1, P2-P1-1)), vtNummeric, slTemp);
          ParamList.Delete(x);
          ParamList.AddStrings(slTemp);
          // don't call inc(x);
        end
        else
        begin
          FExpression := ParamList[x];
          ParamList[x] := FloatToStr(Self.Result);
          inc(x);
        end;
      end
      else
      begin
        FExpression := ParamList[x];
        ParamList[x] := FloatToStr(Self.Result);
        inc(x);
      end;
    end;
  finally
    RestoreContext(SavedContext);
    slTemp.Destroy;
  end;
end;

function TEzParser.ExecFunc(FuncName: String; ParamList: TStringList): Double;
var
  I: Integer;
  SavedContext: PContext;
  Temp: String;

begin
  Result := 0.0;
  SavedContext := nil;
  try
    if FuncName = 'IF' then
    begin
      if ParamList.Count <> 3 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Result := Self.Result;
      if Result <> 0 then
      begin
        FExpression := ParamList.Strings[1];
        Result := Self.Result;
        // Ensure dependency list gets built correctly
        if FRecording then
        begin
          FExpression := ParamList.Strings[2];
          FDummyCalc := True;
          try
            Self.Result;
          finally
            FDummyCalc := False;
          end;
        end;
      end
      else
      begin
        FExpression := ParamList.Strings[2];
        Result := Self.Result;
        // Ensure dependency list gets built correctly
        if FRecording then
        begin
          FExpression := ParamList.Strings[1];
          Self.Result;
        end;
      end;
    end
    else if FuncName = 'LEN' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Result := Length(Self.StrResult);
    end
    else if FuncName = 'EMPTY' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      {ECO: the whole word could be spaces...}
      if Length(Trim(StrResult)) > 0 then
        Result := 0.0 else
        Result := 1.0;
    end
    else if FuncName = 'COMPARE' then
    begin
      if ParamList.Count <> 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Temp := StrResult;
      FExpression := ParamList.Strings[1];
      if CompareStr(Temp, StrResult) = 0 then
        Result := 1.0 else
        Result := 0.0;
    end
    else if FuncName = 'COMPARESTR' then
    begin
      if ParamList.Count <> 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Temp := StrResult;
      FExpression := ParamList.Strings[1];
      Result := CompareStr(Temp, StrResult);
    end
    else if FuncName = 'COMPARETEXT' then
    begin
      if ParamList.Count <> 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Temp := StrResult;
      FExpression := ParamList.Strings[1];
      Result := CompareText(Temp, StrResult);
    end
    // vk:10/03/99
    else if FuncName = 'EVALUATE' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Result := GetEvalVal(Self.Result);
    end
    else if FuncName = 'AND' then
    // Returns a 0.0 for false and a value greater than 1.0 for True;
    begin
      if ParamList.Count < 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      Result := 1.0;
      for I := 0 to ParamList.Count - 1 do
      begin
        FExpression := ParamList.Strings[I];
        Result := Result * Self.Result;
      end;
      if Result > 0.0 then
        Result := 1.0;
    end
    else if FuncName = 'OR' then
    // Returns a 0.0 for false and a value greater than 1.0 for true;
    begin
      if ParamList.Count < 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      Result := 0.0;
      for I := 0 to ParamList.Count - 1 do
      begin
        FExpression := ParamList.Strings[I];
        Result := Result + Self.Result;
      end;
      if Result > 0.0 then
        Result := 1.0;
    end
    else if FuncName = 'ROUND' then
    begin
      if ParamList.Count <> 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Result := Self.Result;
      FExpression := ParamList.Strings[1];
      I := Round(Self.Result);
      Result := StrToFloat(Format('%.*f', [I, Result]));
    end
    else if FuncName = 'ISVARNA' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      if IsVarNA(ParamList.Strings[0]) then
        Result := 1.0
      else
        Result := 0.0;
    end
    else if FuncName = 'ISSTRVARNA' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      if IsStrVarNA(ParamList.Strings[0]) then
        Result := 1.0
      else
        Result := 0.0;
    end
    else if FuncName = 'ISFORMULANA' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      if IsFormulaNA(ParamList.Strings[0]) then
        Result := 1.0
      else
        Result := 0.0;
    end
    {ECO: added a not function... }
    else if FuncName = 'NOT' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      if Trunc(Self.Result) = 0 then
        Result := 1.0
      else
        Result := 0.0;
    end
    else if FuncName = 'SUM' then
    // Returns the sum of expr1+expr2+expr3+...;
    begin
      SavedContext := SaveContext;
      GetRange(ParamList);
      if ParamList.Count < 1 then
        FResultWasNA := true
      else
      begin
        Result := 0.0;
        for I := 0 to ParamList.Count - 1 do
          Result := Result + StrToFloat(ParamList.Strings[I]);
      end;
    end
    else if FuncName = 'MAX' then
    // Returns the maximum of expr1,expr2,expr3,...;
    begin
      SavedContext := SaveContext;
      GetRange(ParamList);
      if ParamList.Count < 1 then
        FResultWasNA := true
      else
      begin
        Result := -MaxDbl;
        for I := 0 to ParamList.Count - 1 do
          Result := max(Result, StrToFloat(ParamList.Strings[I]));
      end;
    end
    else if FuncName = 'MIN' then
    // Returns the minumum of expr1,expr2,expr3,...;
    begin
      SavedContext := SaveContext;
      GetRange(ParamList);
      if ParamList.Count < 1 then
        FResultWasNA := true
      else
      begin
        Result := MaxDbl;
        for I := 0 to ParamList.Count - 1 do
          Result := min(Result, StrToFloat(ParamList.Strings[I]));
      end;
    end
    else if FuncName = 'AVG' then
    // Returns the average of expr1,expr2,expr3,...;
    begin
      SavedContext := SaveContext;
      GetRange(ParamList);
      if ParamList.Count < 1 then
        FResultWasNA := true
      else
      begin
        Result := 0.0;
        for I := 0 to ParamList.Count - 1 do
          Result := Result + StrToFloat(ParamList.Strings[I]);
        Result := Result / ParamList.Count;
      end;
    end
    else if FuncName = 'COUNT' then
    // Returns the number of expressions in expr1,expr2,expr3,...;
    begin
      SavedContext := SaveContext;
      GetRange(ParamList);
      Result := ParamList.Count;
    end;
  finally
    if Assigned(SavedContext) then
      RestoreContext(SavedContext);
  end;
end;

procedure TEzParser.GetStrRange(ParamList: TStringList);
var
  slTemp: TStringList;
  x, P1, P2: integer;
  s, Temp: string;
  SavedContext: PContext;

begin
  SavedContext := SaveContext;
  slTemp := TStringList.Create;
  x := 0;

  try
    while x < ParamList.Count do
    begin
      s := ParamList[x];

      // is parameter a function (i.e. max(x,y,z) or childs(x))
      P1 := Pos('(', s);
      if P1 <> 0 then
      begin
        Temp := UpperCase(Trim(Copy(s, 1, P1-1)));
        // range function encoutered
        if IsRangeFunction(Temp) then
        begin
          P2 := Pos(')', s);
          if P2 = 0 then
            RaiseError(SBraceError+' "'+s+'"');
          slTemp.Clear;
          DoGetRangeValues(Temp, Trim(Copy(s, P1+1, P2-P1-1)), vtString, slTemp);
          ParamList.Delete(x);
          ParamList.AddStrings(slTemp);
          // don't call inc(x);
        end
        else
        begin
          FExpression := ParamList[x];
          ParamList[x] := Self.StrResult;
          inc(x);
        end;
      end
      else
      begin
        FExpression := ParamList[x];
        ParamList[x] := Self.StrResult;
        inc(x);
      end;
    end;
  finally
    RestoreContext(SavedContext);
    slTemp.Destroy;
  end;
end;

function TEzParser.ExecStrFunc(FuncName: String; ParamList: TStringList): String;
var
  I: Integer;
  J: Double;
  SavedContext: PContext;
  Temp: String;
begin
  Result := '';
  SavedContext := nil;
  try
    if FuncName = 'IF' then
    begin
      if ParamList.Count <> 3 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      if Self.Result >= 1.0 then
      begin
        FExpression := ParamList.Strings[1];
        Result := Self.StrResult;
        // Ensure dependency list gets built correctly
        if FRecording then
        begin
          FExpression := ParamList.Strings[2];
          Self.StrResult;
        end;
      end
      else
      begin
        FExpression := ParamList.Strings[2];
        Result := Self.StrResult;
        // Ensure dependency list gets built correctly
        if FRecording then
        begin
          FExpression := ParamList.Strings[1];
          Self.StrResult;
        end;
      end;
    end
    else if FuncName = 'REPL' then
    begin
      if ParamList.Count <> 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Result := Self.StrResult;
      Temp := Result;
      FExpression := ParamList.Strings[1];
      J := Self.Result;
      for I := 1 to Round(J) do
        Result := Result + Temp;
    end
    else if (FuncName = 'CONCATENATE') or (FuncName = 'CONCAT') then
    begin
      if ParamList.Count < 2 then
        raiseerror(SWrongParamCount);
      Result := '';
      SavedContext := SaveContext;
      for I := 0 to ParamList.Count - 1 do
      begin
        FExpression := ParamList.Strings[I];
        Result := Result + Self.StrResult;
      end;
    end
    else if FuncName = 'TOSTR' then
    begin
      if ParamList.Count <> 1 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Result := FloatToStr(Self.Result);
    end
    else if FuncName = 'LEFT' then
    begin
      if ParamList.Count <> 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Result := Self.StrResult;
      FExpression := ParamList.Strings[1];
      Result := Copy(Result, 1, Round(Self.Result));
    end
    else if FuncName = 'RIGHT' then
    begin
      if ParamList.Count <> 2 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Temp := StrResult;
      FExpression := ParamList.Strings[1];
      Result := Copy(Temp, Length(Temp) - Round(Self.Result) + 1, Length(Temp));
    end
    else if FuncName = 'SUBSTR' then
    begin
      if ParamList.Count <> 3 then
        raiseerror(SWrongParamCount);
      SavedContext := SaveContext;
      FExpression := ParamList.Strings[0];
      Temp := StrResult;
      FExpression := ParamList.Strings[1];
      J := Self.Result;
      FExpression := ParamList.Strings[2];
      Result := Copy(Temp, Round(J), Round(Self.Result));
    end
    else if FuncName = 'SUM' then
    begin
      SavedContext := SaveContext;
      GetStrRange(ParamList);
      Result := '';
      for I:=0 to ParamList.Count - 1 do
      begin
        FExpression := ParamList.Strings[I];
        Result := Result + Self.StrResult;
      end;
    end
    else if FuncName = 'MAX' then
    // Returns the maximum of expr1,expr2,expr3,...;
    begin
      SavedContext := SaveContext;
      GetStrRange(ParamList);
      Result := '';
      if ParamList.Count > 0 then
      begin
        ParamList.Sort;
        Result := ParamList[ParamList.Count-1];
      end;
    end
    else if FuncName = 'MIN' then
    // Returns the minumum of expr1,expr2,expr3,...;
    begin
      SavedContext := SaveContext;
      GetStrRange(ParamList);
      Result := '';
      if ParamList.Count > 0 then
      begin
        ParamList.Sort;
        Result := ParamList[0];
      end;
    end
    else if FuncName = 'COUNT' then
    // Returns the number of expressions in expr1,expr2,expr3,...;
    begin
      SavedContext := SaveContext;
      GetStrRange(ParamList);
      Result := IntToStr(ParamList.Count);
    end;
  finally
    if Assigned(SavedContext) then
      RestoreContext(SavedContext);
  end;
end;


function TEzParser.SaveContext: PContext;
begin
  New(Result);
  Result.OldPtr := Ptr;
  Result.OldLN := Lineno;
  Result.OldSValue := SValue;
  Result.OldExp := FExpression;
  Result.OldToken := Token;
  Result.OldComplexResult := FIsComplexResult;
  Result.OldKey := FExpressionKey;
end;

procedure TEzParser.RestoreContext(P: PContext);
begin
  Ptr := P.OldPtr;
  Lineno := P.OldLN;
  SValue := P.OldSValue;
  FExpression := P.OldExp;
  Token := P.OldToken;
  FIsComplexResult := P.OldComplexResult;
  FExpressionKey := P.OldKey;
  Dispose(P);
end;

function TEzParser.IsRangeFunction(FN: String): Boolean;
begin
  Result := FRangeFunctions.IndexOf(FN)<>-1;
end;

function TEzParser.IsSpecialFunction(FN: String): Boolean;
begin
  Result := FSpecialFunctions.IndexOf(FN)<>-1;
end;

function TEzParser.IsInternalFunction(FN: String): Boolean;
begin
  Result := FInternalFunctions.IndexOf(FN)<>-1;
end;

{
function TEzParser.GetExpType(Exp: String): TExpType;
var
  SavedContext: PContext;
begin
  Result := etUnknown;
  // look for quotes
  if Pos('''', Exp) > 0 then
    Result := etString
  else
  begin
    SavedContext := SaveContext;
    FExpression := Exp;
    try
      Ptr := PChar(FExpression);
      Lineno := 1;
      Token := tkError;
      while Token <> tkEOF do
      begin
        lex;
        Inc(Ptr);
        if Token = tkIdent then
        begin
          try
            _GetStrVar(SValue);
          except
            break;
          end;
          Result := etString;
          Exit;
        end;
      end;
      Result := etDouble;
    finally
      RestoreContext(SavedContext);
    end;
  end;
end;
}

function TEzParser.DefCalcProc(ctype: TCalcType; const S: String;
  var V: Double): Boolean;
begin
  Result := TRUE;
  case ctype of
    ctGetValue: begin
      if S = 'PI' then V := Pi else
      if S = 'E' then V := 2.718281828 else
      if S = 'TRUE' then V := 1.0 else
      if S = 'FALSE' then V := 0.0 else
      Result := FALSE;
    end;
    ctSetValue: begin
      Result := FALSE;
    end;
    ctFunction: begin
//      if S = 'ROUND'  then V := Round(V) else
      if S = 'TRUNC'  then V := Trunc(V) else
      if S = 'INT'  then V := Int(V) else
      if S = 'FRAC'  then V := Frac(V) else
      if S = 'SIN'  then V := sin(V) else
      if S = 'COS'  then V := cos(V) else
      if S = 'TAN'  then V := tan(V) else
      if S = 'ATAN' then V := arctan(V) else
      if S = 'LN'   then V := ln(V) else
      if S = 'EXP'  then V := exp(V) else
      if S = 'SIGN' then begin if (V>0) then V := 1 else if(V<0) then V := -1 end else
      if S = 'SGN' then begin if (V>0) then V := 1 else if(V<0) then V := 0 end else
      if S = 'XSGN' then begin if(V<0) then V := 0 end else
      Result := FALSE;
    end;
  end;
end;

function TEzParser.DefStrCalcProc(ctype: TCalcType; const S: String;
  var V: String): Boolean;
begin
  Result := FALSE;
  if ctype = ctFunction then
  begin
    Result := TRUE;
    if S = 'TRIM' then
      V := Trim(V)
    else if S = 'LTRIM' then
      V := TrimLeft(V)
    else if S = 'RTRIM' then
      V := TrimRight(V)
    else
      Result := FALSE;
   end;
end;

function TEzParser.DoStrUserFunction(const Func: string; Params: TStringList) : string;
var
  Handled: Boolean;

begin
  Handled := False;

  if Assigned(FOnStrUserFunc) then
    FOnStrUserFunc(Self, Func, Params, Result, Handled);

  if not Handled then
    RaiseError(SFunctionError + ' "' + Func + '"');
end;

function TEzParser.DoUserFunction(const Func: string; Params: TStringList) : Double;
var
  Handled: Boolean;

begin
  Handled := False;

  if Assigned(FOnUserFunc) then
    FOnUserFunc(Self, Func, Params, Result, Handled);

  if not Handled then
    RaiseError(SFunctionError + ' "' + Func + '"');
end;

function TEzParser.DoGetIdentValue(ctype: TCalcType; const S: String): Double;
begin
  if not DoGetIdentValue(ctype, S, Result) then
    RaiseError(SFunctionError + ' "' + S + '"');
end;

function TEzParser.DoGetIdentValue(ctype: TCalcType; const S: String; var Value: Double) : Boolean;
begin
  Result := False;
  if Assigned(FOnNoIdent) then
    FOnNoIdent(Self, ctGetValue, Name, Value, Result);
end;

function TEzParser.DoGetStrIdentValue(ctype: TCalcType; const S: String): string;
begin
  if not DoGetStrIdentValue(ctype, S, Result) then
    RaiseError(SFunctionError + ' "' + s + '"');
end;

function TEzParser.DoGetStrIdentValue(ctype: TCalcType; const S: String; var Value: string) : Boolean;
begin
  Result := False;
  if Assigned(FOnNoStrIdent) then
    FOnNoStrIdent(Self, ctGetValue, Name, Value, Result);
end;

procedure TEzParser.DoGetRangeValues(const sRange, sVariable: string; ValueType: TValueType; ParamList: TStringList);
begin
  if Assigned(FOnGetRangeValues) then
    FOnGetRangeValues(Self, sRange, sVariable, ValueType, ParamList);
end;

procedure TEzParser.RaiseError(const Msg: String);
begin
  raise ECalculate.Create(Msg + ': ''' + Expression + '''');
end;

function TEzParser.tofloat(B: Boolean): Double;
begin
  if (B) then tofloat := 1.0 else tofloat := 0.0;
end;

{ yylex like function }

procedure TEzParser.lex;
label
  Error;
var
  c, sign: char;
  frac: Double;
  exp: LongInt;
  s_pos: PChar;

  function ConvertNumber(first, last: PChar; base: Word): boolean;
  var
    c: Byte;
  begin
    fvalue := 0;
    while first < last do begin
      c := Ord(first^) - Ord('0');
      if (c > 9) then begin
        Dec(c, Ord('A') - Ord('9') - 1);
        if (c > 15) then Dec(c, Ord('a') - Ord('A'));
      end;
      if (c >= base) then break;
      fvalue := fvalue * base + c;
      Inc(first);
    end;
    Result := (first = last);
  end;

begin
  { skip blanks }
  while ptr^ <> #0 do begin
    if (ptr^ = #13) then Inc(lineno)
    else if (ptr^ > ' ') then break;
    Inc(ptr);
  end;

  { check EOF }
  token := tkEOF;
  if (ptr^ = #0) then Exit;

  s_pos := ptr;
  token := tkNUMBER;

  { match pascal like hex number }
  if (ptr^ = '$') then begin
    Inc(ptr);
    // while CharInSet(ptr^, ['0'..'9', 'A'..'H', 'a'..'h']) do Inc(ptr);
    while CharInSet(ptr^, ['0'..'9', 'A'..'F', 'a'..'f']) do Inc(ptr);
    if not ConvertNumber(s_pos, ptr, 16) then goto Error;
    Exit;
  end;

  { match numbers }
  if CharInSet(ptr^, ['0'..'9']) then begin

    { C like mathing }
    if (ptr^ = '0') then begin
      Inc(ptr);

      { match C like hex number }
      if CharInSet(ptr^, ['x', 'X']) then begin
        Inc(ptr);
        s_pos := ptr;
        // while CharInSet(ptr^, ['0'..'9', 'A'..'H', 'a'..'h']) do Inc(ptr);
        while CharInSet(ptr^, ['0'..'9', 'A'..'F', 'a'..'f']) do Inc(ptr);
        if not ConvertNumber(s_pos, ptr, 16) then goto Error;
        Exit;
      end;

      { match C like binary number }
      if CharInSet(ptr^, ['b', 'B']) then begin
        Inc(ptr);
        s_pos := ptr;
        while CharInSet(ptr^, ['0'..'1']) do Inc(ptr);
        if not ConvertNumber(s_pos, ptr, 2) then goto Error;
        Exit;
      end;
    end;

    while CharInSet(ptr^, ['0'..'9', 'A'..'F', 'a'..'f']) do Inc(ptr);

    { match assembler like hex number }
    if CharInSet(ptr^, ['H', 'h']) then begin
      if not ConvertNumber(s_pos, ptr, 16) then goto Error;
      Inc(ptr);
      Exit;
    end;

    { match assembler like binary number }
    if CharInSet(ptr^, ['B', 'b']) then begin
      if not ConvertNumber(s_pos, ptr, 2) then goto Error;
      Inc(ptr);
      Exit;
    end;

    { match simple decimal number }
    if not ConvertNumber(s_pos, ptr, 10) then goto Error;

    { match degree number }
    if (ptr^ = '`') then begin
      fvalue := fvalue * Pi / 180.0;
      Inc(ptr); frac := 0;
      while CharInSet(ptr^, ['0'..'9']) do begin
        frac := frac * 10 + (Ord(ptr^) - Ord('0'));
        Inc(ptr);
      end;
      fvalue := fvalue + (frac * Pi / 180.0 / 60);
      if (ptr^ = '`') then begin
      Inc(ptr); frac := 0;
      while CharInSet(ptr^, ['0'..'9']) do begin
        frac := frac * 10 + (Ord(ptr^) - Ord('0'));
        Inc(ptr);
      end;
      fvalue := fvalue + (frac * Pi / 180.0 / 60 / 60);
      end;
      fvalue := fmod(fvalue, 2*Pi);
      Exit;
    end;

    { match float numbers }
    if (ptr^ = {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator) then begin Inc(ptr);
      frac := 1;
      while CharInSet(ptr^, ['0'..'9']) do begin
        frac := frac / 10;
        fvalue := fvalue + frac * (Ord(ptr^) - Ord('0'));
        Inc(ptr);
      end;
    end;

    if CharInSet(ptr^, ['E', 'e']) then begin Inc(ptr);
      exp := 0;
      sign := ptr^;
      if CharInSet(ptr^, ['+', '-']) then Inc(ptr);
      if not CharInSet(ptr^, ['0'..'9']) then goto Error;
      while CharInSet(ptr^, ['0'..'9']) do begin
        exp := exp * 10 + Ord(ptr^) - Ord('0');
        Inc(ptr);
      end;
      if (exp = 0)
      then fvalue := 1.0
      else if (sign = '-')
      then while exp > 0 do begin fvalue := fvalue * 10; Dec(exp); end
      else while exp > 0 do begin fvalue := fvalue / 10; Dec(exp); end
    end;
    Exit;
  end;

  {VK : match string }
  if (ptr^ = '''') then
  begin
    svalue := ptr^;
    Inc(ptr);
    while True do
    //    while (Length(svalue) < sizeof(svalue) - 1) do
    begin
      case ptr^ of
        #0, #10, #13:
          RaiseError(SInvalidString);
        '''':
          begin
            Inc(ptr);
            if ptr^ <> '''' then
              break;
          end;
      end;
      svalue := svalue + ptr^;
      Inc(ptr);
    end;
    token := tkString;
    Exit;
  end;

  { match identifiers }
  if CharInSet(ptr^, ['A'..'Z','a'..'z','_']) then begin
    svalue := ptr^;
    Inc(ptr);
    while CharInSet(ptr^, ['A'..'Z','a'..'z','0'..'9','_']) do
    begin
      svalue := svalue + ptr^;
      Inc(ptr);
    end;
    token := tkIDENT;
    Exit;
  end;

  { match operators }
  c := ptr^; Inc(ptr);
  case c of
    '=': begin token := tkASSIGN;
      if (ptr^ = '=') then begin Inc(ptr); token := tkEQ; end;
    end;
    '+': begin token := tkADD; end;
    '-': begin token := tkSUB; end;
    '*': begin token := tkMUL;
      if (ptr^ = '*') then begin Inc(ptr); token := tkPOW; end;
    end;
    '/': begin token := tkDIV; end;
    '%': begin token := tkMOD;
      if (ptr^ = '%') then begin Inc(ptr); token := tkPER; end;
    end;
    '~': begin token := tkINV; end;
    '^': begin token := tkXOR; end;
    '&': begin token := tkAND; end;
    '|': begin token := tkOR; end;
    '<': begin token := tkLT;
      if (ptr^ = '=') then begin Inc(ptr); token := tkLE; end else
      if (ptr^ = '>') then begin Inc(ptr); token := tkNE; end;
    end;
    '>': begin token := tkGT;
      if (ptr^ = '=') then begin Inc(ptr); token := tkGE; end else
      if (ptr^ = '<') then begin Inc(ptr); token := tkNE; end;
    end;
    '!': begin token := tkNOT;
      if (ptr^ = '=') then begin Inc(ptr); token := tkNE; end;
    end;
    '(': begin token := tkLBRACE; end;
    ')': begin token := tkRBRACE; end;
    ';': begin token := tkSEMICOLON end;
    else begin token := tkERROR; dec(ptr); end;
  end;
  Exit;

Error:
  token := tkERROR;
end;

(*
// LL grammatic for calculator, priorities from down to up
//
// start: expr6;
// expr6: expr5 { & expr5 | ^ expr5 | & expr5 }*;
// expr5: expr4 { < expr4 | > expr4 | <= expr4 | >= expr4 | != expr4 | == expr4 }*;
// expr4: expr3 { + expr3 | - expr3 }*;
// expr3: expr2 { * expr2 | / expr2 | % expr2 | %% expr2 }*;
// expr2: expr1 { ! expr1 | ~ expr1 | - expr1 | + expr1 };
// expr1: term ** term
// term: tkNUMBER | tkIDENT | (start) | tkIDENT(start) | tkIDENT = start;
//
*)

procedure TEzParser.term(var R: Double); var S: String;
var
  ParamList: TStringList;
begin
  case token of
    tkNUMBER: begin
      R := fvalue;
      lex;
    end;
    tkLBRACE: begin lex;
      expr6(R);
      if (token = tkRBRACE)
        then lex
        else RaiseError(SSyntaxError);
    end;
    tkIDENT:
    begin
      FIsComplexResult := true;
      if FCaseSensitive then
        S := svalue else
        S := UpperCase(svalue);
      lex;
      if token = tkLBRACE then
      begin
        if IsSpecialFunction(S) then
        begin
          ParamList := TStringList.Create;
          try
            ParseFuncParams(S, ParamList);
            R := ExecFunc(s, ParamList);
          finally
            ParamList.Free;
          end;
          lex;
        end
        else if IsInternalFunction(S) then
        begin
          lex;
          expr6(R);
          if (token = tkRBRACE) then lex
            // vk 11/19/99
            else RaiseError(SFunctionError + ' "' + s + '"');
          if not CalcProc(ctFunction, s, R)
            then RaiseError(SFunctionError+' "'+s+'"');
        end
        else
        begin
          ParamList := TStringList.Create;
          try
            ParseFuncParams(S, ParamList);
            R := DoUserFunction(S, ParamList);
          finally
            ParamList.Free;
          end;
          lex;
        end;
      end
      else if (token = tkASSIGN) then begin
        lex; expr6(R);
        if not calcProc(ctSetValue, s, R)
          then RaiseError(SFunctionError+' "'+s+'"');
      end else
      if not CalcProc(ctGetValue, s, R)
        then RaiseError(SFunctionError+' "'+s+'"');
    end;

    else {case}
      RaiseError(SSyntaxError);
  end;
end;

procedure TEzParser.expr1(var R: Double); var V: Double;
begin
  term(R);
  if (token = tkPOW) then begin
    lex; term(V);
    R := power(R, V);
  end;
end;

procedure TEzParser.expr2(var R: Double);
var oldt: TToken;
begin
  if (token in [tkNOT, tkINV, tkADD, tkSUB]) then
  begin
    oldt := token;
    lex;
    expr2(R);
    case oldt of
      tkNOT:
        if Trunc(R) = 0 then
          R := 1.0
        else
          R := 0.0;
      tkINV: R := (not Trunc(R));
      tkADD: ;
      tkSUB: R := -R;
    end;
  end
  else
    expr1(R);
end;

procedure TEzParser.expr3(var R: Double); var V: Double; oldt: TToken;
begin
  expr2(R);
  while token in [tkMUL, tkDIV, tkMOD, tkPER] do begin
    oldt := token; lex; expr2(V);
    case oldt of
      tkMUL: R := R * V;
      tkDIV: R := R / V;
      tkMOD: R := Trunc(R) mod Trunc(V);
      tkPER: R := R * V / 100.0;
    end;
  end;
end;

procedure TEzParser.expr4(var R: Double); var V: Double; oldt: TToken;
begin
  expr3(R);
  while token in [tkADD, tkSUB] do begin
    oldt := token; lex; expr3(V);
    case oldt of
      tkADD: R := R + V;
      tkSUB: R := R - V;
    end;
  end;
end;

procedure TEzParser.expr5(var R: Double); var V: Double; oldt: TToken;
begin
  expr4(R);
  while token in [tkLT, tkLE, tkEQ, tkNE, tkGE, tkGT] do begin
    oldt := token; lex; expr4(V);
    case oldt of
      tkLT: R := tofloat(R < V);
      tkLE: R := tofloat(R <= V);
      tkEQ: R := tofloat(R = V);
      tkNE: R := tofloat(R <> V);
      tkGE: R := tofloat(R >= V);
      tkGT: R := tofloat(R > V);
    end;
  end;
end;

procedure TEzParser.expr6(var R: Double); var V: Double; oldt: TToken;
begin
  expr5(R);
  while token in [tkOR, tkXOR, tkAND] do begin
    oldt := token; lex; expr5(V);
    case oldt of
      tkOR : R := Trunc(R) or  Trunc(V);
      tkAND: R := Trunc(R) and Trunc(V);
      tkXOR: R := Trunc(R) xor Trunc(V);
    end;
  end;
end;

procedure TEzParser.start(var R: Double);
begin
  expr6(R);
  while (token = tkSEMICOLON) do begin lex; expr6(R); end;
  if not (token = tkEOF) then RaiseError(SSyntaxError);
end;

procedure TEzParser.ParseFuncParams(const Func: string; ParamList: TStringList);
var
  P1, P2: PChar;
  Ctr: Integer;
  ParamStr: String;

begin
  // Extract the parameter(s) string
  P1 := Ptr;
  Ctr := 1;

  while (P1^ <> #0) do
  begin
    if P1^ = '(' then
      Inc(Ctr)
    else if P1^ = ')' then
      Dec(Ctr);
    if Ctr = 0 then
      break
    else
      Inc(P1);
  end;
  if (P1^ <> ')') then
    RaiseError(SFunctionError+' "' + Func + '"');
  if P1=Ptr then Exit; // No function parameters
  
  SetLength(ParamStr, P1 - Ptr);
  StrLCopy(PChar(ParamStr), Ptr, P1 - Ptr);
  // Set Ptr to one past the brace
  Ptr := P1 + 1;

  // Parse the parameters
  P1 := PChar(ParamStr);
  P2 := PChar(ParamStr);
  Ctr := 0;
  while (P1^ <> #0) do
  begin
    if P1^ = '(' then
      Inc(Ctr)
    else  if P1^ = ')' then
      Dec(Ctr);
    if (P1^ = ',') and (Ctr = 0) then
    begin
      ParamList.Add(Trim(Copy(ParamStr, P2 - PChar(ParamStr) + 1, P1 - P2)));
      P2 := P1 + 1;
    end;
    Inc(P1);
  end;
  ParamList.Add(Trim(Copy(ParamStr, P2 - PChar(ParamStr) + 1, P1 - P2)));
end;


procedure TEzParser.StrTerm(var R: String);
var
  S: String;
  ParamList: TStringList;
  SavedContext: PContext;
begin
  case token of
    tkString:
      begin
        // Strip out the enclosing commas
        R := Copy(svalue, 2, Length(svalue) - 1);
        lex;
      end;
    tkLBrace:
      begin
        lex;
        ExpStr(R);
        if (token = tkRBrace) then
          lex
        else
          RaiseError(SSyntaxError);
      end;
    tkIDENT:
      begin
        FIsComplexResult := true;
        if CaseSensitive then
          S := svalue else
          S := UpperCase(svalue);
        lex;
        if token = tkLBRACE then
        begin
          ParamList := TStringList.Create;
          try
            ParseFuncParams(S, ParamList);

            if IsSpecialFunction(S) then
              R := ExecStrFunc(s, ParamList)

            else if ParamList.Count=1 then
            begin
              SavedContext := SaveContext;
              FExpression := ParamList.Strings[0];
              R := Self.StrResult;
              RestoreContext(SavedContext);
              if not DefStrCalcProc(ctFunction, s, R) then
                R := DoStrUserFunction(S, ParamList);
            end
            else
              R := DoStrUserFunction(S, ParamList);
          finally
            ParamList.Free;
          end;
          lex;
{
          lex;
          end
          else
          begin
            lex;
            ExpStr(R);
            if (token = tkRBRACE) then lex
              else RaiseError(SFunctionError+' "' + s + '".');
            if not StrCalcProc(ctFunction, s, R)
              then RaiseError(SFunctionError+' "'+s+'".');
          end;
}
        end
        else if (token = tkAssign) then
        begin
          lex;
          ExpStr(R);
          if not StrCalcProc(ctSetValue, S, R) then
            RaiseError(SFunctionError + ' "' + S + '"');
        end
        else if not StrCalcProc(ctGetValue, S, R) then
            RaiseError(SFunctionError + ' "' + S + '"');
      end;
    else
      RaiseError(SSyntaxError);
  end;
end;

procedure TEzParser.ExpStr(var R: String);
var
  V: String;
begin
  StrTerm(R);
  while token = tkAdd do
  begin
    lex;
    StrTerm(V);
    R := R + V;
  end;
end;

procedure TEzParser.StrStart(var R: String);
begin
  ExpStr(R);
  while (token = tkSEMICOLON) do
  begin
    lex; ExpStr(R);
  end;
  if not (token = tkEOF) then RaiseError(SSyntaxError);
end;

function TEzParser.Calculate(const Formula: String;
  var R: Double; Proc: TCalcCBProc): Boolean;
begin
  if (@Proc = Nil) then
    CalcProc := DefCalcProc
  else
    CalcProc := Proc;
  ptr := PChar(Formula);
  lineno := 1;
  lex;
  start(R);
  Result := TRUE;
end;

function TEzParser.StrCalculate(const Formula: String;
  var R: String; Proc: TCalcCBStrProc): Boolean;
begin
  if (@Proc = Nil) then
    StrCalcProc := DefStrCalcProc
  else
    StrCalcProc := Proc;
  ptr := PChar(Formula);
  lineno := 1;
  lex;
  StrStart(R);
  Result := TRUE;
end;

constructor TEzParser.Create(AOwner: TComponent);
begin
  {$ifdef CALCTEST}
  AssignFile(T, 'CalcTest.dat');
  Rewrite(T);
  Append(T);
  {$endif}
  {$ifdef friendlymode}
  AssignFile(T2, 'FM' + IntToStr(Random(10000)) + '.dat');
  Rewrite(T2);
  Append(T2);
  {$endif}
  inherited Create(AOwner);
  FCaseSensitive := true;
  FCacheResults := true;
  FVars := TList.Create;
  FStrVars := TList.Create;
  FFormulae := TList.Create;
  FIsComplexResult := False;
  FExpCache := TExpressionCache.Create(10, SizeOf(TExpVal));
  FExpCache.SortOrder := tsAscending;
  FMaxExpCache := 1000;
  FRecFormula := TList.Create;
  // vk:10/03/99
  FEvalList := TList.Create;
  // Methods that return a double value but can use non double params.
  FDefaultStrVar := False;
  FDefaultVar := False;
  FDefaultStrVarAs := '';
  FDefaultVarAs := 1;
end;

destructor TEzParser.Destroy;
begin
  {$ifdef CALCTEST}
    CloseFile(T);
  {$endif}
  {$ifdef friendlymode}
    WriteLn(T2, 'Total Variables : ' + IntToStr(VarCount));
    WriteLn(T2, 'Total String Variables : ' + IntToStr(StrVarCount));
    Flush(T2);
    CloseFile(T2);
  {$endif}
  DeleteFormulae;
  DeleteVars;
  DeleteStrVars;
  DeleteEvals;
  FVars.Free;
  FStrVars.Free;
  FFormulae.Free;
  FRecFormula.Free;
  FEvalList.Free;
  FExpCache.Free;
  inherited Destroy;
end;

procedure TEzParser.SetVar(const Name: String; value: Double);
var
  i: SmallInt;
  V: PNamedVar;
begin
  V := nil;
  i := 0;
  while (i < FVars.Count) do begin
    V := FVars[i];
    if CompareStr(V^.Name, UpperCase(Name)) = 0 then Break;
    Inc(i);
  end;
  if (i >= FVars.Count) then begin
    New(V);
    V^.Name := UpperCase(Name);
    V^.IsNA := False ;
    V^.Dep := TList.Create;
    FVars.Add(V);
  end;
  if Assigned(V) then
  begin
    V^.Value := Value;
    for I := 0 to V^.Dep.Count - 1 do
      TFormula(V^.Dep[I]).FValid := False;
  end;

  // Ivalidate and destroy the cache.
  // We kill the entire cache. The complextity of a selective killing algorithm
  // probably outweigh the benifits. A simiple POS search with the variable name
  // will not do since we could leave out the formulas that depend on the variable.
  FExpCache.Clear;
end;

procedure TEzParser.SetStrVar(const Name, Value: String);
var
  i: SmallInt;
  V: PNamedStrVar;
begin
  V := nil;
  i := 0;
  while (i < FStrVars.Count) do begin
    V := FStrVars[i];
    if CompareStr(V^.Name, UpperCase(Name)) = 0 then Break;
    Inc(i);
  end;
  if (i >= FStrVars.Count) then begin
    New(V);
    V^.Name := UpperCase(Name);
    V^.IsNA := False ;
    V^.Dep := TList.Create;
    FStrVars.Add(V);
  end;
  if Assigned(V) then
  begin
    V^.Value := Value;
  // Ivalidate all associated formulae
    for I := 0 to V^.Dep.Count - 1 do
      TFormula(V^.Dep[I]).FValid := False;
  end;
  // Ivalidate and destroy the cache
  // We kill the entire cache. The complextity of a selective killing algorithm
  // probably outweigh the benifits. A simiple POS search with the variable name
  // will not do since we could leave out the formulas that depend on the variable.
  FExpCache.Clear;

end;

function TEzParser._GetVar(const Name: String): Double;
var
  i: SmallInt;
  V: PNamedVar;
begin
  Result := 0.0;
  for i := 0 to FVars.Count-1 do begin
    V := FVars[i];
    if CompareStr(V^.Name, Name) = 0 then begin
      Result := V^.Value;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

function TEzParser._GetVar(const Name: String; var Value: Double) : Boolean;
var
  i: SmallInt;
  V: PNamedVar;
begin
  Result := False;
  for i := 0 to FVars.Count-1 do
  begin
    V := FVars[i];
    if CompareStr(V^.Name, Name) = 0 then
    begin
      Value := V^.Value;
      Result := True;
      Exit;
    end;
  end;
end;

function TEzParser.GetVar(const Name: String): Double;
begin
  Result :=  _GetVar(UpperCase(Name));
end;

function TEzParser._GetStrVar(const Name: String): String;
var
  i: SmallInt;
  V: PNamedStrVar;
begin
  for i := 0 to FStrVars.Count-1 do begin
    V := FStrVars[i];
    if CompareStr(V^.Name, Name) = 0 then begin
      Result := V^.Value;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

function TEzParser.GetStrVar(const Name: String): String;
begin
  Result := _GetStrVar(UpperCase(Name));
end;

function TEzParser.GetResult: Double;
var
  I: Integer;
  Key: TExpVal;

begin
  FResultWasNA := false;

  Key.Key := FExpressionKey;
  Key.Exp := FExpression;

  if FCacheResults and FExpCache.FindItem(I, Key) then
  begin
    Result := FExpCache[I].Val;
    FResultWasNA := FExpCache[I].WasNA;
  end
  else
  begin
    FIsComplexResult := false;
    calculate(FExpression, Result, IdentValue);

    if FIsComplexResult then
    begin
      Key.Val := Result;
      Key.WasNA := ResultWasNA;
      // Add the expression and result to the cache
      if FCacheResults and (FExpCache.Count<FMaxExpCache) then
        FExpCache.Add(Key);
    end;
  end;
  FResultWasString := False;
end;

function TEzParser.IdentValue(ctype: TCalcType;
  const Name: String; var Res: Double): Boolean;
begin

  Result := False;
  try
    Result := DefCalcProc(ctype, name, Res);
  except
  end;

  if Result then Exit;
  Result := TRUE;

  case ctype of
    ctGetValue:
    try
      if not _GetVar(Name, Res) and
         not GetFormulaObj(Name, Res) and
         not DoGetIdentValue(ctGetValue, Name, Res)
      then
        RaiseError(SFunctionError + ' "' + Name + '"');

      if FRecording then
      begin
        // The flage DummyCalc is set to avoid a div by zero in
        // an if expression. It is essential for an IF exression
        // that returns a zero incase the denominator is zero.
        if FDummyCalc then
          Res := 1;
        RecordVar(Name);
      end;
    except
      On E:ECalculate do
      begin
        {$ifdef friendlymode}
        WriteLn(T2, 'Variable ' + Name + ' assigned default value');
        Flush(T2);
        if not DefaultVar then
        begin
          Vars[Name] := 1;
          Res := 1;
        end
        else
        begin
          Vars[Name] := DefaultVarAs;
          Res := DefaultVarAs;
        end;
        if FRecording then
          for I := 0 to FRecFormula.Count - 1 do
            WriteLn(T2, TFormula(FRecFormula[I]).Name, ', ',
              TFormula(FRecFormula[I]).Exp);
        WriteLn(T2, ' ');
        Flush(T2);
        {$endif}
        raise;
      end;
    end;

    ctSetValue: Vars[Name] := Res;
    ctFunction: Res := DoGetIdentValue(ctFunction, Name);
  end;
end;

function TEzParser.IdentStrValue(ctype: TCalcType;
  const Name: String; var Res: String): Boolean;
var
  I: Integer;
begin
  Result := False;
  try
    Result := DefStrCalcProc(ctype, name, Res);
  except
  end;
  if Result then Exit;

  Result := TRUE;
  case ctype of
    ctGetValue:
      try
        Res := _GetStrVar(Name);
        if FRecording then
          RecordStrVar(Name);
      except
        On ECalculate do
        try
          Res := GetFormulaObj(Name, I).StrValue;
        except
          On E:ECalculate do
          begin
            {$ifdef friendlymode}
            WriteLn(T2, 'String Variable ' + Name + ' assigned default value');
            if not DefaultVar then
            begin
              StrVars[Name] := '';
              Res := '';
            end
            else
            begin
              StrVars[Name] := DefaultStrVarAs;
              Res := DefaultStrVarAs;
            end;
            if FRecording then
              for I := 0 to FRecFormula.Count - 1 do
                WriteLn(T2, TFormula(FRecFormula[I]).Name, ', ',
                  TFormula(FRecFormula[I]).Exp);
            WriteLn(T2, ' ');
            Flush(T2);
            {$endif}
            // See if the calling code has the value of the identifier
            // It is not a good idea to have variables that participate
            // in formula definitions to be assigned values in the calling
            // code since it will compromise the working of dependency
            // lists
            Res := DoGetStrIdentValue(ctGetValue, Name);
          end;
        end
        else
          raise;
      end;
    ctSetValue: StrVars[Name] := Res;
    ctFunction: Res := DoGetStrIdentValue(ctFunction, Name);
  end;
end;

function TEzParser.NameOfVar(Index: Word): String;
begin
  Result := PNamedVar(FVars[Index])^.Name;
end;

function TEzParser.NameOfStrVar(Index: Word): String;
begin
  Result := PNamedStrVar(FStrVars[Index])^.Name;
end;

function TEzParser.NameOfFormula(Index: Word): String;
begin
  Result := TFormula(FFormulae[Index]).Name;
end;

// Sledgehammer delete! To be called only from Destroy
procedure TEzParser.DeleteVars;
var
  i: Integer;
  V: PNamedVar;
begin
  for i := FVars.Count-1 downto 0 do begin
    V := FVars[i];
    V^.Dep.Free;
    Dispose(V);
    FVars[i] := Nil;
  end;
  FVars.Clear;
end;

// Sledgehammer delete! To be called only from Destroy
procedure TEzParser.DeleteStrVars;
var
  i: Integer;
  V: PNamedStrVar;
begin
  for i := FStrVars.Count-1 downto 0 do begin
    V := FStrVars[i];
    V^.Dep.Free;
    Dispose(V);
    FStrVars[i] := Nil;
  end;
  FStrVars.Clear;
end;

// Sledgehammer delete! To be called only from Destroy
procedure TEzParser.DeleteFormulae;
var
  i: Integer;
  V: TFormula;
begin
  for i := FFormulae.Count-1 downto 0 do begin
    V := FFormulae[i];
    V.Free;
    FFormulae[i] := Nil;
  end;
  FFormulae.Clear;
end;

procedure TEzParser.DeleteEvals;
var
  i: Integer;
  V: PEvalResult;
begin
  for i := FEvalList.Count-1 downto 0 do begin
    V := FEvalList[i];
    Dispose(V);
    FEvalList[i] := Nil;
  end;
  FEvalList.Clear;
end;

// Property access method
function TEzParser.GetStrResult: String;
var
  I: Integer;
  Key: TExpVal;

begin
  FResultWasNA := false;

  Key.Key := FExpressionKey;
  Key.Exp := FExpression;

  if FCacheResults and FExpCache.FindItem(I, Key) then
  begin
    Result := FExpCache[I].StrVal;
    FResultWasNA := FExpCache[I].WasNA;
  end
  else
  begin
    FIsComplexResult := false;
    StrCalculate(FExpression, Result, IdentStrValue);

    if FIsComplexResult then
    begin
      Key.StrVal := Result;
      Key.WasNA := ResultWasNA;
      // Add the expression and result to the cache
      if FCacheResults and (FExpCache.Count<FMaxExpCache) then
        FExpCache.Add(Key);
    end;
  end;
  FResultWasString := True;
end;

function TEzParser.GetVarCount: Integer;
begin
  Result := FVars.Count;
end;


function TEzParser.GetStrVarCount: Integer;
begin
  Result := FStrVars.Count;
end;

function TEzParser.GetFormulaeCount: Integer;
begin
  Result := FFormulae.Count;
end;

function TEzParser.GetFormula(const Name: String): String;
var
  I: Integer;
  V: TFormula;
begin
  for I := 0 to FFormulae.Count - 1 do
  begin
    V := FFormulae[I];
    if CompareStr(V.Name, UpperCase(Name)) = 0 then
    begin
      Result := V.Exp;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

procedure TEzParser.SetFormula(const Name, Value: String);
var
  i: SmallInt;
  V: TFormula;
//  SavedContext: PContext;
begin
  V := nil;
  i := 0;
  while (i < FFormulae.Count) do begin
    V := FFormulae[i];
    if CompareStr(V.Name, UpperCase(Name)) = 0 then
    begin
      DelFormDep(V);
      Break;
    end;
    Inc(i);
  end;
  if (i = FFormulae.Count) then begin
    V := TFormula.Create(Self);
    V.Name := UpperCase(Name);
    V.IsNA := False;
    FFormulae.Add(V);
  end;
  if Assigned(V) then
  begin
    V.FEvaluated := False;
    V.FValid := False;
    V.Exp := Value;
  end;
end;



procedure TEzParser.SetVarNA(const Name: String; Val: Boolean);
var
  I: Integer;
  P: Pointer;
begin
  for I := 0 to FVars.Count - 1 do
  begin
    P := FVars[I];
    if CompareStr(PNamedVar(P)^.Name, UpperCase(Name)) = 0 then
    begin
      PNamedVar(P)^.IsNA := Val;
      Exit;
    end;
  end;
end;

procedure TEzParser.SetStrVarNA(const Name: String; Val: Boolean);
var
  I: Integer;
  P: Pointer;
begin
  for I := 0 to FStrVars.Count - 1 do
  begin
    P := FStrVars[I];
    if CompareStr(PNamedStrVar(P)^.Name, UpperCase(Name)) = 0 then
    begin
      PNamedStrVar(P)^.IsNA := Val;
      Exit;
    end;
  end;
end;

procedure TEzParser.SetFormulaNA(const Name: String; Val: Boolean);
var
  I: Integer;
  P: Pointer;
begin
  for I := 0 to FFormulae.Count - 1 do
  begin
    P := FFormulae[I];
    if CompareStr(TFormula(P).Name, UpperCase(Name)) = 0 then
    begin
      TFormula(P).IsNA := Val;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;


function TEzParser.IsVarNA(const Name: String): Boolean;
var
  I: Integer;
  P: Pointer;
begin
  Result := False;
  for I := 0 to FVars.Count - 1 do
  begin
    P := FVars[I];
    if CompareStr(PNamedVar(P)^.Name, UpperCase(Name)) = 0 then
    begin
      if FRecording then
        RecordVar(Name);
      Result := PNamedVar(P)^.IsNA;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

function TEzParser.IsStrVarNA(const Name: String): Boolean;
var
  I: Integer;
  P: Pointer;
begin
  Result := False;
  for I := 0 to FStrVars.Count - 1 do
  begin
    P := FStrVars[I];
    if CompareStr(PNamedStrVar(P)^.Name, UpperCase(Name)) = 0 then
    begin
      if FRecording then
        RecordStrVar(Name);
      Result := PNamedStrVar(P)^.IsNA;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

function TEzParser.IsFormulaNA(const Name: String): Boolean;
var
  I: Integer;
  P: Pointer;
  SavedContext: PContext;
begin
  Result := False;
  for I := 0 to FFormulae.Count - 1 do
  begin
    P := FFormulae[I];
    if CompareStr(TFormula(P).Name, UpperCase(Name)) = 0 then
    begin
      if FRecording then
      begin
        SavedContext := SaveContext;
        FExpression := UpperCase(Name);
        Self.Result;
        RestoreContext(SavedContext);
      end;
      Result := TFormula(P).IsNA;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

procedure TEzParser.DeleteVar(const Name: String);
var
  V: PNamedVar;
  I: Integer;
begin
  V := GetVarObj(Name, I);
  if V^.Dep.Count > 0 then
    RaiseError(SCannotDelete);
  V^.Dep.Free;
  Dispose(V);
  FVars.Delete(I);
  Exit;
end;

procedure TEzParser.DeleteStrVar(const Name: String);
var
  V: PNamedStrVar;
  I: Integer;
begin
  V := GetStrVarObj(Name, I);
  if V^.Dep.Count > 0 then
    RaiseError(SCannotDelete);
  V^.Dep.Free;
  Dispose(V);
  FVars.Delete(I);
  Exit;
end;

procedure TEzParser.DeleteFormula(const Name: String);
var
  I: Integer;
  V: TFormula;
begin
  V := GetFormulaObj(Name, I);
  DelFormDep(V);
  FFormulae.Delete(I);
  Exit;
end;


function TEzParser.GetFormulaObj(const Name: String; var Index: Integer): TFormula;
var
  I: Integer;
  V: TFormula;
begin
  Result := nil;
  for I := 0 to FFormulae.Count - 1 do
  begin
    V := FFormulae[I];
    if CompareStr(V.Name, UpperCase(Name)) = 0 then
    begin
      Result := V;
      Index := I;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

function TEzParser.GetFormulaObj(const Name: String; var Value: Double) : Boolean;
var
  I: Integer;
  F: TFormula;
begin
  Result := False;
  for I := 0 to FFormulae.Count - 1 do
  begin
    F := FFormulae[I];
    if CompareStr(F.Name, UpperCase(Name)) = 0 then
    begin
      Value := F.Value;
      Result := True;
      Exit;
    end;
  end;
end;

function TEzParser.GetVarObj(const Name: String; var Index: Integer): PNamedVar;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FVars.Count - 1 do
  begin
    Result := PNamedVar(FVars[I]);
    if CompareStr(Result.Name, UpperCase(Name)) = 0 then
    begin
      Index := I;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;

function TEzParser.GetStrVarObj(const Name: String; var Index: Integer): PNamedStrVar;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to FStrVars.Count - 1 do
  begin
    Result := PNamedStrVar(FStrVars[I]);
    if CompareStr(Result.Name, UpperCase(Name)) = 0 then
    begin
      Index := I;
      Exit;
    end;
  end;
  RaiseError(SFunctionError + ' "' + Name + '"');
end;


// Mechanism for building dependency lists. Adds the variable passed as a
// parameter to the dependency list(s) of the formula(e) being parsed.
procedure TEzParser.RecordVar(const Name: String);
var
  I, J, K: Integer;
  V: PNamedVar;
begin
  V := GetVarObj(Name, I);
  // For each formula that is being currently evaluated
  for J := 0 to FRecFormula.Count - 1 do
  begin
    // if the variable already has this formula in its dependency list
    // then break
    for K := 0 to V^.Dep.Count - 1 do
      if TFormula(V^.Dep[K]).Name = TFormula(FRecFormula[J]).Name then
        break;
    // Otherwise, add it.
    if K >= V^.Dep.Count then
      V^.Dep.Add(FRecFormula[J]);
    // The formula that has been added to the variable's dependency list must
    // in turn must have the variable in its dependency list!
    // If the variable is already there in the formula depency list
    // then break
    for K := 0 to TFormula(FRecFormula[J]).FDep.Count - 1 do
      if TFormula(FRecFormula[J]).FDep[K] = V then
        break;
    // Otherwise, add it.
    if (K >= TFormula(FRecFormula[J]).FDep.Count) then
      TFormula(FRecFormula[J]).FDep.Add(V);
  end;
end;

procedure TEzParser.RecordStrVar(const Name: String);
var
  I, J, K: Integer;
  V: PNamedStrVar;
begin
  V := GetStrVarObj(Name, I);
  // For each formula that is being currently evaluated
  for J := 0 to FRecFormula.Count - 1 do
  begin
    for K := 0 to V^.Dep.Count - 1 do
      if TFormula(V^.Dep[K]).Name = TFormula(FRecFormula[J]).Name then
        break;
    // if the formula is not already in the variable's dep list add it.
    if K >= V^.Dep.Count then
      V^.Dep.Add(FRecFormula[J]);
    for K := 0 to TFormula(FRecFormula[J]).FStrDep.Count - 1 do
      if TFormula(FRecFormula[J]).FStrDep[K] = V then
        break;
    // if the formula does not have the variable in its string dep
    // list add it.
    if  (K >= TFormula(FRecFormula[J]).FStrDep.Count) then
      TFormula(FRecFormula[J]).FStrDep.Add(V);
  end;
end;

procedure TEzParser.DelFormDep(Formula: TFormula);
var
  I, J: Integer;
begin
  for I := Formula.FDep.Count - 1 downto 0 do
    for J := PNamedVar(Formula.FDep[I]).Dep.Count - 1 downto 0 do
      if PNamedVar(Formula.FDep[I])^.Dep[J] = Formula then
        PNamedVar(Formula.FDep[I])^.Dep.Delete(J);
  Formula.FDep.Clear;

  for I := Formula.FStrDep.Count - 1 downto 0 do
    for J := PNamedStrVar(Formula.FStrDep[I]).Dep.Count - 1 downto 0 do
      if PNamedStrVar(Formula.FStrDep[I])^.Dep[J] = Formula then
        PNamedStrVar(Formula.FStrDep[I])^.Dep.Delete(J);
  Formula.FStrDep.Clear;

end;

{$ifdef CALCTEST}
procedure TEzParser.GetFormDep(const Name: String; Dep: TStringList);
var
  Form: TFormula;
  DepCount: Integer;
  I,K: Integer;
  VarType: String;

begin
  Form := nil;
  DepCount := 0;
  try
    Form := GetFormulaObj(Name, I);
  except
  end;
  if Form <> nil then
  begin
    for I := 0 to Form.FDep.Count - 1 do
      Dep.Add(PNamedVar(Form.FDep[I])^.Name);
    if Dep.Count > 0 then
    begin
      VarType := 'Double Formula';
      DepCount := Dep.Count;
    end
    else
    begin
      for I := 0 to Form.FStrDep.Count - 1 do
        Dep.Add(PNamedStrVar(Form.FStrDep[I])^.Name);
      if Dep.Count > 0 then
      begin
        VarType := 'String Formula';
        DepCount := Dep.Count;
      end;
    end;
  end
  else
  begin
    for I := 0 to FVars.Count - 1 do
      if PNamedVar(FVars[I])^.Name = Name then
      begin
        for K := 0 to PNamedVar(FVars[I]).Dep.Count - 1 do
          Dep.Add(TFormula(PNamedVar(FVars[I]).Dep[K]).Name);
        Break;
      end;
    if Dep.Count > 0 then
    begin
      VarType := 'Double Variable';
      DepCount := Dep.Count;
    end
    else
    begin
      for I := 0 to FStrVars.Count - 1 do
        if PNamedStrVar(FStrVars[I])^.Name = Name then
        begin
          for K := 0 to PNamedStrVar(FStrVars[I]).Dep.Count - 1 do
            Dep.Add(TFormula(PNamedStrVar(FStrVars[I]).Dep[K]).Name);
          Break;
        end;
      if Dep.Count > 0 then
      begin
        VarType := 'String Variable';
        DepCount := Dep.Count;
      end;
    end;
  end;
  if DepCount = 0 then
  begin
    Dep.Add('No such Variable / Formula');
    Exit;
  end;
  Dep.Add('Type : ' + VarType);
  if Assigned(Form) then
  begin
    Dep.Add('Exp : ' + Form.Exp);
    if Form.FEvaluated then
      Dep.Add('Formula has been evaluated.')
    else
      Dep.Add('Formula yet to be evaluated.');
    if Form.FValid then
      Dep.Add('Formula is valid.')
    else
      Dep.Add('Formula is Invalid.');
  end;
  Dep.Add('Count of Dep : ' + IntToStr(DepCount));
end;
{$endif}

procedure TEzParser.GetFormulaVarDep(const Name: String; Dep: TStringList);
var
  Form: TFormula;
  I: Integer;

begin
  Form := GetFormulaObj(Name, I);
  if not Form.FEvaluated then
  try
    Form.Value;
  except
    Form.StrValue;
  end;
  for I := 0 to Form.FDep.Count - 1 do
    Dep.Add(PNamedVar(Form.FDep[I])^.Name)
end;

procedure TEzParser.GetFormulaStrVarDep(const Name: String; Dep: TStringList);
var
  Form: TFormula;
  I: Integer;

begin
  Form := GetFormulaObj(Name, I);
  if not Form.FEvaluated then
  try
    Form.Value;
  except
    Form.StrValue;
  end;
  for I := 0 to Form.FStrDep.Count - 1 do
    Dep.Add(PNamedStrVar(Form.FStrDep[I])^.Name)
end;

procedure TEzParser.GetFormulaFormDep(const Name: String; Dep: TStringList);
var
  Form: TFormula;
  I: Integer;

begin
  Form := GetFormulaObj(Name, I);
  if not Form.FEvaluated then
    Form.Value;
  for I := 0 to Form.FFormDep.Count - 1 do
      Dep.Add(TFormula(Form.FFormDep[I]).Name)
end;


procedure TEzParser.GetVarDep(const Name: String; Dep: TStringList);
var
  I, K: Integer;
begin
  GetVarObj(Name, I);
  for K := 0 to PNamedVar(FVars[I]).Dep.Count - 1 do
    Dep.Add(TFormula(PNamedVar(FVars[I]).Dep[K]).Name);
end;

procedure TEzParser.GetStrVarDep(const Name: String; Dep: TStringList);
var
  I, K: Integer;
begin
  GetStrVarObj(Name, I);
  for K := 0 to PNamedStrVar(FStrVars[I]).Dep.Count - 1 do
    Dep.Add(TFormula(PNamedStrVar(FStrVars[I]).Dep[K]).Name);
end;

function TEzParser.GetFormulaValue(const Name: String): Double;
var
  I: Integer;
begin
  Result := GetFormulaObj(Name, I).Value;
end;

function TEzParser.GetFormulaStrValue(const Name: String): String;
var
  I: Integer;
begin
  Result := GetFormulaObj(Name, I).StrValue;
end;

procedure TEzParser.AddEvaluation(LogOp: String; Val, Res: Double);
var
  I: Integer;
  V: PEvalResult;
begin
  For I := 0 to FEvalList.Count - 1 do
  begin
    V := FEvalList[I];
    if (V^.Val = Val) and (V^.LogOp = LogOp) then
    begin
      if V^.Res <> Res then
        V^.Res := Res;
      Exit;
    end;
  end;
  New(V);
  V^.LogOp := LogOp;
  V^.Val := Val;
  V^.Res := Res;
  FEvalList.Add(V);
end;

function TEzParser.GetEvalVal(Eval: Double): Double;
var
  I: Integer;
  V: PEvalResult;
begin
  For I := 0 to FEvalList.Count - 1 do
  begin
    V := FEvalList[I];
    FExpression := FloatToStr(Eval) + V^.LogOp + FloatToStr(V^.Val);
    if Self.Result >= 1.0 then
    begin
      GetEvalVal := V^.Res;
      Exit;
    end;
  end;
  GetEvalVal := Eval;
end;

procedure TEzParser.RemoveEvaluation(LogOp: String; Val: Double);
var
  I: Integer;
  V: PEvalResult;
begin
  For I := 0 to FEvalList.Count - 1 do
  begin
    V := FEvalList[I];
    if (V^.Val = Val) and (V^.LogOp = LogOp) then
    begin
      Dispose(V);
      FEvalList.Delete(I);
      Exit;
    end;
  end;
end;

initialization
begin
  FSpecialFunctions := TStringList.Create;
  FSpecialFunctions.Sorted := True;
  FInternalFunctions := TStringList.Create;
  FInternalFunctions.Sorted := True;
  FRangeFunctions := TStringList.Create;
  FRangeFunctions.Sorted := True;
  with FInternalFunctions do
  begin
    Add('TRUNC');
    Add('INT');
    Add('FRAC');
    Add('SIN');
    Add('COS');
    Add('TAN');
    Add('ATAN');
    Add('LN');
    Add('EXP');
    Add('SIGN');
    Add('SGN');
    Add('XSGN');
  end;
  with FSpecialFunctions do
  begin
    Add('IF');
    Add('EMPTY');
    Add('LEN');
    Add('AND');
    Add('OR');
    Add('CONCATENATE');
    Add('CONCAT');
    Add('REPL');
    Add('LEFT');
    Add('RIGHT');
    Add('SUBSTR');
    ADD('TOSTR');
    Add('COMPARE');
    Add('COMPARESTR');
    Add('COMPARETEXT');
    Add('ROUND');
    Add('ISVARNA');
    Add('ISSTRVARNA');
    Add('ISFORMULANA');
    Add('NOT');
    //vk:10/03/99
    Add('EVALUATE');
    Add('SUM');
    Add('MAX');
    Add('MIN');
    Add('AVG');
    Add('COUNT');
  end;
  with FRangeFunctions do
  begin
    Add('CHILDS');
    Add('CHILDSNOTNULL');
    Add('PARENT');
    Add('PARENTNOTNULL');
    Add('PARENTS');
    Add('PARENTSNOTNULL');
    Add('THIS');
    Add('THISNOTNULL');
  end;
end;

finalization
begin
  FInternalFunctions.Free;
  FSpecialFunctions.Free;
  FRangeFunctions.Free;
end;

end.

