Unit EzTimeBar;

{$I Ez.inc}

interface

uses
  Windows, Forms, controls, SysUtils, classes, extctrls, Graphics, EzDateTime,
  Dialogs, Messages, EzPainting, Contnrs;

ResourceString
  SYear = 'Year';
  SQuarter = 'Quarter';
  SMonth = 'Month';
  SWeek = 'Week';
  SDay = 'Day';
  SHour = 'Hour';
  SMinute = 'Minute';

type
  TWMPrint = packed record
    Msg: Cardinal;
    DC: HDC;
    Flags: Cardinal;
    Result: Integer;
  end;

  TWMPrintClient = TWMPrint;

  TEzTimebarEvent = (teLayoutChange, teDateChanged);

  TEzTimebar = class;
  TEzTimescaleBand = class;
  TEzTimescaleBands = class;
  TEzTimescale = class;
  TEzTimescales = class;
  TEzTimescaleStorage = class;
  TEzTimescaleStorageLink = class;

  TEzTimebarHitPosition = (thpOnScale, thpOnBand, thpInCell, thpOnDivider);
  TEzTimebarHitPositions = set of TEzTimebarHitPosition;

  TEzTimebarHitInfo = record
    Scale: TEzTimeScale;
    Band: TEzTimescaleBand;
    BandRect: TRect;
    CellRect: TRect;
    HitPositions: TEzTimebarHitPositions;
    HitPoint: TPoint;
    ScaleCells: Integer;
    MajorCells: Integer;
  end;

  TEzTimebarLink = class(TPersistent)
    FTimebar: TEzTimebar;

  protected
    procedure LayoutChanged; virtual; abstract;
    procedure DateChanged(OldDate: TDateTime); virtual; abstract;
    procedure UpdateState; virtual; abstract;

    procedure TimebarEvent(Event: TEzTimebarEvent; Info: LongInt);

    procedure SetTimebar(Bar: TEzTimebar);

  public
    property Timebar: TEzTimebar read FTimebar write SetTimebar;
  end;

  TEzTimescaleBand = class(TCollectionItem)
  protected
    FAlignment: TAlignment;
    FScale: TScale;
    FDisplayFormat: WideString;
    FHeight: Integer;
    FCount: integer;
    FOffset: Integer;
    FFont: TFont;
    FColor: TColor;
    FOwnerDraw: Boolean;
    FGanttShowsDividers, FGraphShowsDividers: Boolean;
    FParentFont, FParentColor: Boolean;
    FWeekstart: TEzWeekDays;

    function  GetDisplayName: string; override;
    procedure SetAlignment(Value: TAlignment);
    procedure SetScale(Value: TScale);
    procedure SetDisplayformat(Value: WideString);
    procedure SetCount(Value: Integer);
    procedure SetColor(Value: TColor);
    procedure SetFont(Value: TFont);
    procedure SetHeight(Value: Integer);
    procedure SetGanttShowsDividers(Value: Boolean);
    procedure SetGraphShowsDividers(Value: Boolean);
    procedure SetOffset(Value: Integer);
    procedure SetParentFont(Value: Boolean);
    procedure SetParentColor(Value: Boolean);
    procedure SetWeekStart(Value: TEzWeekDays);

  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;

    procedure   Assign(Source: TPersistent); override;

  published
    property Alignment: TAlignment read FAlignment write SetAlignment default taCenter;
    property Color: TColor read FColor write SetColor default clBtnFace;
    property Count: integer read FCount write SetCount default 1;
    property DisplayFormat: WideString read FDisplayFormat write SetDisplayFormat;
    property Font: TFont read FFont write SetFont;
    property GanttShowsDividers: Boolean read FGanttShowsDividers write SetGanttShowsDividers default False;
    property GraphShowsDividers: Boolean read FGraphShowsDividers write SetGraphShowsDividers default False;
    property Height: Integer read FHeight write SetHeight default -1;
    property Offset: integer read FOffset write SetOffset default 0;
    property OwnerDraw: Boolean read FOwnerDraw write FOwnerDraw default False;
    property ParentColor: Boolean read FParentColor write SetParentColor default True;
    property ParentFont: Boolean read FParentFont write SetParentFont default True;
    property Scale: TScale read FScale write SetScale default msDay;
    property WeekStart: TEzWeekDays read FWeekStart write SetWeekStart default wdMonday;
  end;

  TEzTimescaleBandClass = class of TEzTimescaleBand;

  TEzTimescaleBands = class(TOwnedCollection)
  protected
    function  GetBandClass: TEzTimescaleBandClass;
    function  GetBand(Index: Integer): TEzTimescaleBand;
    procedure SetBand(Index: Integer; Value: TEzTimescaleBand);
    procedure Update(Item: TCollectionItem); override;

  public
    constructor Create(AOwner: TEzTimescale); virtual;
    function Add: TEzTimescaleBand;

    property Items[Index: Integer]: TEzTimescaleBand read GetBand write SetBand; default;
  end;

  TEzTimescale = class(TCollectionItem)
  private
    FBands: TEzTimescaleBands;
    FName: string;
    FMajorCellWidth: Integer;

  protected
    function  GetDisplayName: string; override;
    procedure SetName(Value: string);
    procedure SetBands(Value: TEzTimescaleBands);
    procedure SetMajorCellWidth(Value: Integer);
    procedure Update; virtual;

  public
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;

    procedure Assign(Source: TPersistent); override;

  published
    property Bands: TEzTimescaleBands read FBands write SetBands;
    property MajorCellWidth: Integer read FMajorCellWidth write SetMajorCellWidth default 30;
    property Name: string read GetDisplayName write SetName;
  end;

  TEzTimescales = class(TOwnedCollection)
  protected
    function  GetScale(Index: Integer): TEzTimescale;
    procedure SetScale(Index: Integer; Value: TEzTimescale);
    procedure Update(Item: TCollectionItem); override;

  public
    constructor Create(AOwner: TEzTimescaleStorage); virtual;
    function Add: TEzTimescale;

    property Items[Index: Integer]: TEzTimescale read GetScale write SetScale; default;
  end;

  TEzTimescaleStorage = class(TComponent)
  private
    FScales: TEzTimescales;
    FLinks: TObjectList;

  protected
    procedure AddLink(ALink: TEzTimescaleStorageLink);
    procedure RemoveLink(ALink: TEzTimescaleStorageLink);
    procedure NotifyLinks; virtual;
    procedure SetScales(Value: TEzTimescales);

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure Assign(Source: TPersistent); override;

  published
    property Scales: TEzTimescales read FScales write SetScales;
  end;

  TEzTimescaleStorageLink = class(TPersistent)
  private
    FTimebar: TEzTimebar;
    FStorage: TEzTimescaleStorage;

  protected
    procedure Changed; virtual;
    procedure SetStorage(Value: TEzTimescaleStorage);

  public
    constructor Create(AOwner: TEzTimebar);
    destructor Destroy; override;

    property Storage: TEzTimescaleStorage read FStorage write SetStorage;
    property Timebar: TEzTimebar read FTimebar;
  end;

  TOnDrawBandEvent = procedure(Sender: TEzTimeBar; Band: TEzTimescaleBand; ARect: TRect; StartDate: TDateTime) of object;
  TOnDrawBandCellEvent = procedure(Sender: TEzTimeBar; Band: TEzTimescaleBand; ARect: TRect; ADate: TDateTime) of object;
  TOnDateChangingEvent = procedure(Sender: TEzTimeBar; var NewDate: TDateTime) of object;

  TEzTimeBar = class(TCustomControl)
  private
    FUpdateCount: Integer;
    FButtonStyle: TEzButtonStyle;
    FBorderStyle: TBorderStyle;
    FTimebarLinks: TList;
    FDate: TDateTime;
    FFixedBands: Integer;
    FFixedHeight: Integer;
    FHitInfo: TEzTimebarHitInfo;
    FMajorScale: TScale;
    FMajorCount: Integer;
    FMajorOffset: Integer;
    FMajorWeekStart: TEzWeekDays;
    FUserCanEdit: Boolean;
    FColSizing: Boolean;
    FOrgCellWidth: Integer;
    FAutoUpdateMajorSettings: Boolean;
    FScaleStorageLink: TEzTimescaleStorageLink;
    FScaleIndex: Integer;
    FMinDate, FMaxDate: TDateTime;

    FOnDateChange: TNotifyEvent;
    FOnDateChanging: TOnDateChangingEvent;
    FOnLayoutChange: TNotifyEvent;
    FOnDrawBand: TOnDrawBandEvent;
    FOnDrawCell: TOnDrawBandCellEvent;

  protected
    function  AcquireFocus: Boolean;
    procedure AddTimebarLink(Link: TEzTimebarLink);
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DblClick; override;
    function  GetActiveScale: TEzTimeScale;
    function  GetFixedBands: Integer;
    function  GetFixedHeight: Integer;
    function  GetMajorExplanation: string;
    function  GetMajorWidth: Integer;
    function  GetScaleStorage: TEzTimescaleStorage;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure NotifyLinks(Event: TEzTimebarEvent; Info: Longint);
    procedure Paint; override;
    procedure RemoveTimebarLink(Link: TEzTimebarLink);
    procedure ScalesUpdated; virtual;
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure SetButtonStyle(Style: TEzButtonStyle);
    procedure SetDate(Value: TDateTime);
    procedure SetScales(Value: TEzTimeScales);
    procedure SetScaleStorage(Value: TEzTimescaleStorage);
    procedure SetScaleIndex(Value: Integer);
    procedure SetMajorScale(M:TScale);
    procedure SetMajorCount(C: integer);
    procedure SetMajorOffset(C: integer);
    procedure SetMajorWeekStart(Value: TEzWeekDays);
    procedure SetMajorWidth(Value: Integer);
    procedure SetMaxDate(Value: TDateTime);
    procedure SetMinDate(Value: TDateTime);

    procedure DoDateChanged(OldDate: TDateTime); virtual;
    procedure DoDateChanging(var Newdate: TDateTime); virtual;

    procedure LayoutChanged(DoInvalidate: Boolean = True); virtual;

    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure WMPrint(var Message: TWMPrint); message WM_PRINT;
    procedure WMWindowPosChanging(var Message: TWMWindowPosChanging); message WM_WINDOWPOSCHANGING;
    procedure WMMouseWheel(var Message: TWMMouseWheel); message WM_MOUSEWHEEL;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(Source: TPersistent); override;
    procedure BeginUpdate;
    function  DateTime2X(D: TDateTime): integer;
    procedure EndUpdate;
    procedure GetHitInfo(X, Y: Integer; var HitInfo: TEzTimebarHitInfo);
    function  HasActiveScale: Boolean;
    procedure OpenSetupDialog;
    procedure PaintBand(ARect: TRect; StartDate: TDateTime; Band: TEzTimescaleBand); virtual;
    function  Ticks2Date(Start: TDateTime; Ticks: integer) : TDateTime;
    function  X2DateTime(X : integer) : TDateTime;

    property  ActiveScale: TEzTimescale read GetActiveScale;
    property  Canvas;
    property  FixedBands: Integer read GetFixedBands;
    property  FixedHeight: Integer read GetFixedHeight;
    property  MajorWidth: Integer read GetMajorWidth write SetMajorWidth;

  published
    property AutoUpdateMajorSettings: Boolean read FAutoUpdateMajorSettings write FAutoUpdateMajorSettings;
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsNone;
    property ButtonStyle: TEzButtonStyle read FButtonStyle write SetButtonStyle default btsThinButton;

    property MajorScale: TScale read FMajorScale write SetMajorScale default msDay;
    property MajorCount: Integer read FMajorCount write SetMajorCount default 1;
    property MajorWeekStart: TEzWeekDays read FMajorWeekStart write SetMajorWeekStart default wdMonday;
    property MajorOffset: Integer read FMajorOffset write SetMajorOffset default 0;
    property MaxDate: TDateTime read FMaxDate write SetMaxDate;
    property MinDate: TDateTime read FMinDate write SetMinDate;

    property Anchors;
    property Align default alTop;
//    property BorderStyle;
    property Color default clBtnFace;
    property ColSizing: Boolean read FColSizing;
    property Enabled;
    property Font;
    property Date: TDateTime read FDate write SetDate;
    property PopupMenu;
    property ScaleStorage: TEzTimescaleStorage read GetScaleStorage write SetScaleStorage;
    property ScaleIndex: Integer read FScaleIndex write SetScaleIndex default -1;
    property UserCanEdit: Boolean read FUserCanEdit write FUserCanEdit default True;

    property OnDateChanging: TOnDateChangingEvent read FOnDateChanging write FOnDateChanging;
    property OnDateChange: TNotifyEvent read FOnDateChange write FOnDateChange;
    property OnLayoutChange: TNotifyEvent read FOnLayoutChange write FOnLayoutChange;
    property OnResize;
    property OnDrawBand: TOnDrawBandEvent read FOnDrawBand write FOnDrawBand;
    property OnDrawCell: TOnDrawBandCellEvent read FOnDrawCell write FOnDrawCell;
    property OnDblClick;

    property Visible;
  end;

  function AddTicks(Scale: TScale; ScaleCount: integer; Date: TDateTime; Ticks: integer) : TDateTime;
  function Shadow(C: TColor; F:Double) : TColor;
  function HighLight(C: TColor; F:Double) : TColor;
  function RoundDate(Scale: TScale; ScaleCount, ScaleOffset: integer; dtDate: TDateTime; WeekStart: TEzWeekDays=wdMonday) : TDateTime;
  function TruncDate(Scale: TScale; ScaleCount, ScaleOffset: integer; dtDate: TDateTime; WeekStart: TEzWeekDays=wdMonday) : TDateTime;
  function RoundUnits(Units: Double; SnapSize: Double): Double;
  function TruncUnits(Units: Double; SnapSize: Double): Double;

implementation

uses math, EzTimebarDialog
{$IFDEF EZ_D6}
  , DateUtils
{$ENDIF}
  ;

function Shadow(C: TColor; F:Double) : TColor;
begin
  // System defined color ?
  if (C and $80000000) > 0 then
    Result := clBtnShadow
  else
    Result := RGB(Round(Byte(C)/F), Round(Byte(C shr 8)/F), Round(Byte(C shr 16)/F));
end;

function HighLight(C: TColor; F:Double) : TColor;
begin
  // System defined color ?
  if (C and $80000000) > 0 then
    Result := clBtnHighlight
  else
  if (C = clBlack) then
    Result := clGray
  else
  Result := RGB(min(255, Round(Byte(C       )*F)),
                min(255, Round(Byte(C shr 8 )*F)),
                min(255, Round(Byte(C shr 16)*F)));
end;

function AddTicks(Scale: TScale; ScaleCount: integer; Date: TDateTime; Ticks: integer) : TDateTime;
begin
  Result := Date;
  Ticks := Ticks*ScaleCount;
  case Scale of
    msYear: Result := AddYears(Date, Ticks);
    msQuarter: Result := AddMonths(Date, Ticks*3);
    msMonth: Result := AddMonths(Date, Ticks);
    msWeek: Result := Date + 7 * Ticks;
    msDay: Result := Date + Ticks;
    msHour: Result := Date + Ticks/24;
    msMinute: Result := Date + Ticks/(24*60);
  end;
end;

function RoundDate(Scale: TScale; ScaleCount, ScaleOffset: integer; dtDate: TDateTime; WeekStart: TEzWeekDays) : TDateTime;
var
  Next: TDateTime;

begin
  Result := TruncDate(Scale, ScaleCount, ScaleOffset, dtDate, WeekStart);
  Next := AddTicks(Scale, ScaleCount, Result, 1);
  if (dtDate-Result) >= (Next-dtDate) then
    Result := Next;
end;

function TruncDate(Scale: TScale; ScaleCount, ScaleOffset: Integer; dtDate: TDateTime; WeekStart: TEzWeekDays) : TDateTime;
var
  Day, Month, Year, Hour, Min, Sec, MSec: WORD;
  DOW, M: Integer; // Day of week

begin
  case Scale of
    msYear:
    begin
      DecodeDate(dtDate, Year, Month, Day);
      Day := 1; Month := 1;
      M := (Year-ScaleOffset) mod ScaleCount;
      if M<>0 then Dec(Year, M);
      Result := EncodeDate(Year, Month, Day);
    end;
    msQuarter:
    begin
      DecodeDate(dtDate, Year, Month, Day);
      dec(Month, (Month-1) mod 3);
      M:=(Month-1-ScaleOffset*3) mod ScaleCount*3;
      if M<>0 then Dec(Month, M);
      Day := 1;
      Result := EncodeDate(Year, Month, Day);
    end;
    msMonth:
    begin
      DecodeDate(dtDate, Year, Month, Day);
      M := (Month-1-ScaleOffset) mod ScaleCount;
      if M<>0 then
      begin
        Dec(Month, M);
        if Month>12 then
        begin
          inc(Year, Month div 12);
          Month := Month mod 12;
        end;
      end;
      Day := 1;
      Result := EncodeDate(Year, Month, Day);
    end;
    msWeek:
    begin
      DecodeDate(dtDate, Year, Month, Day);
      DOW := DayOfTheWeek(dtDate)-1;
      if DOW > Ord(WeekStart) then
        Result := trunc(dtDate)-(DOW-Ord(WeekStart))
      else if DOW < Ord(WeekStart) then
        Result := trunc(dtDate)-(7-Ord(WeekStart)+DOW)
      else
        Result := EncodeDate(Year, Month, Day);
    end;
    msDay:
    begin
      Day := trunc(dtDate);
      M := (Day-ScaleOffset) mod ScaleCount;
      if M<>0 then Dec(Day, M);
      Result := Day;
    end;
    msHour:
    begin
      DecodeTime(dtDate, Hour, Min, Sec, MSec);
      M := (Hour-ScaleOffset) mod ScaleCount;
      if M<>0 then
      begin
        Dec(Hour, M);
        if Hour>24 then
        begin
          dtDate := trunc(dtDate) + Hour div 24;
          Hour := Hour mod 24;
        end;
      end;
      Min := 0; Sec := 0; MSec := 0;
      Result := trunc(dtDate) + EncodeTime(Hour, Min, Sec, 0);
    end;
    msMinute:
    begin
      DecodeTime(dtDate, Hour, Min, Sec, MSec);
      M := (Min-ScaleOffset) mod ScaleCount;
      if M<>0 then
      begin
        Dec(Min, M);
        if Min>60 then
        begin
          inc(Hour, Min div 60);
          dtDate := trunc(dtDate) + Hour div 24;
          Hour := Hour mod 24;
          Min := Min mod 60;
        end;
      end;
      Sec := 0; MSec := 0;
      Result := Trunc(dtDate) + EncodeTime(Hour, Min, Sec, 0);
    end;
  else
    Result := 0;
  end;
end;

function RoundUnits(Units: Double; SnapSize: Double): Double;
begin
  Result := Round(Units/SnapSize)*SnapSize;
end;

function TruncUnits(Units: Double; SnapSize: Double): Double;
begin
  Result := Trunc(Units/SnapSize)*SnapSize;
end;


//==============================================================================
//
// TEzTimescaleBand
//
//==============================================================================
constructor TEzTimescaleBand.Create(Collection: TCollection);
begin
  FOwnerDraw := False;
  FFont := TFont.Create;
  FColor := clBtnFace;
  FAlignment := taCenter;
  FScale := msDay;
  FDisplayFormat := DayFormats[0];
  FCount := 1;
  FParentColor := True;
  FParentFont := True;
  FHeight := -1;
  FWeekStart := wdMonday;
  FGanttShowsDividers := False;
  FGraphShowsDividers := False;
  inherited;
end;

destructor TEzTimescaleBand.Destroy;
begin
  inherited;
  FFont.Destroy;
end;

function TEzTimescaleBand.GetDisplayName: string;
begin
  case Scale of
    msYear:     Result := SYear;
    msQuarter:  Result := SQuarter;
    msMonth:    Result := SMonth;
    msWeek:     Result := SWeek;
    msDay:      Result := SDay;
    msHour:     Result := SHour;
    msMinute:   Result := SMinute;
  else
    Result := '';
  end;
end;

procedure TEzTimescaleBand.Assign(Source: TPersistent);
begin
  if Source is TEzTimescaleBand then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      FAlignment := TEzTimescaleBand(Source).Alignment;
      FScale := TEzTimescaleBand(Source).Scale;
      FDisplayFormat := TEzTimescaleBand(Source).DisplayFormat;
      FCount := TEzTimescaleBand(Source).Count;
      FHeight := TEzTimescaleBand(Source).Height;
      FFont.Assign(TEzTimescaleBand(Source).Font);
      FColor := TEzTimescaleBand(Source).Color;
      FOwnerDraw := TEzTimescaleBand(Source).OwnerDraw;
      FParentFont := TEzTimescaleBand(Source).ParentFont;
      FParentColor := TEzTimescaleBand(Source).ParentColor;
      FWeekstart := TEzTimescaleBand(Source).WeekStart;
      FGanttShowsDividers := TEzTimescaleBand(Source).FGanttShowsDividers;
      FGraphShowsDividers := TEzTimescaleBand(Source).FGraphShowsDividers;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

procedure TEzTimescaleBand.SetAlignment(Value: TAlignment);
begin
  if Value <> FAlignment then
  begin
    FAlignment := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetScale(Value: TScale);
begin
  if Value <> FScale then
  begin
    FScale := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetDisplayformat(Value: WideString);
begin
  if Value <> FDisplayFormat then
  begin
    FDisplayFormat := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetCount(Value: Integer);
begin
  if Value <> FCount then
  begin
    FCount := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetColor(Value: TColor);
begin
  if Value <> FColor then
  begin
    FParentColor := False;
    FColor := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetFont(Value: TFont);
begin
  FParentFont := False;
  FFont.Assign(Value);
  Changed(False);
end;

procedure TEzTimescaleBand.SetHeight(Value: Integer);
begin
  if Value <> FHeight then
  begin
    FHeight := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetGanttShowsDividers(Value: Boolean);
begin
  if Value <> FGanttShowsDividers then
  begin
    FGanttShowsDividers := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetGraphShowsDividers(Value: Boolean);
begin
  if Value <> FGraphShowsDividers then
  begin
    FGraphShowsDividers := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetOffset(Value: Integer);
begin
  Value := max(0, Value);
  if Value <> FOffset then
  begin
    FOffset := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetParentFont(Value: Boolean);
begin
  if Value <> FParentFont then
  begin
    FParentFont := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetParentColor(Value: Boolean);
begin
  if Value <> FParentColor then
  begin
    FParentColor := Value;
    Changed(False);
  end;
end;

procedure TEzTimescaleBand.SetWeekStart(Value: TEzWeekDays);
begin
  if Value <> FWeekStart then
  begin
    FWeekStart := Value;
    Changed(False);
  end;
end;

//==============================================================================
//
// TEzTimescaleBands
//
//==============================================================================
constructor TEzTimescaleBands.Create(AOwner: TEzTimescale);
begin
  inherited Create(AOwner, GetBandClass);
end;

function TEzTimescaleBands.Add: TEzTimescaleBand;
begin
  Result := TEzTimescaleBand(inherited Add);
end;

function TEzTimescaleBands.GetBandClass: TEzTimescaleBandClass;
begin
  Result := TEzTimescaleBand;
end;

function TEzTimescaleBands.GetBand(Index: Integer): TEzTimescaleBand;
begin
  Result := TEzTimescaleBand(inherited Items[Index]);
end;

procedure TEzTimescaleBands.SetBand(Index: Integer; Value: TEzTimescaleBand);
begin
  TEzTimescaleBand(inherited Items[Index]).Assign(Value);
end;

procedure TEzTimescaleBands.Update(Item: TCollectionItem);
begin
  if inherited GetOwner<>nil then
    TEzTimeScale(inherited GetOwner).Update;
end;

//==============================================================================
//
// TEzTimescale
//
//==============================================================================
constructor TEzTimescale.Create(Collection: TCollection);
begin
  inherited;
  FBands := TEzTimescaleBands.Create(Self);
  FMajorCellWidth := 30;  
end;

destructor TEzTimescale.Destroy;
begin
  inherited;
  FBands.Destroy;
end;

procedure TEzTimescale.Assign(Source: TPersistent);
begin
  if Source is TEzTimescale then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      FBands.Assign(TEzTimescale(Source).Bands);
      FMajorCellWidth := TEzTimescale(Source).MajorCellWidth;
      FName := TEzTimescale(Source).Name;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TEzTimescale.GetDisplayName: string;
begin
  if FName = '' then
    Result := inherited GetDisplayName else
    Result := FName;
end;

procedure TEzTimescale.SetName(Value: string);
begin
  FName := Value;
end;

procedure TEzTimescale.SetBands(Value: TEzTimescaleBands);
begin
  FBands.Assign(Value);
end;

procedure TEzTimescale.SetMajorCellWidth(Value: Integer);
begin
  if Value<>FMajorCellWidth then
  begin
    FMajorCellWidth:= Value;
    Update;
  end;
end;

procedure TEzTimescale.Update;
begin
  if (Collection<>nil) then
     TEzTimeScales(Collection).Update(Self);
end;

//==============================================================================
//
// TEzTimeScales
//
//==============================================================================
constructor TEzTimescales.Create(AOwner: TEzTimescaleStorage);
begin
  inherited Create(AOwner, TEzTimeScale);
end;

function TEzTimescales.Add: TEzTimescale;
begin
  Result := TEzTimescale(inherited Add);
end;

function  TEzTimescales.GetScale(Index: Integer): TEzTimescale;
begin
  Result := TEzTimescale(inherited Items[Index]);
end;

procedure TEzTimescales.SetScale(Index: Integer; Value: TEzTimescale);
begin
  TEzTimescale(Items[Index]).Assign(Value);
end;

procedure TEzTimescales.Update(Item: TCollectionItem);
begin
  if inherited GetOwner<>nil then
    TEzTimescaleStorage(inherited GetOwner).NotifyLinks;
end;


//==============================================================================
//
// TEzTimeScaleStorage
//
//==============================================================================
constructor TEzTimescaleStorage.Create(AOwner: TComponent);
begin
  inherited;
  FScales := TEzTimescales.Create(Self);
  FLinks := TObjectList.Create;
  FLinks.OwnsObjects := False;
end;

destructor TEzTimescaleStorage.Destroy;
begin
  while FLinks.Count>0 do
    TEzTimescaleStorageLink(FLinks[0]).Storage := nil;

  inherited;
  FScales.Destroy;
  FLinks.Destroy;
end;

procedure TEzTimescaleStorage.Assign(Source: TPersistent);
begin
  if Source is TEzTimescaleStorage then
  begin
    FScales.Assign(TEzTimescaleStorage(Source).Scales);
    NotifyLinks;
  end else
    inherited;
end;

procedure TEzTimescaleStorage.AddLink(ALink: TEzTimescaleStorageLink);
begin
  FLinks.Add(ALink);
end;

procedure TEzTimescaleStorage.RemoveLink(ALink: TEzTimescaleStorageLink);
begin
  FLinks.Remove(ALink);
end;

procedure TEzTimescaleStorage.NotifyLinks;
var
  i: Integer;
begin
  for i:=0 to FLinks.Count-1 do
    TEzTimescaleStorageLink(FLinks[i]).Changed;
end;

procedure TEzTimescaleStorage.SetScales(Value: TEzTimescales);
begin
  FScales.Assign(Value);
end;

//==============================================================================
//
// TEzTimescaleStorageLink
//
//==============================================================================
constructor TEzTimescaleStorageLink.Create(AOwner: TEzTimebar);
begin
  inherited Create;
  FTimebar := AOwner;
end;

destructor TEzTimescaleStorageLink.Destroy;
begin
  inherited;
  Storage := nil;
end;

procedure TEzTimescaleStorageLink.Changed;
begin
  if not (csDestroying in FTimebar.ComponentState) then
    FTimebar.ScalesUpdated;
end;

procedure TEzTimescaleStorageLink.SetStorage(Value: TEzTimescaleStorage);
begin
  if Value<>FStorage then
  begin
    if FStorage<>nil then
      FStorage.RemoveLink(Self);
    FStorage:=Value;
    if FStorage<>nil then
      FStorage.AddLink(Self);
    Changed;
  end;
end;


// =----------------------------------------------------------------------------=
// =----------------------------------------------------------------------------=
// TEzTimebarLink: Link class between timebars and components using timebars
// =----------------------------------------------------------------------------=
// =----------------------------------------------------------------------------=
procedure TEzTimebarLink.SetTimebar(Bar: TEzTimebar);
begin
  if Bar <> FTimebar then
  begin
    if FTimebar <> nil then FTimebar.RemoveTimebarLink(Self);
    if Bar <> nil then Bar.AddTimebarLink(Self);
  end;
end;

procedure TEzTimebarLink.TimebarEvent(Event: TEzTimebarEvent; Info: LongInt);
begin
  case Event of
    teLayoutChange:
      LayoutChanged;
    teDateChanged:
      DateChanged(TDateTime(Pointer(Info)^));
  end;
end;

// =----------------------------------------------------------------------------=
function TEzTimeBar.AcquireFocus: Boolean;
begin
  Result := True;
  if CanFocus and not (csDesigning in ComponentState) then
  begin
    SetFocus;
    Result := Focused;
  end;
end;

procedure TEzTimeBar.AddTimebarLink(Link: TEzTimebarLink);
begin
  FTimebarLinks.Add(Link);
  Link.FTimebar := Self;
  Link.UpdateState;
end;

procedure TEzTimeBar.CreateParams(var Params: TCreateParams);
const
  BorderStyles: array[TBorderStyle] of DWORD = (0, WS_BORDER);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or BorderStyles[FBorderStyle];
    if NewStyleControls and Ctl3D and (FBorderStyle = bsSingle) then
    begin
      Style := Style and not WS_BORDER;
      ExStyle := ExStyle or WS_EX_CLIENTEDGE;
    end;
    WindowClass.style := WindowClass.style and not (CS_HREDRAW or CS_VREDRAW);
  end;
end;

procedure TEzTimeBar.CMCtl3DChanged(var Message: TMessage);
begin
  if NewStyleControls and (FBorderStyle = bsSingle) then RecreateWnd;
  inherited;
end;

procedure TEzTimeBar.WMMouseWheel(var Message: TWMMouseWheel);
var
 ScrollCount, ScrollLines: integer;

begin
  SystemParametersInfo(SPI_GETWHEELSCROLLLINES, 0, @ScrollLines, 0);
  ScrollCount := -ScrollLines * Message.WheelDelta div WHEEL_DELTA;
  Date := AddTicks(MajorScale, MajorCount, Date, ScrollCount);
end;

procedure TEzTimeBar.RemoveTimebarLink(Link: TEzTimebarLink);
begin
  Link.FTimebar := nil;
  FTimebarLinks.Remove(Link);
  Link.UpdateState;
end;

procedure TEzTimeBar.ScalesUpdated;
var
  i: Integer;
  MostGranulateBand: TEzTimescaleBand;
  MaxDelta, Delta: TDateTime;

begin
  if (FUpdateCount>0) or not HasActiveScale then
  begin
    LayoutChanged(True);
    Exit;
  end;

  if FAutoUpdateMajorSettings then
  begin
    MostGranulateBand := nil;
    MaxDelta := END_OF_TIME;
    for i:=0 to ActiveScale.Bands.Count-1 do
    begin
      with ActiveScale.Bands[i] do
      begin
        Delta := AddTicks(Scale, Count, 0, 1);
        if Delta<MaxDelta then
        begin
          MostGranulateBand := ActiveScale.Bands[i];
          MaxDelta := Delta;
        end;
      end;
    end;
    if MostGranulateBand<>nil then
    begin
      FMajorScale := MostGranulateBand.Scale;
      FMajorCount := MostGranulateBand.Count;
      FMajorOffset := MostGranulateBand.Offset;
      FMajorWeekStart := MostGranulateBand.WeekStart;
      LayoutChanged(False);
    end;
  end;
  LayoutChanged(True);

{
  NotifyLinks(teLayoutChange, 0);
  Invalidate;
}
end;

procedure TEzTimeBar.NotifyLinks(Event: TEzTimebarEvent; Info: Longint);
var
  I: Integer;
begin
  for I := FTimebarLinks.Count - 1 downto 0 do
    with TEzTimebarLink(FTimebarLinks[I]) do
      TimebarEvent(Event, Info);
end;

procedure TEzTimeBar.BeginUpdate;
begin
  inc(FUpdateCount);
end;

procedure TEzTimeBar.EndUpdate;
begin
  if FUpdateCount>0 then
    dec(FUpdateCount);
end;

procedure TEzTimeBar.Assign(Source: TPersistent);
var
  CopyFrom: TEzTimebar;
begin
  if (Source is TEzTimebar) then
  begin
    BeginUpdate;
    try
      CopyFrom := Source as TEzTimebar;
      ScaleStorage := CopyFrom.ScaleStorage;
      FScaleIndex := CopyFrom.ScaleIndex;
      FAutoUpdateMajorSettings := CopyFrom.AutoUpdateMajorSettings;
      FButtonStyle := CopyFrom.ButtonStyle;
      FMajorScale := CopyFrom.MajorScale;
      FMajorCount := CopyFrom.MajorCount;
      FMajorOffset := CopyFrom.MajorOffset;
      FMajorWeekStart := CopyFrom.MajorWeekStart;
      FMinDate := CopyFrom.MinDate;
      FMaxDate := CopyFrom.MaxDate;
      Font.Assign(CopyFrom.Font);
      Color := CopyFrom.Color;
      FUserCanEdit := CopyFrom.UserCanedit;
      if Showing then
        invalidate;
    finally
      EndUpdate;
    end;
    LayoutChanged(False);
  end;
end;

procedure TEzTimeBar.DblClick;
begin
  inherited;
  if FUserCanEdit then
    OpenSetupDialog;
end;

function TEzTimeBar.GetActiveScale: TEzTimeScale;
begin
  if not HasActiveScale then
    raise Exception.Create('Timebar''s scales are not setup properly, timebar cannot be used.');
  Result := ScaleStorage.Scales[ScaleIndex];
end;

function TEzTimeBar.GetFixedBands: Integer;
begin
  if FFixedHeight=-1 then GetFixedHeight;
  Result := FFixedBands;
end;

function TEzTimeBar.GetFixedHeight: Integer;
var
  i: Integer;

begin
  if FFixedHeight=-1 then
  begin
    FFixedHeight:=0;
    FFixedBands:=0;
    for i:=0 to ActiveScale.Bands.Count-1 do
      with ActiveScale.Bands[i] do
      begin
        if Height <> -1 then
        begin
          inc(FFixedBands);
          inc(FFixedHeight, Height);
        end;
      end;
  end;
  Result := FFixedHeight;
end;

procedure TEzTimeBar.GetHitInfo(X, Y: Integer; var HitInfo: TEzTimebarHitInfo);
var
  VariableHeight, i, P, H, R: Integer;
  ABand: TEzTimescaleBand;
  LeftDate, RightDate: TDateTime;

begin
  ZeroMemory(@HitInfo, sizeof(HitInfo));
  HitInfo.HitPoint := Point(X,Y);
  if not PtInRect(ClientRect, Point(X,Y)) then Exit;
  HitInfo.HitPositions := [thpOnScale];
  HitInfo.Scale := ActiveScale;
  if HitInfo.Scale.Bands.Count>0 then
  begin
    VariableHeight := (Height-FixedHeight) div (HitInfo.Scale.Bands.Count-FixedBands);

    // Determine scale at position
    i:=0;
    P:=0;
    while (HitInfo.Band=nil) and (i<HitInfo.Scale.Bands.Count) do
    begin
      ABand := HitInfo.Scale.Bands[i];
      if ABand.Height = -1 then
        H := VariableHeight else
        H := ABand.Height;

      if (Y>=P) and (Y<P+H) then
      begin
        HitInfo.Band := ABand;
        Include(HitInfo.HitPositions, thpOnBand);
        SetRect(HitInfo.BandRect, Left, P, Width, P+H);
      end else
      begin
        inc(P, H);
        inc(i);
      end;
    end;

    // We are over a Band, now test for cell or divider hit
    if thpOnBand in HitInfo.HitPositions then
    begin
      ABand := HitInfo.Band;
      RightDate:=TruncDate(ABand.Scale, ABand.Count, ABand.Offset, Date, ABand.WeekStart);
      P:=DateTime2X(RightDate);
      i:=1;

      while (P<HitInfo.BandRect.Right) and (HitInfo.HitPositions * [thpInCell, thpOnDivider] = []) do
      begin
        LeftDate:=RightDate;
        RightDate:=AddTicks(ABand.Scale, ABand.Count, LeftDate, 1);
        R:=DateTime2X(RightDate);

        if (X>=P+4) and (X<R-4) then
        begin
          Include(HitInfo.HitPositions, thpInCell);
          SetRect(HitInfo.CellRect, P, HitInfo.BandRect.Top, R, HitInfo.BandRect.Bottom);
          HitInfo.MajorCells := TickCount(MajorScale, MajorCount, Date, X2DateTime(R));
        end
        else if (X>=R-4) and (X<R+4) then
        begin
          Include(HitInfo.HitPositions, thpOnDivider);
          HitInfo.ScaleCells:=i;
          SetRect(HitInfo.CellRect, P, HitInfo.BandRect.Top, R, HitInfo.BandRect.Bottom);
          HitInfo.MajorCells := max(1, TickCount(MajorScale, MajorCount, Date, X2DateTime(R)));
        end else
        begin
          P:=R;
          inc(i);
        end;
      end;
    end;
  end;
end;

function TEzTimeBar.GetMajorExplanation: string;
begin
  Result := IntToStr(MajorCount);
end;

function TEzTimeBar.GetMajorWidth: Integer;
begin
  if HasActiveScale then
    Result := ActiveScale.MajorCellWidth else
    Result := 30;
end;

function TEzTimeBar.GetScaleStorage: TEzTimescaleStorage;
begin
  Result := FScaleStorageLink.Storage;
end;

function TEzTimeBar.DateTime2X(D: TDateTime): integer;
var
  {4-10-01 BF: use double type instead of integer to avoid 0 result}
  DaySize: double;

begin
  DaySize := 0;
  case MajorScale of
    msYear:
      DaySize := MajorWidth / (365*MajorCount);
    msQuarter:
      DaySize := MajorWidth / (91*MajorCount); // Average days of 3 months
    msMonth:
      DaySize := MajorWidth / (30*MajorCount);
    msWeek:
      DaySize := MajorWidth / (7*MajorCount);
    msDay:
      DaySize := MajorWidth / MajorCount;
    msHour:
      DaySize := (MajorWidth*24) / MajorCount;
    msMinute:
      DaySize := (MajorWidth*24*60) / MajorCount;
  end;
  Result := Round((D-FDate) * DaySize);
end;

function TEzTimeBar.HasActiveScale: Boolean;
begin
  Result := (ScaleStorage<>nil) and (ScaleIndex<>-1) and (ScaleIndex<ScaleStorage.Scales.Count);
end;

constructor TEzTimeBar.Create(AOwner: TComponent);
begin
  inherited;
  FScaleIndex := -1;
  FScaleStorageLink := TEzTimescaleStorageLink.Create(Self);
  DoubleBuffered := True;
  FTimebarLinks := TList.Create;
  FFixedHeight := -1;
  FButtonStyle := btsThinButton;
  FBorderStyle := bsNone;

  FColSizing := False;
  FUserCanEdit := True;
  Align := alTop;
  Height := 40;
  Color := clBtnFace;

  FAutoUpdateMajorSettings := True;
  FMajorScale := msDay;
  FMajorCount := 1;
  FMajorOffset := 0;
  FMaxDate := 0;
  FMinDate := 0;

  FDate := Floor(Now);
end;

destructor TEzTimeBar.Destroy;
begin
  inherited;
  FScaleStorageLink.Destroy;
  FTimebarLinks.Destroy;
end;

procedure TEzTimeBar.DoDateChanged(OldDate: TDateTime);
begin
  NotifyLinks(teDateChanged, LongInt(@OldDate));
  if Assigned(FOnDateChange) then FOnDateChange(Self);
end;

procedure TEzTimeBar.DoDateChanging(var Newdate: TDateTime);
begin
  if Assigned(FOnDateChanging) then FOnDateChanging(Self, NewDate);
end;

procedure TEzTimeBar.LayoutChanged(DoInvalidate: Boolean = True);
begin
  // Trunc date
  Date := Date;
  FFixedHeight:=-1;
  NotifyLinks(teLayoutChange, 0);
  if Assigned(FOnLayoutChange) then FOnLayoutChange(Self);
  if DoInvalidate then
    Invalidate;
end;

procedure TEzTimeBar.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
  begin
    if (AComponent = ScaleStorage) then
      ScaleStorage := nil;
  end;
end;

procedure TEzTimeBar.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if HasActiveScale and AcquireFocus and (Button = mbLeft) then
  begin
    GetHitInfo(X, Y, FHitInfo);
    if thpOnDivider in FHitInfo.HitPositions then
    begin
      MouseCapture;
      FColSizing := True;
      FOrgCellWidth := FHitInfo.Scale.MajorCellWidth;
    end;
  end;
  inherited;
end;

procedure TEzTimeBar.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  if not HasActiveScale then Exit;
  if FColSizing then
    FHitInfo.Scale.MajorCellWidth := max(5, Trunc(X/FHitInfo.MajorCells))
  else
  begin
    GetHitInfo(X, Y, FHitInfo);
    if thpOnDivider in FHitInfo.HitPositions then
      Cursor := crSizeWE else
      Cursor := crDefault;
  end;
  inherited;
end;

procedure TEzTimeBar.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if FColSizing then
  begin
    FColSizing := False;
    LayoutChanged(False);
  end;
  Cursor := crDefault;
end;

procedure TEzTimeBar.OpenSetupDialog;
begin
  with TEzTimebarDlg.Create(Self) do
  try
    Timebar := Self;
    Showmodal;
  finally
    Destroy;
  end;
end;

function TEzTimeBar.Ticks2Date(Start: TDateTime; Ticks: integer) : TDateTime;
begin
  Result := AddTicks(MajorScale, MajorCount, Start, Ticks);
end;

procedure TEzTimeBar.Paint;
var
  H: Integer;
  StartDate: TDateTime;
  Clip, ARect: TRect;
  i, p, VariableHeight: Integer;
  Band: TEzTimescaleBand;

begin
  if not HasActiveScale then Exit;

  if EzIsPrinting then
    Clip := ClientRect else
    Clip := Canvas.ClipRect;

  if ActiveScale.Bands.Count>0 then
  begin
    VariableHeight := (Height-FixedHeight) div (ActiveScale.Bands.Count-FixedBands);

    p:=0;
    for i:=0 to ActiveScale.Bands.Count-1 do
    begin
      Band := ActiveScale.Bands[i];
      if Band.Height = -1 then
        H := VariableHeight else
        H := Band.Height;

      if (P<Clip.Bottom) and (P+H>Clip.Top) then
      begin
        StartDate := TruncDate(Band.Scale, Band.Count, Band.Offset, X2DateTime(Clip.Left), Band.WeekStart);
        SetRect(ARect, DateTime2X(StartDate), P, Clip.Right, P+H);
        if Assigned(FOnDrawBand) and Band.OwnerDraw then
          FOnDrawBand(Self, Band, ARect, StartDate) else
          PaintBand(ARect, StartDate, Band);
      end;
      inc(P, H);
    end;
  end;
end;

procedure TEzTimeBar.PaintBand(ARect: TRect; StartDate: TDateTime; Band: TEzTimescaleBand);
const
  Flags: array[TAlignment] of Cardinal = (DT_LEFT, DT_RIGHT, DT_CENTER);

var
  PrintDate: TDateTime;
  EndPos: Integer;
  CopyRect: TRect;
  ButtonFlags: Cardinal;
  TextFlags: Cardinal;
  TruncMaxDate: TDateTime;
  TruncMinDate: TDateTime;

begin
  TruncMinDate := TruncDate(Band.Scale, Band.Count, Band.Offset, FMinDate, Band.WeekStart);
  TruncMaxDate := TruncDate(Band.Scale, Band.Count, Band.Offset, FMaxDate, Band.WeekStart);

  if FButtonStyle = btsFlatRect then
    ButtonFlags := BF_BOTTOMLEFT else
    ButtonFlags := BF_RECT;

  TextFlags := Flags[Band.Alignment] or (DT_VCENTER + DT_SINGLELINE + DT_WORD_ELLIPSIS);
  if Band.ParentColor then
    Canvas.Brush.Color := Color else
    Canvas.Brush.Color := Band.Color;

  if Band.ParentFont then
    Canvas.Font := Font else
    Canvas.Font := Band.Font;

  EndPos := ARect.Right;
  ARect.Left := max(ARect.Left, 0);

  while ARect.Left < EndPos do
  begin
    PrintDate := StartDate;
    StartDate := AddTicks(Band.Scale, Band.Count, StartDate, 1);
    ARect.Right := DateTime2X(StartDate);
    if ARect.Left = ARect.Right then
      continue;

    CopyRect := ARect;

    if (FButtonStyle <> btsXPButton) and (ARect.Left <> 0) then
      //
      // Draw tick line
      //
    begin
      Canvas.Pen.Color := clBlack;
      Canvas.MoveTo(CopyRect.Left, CopyRect.Top);
      Canvas.LineTo(CopyRect.Left, CopyRect.Bottom);
      inc(CopyRect.Left);
    end;

    CopyRect.Left := max(ARect.Left, 0);
    CopyRect.Right := min(Width, ARect.Right);

    if Assigned(FOnDrawCell) and Band.OwnerDraw then
      FOnDrawCell(Self, Band, ARect, PrintDate) else
      EzRenderTextRect(
          Canvas,
          CopyRect,
          EzFormatDateTime(Band.DisplayFormat, PrintDate, TruncMinDate, TruncMaxDate),
          FButtonStyle,
          bstNormal,
          ButtonFlags,
          TextFlags
      );

    ARect.Left := ARect.Right;
  end;
end;

procedure TEzTimeBar.SetBorderStyle(Value: TBorderStyle);
begin
  if FBorderStyle <> Value then
  begin
    FBorderStyle := Value;
    RecreateWnd;
  end;
end;

procedure TEzTimeBar.SetButtonStyle(Style: TEzButtonStyle);
begin
  if Style <> FButtonStyle then
  begin
    FButtonStyle := Style;
    Invalidate;
  end;
end;

procedure TEzTimeBar.SetDate(Value: TDateTime);
var
  Saved: TDateTime;
begin
  Value := TruncDate(MajorScale, MajorCount, MajorOffset, Value);
  if FDate<>Value then
  begin
    Saved:=Value;
    DoDateChanging(Value);
    if Saved<>Value then
    begin
      Value := TruncDate(MajorScale, MajorCount, MajorOffset, Value);
      if FDate=Value then Exit;
    end;
    Saved:=FDate;
    FDate:=Value;
    Invalidate;
    DoDateChanged(Saved);
  end;
end;

procedure TEzTimeBar.SetScales(Value: TEzTimeScales);
begin
end;

procedure TEzTimeBar.SetMajorScale(M:TScale);
begin
  if M <> FMajorScale then
  begin
    FMajorScale := M;
    LayoutChanged;
  end;
end;

procedure TEzTimeBar.SetMajorCount(C: integer);
begin
  if C <= 0 then C := 1;
  if C <> FMajorCount then
  begin
    FMajorCount := C;
    LayoutChanged;
  end;
end;

procedure TEzTimeBar.SetMajorOffset(C: integer);
begin
  if C <> FMajorOffset then
  begin
    FMajorOffset := C;
    LayoutChanged;
  end;
end;

procedure TEzTimeBar.SetMajorWeekStart(Value: TEzWeekDays);
begin
  if Value <> FMajorWeekStart then
  begin
    FMajorWeekStart := Value;
    LayoutChanged;
  end;
end;

procedure TEzTimeBar.SetMajorWidth(Value: Integer);
begin
  if HasActiveScale then
    ActiveScale.MajorCellWidth := Value;
end;

procedure TEzTimeBar.SetMaxDate(Value: TDateTime);
begin
  if Value <> FMaxDate then
  begin
    FMaxDate := Value;
    LayoutChanged;
  end;
end;

procedure TEzTimeBar.SetMinDate(Value: TDateTime);
begin
  if Value <> FMinDate then
  begin
    FMinDate := Value;
    LayoutChanged;
  end;
end;

procedure TEzTimeBar.SetScaleStorage(Value: TEzTimescaleStorage);
begin
  if Value <> ScaleStorage then
  begin
    FScaleStorageLink.Storage := Value;
    ScalesUpdated;
  end;
end;

procedure TEzTimeBar.SetScaleIndex(Value: Integer);
begin
  if ScaleStorage<>nil then
    Value := min(Value, ScaleStorage.Scales.Count-1);

  if Value <> FScaleIndex then
  begin
    FScaleIndex := Value;
    ScalesUpdated;
  end;
end;

function TEzTimeBar.X2DateTime(X : integer) : TDateTime;
var
  DaySize: Extended;
  D: double;
begin
  DaySize := 0;
  case MajorScale of
    msYear:
      DaySize := MajorWidth / 365;
    msQuarter:
      DaySize := MajorWidth / 91; // Average days of 3 months
    msMonth:
      DaySize := MajorWidth / 30;
    msWeek:
      DaySize := MajorWidth / 7;
    msDay:
      DaySize := MajorWidth;
    msHour:
      DaySize := MajorWidth * 24;
    msMinute:
      DaySize := MajorWidth * 24 * 60;
  end;
  D := X;
  Result := FDate + D * FMajorCount / DaySize;
end;

procedure TEzTimeBar.WMPrint(var Message: TWMPrint);
var
  Rgn: HRgn;
  R: TRect;

begin
  // Draw only if the window is visible or visibility is not required.
  if ((Message.Flags and PRF_CHECKVISIBLE) = 0) or IsWindowVisible(Handle) then
  begin
    EzBeginPrint;
    try
      R := ClientRect;
      if (BorderStyle = bsSingle) then OffsetRect(R, 2, 2);
      LPtoDP(Message.DC, R, 2);
      Rgn := CreateRectRgnIndirect(R);
      SelectClipRgn(Message.DC, Rgn);
      PaintTo(Message.DC, 0, 0);
      SelectClipRgn(Message.DC, 0);
      DeleteObject(Rgn);
    finally
      EzEndPrint;
    end;
  end;
end;

procedure TEzTimeBar.WMWindowPosChanging(var Message: TWMWindowPosChanging);
begin
  inherited;
  if (Message.WindowPos.cy <> Height) or (Message.WindowPos.cx <> Width) then
    Invalidate;
end;

end.
