unit EzCalendarGrid;

{$I Ez.inc}

interface

uses
  Windows, Classes, Grids, Messages, stdctrls, SysUtils, EzDateTime;

type
  TEzCalendarGrid = class(TCustomGrid)
  private
    FDate: TDateTime;
    FMonthOffset: Integer;
    FOnChange: TNotifyEvent;
    FOnDrawCell: TDrawCellEvent;
    FReadOnly: Boolean;
    FWeekStart: TEzWeekDays;
    FUpdating: Boolean;
    FUseCurrentDate: Boolean;
    function  GetCellDate(ACol, ARow: Integer): TDateTime;
    function  GetCellText(ACol, ARow: Integer): string;
    function  GetDateElement(Index: Integer): Integer;
    procedure SetCalendarDate(Value: TDateTime);
    procedure SetDateElement(Index: Integer; Value: Integer);
    procedure SetWeekStart(Value: TEzWeekDays);
    procedure SetUseCurrentDate(Value: Boolean);
    function  StoreCalendarDate: Boolean;
  protected
    procedure Change; dynamic;
    procedure ChangeMonth(Delta: Integer);
    procedure Click; override;
    function  DaysThisMonth: Integer; virtual;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    function  SelectCell(ACol, ARow: Longint): Boolean; override;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;

  public
    constructor Create(AOwner: TComponent); override;
    procedure NextMonth;
    procedure NextYear;
    procedure PrevMonth;
    procedure PrevYear;
    procedure UpdateCalendar; virtual;

    property Canvas;
    property CalendarDate: TDateTime  read FDate write SetCalendarDate stored StoreCalendarDate;
    property CellDate[ACol, ARow: Integer]: TDateTime read GetCellDate;
    property CellText[ACol, ARow: Integer]: string read GetCellText;

  published
    property Align;
    property BorderStyle;
    property Color;
    property Font;
    property Visible;

    property Day: Integer index 3  read GetDateElement write SetDateElement stored False;
    property DefaultDrawing;
    property Month: Integer index 2 read GetDateElement write SetDateElement stored False;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnDrawCell: TDrawCellEvent read FOnDrawCell write FOnDrawCell;
    property ReadOnly: Boolean read FReadOnly write FReadOnly default False;
    property WeekStart: TEzWeekDays read FWeekStart write SetWeekStart;
    property UseCurrentDate: Boolean read FUseCurrentDate write SetUseCurrentDate default True;
    property Year: Integer index 1  read GetDateElement write SetDateElement stored False;
  end;


implementation

constructor TEzCalendarGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FUseCurrentDate := True;
  FixedCols := 0;
  FixedRows := 1;
  ColCount := 7;
  RowCount := 7;
  ScrollBars := ssNone;
  Options := Options - [goRangeSelect] + [goDrawFocusSelected];
  FDate := Now();
  UpdateCalendar;
end;

procedure TEzCalendarGrid.Change;
begin
  if Assigned(FOnChange) then FOnChange(Self);
end;

procedure TEzCalendarGrid.Click;
var
  TheCellText: string;
begin
  inherited Click;
  TheCellText := CellText[Col, Row];
  if TheCellText <> '' then Day := StrToInt(TheCellText);
end;

function TEzCalendarGrid.DaysThisMonth: Integer;
begin
  Result := MonthDays[IsLeapYear(Year)][Month];
end;

procedure TEzCalendarGrid.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
var
  TheText: string;

begin
  if Assigned(FOnDrawCell) then
    FOnDrawCell(Self, ACol, ARow, ARect, AState);

  if DefaultDrawing then
  begin
    TheText := CellText[ACol, ARow];
    with ARect, Canvas do
      TextRect(ARect, Left + (Right - Left - TextWidth(TheText)) div 2,
        Top + (Bottom - Top - TextHeight(TheText)) div 2, TheText);
  end;

end;

function TEzCalendarGrid.GetCellDate(ACol, ARow: Integer): TDateTime;
var
  DayNum: Integer;
  
begin
  DayNum := FMonthOffset + ACol + (ARow - 1) * 7;
  if (DayNum < 1) or (DayNum > DaysThisMonth) then
    Result := 0 else
    Result := EncodeDate(Year, Month, DayNum);
end;

function TEzCalendarGrid.GetCellText(ACol, ARow: Integer): string;
var
  DayNum: Integer;
begin
  if ARow = 0 then  { day names at tops of columns }
  begin
    DayNum := (Ord(WeekStart) + ACol) mod 7 + 2;
    if DayNum>7 then DayNum:=1;
      Result := {$IFDEF XE3}FormatSettings.{$ENDIF}ShortDayNames[DayNum];
  end
  else
  begin
    DayNum := FMonthOffset + ACol + (ARow - 1) * 7;
    if (DayNum < 1) or (DayNum > DaysThisMonth) then Result := ''
    else Result := IntToStr(DayNum);
  end;
end;

function TEzCalendarGrid.SelectCell(ACol, ARow: Longint): Boolean;
begin
  if ((not FUpdating) and FReadOnly) or (CellText[ACol, ARow] = '') then
    Result := False
  else Result := inherited SelectCell(ACol, ARow);
end;

procedure TEzCalendarGrid.SetCalendarDate(Value: TDateTime);
begin
  FDate := Value;
  UpdateCalendar;
  Change;
end;

function TEzCalendarGrid.StoreCalendarDate: Boolean;
begin
  Result := not FUseCurrentDate;
end;

function TEzCalendarGrid.GetDateElement(Index: Integer): Integer;
var
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(FDate, AYear, AMonth, ADay);
  case Index of
    1: Result := AYear;
    2: Result := AMonth;
    3: Result := ADay;
    else Result := -1;
  end;
end;

procedure TEzCalendarGrid.SetDateElement(Index: Integer; Value: Integer);
var
  AYear, AMonth, ADay: Word;
begin
  if Value > 0 then
  begin
    DecodeDate(FDate, AYear, AMonth, ADay);
    case Index of
      1: if AYear <> Value then AYear := Value else Exit;
      2: if (Value <= 12) and (Value <> AMonth) then AMonth := Value else Exit;
      3: if (Value <= DaysThisMonth) and (Value <> ADay) then ADay := Value else Exit;
      else Exit;
    end;
    if ADay > MonthDays[IsLeapYear(AYear)][AMonth] then
      ADay := MonthDays[IsLeapYear(AYear)][AMonth];
    FDate := EncodeDate(AYear, AMonth, ADay);
    FUseCurrentDate := False;
    UpdateCalendar;
    Change;
  end;
end;

procedure TEzCalendarGrid.SetWeekStart(Value: TEzWeekDays);
begin
  if Value <> FWeekStart then
  begin
    FWeekStart := Value;
    UpdateCalendar;
  end;
end;

procedure TEzCalendarGrid.SetUseCurrentDate(Value: Boolean);
begin
  if Value <> FUseCurrentDate then
  begin
    FUseCurrentDate := Value;
    if Value then
    begin
      FDate := Date; { use the current date, then }
      UpdateCalendar;
    end;
  end;
end;

{ Given a value of 1 or -1, moves to Next or Prev month accordingly }
procedure TEzCalendarGrid.ChangeMonth(Delta: Integer);
var
  AYear, AMonth, ADay: Word;
  NewDate: TDateTime;
  CurDay: Integer;
begin
  DecodeDate(FDate, AYear, AMonth, ADay);
  CurDay := ADay;
  if Delta > 0 then ADay := MonthDays[IsLeapYear(AYear)][AMonth]
  else ADay := 1;
  NewDate := EncodeDate(AYear, AMonth, ADay);
  NewDate := NewDate + Delta;
  DecodeDate(NewDate, AYear, AMonth, ADay);
  if MonthDays[IsLeapYear(AYear)][AMonth] > CurDay then ADay := CurDay
  else ADay := MonthDays[IsLeapYear(AYear)][AMonth];
  CalendarDate := EncodeDate(AYear, AMonth, ADay);
end;

procedure TEzCalendarGrid.PrevMonth;
begin
  ChangeMonth(-1);
end;

procedure TEzCalendarGrid.NextMonth;
begin
  ChangeMonth(1);
end;

procedure TEzCalendarGrid.NextYear;
begin
  if IsLeapYear(Year) and (Month = 2) and (Day = 29) then Day := 28;
  Year := Year + 1;
end;

procedure TEzCalendarGrid.PrevYear;
begin
  if IsLeapYear(Year) and (Month = 2) and (Day = 29) then Day := 28;
  Year := Year - 1;
end;

procedure TEzCalendarGrid.UpdateCalendar;
var
  AYear, AMonth, ADay: Word;
  FirstDate: TDateTime;
begin
  FUpdating := True;
  try
    DecodeDate(FDate, AYear, AMonth, ADay);
    FirstDate := EncodeDate(AYear, AMonth, 1);
    FMonthOffset := 2 - ((DayOfWeek(FirstDate) - Ord(WeekStart) + 6) mod 7); { day of week for 1st of month }
    if FMonthOffset = 2 then FMonthOffset := -5;
    MoveColRow((ADay - FMonthOffset) mod 7, (ADay - FMonthOffset) div 7 + 1,
      False, False);
    Invalidate;
  finally
    FUpdating := False;
  end;
end;

procedure TEzCalendarGrid.WMSize(var Message: TWMSize);
var
  GridLines: Integer;
begin
  GridLines := 6 * GridLineWidth;
  DefaultColWidth := (Message.Width - GridLines) div 7;
  DefaultRowHeight := (Message.Height - GridLines) div 7;
end;

end.
