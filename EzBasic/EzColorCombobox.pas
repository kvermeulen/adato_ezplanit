unit EzColorCombobox;

interface

uses
  Windows, Messages, SysUtils, StdCtrls, Classes, Graphics, Controls, Dialogs;

const
  TColors: array[0..15] of TColor = (clBlack, clMaroon, clGreen, clOlive,
                                     clNavy, clPurple, clTeal, clGray, clSilver,
                                     clRed, clLime, clYellow, clBlue, clFuchsia,
                                     clAqua, clWhite);

type
  TEzColorComboBox = class(TCustomComboBox)
  private
    FSelectedColor: TColor;
    FColorDialog: TColorDialog;

  protected
    function  GetSelectedColor: TColor;
    procedure SetSelectedColor(Value: TColor);
    procedure Change; override;
    function  ColorDialog : TColor;
    function  Color2Index(Value: TColor): Integer;
    procedure DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState); override;

  public
    constructor Create(AOwner: TComponent); override;
    procedure CreateWnd; override;

    property ItemIndex stored False;

  published
    property SelectedColor: TColor read GetSelectedColor write SetSelectedColor;

    property BevelEdges;
    property BevelInner;
    property BevelKind default bkNone;
    property BevelOuter;
    property Style; {Must be published before Items}
    property Anchors;
    property BiDiMode;
    property CharCase;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property DropDownCount;
    property Enabled;
    property Font;
    property ImeMode;
    property ImeName;
    property ItemHeight;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnClick;
{$IFDEF EZ_D6}
    property OnCloseUp;
{$ENDIF}
{$IFDEF EZ_D5}
    property OnContextPopup;
{$ENDIF}
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawItem;
    property OnDropDown;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
{$IFDEF EZ_D6}
    property OnSelect;
{$ENDIF}
    property OnStartDock;
    property OnStartDrag;
    property Items; { Must be published after OnMeasureItem }
  end;

implementation

uses Forms;

constructor TEzColorComboBox.Create(AOwner: TComponent);
begin
  inherited;
  Style := csOwnerDrawFixed;
  FSelectedColor := clBlack;
end;

procedure TEzColorComboBox.CreateWnd;
begin
  inherited;
  Items.CommaText := 'Black,Maroon,Green,Olive,Navy,Purple,Teal,' +
                     'Gray,Silver,Red,Lime,Yellow,Blue,Fuchsia,Aqua,White,<user...>';

  if ItemIndex = -1 then
    ItemIndex := Color2Index(FSelectedColor);
end;

procedure TEzColorComboBox.Change;
begin
  if ItemIndex = (High(TColors) + 1) then
    FSelectedColor := ColorDialog;
  inherited;
end;

function TEzColorComboBox.ColorDialog: TColor;
begin
  if not Assigned(FColorDialog) then
    FColorDialog := TColorDialog.Create(Self);
  FColorDialog.Color := FSelectedColor;
  if FColorDialog.Execute then
    Result := FColorDialog.Color
  else
    Result := FSelectedColor;
end;

function TEzColorComboBox.Color2Index(Value: TColor): Integer;
begin
  Result := 0;
  While (Result <= High(TColors)) and (TColors[Result] <> Value) do
    Inc(Result);
end;

function  TEzColorComboBox.GetSelectedColor: TColor;
begin
  if ItemIndex = (High(TColors) + 1) then
    Result := FSelectedColor
  else
    Result := TColors[ItemIndex];
  inherited;
end;

procedure TEzColorComboBox.DrawItem(Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  Inc(Rect.Left, 20);
  // Paint default
  inherited;
  Dec(Rect.Left, 20);
  Rect.Right := Rect.Left + 20;
  Canvas.Brush.Color := Color;
  Canvas.FillRect(Rect);
  InFlateRect(Rect, -2, -2);
  if Index = (High(TColors) + 1) then
    Canvas.Brush.Color := FSelectedColor
  else
    Canvas.Brush.Color := TColors[Index];
  Canvas.FillRect(Rect);
  Canvas.Brush.Color := Color;
end;

procedure TEzColorComboBox.SetSelectedColor(Value: TColor);
begin
  if FSelectedColor <> Value then
  begin
    ItemIndex := Color2Index(Value);
    FSelectedColor := Value;
    Invalidate;
  end;
end;

end.
