unit EzColumnGrid;

{$I Ez.inc}

interface

uses
  Windows, Messages, classes, Controls, Dialogs, graphics, Grids,
  Math, SysUtils, StdCtrls;

type
  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TEditStyle = (esAuto, esSimple, esEllipsis, esPickList);
  TEditButtonEvent = procedure (Sender: TObject) of object;
  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TPiDataType = (dtString, dtMemo, dtInteger, dtBoolean, dtFloat,
              	 dtCurrency, dtDate, dtTime, dtDateTime, dtRecords);

  TEzColumnGridFlag = (igfCanInsert, igfShowWarnings, igfUseInsertCache);
  TEzColumnGridFlags = set of TEzColumnGridFlag;

  TEzColumnGridState = (igsInserting, igsEmpty);
  TEzColumnGridStates = set of TEzColumnGridState;

  TCustomEzColumnGrid = class;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TPopupListbox = class(TCustomListbox)
  private
    FSearchText: String;
    FSearchTickCount: Longint;

  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  end;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TPiInplaceEdit = class(TInplaceEdit)
  private
    FButtonWidth: Integer;
    FActiveList: TWinControl;
    FPickList: TPopupListbox;
    FListVisible: Boolean;
    FPressed: Boolean;
    FTracking: Boolean;
    FEditStyle: TEditStyle;

    procedure ListMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StopTracking;
    procedure TrackButton(X,Y: Integer);
    procedure CMCancelMode(var Message: TCMCancelMode); message CM_CancelMode;
    procedure WMCancelMode(var Message: TMessage); message WM_CancelMode;
    procedure WMKillFocus(var Message: TMessage); message WM_KillFocus;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message wm_LButtonDblClk;
    procedure WMPaint(var Message: TWMPaint); message wm_Paint;
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SetCursor;
    function ButtonRect: TRect;
    function OverButton(const P: TPoint): Boolean;

  protected
    procedure BoundsChanged; override;
    procedure CloseUp(Accept: Boolean);
  	procedure WndProc(var Message: TMessage); override;
    procedure PaintWindow(DC: HDC); override;
    procedure DoDropDownKeys(var Key: Word; Shift: TShiftState);
    procedure DropDown;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure SetEditStyle(Value: TEditStyle);
    procedure UpdateContents; override;

  public
    constructor Create(Owner: TComponent); override;

	published
  	property Color;
    property EditMask;
    property  EditStyle: TEditStyle read FEditStyle write SetEditStyle;
  end;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TEzColumnGridColumn = class(TCollectionItem)
  private
    FTitle: string;
    FDropDownRows: Cardinal;
    FWidth: Integer;
    FPickList: TStrings;
    FAlignment: TAlignment;
    FReadonly: Boolean;
    FEditMask: string;
    FDisplayMask: string;
    FDataType: TPiDataType;
    FDefault: Variant;
    FEditStyle: TEditStyle;

  protected
    function  GetDisplayName: string; override;
    function  GetHasPicklist: Boolean;
    function  GetPickList: TStrings;
    procedure SetPickList(Value: TStrings);
    procedure SetDefault(const Value: Variant);
    procedure SetWidth(Value: Integer);
    procedure SetEditMask(Mask: string);
    procedure SetDisplayMask(Mask: string);


  public
    procedure   Assign(Source: TPersistent); override;
    constructor Create(Collection: TCollection); override;
    destructor  Destroy; override;
    procedure   RestoreDefaults;

    property HasPicklist: Boolean read GetHasPicklist;

  published
    property  Alignment: TAlignment read FAlignment write FAlignment default taLeftJustify;
    property  Default: Variant read FDefault write SetDefault;
    property  DisplayMask: string read FDisplayMask write SetDisplayMask;
    property  DataType: TPiDataType read FDataType write FDataType default dtString;
    property  DropDownRows: Cardinal read FDropDownRows write FDropDownRows default 7;
    property  PickList: TStrings read GetPickList write SetPickList;
    property  Width: Integer read FWidth write SetWidth default 64;
    property  ReadOnly: Boolean read FReadOnly write FReadOnly default False;
    property  EditMask: string read FEditMask write SetEditMask;
    property  EditStyle: TEditStyle read FEditStyle write FEditStyle;    
    property  Title: string read FTitle write FTitle;
  end;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TEzColumnGridColumns = class(TCollection)
  private
    FGrid: TCustomEzColumnGrid;

  protected
    function GetEzColumnGridColumn(Index: Integer): TEzColumnGridColumn;
    function GetOwner: TPersistent; override;
    procedure SetEzColumnGridColumn(Index: Integer; Value: TEzColumnGridColumn);
    procedure Update(Item: TCollectionItem); override;

  public
    constructor Create(Grid: TCustomEzColumnGrid; ColumnClass: TCollectionItemClass);
    function  Add: TEzColumnGridColumn;

    property Grid: TCustomEzColumnGrid read FGrid;
    property Items[Index: Integer]: TEzColumnGridColumn read GetEzColumnGridColumn write SetEzColumnGridColumn; default;
  end;

  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TInsertAction = (iaContinue, iaAbort, iaApply);
  TInsertEvent = procedure (Sender: TObject; ARow: Longint; Data: Variant; var Action: TInsertAction) of object;
  TCanEditShowEvent = procedure (Sender: TObject; ACol, ARow: LongInt; var DoShow: Boolean) of object;
  TGetValueEvent = procedure (Sender: TObject; ACol, ARow: LongInt; var Value: Variant) of object;
  TSetValueEvent = procedure (Sender: TObject; ACol, ARow: LongInt; const Value: Variant) of object;
  //=---------------------------------------------------------------------------=
  //=---------------------------------------------------------------------------=
  TCustomEzColumnGrid = class(TCustomGrid)
  private
    FFlags: TEzColumnGridFlags;
    FState: TEzColumnGridStates;
    FInsertData: Variant;
    FUpdateCount: Integer;
    FLayoutCount: Integer;
    FColumns: TEzColumnGridColumns;
    FDrawBitmap: TBitmap;
    FDefaultDrawing: Boolean;

    // Events
    FOnCanEditShow: TCanEditShowEvent;
    FOnColumnMoved: TMovedEvent;
    FOnDrawCell: TDrawCellEvent;
    FOnInsert: TInsertEvent;
    FBeginInsert: TInsertEvent;
    FOnRowMoved: TMovedEvent;
    FOnSelectCell: TSelectCellEvent;
    FOnTopLeftChanged: TNotifyEvent;
    FOnEditButtonClick: TEditButtonEvent;
    FOnGetValue: TGetValueEvent;
    FOnSetValue: TSetValueEvent;

  protected
    function  AcquireLayoutLock: Boolean;
    function  CanEditShow: Boolean; override;
    function  CreateEditor: TInplaceEdit; override;
    procedure ColumnMoved(FromIndex, ToIndex: Longint); override;
    procedure ColWidthsChanged; override;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure ReadColumns(Reader: TReader);
    procedure WriteColumns(Writer: TWriter);
    procedure DefineProperties(Filer: TFiler); override;
    procedure DoBeginInsert(Sender: TObject; ARow: Longint; Data: Variant; var Action: TInsertAction); virtual;
    procedure DoOnInsert(Sender: TObject; ARow: Longint; Data: Variant; var Action: TInsertAction); virtual;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure DrawCellTo(ACanvas: TCanvas; ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); virtual;
    function  GetDisplayText(ACol, ARow: Longint): string; virtual;
    function  GetEditMask(ACol, ARow: Longint): string; override;
    function  GetEditText(ACol, ARow: Longint): string; override;
    function  GetEmpty: Boolean;
    function  GetFixedCols: Integer;
    function  GetPlainValue(ACol, ARow: Longint): Variant; virtual;
    function  GetSelectedIndex: Integer;
    function  GetValue(ACol, ARow: Longint) : Variant; virtual;
    function  GetCells(ACol, ARow: Integer) : string;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure Loaded; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure RowMoved(FromIndex, ToIndex: Longint); override;
    function  SelectCell(ACol, ARow: Longint): Boolean; override;
    procedure SetCells(ACol, ARow: Integer; aValue: string);
    procedure SetColumns(Value: TEzColumnGridColumns);
    procedure SetColumnAttributes; virtual;
    procedure SetEditText(ACol, ARow: integer; const Value: string); override;
    procedure SetEmpty(Value: Boolean);
    procedure SetFixedCols(Value: Integer);
    procedure SetSelectedIndex(Value: Integer);
    procedure SetState(Value: TEzColumnGridStates);
    procedure SetValue(ACol, ARow: Longint; const Value: Variant); virtual;
    procedure StartInsert(ARow: Integer); virtual;
    procedure EndInsert; virtual;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure BeginLayout;
    procedure EndLayout;
    procedure LayoutChanged;
    procedure InternalLayout;
    function  CancelInsert : Boolean; virtual;
    procedure TopLeftChanged; override;
    procedure WndProc(var Message: TMessage); override;

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure EditButtonClick; dynamic;

    property Empty: Boolean read GetEmpty write SetEmpty stored False;
    property Flags: TEzColumnGridFlags read FFlags write FFlags;
    property InsertData: Variant read FInsertData;
    property SelectedIndex: Integer read GetSelectedIndex write SetSelectedIndex;
    property State: TEzColumnGridStates read FState write SetState stored False;
    property Col;
    property Columns: TEzColumnGridColumns read FColumns write SetColumns;
    property Cells[ACol, ARow: Integer]: string read GetCells write SetCells;
    property DefaultDrawing: Boolean read FDefaultDrawing write FDefaultDrawing default True;
    property FixedCols: Integer read GetFixedCols write SetFixedCols default 1;
    property OnInsert: TInsertEvent read FOnInsert write FOnInsert;
    property BeginInsert: TInsertEvent read FBeginInsert write FBeginInsert;
    property OnCanEditShow: TCanEditShowEvent read FOnCanEditShow write FOnCanEditShow;
    property OnColumnMoved: TMovedEvent read FOnColumnMoved write FOnColumnMoved;
    property OnDrawCell: TDrawCellEvent read FOnDrawCell write FOnDrawCell;
    property OnEditButtonClick: TEditButtonEvent read FOnEditButtonClick
      write FOnEditButtonClick;
    property OnRowMoved: TMovedEvent read FOnRowMoved write FOnRowMoved;
    property OnSelectCell: TSelectCellEvent read FOnSelectCell write FOnSelectCell;
    property OnTopLeftChanged: TNotifyEvent read FOnTopLeftChanged write FOnTopLeftChanged;
    property OnGetValue: TGetValueEvent read FOnGetValue write FOnGetValue;
    property OnSetValue: TSetValueEvent read FOnSetValue write FOnSetValue;
    property Row;
  end;

  TEzColumnGrid = class(TCustomEzColumnGrid)
  public
    property InsertData;
    property Canvas;
    property Col;
    property EditorMode;
    property GridHeight;
    property GridWidth;
    property InplaceEditor;
    property LeftCol;
    property Selection;
    property Row;
    property RowHeights;
    property SelectedIndex;
    property State;
    property TabStops;
    property TopRow;

  published
    property Columns stored False;
    property FixedCols;
    property FixedRows;
    property Flags;
    property OnInsert;
    property BeginInsert;
    property OnCanEditShow;
    property Align;
    property Anchors;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Constraints;
    property Ctl3D;
    property DefaultRowHeight;
    property DefaultDrawing;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property FixedColor;
    property Font;
    property GridLineWidth;
    property Options;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ScrollBars;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property VisibleColCount;
    property VisibleRowCount;
    property OnClick;
    property OnColumnMoved;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnDrawCell;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnEditButtonClick;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnRowMoved;
    property OnSelectCell;
    property OnStartDock;
    property OnStartDrag;
    property OnTopLeftChanged;
    property OnGetValue;
    property OnSetValue;
    property RowCount;
  end;

  function FormatValue(const FormatStr: string; const Arg: Variant; ArgType: TPiDataType) : string;
  function TranslateTo(const FormatStr: string; sValue: string; ArgType: TPiDataType) : Variant;
  function IsEmptyValue(const Mask: string; const sValue: string) : Boolean;
  function TranslateMask(const Mask: string; ArgType: TPiDataType) : string;

implementation

uses Forms, EzPainting,

{$IFDEF EZ_D6}
  Variants, MaskUtils;
{$ELSE}
  Mask;
{$ENDIF}

function IsEmptyValue(const Mask: string; const sValue: string) : Boolean;
begin
  Result :=
    (Length(sValue) = 0) or
    (sValue = FormatMaskText(Mask, ''));
end;

function TranslateTo(const FormatStr: string; sValue: string; ArgType: TPiDataType) : Variant;
const
  Factors: array[0..2] of longword = (24, 1440, 86400);

var
  ExVal: Extended;
  dPos, pPos: Integer;

begin
  case ArgType of
    dtInteger:
      Result := StrToInt(sValue);

    dtBoolean:
    begin
      if Pos(';', FormatStr) <> 0 then
      begin
        if LowerCase(sValue[1]) = LowerCase(FormatStr[1]) then
          Result := True
        else
          Result := False;
      end
      else
        if (sValue[1] = 't') or (sValue[1] = 'T') or (sValue[1] = '1') then
          Result := True
        else
          Result := False;
    end;

    dtFloat:
    begin
      dPos := Pos('d', sValue);
      pPos := Pos({$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, sValue);

      if (dPos <> 0) or (pPos <> 0) then
      begin
        if dPos <> 0 then
          ExVal := StrToInt(Copy(sValue, dPos - 2, 2))
        else
          ExVal := 0;

        if pPos <> 0 then
        begin
          sValue := Copy(sValue, pPos - 2, 1000);
          // Use dPos to store hours
          dPos := StrToInt(Copy(sValue, 1, 2));
          sValue[1] := '0';
          sValue[2] := '0';
          ExVal := ExVal + StrToTime(sValue) + dPos/24;
        end;
      end
      else
        ExVal := StrToFloat(sValue);
      Result := ExVal;
    end;

    dtCurrency:
      Result := StrToCurr(sValue);
    dtDate:
      Result := StrToDate(sValue);
    dtTime:
      Result := StrToTime(sValue);
    dtDateTime:
      Result := StrToDateTime(sValue);
    else
      Result := sValue;
  end;
end;

function TranslateMask(const Mask: string; ArgType: TPiDataType) : string;
begin
  case ArgType of
    dtFloat:
    begin
      Result := StringReplace(Mask, '.', {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator, [rfReplaceAll]);
      Result := StringReplace(Result, ':', {$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, [rfReplaceAll]);
    end;

    dtCurrency:
      Result := StringReplace(Mask, '.', {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator, [rfReplaceAll]);

    dtDate:
      Result := StringReplace(Mask, '/', {$IFDEF XE3}FormatSettings.{$ENDIF}DateSeparator, [rfReplaceAll]);

    dtTime:
      Result := StringReplace(Mask, ':', {$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, [rfReplaceAll]);

    dtDateTime:
    begin
      Result := StringReplace(Mask, '/', {$IFDEF XE3}FormatSettings.{$ENDIF}DateSeparator, [rfReplaceAll]);
      Result := StringReplace(Result, ':', {$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, [rfReplaceAll]);
    end;
  else
    Result := Mask;
  end;
end;

function FormatValue(const FormatStr: string; const Arg: Variant; ArgType: TPiDataType) : string;
var
  ExVal: Extended;
  hPos, dPos: Integer;
  days, hour, min, sec, msec: Word;
begin
  Result := '';
  if varIsNull(Arg) or varIsEmpty(Arg) then Exit;

  case ArgType of
    dtInteger:
    begin
      ExVal := Integer(Arg);
      Result := FormatFloat(FormatStr, ExVal); //Format(FormatStr, [Integer(Arg)]);
    end;

    dtBoolean:
    begin
      dPos := Pos(';', FormatStr);
      if dPos <> 0 then
      begin
        if Boolean(Arg) then
          Result := Copy(FormatStr, 1, dPos-1)
        else
          Result := Copy(FormatStr, dPos+1, Length(FormatStr) - dPos);
      end
      else
        if Boolean(Arg) then
          Result := 'true'
        else
          Result := 'false';
    end;

    dtFloat:
    begin
      ExVal := Arg;
      if ExVal <> 0 then
      begin
        dPos := Pos('dd', FormatStr);
        hPos := Pos({$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, FormatStr);

        if (dPos <> 0) or (hPos <> 0) then
        begin
          days := Trunc(ExVal);
          DecodeTime(ExVal, hour, min, sec, msec);
          Result := FormatStr;

          if dPos <> 0 then
            Result := StringReplace(Result, 'dd', Format('%d', [days]), [rfReplaceAll])
          else
            hour := hour + days * 24;

          if hPos <> 0 then
          begin
            // Do we need to round minutes
            dPos := Pos('ss', FormatStr);
            if (dPos = 0) and (sec > 30) then
            begin
              Inc(min);
              if min = 60 then
              begin
                min := 0;
                inc(hour);
              end;
            end;
            dPos := Pos('mm', FormatStr);
            if (dPos = 0) and (min > 30) then
              Inc(hour);

            Result := StringReplace(Result, 'hh', Format('%.2d', [hour]), [rfReplaceAll]);
            Result := StringReplace(Result, 'mm', Format('%.2d', [min]), [rfReplaceAll]);
            Result := StringReplace(Result, 'ss', Format('%.2d', [sec]), [rfReplaceAll]);
          end;
        end
        else
          Result := FormatFloat(FormatStr, ExVal); // Format(FormatStr, [ExVal]);
      end;
    end;

    dtCurrency:
      Result := FormatCurr(FormatStr, Extended(Arg));
    dtDate:
      if TDateTime(Arg) <> 0 then
        Result := FormatDateTime(FormatStr, Trunc(TDateTime(Arg)));
    dtTime:
      if TDateTime(Arg) <> 0 then
        Result := FormatDateTime(FormatStr, Frac(TDateTime(Arg)));
    dtDateTime:
      if TDateTime(Arg) <> 0 then
        Result := FormatDateTime(FormatStr, TDateTime(Arg));
  end;
end;

//=---------------------------------------------------------------------------=
procedure KillMessage(Wnd: HWnd; Msg: Integer);
// Delete the requested message from the queue, but throw back
// any WM_QUIT msgs that PeekMessage may also return
var
  M: TMsg;
begin
  M.Message := 0;
  if PeekMessage(M, Wnd, Msg, Msg, pm_Remove) and (M.Message = WM_QUIT) then
    PostQuitMessage(M.wparam);
end;


procedure TPopupListBox.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or WS_BORDER;
    ExStyle := WS_EX_TOOLWINDOW or WS_EX_TOPMOST;
    AddBiDiModeExStyle(ExStyle);
    WindowClass.Style := CS_SAVEBITS;
  end;
end;

procedure TPopupListbox.CreateWnd;
begin
  inherited CreateWnd;
  Windows.SetParent(Handle, 0);
  CallWindowProc(DefWndProc, Handle, wm_SetFocus, 0, 0);
end;

procedure TPopupListbox.Keypress(var Key: Char);
var
  TickCount: Integer;
begin
  case Key of
    #8, #27: FSearchText := '';
    #32..#255:
      begin
        TickCount := GetTickCount;
        if TickCount - FSearchTickCount > 2000 then FSearchText := '';
        FSearchTickCount := TickCount;
        if Length(FSearchText) < 32 then FSearchText := FSearchText + Key;
        SendMessage(Handle, LB_SelectString, WORD(-1), Longint(PChar(FSearchText)));
        Key := #0;
      end;
  end;
  inherited Keypress(Key);
end;

procedure TPopupListbox.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  TPiInplaceEdit(Owner).CloseUp((X >= 0) and (Y >= 0) and
      (X < Width) and (Y < Height));
end;

/////////////////////////////////////////////////////////////////
// TPiInplaceComboBox
/////////////////////////////////////////////////////////////////
constructor TPiInplaceEdit.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  FButtonWidth := GetSystemMetrics(SM_CXVSCROLL);
end;

// BoundChanged is called whenever the editor is shown
procedure TPiInplaceEdit.BoundsChanged;
var
  R: TRect;
begin
//	inherited BoundsChanged();
  if EditStyle <> esSimple then
    R := Rect(2, 2, Width - FButtonWidth - 2, Height)
  else
    R := Rect(2, 2, Width - 2, Height);
  SendMessage(Handle, EM_SETRECTNP, 0, LongInt(@R));
  SendMessage(Handle, EM_SCROLLCARET, 0, 0);
end;

procedure TPiInplaceEdit.CMCancelMode(var Message: TCMCancelMode);
begin
  if (Message.Sender <> Self) and (Message.Sender <> FActiveList) then
    CloseUp(False);
end;

procedure TPiInplaceEdit.WMCancelMode(var Message: TMessage);
begin
  StopTracking;
  inherited;
end;

procedure TPiInplaceEdit.WMKillFocus(var Message: TMessage);
begin
  if not SysLocale.FarEast then inherited
  else
  begin
    ImeName := Screen.DefaultIme;
    ImeMode := imDontCare;
    inherited;
    if HWND(Message.WParam) <> TCustomEzColumnGrid(Grid).Handle then
      ActivateKeyboardLayout(Screen.DefaultKbLayout, KLF_ACTIVATE);
  end;
  CloseUp(False);
end;

function TPiInplaceEdit.ButtonRect: TRect;
begin
  if not TCustomEzColumnGrid(Owner).UseRightToLeftAlignment then
    Result := Rect(Width - FButtonWidth, 0, Width, Height)
  else
    Result := Rect(0, 0, FButtonWidth, Height);
end;

function TPiInplaceEdit.OverButton(const P: TPoint): Boolean;
begin
  Result := PtInRect(ButtonRect, P);
end;

procedure TPiInplaceEdit.WMLButtonDblClk(var Message: TWMLButtonDblClk);
begin
  with Message do
  if (FEditStyle <> esSimple) and OverButton(Point(XPos, YPos)) then
    Exit;
  inherited;
end;

procedure TPiInplaceEdit.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    wm_KeyDown, wm_SysKeyDown, wm_Char:
      if FEditStyle = esPickList then
      with TWMKey(Message) do
      begin
        DoDropDownKeys(CharCode, KeyDataToShiftState(KeyData));
        if (CharCode <> 0) and FListVisible then
        begin
          with TMessage(Message) do
            SendMessage(FActiveList.Handle, Msg, WParam, LParam);
          Exit;
        end;
      end
  end;
  inherited;
end;

procedure TPiInplaceEdit.WMPaint(var Message: TWMPaint);
begin
  PaintHandler(Message);
end;

procedure TPiInplaceEdit.WMSetCursor(var Message: TWMSetCursor);
var
  P: TPoint;
begin
  GetCursorPos(P);
  P := ScreenToClient(P);
  if (FEditStyle <> esSimple) and OverButton(P) then
    Windows.SetCursor(LoadCursor(0, idc_Arrow))
  else
    inherited;
end;

procedure TPiInplaceEdit.UpdateContents;
var
  col: TEzColumnGridColumn;
  NewStyle: TEditStyle;
begin
  NewStyle := esSimple;

  col := TCustomEzColumnGrid(Grid).Columns[TCustomEzColumnGrid(Grid).SelectedIndex];

  ReadOnly := col.ReadOnly;
  if not col.ReadOnly then
  begin
    if col.EditStyle = esAuto then
      case col.DataType of
        dtString:
          if col.HasPickList then
            NewStyle := esPickList;
        dtInteger:
          if col.HasPickList then
          begin
            NewStyle := esPickList;
            ReadOnly := True;
          end;
        dtDateTime:
          NewStyle := esEllipsis;
      end
    else
      NewStyle := col.EditStyle;
  end;

  EditStyle := NewStyle;
  inherited UpdateContents;
end;

procedure TPiInplaceEdit.CloseUp(Accept: Boolean);
var
  ListValue: Variant;
begin
  if FListVisible then
  begin
    if GetCapture <> 0 then SendMessage(GetCapture, WM_CANCELMODE, 0, 0);
    if FPickList.ItemIndex <> -1 then
      ListValue := FPickList.Items[FPicklist.ItemIndex];
    SetWindowPos(FActiveList.Handle, 0, 0, 0, 0, 0, SWP_NOZORDER or
      SWP_NOMOVE or SWP_NOSIZE or SWP_NOACTIVATE or SWP_HIDEWINDOW);
    FListVisible := False;
    Invalidate;
    if Accept then
      Text := ListValue;
  end;
end;

procedure TPiInplaceEdit.DoDropDownKeys(var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_UP, VK_DOWN:
      if ssAlt in Shift then
      begin
        if FListVisible then CloseUp(True) else DropDown;
        Key := 0;
      end;
    VK_RETURN, VK_ESCAPE:
      if FListVisible and not (ssAlt in Shift) then
      begin
        CloseUp(Key = VK_RETURN);
        Key := 0;
      end;
  end;
end;

procedure TPiInplaceEdit.DropDown;
var
  P: TPoint;
  I,J,Y: Integer;
  Column: TEzColumnGridColumn;
begin
  if not FListVisible and Assigned(FActiveList) then
  begin
    FActiveList.Width := Width;
    with TCustomEzColumnGrid(Grid) do
      Column := Columns[SelectedIndex];

    begin
      FPickList.Color := Color;
      FPickList.Font := Font;
      FPickList.Items := Column.Picklist;
      if FPickList.Items.Count >= Integer(Column.DropDownRows) then
        FPickList.Height := Integer(Column.DropDownRows) * FPickList.ItemHeight + 4
      else
        FPickList.Height := FPickList.Items.Count * FPickList.ItemHeight + 4;

      if Length(Text) = 0 then
        FPickList.ItemIndex := -1
      else
        FPickList.ItemIndex := FPickList.Items.IndexOf(Text);

      J := FPickList.ClientWidth;
      for I := 0 to FPickList.Items.Count - 1 do
      begin
        Y := FPickList.Canvas.TextWidth(FPickList.Items[I]);
        if Y > J then J := Y;
      end;
      FPickList.ClientWidth := J;
    end;
    P := Parent.ClientToScreen(Point(Left, Top));
    Y := P.Y + Height;
    if Y + FActiveList.Height > Screen.Height then Y := P.Y - FActiveList.Height;
    SetWindowPos(FActiveList.Handle, HWND_TOP, P.X, Y, 0, 0,
      SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
    FListVisible := True;
    Invalidate;
    Windows.SetFocus(Handle);
  end;
end;

procedure TPiInplaceEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (EditStyle = esEllipsis) and (Key = VK_RETURN) and (Shift = [ssCtrl]) then
  begin
    TCustomEzColumnGrid(Grid).EditButtonClick;
    KillMessage(Handle, WM_CHAR);
  end
  else
    inherited KeyDown(Key, Shift);
end;

procedure TPiInplaceEdit.ListMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
    CloseUp(PtInRect(FActiveList.ClientRect, Point(X, Y)));
end;

procedure TPiInplaceEdit.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if (Button = mbLeft) and (FEditStyle <> esSimple) and
    OverButton(Point(X,Y)) then
  begin
    if FListVisible then
      CloseUp(False)
    else
    begin
      MouseCapture := True;
      FTracking := True;
      TrackButton(X, Y);
      if Assigned(FActiveList) then
        DropDown;
    end;
  end;
  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TPiInplaceEdit.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  ListPos: TPoint;
  MousePos: TSmallPoint;
begin
  if FTracking then
  begin
    TrackButton(X, Y);
    if FListVisible then
    begin
      ListPos := FActiveList.ScreenToClient(ClientToScreen(Point(X, Y)));
      if PtInRect(FActiveList.ClientRect, ListPos) then
      begin
        StopTracking;
        MousePos := PointToSmallPoint(ListPos);
        SendMessage(FActiveList.Handle, WM_LBUTTONDOWN, 0, Integer(MousePos));
        Exit;
      end;
    end;
  end;
  inherited MouseMove(Shift, X, Y);
end;

procedure TPiInplaceEdit.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
var
  WasPressed: Boolean;
begin
  WasPressed := FPressed;
  StopTracking;
  if (Button = mbLeft) and (FEditStyle = esEllipsis) and WasPressed then
  begin
    TCustomEzColumnGrid(Grid).EditButtonClick;
  end;
  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TPiInplaceEdit.PaintWindow(DC: HDC);
var
  R: TRect;
  Flags: Integer;
  W, X, Y: Integer;
begin
  if FEditStyle <> esSimple then
  begin
    R := ButtonRect;
    Flags := 0;
    if FEditStyle = esPickList then
    begin
      if FActiveList = nil then
        Flags := DFCS_INACTIVE
      else if FPressed then
        Flags := DFCS_FLAT or DFCS_PUSHED;
      DrawFrameControl(DC, R, DFC_SCROLL, Flags or DFCS_SCROLLCOMBOBOX);
    end
    else   { etEllipsis }
    begin
      if FPressed then Flags := BF_FLAT;
      DrawEdge(DC, R, EDGE_RAISED, BF_RECT or BF_MIDDLE or Flags);
      X := R.Left + ((R.Right - R.Left) shr 1) - 1 + Ord(FPressed);
      Y := R.Top + ((R.Bottom - R.Top) shr 1) - 1 + Ord(FPressed);
      W := FButtonWidth shr 3;
      if W = 0 then W := 1;
      PatBlt(DC, X, Y, W, W, BLACKNESS);
      PatBlt(DC, X - (W * 2), Y, W, W, BLACKNESS);
      PatBlt(DC, X + (W * 2), Y, W, W, BLACKNESS);
    end;
    ExcludeClipRect(DC, R.Left, R.Top, R.Right, R.Bottom);
  end;
  inherited PaintWindow(DC);
end;

procedure TPiInplaceEdit.StopTracking;
begin
  if FTracking then
  begin
    TrackButton(-1, -1);
    FTracking := False;
    MouseCapture := False;
  end;
end;

procedure TPiInplaceEdit.SetEditStyle(Value: TEditStyle);
begin
  if Value = FEditStyle then Exit;
  FEditStyle := Value;
  case Value of
    esPickList:
      begin
        if FPickList = nil then
        begin
          FPickList := TPopupListbox.Create(Self);
          FPickList.Visible := False;
          FPickList.Parent := Self;
          FPickList.OnMouseUp := ListMouseUp;
          FPickList.IntegralHeight := True;
          FPickList.ItemHeight := 11;
        end;
        FActiveList := FPickList;
      end;
  else  { cbsNone, cbsEllipsis, or read only field }
    FActiveList := nil;
  end;
  with TCustomEzColumnGrid(Grid) do
    Self.ReadOnly := Columns[SelectedIndex].ReadOnly;
  Repaint;
end;

procedure TPiInplaceEdit.TrackButton(X,Y: Integer);
var
  NewState: Boolean;
  R: TRect;
begin
  R := ButtonRect;
  NewState := PtInRect(R, Point(X, Y));
  if FPressed <> NewState then
  begin
    FPressed := NewState;
    InvalidateRect(Handle, @R, False);
  end;
end;


//=---------------------------------------------------------------------------=
constructor TEzColumnGridColumn.Create(Collection: TCollection);
var
  Grid: TCustomEzColumnGrid;
begin
  Grid := nil;
  if Assigned(Collection) and (Collection is TEzColumnGridColumns) then
    Grid := TEzColumnGridColumns(Collection).Grid;
  if Assigned(Grid) then Grid.BeginLayout;
  try
    inherited Create(Collection);
    RestoreDefaults;
  finally
    if Assigned(Grid) then Grid.EndLayout;
  end;
end;

destructor TEzColumnGridColumn.Destroy;
begin
  FPickList.Free;
  inherited Destroy;
end;

procedure TEzColumnGridColumn.Assign(Source: TPersistent);
begin
  if Source is TEzColumnGridColumn then
  begin
    if Assigned(Collection) then Collection.BeginUpdate;
    try
      RestoreDefaults;
      FTitle:= TEzColumnGridColumn(Source).Title;
      FDropDownRows := TEzColumnGridColumn(Source).DropDownRows;
      FWidth := TEzColumnGridColumn(Source).Width;
      FPickList.Assign(TEzColumnGridColumn(Source).PickList);
      FAlignment := TEzColumnGridColumn(Source).Alignment;
      FReadonly := TEzColumnGridColumn(Source).ReadOnly;
      FEditMask := TEzColumnGridColumn(Source).EditMask;
      FDisplayMask := TEzColumnGridColumn(Source).DisplayMask;
      FDataType := TEzColumnGridColumn(Source).DataType;
      FDefault := TEzColumnGridColumn(Source).Default;
      FEditStyle := TEzColumnGridColumn(Source).EditStyle;
    finally
      if Assigned(Collection) then Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TEzColumnGridColumn.GetHasPicklist: Boolean;
begin
  Result := (FPickList <> nil) and (FPickList.Count > 0);
end;

function TEzColumnGridColumn.GetDisplayName: string;
begin
  if FTitle = '' then
    Result := inherited GetDisplayName
  else
    Result := FTitle;
end;

function TEzColumnGridColumn.GetPickList: TStrings;
begin
  if FPickList = nil then
    FPickList := TStringList.Create;
  Result := FPickList;
end;

procedure TEzColumnGridColumn.SetPickList(Value: TStrings);
begin
  if Value = nil then
  begin
    FPickList.Free;
    FPickList := nil;
    Exit;
  end;
  PickList.Assign(Value);
end;

procedure TEzColumnGridColumn.SetDefault(const Value: Variant);
var
  NewValue: OleVariant;

begin
  if (VarIsEmpty(Value) or VarIsNull(Value) or (FDataType = dtRecords)) or
     ((DataType = dtString) and (Length(Value) = 0))
  then
    NewValue := Null
  else
    case FDataType of
      dtString, dtMemo:
        NewValue := VarAsType(Value, varOleStr);
      dtInteger:
        NewValue := VarAsType(Value, varInteger);
      dtBoolean:
        NewValue := VarAsType(Value, varBoolean);
      dtFloat:
        NewValue := VarAsType(Value, varDouble);
      dtCurrency:
        NewValue := VarAsType(Value, varCurrency);
      dtDate, dtTime, dtDateTime:
        NewValue := VarAsType(Value, varDate);
    end;
  FDefault := NewValue;
end;

procedure TEzColumnGridColumn.SetWidth(Value: Integer);
begin
  if Value <> FWidth then
  begin
    FWidth := Value;
    Changed(False);
  end;
end;

procedure TEzColumnGridColumn.SetEditMask(Mask: string);
begin
  FEditMask := TranslateMask(Mask, FDataType);
end;

procedure TEzColumnGridColumn.SetDisplayMask(Mask: string);
begin
  FDisplayMask := TranslateMask(Mask, FDataType);
end;

procedure TEzColumnGridColumn.RestoreDefaults;
begin
  FTitle:= '';
  FDropDownRows := 7;
  FWidth := 64;
  FPickList.Free;
  FPickList := nil;
  FAlignment := taLeftJustify;
  FReadonly := False;
  FDataType := dtString;
  FEditMask := '';
  FDisplayMask := '';
  FDefault := NULL;
  FEditStyle := esAuto;
end;

//=---------------------------------------------------------------------------=
constructor TEzColumnGridColumns.Create(Grid: TCustomEzColumnGrid; ColumnClass: TCollectionItemClass);
begin
  inherited Create(ColumnClass);
  FGrid := Grid;
end;

function TEzColumnGridColumns.Add: TEzColumnGridColumn;
begin
  Result := TEzColumnGridColumn(inherited Add);
end;

function TEzColumnGridColumns.GetOwner: TPersistent;
begin
  Result := FGrid;
end;

function TEzColumnGridColumns.GetEzColumnGridColumn(Index: Integer): TEzColumnGridColumn;
begin
  Result := TEzColumnGridColumn(inherited Items[Index]);
end;

procedure TEzColumnGridColumns.SetEzColumnGridColumn(Index: Integer; Value: TEzColumnGridColumn);
begin
  Items[Index].Assign(Value);
end;

procedure TEzColumnGridColumns.Update(Item: TCollectionItem);
begin
  if (FGrid = nil) or (csLoading in FGrid.ComponentState) then Exit;
  if Item = nil then
  begin
    FGrid.LayoutChanged;
  end
  else
  begin
    FGrid.ColWidths[TEzColumnGridColumn(Item).Index+1] :=
      TEzColumnGridColumn(Item).Width;
  end;
end;

//=---------------------------------------------------------------------------=
constructor TCustomEzColumnGrid.Create(AOwner: TComponent);
begin
  inherited;
  FColumns := TEzColumnGridColumns.Create(Self, TEzColumnGridColumn);
  Options := [goEditing, goColSizing, goVertLine, goHorzLine, goFixedVertLine, goFixedHorzLine];
  FFlags := [igfCanInsert, igfShowWarnings, igfUseInsertCache];
  FState := [igsEmpty];
  inherited RowCount := 2;
  inherited ColCount := 2;
  inherited DefaultDrawing := false;
  DefaultDrawing := true;
  FixedRows := 1;
  FixedCols := 0;
  FInsertData := NULL;
  FUpdateCount := 0;
  FLayoutCount := 0;
  FDrawBitmap := TBitmap.Create;
end;

function TCustomEzColumnGrid.AcquireLayoutLock: Boolean;
begin
  Result := (FUpdateCount = 0) and (FLayoutCount = 0);
  if Result then BeginLayout;
end;

function TCustomEzColumnGrid.CanEditShow: Boolean;
begin
  if (igsEmpty in FState) or (Columns.Count = 0) or (Columns[Col-1].DataType = dtRecords) then
    Result := False
  else
    Result := inherited CanEditShow;

  if Result and Assigned(FOnCanEditShow) then FOnCanEditShow(Self, Col, Row, Result);
end;

procedure TCustomEzColumnGrid.CMExit(var Message: TCMExit);
begin
  if (igsInserting in FState) and
      not (csDesigning in ComponentState) and
      not CancelInsert then
    SetFocus
  else
    inherited;
end;

procedure TCustomEzColumnGrid.ColumnMoved(FromIndex, ToIndex: Longint);
begin
  if Assigned(FOnColumnMoved) then FOnColumnMoved(Self, FromIndex, ToIndex);
end;

procedure TCustomEzColumnGrid.ColWidthsChanged;
var
  I: Integer;
begin
  inherited ColWidthsChanged;
  if AcquireLayoutLock then
  try
    for I := 1 to ColCount-1 do
      FColumns[I-1].Width := ColWidths[I];
  finally
    EndLayout;
  end;
end;

destructor TCustomEzColumnGrid.Destroy;
begin
  inherited;
  FColumns.Destroy;
  FDrawBitmap.Destroy;
end;

procedure TCustomEzColumnGrid.StartInsert(ARow: Integer);
var
  I: Integer;
  Action: TInsertAction;
begin
  if not (igfCanInsert in FFlags) then Exit;

  if (igsInserting in FState) then
  begin
    EndInsert;
    Exit;
  end;

  BeginUpdate;
  try
    // Set flag that we are inserting ASAP
    Include(FState, igsInserting);

    FInsertData := VarArrayCreate([0, Columns.Count-1], varVariant);
    for I:=0 to Columns.Count-1 do
        FInsertData[I] := Columns[I].Default;

    Action := iaContinue;
    DoBeginInsert(Self, ARow, FInsertData, Action);
    if Action = iaAbort then
        Exclude(FState, igsInserting)
    else
    begin
      // Add extra row when the grid is not empty
      if not (igsEmpty in FState) then
        RowCount := RowCount + 1;
      Exclude(FState, igsEmpty);
      Row := ARow;
      Col := FixedCols;
      EditorMode := True;
      Invalidate;
    end;
  finally
    EndUpdate;
  end;
end;

function TCustomEzColumnGrid.CreateEditor: TInplaceEdit;
begin
  Result := TPiInplaceEdit.Create(Self);
end;

procedure TCustomEzColumnGrid.ReadColumns(Reader: TReader);
begin
  Columns.Clear;
  Reader.ReadValue;
  Reader.ReadCollection(Columns);
end;

procedure TCustomEzColumnGrid.WndProc(var Message: TMessage);
begin
  if Message.Msg in [WM_KILLFOCUS, WM_SETFOCUS] then
    InvalidateCell(0, Row);
  inherited;
end;

procedure TCustomEzColumnGrid.WriteColumns(Writer: TWriter);
begin
  Writer.WriteCollection(Columns)
end;

procedure TCustomEzColumnGrid.DefineProperties(Filer: TFiler);
begin
  Filer.DefineProperty('Columns', ReadColumns, WriteColumns, True);
end;

procedure TCustomEzColumnGrid.DoBeginInsert(Sender: TObject; ARow: Longint; Data: Variant; var Action: TInsertAction);
begin
  if Assigned(FBeginInsert) then FBeginInsert(Sender, ARow, Data, Action);
end;

procedure TCustomEzColumnGrid.DoOnInsert(Sender: TObject; ARow: Longint; Data: Variant; var Action: TInsertAction);
begin
  if Assigned(FOnInsert) then FOnInsert(Sender, ARow, Data, Action);
end;

procedure TCustomEzColumnGrid.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
begin
  DrawCellTo(Canvas, ACol, ARow, ARect, AState);
end;

procedure TCustomEzColumnGrid.DrawCellTo(ACanvas: TCanvas; ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
const
  TextFlags : array[TAlignment] of Cardinal = (DT_LEFT, DT_RIGHT, DT_CENTER);
  DEFAULT_FLAGS = DT_SINGLELINE or DT_WORD_ELLIPSIS;

var
  Text: string;
  RCopy: TRect;

begin
  RCopy := ARect;
  Canvas.Brush.Color := Color;
  Canvas.FillRect(RCopy);

  if DefaultDrawing and (FUpdateCount = 0) then
  begin
    Text := '';

    // Draw row indicator ?
    if ACol = 0 then
    begin
      Canvas.Brush.Color := clBtnFace;
      Canvas.Font := Font;

      Canvas.Font.Style := [fsBold];
      if Focused or EditorMode then
        Canvas.Font.Color := clBlack else
        Canvas.Font.Color := clGray;

      if ARow = Row then
        if (igsInserting in FState) then
          Text := '*'
        else if EditorMode then
          Text := 'I'
        else
          Text := '>';

      EzRenderTextRect(Canvas, RCopy, Text, btsFlatButton, bstNormal, 0, DT_VCENTER or DT_CENTER or DT_SINGLELINE);

      Exit;
    end
    else
    // Draw title
    if (FixedRows > 0) and (Columns.Count <> 0) and (ARow = 0) then
    begin
      Canvas.Brush.Color := clBtnFace;
      Canvas.Font := Font;
      EzRenderTextButton(Canvas, RCopy, Columns[ACol-1].Title, btsFlatButton, bstNormal);
    end
    else if not Empty then
    begin
      InflateRect(RCopy, -2, -2);
      Text := GetDisplayText(ACol, ARow);
      Canvas.Font := Font;
      if Length(Text) > 0 then
        EzRenderText(Canvas, RCopy, Text, tsNormal, TextFlags[Columns[ACol-1].Alignment] or DEFAULT_FLAGS or DT_VCENTER);

      if gdSelected in AState then
        Canvas.DrawFocusRect(ARect);
    end;
  end;
  if Assigned(FOnDrawCell) then FOnDrawCell(Self, ACol, ARow, ARect, AState);
end;

procedure TCustomEzColumnGrid.EditButtonClick;
begin
  if Assigned(FOnEditButtonClick) then
    FOnEditButtonClick(Self);
end;

function TCustomEzColumnGrid.GetDisplayText(ACol, ARow: Longint): string;
var
  vData: Variant;
begin
  vData := GetPlainValue(ACol, ARow);
  if VarIsNull(vData) then
    Result := ''
  else
  if Length(Columns[ACol-1].DisplayMask) > 0 then
    Result := FormatValue(Columns[ACol-1].DisplayMask,
                          vData,
                          Columns[ACol-1].DataType)
  else
    Result := vData;
end;

function TCustomEzColumnGrid.GetEditMask(ACol, ARow: Longint): string;
begin
  Result := Columns[ACol-1].EditMask;
end;

function TCustomEzColumnGrid.GetEditText(ACol, ARow: Longint): string;
var
  vData: Variant;
begin
  vData := GetPlainValue(ACol, ARow);
  if VarIsNull(vData) then
    Result := ''
  else
  if (Length(Columns[ACol-1].DisplayMask) > 0) then
    Result := FormatValue(Columns[ACol-1].DisplayMask,
                          vData,
                          Columns[ACol-1].DataType)
  else
    Result := vData;
end;

function TCustomEzColumnGrid.GetPlainValue(ACol, ARow: Longint): Variant;
begin
  if Columns[ACol-1].DataType = dtRecords then
    Result := NULL
  else
  if (igsInserting in FState) and (ARow = Row) then
    Result := FInsertData[ACol-1]
  else
  if (igsInserting in FState) and (ARow > Row) then
    Result := GetValue(ACol, ARow-1)
  else
    Result := GetValue(ACol, ARow);

  if not VarIsNull(Result) and
     Columns[ACol-1].HasPicklist and
     (Columns[ACol-1].DataType = dtInteger)
  then
  begin
    try
      Result := Columns[ACol-1].PickList[Result];
    except
      Result := NULL;
    end;
  end;
end;

function TCustomEzColumnGrid.GetEmpty: Boolean;
begin
  Result := (igsEmpty in FState) or (FColumns.Count = 0);
end;

function TCustomEzColumnGrid.GetValue(ACol, ARow: Longint) : Variant;
begin
  if Assigned(FOnGetValue) then FOnGetValue(Self, ACol, ARow, Result);
end;

function TCustomEzColumnGrid.GetFixedCols: Integer;
begin
  Result := inherited FixedCols;
end;

function TCustomEzColumnGrid.GetSelectedIndex: Integer;
begin
  Result := Col-1;
end;

function TCustomEzColumnGrid.CancelInsert : Boolean;
var
  I: Integer;
  HasData: Boolean;
begin
  HasData := False;
  for I:=0 to Columns.Count-1 do
    if FInsertData[I] <> Columns[I].Default then
    begin
      HasData := True;
      break;
    end;

  if HasData and (igfShowWarnings in FFlags) and
    (
      MessageDlg('Are you sure you want to cancel the insertion of this record.',
                 mtConfirmation, [mbYes, mbNo], 0) = idYes
    ) then
      HasData := False; // abort

  if not HasData then
  begin
    FInsertData := NULL;
    EndInsert; // This will abort the insertsertion
    Result := True;
  end
  else
    Result := False;
end;

procedure TCustomEzColumnGrid.BeginUpdate;
begin
  Inc(FUpdateCount);
end;

procedure TCustomEzColumnGrid.EndUpdate;
begin
  if FUpdateCount > 0 then
    Dec(FUpdateCount);
end;

function TCustomEzColumnGrid.GetCells(ACol, ARow: Integer) : string;
begin
  if EditorMode then
    Result := GetEditText(ACol, ARow)
  else
    Result := GetDisplayText(ACol, ARow)
end;

procedure TCustomEzColumnGrid.BeginLayout;
begin
  BeginUpdate;
  if FLayoutCount = 0 then Columns.BeginUpdate;
  Inc(FLayoutCount);
end;

procedure TCustomEzColumnGrid.EndLayout;
begin
  if FLayoutCount > 0 then
  begin
    try
      try
        if FLayoutCount = 1 then
          InternalLayout;
      finally
        if FLayoutCount = 1 then
          FColumns.EndUpdate;
      end;
    finally
      Dec(FLayoutCount);
      EndUpdate;
    end;
  end;
end;

procedure TCustomEzColumnGrid.InternalLayout;
begin
  if (csLoading in ComponentState) then Exit;
  inherited ColCount := max(2, FColumns.Count + 1);
  SetColumnAttributes;
end;

procedure TCustomEzColumnGrid.LayoutChanged;
begin
  if AcquireLayoutLock then
    EndLayout;
end;

procedure TCustomEzColumnGrid.EndInsert;
var
  I, NewCol, NewRow: Integer;
  HasData, CellFound: Boolean;
  Action: TInsertAction;

begin
  HasData := False;
  if (igfUseInsertCache in FFlags) and VarIsArray(FInsertData)then
  begin
    for I:=0 to Columns.Count-1 do
      if FInsertData[I] <> Columns[I].Default then
      begin
        HasData := True;
        break;
      end;
  end;

  if HasData then
  begin
    Action := iaApply;
    DoOnInsert(Self, Row, FInsertData, Action);
  end
  else
  begin
    // When no data has been entered, abort the insertion
    Action := iaAbort;
    if RowCount = 2 then
      Include(FState, igsEmpty);
  end;

  if Action <> iaContinue then
  begin
    // Save value for NewRow
    NewRow := Row;
    NewCol := 0;

    BeginUpdate;

    EditorMode := False;
    Exclude(FState, igsInserting);
    if Action = iaAbort then
    begin
      if (RowCount > 2) then
      begin
        if Row = RowCount - 1 then
          Row := Row - 1;
        RowCount := RowCount - 1;
        NewRow := Max(2, Min(RowCount - 1, NewRow));
      end;
    end
    else
      Exclude(FState, igsEmpty);

    FInsertData := NULL;

    EndUpdate;

    // Determine what cell can be selected
    CellFound := False;
    while not CellFound and (NewRow >= FixedRows) do
    begin
      NewCol := FixedCols;
      NewRow := max(NewRow-1, FixedRows);
      while not CellFound and (NewCol < ColCount) do
        if SelectCell(NewCol, NewRow) then
          CellFound := True else
          Inc(NewCol);
    end;
    // Assign column first
    Col := NewCol;
    Row := NewRow;
    Invalidate;
  end;
end;

procedure TCustomEzColumnGrid.KeyDown(var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_INSERT:
      if not (igsInserting in FState) then
      begin
        StartInsert(Row);
        Key := 0;
      end;
    VK_DOWN:
      // Start insert at last row ?
      if not (igsInserting in FState) and (Row = RowCount - 1) then
      begin
        StartInsert(Row + 1);
        Key := 0;
      end
      else
      // Stop inserting when I am already inserting
      if (igsInserting in FState) then
      begin
        EndInsert;
        Key := 0;
      end;
    VK_UP:
      if (igsInserting in FState) then
      begin
        EndInsert;
        Key := 0;
      end;
    VK_ESCAPE:
      // Escape first cancels the editor and then insert mode
      if (igsInserting in FState) then
      begin
        if EditorMode then
          EditorMode := False
        else
          CancelInsert;
        Key := 0;
      end;
    else
      if (igsEmpty in FState) then
        StartInsert(Row);
    end;

  if Key <> 0 then
    inherited KeyDown(Key, Shift);
end;

procedure TCustomEzColumnGrid.Loaded;
begin
  inherited Loaded;
  if FColumns.Count > 0 then
    ColCount := FColumns.Count+1;
  LayoutChanged;
end;

procedure TCustomEzColumnGrid.MouseDown
  (Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  EditorMode := False;
  inherited;
end;

procedure TCustomEzColumnGrid.RowMoved(FromIndex, ToIndex: Longint);
begin
  if Assigned(FOnRowMoved) then FOnRowMoved(Self, FromIndex, ToIndex);
end;

function TCustomEzColumnGrid.SelectCell(ACol, ARow: Longint): Boolean;
begin
  if FUpdateCount > 0 then Exit;

  // If we are inserting and we leave the row, insert data entered
  if (igsInserting in FState) and (ARow <> Row) then
  begin
    EndInsert;
    Result := False; // Don't leave the row
  end
  else
    Result := True;
  // Let client determine if cell can be selected
  if Result and not (igsEmpty in FState) and Assigned(FOnSelectCell) then
    FOnSelectCell(Self, ACol, ARow, Result);

  if Result and (ARow <> Row) then
  begin
    InvalidateCell(0, ARow);
    InvalidateCell(0, Row);
  end;
end;

procedure TCustomEzColumnGrid.SetCells(ACol, ARow: Integer; aValue: string);
begin
  SetEditText(ACol, ARow, aValue);
  if EditorMode then
    InplaceEditor.Text := aValue;
  InvalidateCell(ACol, ARow);
end;

procedure TCustomEzColumnGrid.SetColumns(Value: TEzColumnGridColumns);
begin
  FColumns.Assign(Value);
end;

procedure TCustomEzColumnGrid.SetColumnAttributes;
var
  I: Integer;
begin
  for I := 0 to FColumns.Count-1 do
    with FColumns[I] do
      ColWidths[I+1] := Width;
  ColWidths[0] := 10;
end;

procedure TCustomEzColumnGrid.SetEditText(ACol, ARow: integer; const Value: string);
var
  vData: Variant;

begin
  try
  with Columns[ACol-1] do
  begin
    if IsEmptyValue(EditMask, Value) then
      vData := NULL
    else
    if HasPicklist then
    begin
      if DataType = dtString then
        vData := Value
      else
        vData := PickList.IndexOf(Value);
    end
    else
    if DataType = dtBoolean then
      vData := TranslateTo(DisplayMask,Value,DataType)
    else
    if not EditorMode then
      vData := TranslateTo(EditMask, Value, DataType)
    else
      vData := Value;
  end;
  except
    on O:TObject do
    begin
      vData := NULL;
      Application.HandleException(O);
    end;
  end;

  if (FUpdateCount = 0) then
    if (igsInserting in FState) then
      FInsertData[ACol-1] := vData
    else
      SetValue(ACol, ARow, vData);
end;

procedure TCustomEzColumnGrid.SetEmpty(Value: Boolean);
begin
  if Value then
    Include(FState, igsEmpty)
  else
    Exclude(FState, igsEmpty);
end;

procedure TCustomEzColumnGrid.SetFixedCols(Value: Integer);
begin
  if (Value > Columns.Count) then //and not (csLoading in ComponentState) then
    inherited FixedCols := Columns.Count+1
  else
    inherited FixedCols := max(1, Value);
end;

procedure TCustomEzColumnGrid.SetSelectedIndex(Value: Integer);
begin
  Col := Value + 1;
end;

procedure TCustomEzColumnGrid.SetState(Value: TEzColumnGridStates);
begin
  if (Columns.Count = 0) then
    FState := [igsEmpty]
  else
    FState := Value;
end;

procedure TCustomEzColumnGrid.SetValue(ACol, ARow: Longint; const Value: Variant);
begin
  if Assigned(FOnSetValue) then FOnSetValue(Self, ACol, ARow, Value);
end;

procedure TCustomEzColumnGrid.TopLeftChanged;
begin
  inherited TopLeftChanged;
  if Assigned(FOnTopLeftChanged) then FOnTopLeftChanged(Self);
end;

end.
