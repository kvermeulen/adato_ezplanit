unit CharInSet;

interface

uses EzMasks;

type
  TSysCharSet = set of SystemChar;

  function CharInSet(C: AnsiChar; const CharSet: TSysCharSet): Boolean;

implementation

function CharInSet(C: AnsiChar; const CharSet: TSysCharSet): Boolean;
begin
  Result := C in CharSet;
end;


end.
