object EzTimeBarDlg: TEzTimeBarDlg
  Left = 200
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Timebar setup'
  ClientHeight = 526
  ClientWidth = 434
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 269
    Top = 493
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 349
    Top = 493
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 61
    Width = 417
    Height = 49
    Caption = ' General settings '
    TabOrder = 2
    object Label4: TLabel
      Left = 8
      Top = 18
      Width = 74
      Height = 13
      Caption = 'Major cell width'
      FocusControl = EdWidth
    end
    object EdWidth: TEdit
      Left = 96
      Top = 14
      Width = 49
      Height = 21
      TabOrder = 0
      Text = '100'
      OnChange = EdWidthChange
    end
    object UdWidth: TUpDown
      Left = 145
      Top = 14
      Width = 15
      Height = 21
      Associate = EdWidth
      Min = 10
      Max = 500
      Position = 100
      TabOrder = 1
    end
  end
  object gbBands: TGroupBox
    Left = 8
    Top = 114
    Width = 417
    Height = 239
    Caption = ' Band settings '
    TabOrder = 3
    object gbProperties: TGroupBox
      Left = 136
      Top = 16
      Width = 273
      Height = 217
      Caption = ' Properties '
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 11
        Top = 20
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Units of measure'
      end
      object Label3: TLabel
        Left = 45
        Top = 75
        Width = 47
        Height = 13
        Alignment = taRightJustify
        Caption = 'Alignment'
        FocusControl = cbAlignment
      end
      object Label10: TLabel
        Left = 63
        Top = 102
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Count'
      end
      object Label5: TLabel
        Left = 6
        Top = 40
        Width = 108
        Height = 26
        Alignment = taRightJustify
        Caption = 'select predefined label'#13#10'or enter one yourself'
      end
      object lbColor: TLabel
        Left = 68
        Top = 130
        Width = 25
        Height = 13
        Caption = 'Color'
      end
      object cbUnits: TComboBox
        Left = 96
        Top = 16
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = CbUnitsChange
        Items.Strings = (
          'Years'
          'Quarters'
          'Months'
          'Weeks'
          'Days'
          'Hours'
          'Minutes')
      end
      object cbLabel: TComboBox
        Left = 120
        Top = 43
        Width = 145
        Height = 21
        ItemHeight = 0
        TabOrder = 1
        OnChange = cbLabelChange
      end
      object cbAlignment: TComboBox
        Left = 96
        Top = 71
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        OnChange = cbAlignmentChange
        Items.Strings = (
          'Left'
          'Right'
          'Centered')
      end
      object edCount: TEdit
        Left = 96
        Top = 98
        Width = 65
        Height = 21
        TabOrder = 3
        Text = '1'
        OnChange = edCountChange
      end
      object udCount: TUpDown
        Left = 161
        Top = 98
        Width = 16
        Height = 21
        Associate = edCount
        Min = 1
        Position = 1
        TabOrder = 4
      end
      object chkGanttDividers: TCheckBox
        Left = 8
        Top = 176
        Width = 257
        Height = 17
        Caption = 'Ganttchart displays dividers at this scale'#39's interval.'
        TabOrder = 5
        OnClick = chkGanttDividersClick
      end
      object chkGraphDividers: TCheckBox
        Left = 8
        Top = 192
        Width = 233
        Height = 17
        Caption = 'Graph displays dividers at this scale'#39's interval.'
        TabOrder = 6
        OnClick = chkGraphDividersClick
      end
      object pnlColorCombobox: TPanel
        Left = 96
        Top = 126
        Width = 137
        Height = 21
        BevelOuter = bvNone
        Caption = 'pnlColorCombobox'
        TabOrder = 7
      end
      object Button3: TButton
        Left = 96
        Top = 154
        Width = 81
        Height = 21
        Caption = 'Font ...'
        TabOrder = 8
        OnClick = BtnFontClick
      end
    end
    object Panel1: TPanel
      Left = 10
      Top = 20
      Width = 113
      Height = 145
      BevelOuter = bvNone
      TabOrder = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 113
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btnAdd: TSpeedButton
          Left = 5
          Top = 6
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
            FFFFFF77777777777777F000000000000007F0FBFBFBFB00FB07F0BFBFBFBF08
            0F07F0FBFBFBFB0B8007F0BFBFBFBF000007F0F7BFBFBBFBFB077BB7FB7BFFBF
            BF07F7F7B7BFBBFBFB07777F7FBFBFBFBF07FB7BF7777000000FF7B7B7BFFFFF
            FFFF7BF7FF7BFFFFFFFFBFF7BFF7FFFFFFFFFFF7FFFFFFFFFFFF}
          OnClick = btnAddClick
        end
        object btnDelete: TSpeedButton
          Left = 29
          Top = 6
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
            FFFFFF77777777777777F000000000000007F0FBFBFBFB00FB0770BFBFBFBF08
            0F0710FBFBFBFB0B800711BFBF71BF000007717BF717FBFBFB07F117B11FBFBF
            BF07F71111FBFBFBFB07F7111FBFBFBFBF07711117000000000F117F117FFFFF
            FFFFFFFFF117FFFFFFFFFFFFFF117FFFFFFFFFFFFFFFFFFFFFFF}
          OnClick = btnDeleteClick
        end
        object btnMoveUp: TSpeedButton
          Left = 61
          Top = 6
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333777F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
            3333333777737777F333333099999990333333373F3333373333333309999903
            333333337F33337F33333333099999033333333373F333733333333330999033
            3333333337F337F3333333333099903333333333373F37333333333333090333
            33333333337F7F33333333333309033333333333337373333333333333303333
            333333333337F333333333333330333333333333333733333333}
          NumGlyphs = 2
          OnClick = btnMoveUpClick
        end
        object btnMoveDown: TSpeedButton
          Left = 85
          Top = 6
          Width = 23
          Height = 22
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            333333333337F33333333333333033333333333333373F333333333333090333
            33333333337F7F33333333333309033333333333337373F33333333330999033
            3333333337F337F33333333330999033333333333733373F3333333309999903
            333333337F33337F33333333099999033333333373333373F333333099999990
            33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333300033333333333337773333333}
          NumGlyphs = 2
          OnClick = btnMoveDownClick
        end
      end
      object lbBands: TListBox
        Left = 0
        Top = 33
        Width = 113
        Height = 112
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbBandsClick
      end
    end
  end
  object gbPredefinedScales: TGroupBox
    Left = 8
    Top = 8
    Width = 417
    Height = 49
    Caption = ' Predefined scales '
    TabOrder = 4
    object Label2: TLabel
      Left = 16
      Top = 22
      Width = 56
      Height = 13
      Caption = 'Select scale'
    end
    object cbScales: TComboBox
      Left = 80
      Top = 18
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 0
      TabOrder = 0
      OnChange = cbScalesChange
    end
    object Button1: TButton
      Left = 232
      Top = 18
      Width = 49
      Height = 21
      Caption = 'New...'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 288
      Top = 18
      Width = 49
      Height = 21
      Caption = 'Delete'
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 360
    Width = 417
    Height = 105
    Caption = ' Example '
    TabOrder = 5
    object pnlExample: TPanel
      Left = 8
      Top = 15
      Width = 401
      Height = 82
      Anchors = [akLeft, akRight, akBottom]
      BevelOuter = bvLowered
      Color = clWhite
      TabOrder = 0
    end
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 392
    Top = 48
  end
  object EzDelayedIdle1: TEzDelayedIdle
    Active = False
    Delay = 20
    FireSynchronized = False
    OnDelayedIdle = EzDelayedIdle1DelayedIdle
    Left = 360
    Top = 48
  end
end
