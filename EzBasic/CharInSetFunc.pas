unit CharInSetFunc;

interface

uses EzMasks;

type
  TSysCharSet = set of Char;

  function CharInSet(C: WideChar; const CharSet: TSysCharSet): Boolean; overload;
  function CharInSet(C: Char; const CharSet: TSysCharSet): Boolean; overload;

implementation

function CharInSet(C: WideChar; const CharSet: TSysCharSet): Boolean;
begin
  Result := Char(C) in CharSet;
end;

function CharInSet(C: Char; const CharSet: TSysCharSet): Boolean;
begin
  Result := C in CharSet;
end;

end.
