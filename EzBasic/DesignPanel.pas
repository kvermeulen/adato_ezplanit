unit DesignPanel;

interface

uses
  Windows, Forms, SysUtils, Classes, Graphics, Controls, Dialogs,
  StdCtrls, extctrls;

type
  TDesignPanel = class(TScrollingWinControl)
  protected
    procedure DefineProperties(Filer: TFiler); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure SetParent(AParent: TWinControl); override;
    procedure ReadDummyInt(Reader: TReader);
    procedure ReadDummyBoolean(Reader: TReader);

  public
    constructor Create(AOwner: TComponent); override;

    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
  end;

implementation

constructor TDesignPanel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ControlStyle := [csCaptureMouse, csClickEvents,
    csSetCaption, csDoubleClicks];
  if (ClassType <> TDesignPanel) then
  begin
    if not InitInheritedComponent(Self, TDesignPanel) then
      raise EResNotFound.CreateFmt('Resource %s not found', [ClassName]);
  end
end;

procedure TDesignPanel.CreateParams(var Params: TCreateParams);
begin
  inherited;

  with Params do
  begin
    Style := Style or WS_CLIPCHILDREN;
    ExStyle := ExStyle or WS_EX_CONTROLPARENT;
  end;

  if Parent = nil then
    Params.WndParent := Application.Handle;
end;

procedure TDesignPanel.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineProperty('ClientHeight', ReadDummyInt, nil, False);
  Filer.DefineProperty('ClientWidth', ReadDummyInt, nil, False);
  Filer.DefineProperty('OldCreateOrder', ReadDummyBoolean, nil, False);
end;

procedure TDesignPanel.ReadDummyInt(Reader: TReader);
begin
  Reader.ReadInteger;
end;

procedure TDesignPanel.ReadDummyBoolean(Reader: TReader);
begin
  Reader.ReadBoolean;
end;

procedure TDesignPanel.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  I: Integer;
  OwnedComponent: TComponent;
begin
  inherited GetChildren(Proc, Root);
  if Root = Self then
    for I := 0 to ComponentCount - 1 do
    begin
      OwnedComponent := Components[I];
      if not OwnedComponent.HasParent then Proc(OwnedComponent);
    end;
end;

procedure TDesignPanel.SetParent(AParent: TWinControl);
begin
  if (Parent = nil) and HandleAllocated then
    DestroyHandle;
  inherited;
end;

end.
