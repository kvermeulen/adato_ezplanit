unit EzTimebarDialog;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, EzDateTime, EzTimebar, Dialogs,
  EzColorCombobox, DelayedIdle;

type
  FormatObject = class(TObject)
  public
    Format: string;
    constructor Create(_Format: string);

  end;

  TEzTimeBarDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    FontDialog: TFontDialog;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    EdWidth: TEdit;
    UdWidth: TUpDown;
    gbBands: TGroupBox;
    gbProperties: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label10: TLabel;
    cbUnits: TComboBox;
    cbLabel: TComboBox;
    cbAlignment: TComboBox;
    edCount: TEdit;
    udCount: TUpDown;
    Panel1: TPanel;
    Panel2: TPanel;
    lbBands: TListBox;
    btnAdd: TSpeedButton;
    btnDelete: TSpeedButton;
    btnMoveUp: TSpeedButton;
    btnMoveDown: TSpeedButton;
    EzDelayedIdle1: TEzDelayedIdle;
    Label5: TLabel;
    chkGanttDividers: TCheckBox;
    chkGraphDividers: TCheckBox;
    gbPredefinedScales: TGroupBox;
    Label2: TLabel;
    cbScales: TComboBox;
    Button1: TButton;
    Button2: TButton;
    GroupBox2: TGroupBox;
    pnlExample: TPanel;
    lbColor: TLabel;
    pnlColorCombobox: TPanel;
    Button3: TButton;
    procedure CbUnitsChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnFontClick(Sender: TObject);
    procedure cbColorChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnAddClick(Sender: TObject);
    procedure EzDelayedIdle1DelayedIdle(Sender: TObject;
      var NewState: TEzIdleState);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnMoveUpClick(Sender: TObject);
    procedure btnMoveDownClick(Sender: TObject);
    procedure cbLabelChange(Sender: TObject);
    procedure cbAlignmentChange(Sender: TObject);
    procedure chkGraphDividersClick(Sender: TObject);
    procedure lbBandsClick(Sender: TObject);
    procedure chkGanttDividersClick(Sender: TObject);
    procedure edCountChange(Sender: TObject);
    procedure EdWidthChange(Sender: TObject);
    procedure cbScalesChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    cbColor: TEzColorComboBox;
    FExampleBar, FTimebar: TEzTimeBar;
    FExampleStorage: TEzTimescaleStorage;
    DialogUpdating: Boolean;

  protected
    procedure AddObjectsToComboBox(LabelBox: TComboBox; Labels, Formats: array of WideString; SelectedFormat: WideString);
    procedure ExamplebarLayoutChange(Sender: TObject);
    procedure UpdateDialog;
    procedure SetTimebar(Value: TEzTimebar);

  public
    procedure FillLabelComboBox(Scale: TScale; LabelBox: TComboBox; SelectedFormat: string);

  published
    property Examplebar: TEzTimebar read FExamplebar;
    property Timebar: TEzTimebar read FTimebar write SetTimebar;
  end;

implementation

{$R *.DFM}

uses EzStrings_2, Math, EzPainting;

constructor FormatObject.Create(_Format: string);
begin
  inherited Create;
  Format := _Format;
end;

procedure TEzTimeBarDlg.FormCreate(Sender: TObject);
begin
  DialogUpdating := false;
  FExamplebar := TEzTimebar.Create(Self);
  FExamplebar.Parent := pnlExample;
  FExamplebar.UserCanEdit := False;
  FExamplebar.OnLayoutChange := ExamplebarLayoutChange;

  FExampleStorage := TEzTimescaleStorage.Create(Self);

  // Because the TEzColorComboBox component is part of this package,
  // we cannot use Delphi's designer to add it to the dialog box.
  // Therefore we create one manualy.
  cbColor := TEzColorComboBox.Create(Self);
  cbColor.Parent := pnlColorCombobox;
  cbColor.Align := alClient;
  cbColor.OnChange := cbColorChange;
end;

procedure TEzTimeBarDlg.CbUnitsChange(Sender: TObject);
var
  Band: TEzTimescaleBand;
begin
  Band := FExamplebar.ActiveScale.Bands[lbBands.ItemIndex];
  Band.Scale := TScale(cbUnits.ItemIndex);
  FillLabelComboBox(Band.Scale, cbLabel, Band.DisplayFormat);
end;

procedure TEzTimeBarDlg.FillLabelComboBox(Scale: TScale; LabelBox: TComboBox; SelectedFormat: string);
begin
  case Scale of
    msYear: AddObjectsToComboBox(LabelBox, YearLabels, YearFormats, SelectedFormat);
    msQuarter: AddObjectsToComboBox(LabelBox, QuarterLabels,QuarterFormats, SelectedFormat);
    msMonth: AddObjectsToComboBox(LabelBox, MonthLabels, MonthFormats, SelectedFormat);
    msWeek: AddObjectsToComboBox(LabelBox, WeekLabels, WeekFormats, SelectedFormat);
    msDay: AddObjectsToComboBox(LabelBox, DayLabels, DayFormats, SelectedFormat);
    msHour: AddObjectsToComboBox(LabelBox, HourLabels, HourFormats, SelectedFormat);
    msMinute: AddObjectsToComboBox(LabelBox, MinuteLabels, MinuteFormats, SelectedFormat);
  end;
end;

procedure TEzTimeBarDlg.AddObjectsToComboBox(LabelBox: TComboBox; Labels, Formats: array of WideString; SelectedFormat: WideString);
var
  C, I: integer;
begin
  I := -1;
  LabelBox.Items.Clear;
  for C:=0 to High(Formats) do
  begin
    if Formats[C]=SelectedFormat then
      I:=C;
    LabelBox.Items.Add(Labels[C]); //  + ' (' + Formats[C] + ')');
  end;

  if I=-1 then
    LabelBox.Text := SelectedFormat {no default label selected} else
    LabelBox.ItemIndex := I;
end;

procedure TEzTimeBarDlg.ExamplebarLayoutChange(Sender: TObject);
begin
  if DialogUpdating then Exit;
  UpdateDialog;
end;

procedure TEzTimeBarDlg.UpdateDialog;
var
  i, BandIndex: Integer;
  Band: TEzTimescaleBand;

begin
  DialogUpdating := true;
  try
    cbScales.Items.Clear;

    BandIndex := lbBands.ItemIndex;
    lbBands.Items.Clear;

    pnlColorCombobox.Visible := FExamplebar.ButtonStyle<>btsXPButton;
    lbColor.Visible := pnlColorCombobox.Visible;

    if not FExamplebar.HasActiveScale then Exit;
    //
    // Load scales
    //
    for i:=0 to FExamplebar.ScaleStorage.Scales.Count-1 do
      cbScales.Items.Add(FExamplebar.ScaleStorage.Scales[i].DisplayName);
    cbScales.ItemIndex := FExamplebar.ScaleIndex;

    //
    // Load bands
    //
    udWidth.Position := FExamplebar.ActiveScale.MajorCellWidth;
    for i:=0 to FExamplebar.ActiveScale.Bands.Count-1 do
      lbBands.Items.Add(FExamplebar.ActiveScale.Bands[i].DisplayName);

    BandIndex := min(FExamplebar.ActiveScale.Bands.Count-1, BandIndex);
    if BandIndex <> -1 then
    begin
      lbBands.ItemIndex := BandIndex;
      gbProperties.Enabled := True;
      Band := FExamplebar.ActiveScale.Bands[BandIndex];
      cbUnits.ItemIndex := Ord(Band.Scale);
      if ActiveControl <> cbLabel then
        FillLabelComboBox(Band.Scale, cbLabel, Band.DisplayFormat);
      cbAlignment.ItemIndex := Ord(Band.Alignment);
      udCount.Position := Band.Count;
      cbColor.SelectedColor := Band.Color;
      chkGanttDividers.Checked := Band.GanttShowsDividers;
      chkGraphDividers.Checked := Band.GraphShowsDividers;
    end else
      gbProperties.Enabled := False;
  finally
    DialogUpdating := false;
  end;
end;

procedure TEzTimeBarDlg.BtnFontClick(Sender: TObject);
var
  Band: TEzTimescaleBand;
begin
  Band := FExamplebar.ActiveScale.Bands[lbBands.ItemIndex];
  FontDialog.Font.Assign(Band.Font);
  if FontDialog.Execute then
    Band.Font := FontDialog.Font;
end;

procedure TEzTimeBarDlg.cbColorChange(Sender: TObject);
var
  Band: TEzTimescaleBand;
begin
  Band := FExamplebar.ActiveScale.Bands[lbBands.ItemIndex];
  Band.Color := cbColor.SelectedColor;
end;

procedure TEzTimeBarDlg.SetTimebar(Value: TEzTimebar);
begin
  if Value <> FTimebar then
  begin
    FTimebar := Value;
    if FTimebar<>nil then
    begin
      if not FTimebar.HasActiveScale then
        raise Exception.Create('Dialog can only be used when timebar has an active scale.');

      // Copy contents of scale storage object used by timebar into
      // a temporary object
      FExampleStorage.Assign(FTimebar.ScaleStorage);

      // Copy properties of edit bar into example bar
      FExamplebar.Assign(Value);

      // Resore link to our example storage
      FExamplebar.ScaleStorage := FExampleStorage;
      FExamplebar.Height := FTimebar.Height;
    end;
    UpdateDialog;
  end;
end;

procedure TEzTimeBarDlg.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  SavedStorage: TEzTimescaleStorage;

begin
  if (Modalresult = idOK) then
  begin
    SavedStorage := FTimebar.ScaleStorage;
    try
      FTimebar.Assign(FExamplebar);
      SavedStorage.Assign(FExampleStorage);
    finally
      FTimebar.ScaleStorage := SavedStorage;
    end;
  end;
end;

procedure TEzTimeBarDlg.btnAddClick(Sender: TObject);
begin
  FExamplebar.ActiveScale.Bands.Add;
end;

procedure TEzTimeBarDlg.EzDelayedIdle1DelayedIdle(Sender: TObject;
  var NewState: TEzIdleState);
begin
  btnDelete.Enabled := lbBands.ItemIndex<>-1;
  btnMoveUp.Enabled := lbBands.ItemIndex>=1;
  btnMoveDown.Enabled := lbBands.ItemIndex<lbBands.Items.Count-1;
end;

procedure TEzTimeBarDlg.btnDeleteClick(Sender: TObject);
begin
  if lbBands.ItemIndex>=0 then
    FExamplebar.ActiveScale.Bands.Delete(lbBands.ItemIndex);
end;

procedure TEzTimeBarDlg.btnMoveUpClick(Sender: TObject);
var
  i: Integer;

begin
  if lbBands.ItemIndex>=1 then
  begin
    i := lbBands.ItemIndex;
    FExamplebar.ActiveScale.Bands[i].Index := FExamplebar.ActiveScale.Bands[i].Index-1;
    lbBands.ItemIndex := i-1;
  end;
end;

procedure TEzTimeBarDlg.btnMoveDownClick(Sender: TObject);
var
  i: Integer;

begin
  if (lbBands.ItemIndex<>-1) and (lbBands.ItemIndex<lbBands.Items.Count-1) then
  begin
    i := lbBands.ItemIndex;
    FExamplebar.ActiveScale.Bands[i].Index := FExamplebar.ActiveScale.Bands[i].Index+1;
    lbBands.ItemIndex := i+1;
  end;
end;

procedure TEzTimeBarDlg.cbLabelChange(Sender: TObject);
var
  i: Integer;
  Band: TEzTimescaleBand;

begin
  i := cbLabel.Items.IndexOf(cbLabel.Text);
  Band := FExamplebar.ActiveScale.Bands[lbBands.ItemIndex];
  if i=-1 then
    Band.DisplayFormat := cbLabel.Text
  else
    case Band.Scale of
      msYear: Band.DisplayFormat := YearFormats[i];
      msQuarter: Band.DisplayFormat := QuarterFormats[i];
      msMonth: Band.DisplayFormat := MonthFormats[i];
      msWeek: Band.DisplayFormat := WeekFormats[i];
      msDay: Band.DisplayFormat := DayFormats[i];
      msHour: Band.DisplayFormat := HourFormats[i];
      msMinute: Band.DisplayFormat := MinuteFormats[i];
    end;
end;

procedure TEzTimeBarDlg.cbAlignmentChange(Sender: TObject);
begin
  FExamplebar.ActiveScale.Bands[lbBands.ItemIndex].Alignment := TAlignment(cbAlignment.ItemIndex);
end;

procedure TEzTimeBarDlg.chkGraphDividersClick(Sender: TObject);
begin
  if not DialogUpdating then
    FExamplebar.ActiveScale.Bands[lbBands.ItemIndex].GraphShowsDividers := chkGraphDividers.Checked;
end;

procedure TEzTimeBarDlg.lbBandsClick(Sender: TObject);
begin
  UpdateDialog;
end;

procedure TEzTimeBarDlg.chkGanttDividersClick(Sender: TObject);
begin
  if not DialogUpdating then
    FExamplebar.ActiveScale.Bands[lbBands.ItemIndex].GanttShowsDividers := chkGanttDividers.Checked;
end;

procedure TEzTimeBarDlg.edCountChange(Sender: TObject);
begin
  if lbBands.ItemIndex<>-1 then
    FExamplebar.ActiveScale.Bands[lbBands.ItemIndex].Count := udCount.Position;
end;

procedure TEzTimeBarDlg.EdWidthChange(Sender: TObject);
begin
  try
    FExamplebar.ActiveScale.MajorCellWidth := StrToInt(EdWidth.Text);
  except
  end;
end;

procedure TEzTimeBarDlg.cbScalesChange(Sender: TObject);
begin
  FExamplebar.ScaleIndex := cbScales.ItemIndex;
end;

procedure TEzTimeBarDlg.Button1Click(Sender: TObject);
var
  s: string;
  Scale: TEzTimeScale;

begin
  if InputQuery('Add new scale', 'Enter name for new scale', s) then
  begin
    Scale := FExamplebar.ScaleStorage.Scales.Add;
    Scale.Name := s;
    FExamplebar.ScaleIndex := FExamplebar.ScaleStorage.Scales.Count-1;
  end;
end;

procedure TEzTimeBarDlg.Button2Click(Sender: TObject);
begin
  if FExamplebar.ScaleStorage.Scales.Count=1 then
  begin
    MessageDlg(SYouCannotDeleteFinalScale, mtError, [mbOK], 0);
    Exit;
  end;

  if MessageDlg(SConfirmDeleteScale, mtConfirmation, mbOkCancel, 0)=idOK then
  begin
    FExamplebar.ScaleStorage.Scales.Delete(cbScales.ItemIndex);
    FExamplebar.ScaleIndex := FExamplebar.ScaleIndex-1;
  end;
end;

end.
