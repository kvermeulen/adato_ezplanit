unit EzPainting;

{$I Ez.inc}

interface

uses Windows, Graphics, Math, Classes, SysUtils;

type
  TEzButtonStyle = (btsThickButton, btsThinButton, btsFlatButton, btsFlatRect, btsXPButton);
  TEzButtonState = (bstNormal, bstDisabled, bstDown, bstHover);
  TEzColorSteps = 2..255;
  TEzTextState = (tsNormal, tsDisabled, tsHighlight);

  TEzGradientStyle = (grFilled, grEllipse, grHorizontal, grVertical, grPyramid, grMount);
  TEzGradientFillStyle = class
  public
    Steps: Word;
    Style: TEzGradientStyle;
    StartColor: TColor;
    EndColor: TColor;
    CacheBitmap: TBitmap;
    CacheWidth: Integer;
    CacheHeight: Integer;

    constructor Create;
    destructor  Destroy; override;
  end;

  procedure EzBeginPrint;
  procedure EzEndPrint;
  function  EzIsPrinting : Boolean;
  procedure EzGradientFill(ACanvas: TCanvas; ARect: TRect; GradientStyle: TEzGradientFillStyle);
  procedure EzRenderButton(Bitmap: TBitmap; var ARect: TRect; Style: TEzButtonStyle; State: TEzButtonState; Flags: Cardinal); overload;
  procedure EzRenderButton(ACanvas: TCanvas; var ARect: TRect; Style: TEzButtonStyle; State: TEzButtonState; Flags: Cardinal; DoubleBuffered: Boolean = true); overload;
  procedure EzRenderText(Bitmap: TBitmap; var ARect: TRect; const Text: string; State: TEzTextState; Flags: Cardinal); overload;
  procedure EzRenderText(ACanvas: TCanvas; var ARect: TRect; const Text: string; State: TEzTextState; Flags: Cardinal; DoubleBuffered: Boolean = false); overload;

  procedure EzRenderTextRect(Bitmap: TBitmap; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0); overload;
  procedure EzRenderTextRect(ACanvas: TCanvas; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0; DoubleBuffered: Boolean = true); overload;

  procedure EzRenderTextButton(Bitmap: TBitmap; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0); overload;
  procedure EzRenderTextButton(ACanvas: TCanvas; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; DoubleBuffered: Boolean = true); overload;

  function  IntersectionPoint(P1, P2, P3, P4: TPoint): TPoint;
  procedure PrintBitmap(Canvas: TCanvas; DestRect: TRect; Bitmap: TBitmap; dwROP: DWORD = SRCCOPY);
  procedure CopyDraw(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect); overload;
  procedure TransparentDraw(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect); overload;
  procedure TransparentDraw(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect; UseCopy: Boolean); overload;
  procedure TransparentDraw(AHandle: HDC; Bitmap: TBitmap; const ARect: TRect; UseCopy: Boolean); overload;
  procedure TransparentDraw(AHandle: HDC; Bitmap: TBitmap; DestX, DestY, W, H, SrcX, SrcY: Integer; UseCopy: Boolean); overload;
  procedure TransparentPrint(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect);

{$IFDEF EZ_D6}
  // WideString versions
  procedure EzRenderText(Bitmap: TBitmap; var ARect: TRect; const Text: WideString; State: TEzTextState; Flags: Cardinal); overload;
  procedure EzRenderText(ACanvas: TCanvas; var ARect: TRect; const Text: WideString; State: TEzTextState; Flags: Cardinal; DoubleBuffered: Boolean = false); overload;

  procedure EzRenderTextRect(Bitmap: TBitmap; var ARect: TRect; const Text: WideString;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0); overload;

  procedure EzRenderTextRect(ACanvas: TCanvas; var ARect: TRect; const Text: WideString;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0; DoubleBuffered: Boolean = true); overload;
{$ENDIF}

var
  FSharedBitmap, FMaskBitmap: TBitmap;
  FUseCount: integer;
  EzTransparentColor: TColor;
  EzPrintCount: Integer;

implementation

uses Dialogs;

procedure UseBitmap;
begin
  if FUseCount = 0 then
  begin
    EzTransparentColor := $00404040;
    FSharedBitmap := TBitmap.Create;
    FSharedBitmap.TransparentColor := EzTransparentColor;
    FMaskBitmap := TBitmap.Create;
    FMaskBitmap.MonoChrome := True;
  end;

  inc(FUseCount);
end;

procedure ReleaseBitmap;
begin
  dec(FUseCount);
  if FUseCount = 0 then
  begin
    FSharedBitmap.Free;
    FMaskBitmap.Free;
  end;
end;

procedure EzBeginPrint;
begin
  Inc(EzPrintCount);
end;

procedure EzEndPrint;
begin
  if EzPrintCount > 0 then
    dec(EzPrintCount);
end;

function EzIsPrinting : Boolean;
begin
  Result := EzPrintCount > 0;
end;

constructor TEzGradientFillStyle.Create;
begin
  inherited;
  Steps := 5;
  Style := grFilled;
  StartColor := clWhite;
  EndColor := clBlue;
  CacheBitmap := nil;
  CacheWidth := 0;
  CacheHeight := 0;
end;

destructor TEzGradientFillStyle.Destroy;
begin
  inherited;
  CacheBitmap.Free;
end;

procedure EzGradientFill(ACanvas: TCanvas; ARect: TRect; GradientStyle: TEzGradientFillStyle);
var
  bt: TBitmap;
  i, w, h: Integer;
  j, k: Real;
  Deltas: array[0..2] of Real; //R,G,B
  r: TRect;
  FStart, FEnd: TColor;
begin
  with GradientStyle do
  begin
    w := 0;
    h := 0;
    if not (Style in [grVertical, grHorizontal]) then
    begin
      w := ARect.Right - ARect.Left;
      h := ARect.Bottom - ARect.Top;
    end
    else if Style = grVertical then
    begin
      w := 1;
      h := ARect.Bottom - ARect.Top;
    end else if Style = grHorizontal then
    begin
      w := ARect.Right - ARect.Left;
      h := 1;
    end;

    if (CacheWidth <> w) or (CacheWidth <> h) then
    begin
      if StartColor < 0 then
        FStart := GetSysColor(StartColor and not $80000000) else
        FStart := StartColor;

      if EndColor < 0 then
        FEnd := GetSysColor(EndColor and not $80000000) else
        FEnd := EndColor;

      CacheWidth := w;
      CacheHeight := h;
      bt := TBitmap.Create;
      bt.Width := CacheWidth;
      bt.Height := CacheHeight;
      case Style of
        grFilled:
          begin
            bt.Canvas.Brush.Color := FStart;
            bt.Canvas.Brush.Style := bsSolid;
            bt.Canvas.FillRect(Rect(0, 0, CacheWidth, CacheHeight));
          end;
        grEllipse:
          begin
            bt.Canvas.Brush.Color := FStart;
            bt.Canvas.Brush.Style := bsSolid;
            bt.Canvas.FillRect(Rect(0, 0, CacheWidth, CacheHeight));
            if Steps > (CacheWidth div 2) then
              Steps := Trunc(CacheWidth / 2);
            if Steps > (CacheHeight div 2) then
              Steps := Trunc(CacheHeight / 2);
            Deltas[0] := (GetRValue(FEnd) - GetRValue(FStart)) / Steps;
            Deltas[1] := (GetGValue(FEnd) - GetGValue(FStart)) / Steps;
            Deltas[2] := (GetBValue(FEnd) - GetBValue(FStart)) / Steps;

            j := (CacheWidth / Steps) / 2;
            k := (CacheHeight / Steps) / 2;
            for i := 0 to Steps do
            begin
              r.Top := Round(i * k);
              r.Bottom := CacheHeight - r.Top;
              r.Right := Round(i * j);
              r.Left := CacheWidth - r.Right;
              bt.Canvas.Brush.Color := RGB(Round(GetRValue(FStart) + i * Deltas[0]), Round(GetGValue(FStart) + i *
                Deltas[1]), Round(GetBValue(FStart) + i * Deltas[2]));
              bt.Canvas.Pen.Color := bt.Canvas.Brush.Color;
              bt.Canvas.Ellipse(r.Right, r.Top, r.Left, r.Bottom);
            end;
          end;
        grHorizontal:
          begin
            if Steps > CacheWidth then
              Steps := CacheWidth;
            Deltas[0] := (GetRValue(FEnd) - GetRValue(FStart)) / Steps;
            Deltas[1] := (GetGValue(FEnd) - GetGValue(FStart)) / Steps;
            Deltas[2] := (GetBValue(FEnd) - GetBValue(FStart)) / Steps;
            bt.Canvas.Brush.Style := bsSolid;
            j := CacheWidth / Steps;
            for i := 0 to Steps do
            begin
              r.Top := 0;
              r.Bottom := CacheHeight;
              r.Left := Round(i * j);
              r.Right := Round((i + 1) * j);
              bt.Canvas.Brush.Color := RGB(Round(GetRValue(FStart) + i * Deltas[0]), Round(GetGValue(FStart) + i *
                Deltas[1]), Round(GetBValue(FStart) + i * Deltas[2]));
              bt.Canvas.FillRect(r);
            end;
          end;
        grVertical:
          begin
            if Steps > CacheHeight then
              Steps := CacheHeight;
            Deltas[0] := (GetRValue(FEnd) - GetRValue(FStart)) / Steps;
            Deltas[1] := (GetGValue(FEnd) - GetGValue(FStart)) / Steps;
            Deltas[2] := (GetBValue(FEnd) - GetBValue(FStart)) / Steps;
            bt.Canvas.Brush.Style := bsSolid;
            j := CacheHeight / Steps;
            for i := 0 to Steps do
            begin
              r.Left := CacheWidth;
              r.Right := 0;
              r.Top := Round(i * j);
              r.Bottom := Round((i + 1) * j);
              bt.Canvas.Brush.Color := RGB(Round(GetRValue(FStart) + i * Deltas[0]), Round(GetGValue(FStart) + i *
                Deltas[1]), Round(GetBValue(FStart) + i * Deltas[2]));
              bt.Canvas.FillRect(r);
            end;
          end;
      grMount:
          begin
            bt.Canvas.Brush.Color := FStart;
            bt.Canvas.Brush.Style := bsSolid;
            bt.Canvas.FillRect(Rect(0, 0, CacheWidth, CacheHeight));
            if Steps > (CacheWidth div 2) then
              Steps := Trunc(CacheWidth / 2);
            if Steps > (CacheHeight div 2) then
              Steps := Trunc(CacheHeight / 2);
            Deltas[0] := (GetRValue(FEnd) - GetRValue(FStart)) / Steps;
            Deltas[1] := (GetGValue(FEnd) - GetGValue(FStart)) / Steps;
            Deltas[2] := (GetBValue(FEnd) - GetBValue(FStart)) / Steps;
            j := (CacheWidth / Steps) / 2;
            k := (CacheHeight / Steps) / 2;
            for i := 0 to Steps do
            begin
              r.Top := Round(i * k);
              r.Bottom := CacheHeight - r.Top;
              r.Right := Round(i * j);
              r.Left := CacheWidth - r.Right;
              bt.Canvas.Brush.Color := RGB(Round(GetRValue(FStart) + i * Deltas[0]), Round(GetGValue(FStart) + i *
                Deltas[1]), Round(GetBValue(FStart) + i * Deltas[2]));
              bt.Canvas.Pen.Color := bt.Canvas.Brush.Color;
              bt.Canvas.RoundRect(r.Right, r.Top, r.Left, r.Bottom, ((r.Left - r.Right) div 2), ((r.Bottom - r.Top) div
                2));
            end;
          end;
        grPyramid:
          begin
            bt.Canvas.Brush.Color := FStart;
            bt.Canvas.Brush.Style := bsSolid;
          bt.Canvas.FillRect(Rect(0, 0, CacheWidth, CacheHeight));
            if Steps > (CacheWidth div 2) then
              Steps := Trunc(CacheWidth / 2);
            if Steps > (CacheHeight div 2) then
              Steps := Trunc(CacheHeight / 2);
            Deltas[0] := (GetRValue(FEnd) - GetRValue(FStart)) / Steps;
            Deltas[1] := (GetGValue(FEnd) - GetGValue(FStart)) / Steps;
            Deltas[2] := (GetBValue(FEnd) - GetBValue(FStart)) / Steps;
            j := (CacheWidth / Steps) / 2;
            k := (CacheHeight / Steps) / 2;
            for i := 0 to Steps do
            begin
              r.Top := Round(i * k);
              r.Bottom := CacheHeight - r.Top;
              r.Right := Round(i * j);
            r.Left := CacheWidth - r.Right;
              bt.Canvas.Brush.Color := RGB(Round(GetRValue(FStart) + i * Deltas[0]), Round(GetGValue(FStart) + i *
                Deltas[1]), Round(GetBValue(FStart) + i * Deltas[2]));
              bt.Canvas.Pen.Color := bt.Canvas.Brush.Color;
              bt.Canvas.FillRect(Rect(r.Right, r.Top, r.Left, r.Bottom));
            end;
          end;
        end;
        if not Assigned(CacheBitmap) then
          CacheBitmap := bt
        else
        begin
          CacheBitmap.Assign(bt);
          bt.Free;
        end;
    end;

    if not (Style in [grVertical, grHorizontal]) then
      CopyDraw(ACanvas, CacheBitmap, ARect) else
      ACanvas.StretchDraw(ARect, CacheBitmap);
  end;
end;

function IntersectionPoint(P1, P2, P3, P4: TPoint): TPoint;
var
  denom: integer;

begin
  denom := (P4.Y-P3.Y)*(P2.X-P1.X)-(P4.X-P3.X)*(P2.Y-P1.Y);
  if denom=0 then
  begin
    Result := Point(MaxInt, MaxInt);
    Exit;
  end;
  Result.X := P1.X + MulDiv((P4.X-P3.X)*(P1.Y-P3.Y)-(P4.Y-P3.Y)-(P1.X-P3.X), (P2.X-P1.X), denom);
  Result.Y := P1.Y + MulDiv((P2.X-P1.X)*(P1.Y-P3.Y)-(P2.Y-P1.Y)*(P1.X-P3.X), (P2.Y-P1.Y), denom);
end;

// This is the recommended way to print images on a printer.
// Based on posting to borland.public.delphi.winapi by Rodney E Geraghty, 8/8/97.
// With suggested improvements for Delphi 1 by Danny Smalle.  3/4/98.
procedure PrintBitmap(Canvas: TCanvas; DestRect: TRect; Bitmap: TBitmap; dwROP: DWORD = SRCCOPY);
  var
    BitmapHeader:  pBitmapInfo;
    BitmapImage :  POINTER;
    HeaderSize  :  DWORD;         // Delphi 3
    ImageSize   :  DWORD;
    p: TPoint;

    procedure WinGetMem(var P: Pointer; Size: DWord);
    begin
      P := VirtualAlloc(nil, Size, MEM_COMMIT or MEM_RESERVE,
                        PAGE_READWRITE);
    end;

    procedure WinFreeMem(P: Pointer);
    begin
      if not VirtualFree(P, 0, MEM_RELEASE) then
        RaiseLastWin32Error;
    end;

begin
  GetDIBSizes(Bitmap.Handle,HeaderSize,ImageSize);
  WinGetMem(pointer(BitmapHeader),HeaderSize);
  WinGetMem(BitmapImage,ImageSize);
  try
    p := DestRect.TopLeft;
    GetDIB(Bitmap.Handle, Bitmap.Palette, BitmapHeader^, BitmapImage^);
    StretchDIBits(Canvas.Handle,
                  p.x, p.y,     // Destination Origin
                  DestRect.Right  - DestRect.Left, // Destination Width
                  DestRect.Bottom - DestRect.Top,  // Destination Height
                  0,0,                             // Source Origin
                  DestRect.Right  - DestRect.Left, // Source Width
                  DestRect.Bottom - DestRect.Top,  // Source Height
                  BitmapImage,
                  TBitmapInfo(BitmapHeader^),
                  DIB_RGB_COLORS,
                  dwROP);
  finally
    WinFreeMem(BitmapHeader);
    WinFreeMem(BitmapImage)
  end;
end;  {PrintBitmap}

// Draw a transparent bitmap to printer
procedure TransparentPrint(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect);
var
  Src, hdcMask: HDC;
  W, H: Integer;
  p: TPoint;
  R: TRect;
  bmpCopy, bmpMask: TBitmap;

begin
  R := ARect;       
  if R.Left < 0 then
  begin
    p.x := -R.Left;
    R.Left := 0;
    W := R.Right-R.Left;
  end else
  begin
    p.x := 0;
    W := R.Right - R.Left;
  end;

  if R.Top < 0 then
  begin
    p.y := -R.Top;
    R.Top := 0;
    H := R.Bottom-R.Top;
  end else
  begin
    p.y := 0;
    H := R.Bottom-R.Top;
  end;

  bmpCopy := TBitmap.Create;
  bmpMask := TBitmap.Create;
  try
    bmpCopy.Width := W;
    bmpCopy.Height := H;
    bmpCopy.Canvas.CopyRect(Rect(0, 0, W, H), Bitmap.Canvas, Rect(p.x, p.y, W+p.x, H+p.y));
    Src := bmpCopy.Canvas.Handle;
    p.X := 0;
    p.Y := 0;

    bmpMask.Monochrome := True;
    bmpMask.Width := W;
    bmpMask.Height := H;
    hdcMask := bmpMask.Canvas.Handle;

    SetBkColor(Src, ColorToRGB(Bitmap.TransparentColor));
    Win32Check(BitBlt(hdcMask, 0, 0, W, H, Src, 0, 0, SRCCOPY));

    if Bitmap.TransparentColor <> clBlack then
    begin
      // Black out transparent pixels in source bitmap
      SetBkColor(Src, RGB(0,0,0));          // 1s --> black (0x000000)
      SetTextColor(Src, RGB(255,255,255));  // 0s --> white (0xFFFFFF)
      Win32Check(BitBlt(Src, 0, 0, W, H, hdcMask, 0, 0, SRCAND));
    end;

    SetBkColor(ACanvas.Handle, RGB(255, 255, 255));    // 1s --> 0xFFFFFF
    SetTextColor(ACanvas.Handle, RGB(0, 0, 0));        // 0s --> 0x000000

    PrintBitmap(ACanvas, R, bmpMask, SRCAND);
    PrintBitmap(ACanvas, R, bmpCopy, SRCPAINT);
  finally
    bmpMask.Free;
    bmpCopy.Free;
  end;
end;

// Copies bitmap onto ACanvas
procedure CopyDraw(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect);
begin
  BitBlt(ACanvas.Handle,
         ARect.Left, ARect.Top, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top,
         Bitmap.Canvas.Handle, 0, 0, SRCCOPY);
end;

// Draw a transparent bitmap to screen
procedure TransparentDraw(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect);
begin
  TransparentDraw(ACanvas, Bitmap, ARect, False);
end;

// Draw a transparent bitmap to screen
procedure TransparentDraw(ACanvas: TCanvas; Bitmap: TBitmap; const ARect: TRect; UseCopy: Boolean);
begin
  TransparentDraw(ACanvas.Handle, Bitmap, ARect, UseCopy);
end;

procedure TransparentDraw(AHandle: HDC; Bitmap: TBitmap; const ARect: TRect; UseCopy: Boolean);
var
  Src, hdcMask: HDC;
  W, H: Integer;
  BkSrc, TextSrc, BkDest, TextDest: COLORREF;
  p: TPoint;
  R: TRect;

begin
  R := ARect;
  if R.Left < 0 then
  begin
    p.x := -R.Left;
    R.Left := 0;
    W := R.Right;
  end else
  begin
    p.x := 0;
    W := R.Right - R.Left;
  end;

  if R.Top < 0 then
  begin
    p.y := -R.Top;
    R.Top := 0;
    H := R.Bottom;
  end else
  begin
    p.y := 0;
    H := R.Bottom - R.Top;
  end;

  if UseCopy then
  begin
    FSharedBitmap.Width := max(W, FSharedBitmap.Width);
    FSharedBitmap.Height := max(H, FSharedBitmap.Height);
    FSharedBitmap.Canvas.CopyRect(Rect(0, 0, W, H), Bitmap.Canvas, Rect(p.x, p.y, W+p.x, H+p.y));
    p.X := 0;
    p.Y := 0;
    Src := FSharedBitmap.Canvas.Handle;
  end else
    Src := Bitmap.Canvas.Handle;

  TextSrc := CLR_INVALID;
  FMaskBitmap.Width := max(W, FMaskBitmap.Width);
  FMaskBitmap.Height := max(H, FMaskBitmap.Height);
  hdcMask := FMaskBitmap.Canvas.Handle;

  BkSrc := SetBkColor(Src, ColorToRGB(Bitmap.TransparentColor));
  Win32Check(BitBlt(hdcMask, 0, 0, W, H, Src, p.x, p.y, SRCCOPY));

  if Bitmap.TransparentColor <> clBlack then
  begin
    // Black out transparent pixels in source bitmap
    SetBkColor(Src, RGB(0,0,0));          // 1s --> black (0x000000)
    TextSrc := SetTextColor(Src, RGB(255,255,255));  // 0s --> white (0xFFFFFF)
    Win32Check(BitBlt(Src, p.x, p.y, W, H, hdcMask, 0, 0, SRCAND));
  end;

  BkDest := SetBkColor(AHandle, RGB(255, 255, 255));    // 1s --> 0xFFFFFF
  TextDest := SetTextColor(AHandle, RGB(0, 0, 0));        // 0s --> 0x000000

  Win32Check(BitBlt(AHandle, R.Left, R.Top, W, H, hdcMask, 0, 0, SRCAND));
  Win32Check(BitBlt(AHandle, R.Left, R.Top, W, H, Src, p.x, p.y, SRCPAINT));

  // Restore
  SetBkColor(AHandle, BkDest);
  SetTextColor(AHandle, TextDest);
  SetBkColor(Src, BkSrc);
  if Bitmap.TransparentColor <> clBlack then
    SetTextColor(Src, TextSrc);
end;

procedure TransparentDraw(AHandle: HDC; Bitmap: TBitmap; DestX, DestY, W, H, SrcX, SrcY: Integer; UseCopy: Boolean); overload;
var
  Src, hdcMask: HDC;
  BkSrc, TextSrc, BkDest, TextDest: COLORREF;

begin
  if UseCopy then
  begin
    FSharedBitmap.Width := max(W, FSharedBitmap.Width);
    FSharedBitmap.Height := max(H, FSharedBitmap.Height);
    FSharedBitmap.Canvas.CopyRect(Rect(0, 0, W, H), Bitmap.Canvas, Rect(SrcX, SrcY, SrcX+W, SrcY+H));
    SrcX := 0;
    SrcY := 0;
    Src := FSharedBitmap.Canvas.Handle;
  end else
    Src := Bitmap.Canvas.Handle;

  TextSrc := CLR_INVALID;
  FMaskBitmap.Width := max(W, FMaskBitmap.Width);
  FMaskBitmap.Height := max(H, FMaskBitmap.Height);
  hdcMask := FMaskBitmap.Canvas.Handle;

  BkSrc := SetBkColor(Src, ColorToRGB(Bitmap.TransparentColor));
//  Assert(BkSrc <> CLR_INVALID);
  Win32Check(BitBlt(hdcMask, 0, 0, W, H, Src, SrcX, SrcY, SRCCOPY));

  if Bitmap.TransparentColor <> clBlack then
  begin
    // Black out transparent pixels in source bitmap
    SetBkColor(Src, RGB(0,0,0));          // 1s --> black (0x000000)
    TextSrc := SetTextColor(Src, RGB(255,255,255));  // 0s --> white (0xFFFFFF)
    Win32Check(BitBlt(Src, SrcX, SrcY, W, H, hdcMask, 0, 0, SRCAND));
  end;

  BkDest := SetBkColor(AHandle, RGB(255, 255, 255));    // 1s --> 0xFFFFFF
  TextDest := SetTextColor(AHandle, RGB(0, 0, 0));        // 0s --> 0x000000

  Win32Check(BitBlt(AHandle, DestX, DestY, W, H, hdcMask, 0, 0, SRCAND));
  Win32Check(BitBlt(AHandle, DestX, DestY, W, H, Src, SrcX, SrcY, SRCPAINT));

  // Restore
  SetBkColor(AHandle, BkDest);
  SetTextColor(AHandle, TextDest);
  SetBkColor(Src, BkSrc);
  if Bitmap.TransparentColor <> clBlack then
    SetTextColor(Src, TextSrc);
end;

procedure EzRenderButton(Bitmap: TBitmap; var ARect: TRect; Style: TEzButtonStyle; State: TEzButtonState; Flags: Cardinal);
begin
  Bitmap.Width := max(Bitmap.Width, ARect.Right);
  Bitmap.Height := max(Bitmap.Height, ARect.Bottom);
  EzRenderButton(Bitmap.Canvas, ARect, Style, State, Flags, False);
end;

procedure EzRenderButton(ACanvas: TCanvas; var ARect: TRect; Style: TEzButtonStyle; State: TEzButtonState; Flags: Cardinal; DoubleBuffered: Boolean = true);
const
  Styles: array[btsThickButton..btsFlatRect, TEzButtonState] of Cardinal =
  (
      (BDR_RAISEDINNER or BDR_RAISEDOUTER, BDR_RAISEDINNER, BDR_RAISEDINNER, BDR_SUNKENOUTER),
      (BDR_RAISEDINNER, BDR_RAISEDINNER or BDR_RAISEDOUTER, BDR_RAISEDINNER or BDR_RAISEDOUTER, BDR_SUNKENOUTER),
      (0, 0, BDR_SUNKENOUTER, BDR_SUNKENOUTER),
      (BDR_RAISEDOUTER, BDR_RAISEDOUTER, BDR_SUNKENOUTER, BDR_RAISEDINNER)
  );

  Colors: array[TEzButtonState, 0..1] of TColor =
    (($00FFFFFF, $00E7EBEF), ($00FFFFFF, $00E7EBEF), ($00FFFFFF, $00E7EBEF), ($00FFFFFF, $00E7EBEF));

var
  PaintRect: TRect;
  GradientStyle: TEzGradientFillStyle;

  procedure DoPaint(TheCanvas: TCanvas);
  begin
    if Style <> btsXPButton then
    begin
      if Flags = 0 then
        Flags := BF_RECT;

      Flags := Flags or BF_ADJUST;

      if Style = btsFlatButton then
        Flags := Flags or BF_FLAT
      else if (Style = btsFlatRect) and (State in [bstNormal, bstDisabled]) then
        Flags := Flags or BF_FLAT;

      TheCanvas.FillRect(PaintRect);
      DrawEdge(TheCanvas.Handle, PaintRect, Styles[Style, State], Flags);
    end else
    begin
      GradientStyle := TEzGradientFillStyle.Create;
      GradientStyle.Style := grVertical;
      GradientStyle.Steps := 16;
      GradientStyle.StartColor := Colors[State, 0];
      GradientStyle.EndColor := Colors[State, 1];
      EzGradientFill(TheCanvas, PaintRect, GradientStyle);
      GradientStyle.Destroy;
      TheCanvas.Brush.Style := bsClear;
      TheCanvas.Pen.Color := $00733C00;
      TheCanvas.RoundRect(PaintRect.Left, PaintRect.Top, PaintRect.Right, PaintRect.Bottom, 5, 5);
    end;
  end;

begin
  if DoubleBuffered then
    FSharedBitmap.Canvas.Lock;

  try
    if DoubleBuffered then
    begin
      SetRect(PaintRect, 0, 0, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top);
      FSharedBitmap.Width := max(FSharedBitmap.Width, PaintRect.Right);
      FSharedBitmap.Height := max(FSharedBitmap.Height, PaintRect.Bottom);
      FSharedBitmap.Canvas.Brush := ACanvas.Brush;
      if Style = btsXPButton then FSharedBitmap.Canvas.Pen := ACanvas.Pen;
      DoPaint(FSharedBitmap.Canvas);
      CopyDraw(ACanvas, FSharedBitmap, ARect);
      SetRect(ARect, ARect.Left, ARect.Top, ARect.Left+PaintRect.Right, ARect.Top+PaintRect.Bottom);
    end else
    begin
      PaintRect := ARect;
      DoPaint(ACanvas);
      ARect := PaintRect;
    end;
  finally
    if DoubleBuffered then
      FSharedBitmap.Canvas.UnLock;
  end;
end;

procedure EzRenderText(Bitmap: TBitmap; var ARect: TRect; const Text: string; State: TEzTextState; Flags: Cardinal);
begin
  Bitmap.Width := max(Bitmap.Width, ARect.Right);
  Bitmap.Height := max(Bitmap.Height, ARect.Bottom);
  EzRenderText(Bitmap.Canvas, ARect, Text, State, Flags, False);
end;

procedure EzRenderText(ACanvas: TCanvas; var ARect: TRect; const Text: string; State: TEzTextState; Flags: Cardinal; DoubleBuffered: Boolean = false);
var
  PaintRect: TRect;
  C: TColor;

  procedure DoPaint(PaintTo: TCanvas);
  begin
    with PaintTo do
    begin
      SetBkMode(Handle, TRANSPARENT);

      C := Font.Color;

      if State = tsHighlight then
        Font.Color := clHighLightText

      else if State = tsDisabled then
      begin
        Font.Color := clHighLightText;
        OffsetRect(ARect, 1, 1);
      end;

      if DrawText(Handle, PChar(Text), -1, PaintRect, Flags) = 0 then
      {$IFNDEF EZ_D6}
        RaiseLastWin32Error;
      {$ELSE}
        RaiseLastOSError;
      {$ENDIF}

      if State = tsDisabled then
      begin
        OffsetRect(ARect, -1, -1);
        Font.Color := clBtnShadow;
        if DrawText(Handle, PChar(Text), -1, PaintRect, Flags) = 0 then
        {$IFNDEF EZ_D6}
          RaiseLastWin32Error;
        {$ELSE}
          RaiseLastOSError;
        {$ENDIF}
      end;

      Font.Color := C;
    end;
  end;

begin
  if DoubleBuffered then
    FSharedBitmap.Canvas.Lock;

  try
    if DoubleBuffered then
      with FSharedBitmap do
      begin
        SetRect(PaintRect, 0, 0, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top);

        Canvas.Font := ACanvas.Font;
        Canvas.Brush.Style := bsSolid;
        Canvas.Brush.Color := FSharedBitmap.TransparentColor;
        Canvas.FillRect(PaintRect);
        Canvas.Brush := ACanvas.Brush;

        DrawText(Canvas.Handle, PChar(Text), -1, PaintRect, DT_CALCRECT);
        PaintRect.Right := min(PaintRect.Right, ARect.Right - ARect.Left);
        PaintRect.Bottom := min(PaintRect.Bottom, ARect.Bottom - ARect.Top);

        Width := max(Width, PaintRect.Right);
        Height := max(Height, PaintRect.Bottom);

        DoPaint(Canvas);

        if (Flags and DT_BOTTOM) <> 0 then
          ARect.Top := ARect.Bottom - PaintRect.Bottom
        else if (Flags and DT_VCENTER) <> 0 then
        begin
          ARect.Top := ARect.Top + (ARect.Bottom+1-ARect.Top) div 2 - PaintRect.Bottom div 2;
          ARect.Bottom := ARect.Top + PaintRect.Bottom;
        end
        else // DT_TOP
          ARect.Bottom := ARect.Top + PaintRect.Bottom;


        if (Flags and DT_RIGHT) <> 0 then
          ARect.Left := ARect.Right - PaintRect.Right
        else if (Flags and DT_CENTER) <> 0 then
        begin
          ARect.Left := ARect.Left + (ARect.Right-ARect.Left) div 2 - PaintRect.Right div 2;
          ARect.Right := ARect.Left + PaintRect.Right;
        end
        else // DT_LEFT
          ARect.Right := ARect.Left + PaintRect.Right;

        TransparentDraw(ACanvas, FSharedBitmap, ARect);
      end
    else
    begin
      PaintRect := ARect;
      DoPaint(ACanvas);
      ARect := PaintRect;
    end;
  finally
    if DoubleBuffered then
      FSharedBitmap.Canvas.UnLock;
  end;
end;

procedure EzRenderTextRect(Bitmap: TBitmap; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0);

begin
  Bitmap.Width := max(Bitmap.Width, ARect.Right);
  Bitmap.Height := max(Bitmap.Height, ARect.Bottom);
  EzRenderTextRect(Bitmap.Canvas, ARect, Text, Style, ButtonState, ButtonFlags, TextFlags, False);
end;

procedure EzRenderTextRect(ACanvas: TCanvas; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0; DoubleBuffered: Boolean = true);
var
  PaintRect: TRect;

  procedure DoPaint(TheCanvas: TCanvas);
  var
    TextState: TEzTextState;

  begin
    EzRenderButton(TheCanvas, PaintRect, Style, ButtonState, ButtonFlags, False);

    TextState := tsNormal;
    if ButtonState = bstDisabled then
      TextState := tsDisabled
    else if ButtonState = bstHover then
      TextState := tsHighLight
    else if ButtonState = bstDown then
      inc(ARect.Top, 1);

{
    if TextFlags = 0 then
      TextFlags := DT_CENTER or DT_VCENTER or DT_SINGLELINE or DT_WORD_ELLIPSIS;
}
    InflateRect(PaintRect, -2, -2);
    EzRenderText(TheCanvas, PaintRect, Text, TextState, TextFlags, False);
  end;

begin
  if DoubleBuffered then
    FSharedBitmap.Canvas.Lock;

  try
    if DoubleBuffered then
      with FSharedBitmap do
      begin
        SetRect(PaintRect, 0, 0, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top);
        FSharedBitmap.Width := max(FSharedBitmap.Width, PaintRect.Right);
        FSharedBitmap.Height := max(FSharedBitmap.Height, PaintRect.Bottom);
        FSharedBitmap.Canvas.Font := ACanvas.Font;
        FSharedBitmap.Canvas.Brush := ACanvas.Brush;
        if Style = btsXPButton then FSharedBitmap.Canvas.Pen := ACanvas.Pen;
        DoPaint(FSharedBitmap.Canvas);
        CopyDraw(ACanvas, FSharedBitmap, ARect);
        SetRect(ARect, ARect.Left, ARect.Top, ARect.Left+PaintRect.Right, ARect.Top+PaintRect.Bottom);
      end
    else
    begin
      PaintRect := ARect;
      DoPaint(ACanvas);
      ARect := PaintRect;
    end;
  finally
    if DoubleBuffered then
      FSharedBitmap.Canvas.UnLock;
  end;
end;

procedure EzRenderTextButton(Bitmap: TBitmap; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0);
begin
  EzRenderTextRect(Bitmap, ARect, Text, Style, ButtonState, ButtonFlags);
end;

procedure EzRenderTextButton(ACanvas: TCanvas; var ARect: TRect; const Text: string;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; DoubleBuffered: Boolean = true);
begin
  EzRenderTextRect(ACanvas, ARect, Text, Style, ButtonState, ButtonFlags);
end;

{$IFDEF EZ_D6}
procedure EzRenderText(Bitmap: TBitmap; var ARect: TRect; const Text: WideString; State: TEzTextState; Flags: Cardinal);
begin
  Bitmap.Width := max(Bitmap.Width, ARect.Right);
  Bitmap.Height := max(Bitmap.Height, ARect.Bottom);
  EzRenderText(Bitmap.Canvas, ARect, Text, State, Flags, False);
end;

procedure EzRenderText(ACanvas: TCanvas; var ARect: TRect; const Text: WideString; State: TEzTextState; Flags: Cardinal; DoubleBuffered: Boolean = false);
var
  PaintRect: TRect;
  C: TColor;

  procedure DoPaint(PaintTo: TCanvas);
  begin
    with PaintTo do
    begin
      SetBkMode(Handle, TRANSPARENT);

      C := Font.Color;

      if State = tsHighlight then
        Font.Color := clHighLightText

      else if State = tsDisabled then
      begin
        Font.Color := clHighLightText;
        OffsetRect(ARect, 1, 1);
      end;

      if DrawTextW(Handle, PWideChar(Text), -1, PaintRect, Flags) = 0 then
      {$IFNDEF EZ_D6}
        RaiseLastWin32Error;
      {$ELSE}
        RaiseLastOSError;
      {$ENDIF}

      if State = tsDisabled then
      begin
        OffsetRect(ARect, -1, -1);
        Font.Color := clBtnShadow;
        if DrawTextW(Handle, PWideChar(Text), -1, PaintRect, Flags) = 0 then
        {$IFNDEF EZ_D6}
          RaiseLastWin32Error;
        {$ELSE}
          RaiseLastOSError;
        {$ENDIF}
      end;

      Font.Color := C;
    end;
  end;

begin
  if DoubleBuffered then
    FSharedBitmap.Canvas.Lock;

  try
    if DoubleBuffered then
      with FSharedBitmap do
      begin
        SetRect(PaintRect, 0, 0, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top);

        Canvas.Font := ACanvas.Font;
        Canvas.Brush.Style := bsSolid;
        Canvas.Brush.Color := FSharedBitmap.TransparentColor;
        Canvas.FillRect(PaintRect);
        Canvas.Brush := ACanvas.Brush;

        DrawTextW(Canvas.Handle, PWideChar(Text), -1, PaintRect, DT_CALCRECT);
        PaintRect.Right := min(PaintRect.Right, ARect.Right - ARect.Left);
        PaintRect.Bottom := min(PaintRect.Bottom, ARect.Bottom - ARect.Top);

        Width := max(Width, PaintRect.Right);
        Height := max(Height, PaintRect.Bottom);

        DoPaint(Canvas);

        if (Flags and DT_BOTTOM) <> 0 then
          ARect.Top := ARect.Bottom - PaintRect.Bottom
        else if (Flags and DT_VCENTER) <> 0 then
        begin
          ARect.Top := ARect.Top + (ARect.Bottom+1-ARect.Top) div 2 - PaintRect.Bottom div 2;
          ARect.Bottom := ARect.Top + PaintRect.Bottom;
        end
        else // DT_TOP
          ARect.Bottom := ARect.Top + PaintRect.Bottom;

        if (Flags and DT_RIGHT) <> 0 then
          ARect.Left := ARect.Right - PaintRect.Right
        else if (Flags and DT_CENTER) <> 0 then
        begin
          ARect.Left := ARect.Left + (ARect.Right-ARect.Left) div 2 - PaintRect.Right div 2;
          ARect.Right := ARect.Left + PaintRect.Right;
        end
        else // DT_LEFT
          ARect.Right := ARect.Left + PaintRect.Right;

        TransparentDraw(ACanvas, FSharedBitmap, ARect);
      end
    else
    begin
      PaintRect := ARect;
      DoPaint(ACanvas);
      ARect := PaintRect;
    end;
  finally
    if DoubleBuffered then
      FSharedBitmap.Canvas.UnLock;
  end;
end;

procedure EzRenderTextRect(Bitmap: TBitmap; var ARect: TRect; const Text: WideString;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0);

begin
  Bitmap.Width := max(Bitmap.Width, ARect.Right);
  Bitmap.Height := max(Bitmap.Height, ARect.Bottom);
  EzRenderTextRect(Bitmap.Canvas, ARect, Text, Style, ButtonState, ButtonFlags, TextFlags, False);
end;

procedure EzRenderTextRect(ACanvas: TCanvas; var ARect: TRect; const Text: WideString;
      Style: TEzButtonStyle; ButtonState: TEzButtonState;
      ButtonFlags: Cardinal = 0; TextFlags: Cardinal = 0; DoubleBuffered: Boolean = true);
var
  PaintRect: TRect;

  procedure DoPaint(TheCanvas: TCanvas);
  var
    TextState: TEzTextState;

  begin
    EzRenderButton(TheCanvas, PaintRect, Style, ButtonState, ButtonFlags, False);

    TextState := tsNormal;
    if ButtonState = bstDisabled then
      TextState := tsDisabled
    else if ButtonState = bstHover then
      TextState := tsHighLight
    else if ButtonState = bstDown then
      inc(ARect.Top, 1);

    InflateRect(PaintRect, -2, -2);
    EzRenderText(TheCanvas, PaintRect, Text, TextState, TextFlags, False);
  end;

begin
  if DoubleBuffered then
    FSharedBitmap.Canvas.Lock;

  try
    if DoubleBuffered then
      with FSharedBitmap do
      begin
        SetRect(PaintRect, 0, 0, ARect.Right - ARect.Left, ARect.Bottom - ARect.Top);
        FSharedBitmap.Width := max(FSharedBitmap.Width, PaintRect.Right);
        FSharedBitmap.Height := max(FSharedBitmap.Height, PaintRect.Bottom);
        FSharedBitmap.Canvas.Font := ACanvas.Font;
        FSharedBitmap.Canvas.Brush := ACanvas.Brush;
        if Style = btsXPButton then FSharedBitmap.Canvas.Pen := ACanvas.Pen;
        DoPaint(FSharedBitmap.Canvas);
        CopyDraw(ACanvas, FSharedBitmap, ARect);
        SetRect(ARect, ARect.Left, ARect.Top, ARect.Left+PaintRect.Right, ARect.Top+PaintRect.Bottom);
      end
    else
    begin
      PaintRect := ARect;
      DoPaint(ACanvas);
      ARect := PaintRect;
    end;
  finally
    if DoubleBuffered then
      FSharedBitmap.Canvas.UnLock;
  end;
end;
{$ENDIF}


initialization
  UseBitmap;

finalization
  ReleaseBitmap;

end.
