unit BasicReg;

{$I Ez.inc}

interface

uses Classes; // , extctrls;

  procedure Register;

implementation

{$R BasicReg.RES}

uses EzCalendarGrid, EzColorComboBox, EzTimebar, EzColumnGrid,
     EzParser;

procedure Register;
begin
  RegisterComponents
  (
    'EzPlan-IT', [TEzCalendarGrid,
                  TEzColorComboBox,
                  TEzTimebar, TEzTimescaleStorage,
                  TEzParser]
  );
end;

end.
