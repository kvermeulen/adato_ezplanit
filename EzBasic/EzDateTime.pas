unit EzDateTime;

{$I Ez.inc}

interface

uses SysUtils, EzMasks, Windows;

const
  TIME_FACTOR=1000000000; // factor used to multiply and divide timevalues
                          // when converting from EzDateTime type to TDateTime

  ONE_DAY: Int64 =TIME_FACTOR;
  ONE_SECOND: Int64 = 11574; // = TIME_FACTOR/24/60/60
  END_OF_TIME=999999999999999; // Some very large date value.
  MaxEzDatetime=END_OF_TIME;

  DayNumNames: array[1..31] of WideString = ('1st','2nd','3rd','4th','5th','6th','7th','8th','9th','10th','11th','12th','13th','14th','15th','16th','17th','18th','19th','20th','21st','22nd','23rd','24th','25th','26th','27th','28th','29th','30th','31th');
  DayMonthNames: array[1..5] of WideString = ('1st','2nd','3rd','4th','last');

  YearLabels: array [0..2] of WideString = ('1998,1999,...','98,99,...','''98, ''99, ...');
  YearFormats: array [0..2] of WideString = ('yyyy','yy','''yy');
  QuarterLabels: array [0..3] of WideString = ('Qtr 1,Qtr 2,...','Qtr 1 (98), Qtr 2 (98), ...','Qtr 1 (1998), Qtr 2 (1998), ...', 'Q1 98, Q2 98, ...');
  QuarterFormats: array [0..3] of WideString = ('"Qtr" !3','"Qtr" !3 (yy)','"Qtr" !3 (yyyy)', 'Q!3 yy');
  MonthLabels: array [0..7] of WideString = ('Januari,Februari,...','Jan,Feb,...','Jan 98,Feb 98,...','Jan 1998,Feb 1998,...', '01/98,02/98,...','1/98,2/98,...','01,02,03,...','1,2,3,...');
  MonthFormats: array [0..7] of WideString = ('mmmm','mmm','mmm yy', 'mmm yyyy','mm/yy','m/yy','mm','m');
  WeekLabels: array [0..10] of WideString = ('31 Jan 98,7 Feb 98,...','31 Jan,07 Feb,...','31 Jan,7 Feb,...','31/01,07/02,...','31/1,7/2,...','Wk 1,Wk 2,...','Week 1,Week 2,...','W1,W2,...','5 dec 2005 [W49],12 dec 2005 [W50],...','5/12/05 [W49],12/12/05 [W50],...', 'W49 (5/12/05),W50 (12/12/05),...');
  WeekFormats: array [0..10] of WideString = ('d mmm yy','dd mmm','d mmm','dd/mm','d/m','"Wk" !1','"Week" !1','W!1','d mmm yyyy [W!1]','d/mm/yy [W!1]','W!1 (d/mm/yy)');
  DayLabels: array [0..5] of WideString = ('M,T,...', 'm,t,...', '1,2,3,...', '01,02,03,...', 'Mon,Tue,...','1 nov 99, 2 nov 99, 3 nov 99');
  DayFormats: array [0..5] of WideString = ('$','~','d','dd','ddd','d mmm yy');
  HourLabels: array [0..4] of WideString = ('10:00,11:00,...', '11,12,13,...', '1h,2h,3h,...', '01h,02h,03h,...', '1 jan 01 15:00, 1 jan 01 16:00, 1 jan 01 17:00, ...');
  HourFormats: array [0..4] of WideString = ('hh:mm', 'hh', 'h''h''', 'hh''h''', 'd mmm yy hh:mm');
  MinuteLabels: array [0..1] of WideString = ('08:01,08:02,...','8:01,8:02,...');
  MinuteFormats: array [0..1] of WideString = ('hh:mm','h:mm');

type
  TScale = (msYear, msQuarter, msMonth, msWeek, msDay, msHour, msMinute, msSecond);
  TEzWeekDays = (wdMonday, wdTuesday, wdWednesday, wdThursday, wdFriday, wdSaturday, wdSunday);
  TEzDateTime = int64;
  TEzDuration = int64;

  function  TickCount(Scale: TScale; ScaleCount: Integer; dtStart, dtStop: TDateTime): integer;
  function  DateTimeToEzDateTime(Value: TDateTime): TEzDateTime;
  function  EzDateTimeToDateTime(Value: TEzDateTime): TDateTime;
  function  EzDateTimeToTime(Value: TEzDateTime): TDateTime;
  function  EzDateTimeToDate(Value: TEzDateTime): TDateTime;
  function  FloorEzDatetime(Value: TEzDatetime): TEzDatetime;
  function  FracEzDatetime(Value: TEzDatetime): TEzDatetime;
  function  DoubleToEzDuration(Value: Double): TEzDuration;
  function  AddYears(D: TDateTime; n: Integer) : TDateTime;
  function  AddMonths(D: TDateTime; n: Integer) : TDateTime;
  function  AddQuarters(D: TDateTime; n: Integer) : TDateTime;

{$IFNDEF EZ_D6}
type
  TRoundToRange = -37..37;

  function  DayOfTheWeek(const AValue: TDateTime): Word;
  function  DaysInAMonth(const AYear, AMonth: Word): Word;
  function  DecodeDateFully(const DateTime: TDateTime; var Year, Month, Day, DOW: Word): Boolean;
  procedure DecodeDateMonthWeek(const AValue: TDateTime;               {ISO 8601x}
    out AYear, AMonth, AWeekOfMonth, ADayOfWeek: Word);
  function  WeekOfTheMonth(const AValue: TDateTime): Word;
  function  RoundTo(const AValue: Double; const ADigit: TRoundToRange): Double;
{$ENDIF}

  function  NthDay(wYear, wMonth, wDay: WORD; var bIsLast: Boolean) : WORD;
  function  EzFormatDateTime(
    const Format: SystemString;
    DateTime: TDateTime;
    MinDate: TDateTime = 0;
    MaxDate: TDateTime = 0): SystemString;
  procedure ReplaceTime(var DateTime: TDateTime; const NewTime: TDateTime);
  function  Scale2Format(Scale: TScale; I: Integer) : string;

const
  // Store default label indexes.
  // These indexes are used to get the default label / labelformat when
  // the user selects a certain scale. For every scale an index is stored
  // for the major and minor scales.
  LabelDefaults: array[msYear..msMinute,0..1] of integer =
    ((0,0){msYear},(1,0){msQuarter},(2,1){msMonth},(0,3){msWeek},
     (4,0){msDay},(0,1){msHour},(1,1){msMinute});

implementation

uses Math
{$IFDEF EZ_D6}
  , DateUtils
{$ENDIF}
{$IFNDEF EZ_D2009}
  , CharInSetFunc
{$ENDIF}
  ;

procedure ReplaceTime(var DateTime: TDateTime; const NewTime: TDateTime);
begin
  DateTime := Trunc(DateTime);
  if DateTime >= 0 then
    DateTime := DateTime + Abs(Frac(NewTime))
  else
    DateTime := DateTime - Abs(Frac(NewTime));
end;

function Scale2Format(Scale: TScale; I: Integer) : string;
begin
  case Scale of
    msYear: Result := YearFormats[I];
    msQuarter: Result := QuarterFormats[I];
    msMonth: Result := MonthFormats[I];
    msWeek: Result := WeekFormats[I];
    msDay: Result := DayFormats[I];
    msHour: Result := HourFormats[I];
    msMinute: Result := MinuteFormats[I];
  end;
end;

function NthDay(wYear, wMonth, wDay: WORD; var bIsLast: Boolean) : WORD;
begin
	Result := ((wDay - 1) div 7) + 1;
	if (Result >= 4) then
	    bIsLast := (MonthDays[IsLeapYear(wYear)][wMonth] - wDay) < 7
	else
		bIsLast := False;
end;

function AddYears(D: TDateTime; n:Integer) : TDateTime;
var
  DayTable: PDayTable;
  Year, Month, Day: WORD;

begin
  DecodeDate(D, Year, Month, Day);
  Year := Year + n;
  DayTable := @MonthDays[IsLeapYear(Year)];
  if Day > DayTable^[Month] then
    Day := DayTable^[Month];
  Result := EncodeDate(Year, Month, Day);
  ReplaceTime(Result, D);
end;

function AddMonths(D: TDateTime; n: Integer) : TDateTime;
begin
  Result := IncMonth(D, n);
end;

function AddQuarters(D: TDateTime; n: Integer) : TDateTime;
var
  DayTable: PDayTable;
  Year, Month, Day: WORD;

begin
  DecodeDate(D, Year, Month, Day);
  Month := Month + n*3;
  Year := Year + (Month-1) div 12;
  Month := Month mod 12;
  if Month=0 then Month:=12;
  DayTable := @MonthDays[IsLeapYear(Year)];
  if Day > DayTable^[Month] then
    Day := DayTable^[Month];
  Result := EncodeDate(Year, Month, Day);
  ReplaceTime(Result, D);
end;

function TickCount(Scale: TScale; ScaleCount: Integer; dtStart, dtStop: TDateTime): integer;
var
  Dummy, Month1, Year1, Month2, Year2: WORD;

begin
  Result := 0;
  case Scale of
    msYear:
    begin
      DecodeDate(dtStart, Year1, Month1, Dummy);
      DecodeDate(dtStop, Year2, Month2, Dummy);
      Result := Year2 - Year1;
    end;
    msQuarter:
      Result := TickCount(msMonth, 1, dtStart, dtStop) div 3;

    msMonth:
    begin
      DecodeDate(dtStart, Year1, Month1, Dummy);
      DecodeDate(dtStop, Year2, Month2, Dummy);
      Result := ((Year2 - Year1)-1)*12+12 - Month1 + Month2;
    end;
    msWeek:
      Result := (Trunc(dtStop) - Trunc(dtStart)) div 7;
    msDay:
      Result := Trunc(dtStop) - Trunc(dtStart);
    msHour:
      Result := Round((dtStop-dtStart)*24);
    msMinute:
      Result := Round((dtStop-dtStart)*24*60);
    msSecond:
      Result := Round((dtStop-dtStart)*24*60*60);
  end;
  Result := Result div ScaleCount;
end;

{$IFNDEF EZ_D6}
const
  DaysPerWeek = 7;

  CDayMap: array [1..7] of Word = (7, 1, 2, 3, 4, 5, 6);
  DayMonday = 1;
  DayTuesday = 2;
  DayWednesday = 3;
  DayThursday = 4;
  DayFriday = 5;
  DaySaturday = 6;
  DaySunday = 7;

  HoursPerDay   = 24;
  MinsPerHour   = 60;
  SecsPerMin    = 60;
  MinsPerDay    = HoursPerDay * MinsPerHour;
  
//var
  { average over a 4 year span }
//  ApproxDaysPerMonth: Double = 30.4375;
//  ApproxDaysPerYear: Double  = 365.25;

function DayOfTheWeek(const AValue: TDateTime): Word;
begin
  Result := (DateTimeToTimeStamp(AValue).Date - 1) mod 7 + 1;
end;

function DaysInAMonth(const AYear, AMonth: Word): Word;
begin
  Result := MonthDays[(AMonth = 2) and IsLeapYear(AYear), AMonth];
end;

procedure DecodeDateMonthWeek(const AValue: TDateTime;
  out AYear, AMonth, AWeekOfMonth, ADayOfWeek: Word);
var
  LDay, LDaysInMonth: Word;
  LDayOfMonth: Integer;
  LStart: TDateTime;
  LStartDayOfWeek, LEndOfMonthDayOfWeek: Word;
begin
  DecodeDateFully(AValue, AYear, AMonth, LDay, ADayOfWeek);
  ADayOfWeek := CDayMap[ADayOfWeek];
  LStart := EncodeDate(AYear, AMonth, 1);
  LStartDayOfWeek := DayOfTheWeek(LStart);
  LDayOfMonth := LDay;
  if LStartDayOfWeek in [DayFriday, DaySaturday, DaySunday] then
    Dec(LDayOfMonth, 8 - LStartDayOfWeek)
  else
    Inc(LDayOfMonth, LStartDayOfWeek - 1);
  if LDayOfMonth <= 0 then
    DecodeDateMonthWeek(LStart - 1, AYear, AMonth, AWeekOfMonth, LDay)
  else
  begin
    AWeekOfMonth := LDayOfMonth div 7;
    if LDayOfMonth mod 7 <> 0 then
      Inc(AWeekOfMonth);
    LDaysInMonth := DaysInAMonth(AYear, AMonth);
    LEndOfMonthDayOfWeek := DayOfTheWeek(EncodeDate(AYear, AMonth, LDaysInMonth));
    if (LEndOfMonthDayOfWeek in [DayMonday, DayTuesday, DayWednesday]) and
       (LDaysInMonth - LDay < LEndOfMonthDayOfWeek) then
    begin
      Inc(AMonth);
      if AMonth = 13 then
      begin
        AMonth := 1;
        Inc(AYear);
      end;
      AWeekOfMonth := 1;
    end;
  end;
end;

function DecodeDateFully(const DateTime: TDateTime; var Year, Month, Day, DOW: Word): Boolean;
const
  D1 = 365;
  D4 = D1 * 4 + 1;
  D100 = D4 * 25 - 1;
  D400 = D100 * 4 + 1;
var
  Y, M, D, I: Word;
  T: Integer;
  DayTable: PDayTable;

  procedure DivMod(Dividend: Integer; Divisor: Word;
    var Result, Remainder: Word);
  asm
        PUSH    EBX
        MOV     EBX,EDX
        MOV     EDX,EAX
        SHR     EDX,16
        DIV     BX
        MOV     EBX,Remainder
        MOV     [ECX],AX
        MOV     [EBX],DX
        POP     EBX
  end;

begin
  T := DateTimeToTimeStamp(DateTime).Date;
  if T <= 0 then
  begin
    Year := 0;
    Month := 0;
    Day := 0;
    DOW := 0;
    Result := False;
  end else
  begin
    DOW := T mod 7 + 1;
    Dec(T);
    Y := 1;
    while T >= D400 do
    begin
      Dec(T, D400);
      Inc(Y, 400);
    end;
    DivMod(T, D100, I, D);
    if I = 4 then
    begin
      Dec(I);
      Inc(D, D100);
    end;
    Inc(Y, I * 100);
    DivMod(D, D4, I, D);
    Inc(Y, I * 4);
    DivMod(D, D1, I, D);
    if I = 4 then
    begin
      Dec(I);
      Inc(D, D1);
    end;
    Inc(Y, I);
    Result := IsLeapYear(Y);
    DayTable := @MonthDays[Result];
    M := 1;
    while True do
    begin
      I := DayTable^[M];
      if D < I then Break;
      Dec(D, I);
      Inc(M);
    end;
    Year := Y;
    Month := M;
    Day := D + 1;
  end;
end;

procedure DecodeDateWeek(const AValue: TDateTime; out AYear, AWeekOfYear,
  ADayOfWeek: Word);
var
  LDayOfYear: Integer;
  LMonth, LDay: Word;
  LStart: TDateTime;
  LStartDayOfWeek, LEndDayOfWeek: Word;
  LLeap: Boolean;
begin
  LLeap := DecodeDateFully(AValue, AYear, LMonth, LDay, ADayOfWeek);
  ADayOfWeek := CDayMap[ADayOfWeek];
  LStart := EncodeDate(AYear, 1, 1);
  LDayOfYear := Trunc(AValue - LStart + 1);

  LStartDayOfWeek := DayOfTheWeek(LStart);
  if LStartDayOfWeek in [DayFriday, DaySaturday, DaySunday] then
    Dec(LDayOfYear, 8 - LStartDayOfWeek)
  else
    Inc(LDayOfYear, LStartDayOfWeek - 1);
  if LDayOfYear <= 0 then
    DecodeDateWeek(LStart - 1, AYear, AWeekOfYear, LDay)
  else
  begin
    AWeekOfYear := LDayOfYear div 7;
    if LDayOfYear mod 7 <> 0 then
      Inc(AWeekOfYear);
    if AWeekOfYear > 52 then
    begin
      LEndDayOfWeek := LStartDayOfWeek;
      if LLeap then
      begin
        if LEndDayOfWeek = DaySunday then
          LEndDayOfWeek := DayMonday
        else
          Inc(LEndDayOfWeek);
      end;
      if LEndDayOfWeek in [DayMonday, DayTuesday, DayWednesday] then
      begin
        Inc(AYear);
        AWeekOfYear := 1;
      end;
    end;
  end;
end;

function WeekOfTheMonth(const AValue: TDateTime): Word;
var
  LYear, LMonth, LDayOfWeek: Word;
begin
  DecodeDateMonthWeek(AValue, LYear, LMonth, Result, LDayOfWeek);
end;

function RoundTo(const AValue: Double; const ADigit: TRoundToRange): Double;
var
  LFactor: Double;
begin
  LFactor := IntPower(10, ADigit);
  Result := Round(AValue / LFactor) * LFactor;
end;

{$ENDIF}

function CharNext(const Str: SystemPChar): SystemPChar;
begin
{$IFDEF EZ_D2005}
  Result := Windows.CharNextW(Str);
{$ELSE}
  Result := Windows.CharNext(Str);
{$ENDIF}
end;

function StrCharLength(const Str: SystemPChar): Integer;
begin
  if SysLocale.FarEast then
    Result := Integer(CharNext(Str)) - Integer(Str)
  else
    Result := 1;
end;

function StrNextChar(const Str: SystemPChar): SystemPChar;
begin
  Result := CharNext(Str);
end;

function DateTimeToEzDateTime(Value: TDateTime): TEzDateTime;
begin
  Result := Trunc(Value*TIME_FACTOR);
//  Result := Round(Value*TIME_FACTOR);
end;

function EzDateTimeToDateTime(Value: TEzDateTime): TDateTime;
begin
  Result := Value/TIME_FACTOR;
end;

function EzDateTimeToTime(Value: TEzDateTime): TDateTime;
begin
  Result := Frac(EzDateTimeToDateTime(Value));
end;

function EzDateTimeToDate(Value: TEzDateTime): TDateTime;
begin
  Result := Trunc(EzDateTimeToDateTime(Value));
//  Result := Round(EzDateTimeToDateTime(Value));
end;

function FloorEzDatetime(Value: TEzDatetime): TEzDatetime;
begin
  Result := Value - (Value mod TIME_FACTOR);
end;

function FracEzDatetime(Value: TEzDatetime): TEzDatetime;
begin
//  Result := Value - FloorEzDatetime(Value);
  Result := Value mod TIME_FACTOR;
end;

function DoubleToEzDuration(Value: Double): TEzDuration;
begin
  Result := Trunc(Value*TIME_FACTOR);
//  Result := Round(Value*TIME_FACTOR);
end;

function EzFormatDateTime(const Format: SystemString; DateTime: TDateTime; MinDate: TDateTime = 0; MaxDate: TDateTime = 0): SystemString;
var
  BufPos: Integer;
  Buffer: array[0..255] of SystemChar;

  procedure AppendChars(P: SystemPChar; Count: Integer);
  var
    N: Integer;
  begin
    N := SizeOf(Buffer) - BufPos;
    if N > Count then N := Count;
    if N <> 0 then Move(P[0], Buffer[BufPos], N * SizeOf(SystemChar));
    Inc(BufPos, N);
  end;

  procedure AppendString(const S: string);
  begin
    AppendChars(Pointer(S), Length(S));
  end;

  procedure AppendNumber(Number, Digits: Integer);
  var
    s: String;

  begin
    s := SysUtils.Format('%.*d', [Digits, Number]);
    AppendChars(SystemPChar(SystemString(s)), Length(s));
  end;

  procedure AppendFormat(Format: SystemPChar);
  var
    Starter, Token: SystemChar;
    P: SystemPChar;
    Count: Integer;
    Year, Month, Day: Word;
    DOW, AWeekOfYear, AWeekOfMonth: WORD;
    FullyDecoded, WeekDecoded, WeekOfMonthDecoded: boolean;

    procedure GetCount;
    var
      P: SystemPChar;
    begin
      P := Format;
      while Format^ = Starter do Inc(Format);
      Count := Format - P + 1;
    end;

    procedure GetDoubleCharCount;
    var
      P: SystemPChar;
    begin
      Starter := Format^;
      P := Format;
      while Format^ = Starter do Inc(Format);
      Count := Format - P;
    end;

    procedure AppendDuration(Number: Integer);
    begin
      GetDoubleCharCount;
      AppendNumber(Number, Count-1);
    end;

    procedure DecodeFully;
    begin
      if FullyDecoded then Exit;
      DecodeDateFully(DateTime, Year, Month, Day, DOW);
      FullyDecoded :=  true;
    end;

    procedure DecodeWeek;
    var
      d1, d2: Word;

    begin
      if WeekDecoded then Exit;
      DecodeDateWeek(DateTime, d1, AWeekOfYear, d2);
      WeekDecoded :=  true;
    end;

    procedure DecodeWeekOfMonth;
    begin
      if WeekOfMonthDecoded then Exit;
      AWeekOfMonth := WeekOfTheMonth(DateTime);
      WeekOfMonthDecoded :=  true;
    end;

  begin
    if (Format <> nil) then
    begin
      FullyDecoded := False;
      WeekDecoded := False;
      WeekOfMonthDecoded := False;

      while Format^ <> #0 do
      begin
        Starter := Format^;
        if CharInset(Starter, LeadBytes) then
        begin
          AppendChars(Format, StrCharLength(Format));
          Format := StrNextChar(Format);
          Continue;
        end;
        Format := StrNextChar(Format);
        Token := Starter;
        if CharInSet(Token, ['a'..'z']) then Dec(Token, 32);

        case Token of
          '$': // First letter of day uppercase
          begin
            DecodeFully;
            Token := SystemChar({$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[DOW][1]);
            if CharInSet(Token, ['a'..'z']) then Dec(Token, 32);
            AppendChars(@Token, 1);
          end;

          '~': // First letter of day lowercase
          begin
            DecodeFully;
            Token := SystemChar({$IFDEF XE3}FormatSettings.{$ENDIF}LongDayNames[DOW][1]);
            if CharInSet(Token, ['A'..'Z']) then Inc(Token, 32);
            AppendChars(@Token, 1);
          end;

          'X':
          begin
            DecodeWeek;
            GetCount;
            AppendNumber(AWeekOfYear, Count);
          end;

          '!':
          begin
            P := Format;
            case P^ of
              '1': // Week (of year) value
              begin
                DecodeWeek;
                GetDoubleCharCount;
                AppendNumber(AWeekOfYear, Count);
              end;

              '2': // Week of month value
              begin
                DecodeWeekOfMonth;
                GetDoubleCharCount;
                AppendNumber(AWeekOfMonth, Count);
              end;

              '3': // Quarter
              begin
                DecodeFully;
                GetDoubleCharCount;
                AppendNumber(Trunc((Month-1)/3+1), Count);
              end;

            else
              AppendChars(@Starter, 1);
            end;
          end;

          '<':
          begin
            P := Format;
            case P^ of
              '1': AppendDuration(TickCount(msYear, 1, MinDate, DateTime));
              '2': AppendDuration(TickCount(msMonth, 1, MinDate, DateTime));
              '3': AppendDuration(TickCount(msWeek, 1, MinDate, DateTime));
              '4': AppendDuration(TickCount(msDay, 1, MinDate, DateTime));
              '5': AppendDuration(TickCount(msHour, 1, MinDate, DateTime));
              '6': AppendDuration(TickCount(msMinute, 1, MinDate, DateTime));
              '7': AppendDuration(TickCount(msSecond, 1, MinDate, DateTime));
            else
              AppendChars(@Starter, 1);
            end;
          end;

          '>':
          begin
            P := Format;
            case P^ of
              '1': AppendDuration(TickCount(msYear, 1, MaxDate, DateTime));
              '2': AppendDuration(TickCount(msMonth, 1, MaxDate, DateTime));
              '3': AppendDuration(TickCount(msWeek, 1, MaxDate, DateTime));
              '4': AppendDuration(TickCount(msDay, 1, MaxDate, DateTime));
              '5': AppendDuration(TickCount(msHour, 1, MaxDate, DateTime));
              '6': AppendDuration(TickCount(msMinute, 1, MaxDate, DateTime));
              '7': AppendDuration(TickCount(msSecond, 1, MaxDate, DateTime));
            else
              AppendChars(@Starter, 1);
            end;
          end;

        else
          AppendChars(@Starter, 1);
        end;
      end;
    end;
  end;

begin
  Result := FormatDateTime(Format, DateTime);

  BufPos := 0;
  if Result <> '' then
  begin
    AppendFormat(Pointer(Result));
    SetString(Result, Buffer, BufPos);
  end;

end;

end.

