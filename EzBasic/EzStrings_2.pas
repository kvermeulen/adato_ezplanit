unit EzStrings_2;

interface

resourcestring
  SConfirmDeleteScale = 'Are you sure this scale must be deleted?';
  SYouCannotDeleteFinalScale = 'You may not delete the final scale.';

  SSyntaxError = 'Syntax error.';
  SFunctionError = 'Unknown function or variable';
  SBraceError = 'Unmached "(" in function: ';
  SInvalidDegree = 'Invalid degree %s';
  SInvalidString = 'Invalid string';
  SCannotDelete = 'Cannot delete a variable with formula dependencies!';
  SWrongParamCount = 'Incorrect parameter count for function!';
  SCircularDependency = 'Circular dependency in formula';

implementation

end.
