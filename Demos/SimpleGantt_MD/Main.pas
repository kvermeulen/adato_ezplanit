unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, EzDataset, EzGantt, EzTimeBar, Grids, DBGrids, EzDBGrid,
  ExtCtrls, StdCtrls, ImgList;

type
  TForm1 = class(TForm)
    qrTasks: TADODataSet;
    EzDataset1: TEzDataset;
    dsTasks: TDataSource;
    EzDatasource: TDataSource;
    EzDataset1idTask: TAutoIncField;
    EzDataset1taidParent: TIntegerField;
    EzDataset1taDesc: TWideStringField;
    Panel2: TPanel;
    EzTimebar1: TEzTimebar;
    EzGanttChart1: TEzGanttChart;
    Panel3: TPanel;
    Button1: TButton;
    qrEmployees: TADODataSet;
    dsEmployees: TDataSource;
    ilButtonImages: TImageList;
    scGridSchemes: TEzTreeGridSchemes;
    imgEndPoints: TImageList;
    eplEndPoints: TEzEndPointList;
    EzGrid: TEzDBTreeGrid;
    tssGlobalScales: TEzTimescaleStorage;
    Splitter1: TSplitter;
    EzDataset1StartDate: TDateTimeField;
    EzDataset1StopDate: TDateTimeField;
    btnAddTask: TButton;
    procedure Button1Click(Sender: TObject);
    procedure EzDataset1AfterScroll(DataSet: TDataSet);
    procedure btnAddTaskClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure ScrollToTask;
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  EzDataset1.Open;
end;

procedure TForm1.EzDataset1AfterScroll(DataSet: TDataSet);
begin
  // Scroll current task into view
  ScrollToTask;
end;

//
// This procedure scrolls the Gantt chart in such a way that the taskbar,
// drawn for the current record, will be visible. This method is called
// automatically when AutoTaskScroll is set to True.
//
procedure TForm1.ScrollToTask;
var
  Start, Stop, EndDate, Current: TDateTime;
begin
  if not EzDataset1.FieldByName('StartDate').IsNull then
    Start := EzDataset1.FieldByName('StartDate').AsDateTime else
    Start := 0;

  if not EzDataset1.FieldByName('StopDate').IsNull then
    Stop := EzDataset1.FieldByName('StopDate').AsDateTime else
    Stop := 0;

  with EzGanttChart1.Timebar do
  begin
    Current := Date;
    EndDate := X2DateTime(Width);

    if (Start <> 0) and (Start < Current) then
      Date := Start
    else if (Stop <> 0) and (Stop > EndDate) then
    begin
      if Start-Current < Stop-EndDate then
        Date := Start else
        Date := AddTicks(MajorScale, MajorCount, Date + Stop-EndDate, 1);
    end;
  end;
end;

procedure TForm1.btnAddTaskClick(Sender: TObject);
begin
  if EzDataset1.Level=0 then
    EzDataset1.AddChild
  else
    EzDataset1.Add;
  ActiveControl := EzGrid;
end;

end.
