unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, EzDataset, EzGantt, EzTimeBar, Grids, DBGrids, EzDBGrid,
  ExtCtrls, StdCtrls, ImgList, EzVirtualDataset,
  Generics.Collections;

type
  TTaskItem = class;

  TForm1 = class(TForm)
    EzDataset1: TEzDataset;
    EzDatasource: TDataSource;
    Panel2: TPanel;
    EzTimebar1: TEzTimebar;
    EzGanttChart1: TEzGanttChart;
    Panel3: TPanel;
    Button1: TButton;
    ilButtonImages: TImageList;
    scGridSchemes: TEzTreeGridSchemes;
    imgEndPoints: TImageList;
    eplEndPoints: TEzEndPointList;
    EzGrid: TEzDBTreeGrid;
    tssGlobalScales: TEzTimescaleStorage;
    Splitter1: TSplitter;
    btnAddTask: TButton;
    EzArrayDataset1: TEzArrayDataset;
    EzArrayDataset1ParentID: TVariantField;
    EzArrayDataset1ID: TVariantField;
    EzArrayDataset1Description: TStringField;
    EzArrayDataset1Start: TDateTimeField;
    EzArrayDataset1Stop: TDateTimeField;
    DataSource1: TDataSource;
    EzDataset1ID: TVariantField;
    EzDataset1ParentID: TVariantField;
    EzDataset1Description: TStringField;
    EzDataset1Start: TDateTimeField;
    EzDataset1Stop: TDateTimeField;
    procedure Button1Click(Sender: TObject);
    procedure EzDataset1AfterScroll(DataSet: TDataSet);
    procedure btnAddTaskClick(Sender: TObject);
    procedure EzArrayDataset1GetFieldValue(Sender: TCustomEzArrayDataset; Field:
        TField; Index: Integer; var Value: Variant);
    procedure EzArrayDataset1GetRecordCount(Sender: TCustomEzArrayDataset; var
        Count: Integer);
    procedure EzArrayDataset1Locate(Sender: TCustomEzArrayDataset; const KeyFields:
        string; const KeyValues: Variant; Options: TLocateOptions; var Index:
        Integer);
  private
    { Private declarations }
  protected
    FTasks: TList<TTaskItem>;
    procedure CreateTaskList;
  public
    procedure ScrollToTask;
    { Public declarations }
  end;

  TTaskItem = class
    ID: Integer;
    ParentID: Variant;
    Description: string;
    Start: TDateTime;
    Stop: TDateTime;
    Progress: Double;
    IsMileStone: Boolean;
    Predecessor: TTaskItem;
  end;

var
  Form1: TForm1;

implementation

uses
  Variants;

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  CreateTaskList;

  EzDataset1.Open;
end;

procedure TForm1.EzDataset1AfterScroll(DataSet: TDataSet);
begin
  // Scroll current task into view
  ScrollToTask;
end;

//
// This procedure scrolls the Gantt chart in such a way that the taskbar,
// drawn for the current record, will be visible. This method is called
// automatically when AutoTaskScroll is set to True.
//
procedure TForm1.ScrollToTask;
var
  Start, Stop, EndDate, Current: TDateTime;
begin
  if not EzDataset1.FieldByName('Start').IsNull then
    Start := EzDataset1.FieldByName('Start').AsDateTime else
    Start := 0;

  if not EzDataset1.FieldByName('Stop').IsNull then
    Stop := EzDataset1.FieldByName('Stop').AsDateTime else
    Stop := 0;

  with EzGanttChart1.Timebar do
  begin
    Current := Date;
    EndDate := X2DateTime(Width);

    if (Start <> 0) and (Start < Current) then
      Date := Start
    else if (Stop <> 0) and (Stop > EndDate) then
    begin
      if Start-Current < Stop-EndDate then
        Date := Start else
        Date := AddTicks(MajorScale, MajorCount, Date + Stop-EndDate, 1);
    end;
  end;
end;

procedure TForm1.btnAddTaskClick(Sender: TObject);
begin
  if EzDataset1.Level=0 then
    EzDataset1.AddChild
  else
    EzDataset1.Add;
  ActiveControl := EzGrid;
end;

procedure TForm1.CreateTaskList;
var
  i: Integer;
  n: Integer;
  Parent: TTaskItem;
  prevTask: TTaskItem;
  Task: TTaskItem;
begin
  FTasks := TList<TTaskItem>.Create;

  for i := 0 to 5 do
  begin
    Parent := TTaskItem.Create;
    Parent.ID := i * 5; // Unique key for parent
    Parent.Description := 'Parent ' + IntToStr(Parent.ID);
    Parent.ParentID := Null;
    Parent.Start := Trunc(Now);
    Parent.Stop := Parent.Start + 10;

    FTasks.Add(Parent);

    prevTask := nil;

    for n := 1 to 4 do
    begin
      Task := TTaskItem.Create;
      Task.ID := Parent.ID + n; // Unique key for task
      Task.Description := 'Task ' + IntToStr(Task.ID);
      Task.Progress := 0.5;
      Task.ParentID := Parent.ID;
      Task.Start := Trunc(Now) + Task.ID;

      if n = 4 then
        Task.IsMileStone := True else
        Task.Stop := Task.Start + 10;

      if prevTask <> nil then
        //
        // Previous task is a predecessor for this task
        //
        Task.Predecessor := prevTask;

      FTasks.Add(Task);

      prevTask := Task;
    end;
  end;

end;

procedure TForm1.EzArrayDataset1GetFieldValue(Sender: TCustomEzArrayDataset;
    Field: TField; Index: Integer; var Value: Variant);
var
  fieldName: string;
  taskItem: TTaskItem;
begin
  taskItem := FTasks[Index];

  fieldName := Field.FieldName;
  if fieldName = 'ID' then
    Value := taskItem.ID

  else if fieldName = 'Description' then
    Value := taskItem.Description
  else if fieldName = 'ParentID' then
    Value := taskItem.ParentID
  else if fieldName = 'Progress' then
    Value := taskItem.Progress
  else if fieldName = 'Start' then
    Value := taskItem.Start
  else if fieldName = 'Stop' then
    Value := taskItem.Stop;
end;

procedure TForm1.EzArrayDataset1GetRecordCount(Sender: TCustomEzArrayDataset;
    var Count: Integer);
begin
  Count := FTasks.Count;
end;

procedure TForm1.EzArrayDataset1Locate(
  Sender: TCustomEzArrayDataset;
  const KeyFields: string;
  const KeyValues: Variant;
  Options: TLocateOptions;
  var Index: Integer);

begin
  // Return Index of record indicated by KeyValues (=ID of task)
  Index := Integer(KeyValues);
end;

end.
