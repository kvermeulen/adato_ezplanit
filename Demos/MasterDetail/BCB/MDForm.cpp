//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MDForm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "EzDataset"
#pragma link "EzDBGrid"
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  EzDataset1->Active = !EzDataset1->Active;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
  EzDataset1->ExpandAll();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
  EzDataset1->CollapseAll();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Button5Click(TObject *Sender)
{
  EzDataset1->Insert();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
  EzDataset1->Append();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{
  EzDataset1->Add();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
  EzDataset1->AddChild();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::cbSortChange(TObject *Sender)
{
  EzDataset1->ActiveCursorIndex = cbSort->ItemIndex;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit1Change(TObject *Sender)
{
  EzDBGrid1->FixedCols = UpDown1->Position;  
}
//---------------------------------------------------------------------------

