//---------------------------------------------------------------------------

#ifndef MDFormH
#define MDFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "EzDataset.hpp"
#include "EzDBGrid.hpp"
#include <ComCtrls.hpp>
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <DBTables.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ImgList.hpp>
#include <Mask.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl1;
        TTabSheet *TabSheet2;
        TEzDBGrid *EzDBGrid1;
        TMemo *Memo1;
        TPanel *Panel3;
        TLabel *Label12;
        TLabel *Label5;
        TComboBox *cbSort;
        TEdit *Edit1;
        TUpDown *UpDown1;
        TButton *Button5;
        TButton *Button6;
        TButton *Button7;
        TButton *Button8;
        TPanel *Panel6;
        TLabel *Label11;
        TLabel *Label13;
        TLabel *Label4;
        TDBEdit *DBEdit8;
        TPanel *Panel2;
        TDBEdit *DBEdit9;
        TDBEdit *DBEdit1;
        TPanel *Panel1;
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        TDatabase *Database1;
        TTable *tblCustomers;
        TFloatField *tblCustomersCustNo;
        TStringField *tblCustomersCompany;
        TStringField *tblCustomersAddr1;
        TStringField *tblCustomersAddr2;
        TStringField *tblCustomersCity;
        TTable *tblOrders;
        TFloatField *tblOrdersOrderNo;
        TFloatField *tblOrdersCustNo;
        TStringField *tblOrdersEmpName;
        TDateTimeField *tblOrdersSaleDate;
        TStringField *tblOrdersShipToCity;
        TStringField *tblOrdersShipToPhone;
        TTable *tblOrderItems;
        TDataSource *dsCustomers;
        TDataSource *dsOrders;
        TDataSource *dsOrderItems;
        TEzDataset *EzDataset1;
        TStringField *EzDataset1Company;
        TStringField *EzDataset1ShipToPhone;
        TFloatField *EzDataset1CustNo;
        TFloatField *EzDataset1OrderNo;
        TAutoIncField *EzDataset1ItemNo;
        TStringField *EzDataset1City;
        TDateTimeField *EzDataset1SaleDate;
        TIntegerField *EzDataset1Qty;
        TStringField *EzDataset1OrderQty;
        TStringField *EzDataset1ShipToCity;
        TDataSource *dsMasterDetail;
        TImageList *ilButtonImages;
        TEzTreeGridSchemes *scGridSchemes;
        void __fastcall Button1Click(TObject *Sender);
  void __fastcall Button2Click(TObject *Sender);
  void __fastcall Button3Click(TObject *Sender);
  void __fastcall Button5Click(TObject *Sender);
  void __fastcall Button6Click(TObject *Sender);
  void __fastcall Button7Click(TObject *Sender);
  void __fastcall Button8Click(TObject *Sender);
  void __fastcall cbSortChange(TObject *Sender);
  void __fastcall Edit1Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
