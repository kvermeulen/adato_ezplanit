object Form1: TForm1
  Left = 192
  Top = 114
  Width = 870
  Height = 640
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 33
    Width = 862
    Height = 573
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet2: TTabSheet
      Caption = 'TEzDBTreeGrid'
      ImageIndex = 1
      object EzDBGrid1: TEzDBGrid
        Left = 0
        Top = 65
        Width = 460
        Height = 480
        Align = alClient
        BorderStyle = bsNone
        ColumnStyle = btsXPButton
        DataSource = dsMasterDetail
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ScrollBars = ssHorizontal
        TabOrder = 0
        SchemeCollection = scGridSchemes
        TitlebarHeight = 20
        Columns = <
          item
            Expanded = False
            FieldName = 'CustNo'
            Title.Caption = 'CustNo/OrderNo'
            Title.VerticalAlignment = tvaTop
            Width = 92
            Visible = True
            SchemeList = (
              0
              1
              2)
          end
          item
            Expanded = False
            FieldName = 'Company'
            Title.VerticalAlignment = tvaTop
            Width = 136
            Visible = True
            SchemeList = (
              3
              4
              6)
          end
          item
            Expanded = False
            FieldName = 'City'
            Title.VerticalAlignment = tvaTop
            Width = 68
            Visible = True
            SchemeList = (
              3
              5
              6)
          end
          item
            Expanded = False
            FieldName = 'OrderQty'
            Title.VerticalAlignment = tvaTop
            Width = 54
            Visible = True
            SchemeList = (
              3
              5
              6)
          end>
      end
      object Memo1: TMemo
        Left = 635
        Top = 65
        Width = 219
        Height = 480
        Align = alRight
        BorderStyle = bsNone
        Color = clInfoBk
        Lines.Strings = (
          'This page displays the TEzDbTreeGrid. This '
          'grid is connected to a TEzDataset object, '
          'Kever-IT'#39's implementation of an hierarchical '
          'dataset. '
          ''
          'Data is organized in the following manner:'
          'Level 0:   Customers'
          'Level 1:         Orders'
          'Level 2:               OrderItems'
          ''
          'You can expand and collapse records by '
          'clicking on the arrows displayed on the left '
          'side of the row.'
          ''
          'Notice the Qty column. This column contains '
          'a calculated value equal to the sum of the '
          'Qty '
          'of the OrderItems record.'
          ''
          'TEzDataset has a dataset designer which '
          'helps you with it'#39's configuration. The '
          'designer '
          'opens when you double '
          'click the TEzDataset component.')
        TabOrder = 1
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 854
        Height = 65
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label12: TLabel
          Left = 16
          Top = 12
          Width = 19
          Height = 13
          Caption = 'Sort'
        end
        object Label5: TLabel
          Left = 248
          Top = 12
          Width = 67
          Height = 13
          Caption = 'Fixed columns'
        end
        object cbSort: TComboBox
          Left = 40
          Top = 8
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbSortChange
          Items.Strings = (
            '<none>'
            'Alphabetical'
            'Customer/SaleDate'
            'City')
        end
        object Edit1: TEdit
          Left = 320
          Top = 8
          Width = 33
          Height = 21
          TabOrder = 1
          Text = '0'
          OnChange = Edit1Change
        end
        object UpDown1: TUpDown
          Left = 353
          Top = 8
          Width = 16
          Height = 21
          Associate = Edit1
          Min = 0
          Position = 0
          TabOrder = 2
          Wrap = False
        end
        object Button5: TButton
          Left = 40
          Top = 32
          Width = 75
          Height = 25
          Caption = 'Insert'
          TabOrder = 3
          OnClick = Button5Click
        end
        object Button6: TButton
          Left = 120
          Top = 32
          Width = 75
          Height = 25
          Caption = 'Append'
          TabOrder = 4
          OnClick = Button6Click
        end
        object Button7: TButton
          Left = 200
          Top = 32
          Width = 75
          Height = 25
          Caption = 'Add'
          TabOrder = 5
          OnClick = Button7Click
        end
        object Button8: TButton
          Left = 280
          Top = 32
          Width = 75
          Height = 25
          Caption = 'Add child'
          TabOrder = 6
          OnClick = Button8Click
        end
      end
      object Panel6: TPanel
        Left = 460
        Top = 65
        Width = 175
        Height = 480
        Align = alRight
        TabOrder = 3
        object Label11: TLabel
          Left = 8
          Top = 128
          Width = 51
          Height = 13
          Caption = 'ShipToCity'
          FocusControl = DBEdit8
        end
        object Label13: TLabel
          Left = 8
          Top = 80
          Width = 44
          Height = 13
          Caption = 'SaleDate'
          FocusControl = DBEdit9
        end
        object Label4: TLabel
          Left = 8
          Top = 40
          Width = 53
          Height = 13
          Caption = 'Description'
          FocusControl = DBEdit1
        end
        object DBEdit8: TDBEdit
          Left = 8
          Top = 144
          Width = 94
          Height = 21
          DataField = 'ShipToCity'
          DataSource = dsMasterDetail
          TabOrder = 0
        end
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 173
          Height = 24
          Align = alTop
          Caption = 'Detail fields'
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 8
          Top = 96
          Width = 94
          Height = 21
          DataField = 'SaleDate'
          DataSource = dsMasterDetail
          TabOrder = 2
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 56
          Width = 161
          Height = 21
          DataField = 'Company'
          DataSource = dsMasterDetail
          TabOrder = 3
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 33
    Align = alTop
    TabOrder = 1
    object Button1: TButton
      Left = 24
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Open'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 112
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Expand all'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 200
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Collapse all'
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object Database1: TDatabase
    Connected = True
    DatabaseName = 'MasterDetail'
    DriverName = 'STANDARD'
    Params.Strings = (
      'PATH=..\mddb'
      'DEFAULT DRIVER=PARADOX'
      'ENABLE BCD=FALSE')
    SessionName = 'Default'
    Left = 16
    Top = 184
  end
  object tblCustomers: TTable
    DatabaseName = 'MasterDetail'
    IndexFieldNames = 'CustNo'
    TableName = 'customer.db'
    Left = 64
    Top = 184
    object tblCustomersCustNo: TFloatField
      FieldName = 'CustNo'
      Required = True
    end
    object tblCustomersCompany: TStringField
      FieldName = 'Company'
      Size = 30
    end
    object tblCustomersAddr1: TStringField
      FieldName = 'Addr1'
      Size = 30
    end
    object tblCustomersAddr2: TStringField
      FieldName = 'Addr2'
      Size = 30
    end
    object tblCustomersCity: TStringField
      FieldName = 'City'
      Size = 15
    end
  end
  object tblOrders: TTable
    DatabaseName = 'MasterDetail'
    IndexFieldNames = 'OrderNo'
    TableName = 'orders.db'
    Left = 96
    Top = 184
    object tblOrdersOrderNo: TFloatField
      FieldName = 'OrderNo'
      Required = True
    end
    object tblOrdersCustNo: TFloatField
      FieldName = 'CustNo'
      Required = True
    end
    object tblOrdersEmpName: TStringField
      FieldName = 'EmpName'
    end
    object tblOrdersSaleDate: TDateTimeField
      FieldName = 'SaleDate'
    end
    object tblOrdersShipToCity: TStringField
      FieldName = 'ShipToCity'
      Size = 15
    end
    object tblOrdersShipToPhone: TStringField
      FieldName = 'ShipToPhone'
      Size = 15
    end
  end
  object tblOrderItems: TTable
    DatabaseName = 'MasterDetail'
    IndexFieldNames = 'ItemNo;OrderNo'
    TableName = 'OrderItems.db'
    Left = 136
    Top = 184
  end
  object dsCustomers: TDataSource
    DataSet = tblCustomers
    Left = 64
    Top = 216
  end
  object dsOrders: TDataSource
    DataSet = tblOrders
    Left = 96
    Top = 216
  end
  object dsOrderItems: TDataSource
    DataSet = tblOrderItems
    Left = 136
    Top = 216
  end
  object EzDataset1: TEzDataset
    Cursors = <
      item
        CursorName = 'DefaultCursor'
      end
      item
        IndexFieldNames = 'Company'
        CursorName = 'Alphabetical'
        Options = [cpSorted]
      end
      item
        IndexFieldNames = 'SaleDate;Company'
        CursorName = 'Company/Saledate'
        Options = [cpSorted]
      end
      item
        IndexFieldNames = 'City'
        CursorName = 'City'
        Options = [cpSorted]
      end>
    EzDataLinks = <
      item
        DataSource = dsCustomers
        KeyField = 'CustNo'
        FieldLinks = (
          (
            'OrderQty'
            'concatenate(tostr(sum(childs(Qty))),'#39'+'#39')'))
      end
      item
        DataSource = dsOrders
        KeyField = 'OrderNo'
        ParentRefField = 'CustNo'
        FieldLinks = (
          (
            'City'
            'ShipToCity')
          (
            'Company'
            'concatenate('#39'Order: '#39', tostr(OrderNo))')
          (
            'CustNo'
            'OrderNo')
          (
            'OrderQty'
            'concatenate(tostr(sum(childs(Qty))),'#39'+'#39')'))
      end
      item
        DataSource = dsOrderItems
        KeyField = 'ItemNo'
        ParentRefField = 'OrderNo'
        FieldLinks = (
          (
            'Company'
            'ItemName')
          (
            'CustNo'
            'ItemNo')
          (
            'OrderNo'
            '')
          (
            'OrderQty'
            'Qty')
          (
            'SaleDate'
            'dsOrders.SaleDate')
          (
            'ShipToCity'
            'dsOrders.ShipToCity')
          (
            'ShipToPhone'
            'dsOrders.ShipToPhone'))
      end>
    Options = [eoCascadeDelete, eoParentKeysRequired, eoGenerateKeys]
    Left = 192
    Top = 184
    object EzDataset1Company: TStringField
      FieldName = 'Company'
      Size = 30
    end
    object EzDataset1ShipToPhone: TStringField
      FieldName = 'ShipToPhone'
      Size = 15
    end
    object EzDataset1CustNo: TFloatField
      FieldName = 'CustNo'
    end
    object EzDataset1OrderNo: TFloatField
      FieldName = 'OrderNo'
    end
    object EzDataset1ItemNo: TAutoIncField
      FieldName = 'ItemNo'
      ReadOnly = True
    end
    object EzDataset1City: TStringField
      FieldName = 'City'
      Size = 15
    end
    object EzDataset1SaleDate: TDateTimeField
      FieldName = 'SaleDate'
    end
    object EzDataset1Qty: TIntegerField
      FieldName = 'Qty'
    end
    object EzDataset1OrderQty: TStringField
      Alignment = taRightJustify
      DisplayLabel = 'Qty'
      FieldName = 'OrderQty'
    end
    object EzDataset1ShipToCity: TStringField
      FieldName = 'ShipToCity'
      Size = 15
    end
  end
  object dsMasterDetail: TDataSource
    DataSet = EzDataset1
    Left = 192
    Top = 216
  end
  object ilButtonImages: TImageList
    Height = 9
    Width = 9
    Left = 96
    Top = 272
    Bitmap = {
      494C010103000400040009000900FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000024000000090000000100180000000000CC03
      0000000000000000000000000000000000008080808080808080808080808080
      8080808080808080808080808080808080808080808080808080808080808080
      8080808080808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF0000000000000000000000000000000000000000000000000000008080
      80FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080808080FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000
      00000000000000000000808080FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFF
      FFFF808080808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080
      FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFF0000000000
      00000000000000000000000000000000000000000000808080FFFFFFFFFFFFFF
      FFFF000000FFFFFFFFFFFFFFFFFF808080808080FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFF
      FFFFFFFFFFFFFF00000000000000000000000000000000000000000000000000
      0000808080FFFFFF000000000000000000000000000000FFFFFF808080808080
      FFFFFF000000000000000000000000000000FFFFFF808080FFFFFFFFFFFFC0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFF00000000000000000000000000
      0000000000000000000000000000808080FFFFFFFFFFFFFFFFFF000000FFFFFF
      FFFFFFFFFFFF808080808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF808080FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFF00
      0000000000000000000000000000000000000000000000000000808080FFFFFF
      FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF808080808080FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FF
      FFFFFFFFFFFFFFFFFFFFFF000000000000000000000000000000000000000000
      000000000000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8080
      80808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000
      0000000000000000000000000000000000008080808080808080808080808080
      8080808080808080808080808080808080808080808080808080808080808080
      8080808080808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF000000000000000000000000000000000000000000000000000000424D
      3E000000000000003E0000002800000024000000090000000100010000000000
      480000000000000000000000000000000000000000000000FFFFFF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000}
  end
  object scGridSchemes: TEzTreeGridSchemes
    Schemes = <
      item
        Name = 'Hierarchy cell Customers'
        Color = 16769484
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        TextIndent = 14
        ExpandButtons.Expand = 0
        ExpandButtons.Collapse = 1
        ExpandButtons.Grayed = 2
        Indent = 14
        HierarchyCell = True
      end
      item
        Name = 'Hierarchy cell Orders'
        Color = 16773864
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ExpandButtons.Expand = 0
        ExpandButtons.Collapse = 1
        ExpandButtons.Grayed = 2
        DataVisible = False
        HierarchyCell = True
      end
      item
        Name = 'Hierarchy cell OrderItems'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        HierarchyCell = True
      end
      item
        Name = 'Customer cell'
        Color = 16769484
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      item
        Name = 'Order header cell'
        Color = 16773864
        Colspan = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ExpandButtons.Expand = 1
        ExpandButtons.Collapse = 0
        ExpandButtons.Grayed = 2
      end
      item
        Name = 'Orders cell'
        Color = 16773864
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      item
        Name = 'OrderItems cell'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end>
    ButtonImages = ilButtonImages
    Left = 128
    Top = 272
  end
end
