unit MDForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, Grids, DBGrids, EzDBGrid, Db, EzDataset, DBTables,
  StdCtrls, ExtCtrls, Mask, DBCtrls, ImgList;

type
  TfrmMasterDetail = class(TForm)
    Database1: TDatabase;
    tblCustomers: TTable;
    tblOrders: TTable;
    dsCustomers: TDataSource;
    dsOrders: TDataSource;
    EzDataset1: TEzDataset;
    dsMasterDetail: TDataSource;
    Panel1: TPanel;
    Button1: TButton;
    EzDataset1CustNo: TFloatField;
    EzDataset1Company: TStringField;
    EzDataset1City: TStringField;
    EzDataset1OrderNo: TFloatField;
    tblOrdersOrderNo: TFloatField;
    tblOrdersCustNo: TFloatField;
    tblOrdersShipToCity: TStringField;
    tblOrderItems: TTable;
    dsOrderItems: TDataSource;
    EzDataset1ItemNo: TAutoIncField;
    EzDataset1Qty: TIntegerField;
    tblOrdersShipToPhone: TStringField;
    EzDataset1ShipToCity: TStringField;
    EzDataset1ShipToPhone: TStringField;
    tblOrdersEmpName: TStringField;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    EzDBGrid1: TEzDBTreeGrid;
    Memo1: TMemo;
    Panel3: TPanel;
    Label12: TLabel;
    cbSort: TComboBox;
    tblOrdersSaleDate: TDateTimeField;
    EzDataset1SaleDate: TDateTimeField;
    Edit1: TEdit;
    Label5: TLabel;
    UpDown1: TUpDown;
    Panel6: TPanel;
    Label11: TLabel;
    Label13: TLabel;
    Label4: TLabel;
    DBEdit8: TDBEdit;
    Panel2: TPanel;
    DBEdit9: TDBEdit;
    DBEdit1: TDBEdit;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    tblCustomersCustNo: TFloatField;
    tblCustomersCompany: TStringField;
    tblCustomersAddr1: TStringField;
    tblCustomersAddr2: TStringField;
    tblCustomersCity: TStringField;
    scGridSchemes: TEzTreeGridSchemes;
    ilButtonImages: TImageList;
    EzDataset1OrderQty: TStringField;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbSortChange(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMasterDetail: TfrmMasterDetail;

implementation

{$R *.DFM}

procedure TfrmMasterDetail.Button1Click(Sender: TObject);
begin
  EzDataset1.Active := not EzDataset1.Active;
end;

procedure TfrmMasterDetail.FormCreate(Sender: TObject);
begin
  cbSort.ItemIndex := 0;
end;

procedure TfrmMasterDetail.cbSortChange(Sender: TObject);
begin
  EzDataset1.ActiveCursorIndex := cbSort.ItemIndex;
end;

procedure TfrmMasterDetail.Edit1Change(Sender: TObject);
begin
  EzDBGrid1.FixedCols := UpDown1.Position;
end;

procedure TfrmMasterDetail.Button2Click(Sender: TObject);
begin
  EzDataset1.ExpandAll;
end;

procedure TfrmMasterDetail.Button3Click(Sender: TObject);
begin
  EzDataset1.CollapseAll;
end;

procedure TfrmMasterDetail.Button4Click(Sender: TObject);
var
 T: DWORD;
 i: Integer;
begin
  T := GetTickCount;

  for i:=0 to 9 do
  begin
    EzDataset1.Refresh;
    Update;
  end;

  ShowMessage('Duration: ' + IntToStr(GetTickCount-T));
end;

procedure TfrmMasterDetail.Button5Click(Sender: TObject);
begin
  EzDataset1.Insert;
end;

procedure TfrmMasterDetail.Button6Click(Sender: TObject);
begin
  EzDataset1.Append;
end;

procedure TfrmMasterDetail.Button7Click(Sender: TObject);
begin
  EzDataset1.Add;
end;

procedure TfrmMasterDetail.Button8Click(Sender: TObject);
begin
  EzDataset1.AddChild;
end;

end.
