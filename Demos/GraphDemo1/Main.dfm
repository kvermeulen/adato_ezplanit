object Form1: TForm1
  Left = 30
  Top = 106
  Width = 1025
  Height = 642
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel2: TBevel
    Left = 8
    Top = 136
    Width = 577
    Height = 2
    Shape = bsTopLine
  end
  object Label2: TLabel
    Left = 8
    Top = 130
    Width = 94
    Height = 13
    Caption = 'TEzCalendarPanel  '
  end
  object EzCalendarPanel1: TEzCalendarPanel
    Left = 8
    Top = 256
    Width = 641
    Height = 297
    BorderWidth = 0
    BorderStyle = bsNone
    CalendarColor = clWindow
    RulesColor = clWindow
    OldCreateOrder = False
    IsControl = True
  end
  object Memo1: TMemo
    Left = 8
    Top = 152
    Width = 577
    Height = 89
    BorderStyle = bsNone
    Lines.Strings = (
      
        'A calendar stores a set of rules that determine when resources a' +
        're working or not. A rule could for example say that '
      #39'resources are not working on saturday'#39'.'
      ''
      
        'Rules can be accessed through the TEzCalendarPanel.Rules propert' +
        'y.')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 674
    Top = 130
    Width = 336
    Height = 419
    Caption = 'Panel1'
    TabOrder = 2
    object EzGraph1: TEzGraph
      Left = 1
      Top = 41
      Width = 334
      Height = 110
      BorderStyle = bsNone
      Lines = <
        item
          Description = 'Calendar data'
          Visible = True
          Pen.Color = 51712
          Brush.Color = clSilver
          Tag = 0
        end>
      MaxDate = 38182.000000000000000000
      Xax.Pen.Width = 2
      Xax.Position = 90
      Yax.Color = clBtnFace
      Yax.Font.Charset = DEFAULT_CHARSET
      Yax.Font.Color = clWindowText
      Yax.Font.Height = -11
      Yax.Font.Name = 'MS Sans Serif'
      Yax.Font.Style = []
      Yax.DrawInside = True
      Yax.SnapSize = 0.250000000000000000
      Yax.Width = 0
      Timebar = EzTimebar1
      GridPen.Color = clGray
      GridPen.Style = psDot
      Align = alTop
    end
    object EzTimebar1: TEzTimebar
      Left = 1
      Top = 1
      Width = 334
      Height = 40
      AutoUpdateMajorSettings = True
      Date = 38182.000000000000000000
      ScaleStorage = EzTimescaleStorage1
      ScaleIndex = 0
    end
    object EzGraph2: TEzGraph
      Left = 1
      Top = 175
      Width = 334
      Height = 110
      BorderStyle = bsNone
      Lines = <
        item
          Description = 'Calendar data'
          Visible = True
          CanEdit = True
          Pen.Color = clRed
          Pen.Width = 2
          Brush.Style = bsClear
          Tag = 0
        end>
      MaxDate = 38182.000000000000000000
      ScrollbarOptions.ScrollBars = ssVertical
      Xax.Pen.Width = 2
      Xax.Position = 102
      Yax.Color = clBtnFace
      Yax.Font.Charset = DEFAULT_CHARSET
      Yax.Font.Color = clWindowText
      Yax.Font.Height = -11
      Yax.Font.Name = 'MS Sans Serif'
      Yax.Font.Style = []
      Yax.DrawInside = True
      Yax.SnapSize = 0.250000000000000000
      Yax.Width = 0
      Timebar = EzTimebar1
      GridPen.Color = clGray
      GridPen.Style = psDot
      Align = alTop
    end
    object Panel4: TPanel
      Left = 1
      Top = 151
      Width = 334
      Height = 24
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'Graph 2'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object Panel5: TPanel
      Left = 1
      Top = 285
      Width = 334
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Graph 3'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object EzGraph3: TEzGraph
      Left = 1
      Top = 310
      Width = 334
      Height = 108
      BorderStyle = bsNone
      Lines = <
        item
          Description = 'Calendar data'
          Visible = True
          CanEdit = True
          Pen.Color = 51712
          Brush.Color = clSilver
          Tag = 0
        end>
      MaxDate = 38182.000000000000000000
      ScrollbarOptions.ScrollBars = ssVertical
      Xax.Pen.Width = 2
      Xax.Position = 100
      Yax.Color = clBtnFace
      Yax.Font.Charset = DEFAULT_CHARSET
      Yax.Font.Color = clWindowText
      Yax.Font.Height = -11
      Yax.Font.Name = 'MS Sans Serif'
      Yax.Font.Style = []
      Yax.DrawInside = True
      Yax.SnapSize = 0.250000000000000000
      Yax.Width = 0
      Timebar = EzTimebar1
      GridPen.Color = clGray
      GridPen.Style = psDot
      Align = alClient
    end
  end
  object Memo2: TMemo
    Left = 672
    Top = 8
    Width = 338
    Height = 105
    BorderStyle = bsNone
    Lines.Strings = (
      
        'A TEzGraph displays the contents of a TEzAvailabilityProfile in ' +
        'a'
      'graphical manner.'
      ''
      
        'Availability profiles can indicate availabilty and/or allocation' +
        '.'
      'Availability when used with resources in which case the'
      
        'profile indicates when a certain resource is availble. Allocatio' +
        'n when'
      'used with the scheduler in which case the profile'
      'indicates the periods when tasks are running.'
      ''
      '======='
      'GRAPH 1'
      '======='
      
        'In this demo, this graph is connected to the calendar located on' +
        ' top'
      'of this page. Therefore the line displays the'
      'translation of the ruleset into working and non working periods.'
      'Notice that whenever a rule is changed, added or'
      'deleted the graph is updated accordingly.'
      ''
      '======='
      'GRAPH 2'
      '======='
      
        'This is a second TEzGraph. This graph displays a single line whi' +
        'ch '
      'marks the maximum units available. You can edit'
      
        'this line using your mouse thereby changing the availability of ' +
        'the '
      'resource. Hold down the '#39'shift'#39' key to select only'
      'part'
      'of the line. By default, the availability is set to 1 unit.'
      ''
      '======='
      'GRAPH 3'
      '======='
      'This graph combines the data from the calendar and the second '
      'graph into one availability profile. Notice that when'
      
        'the rules stored in the calendar are changed, this graph is upda' +
        'ted '
      'accordingly.  The same happens when you'
      'change the second graph.'
      ''
      'This graph is important because this is the graph used by the '
      'scheduler when scheduling a task.'
      '')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 3
  end
  object Memo5: TMemo
    Left = 8
    Top = 8
    Width = 577
    Height = 113
    BorderStyle = bsNone
    Color = clWhite
    Lines.Strings = (
      
        'This demonstration shows how to use the TEzCalendarPanel and TEz' +
        'Graph components. '
      ''
      
        'There are four components on this form, 1 calendar and 3 graphs.' +
        ' '
      
        'The calendar maintains a list of calendar rules which you can ed' +
        'it by pressing the buttons located on the calendar '
      
        'panel. These rules are then applied to an availability profile w' +
        'hich is made visible in the first graph. '
      
        'The second graph enable you to edit the units at which resources' +
        ' are available. The line shown indicates the number '
      'of resources available during a certain period.'
      
        'Finally the third graph combines data from the calendar and the ' +
        'second graph into a single availability profile.'
      ''
      
        'Notice that changes made in one component are automatically appl' +
        'ied to the other components as well.')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object Panel2: TPanel
    Left = 0
    Top = 567
    Width = 1017
    Height = 41
    Align = alBottom
    TabOrder = 5
    object Button1: TButton
      Left = 440
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object EzTimescaleStorage1: TEzTimescaleStorage
    Scales = <
      item
        Bands = <
          item
            DisplayFormat = 'W x'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Scale = msWeek
          end
          item
            DisplayFormat = '$'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
          end>
        Name = 'TEzTimescale'
      end>
    Left = 640
    Top = 200
  end
end
