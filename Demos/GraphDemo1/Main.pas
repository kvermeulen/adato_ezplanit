unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EzTimeBar, EzGraph, ExtCtrls, StdCtrls, DesignPanel, EzCalendar;

type
  TForm1 = class(TForm)
    EzCalendarPanel1: TEzCalendarPanel;
    Memo1: TMemo;
    Panel1: TPanel;
    EzGraph1: TEzGraph;
    EzTimebar1: TEzTimebar;
    Memo2: TMemo;
    Label2: TLabel;
    Bevel2: TBevel;
    Memo5: TMemo;
    EzGraph2: TEzGraph;
    Panel4: TPanel;
    Panel5: TPanel;
    EzGraph3: TEzGraph;
    Panel2: TPanel;
    Button1: TButton;
    EzTimescaleStorage1: TEzTimescaleStorage;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // The line will display the contants of the availability
  // profile maintained by the calendar.
  //
  EzGraph1.Lines[0].Profile := EzCalendarPanel1.Availability;

  // Update the default availbility in the second graph to 1 unit.
  EzGraph2.Lines[0].Profile.Add(0, 1);

  EzGraph3.Lines[0].Profile.Rules := EzCalendarPanel1.Rules;
  EzGraph3.Lines[0].Profile.AddSource(EzGraph2.Lines[0].Profile);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Close;
end;

end.
