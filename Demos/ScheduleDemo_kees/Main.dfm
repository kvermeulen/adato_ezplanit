object Form1: TForm1
  Left = 192
  Top = 114
  Caption = 'Form1'
  ClientHeight = 501
  ClientWidth = 875
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 875
    Height = 460
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 0
    object tsProjectData: TTabSheet
      Caption = 'Project data'
      ImageIndex = 3
      object Label6: TLabel
        Left = 32
        Top = 28
        Width = 56
        Height = 13
        Caption = 'Project start'
      end
      object Label7: TLabel
        Left = 32
        Top = 60
        Width = 54
        Height = 13
        Caption = 'Project end'
      end
      object Label10: TLabel
        Left = 9
        Top = 92
        Width = 77
        Height = 13
        Caption = 'Project calendar'
      end
      object ProjectStart: TDateTimePicker
        Left = 96
        Top = 24
        Width = 137
        Height = 21
        Date = 37959.675753645830000000
        Time = 37959.675753645830000000
        TabOrder = 0
      end
      object ProjectEnd: TDateTimePicker
        Left = 96
        Top = 56
        Width = 137
        Height = 21
        Date = 37959.675830381940000000
        Time = 37959.675830381940000000
        TabOrder = 1
      end
      object ProjectCalendar: TEzCalendarPanel
        Left = 96
        Top = 80
        Width = 657
        Height = 301
        BorderWidth = 0
        BorderStyle = bsNone
        CalendarColor = clWindow
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        RulesColor = clWindow
        OldCreateOrder = False
        IsControl = True
      end
      object Button4: TButton
        Left = 8
        Top = 128
        Width = 75
        Height = 25
        Caption = 'Button4'
        TabOrder = 3
        OnClick = Button4Click
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Tasks'
      object Label8: TLabel
        Left = 8
        Top = 248
        Width = 51
        Height = 13
        Caption = 'Resources'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 8
        Top = 288
        Width = 64
        Height = 13
        Caption = 'Predecessors'
        FocusControl = DBEdit2
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 12
        Width = 753
        Height = 229
        DataSource = DataSource1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'TaskID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Description'
            Width = 71
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Start'
            Width = 104
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Stop'
            Width = 99
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Work'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ConstraintLookup'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ConstraintDate'
            Visible = True
          end>
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 264
        Width = 217
        Height = 21
        DataField = 'Resources'
        DataSource = DataSource1
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 8
        Top = 304
        Width = 217
        Height = 21
        DataField = 'Predecessors'
        DataSource = DataSource1
        TabOrder = 2
      end
    end
    object tsResources: TTabSheet
      BorderWidth = 10
      Caption = 'Resource availability'
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 33
        Width = 610
        Height = 250
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 0
        object AvailabilityGraphs: TEzGraph
          Left = 1
          Top = 42
          Width = 608
          Height = 207
          BorderStyle = bsNone
          Lines = <
            item
              Description = 'Resource'
              CanEdit = True
              Pen.Color = clRed
              Pen.Width = 2
              Brush.Style = bsClear
              Tag = 0
            end>
          MaxDate = 37960.000000000000000000
          OnChanged = AvailabilityGraphsChanged
          VSnapSize = 0.250000000000000000
          Xax.Position = 177
          Yax.Color = clBtnFace
          Yax.Font.Charset = DEFAULT_CHARSET
          Yax.Font.Color = clWindowText
          Yax.Font.Height = -11
          Yax.Font.Name = 'MS Sans Serif'
          Yax.Font.Style = []
          Timebar = EzTimebar1
          Align = alClient
        end
        object Panel10: TPanel
          Left = 1
          Top = 1
          Width = 608
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Panel10'
          TabOrder = 1
          object EzTimebar1: TEzTimebar
            Left = 20
            Top = 0
            Width = 588
            Height = 41
            AutoUpdateMajorSettings = True
            Align = alClient
            Date = 37960.000000000000000000
            ScaleStorage = EzTimescaleStorage1
            ScaleIndex = 0
          end
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 20
            Height = 41
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
          end
        end
      end
      object Panel2: TPanel
        Left = 610
        Top = 33
        Width = 237
        Height = 250
        Align = alRight
        Caption = 'Panel2'
        TabOrder = 1
        object Memo1: TMemo
          Left = 1
          Top = 25
          Width = 235
          Height = 224
          Align = alClient
          TabOrder = 0
        end
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 235
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Textual contents of availability profile'
          TabOrder = 1
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 847
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label5: TLabel
          Left = 10
          Top = 8
          Width = 74
          Height = 13
          Caption = 'Select resource'
        end
        object Button10: TButton
          Left = 488
          Top = 2
          Width = 75
          Height = 25
          Caption = 'Reset'
          TabOrder = 0
          OnClick = Button10Click
        end
        object cbSelectResource: TComboBox
          Left = 88
          Top = 4
          Width = 105
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
          OnChange = cbSelectResourceChange
          OnDropDown = cbSelectResourceDropDown
        end
        object chkResourceMaySwitch: TCheckBox
          Left = 208
          Top = 6
          Width = 201
          Height = 17
          Caption = 'Resource may switch between tasks'
          TabOrder = 2
          OnClick = chkResourceMaySwitchClick
        end
        object btnSaveGraphs: TButton
          Left = 408
          Top = 2
          Width = 75
          Height = 25
          Caption = 'Save data'
          TabOrder = 3
          OnClick = btnSaveGraphsClick
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 283
        Width = 847
        Height = 129
        Align = alBottom
        Caption = 'Panel9'
        TabOrder = 3
        object GroupBox1: TGroupBox
          Left = 8
          Top = 8
          Width = 465
          Height = 113
          Caption = 'Edit availability graph'
          TabOrder = 0
          object Label3: TLabel
            Left = 33
            Top = 16
            Width = 23
            Height = 13
            Caption = 'Date'
          end
          object Label1: TLabel
            Left = 144
            Top = 16
            Width = 24
            Height = 13
            Caption = 'Units'
          end
          object Label2: TLabel
            Left = 208
            Top = 16
            Width = 40
            Height = 13
            Caption = 'Duration'
          end
          object Label4: TLabel
            Left = 248
            Top = 16
            Width = 28
            Height = 13
            Caption = '(days)'
          end
          object ProfileDate: TDateTimePicker
            Left = 32
            Top = 32
            Width = 105
            Height = 21
            Date = 37951.685678379640000000
            Time = 37951.685678379640000000
            TabOrder = 0
          end
          object Edit1: TEdit
            Left = 144
            Top = 32
            Width = 57
            Height = 21
            TabOrder = 1
            Text = '1'
          end
          object Edit2: TEdit
            Left = 208
            Top = 32
            Width = 49
            Height = 21
            TabOrder = 2
            Text = '4'
          end
          object Button2: TButton
            Left = 384
            Top = 48
            Width = 75
            Height = 25
            Caption = 'Sum'
            TabOrder = 3
            OnClick = Button2Click
          end
          object Button9: TButton
            Left = 384
            Top = 16
            Width = 75
            Height = 25
            Caption = 'Allocate'
            TabOrder = 4
            OnClick = Button9Click
          end
          object Button1: TButton
            Left = 296
            Top = 16
            Width = 75
            Height = 25
            Caption = 'Add point'
            TabOrder = 5
            OnClick = Button1Click
          end
          object btnSetInterval: TButton
            Left = 296
            Top = 48
            Width = 75
            Height = 25
            Caption = 'Set interval'
            TabOrder = 6
            OnClick = btnSetIntervalClick
          end
          object chkAllowNegative: TCheckBox
            Left = 32
            Top = 56
            Width = 129
            Height = 17
            Caption = 'Allow negative values'
            TabOrder = 7
          end
          object chkAllocated: TCheckBox
            Left = 32
            Top = 72
            Width = 153
            Height = 17
            Caption = 'Mark interval as '#39'allocated'#39
            TabOrder = 8
          end
          object chkNoScheduleAcross: TCheckBox
            Left = 32
            Top = 88
            Width = 257
            Height = 17
            Caption = 'Tasks may not be scheduled across this interval'
            TabOrder = 9
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      BorderWidth = 10
      Caption = 'Resource allocation'
      ImageIndex = 2
      object Panel5: TPanel
        Left = 0
        Top = 121
        Width = 610
        Height = 291
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 0
        object EzGraph2: TEzGraph
          Left = 1
          Top = 41
          Width = 608
          Height = 249
          BorderStyle = bsNone
          Lines = <
            item
              Description = 'Resource line'
              Visible = True
              CanEdit = True
              Pen.Color = clRed
              Brush.Style = bsClear
              Tag = 0
            end>
          MaxDate = 37865.000000000000000000
          VScale = 30
          VSnapSize = 0.250000000000000000
          Xax.Position = 212
          Yax.Color = clBtnFace
          Yax.Font.Charset = DEFAULT_CHARSET
          Yax.Font.Color = clWindowText
          Yax.Font.Height = -11
          Yax.Font.Name = 'MS Sans Serif'
          Yax.Font.Style = []
          Yax.DrawInside = True
          Yax.Width = 0
          Timebar = EzTimebar2
          Align = alClient
        end
        object EzTimebar2: TEzTimebar
          Left = 1
          Top = 1
          Width = 608
          Height = 40
          AutoUpdateMajorSettings = True
          Date = 37865.000000000000000000
          ScaleStorage = EzTimescaleStorage1
          ScaleIndex = 0
        end
      end
      object Panel6: TPanel
        Left = 610
        Top = 121
        Width = 237
        Height = 291
        Align = alRight
        Caption = 'Panel2'
        TabOrder = 1
        object Memo2: TMemo
          Left = 1
          Top = 25
          Width = 235
          Height = 265
          Align = alClient
          TabOrder = 0
        end
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 235
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Contents of availability profile'
          TabOrder = 1
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 847
        Height = 121
        Align = alTop
        Caption = 'Panel8'
        TabOrder = 2
        object cbResources: TComboBox
          Left = 40
          Top = 20
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = cbResourcesChange
          OnDropDown = cbResourcesDropDown
        end
      end
    end
    object tsScheduleResult: TTabSheet
      Caption = 'Schedule messages'
      ImageIndex = 4
      object lvScheduleMessages: TListView
        Left = 32
        Top = 56
        Width = 665
        Height = 177
        Columns = <
          item
            Caption = 'Task id'
          end
          item
            AutoSize = True
            Caption = 'Message'
          end>
        TabOrder = 0
        ViewStyle = vsReport
      end
    end
    object TabSheet2: TTabSheet
      BorderWidth = 5
      Caption = 'Profile calculator'
      ImageIndex = 5
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 337
        Height = 422
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 337
          Height = 81
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label11: TLabel
            Left = 10
            Top = 10
            Width = 74
            Height = 13
            Caption = 'Select resource'
          end
          object cbResourceProfiles: TComboBox
            Left = 88
            Top = 6
            Width = 105
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnDropDown = cbResourceProfilesDropDown
          end
          object btnAddProfile: TButton
            Left = 208
            Top = 6
            Width = 75
            Height = 21
            Caption = 'Add profile'
            TabOrder = 1
            OnClick = btnAddProfileClick
          end
          object Panel14: TPanel
            Left = 0
            Top = 40
            Width = 337
            Height = 41
            Align = alBottom
            BevelOuter = bvNone
            Caption = 'Panel10'
            TabOrder = 2
            object Timebar: TEzTimebar
              Left = 20
              Top = 0
              Width = 317
              Height = 41
              AutoUpdateMajorSettings = True
              ButtonStyle = btsFlatRect
              Align = alClient
              Date = 37960.000000000000000000
            end
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 20
              Height = 41
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
            end
          end
        end
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 81
          Width = 337
          Height = 324
          Align = alClient
          BorderStyle = bsNone
          TabOrder = 1
        end
        object ScrollBar1: TScrollBar
          Left = 0
          Top = 405
          Width = 337
          Height = 17
          Align = alBottom
          PageSize = 0
          TabOrder = 2
          OnScroll = ScrollBar1Scroll
        end
      end
      object Panel16: TPanel
        Left = 337
        Top = 0
        Width = 520
        Height = 422
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel16'
        TabOrder = 1
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 520
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Panel17'
          TabOrder = 0
        end
        object Panel18: TPanel
          Left = 0
          Top = 41
          Width = 520
          Height = 381
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel1'
          TabOrder = 1
          object EzGraph1: TEzGraph
            Left = 0
            Top = 41
            Width = 520
            Height = 340
            BorderStyle = bsNone
            Lines = <
              item
                Description = 'Resource'
                CanEdit = True
                Pen.Color = clRed
                Pen.Width = 2
                Brush.Style = bsClear
                Tag = 0
              end>
            MaxDate = 37960.000000000000000000
            OnChanged = AvailabilityGraphsChanged
            VSnapSize = 0.250000000000000000
            Xax.Position = 181
            Yax.Color = clBtnFace
            Yax.Font.Charset = DEFAULT_CHARSET
            Yax.Font.Color = clWindowText
            Yax.Font.Height = -11
            Yax.Font.Name = 'MS Sans Serif'
            Yax.Font.Style = []
            Timebar = EzTimebar3
            Align = alClient
          end
          object Panel19: TPanel
            Left = 0
            Top = 0
            Width = 520
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            Caption = 'Panel10'
            TabOrder = 1
            object EzTimebar3: TEzTimebar
              Left = 20
              Top = 0
              Width = 500
              Height = 41
              AutoUpdateMajorSettings = True
              ButtonStyle = btsFlatRect
              Align = alClient
              Date = 37960.000000000000000000
              ScaleStorage = EzTimescaleStorage1
              ScaleIndex = 0
            end
            object Panel20: TPanel
              Left = 0
              Top = 0
              Width = 20
              Height = 41
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
            end
          end
        end
      end
    end
  end
  object Panel21: TPanel
    Left = 0
    Top = 0
    Width = 875
    Height = 41
    Align = alTop
    Caption = 'Panel21'
    TabOrder = 1
    object btnReschedule: TButton
      Left = 21
      Top = 9
      Width = 75
      Height = 25
      Caption = 'Reschedule'
      TabOrder = 0
      OnClick = btnRescheduleClick
    end
  end
  object EzScheduler1: TEzScheduler
    OnLoadResourceData = EzScheduler1LoadResourceData
    Left = 472
    Top = 24
  end
  object DataSource1: TDataSource
    DataSet = Tasks
    Left = 592
    Top = 8
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'xml'
    Filter = 'xml (*.xml)|*.xml'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 632
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'xml'
    Filter = 'Xml (*.xml)|*.xml'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 672
    Top = 8
  end
  object Cnn: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.\EzProject.mdb;Per' +
      'sist Security Info=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 512
    Top = 8
  end
  object Tasks: TADODataSet
    Connection = Cnn
    OnNewRecord = TasksNewRecord
    CommandText = 'select * from tasks'
    Parameters = <>
    Left = 560
    Top = 8
    object TasksTaskID: TAutoIncField
      FieldName = 'TaskID'
      ReadOnly = True
    end
    object TasksDescription: TWideStringField
      FieldName = 'Description'
      Size = 50
    end
    object TasksStart: TDateTimeField
      FieldName = 'Start'
    end
    object TasksStop: TDateTimeField
      FieldName = 'Stop'
    end
    object TasksWork: TEzDurationField
      FieldName = 'Work'
      DisplayFormat = 'd\d hh:mm'
      InputSettings.HoursPerDay = 24.000000000000000000
    end
    object TasksConstraint: TIntegerField
      FieldName = 'Constraint'
    end
    object TasksConstraintDate: TDateTimeField
      FieldName = 'ConstraintDate'
    end
    object TasksResources: TWideStringField
      FieldName = 'Resources'
      Size = 50
    end
    object TasksPredecessors: TWideStringField
      FieldName = 'Predecessors'
      Size = 50
    end
    object TasksConstraintLookup: TStringField
      DisplayLabel = 'Constraint'
      FieldKind = fkLookup
      FieldName = 'ConstraintLookup'
      LookupDataSet = qrConstraintNames
      LookupKeyFields = 'ConstraintID'
      LookupResultField = 'Description'
      KeyFields = 'Constraint'
      Lookup = True
    end
  end
  object qrConstraintNames: TADODataSet
    Connection = Cnn
    CommandText = 'select * from ConstraintNames'
    Parameters = <>
    Left = 560
    Top = 40
    object qrConstraintNamesConstraintID: TIntegerField
      FieldName = 'ConstraintID'
    end
    object qrConstraintNamesDescription: TWideStringField
      FieldName = 'Description'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qrConstraintNames
    Left = 592
    Top = 40
  end
  object qrResources: TADODataSet
    Connection = Cnn
    CommandText = 'select * from Resources'
    Parameters = <>
    Left = 560
    Top = 72
    object qrResourcesResourceID: TAutoIncField
      FieldName = 'ResourceID'
      ReadOnly = True
    end
    object qrResourcesName: TWideStringField
      FieldName = 'Name'
      Size = 50
    end
    object qrResourcesAllowTaskSwitching: TBooleanField
      FieldName = 'AllowTaskSwitching'
    end
  end
  object dsResources: TDataSource
    DataSet = qrResources
    Left = 592
    Top = 72
  end
  object qrResourceAvailability: TADODataSet
    Connection = Cnn
    CommandText = 'select * from ResourceAvailability'
    Parameters = <>
    Left = 560
    Top = 104
    object qrResourceAvailabilityResourceID: TIntegerField
      FieldName = 'ResourceID'
    end
    object qrResourceAvailabilityDate: TDateTimeField
      FieldName = 'Date'
    end
    object qrResourceAvailabilityUnits: TFloatField
      FieldName = 'Units'
    end
  end
  object DataSource3: TDataSource
    DataSet = qrResourceAvailability
    Left = 592
    Top = 104
  end
  object EzTimescaleStorage1: TEzTimescaleStorage
    Scales = <
      item
        Bands = <
          item
            DisplayFormat = 'W x'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Scale = msWeek
          end
          item
            DisplayFormat = '$'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
          end>
        Name = 'TEzTimescale'
      end>
    Left = 352
    Top = 40
  end
end
