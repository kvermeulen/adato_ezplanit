unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DesignPanel, EzDataset, EzCalendar, StdCtrls, ComCtrls, EzScheduler,
  EzTimeBar, DB, EzGraph, Grids, DBGrids, ExtCtrls, EzDurationField,
  EzVirtualDataset, EzDBGrid, DBTables, ADODB, Mask, DBCtrls, EzArrays, EzDatetime;

type
  TForm1 = class(TForm)
    EzScheduler1: TEzScheduler;
    btnReschedule: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    tsResources: TTabSheet;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Panel1: TPanel;
    AvailabilityGraphs: TEzGraph;
    Panel2: TPanel;
    Memo1: TMemo;
    Panel3: TPanel;
    Panel4: TPanel;
    Button10: TButton;
    TabSheet3: TTabSheet;
    Panel5: TPanel;
    EzGraph2: TEzGraph;
    EzTimebar2: TEzTimebar;
    Panel6: TPanel;
    Memo2: TMemo;
    Panel7: TPanel;
    Panel8: TPanel;
    cbResources: TComboBox;
    cbSelectResource: TComboBox;
    Label5: TLabel;
    tsProjectData: TTabSheet;
    ProjectStart: TDateTimePicker;
    ProjectEnd: TDateTimePicker;
    Label6: TLabel;
    Label7: TLabel;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    tsScheduleResult: TTabSheet;
    lvScheduleMessages: TListView;
    Panel9: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    ProfileDate: TDateTimePicker;
    Edit1: TEdit;
    Edit2: TEdit;
    Button2: TButton;
    Button9: TButton;
    Button1: TButton;
    btnSetInterval: TButton;
    chkAllowNegative: TCheckBox;
    chkAllocated: TCheckBox;
    chkNoScheduleAcross: TCheckBox;
    chkResourceMaySwitch: TCheckBox;
    Panel10: TPanel;
    EzTimebar1: TEzTimebar;
    Panel11: TPanel;
    Cnn: TADOConnection;
    Tasks: TADODataSet;
    TasksTaskID: TAutoIncField;
    TasksDescription: TWideStringField;
    TasksStart: TDateTimeField;
    TasksStop: TDateTimeField;
    TasksConstraint: TIntegerField;
    TasksConstraintDate: TDateTimeField;
    TasksResources: TWideStringField;
    TasksPredecessors: TWideStringField;
    TasksWork: TEzDurationField;
    qrConstraintNames: TADODataSet;
    DataSource2: TDataSource;
    qrConstraintNamesConstraintID: TIntegerField;
    qrConstraintNamesDescription: TWideStringField;
    TasksConstraintLookup: TStringField;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    qrResources: TADODataSet;
    dsResources: TDataSource;
    qrResourcesResourceID: TAutoIncField;
    qrResourcesName: TWideStringField;
    qrResourcesAllowTaskSwitching: TBooleanField;
    btnSaveGraphs: TButton;
    qrResourceAvailability: TADODataSet;
    DataSource3: TDataSource;
    qrResourceAvailabilityResourceID: TIntegerField;
    qrResourceAvailabilityDate: TDateTimeField;
    qrResourceAvailabilityUnits: TFloatField;
    ProjectCalendar: TEzCalendarPanel;
    Label10: TLabel;
    Button4: TButton;
    TabSheet2: TTabSheet;
    Panel12: TPanel;
    Panel13: TPanel;
    ScrollBox1: TScrollBox;
    Label11: TLabel;
    cbResourceProfiles: TComboBox;
    btnAddProfile: TButton;
    Panel14: TPanel;
    Timebar: TEzTimebar;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    EzGraph1: TEzGraph;
    Panel19: TPanel;
    EzTimebar3: TEzTimebar;
    Panel20: TPanel;
    ScrollBar1: TScrollBar;
    EzTimescaleStorage1: TEzTimescaleStorage;
    Panel21: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnSetIntervalClick(Sender: TObject);
    procedure btnRescheduleClick(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure cbResourcesDropDown(Sender: TObject);
    procedure cbResourcesChange(Sender: TObject);
    procedure cbSelectResourceDropDown(Sender: TObject);
    procedure cbSelectResourceChange(Sender: TObject);
    procedure chkResourceMaySwitchClick(Sender: TObject);
    procedure btnSaveGraphsClick(Sender: TObject);
    procedure TasksNewRecord(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure AvailabilityGraphsChanged(Sender: TEzGraph;
      Line: TEzGraphLine; Point: TEzAvailabilityPoint;
      Event: TEzListNotification);
    procedure EzScheduler1LoadResourceData(Scheduler: TEzScheduler;
      Resource: TEzResource);
    procedure Button4Click(Sender: TObject);
    procedure btnAddProfileClick(Sender: TObject);
    procedure cbResourceProfilesDropDown(Sender: TObject);
    procedure ScrollBar1Scroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);
  private
    function GetIntervalFlags: TAvailabilityFlags;

  public
    FrameCount: Integer;
    FScheduler: TEzScheduler;
    FResources: TEzResources;

    procedure LoadResources;
    procedure ReloadList(ADate: TDateTime);
    procedure ReloadAllocationData;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses Math, MsXml, Graph;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ProjectStart.Date := Now;
  ProjectStart.Time := 0;
  ProjectEnd.Date := Now+100;
  ProjectEnd.Time := 0;
  ProfileDate.Date := now;
  ProfileDate.Time := 0;

  FScheduler := TEzScheduler.Create(Self);
  FScheduler.Name := 'EzScheduler';

  // FResources holds a list of resources
  FResources := TEzResources.Create(True);

  Tasks.Open;
end;

function TForm1.GetIntervalFlags: TAvailabilityFlags;
begin
  Result := [];
  if chkAllocated.Checked then
    Include(Result, afIsAllocated);

  if chkNoScheduleAcross.Checked then
    Include(Result, afNoScheduleAcross);
end;

procedure TForm1.ReloadList(ADate: TDateTime);
var
  i: Integer;
  s: string;
  Profile: TEzAvailabilityProfile;

begin
  Profile := AvailabilityGraphs.Lines[0].Profile;
  Memo1.Lines.Clear;
  for i:=0 to Profile.Count-1 do
  begin
    s := DateToStr(EzDateTimeToDateTime(Profile[i].DateValue)) + ': ' + FloatToStr(Profile[i].Units) + ' units';

    if afIsAllocated in Profile[i].Flags then
      s := s + ', allocated';

    if afNoScheduleAcross in Profile[i].Flags then
      s := s + ', no schedule across';

    Memo1.Lines.Add(s)
  end;

  EzTimebar1.Date := ADate;
  AvailabilityGraphs.Invalidate;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  Profile: TEzAvailabilityProfile;
begin
  Profile := AvailabilityGraphs.Lines[cbSelectResource.ItemIndex].Profile;
  if Profile.SumInterval(ProfileDate.Date, ProfileDate.Date+StrToInt(Edit2.Text), StrToFloat(Edit1.Text), chkAllowNegative.Checked) then
    ShowMessage('A negative value was inserted.');
  ReloadList(ProfileDate.Date);
end;

procedure TForm1.Button9Click(Sender: TObject);
var
  Profile: TEzAvailabilityProfile;
begin
  Profile := AvailabilityGraphs.Lines[cbSelectResource.ItemIndex].Profile;
  if Profile.AllocateInterval(DatetimetoEzDatetime(ProfileDate.Date), DatetimetoEzDatetime(ProfileDate.Date+StrToInt(Edit2.Text)), StrToFloat(Edit1.Text), chkAllowNegative.Checked) then
    ShowMessage('A negative value was inserted, resource is over allocated.');
  ReloadList(ProfileDate.Date);
end;

procedure TForm1.btnSetIntervalClick(Sender: TObject);
var
  Profile: TEzAvailabilityProfile;
begin
  Profile := AvailabilityGraphs.Lines[cbSelectResource.ItemIndex].Profile;
  Profile.SetInterval(ProfileDate.Date, ProfileDate.Date+StrToInt(Edit2.Text), StrToFloat(Edit1.Text), GetIntervalFlags);
  ReloadList(ProfileDate.Date);
end;

procedure TForm1.btnRescheduleClick(Sender: TObject);
var
  Task: TEzScheduleTask;
  Int: TEzScheduleInterval;
  i: Integer;
  li: TListItem;

begin
  // Clean up before we start
  lvScheduleMessages.Items.Clear;

  FScheduler.Reset;

  // Make sure all resources assigned to tasks are loaded
  LoadResources;

  //
  // Load tasks
  //
  Tasks.First;
  while not Tasks.Eof do
    //
    // Load all tasks into the scheduler
    //
  begin
    Task := TEzScheduleTask.Create;
    Task.Key.Level := -1;
    Task.Key.Value := Tasks['TaskID'];
    Task.Parent.Value := Null;
    Task.Resources.Text := Tasks.FieldByName('Resources').AsString;
    Task.Predecessors.Text := Tasks.FieldByName('Predecessors').AsString;

    Task.Constraint := TEzConstraint(Tasks.FieldByName('Constraint').AsInteger);
    Task.ConstraintDate := Tasks.FieldByName('ConstraintDate').AsDateTime;

    Int := TEzScheduleInterval.Create;
    Int.Duration := Tasks.FieldByName('Work').AsFloat;
    Task.Intervals.Add(Int);

    FScheduler.AddTask(Task);
    Tasks.Next;
  end;

  // Window holds the project schedule window.
  // That is the start and stop dates in which the project must be completed.
  FScheduler.ProjectWindowStart := Floor(ProjectStart.Date);
  FScheduler.ProjectWindowStop := Floor(ProjectEnd.Date);

  // Set projects availability profile. This profile is used
  // for scheduling tasks without resources.
  FScheduler.ProjectProfile := TEzAvailabilityProfile.Create(1);
  // ProjectCalendar.Availability;

  FScheduler.ProjectProfile.Rules := TEzCalendarRules.Create;
  FScheduler.ProjectProfile.Rules.AddEncoded (StrToDate ('07-01-1903'), itNonWorking);
  FScheduler.ProjectProfile.Rules.AddEncoded (StrToDate ('01-01-1903'), itNonWorking);

  FScheduler.OnLoadResourceData := EzScheduler1LoadResourceData;
  //
  // Reschedule project
  //
  FScheduler.Reschedule;
  if srScheduledOK in FScheduler.ScheduleResults then
  begin
    Task := nil;
    while FScheduler.GetNextScheduledTask(Task) do
    begin
      Tasks.Locate('TaskID', Task.Key.Value, []);
      Tasks.Edit;
      Tasks['Start'] := Task.Intervals[0].Start;
      Tasks['Stop'] := Task.Intervals[0].Stop;
      Tasks.Post;
    end;
  end;

  for i:=0 to FScheduler.Messages.Count-1 do
  begin
    li := lvScheduleMessages.Items.Add;
    li.Caption := FScheduler.Messages[i].TaskKey.Value;
    li.SubItems.Add(FScheduler.Messages[i].MessageText);
  end;

  if lvScheduleMessages.Items.Count>0 then
    PageControl1.ActivePage := tsScheduleResult;

  // Refresh Tasks dataset to display new data.
  Tasks.Refresh;
end;

procedure TForm1.Button10Click(Sender: TObject);
var
  Profile: TEzAvailabilityProfile;
begin
  Profile := AvailabilityGraphs.Lines[cbSelectResource.ItemIndex].Profile;
  Profile.Clear;
  ReloadList(ProfileDate.Date);
end;

procedure TForm1.cbResourcesDropDown(Sender: TObject);
var
  i: Integer;

begin
  cbResources.Items.Clear;
  for i:=0 to FScheduler.Resources.Count-1 do
    cbResources.Items.Add(FScheduler.Resources[i].Key);
end;

procedure TForm1.cbResourcesChange(Sender: TObject);
begin
  EzGraph2.Lines[0].Profile := FScheduler.Resources[cbResources.ItemIndex].ActualAvailability;
  ReloadAllocationData;
end;

procedure TForm1.ReloadAllocationData;
var
  i: Integer;
  s: string;
  Profile: TEzAvailabilityProfile;

begin
  Memo2.Lines.Clear;
  Profile := EzGraph2.Lines[0].Profile;
  for i:=0 to Profile.Count-1 do
  begin
    s := DateTimeToStr(EzDateTimeToDateTime(Profile[i].DateValue)) + ': ' + FloatToStr(Profile[i].Units) + ' units';

    if afIsAllocated in Profile[i].Flags then
      s := s + ', allocated';

    if afNoScheduleAcross in Profile[i].Flags then
      s := s + ', no schedule across';

    Memo2.Lines.Add(s)
  end;

  EzTimebar2.Date := EzDateTimeToDateTime(Profile[min(Profile.Count-1, 1)].DateValue);
end;

procedure TForm1.cbSelectResourceDropDown(Sender: TObject);
var
  i: integer;

begin
  LoadResources;
  cbSelectResource.Items.Clear;
  for i:=0 to FResources.Count-1 do
    cbSelectResource.Items.Add(FResources[i].Name);
end;

procedure TForm1.cbSelectResourceChange(Sender: TObject);
begin
  // Make selected line visible
  AvailabilityGraphs.Lines[0].Profile := FResources[cbSelectResource.ItemIndex].Availability;
  AvailabilityGraphs.Lines[0].Visible := True;

  // Update checkbox
  chkResourceMaySwitch.Checked := FResources[cbSelectResource.ItemIndex].AllowTaskSwitching;

  // Load textual data into listbox
  ReloadList(ProjectStart.Date);
end;

procedure TForm1.chkResourceMaySwitchClick(Sender: TObject);
begin
  if cbSelectResource.ItemIndex <> -1 then
    FResources[cbSelectResource.ItemIndex].AllowTaskSwitching := chkResourceMaySwitch.Checked;
end;

procedure TForm1.btnSaveGraphsClick(Sender: TObject);
var
  i, n: Integer;
  ALine: TEzGraphLine;
  APoint: TEzAvailabilityPoint;
  ResourceName: string;

begin
  // First we clean-up existing resource and availability data from the database.
  Cnn.Execute('delete from ResourceAvailability');
  Cnn.Execute('delete from Resources');

  qrResources.Open;
  qrResourceAvailability.Open;

  // Hide all lines
  for i:=0 to AvailabilityGraphs.Lines.Count-1 do
  begin
    ALine := AvailabilityGraphs.Lines[i];
    ResourceName := cbSelectResource.Items[i];

    // Add resource to database
    qrResources.Insert;
    qrResources['Name'] := ResourceName;
    qrResources['AllowTaskSwitching'] := ALine.Tag;
    qrResources.Post;

    for n:=0 to ALine.Profile.Count-1 do
    begin
      APoint := ALine.Profile[n];
      qrResourceAvailability.Insert;
      qrResourceAvailability['ResourceID'] := qrResources['ResourceID'];
      qrResourceAvailability['Date'] := APoint.DateValue;
      qrResourceAvailability['Units'] := APoint.Units;
      qrResourceAvailability.Post;
    end;
   end;
  qrResources.Close;
  qrResourceAvailability.Close;
  ShowMessage('Data has been saved');
end;

procedure TForm1.TasksNewRecord(DataSet: TDataSet);
begin
  Tasks.FieldByName('Constraint').AsInteger := Ord(cnASAP);
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Profile: TEzAvailabilityProfile;

begin
  Profile := AvailabilityGraphs.Lines[cbSelectResource.ItemIndex].Profile;
  Profile.Add(ProfileDate.Date, StrToFloat(Edit1.Text), GetIntervalFlags);
  ReloadList(ProfileDate.Date);
end;

procedure TForm1.LoadResources;
var
  Task: TEzScheduleTask;
  i, r: integer;
  Resource: TEzResource;
begin
  // Use a task object to parse resource strings.
  Task := TEzScheduleTask.Create;

  Tasks.First;
  while not Tasks.Eof do
  begin
    // Parse resource string
    Task.Resources.Text := Tasks.FieldByName('Resources').AsString;

    for r:=0 to Task.Resources.Count-1 do
    begin
      if not FResources.ResourceExists(Task.Resources[r].Key) then
      begin
        Resource := TEzResource.Create(Task.Resources[r].Key);
        Resource.Name := Resource.Key;
        Resource.Availability := TEzAvailabilityProfile.Create(1);
        FResources.Add(Resource);
      end;
    end;
    Tasks.Next;
  end;
  Task.Destroy;
end;

procedure TForm1.AvailabilityGraphsChanged(Sender: TEzGraph;
  Line: TEzGraphLine; Point: TEzAvailabilityPoint;
  Event: TEzListNotification);
begin
  ReloadList(EzTimebar1.Date);
end;

procedure TForm1.EzScheduler1LoadResourceData(Scheduler: TEzScheduler;
  Resource: TEzResource);
var
  InternalResource: TEzResource;
begin
  InternalResource := FResources.ResourceByKey(Resource.Key);
  Resource.Availability := InternalResource.Availability;
  Resource.OwnsAvailability := False;
  Resource.AllowTaskSwitching := InternalResource.AllowTaskSwitching;
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  d1, d2: TDateTime;

begin
  d1 := StrToDateTime('01-01-1902 16:00');
  d2 := StrToDateTime('02-01-1902 00:00');
  ProjectCalendar.Rules.AddEncoded(d1, d2, itWorking);

  d1 := StrToDateTime('01-01-1902 00:00');
  d2 := StrToDateTime('01-01-1902 02:00');
  ProjectCalendar.Rules.AddEncoded(d1, d2, itWorking);
end;

procedure TForm1.btnAddProfileClick(Sender: TObject);
var
  Graph: TGraphFrame;

begin
  Graph := TGraphFrame.Create(Self);
  Graph.Parent := ScrollBox1;
  Graph.Align := alTop;
  inc(FrameCount);
  Graph.Name := 'GraphFrame' + IntToStr(FrameCount);
  Graph.EzGraph.Timebar := Timebar;
  Graph.Top := 1000;

  // Make selected line visible
  Graph.EzGraph.Lines.Add;
  Graph.EzGraph.Lines[0].Profile := FResources[cbResourceProfiles.ItemIndex].Availability;
  Graph.EzGraph.Lines[0].Visible := True;
end;

procedure TForm1.cbResourceProfilesDropDown(Sender: TObject);
var
  i: integer;

begin
  LoadResources;
  with Sender as TCombobox do
  begin
    Items.Clear;
    for i:=0 to FResources.Count-1 do
      Items.Add(FResources[i].Name);
  end;
end;

procedure TForm1.ScrollBar1Scroll(Sender: TObject; ScrollCode: TScrollCode;
  var ScrollPos: Integer);
begin
  case ScrollCode of
    scLineUp:
      Timebar.Date := AddTicks(Timebar.MajorScale, Timebar.MajorCount, Timebar.Date, -1);
    scLineDown:
      Timebar.Date := AddTicks(Timebar.MajorScale, Timebar.MajorCount, Timebar.Date, 1);
  end;
end;

end.

