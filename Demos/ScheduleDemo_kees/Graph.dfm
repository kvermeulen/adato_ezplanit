object GraphFrame: TGraphFrame
  Left = 0
  Top = 0
  Width = 695
  Height = 115
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 695
    Height = 115
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object EzGraph: TEzGraph
      Left = 1
      Top = 1
      Width = 693
      Height = 113
      BorderStyle = bsNone
      Lines = <
        item
          Description = 'Resource'
          CanEdit = True
          Pen.Color = clRed
          Pen.Width = 2
          Brush.Style = bsClear
          Tag = 0
        end>
      MaxDate = 37960.000000000000000000
      ScrollbarOptions.ScrollBars = ssNone
      VSnapSize = 0.250000000000000000
      Xax.Position = 83
      Yax.Color = clBtnFace
      Yax.Font.Charset = DEFAULT_CHARSET
      Yax.Font.Color = clWindowText
      Yax.Font.Height = -11
      Yax.Font.Name = 'MS Sans Serif'
      Yax.Font.Style = []
      Align = alClient
    end
  end
end
