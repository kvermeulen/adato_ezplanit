unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, EzDataset, EzGantt, EzTimeBar, Grids, DBGrids, EzDBGrid,
  ExtCtrls, StdCtrls, ImgList, EzCalendar, EzDateTime, EzScheduler,
  EzDurationField, ComCtrls;

type
  TForm1 = class(TForm)
    qrTasks: TADODataSet;
    EzDataset1: TEzDataset;
    dsTasks: TDataSource;
    EzDatasource: TDataSource;
    Panel2: TPanel;
    EzTimebar1: TEzTimebar;
    EzGanttChart1: TEzGanttChart;
    Panel3: TPanel;
    Button1: TButton;
    qrResources: TADODataSet;
    dsResources: TDataSource;
    ilButtonImages: TImageList;
    scGridSchemes: TEzTreeGridSchemes;
    imgEndPoints: TImageList;
    eplEndPoints: TEzEndPointList;
    EzGrid: TEzDBTreeGrid;
    tssGlobalScales: TEzTimescaleStorage;
    Splitter1: TSplitter;
    btnAddTask: TButton;
    qrTaskResources: TADODataSet;
    EzDataset1idTask: TAutoIncField;
    EzDataset1idParent: TIntegerField;
    EzDataset1taDesc: TWideStringField;
    EzDataset1taStart: TWideStringField;
    EzDataset1taStop: TWideStringField;
    Panel1: TPanel;
    dsTaskResources: TDataSource;
    EzDBCoolGrid1: TEzDBCoolGrid;
    EzDBCoolGrid2: TEzDBCoolGrid;
    qrTaskResourcesidTask: TIntegerField;
    qrTaskResourcesidResource: TIntegerField;
    qrTaskResourceslkResource: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    Button2: TButton;
    Button3: TButton;
    qrResourcesidResource: TAutoIncField;
    qrResourcesName: TWideStringField;
    Button4: TButton;
    ADOConnection1: TADOConnection;
    Button5: TButton;
    Button6: TButton;
    rbUseResourceProfile: TRadioButton;
    rbUseScheduler: TRadioButton;
    EzScheduler1: TEzScheduler;
    EzDataset1Work: TEzDurationField;
    Panel4: TPanel;
    lvScheduleMessages: TListView;
    qrTasksidTask: TAutoIncField;
    qrTasksidParent: TIntegerField;
    qrTaskstaDesc: TWideStringField;
    qrTaskstaStart: TWideStringField;
    qrTaskstaStop: TWideStringField;
    qrTaskstaWork: TFloatField;
    btnReschedule: TButton;
    procedure Button1Click(Sender: TObject);
    procedure EzDataset1AfterScroll(DataSet: TDataSet);
    procedure btnAddTaskClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EzGanttChart1GetBarProps(Sender: TObject;
      const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
    procedure Button6Click(Sender: TObject);
    procedure rbUseSchedulerClick(Sender: TObject);
    procedure rbUseResourceProfileClick(Sender: TObject);
    procedure EzScheduler1LoadResourceData(Scheduler: TEzScheduler;
      Resource: TEzResource);
    procedure btnRescheduleClick(Sender: TObject);
    procedure EzDataset1AfterPost(DataSet: TDataSet);
  private
    ProfileCache: TStringList;
    FUpdateCount: Integer;

    { Private declarations }
  public
    // General purpose procedures
    procedure ScrollToTask;
    function  LoadResourceProfile(idResource: Integer): TEzAvailabilityProfile;
    procedure Reset;

    //
    // Method 2 procedures
    // These methods use the scheduler to get working periods
    //
    procedure GetBarPropsFromResourceProfile(Sender: TObject;
      const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
    function  LoadScheduleProfile: TEzAvailabilityProfile;

    //
    // Method 2 procedures
    // These methods use the scheduler to get working periods
    //
    procedure ClearScheduleMessages;
    procedure GetBarPropsFromScheduler(Sender: TObject;
      const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
    procedure LoadScheduleMessages;
    procedure Reschedule;
  end;

  procedure LoadAvailability(Profile: TEzAvailabilityProfile; Cnn: TADOConnection; idResource: Integer);
  procedure LoadResourceCalendar(idResource: Integer; Rules: TEzCalendarRules; Cnn: TAdoConnection);
  procedure SaveAvailability(Profile: TEzAvailabilityProfile; Cnn: TADOConnection; idResource: Integer);
  procedure SaveResourceCalendar(idResource: Integer; Rules: TEzCalendarRules; Cnn: TAdoConnection);

var
  Form1: TForm1;

implementation

uses Variants, Math, Calendar, ResourceAvailability;

{$R *.DFM}

procedure LoadAvailability(Profile: TEzAvailabilityProfile; Cnn: TADOConnection; idResource: Integer);

  //
  // Load contents of table Availability into a TEzAvailabilityProfile object
  //

begin
  Profile.Clear;

  with TADOQuery.Create(nil) do
  try
    Connection := Cnn;
    Sql.Add('select avDate, avUnits ');
    Sql.Add('from ResourceProfile where avidResource=' + IntToStr(idResource));
    Open;
    while not Eof do
    begin
      Profile.Add(Fields[0].AsDateTime, Fields[1].AsFloat);
      Next;
    end;
  finally
    Destroy;
  end;
end;

procedure LoadResourceCalendar(idResource: Integer; Rules: TEzCalendarRules; Cnn: TAdoConnection);

  //
  // Load contents of table CalendarRules into a TEzCalendarRules object
  //
  // Rules: Holds the object to load the date into
  // idResource: Unique ID of Resource

var
  Rule: TEzCalendarRule;

begin
  Rules.Clear;
  with TADOQuery.Create(nil) do
  try
    Connection := Cnn;
    Sql.Add('select idResourceRule, crStart, crStop, crIsWorking, crCount, crRangeStart, crRangeEnd, crOccurrences ');
    Sql.Add('from ResourceRules where cridResource=' + IntToStr(idResource));
    Open;

    while not Eof do
    begin
      Rule := TEzCalendarRule.Create;
      Rule.Tag := Fields[0].AsInteger;
      Rule.Start := Fields[1].AsDateTime;
      Rule.Stop := Fields[2].AsDateTime;
      Rule.RuleType := DecodeRuleType(Rule.Start);
      if FieldByName('crIsWorking').AsBoolean then
        Rule.IntervalType := itWorking else
        Rule.IntervalType := itNonWorking;
      Rule.Count := Fields[4].AsInteger;
      Rule.RangeStart := Fields[5].AsDateTime;
      Rule.RangeEnd := Fields[6].AsDateTime;
      Rule.Occurrences := Fields[7].AsInteger;
      Rules.Add(Rule);
      Next;
    end;
  finally
    Destroy;
  end;
end;

procedure SaveAvailability(Profile: TEzAvailabilityProfile; Cnn: TADOConnection; idResource: Integer);

  //
  // Saves availability data stored in a TEzAvailabilityProfile object into
  // table Availability
  //
var
  AvPoint: TEzAvailabilityPoint;
  i: Integer;

begin
  with TADOQuery.Create(nil) do
  try
    LockType := ltBatchOptimistic;
    Connection := Cnn;
    Cnn.BeginTrans;
    try
      //
      // Remove existing data from database
      //
      Sql.Add('delete from ResourceProfile where avidResource=' + IntToStr(idResource));
      ExecSql;
      Sql.Clear;

      //
      // Add new data to database
      //
      Sql.Add('select avidResource, avDate, avUnits ');
      Sql.Add('from ResourceProfile where avidResource=0');
      Open;

      for i:=0 to Profile.Count-1 do
      begin
        AvPoint := Profile[i];
        AppendRecord([idResource, EzDatetimeToDatetime(AvPoint.DateValue), AvPoint.Units]);
      end;

      UpdateBatch;
      Cnn.CommitTrans;
    except
      CancelBatch;
      Cnn.RollbackTrans;
    end;
  finally
    Destroy;
  end;
end;

procedure SaveResourceCalendar(idResource: Integer; Rules: TEzCalendarRules; Cnn: TAdoConnection);

  //
  // Load contents of table CalendarRules into a TEzCalendarRules object
  //
  // Rules: Holds the object to load the date into
  // idResource: Unique ID of Resource

var
  i: Integer;
  Rule: TEzCalendarRule;

begin
  with TADOQuery.Create(nil) do
  try
    Connection := Cnn;
    // Delete any exisiting data
    Cnn.Execute('delete from ResourceRules where cridResource=' + IntToStr(idResource));

    // Open empty recordset
    Sql.Add('select cridResource, crStart, crStop, crIsWorking, crCount, crRangeStart, crRangeEnd, crOccurrences ');
    Sql.Add('from ResourceRules where cridResource=0');
    Open;

    for i:=0 to Rules.Count-1 do
    begin
      Rule := Rules[i];

      Insert;
      Fields[0].AsInteger := idResource;
      Fields[1].AsDateTime := Rule.Start;
      Fields[2].AsDateTime := Rule.Stop;
      if Rule.IntervalType = itWorking then
        FieldByName('crIsWorking').AsBoolean := True else
        FieldByName('crIsWorking').AsBoolean := False;

      Fields[4].AsInteger := Rule.Count;
      Fields[5].AsDateTime := Rule.RangeStart;
      Fields[6].AsDateTime := Rule.RangeEnd;
      Fields[7].AsInteger := Rule.Occurrences;
      Post;
    end;
  finally
    Destroy;
  end;
end;

procedure TForm1.rbUseSchedulerClick(Sender: TObject);
begin
  btnReschedule.Enabled := True;
  Reset;
end;

procedure TForm1.rbUseResourceProfileClick(Sender: TObject);
begin
  btnReschedule.Enabled := False;
  Reset;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  EzDataset1.Open;
  qrResources.Open;
  qrTaskResources.Open;
end;

procedure TForm1.EzDataset1AfterScroll(DataSet: TDataSet);
begin
  // Scroll current task into view
  ScrollToTask;
end;

//
// This procedure scrolls the Gantt chart in such a way that the taskbar,
// drawn for the current record, will be visible. This method is called
// automatically when AutoTaskScroll is set to True.
//
procedure TForm1.ScrollToTask;
var
  Start, Stop, EndDate, Current: TDateTime;
begin
  if not EzDataset1.FieldByName('taStart').IsNull then
    Start := EzDataset1.FieldByName('taStart').AsDateTime else
    Start := 0;

  if not EzDataset1.FieldByName('taStop').IsNull then
    Stop := EzDataset1.FieldByName('taStop').AsDateTime else
    Stop := 0;

  with EzGanttChart1.Timebar do
  begin
    Current := Date;
    EndDate := X2DateTime(Width);

    if (Start <> 0) and (Start < Current) then
      Date := Start
    else if (Stop <> 0) and (Stop > EndDate) then
    begin
      if Start-Current < Stop-EndDate then
        Date := Start else
        Date := AddTicks(MajorScale, MajorCount, Date + Stop-EndDate, 1);
    end;
  end;
end;

procedure TForm1.btnAddTaskClick(Sender: TObject);
begin
  if EzDataset1.Level=0 then
    EzDataset1.AddChild
  else
    EzDataset1.Add;
  ActiveControl := EzGrid;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  qrTaskResources.AppendRecord([
      EzDataset1.FieldByName('idTask').AsInteger,
      qrResources.FieldByName('idResource').AsInteger]);
  Reset;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  qrTaskResources.Delete;
  Reset;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  LoadResourceCalendar(
      qrResources.FieldByName('idResource').AsInteger,
      frmCalendar.EzCalendarPanel1.Rules,
      ADOConnection1);
  if frmCalendar.ShowModal=idOK then
  begin
    SaveResourceCalendar(
        qrResources.FieldByName('idResource').AsInteger,
        frmCalendar.EzCalendarPanel1.Rules,
        ADOConnection1);
    Reset;
  end;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  with TfrmAvailability.Create(Self) do
  try
    idResource := qrResources.FieldByName('idResource').AsInteger;
    Open;
    if ShowModal=idOK then
      Reset;
  finally
    Free;
  end;
end;

//
// This method returns an combined availability profile which
// is the created by loading a profile for every resource assigned to the
// current task and then merging these profiles into the resulting profile
// using the 'and' operator.
//
function TForm1.LoadScheduleProfile: TEzAvailabilityProfile;
begin
  Result := nil;

  // Update range
  qrTaskResources.Close;
  qrTaskResources.Parameters.ParamValues['idTask'] := EzDataset1['idTask'];
  qrTaskResources.Open;

  if qrTaskResources.RecordCount=0 then
    Exit;

  // Nothing special, simply return the profile of this resource
  if qrTaskResources.RecordCount=1 then
    Result := LoadResourceProfile(qrTaskResources.FieldByName('idResource').AsInteger)

  else
  begin
    Result := TEzAvailabilityProfile.Create(1);
    Result.Operator := opAnd;

    qrTaskResources.DisableControls;
    try
      qrTaskResources.First;
      while not qrTaskResources.Eof do
      begin
        Result.AddSource(LoadResourceProfile(qrTaskResources.FieldByName('idResource').AsInteger));
        qrTaskResources.Next;
      end;
    finally
      qrTaskResources.EnableControls;
    end;
  end;
end;

function TForm1.LoadResourceProfile(idResource: Integer): TEzAvailabilityProfile;
var
  Rules: TEzCalendarRules;
  BaseProfile: TEzAvailabilityProfile;
  i: Integer;

begin
  // Locate profile in the cache
  i := ProfileCache.IndexOf(IntToStr(idResource));
  if i<>-1 then
  begin
    Result := TEzAvailabilityProfile(ProfileCache.Objects[i]);
    Exit;
  end;

  //
  // Load calendar rules for this resource
  //
  Rules := TEzCalendarRules.Create;
  LoadResourceCalendar(idResource, Rules, ADOConnection1);

  //
  // Load availability profile for this resource
  //
  BaseProfile := TEzAvailabilityProfile.Create(1);
  LoadAvailability(BaseProfile, ADOConnection1, idResource);

  //
  // Create a destination profile
  //
  Result := TEzAvailabilityProfile.Create(0);

  //
  // Assign calendar rules and availability profile to the destination profile.
  // The destination profile will merge the rules with the base availability
  // profile into one availability profile.
  //
  Result.Rules := Rules;

  // Rules must be freed automatically
  Result.OwnsRules := True;
  Result.AddSource(BaseProfile);

  // Sources must be freed automatically
  Result.OwnsSources := True;

  // Add profile to the cache
  ProfileCache.AddObject(IntToStr(idResource), Result);
end;

procedure TForm1.Reset;
var
  i: Integer;

begin
  if FUpdateCount>0 then Exit;

  // Unload resource profiles from cache
  for i:=0 to ProfileCache.Count-1 do
    ProfileCache.Objects[i].Free;

  ProfileCache.Clear;

  // Are we using the scheduler?
  if rbUseScheduler.Checked then
  begin
    EzScheduler1.Reset;
    Reschedule;
  end;

  // Reset gantt chart, data will be reloaded during paint operation
  EzGanttChart1.DrawInfo.ScreenCache.ClearBars;
  EzGanttChart1.Invalidate;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ProfileCache := TStringList.Create;
end;

procedure TForm1.EzGanttChart1GetBarProps(Sender: TObject;
  const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
begin
  if rbUseResourceProfile.Checked then
    GetBarPropsFromResourceProfile(Sender, RowNode, BarInfo, Show) else
    GetBarPropsFromScheduler(Sender, RowNode, BarInfo, Show)
end;

procedure TForm1.GetBarPropsFromResourceProfile(Sender: TObject;
  const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
var
  Profile: TEzAvailabilityProfile;
  Point: TEzAvailabilityPoint;
  i_point: Integer;
  IntervalStart, IntervalStop, EndDate: TEzDateTime;
  ScreenBar: TScreenBar;

begin
  // Get a merged profile for all resources attached to current task
  Profile := LoadScheduleProfile;
  if Profile<>nil then
    //
    // Add gantt bars for each working interval between task start and task stop
    //
  begin
    // Make sure availability profile is prepared over the
    // requested interval
    Profile.PrepareDateRange(MakeTimespan(BarInfo.BarStart, BarInfo.BarStop));

    // Locate datapoint in graph near BarInfo.BarStart
    if not Profile.IndexOf(i_point, DateTimeToEzDateTime(BarInfo.BarStart)) then
      dec(i_point);

    EndDate := 0;
    Point := Profile.Items[i_point];
    while EndDate < DateTimeToEzDateTime(BarInfo.BarStop) do
    begin
      if Point.Units>0 then
      begin
        IntervalStart := max(Point.DateValue, DateTimeToEzDateTime(BarInfo.BarStart));
        if i_point < Profile.Count-1 then
          EndDate := Profile.Items[i_point+1].DateValue else
          EndDate := END_OF_TIME;
        IntervalStop := min(EndDate, DateTimeToEzDateTime(BarInfo.BarStop));

        if IntervalStart<IntervalStop then
        begin
          ScreenBar := TScreenBar.Create;
          // Assign visual properties
          ScreenBar.BarProps.Assign((Sender as TEzGanttChart).Ganttbars[1]);
          ScreenBar.BarStart := EzDateTimeToDateTime(IntervalStart);
          ScreenBar.BarStop := EzDateTimeToDateTime(IntervalStop);
          // Make sure bar is displayed on top of task bar
          ScreenBar.BarProps.Z_Index := BarInfo.BarProps.Z_Index+1;
          // Add new bar to the screencache
          (Sender as TEzGanttChart).DrawInfo.ScreenCache[BarInfo.Row].Add(ScreenBar);
        end;
      end else
        EndDate := Point.DateValue;

      if i_point < Profile.Count-1 then
      begin
        inc(i_point);
        Point := Profile.Items[i_point];
      end;
    end;

  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  with TfrmAvailability.Create(Self) do
  try
    EzGraph.Lines[1].Profile := LoadScheduleProfile;
    ShowModal;
  finally
    Free;
  end;
end;

// *****************************************************************************
//
// Use scheduler to retrieve the actual working periods
//
// *****************************************************************************
procedure TForm1.ClearScheduleMessages;
var
  i: Integer;

begin
  for i:=0 to lvScheduleMessages.Items.Count-1 do
    lvScheduleMessages.Items[i].SubItems.Objects[0].Free;

  lvScheduleMessages.Items.Clear;
end;


//
// This method adds virtual gantt bars to the gantt chart to indicate the
// actual working periods for a task. The actual working periods are loaded
// from the scheduler component.
//
// To retrieve the actual working periods, TEzScheduler.UseWorkingPeriods must
// be set to True and scheduler must be run in advance. Then the working periods
// can be loaded from ....
//
procedure TForm1.GetBarPropsFromScheduler(Sender: TObject;
  const RowNode: TRecordNode; BarInfo: TScreenbar; var Show: Boolean);
var
  Key: TNodeKey;
  Task: TEzScheduleTask;
  i: Integer;
  ScreenBar: TScreenBar;

begin
  // There is a problem with scheduling,
  // Data cannot be retrieved
  if (srScheduledFailed in EzScheduler1.ScheduleResults) then
    Exit;

  if (srScheduledOK in EzScheduler1.ScheduleResults) then
  begin
    Key.Level := -1;
    Key.Value := EzDataset1['idTask'];
    Task := EzScheduler1.FindTask(Key);
    Assert(Task<>nil);

    if Task.Intervals.Count>0 then
      for i:=0 to Task.Intervals[0].WorkingPeriods.Count-1 do
      begin
        ScreenBar := TScreenBar.Create;
        // Assign visual properties
        ScreenBar.BarProps.Assign(EzGanttChart1.Ganttbars[1]);
        ScreenBar.BarStart := EzDateTimeToDateTime(Task.Intervals[0].WorkingPeriods[i].Start);
        ScreenBar.BarStop := EzDateTimeToDateTime(Task.Intervals[0].WorkingPeriods[i].Stop);
        ScreenBar.BarProps.Z_Index := BarInfo.BarProps.Z_Index+1;
        EzGanttChart1.DrawInfo.ScreenCache[BarInfo.Row].Add(ScreenBar);
      end;
  end;
end;

procedure TForm1.LoadScheduleMessages;
var
  i: Integer;
  AMessage, AMessageCopy: TEzScheduleMessage;
  li: TListItem;

begin
  // Display any/other schedule messages.
  for i:=0 to EzScheduler1.Messages.Count-1 do
  begin
    li := lvScheduleMessages.Items.Add;
    li.Caption := '';
    AMessage := EzScheduler1.Messages[i];
    li.SubItems.Add(AMessage.TaskKey.Value);
    li.SubItems.Add(AMessage.MessageText);

    // Keep a copy of the schedule error
    AMessageCopy := TEzScheduleMessage.Create;
    AMessageCopy.Assign(AMessage);
    li.SubItems.Objects[0] := AMessageCopy;
  end;
end;

procedure TForm1.Reschedule;
var
  Task: TEzScheduleTask;
  Int: TEzScheduleInterval;

  // Attach resources to schedule task
  procedure AddResources;
  var
    R: TEzResourceAssignment;

  begin
    qrTaskResources.Close;
    qrTaskResources.Parameters.ParamValues['idTask'] := Task.Key.Value;
    qrTaskResources.Open;

    while not qrTaskResources.Eof do
    begin
      R := TEzResourceAssignment.Create;
      R.Key := qrTaskResources['idResource'];
      // We use fixed assignements,
      // in real applications this should be a variable (see EzProject).
      R.Assigned := 1.0;

      // Add resource to task
      Task.Resources.Add(R);
      qrTaskResources.Next;
    end;
  end;

  procedure LoadScheduleData;
  var
    Duration: Double;

  begin
    EzDataset1.ActiveCursor.MoveFirst;
    while not EzDataset1.ActiveCursor.Eof do
      //
      // Load all tasks into the EzScheduler1
      //
    begin
      Task := TEzScheduleTask.Create;
      Task.Key := EzDataset1.ActiveCursor.CurrentNode.Key;
      if EzDataset1.ActiveCursor.CurrentNode.Parent <> nil then
        Task.Parent := EzDataset1.ActiveCursor.CurrentNode.Parent.Key else
        Task.Parent.Value := Null;

      // In this sample we don't use:
      //  Constraints
      //  Priorities
      //  Task.Constraint := TEzConstraint(EzDataset1.FieldByName('ScheduleConstraint').AsInteger);
      //  Task.ConstraintDate := EzDataset1.FieldByName('ScheduleConstraintDate').AsDateTime;
      //  Task.Priority := EzDataset1.FieldByName('taidPriority').AsInteger;

      AddResources;

      Duration := EzDataset1.FieldByName('taWork').AsFloat;
      if Duration>0 then
        // Add a schedule interval
      begin
        Int := TEzScheduleInterval.Create;
        Int.Start := EzDataset1.FieldByName('taStart').AsDateTime;
        Int.Stop := EzDataset1.FieldByName('taStop').AsDateTime;
        Int.Duration := Duration;
        Task.Intervals.Add(Int);
      end;

      // Load task into EzScheduler1
      EzScheduler1.AddTask(Task);

      // Go to next record
      EzDataset1.ActiveCursor.MoveNext;
    end;
  end;

  procedure SaveData;
  begin
    // Prevent recursive calls to Reset method
    inc(FUpdateCount);
    try
      Task := nil;
      while EzScheduler1.GetNextScheduledTask(Task) do
      begin
        EzDataset1.RecordNode := EzDataset1.ActiveCursor.FindNode(Task.Key);
        EzDataset1.Edit;
        EzDataset1['taStart'] := Task.Intervals[0].Start;
        EzDataset1['taStop'] := Task.Intervals[0].Stop;
        EzDataset1.Post;
      end;
    finally
      dec(FUpdateCount);
    end;
  end;


begin
  EzDataset1.StartDirectAccess([cpShowAll], True);
  try
    qrTaskResources.DisableControls;
    try
      if EzDataset1.State in [dsEdit, dsInsert] then
        EzDataset1.Post;

      // Prepare EzEzScheduler11 for next run
      EzScheduler1.Reset;

      ClearScheduleMessages;

      // ProjectWindow holds the project schedule window.
      // That is the start and stop dates in which the project must be completed.
      EzScheduler1.ProjectWindowStart := Trunc(Now);
      EzScheduler1.ProjectWindowStop := Trunc(Now)+365;

      // Set project calendar,
      // Create calendar with 24 hours availability
      EzScheduler1.ProjectProfile := TEzAvailabilityProfile.Create(1);

      // Import data in EzEzScheduler11 component
      LoadScheduleData;

      //
      // Reschedule project
      //
      EzScheduler1.Reschedule;

      if srScheduledOK in EzScheduler1.ScheduleResults then
        SaveData;

      LoadScheduleMessages;
    finally
      qrTaskResources.EnableControls;
    end;
  finally
    EzDataset1.EndDirectAccess;
  end;
end;

procedure TForm1.EzScheduler1LoadResourceData(Scheduler: TEzScheduler;
  Resource: TEzResource);
begin
  Resource.Availability := LoadResourceProfile(Resource.Key);
  Resource.OwnsAvailability := False;
end;

procedure TForm1.btnRescheduleClick(Sender: TObject);
begin
  btnReschedule.Enabled := True;
  Reset;
end;

procedure TForm1.EzDataset1AfterPost(DataSet: TDataSet);
begin
  Reset;
end;

end.
