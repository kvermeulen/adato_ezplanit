program WorkingPeriods;

uses
  Forms,
  Main in 'Main.pas' {Form1},
  Calendar in 'Calendar.pas' {frmCalendar},
  ResourceAvailability in 'ResourceAvailability.pas' {frmAvailability};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TfrmCalendar, frmCalendar);
  Application.Run;
end.
