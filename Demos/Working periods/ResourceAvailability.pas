unit ResourceAvailability;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EzTimeBar, EzGraph, ExtCtrls, StdCtrls, EzCalendar, EzArrays, Buttons,
  ComCtrls;

resourcestring
  SNoMoreData = 'No more data.';

type
  TfrmAvailability = class(TForm)
    pnlGraph: TPanel;
    EzGraph: TEzGraph;
    Panel2: TPanel;
    EzTimebar: TEzTimebar;
    Panel3: TPanel;
    Panel4: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    Panel7: TPanel;
    Label1: TLabel;
    Panel5: TPanel;
    dtpDateTime: TDateTimePicker;
    btnScrollPrev: TSpeedButton;
    btnScrollNext: TSpeedButton;
    chkSnap: TCheckBox;
    Scales: TEzTimescaleStorage;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EzGraphChanged(Sender: TEzGraph; Line: TEzGraphLine;
      Point: TEzAvailabilityPoint; Event: TEzListNotification);
    procedure EzTimebarDateChange(Sender: TObject);
    procedure dtpDateTimeChange(Sender: TObject);
    procedure btnScrollPrevClick(Sender: TObject);
    procedure btnScrollNextClick(Sender: TObject);
    procedure chkSnapClick(Sender: TObject);
  private

  protected
    FIdResource: Integer;
    FDataChanged: Boolean;
    FRules: TEzCalendarRules;

  public
    procedure Open;

    property DataChanged: Boolean read FDataChanged;
    property idResource: Integer read FIdResource write FIdResource;
  end;

implementation

uses Main;

{$R *.DFM}

procedure TfrmAvailability.FormCreate(Sender: TObject);
begin
  FRules := TEzCalendarRules.Create;

  EzTimebar.Date := Now;

  EzGraph.MinDate := EzTimebar.Date;
  EzGraph.MaxDate := EzTimebar.Date + 10;
end;

procedure TfrmAvailability.FormDestroy(Sender: TObject);
begin
  FRules.Destroy;
end;

procedure TfrmAvailability.Open;
begin
  // Setup profile 1:
  // Profile 1 displays an editable availability profile that marks the
  // number of resources available at a certain time.

  // Set default availability to 1
  EzGraph.Lines[1].Profile.DefaultAllocation := 1;
  LoadAvailability(EzGraph.Lines[1].Profile, Form1.qrResources.Connection, idResource);

  // Setup profile 0:
  // Profile 0 displays the availability profile which is the result of merging
  // profile 1 with the calendar rules.
  LoadResourceCalendar(idResource, FRules, Form1.qrResources.Connection);

  EzGraph.Lines[0].Profile.Rules := FRules;
  EzGraph.Lines[0].Profile.AddSource(EzGraph.Lines[1].Profile);

  // We currently do not support the DataChanged flag. Setting it to True will
  // allways update data!
  FDataChanged := True;
end;

procedure TfrmAvailability.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if DataChanged and (ModalResult = mrOK) then
    SaveAvailability(EzGraph.Lines[1].Profile, Form1.ADOConnection1, idResource);
end;

procedure TfrmAvailability.EzGraphChanged(Sender: TEzGraph;
  Line: TEzGraphLine; Point: TEzAvailabilityPoint;
  Event: TEzListNotification);
begin
  FDataChanged := True;
end;

procedure TfrmAvailability.EzTimebarDateChange(Sender: TObject);
begin
  dtpDateTime.Date := EzTimebar.Date;
end;

procedure TfrmAvailability.dtpDateTimeChange(Sender: TObject);
begin
  EzTimebar.Date := dtpDateTime.Date;
end;

procedure TfrmAvailability.btnScrollPrevClick(Sender: TObject);
var
  Profile: TEzAvailabilityProfile;
  i: Integer;

begin
  Profile := EzGraph.Lines[1].Profile; // = availability line
  Profile.IndexOf(i, EzTimebar.Date);
  if i=1 then
    MessageDlg(SNoMoreData, mtInformation, [mbOK], 0) else
    EzTimebar.Date := Profile[i-1].DateValue;
end;

procedure TfrmAvailability.btnScrollNextClick(Sender: TObject);
var
  Profile: TEzAvailabilityProfile;
  i: Integer;
  ADate: TDateTime;

begin
  Profile := EzGraph.Lines[1].Profile; // = availability line
  Profile.IndexOf(i, EzTimebar.Date);

  if i=Profile.Count then
    MessageDlg(SNoMoreData, mtInformation, [mbOK], 0)
  else
  begin
    ADate := EzTimebar.Date;
    // Keep moving to the next point till the truncated datevalue
    // is different
    while (i<Profile.Count) and (ADate=EzTimebar.Date) do
    begin
      ADate := TruncDate(EzTimebar.MajorScale, EzTimebar.MajorCount, EzTimebar.MajorOffset, Profile[i].DateValue, EzTimebar.MajorWeekStart);
      inc(i);
    end;

    if ADate=EzTimebar.Date then
      MessageDlg(SNoMoreData, mtInformation, [mbOK], 0) else
      EzTimebar.Date := ADate;
  end;
end;

procedure TfrmAvailability.chkSnapClick(Sender: TObject);
begin
  if chkSnap.Checked then
  begin
    EzGraph.SnapScale := EzTimebar.MajorScale;
    EzGraph.SnapCount := EzTimebar.MajorCount;
  end else
    EzGraph.SnapSize := '';
end;

end.
