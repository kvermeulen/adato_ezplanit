object frmCalendar: TfrmCalendar
  Left = 283
  Top = 180
  Caption = 'Resource calendar'
  ClientHeight = 360
  ClientWidth = 854
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object EzCalendarPanel1: TEzCalendarPanel
    Left = 0
    Top = 0
    Width = 854
    Height = 319
    Align = alClient
    BorderWidth = 0
    BorderStyle = bsNone
    CalendarColor = clWindow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    RulesColor = clWindow
    OldCreateOrder = False
    IsControl = True
  end
  object Panel1: TPanel
    Left = 0
    Top = 319
    Width = 854
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 656
      Top = 8
      Width = 99
      Height = 25
      Caption = '&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 760
      Top = 8
      Width = 99
      Height = 25
      Caption = '&Cancel'
      ModalResult = 1
      TabOrder = 1
    end
  end
end
