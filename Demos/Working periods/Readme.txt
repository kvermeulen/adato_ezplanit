
This file describes the working periods demo application.

Working periods demo
====================

This demo shows how you can retrieve the actual working periods for a task
and how to display them in the gantt chart.

There are two methods you can use to implement this:

1) Resource profiles
   When adding ganttbars to the screencache, you insert additional bars
   indicating the actual working periods for the resource(s). These periods
   can be retrieved from the resource profile.

2) Scheduler
   You can the extract the exact schedule data from the scheduler
   (the actual working intervals allocated per task) and use this data to
   add gantt bars to the screen cache.

Both methods are implemented in this sample application.

Using resource profiles
=======================

This method retrieves the actual working periods from the resource profile.
This profile uses both calendar rules and availability to create a graph
indicating when a resource is working or not. In case multiple resources are
assigned to the same task, a combined profile is used which has multiple
profiles as it's source.

Loading the resource profile
----------------------------

To learn how to load a resource profile, take a look at method
LoadResourceProfile in Main.pas. This method first loads the calendar
rules into a TEzCalendar object, then loads the availability graph into a
TEzAvailabilityProfile and finally merges these two components into a single
TEzAvailabilityProfile to get the real availability for this resource.

Rules are used to tell the profile when a resource is working in an
easy manner like, for example:

  - A resource is not working on sunday
  - January 1st is a non working day

Rules can be edited using a TEzCalendarPanel. A sample on how to use this panel
is included with this application (see file Calendar.pas) and can be opened
by pressing button 'Edit calendar'.

Resource availability is implemented by class TEzAvailablityProfile. An
availability profile holds the information about the number of resources
working at any time, this in contrast with the calendar rules which tell when
a resource is working.

Availability profiles can be edited using a TEzGraph component. This is also
included with this sample (see file ResourceAvailability.pas). The availability
editor opens after pressing 'Edit profile' button.

Loading a profile for multiple resources
----------------------------------------

A task can have multiple resources assigned and to get the actual working
periods for these resources requires merging multiple profiles into one profile.
This can be done by allocating a new TEzAvailablityProfile which has each
resource profile added to the source list. The resulting profile then uses
the 'and' operator to merge the sources and as a result, the graph will only
show availability (Units>0) when all resources are available.

This is demonstrated in method LoadScheduleProfile (Main.pas) which returns
the availability profile for the resources assigned to the current task.
You can open this profile by pressing button 'Show profile'.

Adding virtual gantt bars to the gantt chart
--------------------------------------------

Once you have the right availability profile, you need to add dummy gantt bars
to the gantt chart. The best place for doing this is in event handler
TEzGanttChart.GetBarProps.

Event GetBarProps is fired by the gantt chart whenever a gantt bar is added
to the screen cache and enables you to change the visual properties of the
bar to be added or to add additional bars at will. Adding new bars requires code
similar to this:

          ScreenBar := TScreenBar.Create;
          // Assign visual properties
          ScreenBar.BarProps.Assign(EzGanttChart1.Ganttbars[1]);
          ScreenBar.BarStart := EzDateTimeToDateTime(IntervalStart);
          ScreenBar.BarStop := EzDateTimeToDateTime(IntervalStop);
          ScreenBar.BarProps.Z_Index := BarInfo.BarProps.Z_Index+1;
          // Add new bar to the screencache
          EzGanttChart1.DrawInfo.ScreenCache[BarInfo.Row].Add(ScreenBar);

Bars should be added for each working period between the task start and stop
date. This is demonstrated in method GetBarPropsFromResourceProfile in Main.pas.

Using the scheduler
===================

Instead of getting the actual working periods directly from the resource
profile, you can also load this data from the scheduler. There is an additional
bonus here because the scheduler data is more accurate in case you allow
resources to work on different tasks simultaneously. It could, for example,
happen that a resource is working on task 1 from monday till friday, but not
on wednessday even though this is a working day for this resource. Because
wednessday is not allocated for this task, the scheduler could decide to
allocate task 2 on this day. When using resource profiles (method 1) you'll
never be able to highlight the right working periods for task 1 because
wednessday will be higlighted as well.

During scheduling, the scheduler keeps track of the exact working periods per
task. However, this information is normally discared once the scheduler
finishes. However, if you set TEzScheduler.UseWorkingPeriods to true, this data
is stored along with each task and can be retrieved using property:

        Task.Intervals[x].WorkingPeriods

So to use the scheduler, requires the following steps:
  - Add a TEzScheduler component to your application
  - Set UseWorkingPeriods to true
  - Implement event TEzScheduler.OnLoadResourceData
    This event must return the resource availability profile per resource.
  - Add a Reschedule method to the application
    This method must load all tasks into the scheduler then reschedule the
    project and then update the database with the new start and stop dates.
  - Update event TEzGanttChart.GetBarProps to load the real working periods
    from the scheduler.

These steps are all demonstrated in this application, see methods:
  - ClearScheduleMessages
    This is a helper method that removes any messages returned from the
    scheduler.
  - LoadScheduleMessages
    This is a helper method that loads all messages from the
    scheduler in case anything went wrong during scheduling.
  - Reschedule
    This method will load all tasks into the schedulerm then reschedule the
    project and then update the database with the new schedule data.
  - GetBarPropsFromScheduler
    This is the updated implementation of event handler
    TEzGanttChart.GetBarProps.

===============
End of document
===============




