unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, EzDataset, EzGantt, EzTimeBar, Grids, DBGrids, EzDBGrid,
  ExtCtrls, StdCtrls, ImgList, DBTables;

type
  TForm1 = class(TForm)
    EzDataset1: TEzDataset;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    Panel1: TPanel;
    EzDBGrid1: TEzDBGrid;
    Panel2: TPanel;
    EzTimebar1: TEzTimebar;
    EzGanttChart1: TEzGanttChart;
    Panel3: TPanel;
    Button1: TButton;
    Splitter1: TSplitter;
    imgEndPoints: TImageList;
    eplEndPoints: TEzEndPointList;
    ilButtonImages: TImageList;
    scGridSchemes: TEzTreeGridSchemes;
    Table1: TTable;
    Table1IdTask: TAutoIncField;
    Table1IdParent: TIntegerField;
    Table1Description: TStringField;
    Table1Startdate: TDateTimeField;
    Table1Enddate: TDateTimeField;
    EzDataset1IdTask: TAutoIncField;
    EzDataset1IdParent: TIntegerField;
    EzDataset1Description: TStringField;
    EzDataset1Startdate: TDateTimeField;
    EzDataset1Enddate: TDateTimeField;
    procedure Button1Click(Sender: TObject);
    procedure EzDataset1AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  EzDataset1.Open;
end;

procedure TForm1.EzDataset1AfterOpen(DataSet: TDataSet);
begin
  // Scroll ganttchart to first task
  if not EzDataset1.Eof and not EzDataset1.FieldByName('Startdate').IsNull then
    EzTimebar1.Date := EzDataset1['Startdate'] else
    EzTimebar1.Date := Now;
end;

end.
