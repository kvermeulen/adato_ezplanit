unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, EzDataset, EzGantt, EzTimeBar, Grids, DBGrids, EzDBGrid,
  ExtCtrls, StdCtrls, ImgList;

type
  TForm1 = class(TForm)
    ADODataSet1: TADODataSet;
    EzDataset1: TEzDataset;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    EzDataset1idTask: TAutoIncField;
    EzDataset1taidParent: TIntegerField;
    EzDataset1taDesc: TWideStringField;
    Panel1: TPanel;
    EzDBGrid1: TEzDBTreeGrid;
    Panel2: TPanel;
    EzTimebar1: TEzTimebar;
    EzGanttChart1: TEzGanttChart;
    Panel3: TPanel;
    Button1: TButton;
    Splitter1: TSplitter;
    imgEndPoints: TImageList;
    eplEndPoints: TEzEndPointList;
    ilButtonImages: TImageList;
    scGridSchemes: TEzTreeGridSchemes;
    EzDataset1taStart: TDateTimeField;
    EzDataset1taStop: TDateTimeField;
    tssGlobalScales: TEzTimescaleStorage;
    procedure Button1Click(Sender: TObject);
    procedure EzDataset1AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
begin
  EzDataset1.Open;
end;

procedure TForm1.EzDataset1AfterOpen(DataSet: TDataSet);
begin
  // Scroll ganttchart to first task
  EzTimebar1.Date := EzDataset1['taStart'];
end;

end.
