//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "EzDataset.hpp"
#include <ADODB.hpp>
#include <DB.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TADODataSet *ADODataSet1;
        TDataSource *DataSource1;
        TEzDataset *EzDataset1;
        TAutoIncField *EzDataset1idTask;
        TIntegerField *EzDataset1taidParent;
        TWideStringField *EzDataset1taDesc;
        TDateTimeField *EzDataset1taStart;
        TDateTimeField *EzDataset1taStop;
        TDataSource *DataSource2;
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
