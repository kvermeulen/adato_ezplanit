object Form1: TForm1
  Left = 192
  Top = 114
  Width = 870
  Height = 640
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ADODataSet1: TADODataSet
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=P:\Delphi\EzPlanIT\' +
      'Demos\Demo1\BCB\BDE\Data.mdb;Persist Security Info=False'
    CommandText = 'select * from Tasks'
    Parameters = <>
    Left = 96
    Top = 184
  end
  object DataSource1: TDataSource
    DataSet = ADODataSet1
    Left = 128
    Top = 184
  end
  object EzDataset1: TEzDataset
    Cursors = <
      item
        CursorName = 'DefaultCursor'
        Options = [cpAutoCreateParents, cpDefExpanded]
      end>
    EzDataLinks = <
      item
        DataSource = DataSource1
        KeyField = 'idTask'
        ParentRefField = 'taidParent'
        SingleNodeFields = ()
        RootNodeFields = (
          (
            'taStart'
            'min(childs(taStart))')
          (
            'taStop'
            'max(childs(taStop))'))
        ParentNodeFields = (
          (
            'taStart'
            'min(childs(taStart))')
          (
            'taStop'
            'max(childs(taStop))'))
        ChildNodeFields = ()
      end>
    Left = 168
    Top = 184
    object EzDataset1idTask: TAutoIncField
      FieldName = 'idTask'
    end
    object EzDataset1taidParent: TIntegerField
      FieldName = 'taidParent'
    end
    object EzDataset1taDesc: TWideStringField
      FieldName = 'taDesc'
      Size = 50
    end
    object EzDataset1taStart: TDateTimeField
      FieldName = 'taStart'
    end
    object EzDataset1taStop: TDateTimeField
      FieldName = 'taStop'
    end
  end
  object DataSource2: TDataSource
    DataSet = EzDataset1
    Left = 200
    Top = 184
  end
end
