unit Main;

interface

uses
  ActiveX, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EzScheduler, EzCalendar, EzDataset, StdCtrls, Grids, ComCtrls;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Button1: TButton;
    EzScheduler1: TEzScheduler;
    edDuration: TEdit;
    edResources: TEdit;
    cbConstraint: TComboBox;
    cnDate: TDateTimePicker;
    cnTime: TDateTimePicker;
    udDuration: TUpDown;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    mmMessages: TMemo;
    lbStart: TLabel;
    lbStop: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure EzScheduler1LoadResourceData(Scheduler: TEzScheduler;
      Resource: TEzResource);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

uses Math;

procedure TForm1.FormCreate(Sender: TObject);
begin
  cbConstraint.ItemIndex := 7;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  i: Integer;
  Task: TEzScheduleTask;
  Int: TEzScheduleInterval;

begin
  mmMessages.Lines.Clear;
  EzScheduler1.Reset;

  EzScheduler1.ProjectWindowStart := Trunc(Now);
  EzScheduler1.ProjectWindowStop := Trunc(Now)+365;

  // Profile having 24 hours availability
  EzScheduler1.ProjectProfile := TEzAvailabilityProfile.Create(1);

  Task := TEzScheduleTask.Create;
  Task.Key.Value := 1; // Unique value identifying this task
  Task.Key.Level := -1;

  // Set task's constraint. Index in combobox corresponds with constraint ID.
  Task.Constraint := TEzConstraint(cbConstraint.ItemIndex);

  // Set tasks constraint date.
  Task.ConstraintDate := Floor(cnDate.DateTime) + Frac(cnTime.DateTime);

  Task.Resources.Text := edResources.Text;

  // Add a schedule interval
  Int := TEzScheduleInterval.Create;
  Int.Duration := udDuration.Position*1/24 {hours};
  Task.Intervals.Add(Int);

  EzScheduler1.AddTask(Task);
  //
  // Run schedule engine
  //
  EzScheduler1.Reschedule;

  if (srScheduledOK in EzScheduler1.ScheduleResults) then
  begin
    if srScheduledWithErrors in EzScheduler1.ScheduleResults then
      mmMessages.Lines.Add('Scheduler ran successfully, however some tasks could not be scheduled.') else
      mmMessages.Lines.Add('Scheduler ran successfully');

    Task := nil;
    if EzScheduler1.GetNextScheduledTask(Task) then
    begin
      lbStart.Caption := DateTimeToStr(Task.Intervals[0].Start);
      lbStop.Caption := DateTimeToStr(Task.Intervals[0].Stop);
    end;
  end else
    mmMessages.Lines.Add('Error in scheduler.');

  for i:=0 to EzScheduler1.Messages.Count-1 do
    mmMessages.Lines.Add(
        string(EzScheduler1.Messages[i].TaskKey.Value) + ' - ' +
        EzScheduler1.Messages[i].MessageText);
end;

procedure TForm1.EzScheduler1LoadResourceData(Scheduler: TEzScheduler;
  Resource: TEzResource);
var
  Rules: TEzCalendarRules;

begin
  //
  // Create a destination profile
  //
  Resource.Availability := TEzAvailabilityProfile.Create(1 {100% available});

  // Create rules object
  Rules := TEzCalendarRules.Create;

  // Set working hours between 8:00 and 12:00
  Rules.Add(8/24, 12/24, itWorking, crtWorkingHour);

  // Set working hours between 13:00 and 17:00
  Rules.Add(13/24, 17/24, itWorking, crtWorkingHour);

  // We are NOT working on Sunday
  Rules.AddEncoded(EncodeRecurringDayOfWeek(1), itNonWorking);

  // We are NOT working on Saturday
  Rules.AddEncoded(EncodeRecurringDayOfWeek(7), itNonWorking);

  // Assign calendar rules to profile
  Resource.Availability.Rules := Rules;
end;

end.
