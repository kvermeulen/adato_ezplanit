object Form1: TForm1
  Left = 208
  Top = 114
  Width = 442
  Height = 374
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 417
    Height = 129
    Caption = 'Task data'
    TabOrder = 0
    object Label1: TLabel
      Left = 288
      Top = 24
      Width = 26
      Height = 13
      Caption = 'hours'
    end
    object Label2: TLabel
      Left = 288
      Top = 48
      Width = 115
      Height = 13
      Caption = '(for example: John,Chris)'
    end
    object Label3: TLabel
      Left = 63
      Top = 20
      Width = 40
      Height = 13
      Caption = 'Duration'
    end
    object Label4: TLabel
      Left = 52
      Top = 44
      Width = 51
      Height = 13
      Caption = 'Resources'
    end
    object Label5: TLabel
      Left = 56
      Top = 68
      Width = 47
      Height = 13
      Caption = 'Constraint'
    end
    object Label6: TLabel
      Left = 8
      Top = 92
      Width = 95
      Height = 13
      Caption = 'Constraint date/time'
    end
    object Button1: TButton
      Left = 296
      Top = 84
      Width = 105
      Height = 25
      Caption = 'Schedule'
      TabOrder = 0
      OnClick = Button1Click
    end
    object edDuration: TEdit
      Left = 112
      Top = 16
      Width = 153
      Height = 21
      TabOrder = 1
      Text = '0'
    end
    object edResources: TEdit
      Left = 112
      Top = 40
      Width = 169
      Height = 21
      TabOrder = 2
    end
    object cbConstraint: TComboBox
      Left = 112
      Top = 64
      Width = 169
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      Items.Strings = (
        'Do not level'
        'Must start on'
        'Must finish on'
        'Start no earlier than'
        'Finish at or before'
        'Start no later than'
        'Finish no later than'
        'As soon as possible'
        'At end of project'
        'Skip')
    end
    object cnDate: TDateTimePicker
      Left = 112
      Top = 88
      Width = 81
      Height = 21
      Date = 37538.445688761610000000
      Time = 37538.445688761610000000
      TabOrder = 4
    end
    object cnTime: TDateTimePicker
      Left = 200
      Top = 88
      Width = 81
      Height = 21
      Date = 37538.445746250000000000
      Time = 37538.445746250000000000
      Kind = dtkTime
      TabOrder = 5
    end
    object udDuration: TUpDown
      Left = 265
      Top = 16
      Width = 16
      Height = 21
      Associate = edDuration
      TabOrder = 6
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 144
    Width = 417
    Height = 153
    Caption = 'Schedule result'
    TabOrder = 1
    object Label7: TLabel
      Left = 15
      Top = 64
      Width = 48
      Height = 13
      Caption = 'Messages'
    end
    object lbStart: TLabel
      Left = 72
      Top = 16
      Width = 46
      Height = 16
      Caption = 'lbStart'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbStop: TLabel
      Left = 72
      Top = 32
      Width = 46
      Height = 16
      Caption = 'lbStop'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 40
      Top = 18
      Width = 22
      Height = 13
      Caption = 'Start'
    end
    object Label11: TLabel
      Left = 40
      Top = 34
      Width = 22
      Height = 13
      Caption = 'Stop'
    end
    object mmMessages: TMemo
      Left = 72
      Top = 64
      Width = 321
      Height = 73
      BorderStyle = bsNone
      ReadOnly = True
      TabOrder = 0
    end
  end
  object Button2: TButton
    Left = 350
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 2
    OnClick = Button2Click
  end
  object EzScheduler1: TEzScheduler
    OnLoadResourceData = EzScheduler1LoadResourceData
    Left = 176
    Top = 16
  end
end
