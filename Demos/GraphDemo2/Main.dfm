object Form1: TForm1
  Left = 206
  Top = 114
  Caption = 'Form1'
  ClientHeight = 574
  ClientWidth = 909
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo5: TMemo
    Left = 8
    Top = 8
    Width = 337
    Height = 89
    BorderStyle = bsNone
    Color = clWhite
    Lines.Strings = (
      
        'This demonstration shows how multiple TEzAvailabilityProfiles ca' +
        'n '
      'be combined together.'
      ''
      
        'On the left side, you'#39'll see two graphs. Each graph having a sin' +
        'gle '
      'red line. Each line can be changed with the mouse (hold down '
      #39'shift'#39' to select only part of the line).'
      ''
      
        'On the right side there is a single graph which displays the res' +
        'ult of '
      'adding or substracting the first and second graph.'
      ''
      
        'This functionality can be used for displaying resource allocatio' +
        'n '
      
        'graphs after scheduling a project. For example, in EzProject the' +
        ' '
      'resource '
      
        'allocation graph is the result of substracting the actual resour' +
        'ce '
      
        'availability graph from the original resource availability graph' +
        '.'
      ''
      'Notice that changes made in one component are automatically'
      'applied to the other components as well.')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 533
    Width = 909
    Height = 41
    Align = alBottom
    TabOrder = 1
    object Button1: TButton
      Left = 440
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Panel3: TPanel
    Left = 10
    Top = 100
    Width = 336
    Height = 419
    Caption = 'Panel1'
    TabOrder = 2
    object EzGraph1: TEzGraph
      Left = 1
      Top = 41
      Width = 334
      Height = 184
      BorderStyle = bsNone
      Lines = <
        item
          Description = 'Line 1'
          Visible = True
          CanEdit = True
          Pen.Color = clRed
          Pen.Width = 2
          Brush.Style = bsClear
          Tag = 0
        end>
      MaxDate = 38182.000000000000000000
      SnapSize = '1 day(s)'
      Xax.Pen.Width = 2
      Xax.Position = 164
      Yax.Color = clBtnFace
      Yax.Font.Charset = DEFAULT_CHARSET
      Yax.Font.Color = clWindowText
      Yax.Font.Height = -11
      Yax.Font.Name = 'MS Sans Serif'
      Yax.Font.Style = []
      Yax.DrawInside = True
      Yax.SnapSize = 0.250000000000000000
      Yax.Width = 0
      Timebar = EzTimebar2
      GridPen.Color = clGray
      GridPen.Style = psDot
      Align = alTop
    end
    object EzTimebar2: TEzTimebar
      Left = 1
      Top = 1
      Width = 334
      Height = 40
      AutoUpdateMajorSettings = True
      Date = 38182.000000000000000000
      ScaleStorage = EzTimescaleStorage1
      ScaleIndex = 0
    end
    object EzGraph2: TEzGraph
      Left = 1
      Top = 249
      Width = 334
      Height = 169
      BorderStyle = bsNone
      Lines = <
        item
          Description = 'Calendar data'
          Visible = True
          CanEdit = True
          Pen.Color = clRed
          Pen.Width = 2
          Brush.Style = bsClear
          Tag = 0
        end>
      MaxDate = 38182.000000000000000000
      ScrollbarOptions.ScrollBars = ssVertical
      SnapSize = '1 day(s)'
      Xax.Pen.Width = 2
      Xax.Position = 161
      Yax.Color = clBtnFace
      Yax.Font.Charset = DEFAULT_CHARSET
      Yax.Font.Color = clWindowText
      Yax.Font.Height = -11
      Yax.Font.Name = 'MS Sans Serif'
      Yax.Font.Style = []
      Yax.DrawInside = True
      Yax.SnapSize = 0.250000000000000000
      Yax.Width = 0
      Timebar = EzTimebar2
      GridPen.Color = clGray
      GridPen.Style = psDot
      Align = alClient
    end
    object Panel6: TPanel
      Left = 1
      Top = 225
      Width = 334
      Height = 24
      Align = alTop
      BevelOuter = bvLowered
      Caption = 'Graph 2'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 368
    Top = 224
    Width = 185
    Height = 105
    Caption = 'Operator'
    Items.Strings = (
      'Sum'
      'Substract'
      'And'
      'Or'
      'Schedule profile')
    TabOrder = 3
    OnClick = RadioGroup1Click
  end
  object PageControl1: TPageControl
    Left = 576
    Top = 16
    Width = 329
    Height = 497
    ActivePage = TabSheet1
    TabOrder = 4
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Graphical data'
      object Panel1: TPanel
        Left = 0
        Top = 105
        Width = 321
        Height = 364
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 0
        object EzGraph3: TEzGraph
          Left = 1
          Top = 41
          Width = 319
          Height = 322
          BorderStyle = bsNone
          Lines = <
            item
              Description = 'Calendar data'
              Visible = True
              Pen.Color = 51712
              Brush.Color = clSilver
              Tag = 0
            end>
          MaxDate = 38182.000000000000000000
          Xax.Pen.Width = 2
          Xax.Position = 302
          Yax.Color = clBtnFace
          Yax.Font.Charset = DEFAULT_CHARSET
          Yax.Font.Color = clWindowText
          Yax.Font.Height = -11
          Yax.Font.Name = 'MS Sans Serif'
          Yax.Font.Style = []
          Yax.DrawInside = True
          Yax.SnapSize = 0.250000000000000000
          Yax.Width = 0
          Timebar = EzTimebar1
          GridPen.Color = clGray
          GridPen.Style = psDot
          Align = alClient
        end
        object EzTimebar1: TEzTimebar
          Left = 1
          Top = 1
          Width = 319
          Height = 40
          AutoUpdateMajorSettings = True
          Date = 38182.000000000000000000
          ScaleStorage = EzTimescaleStorage1
          ScaleIndex = 0
        end
      end
      object Memo2: TMemo
        Left = 0
        Top = 0
        Width = 321
        Height = 105
        Align = alTop
        BorderStyle = bsNone
        Lines.Strings = (
          'This graph displays the result of adding or substracting the '
          'graphs '
          'displayed on the left.')
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Textual data'
      ImageIndex = 1
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 321
        Height = 57
        Align = alTop
        BorderStyle = bsNone
        Lines.Strings = (
          'This list displays the result of adding or substracting the '
          'graphs displayed on the left in a textual manner.')
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object ListBox1: TListBox
        Left = 0
        Top = 57
        Width = 321
        Height = 412
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
  end
  object EzTimescaleStorage1: TEzTimescaleStorage
    Scales = <
      item
        Bands = <
          item
            DisplayFormat = 'd mmm yy (x)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Scale = msWeek
          end
          item
            DisplayFormat = '$'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
          end>
        Name = 'TEzTimescale'
      end>
    Left = 416
    Top = 88
  end
end
