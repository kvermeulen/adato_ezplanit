unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, EzTimeBar, EzGraph, ExtCtrls, StdCtrls, DesignPanel, EzCalendar,
  ComCtrls;

type
  TForm1 = class(TForm)
    Memo5: TMemo;
    Panel2: TPanel;
    Button1: TButton;
    Panel3: TPanel;
    EzGraph1: TEzGraph;
    EzTimebar2: TEzTimebar;
    EzGraph2: TEzGraph;
    Panel6: TPanel;
    RadioGroup1: TRadioGroup;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    EzGraph3: TEzGraph;
    EzTimebar1: TEzTimebar;
    Memo2: TMemo;
    Memo1: TMemo;
    ListBox1: TListBox;
    EzTimescaleStorage1: TEzTimescaleStorage;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Add both graph 1 and 2 to the source list of graph 3. Then set the operator
  // to be used when combining these to graphs.
  //
  EzGraph3.Lines[0].Profile.AddSource(EzGraph1.Lines[0].Profile);
  EzGraph3.Lines[0].Profile.AddSource(EzGraph2.Lines[0].Profile);
  EzGraph3.Lines[0].Profile.Operator := opSum;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.RadioGroup1Click(Sender: TObject);
begin
  case RadioGroup1.ItemIndex of
    0: EzGraph3.Lines[0].Profile.Operator := opSum;
    1: EzGraph3.Lines[0].Profile.Operator := opSubstract;
    2: EzGraph3.Lines[0].Profile.Operator := opAnd;
    3: EzGraph3.Lines[0].Profile.Operator := opOr;
    4: EzGraph3.Lines[0].Profile.Operator := opScheduleProfile;
  end;
end;

procedure TForm1.PageControl1Change(Sender: TObject);
var
  i: Integer;
  s: string;

begin
  if PageControl1.ActivePage = TabSheet2 then
  begin
    ListBox1.Items.Clear;
    for i:=0 to EzGraph3.Lines[0].Profile.Count-1 do
    begin
      s := DateToStr(EzGraph3.Lines[0].Profile[i].DateValue) + ' - ' +
           FloatToStr(EzGraph3.Lines[0].Profile[i].Units);

      if afIsAllocated in EzGraph3.Lines[0].Profile[i].Flags then
        s := s + ' - Allocated';
      ListBox1.Items.Add(s);
    end;
  end;
end;

end.
