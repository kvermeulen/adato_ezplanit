
=------------------------------------------------------------------------------=
EzVirtualDataset readme file
  E-Mail:  Info@kever.com
  Web:     www.kever.com

Copyright (C) 2009, by Kever IT
=------------------------------------------------------------------------------=

This readme file accompanies the EzVirtualDataset package. License information
can be found in the file 'License.txt'.

The EzVirtualDataset zip-file only includes the source code. You will therefore
have to compile and install this component by hand. Compiling is easy by
opening the right package file. For example, EzVDS_D5.dpk is the package file to
use with Delphi 5 versions. A package file is provided for D4, D5, D6, D7, D2006,
D2007 and BC4, BC5 and BC6.

After opening the right package file you press the 'Compile' button and then the
'Install' button. If everything goes well, a new component is installed on the
'EzVirtualDataset' page.

D2006 and D2007 require a little different setup because these require you to 
compile a runtime and design time package seperately. The runtime package is named
EzSpecials_D200X while the design time package is named EzSpecialsDS_D200X. Only the
design time package needs to be installed using the 'Install' command.

A sample application is provided to show how to use the TEzArrayDataset
component.

=====================
HELP FILE
=====================

A help file specific to your Delphi or C++ Builder version can be found in the help directory. 

=====================
NEW FEATURES
=====================

Please read the file Release.txt to see the updates for the
current release.

=====================
DEMO APPLICATION
=====================

A demo application is installed in the directory <install dir>\demo. You
can examine this demo to learn how to use the components. 

You will need to install the virtual dataset component before you can 
compile the demo application. 

There is no project file included with the demo so you will have to create 
this yourself. This can be done by creating a new, empty project and then 
adding Main.pas and MSXML2_TLB.pas to it.

The demo application shows how to use the EzArrayDataset component in:
- A master/detail configuration
- A master/detail configuration with a standard TDataset
- A XML configuration

*******************         End of readme file               *******************
================================================================================
