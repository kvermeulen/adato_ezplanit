unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  EzVirtualDataset, StdCtrls, Db, DBGrids, DBCtrls, Grids, Mask,
  DBTables, ComCtrls, MSXML_TLB, ExtCtrls;

const
  NumericTypes = [ftInteger, ftBoolean, ftDateTime, ftDate, ftTime];

type
  POrderObject = ^TOrderObject;
  TOrderObject = record
    Supplier: Variant;
    OrderDescription: string;
    OrderDate: TDateTime;
  end;

  PDataObject = ^TDataObject;
  TDataObject = record
    Name: string;
    Age: Integer;
    Birthday: TDateTime;
    Memo: string;
    Orders: TList;
  end;

  TForm1 = class(TForm)
    DataSource1: TDataSource;
    EzNamesDataset: TEzArrayDataset;
    EzNamesDatasetName: TStringField;
    EzNamesDatasetBirthdate: TDateField;
    EzNamesDatasetMemo: TMemoField;
    Button2: TButton;
    PageControl1: TPageControl;
    tsMasterDetail: TTabSheet;
    Button1: TButton;
    Label1: TLabel;
    edRecNo: TEdit;
    Button3: TButton;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBMemo1: TDBMemo;
    EzOrdersDataset: TEzArrayDataset;
    DataSource3: TDataSource;
    EzOrdersDatasetDescription: TStringField;
    EzNamesDatasetAge: TIntegerField;
    EzOrdersDatasetOrderDate: TDateTimeField;
    DBGrid3: TDBGrid;
    Merged: TTabSheet;
    tblCustomers: TTable;
    DataSource2: TDataSource;
    DBGrid2: TDBGrid;
    DBGrid4: TDBGrid;
    EzTableDetail: TEzArrayDataset;
    StringField1: TStringField;
    DateTimeField1: TDateTimeField;
    DataSource4: TDataSource;
    Button4: TButton;
    Memo1: TMemo;
    Memo2: TMemo;
    tsXml: TTabSheet;
    Button5: TButton;
    XmlMaster: TEzArrayDataset;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    DateField1: TDateField;
    MemoField1: TMemoField;
    dsXmlMaster: TDataSource;
    XmlDetail: TEzArrayDataset;
    StringField3: TStringField;
    DateTimeField2: TDateTimeField;
    dsXmlDetail: TDataSource;
    DBGrid5: TDBGrid;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBMemo2: TDBMemo;
    DBGrid6: TDBGrid;
    Memo3: TMemo;
    Button6: TButton;
    Button7: TButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    tsSetupDialog: TTabSheet;
    XmlSetupDataset: TEzArrayDataset;
    DataSource5: TDataSource;
    Panel1: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    XmlSetupDatasetDirectory: TStringField;
    XmlSetupDatasetDateTime: TDateTimeField;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    Button8: TButton;
    Button9: TButton;
    Memo4: TMemo;
    XmlSetupDatasetMemo: TMemoField;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    Label15: TLabel;
    DBMemo3: TDBMemo;
    XmlSetupDatasetInteger: TIntegerField;
    DBEdit10: TDBEdit;
    Button10: TButton;
    EzOrdersDatasetidCustomer: TIntegerField;
    EzOrdersDatasetlkCustomer: TStringField;
    CountryLookup: TEzArrayDataset;
    CountryLookupidCountry: TIntegerField;
    CountryLookupName: TStringField;
    XmlSetupDatasetidCountry: TIntegerField;
    XmlSetupDatasetCountryLookup: TStringField;
    Label16: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    dsCountryLookup: TDataSource;
    procedure Button1Click(Sender: TObject);
    procedure EzNamesDatasetGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure FormCreate(Sender: TObject);
    procedure EzNamesDatasetGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure EzNamesDatasetPostData(Sender: TCustomEzArrayDataset; Index: Integer);
    procedure EzNamesDatasetDeleteRecord(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure edRecNoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button3Click(Sender: TObject);
    procedure EzNamesDatasetAfterScroll(DataSet: TDataSet);
    procedure EzOrdersDatasetGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure EzOrdersDatasetGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure EzOrdersDatasetPostData(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure EzOrdersDatasetDeleteRecord(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure EzOrdersDatasetBeforeInsert(DataSet: TDataSet);
    procedure Button4Click(Sender: TObject);
    procedure EzTableDetailGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure EzTableDetailPostData(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure EzTableDetailGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure EzTableDetailDeleteRecord(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure Button5Click(Sender: TObject);
    procedure XmlMasterGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure XmlMasterGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure XmlMasterDeleteRecord(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure XmlMasterPostData(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure Button7Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure XmlDetailGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure XmlDetailGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure XmlDetailPostData(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure XmlDetailDeleteRecord(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure XmlDetailBeforeInsert(DataSet: TDataSet);
    procedure XmlSetupDatasetGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure XmlSetupDatasetGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure XmlSetupDatasetPostData(Sender: TCustomEzArrayDataset;
      Index: Integer);
    procedure Button9Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure CountryLookupGetRecordCount(Sender: TCustomEzArrayDataset;
      var Count: Integer);
    procedure CountryLookupGetFieldValue(Sender: TCustomEzArrayDataset;
      Field: TField; Index: Integer; var Value: Variant);
    procedure CountryLookupLocate(Sender: TCustomEzArrayDataset;
      const KeyFields: String; const KeyValues: Variant;
      Options: TLocateOptions; var Index: Integer);
    procedure CountryLookupLookupValue(Sender: TCustomEzArrayDataset;
      const KeyFields: String; const KeyValues: Variant;
      const ResultFields: String; var Value: Variant);

  private
    FData: TList;
    FMergedDetailData: TStringList;
    XMLDoc: IXMLDOMDocument;
    XMLSetup: IXMLDOMDocument;


  public
    { Public declarations }
  end;

  procedure FreeDataObject(Value: TDataObject);
  procedure FreeOrderObject(Value: TOrderObject);

var
  Form1: TForm1;

implementation

{$R *.DFM}

{$IFNDEF VER150} { Borland Delphi 7.0 }
uses Variants;
{$ENDIF}

procedure FreeOrderObject(Value: TOrderObject);
begin
  Finalize(Value);
end;

procedure FreeDataObject(Value: TDataObject);
var
  i: Integer;

begin
  with Value.Orders do
  begin
    for i:=0 to Count-1 do
      FreeOrderObject(POrderObject(Items[i])^);
  end;
  Value.Orders.Destroy;
  Finalize(Value);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  P: PDataObject;
  POrder: POrderObject;
  i: Integer;

begin
  FData := TList.Create;
  FMergedDetailData := TStringList.Create;
  FMergedDetailData.Sorted := True;
  FMergedDetailData.Duplicates := dupAccept;
  PageControl1.ActivePage := tsMasterDetail;

  //
  // Create some sample data
  //
  for i:=0 to 19 do
  begin
    new(P);
    P^.Name := 'Name ' + IntToStr(i+1 {TEzArrayDataset uses 0 based indexing !!});
    P^.Age := i;
    P^.Birthday := EncodeDate(2002, 1, 1) + i;
    P^.Memo := 'Memo data for ' + P^.Name;
    P^.Orders := TList.Create;
    New(POrder);
    POrder^.Supplier := 1221; // ID of Kauai Dive Shoppe in table Customers
    POrder^.OrderDescription := 'Order placed by ' + P^.Name;
    POrder^.OrderDate := Now;
    P^.Orders.Add(POrder);
    FData.Add(P);
  end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
var
  i: Integer;

begin
  for i:=0 to FData.Count-1 do
  begin
    FreeDataObject(PDataObject(FData[i])^);
    Dispose(FData[i]);
  end;

  for i:=0 to FMergedDetailData.Count-1 do
  begin
    FreeOrderObject(POrderObject(FMergedDetailData.Objects[i])^);
    Dispose(Pointer(FMergedDetailData.Objects[i]));
  end;

  FMergedDetailData.Destroy;
  FData.Destroy;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  tblCustomers.Open;
  EzNamesDataset.Open;
  EzOrdersDataset.Open;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.EzNamesDatasetGetRecordCount(Sender: TCustomEzArrayDataset;
  var Count: Integer);

  //
  // Event handler for OnGetRecordCount event.
  //
  // Count must be set to the number of records the virtual dataset contains.
  //

begin
  Count := FData.Count;
end;

procedure TForm1.EzNamesDatasetGetFieldValue(Sender: TCustomEzArrayDataset; Field: TField;
  Index: Integer; var Value: Variant);

  //
  // Event handler for OnGetFieldValue event.
  //
  // This event returns the field data to the virtual dataset.
  //
  //  Sender: TCustomEzArrayDataset firing this event
  //  Field:  Field object for which the value should be returned
  //  Index:  Absolute index of the record for which data should be returned (0 based)
  //  Value: Variable to store result in.

var
  PData: PDataObject;

begin
  PData := FData[Index];

  case Field.FieldNo of
    1: Value := PData^.Name;
    2: Value := PData^.Age;
    3: Value := PData^.Birthday;
    4: Value := PData^.Memo;
  end;

{
  Alternative implementation

  if Field.FieldName = 'Name' then
    Value := PData^.Name
  else if Field.FieldName = 'Age' then
    Value := PData^.Age
  else if Field.FieldName = 'Birthday' then
    Value := PData^.Birthday
  else if Field.FieldName = 'Memo' then
    Value := PData^.Memo;
}
end;

procedure TForm1.EzNamesDatasetPostData(Sender: TCustomEzArrayDataset; Index: Integer);

  //
  // Event handler for OnPostData event.
  //
  // This event is fired to persist any updates applied to the dataset.
  // Here you should update your data buffer with the temporary stored values.
  //
  //  Sender: TCustomEzArrayDataset firing this event
  //  Index:  Absolute index of the record being updated.

var
  PData: PDataObject;

begin
  if EzNamesDataset.State = dsInsert then
  begin
    New(PData);
    PData^.Orders := TList.Create;
    if Index = -1 then
      FData.Add(PData) else
      FData.Insert(Index, PData);
  end else
    PData := PDataObject(FData[Index]);

  PData^.Name := EzNamesDataset.FieldByName('Name').AsString;
  PData^.Age := EzNamesDataset.FieldByName('Age').AsInteger;
  PData^.Birthday := EzNamesDataset.FieldByName('Birthday').AsDatetime;
  PData^.Memo := EzNamesDataset.FieldByName('Memo').AsString;
end;

procedure TForm1.EzNamesDatasetDeleteRecord(Sender: TCustomEzArrayDataset;
  Index: Integer);

  //
  // Event handler for OnDeleteRecord event.
  //
  // This event is fired when a record is deleted from the dataset.
  // Here you should remove your data object from the list.
  //
  //  Sender: TCustomEzArrayDataset firing this event
  //  Index:  Absolute index of the record being deleted.

begin
  FreeDataObject(PDataObject(FData[Index])^);
  Dispose(FData[Index]);
  FData.Delete(Index);
end;

procedure TForm1.edRecNoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    EzNamesDataset.RecNo := StrToInt(EdRecNo.Text);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  EzNamesDataset.RecNo := StrToInt(EdRecNo.Text);
end;

procedure TForm1.EzNamesDatasetAfterScroll(DataSet: TDataSet);
begin
  EdRecNo.Text := IntToStr(EzNamesDataset.RecNo);
end;

procedure TForm1.EzOrdersDatasetBeforeInsert(DataSet: TDataSet);
begin
  if EzNamesDataset.State = dsInsert then
    EzNamesDataset.Post;
end;

procedure TForm1.EzOrdersDatasetGetFieldValue(
  Sender: TCustomEzArrayDataset; Field: TField; Index: Integer;
  var Value: Variant);

var
  PData: POrderObject;

begin
  PData := PDataObject(FData[EzNamesDataset.Index])^.Orders[Index];

  case Field.FieldNo of
    1: Value := PData^.OrderDescription;
    2: Value := PData^.OrderDate;
    3: Value := PData^.Supplier;    
  end;
end;

procedure TForm1.EzOrdersDatasetGetRecordCount(
  Sender: TCustomEzArrayDataset; var Count: Integer);
begin
  Count := PDataObject(FData[EzNamesDataset.Index])^.Orders.Count;
end;

procedure TForm1.EzOrdersDatasetPostData(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  PData: POrderObject;

begin
  if EzOrdersDataset.State = dsInsert then
  begin
    New(PData);
    if Index = -1 then
      PDataObject(FData[EzNamesDataset.Index])^.Orders.Add(PData) else
      PDataObject(FData[EzNamesDataset.Index])^.Orders.Insert(Index, PData);
  end else
    PData := PDataObject(FData[EzNamesDataset.Index])^.Orders[Index];

  PData^.Supplier := EzOrdersDataset.FieldByName('idSupplier').AsInteger;
  PData^.OrderDescription := EzOrdersDataset.FieldByName('Description').AsString;
  PData^.OrderDate := EzOrdersDataset.FieldByName('OrderDate').AsDateTime;
end;

procedure TForm1.EzOrdersDatasetDeleteRecord(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  PData: POrderObject;

begin
  PData := PDataObject(FData[EzNamesDataset.Index])^.Orders[Index];
  Finalize(PData^);
  Dispose(PData);
  PDataObject(FData[EzNamesDataset.Index])^.Orders.Delete(Index);
end;

//=---------------------------------------------------------------------------=
//
// Merged dataset example
//
// This code shows how you can combine a standard TTable object with
// a TEzArrayDataset.
//
// We use a TStringlist to store the detail data even though this is not
// the fastest way of implementing this.
//
//=---------------------------------------------------------------------------=
procedure TForm1.Button4Click(Sender: TObject);
begin
  tblCustomers.Active := not tblCustomers.Active;
  EzTableDetail.Active := tblCustomers.Active;
end;

procedure TForm1.EzTableDetailGetRecordCount(Sender: TCustomEzArrayDataset;
  var Count: Integer);
var
  i: Integer;
  Key: string;

begin
  // Detail data is accessed using the customer number
  Key := tblCustomers.FieldByName('CustNo').AsString;

  i := FMergedDetailData.IndexOf(Key);
  if i<>-1 then
  begin
    Count := 1;
    inc(i);
    while (i < FMergedDetailData.Count) and (FMergedDetailData[i] = Key) do
    begin
      inc(i);
      inc(Count);
    end;
  end;
end;

procedure TForm1.EzTableDetailGetFieldValue(Sender: TCustomEzArrayDataset;
  Field: TField; Index: Integer; var Value: Variant);
var
  i: Integer;
  Key: string;

begin
  // Detail data is accessed using the customer number
  Key := tblCustomers.FieldByName('CustNo').AsString;

  i := FMergedDetailData.IndexOf(Key) + Index; // Index is relative to first
                                               // Key-item in stringlist.
  case Field.FieldNo of
    1: Value := POrderObject(FMergedDetailData.Objects[i])^.OrderDescription;
    2: Value := POrderObject(FMergedDetailData.Objects[i])^.OrderDate;
  end;
end;

procedure TForm1.EzTableDetailPostData(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  PData: POrderObject;
  Key: string;
  i, Count, Dest: Integer;

begin
  // Detail data is accessed using the customer number
  Key := tblCustomers.FieldByName('CustNo').AsString;

  if EzTableDetail.State = dsInsert then
  begin
    New(PData);
    i := FMergedDetailData.AddObject(Key, TObject(PData));

    // Repair positioning by calling Exchange
    if Index <> 0 then
    begin
      Count := EzTableDetail.RecordCount; // This includes newly created record!
      if Count <> 1 then
      begin
        if Index = -1 then // Append
          Dest := i+Count-1 else
          Dest := i+Index;

        while i < Dest do
        begin
          FMergedDetailData.Exchange(i, i+1);
          inc(i);
        end;
      end;
    end;
  end else
  begin
    i := FMergedDetailData.IndexOf(Key) + Index;
    PData := POrderObject(FMergedDetailData.Objects[i]);
  end;

  PData^.OrderDescription := EzTableDetail.FieldByName('Description').AsString;
  PData^.OrderDate := EzTableDetail.FieldByName('OrderDate').AsDateTime;
end;

procedure TForm1.EzTableDetailDeleteRecord(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  i: Integer;
  Key: string;

begin
  // Detail data is accessed using the customer number
  Key := tblCustomers.FieldByName('CustNo').AsString;
  i := FMergedDetailData.IndexOf(Key) + Index; // Index is relative to first
                                               // Key-item in stringlist.
  FreeOrderObject(POrderObject(FMergedDetailData.Objects[i])^);
  Dispose(Pointer(FMergedDetailData.Objects[i]));
  FMergedDetailData.Delete(i);
end;

//=----------------------------------------------------------------------------=
//
// XML Master/Detail Sample code
//
//=----------------------------------------------------------------------------=
procedure TForm1.Button5Click(Sender: TObject);
begin
  XMLDoc := CoDOMDocument.Create;

  XMLDoc.loadXml(
    '<Contacts>' +
      '<Contact>' +
        '<Name>Mike Johnson</Name>' +
        '<Age>20</Age>' +
        '<Birthday>27/11/1970</Birthday>' +
        '<Memo>Mike is my friend.</Memo>' +
        '<Orders>' +
          '<Order>' +
            '<Description>Order description</Description>' +
          '</Order>' +
        '</Orders>' +
      '</Contact>' +
    '</Contacts>');

  XmlMaster.Open;
  XmlDetail.Open;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    XmlMaster.Close;
    XmlDetail.Close;
    XMLDoc.load(OpenDialog1.FileName);
    if XMLDoc.documentElement.nodeName <> 'Contacts' then
      raise Exception.Create('Invalid xml document');
    XmlMaster.Open;
    XmlDetail.Open;
  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    XMLDoc.Save(SaveDialog1.FileName);
end;

procedure TForm1.Button10Click(Sender: TObject);
begin
  ShowMessage(XMLDoc.documentElement.xml);
end;

procedure TForm1.XmlMasterGetRecordCount(Sender: TCustomEzArrayDataset;
  var Count: Integer);
begin
  if XMLDoc.documentElement = nil then Exit;
  Count := XMLDoc.documentElement.childNodes.length;
end;

procedure TForm1.XmlMasterGetFieldValue(Sender: TCustomEzArrayDataset;
  Field: TField; Index: Integer; var Value: Variant);
var
  Node: IXMLDOMNode;

begin
  if XMLDoc.documentElement = nil then Exit;
  Node := XMLDoc.documentElement.childNodes[Index].selectSingleNode(Field.FieldName);

  if Node <> nil then
    if (Field.DataType in NumericTypes) and (Node.text = '') then
      Value := Null else
      Value := Node.Text;
end;

procedure TForm1.XmlMasterDeleteRecord(Sender: TCustomEzArrayDataset;
  Index: Integer);
begin
  XMLDoc.documentElement.removeChild(XMLDoc.documentElement.childNodes[Index]);
end;

procedure TForm1.XmlMasterPostData(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  Node, Child: IXMLDOMNode;
  i: Integer;

begin
  if XmlMaster.State = dsInsert then
  begin
    Node := XMLDoc.createElement('Contact');
    XMLDoc.documentElement.appendChild(Node);
  end else
    Node := XMLDoc.documentElement.childNodes[Index];

  with Node do
    for i:=0 to XmlMaster.Fields.Count-1 do
    begin
      Child := selectSingleNode(XmlMaster.Fields[i].FieldName);
      if Child <> nil then
        Child.text := XmlMaster.Fields[i].AsString else
        begin
          Child := XMLDoc.createElement(XmlMaster.Fields[i].FieldName);
          Child.text := XmlMaster.Fields[i].AsString;
          appendChild(Child);
        end;
    end;
end;

procedure TForm1.XmlDetailGetRecordCount(Sender: TCustomEzArrayDataset;
  var Count: Integer);

var
  Node: IXMLDOMNode;

begin
  if (XmlMaster.Index = -1) or (XMLDoc.documentElement = nil) then Exit;
  Node := XMLDoc.documentElement.childNodes[XmlMaster.Index].selectSingleNode('Orders');
  if Node <> nil then
    Count := Node.childNodes.length else
    Count := 0;
end;

procedure TForm1.XmlDetailGetFieldValue(Sender: TCustomEzArrayDataset;
  Field: TField; Index: Integer; var Value: Variant);
var
  Orders, Node: IXMLDOMNode;

begin
  if XMLDoc.documentElement = nil then Exit;
  Orders := XMLDoc.documentElement.childNodes[XmlMaster.Index].selectSingleNode('Orders');

  if Orders <> nil then
  begin
    Node := Orders.childNodes[Index].selectSingleNode(Field.FieldName);
    if Node <> nil then
      if (Field.DataType in NumericTypes) and (Node.text = '') then
        Value := Null else
        Value := Node.Text;
  end;
end;

procedure TForm1.XmlDetailPostData(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  Orders, Order, Child: IXMLDOMNode;
  i: Integer;

begin
  Orders := XMLDoc.documentElement.childNodes[XmlMaster.Index].selectSingleNode('Orders');
  if Orders = nil then
  begin
    Orders := XMLDoc.createElement('Orders');
    XMLDoc.documentElement.childNodes[XmlMaster.Index].appendChild(Orders);
  end;

  if XmlDetail.State = dsInsert then
  begin
    Order := XMLDoc.createElement('Order');
    Orders.appendChild(Order);
  end else
    Order := Orders.childNodes[Index];

  with Order do
    for i:=0 to XmlDetail.Fields.Count-1 do
    begin
      Child := selectSingleNode(XmlDetail.Fields[i].FieldName);
      if Child <> nil then
        Child.text := XmlDetail.Fields[i].AsString else
        begin
          Child := XMLDoc.createElement(XmlDetail.Fields[i].FieldName);
          Child.text := XmlDetail.Fields[i].AsString;
          appendChild(Child);
        end;
    end;
end;

procedure TForm1.XmlDetailDeleteRecord(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  Orders: IXMLDOMNode;

begin
  Orders := XMLDoc.documentElement.childNodes[XmlMaster.Index].selectSingleNode('Orders');
  Orders.removeChild(Orders.childNodes[XmlDetail.Index]);
end;

procedure TForm1.XmlDetailBeforeInsert(DataSet: TDataSet);
begin
  // Master record must be posted before when can insert a detail record.
  if XmlMaster.State = dsInsert then
    XmlMaster.Post;
end;

//=----------------------------------------------------------------------------=
//
// XML Setup dialog Sample
//
//=----------------------------------------------------------------------------=
procedure TForm1.XmlSetupDatasetGetRecordCount(
  Sender: TCustomEzArrayDataset; var Count: Integer);
begin
  Count := 1;
end;

procedure TForm1.XmlSetupDatasetGetFieldValue(
  Sender: TCustomEzArrayDataset; Field: TField; Index: Integer;
  var Value: Variant);
var
  Node: IXMLDOMNode;

begin
  Node := XMLSetup.documentElement.selectSingleNode(Field.FieldName);

  if Node <> nil then
    if (Field.DataType in NumericTypes) and (Node.text = '') then
      Value := Null else
      Value := Node.Text;
end;

procedure TForm1.XmlSetupDatasetPostData(Sender: TCustomEzArrayDataset;
  Index: Integer);
var
  Node, Child: IXMLDOMNode;
  i: Integer;

begin
  Node := XMLSetup.documentElement;

  with Node do
    for i:=0 to XmlSetupDataset.Fields.Count-1 do
    begin
      Child := selectSingleNode(XmlSetupDataset.Fields[i].FieldName);
      if Child <> nil then
        Child.text := XmlSetupDataset.Fields[i].AsString else
        begin
          Child := XMLSetup.createElement(XmlSetupDataset.Fields[i].FieldName);
          Child.text := XmlSetupDataset.Fields[i].AsString;
          appendChild(Child);
        end;
    end;
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
  XmlSetupDataset.Close;

  XMLSetup := CoDOMDocument.Create;
  if FileExists(ExtractFileDir(ParamStr(0)) + '\Settings.xml') then
  begin
    XMLSetup.load(ExtractFileDir(ParamStr(0)) + '\Settings.xml');
    ShowMessage('Data loaded from: ' + ExtractFileDir(ParamStr(0)) + '\Settings.xml');
  end
  else
  begin
    XMLSetup.loadXml('<Settings />');
    ShowMessage('Setup data initialized');
  end;

  CountryLookup.Open;
  XmlSetupDataset.Open;
end;

procedure TForm1.Button8Click(Sender: TObject);
begin
  if XmlSetupDataset.State = dsEdit then XmlSetupDataset.Post;
  XMLSetup.Save(ExtractFileDir(ParamStr(0)) + '\Settings.xml');
  ShowMessage('Data saved to: ' + ExtractFileDir(ParamStr(0)) + '\Settings.xml');
end;

procedure TForm1.CountryLookupGetRecordCount(Sender: TCustomEzArrayDataset;
  var Count: Integer);
begin
  Count := 2;
end;

procedure TForm1.CountryLookupGetFieldValue(Sender: TCustomEzArrayDataset;
  Field: TField; Index: Integer; var Value: Variant);
begin
  if(Field.FieldName='idCountry') then
    Value := Index else
    Value := Format('Country %d', [Index]);
end;

procedure TForm1.CountryLookupLocate(Sender: TCustomEzArrayDataset;
  const KeyFields: String; const KeyValues: Variant;
  Options: TLocateOptions; var Index: Integer);
begin
  Index := Integer(KeyValues);
end;

procedure TForm1.CountryLookupLookupValue(Sender: TCustomEzArrayDataset;
  const KeyFields: String; const KeyValues: Variant;
  const ResultFields: String; var Value: Variant);
begin
  if VarIsNull(KeyValues) then
  begin
    Value := Null;
    Exit;
  end;
  if ResultFields='idCountry' then
    Value := Integer(KeyValues) else
    Value := Format('Country %d', [Integer(KeyValues)]);
end;

end.
