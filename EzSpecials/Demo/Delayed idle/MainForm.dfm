object Form1: TForm1
  Left = 205
  Top = 114
  Width = 513
  Height = 330
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 264
    Top = 57
    Width = 46
    Height = 19
    Caption = 'Delay'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 408
    Top = 60
    Width = 25
    Height = 13
    Caption = 'msec'
  end
  object Label3: TLabel
    Left = 232
    Top = 201
    Width = 78
    Height = 19
    Caption = 'Test area'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 266
    Top = 88
    Width = 44
    Height = 19
    Caption = 'State'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbState: TLabel
    Left = 328
    Top = 88
    Width = 81
    Height = 25
    AutoSize = False
    Caption = 'not active'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 220
    Top = 120
    Width = 90
    Height = 19
    Caption = 'Next event'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbTimeOut: TLabel
    Left = 328
    Top = 120
    Width = 81
    Height = 17
    AutoSize = False
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbFired: TLabel
    Left = 328
    Top = 152
    Width = 42
    Height = 19
    Caption = 'False'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 269
    Top = 152
    Width = 41
    Height = 19
    Caption = 'Fired'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 193
    Height = 265
    Caption = 'Event triggered by'
    TabOrder = 0
    object CheckBox1: TCheckBox
      Left = 8
      Top = 24
      Width = 97
      Height = 17
      Caption = 'All messages'
      TabOrder = 0
      OnClick = CheckBoxClick
    end
    object CheckBox2: TCheckBox
      Tag = 1
      Left = 8
      Top = 48
      Width = 145
      Height = 17
      Caption = 'Keyboard messages'
      Checked = True
      State = cbChecked
      TabOrder = 1
      OnClick = CheckBoxClick
    end
    object CheckBox3: TCheckBox
      Tag = 2
      Left = 8
      Top = 72
      Width = 97
      Height = 17
      Caption = 'Mouse clicks'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = CheckBoxClick
    end
    object CheckBox4: TCheckBox
      Tag = 3
      Left = 8
      Top = 96
      Width = 97
      Height = 17
      Caption = 'Mouse move'
      TabOrder = 3
      OnClick = CheckBoxClick
    end
    object CheckBox5: TCheckBox
      Tag = 4
      Left = 8
      Top = 120
      Width = 97
      Height = 17
      Caption = 'Mouse scroll'
      TabOrder = 4
      OnClick = CheckBoxClick
    end
    object CheckBox6: TCheckBox
      Tag = 5
      Left = 8
      Top = 144
      Width = 145
      Height = 17
      Caption = 'User defined messages'
      TabOrder = 5
      OnClick = CheckBoxClick
    end
    object CheckBox7: TCheckBox
      Tag = 6
      Left = 8
      Top = 168
      Width = 129
      Height = 17
      Caption = 'Scroll messages'
      TabOrder = 6
      OnClick = CheckBoxClick
    end
    object CheckBox8: TCheckBox
      Tag = 7
      Left = 8
      Top = 192
      Width = 97
      Height = 17
      Caption = 'Paint messages'
      TabOrder = 7
      OnClick = CheckBoxClick
    end
    object CheckBox9: TCheckBox
      Tag = 8
      Left = 8
      Top = 216
      Width = 97
      Height = 17
      Caption = 'Window position messages'
      TabOrder = 8
      OnClick = CheckBoxClick
    end
    object CheckBox10: TCheckBox
      Tag = 9
      Left = 8
      Top = 240
      Width = 97
      Height = 17
      Caption = 'Other messages'
      TabOrder = 9
      OnClick = CheckBoxClick
    end
  end
  object Edit1: TEdit
    Left = 320
    Top = 56
    Width = 81
    Height = 21
    TabOrder = 1
    Text = '0'
    OnChange = Edit1Change
  end
  object Edit2: TEdit
    Left = 320
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'Enter your text here'
  end
  object Button1: TButton
    Left = 320
    Top = 242
    Width = 121
    Height = 25
    Caption = 'Activate'
    TabOrder = 3
    OnClick = Button1Click
  end
  object EzDelayedIdle1: TEzDelayedIdle
    Active = False
    OnDelayedIdle = EzDelayedIdle1DelayedIdle
    OnStateChanged = EzDelayedIdle1StateChanged
    OnWaiting = EzDelayedIdle1Waiting
    Left = 40
    Top = 64
  end
end
