unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DelayedIdle;

type
  TForm1 = class(TForm)
    EzDelayedIdle1: TEzDelayedIdle;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    Label4: TLabel;
    lbState: TLabel;
    Label5: TLabel;
    lbTimeOut: TLabel;
    lbFired: TLabel;
    Label7: TLabel;
    Button1: TButton;
    procedure CheckBoxClick(Sender: TObject);
    procedure EzDelayedIdle1StateChanged(Sender: TObject;
      NewState: TEzIdleState);
    procedure EzDelayedIdle1Waiting(Sender: TObject);
    procedure EzDelayedIdle1DelayedIdle(Sender: TObject;
      var NewState: TEzIdleState);
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.CheckBoxClick(Sender: TObject);
begin
  with Sender as TCheckBox do
    if Checked then
      EzDelayedIdle1.TriggerEvents :=
        EzDelayedIdle1.TriggerEvents + [TEzTriggerEvent(Tag)]
    else
      EzDelayedIdle1.TriggerEvents :=
        EzDelayedIdle1.TriggerEvents - [TEzTriggerEvent(Tag)];
end;

procedure TForm1.EzDelayedIdle1StateChanged(Sender: TObject;
  NewState: TEzIdleState);
begin
  case NewState of
    isInActive:
    begin
      lbState.Caption := 'not active';
      Button1.Caption := 'Activate';
    end;

    isActive:
    begin
      lbState.Caption := 'Active';
      lbFired.Caption := 'False';
      Button1.Caption := 'Deactivate';
      lbTimeOut.Caption := IntToStr(EzDelayedIdle1.TimeOut);
    end;

    isWaiting:
    begin
      lbState.Caption := 'Waiting';
      lbTimeOut.Caption := IntToStr(EzDelayedIdle1.TimeOut);
    end;

    isHandled: lbState.Caption := 'Handled';
  end;
end;

procedure TForm1.EzDelayedIdle1Waiting(Sender: TObject);
begin
  lbTimeOut.Caption := IntToStr(EzDelayedIdle1.TimeOut);
end;

procedure TForm1.EzDelayedIdle1DelayedIdle(Sender: TObject;
  var NewState: TEzIdleState);
begin
  lbFired.Caption := 'True';
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  EzDelayedIdle1.Active := not EzDelayedIdle1.Active;
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  try
    EzDelayedIdle1.Delay := StrToInt(Edit1.Text);
  except
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Edit1.Text := IntToStr(EzDelayedIdle1.Delay);
end;


end.
