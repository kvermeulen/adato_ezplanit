unit EzDurationField;

interface

{$I Ez.inc}

uses classes, db, dbconsts, EzMasks {$IFDEF TNT_UNICODE}, TntDB{$ELSE}, EzDB{$ENDIF};

type
  {
    Overview:
      Helper class that encapsulates TEzDurationSettings and provides property setters and getters for the variables stored therein.

    Description:
      TEzDurationSettings stores input characters used for parsing user input.
      This class is used in calls to FormatDuration and ParseDuration to provide
      a thread-safe implementation.
      When parsing data entered by the user certain chartacters are recognized
      as indicators for the type a value represents. For example if a user
      enters '5h' then this is read as 5 hours while entering only '5' would
      have been read as 5 days. Because different languages use different
      characters, you can use the properties contained in this class to set
      the characters that mark certain values.

      By default DayChar is set to 'd', HourChar is set to 'h', MinuteChar is
      set to 'm', SecondsChar is set to 's' and MSecondsChar is set to 'z'.
      Therefore, when the user enters '5d 4h 3m 40s 100z' then this is
      converted to:

      5 * HoursPerDay/24 + 4 hours + 3 minutes + 40 seconds + 100 miliseconds.

      If the input characters are not included in the input string then
      ParseDuration will still try to convert the string to a proper duration
      value. It thereby assumes that data is entered in the order:
      days, hours, minutes, seconds and miliseconds.

  }
  TEzDurationSettingsHelper = class(TPersistent)
  private
    FSettings: TEzDurationSettings;

  public
    {
      Overview:
        Creates a new instance of TEzDurationSettingsHelper.
      Description:
        Create allocates a new instance of the TEzDurationSettingsHelper class.
        Because this class is only used by TEzDurationField you normally should
        not have reason to call this method directly.
    }
    constructor Create;

    {
      Overview:
        Assigns Source to this instance of TEzDurationSettingsHelper.
      Description:
        Assign copies Source into this instance of TEzDurationSettingsHelper.
        Normally assign is called automatically by Delphi when needed.
    }
    procedure Assign(Source: TPersistent); override;

    {
      Overview:
        Settings holds the actual data stored in a TEzDurationSettingsHelper.
      Description:
        Settings points to the TEzDurationSettings record which holds the actual
        data accessed by TEzDurationSettingsHelper. You can use Settings to
        directly access the TEzDurationSettings properties.
    }
    property Settings: TEzDurationSettings read FSettings;

  published
    {
      Overview:
        Holds the chartacter used to indicate a day value.
      Description:
        See TEzDurationSettings.
      See also:
        TEzDurationSettings.DayChar
    }
    property DayChar: SystemChar read FSettings.DayChar write FSettings.DayChar default 'd';

    {
      Overview:
        Holds the chartacter used to indicate an hour value.
      Description:
        When parsing data entered by the user this property holds the character
        that indicates that an hour value was entered. For example if a user
        entered '5h' then the result would be 5 hours.

        By default this property is set to 'h'.
    }
    property HourChar: SystemChar read FSettings.HourChar write FSettings.HourChar default 'h';

    {
      Overview:
        Holds the chartacter used to indicate a minute value.
      Description:
        When parsing data entered by the user this property holds the character
        that indicates a minute value was entered. For example if a user
        entered '5m' then the result would be 5 minutes.

        By default this property is set to 'm'.
    }
    property MinuteChar: SystemChar read FSettings.MinuteChar write FSettings.MinuteChar default 'm';

    {
      Overview:
        Holds the chartacter used to indicate a seconds value.
      Description:
        When parsing data entered by the user this property holds the character
        that indicates a second value was entered. For example if a user
        entered '5s' then the result would be 5 seconds.

        By default this property is set to 's'.
    }
    property SecondsChar: SystemChar read FSettings.SecondsChar write FSettings.SecondsChar default 's';

    {
      Overview:
        Holds the chartacter used to indicate a miliseconds value.

      Description:
        When parsing data entered by the user this property holds the character
        that indicates a milisecond value was entered. For example if a user
        entered '5z' then the result would be 5 miliseconds.

        By default this property is set to 'z'.
    }
    property MSecondsChar: SystemChar read FSettings.MSecondsChar write FSettings.MSecondsChar default 'z';

    {
      Overview:
       Holds the number of hours contained in a working day.
      Description:
        This value is used to translate between real days (24 hours) and
        working days. Many times, when a users enters a duration of 1 day, he
        or she actually means a working day. A working day normally equals to
        8 hours and therefore the calculated value should be 1*8/24 instead of 1.
        This property allows you to control such behaviour.

        Set HoursPerDay to the number of workinghours in a regular working day.
        Now, when a duration is parsed and/or displayed, 1 day represents
        HoursPerDay instead of real day (24 hours).
    }
    property HoursPerDay: Double read FSettings.HoursPerDay write FSettings.HoursPerDay;

    {
      Overview:
        Holds the chartacter used by default, that is when the inputstring does
        not contain any other characters.
      Description:
        When parsing data entered by the user this property holds the default
        character used if an inputcharacter is not present. For example if a
        user entered '5' the parser function would not know whether the user
        meant 5 days, hours, minutes or whatever. In such situations, the
        default character will be used. Therefore if the default character
        equals DayChar then the value would be read as 5 days, if the default
        character equals HourChar then the value would be read as 5 hours.

        By default this property is set to 'd' which equals DayChar.
    }
    property DefaultChar: SystemChar read FSettings.DefaultChar write FSettings.DefaultChar default 'd';
  end;

  {
    Overview:
      TEzDurationField represents a duration field in a dataset.
    Description:
      TEzDurationField encapsulates the fundamental behavior common to
      floating point values representing durations. You can use
      TEzDurationField instead of Delphi's TFloatField type.
      Durations can be stored as a floating point value where the integer
      part equals the number of days and the fractional part equals the
      fraction of a 24 hour day. TEzDurationField makes editing and
      displaying such data straightforward by providing a duration string
      parser and a formatter. The parser will read text entered by the user
      and convert it into a floating point value. The formatter will display
      the floating point value into a readable text format.

      Delphi will automatically add field components to the dataset
      field list when you choose 'Add fields' from the pop-up menu in the
      fields editor. By default floating point values are mapped to
      TFloatField or TBCDFields types. Therefore if you want to add a
      TEzDurationField to your dataset you will have to do this by hand.
      This can be done by choosing 'New field' from the pop-up
      menu in the fields editor.

      Some samples of duration strings which can be parsed are '3d 4h 40s'
      (will be converted to 3 days, 4 hours and 40 seconds which equals 3.667129)
      and '76h 40s' (this will be read as the same value).
  }
  TEzDurationField = class(TField, IWideStringField)
  private
    FInputSettings: TEzDurationSettingsHelper;
    FDisplayFormat: SystemString;
    FEditMask: SystemString;

    FOnGetText: TFieldGetWideTextEvent;
    FOnSetText: TFieldSetWideTextEvent;

  protected
    procedure SetDisplayFormat(const Value: SystemString);
    procedure SetEditMask(const Value: SystemString);

    procedure SetOnGetText(const Value: TFieldGetWideTextEvent);
    procedure SetOnSetText(const Value: TFieldSetWideTextEvent);

    function  GetWideDisplayText: WideString;
    function  GetWideEditText: WideString;
    procedure SetWideEditText(const Value: WideString);

{$IFNDEF WIDESTRINGSUPPORTED}
    function  GetAsWideString: SystemWideString;
    procedure SetAsWideString(Value: SystemWideString);
{$ENDIF}

  protected
    {GH_NOIMPORT}
    function GetAsFloat: Double; override;
    function GetAsInteger: Longint; override;
    function GetAsString: string; override;
    function GetAsVariant: Variant; override;

{$IFNDEF EZ_D5}
    function GetDataSize: Word; override;
{$ELSE}
    function GetDataSize: Integer; override;
{$ENDIF}

    procedure GetText(var Text: string; DisplayText: Boolean); override;
    procedure SetAsFloat(Value: Double); override;
    procedure SetAsInteger(Value: Longint); override;
    procedure SetAsString(const Value: string); override;
    procedure SetInputSettings(Value: TEzDurationSettingsHelper);
    procedure SetVarValue(const Value: Variant); override;
    {GH_IMPORT}
  public
    {
      Overview:
        Creates a new instance of TEzDurationField.
      Description:
        Create allocates a new instance of the TEzDurationField class.

        Most applications do not explicitly create instances of TEzDurationField.
        Instead, the field components are created manually, as persistent
        field components defined in the Fields editor at design time when you
        choose 'New field' from the pop-up menu in the fields editor.
    }
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    property Value: SystemWideString read GetAsWideString write SetAsWideString;
    property DisplayText{TNT-ALLOW DisplayText}: WideString read GetWideDisplayText;
    property Text: WideString read GetWideEditText write SetWideEditText;
    {$IFNDEF COMPILER_10_UP}
    property AsWideString: SystemWideString read GetAsWideString write SetAsWideString;
    {$ENDIF}
    property WideDisplayText: WideString read GetWideDisplayText;
    property WideText: WideString read GetWideEditText write SetWideEditText;

  published
    {
      Overview:
        Determines how a duration field�s value is formatted for display
        in a data-aware control.
      Description:
        Use the DisplayFormat property to override automatic formatting of
        a field for display purposes.
        The value of DisplayFormat is a string that encodes the formatting
        of duration data using the specifiers recognized by the FormatDuration
        method. See FormatDuration for a list of format specifiers.

      See also:
        FormatDuration
    }
    property DisplayFormat: SystemString read FDisplayFormat write SetDisplayFormat;
    property EditMask: SystemString read FEditMask write SetEditMask;
    {
      Overview:
        Holds a reference to a TEzDurationSettingsHelper object that stores
        the input characters recognized by this TEzDurationField.
      Description:
        When parsing data entered by the user TEzDurationField recognises
        certain chartacters as indicators for the type a value represents.
        For example if a user enters '5h' then this is read as 5 hours while
        entering only '5' would have been read as 5 days. Because different
        languages use different characters, you can use this property to change
        the meaning of the indicator characters.
      See also:
        FormatDuration
    }
    property InputSettings: TEzDurationSettingsHelper
          read FInputSettings write SetInputSettings;

    property OnGetText: TFieldGetWideTextEvent read FOnGetText write SetOnGetText;
    property OnSetText: TFieldSetWideTextEvent read FOnSetText write SetOnSetText;
  end;

implementation

uses sysutils, Windows {$IFDEF EZ_D6}, Variants;{$ELSE};{$ENDIF}

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzDurationSettingsHelper.Create;
begin
  inherited;
  GetLocaleDurationSettings(GetThreadLocale, FSettings);
end;

procedure TEzDurationSettingsHelper.Assign(Source: TPersistent);
begin
  if Source is TEzDurationSettingsHelper then
    FSettings := TEzDurationSettingsHelper(Source).FSettings else
    inherited;
end;

//=---------------------------------------------------------------------------=
//=---------------------------------------------------------------------------=
constructor TEzDurationField.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FInputSettings := TEzDurationSettingsHelper.Create;
  SetDataType(ftFloat);
  ValidChars := ['0'..'9', ':', ' ', 'd', 'h', 'm', '+', '-', '.'];
end;

destructor TEzDurationField.Destroy;
begin
  inherited;
  FInputSettings.Destroy;
end;

function TEzDurationField.GetAsFloat: Double;
begin
  if not GetData(@Result) then Result := 0;
end;

function TEzDurationField.GetAsInteger: Longint;
begin
  Result := Longint(Round(GetAsFloat));
end;

function TEzDurationField.GetAsString: string;
var
  F: Double;
begin
  if GetData(@F) then Result := FloatToStr(F) else Result := '';
end;

function TEzDurationField.GetAsVariant: Variant;
var
  F: Double;
begin
  if GetData(@F) then Result := F else Result := Null;
end;

{$IFNDEF EZ_D5}
function TEzDurationField.GetDataSize: Word;
{$ELSE}
function TEzDurationField.GetDataSize: Integer;
{$ENDIF}
begin
  Result := SizeOf(Double);
end;

procedure TEzDurationField.GetText(var Text: string; DisplayText: Boolean);
begin
  if DisplayText then
    Text := GetWideDisplayText else
    Text := GetWideEditText;
end;

function TEzDurationField.GetWideDisplayText: WideString;
const
  DefMask='[d\d ][hh:mm][:ss]';

var
  Mask: SystemString;
  D: Double;
begin
  D := GetAsFloat;
  if (FDisplayFormat <> '') then
    Mask := FDisplayFormat else
    Mask := DefMask;

  Result := FormatDuration(Mask, D, InputSettings.Settings);
  if Result='' then;
end;

function TEzDurationField.GetWideEditText: WideString;
const
  DefMask='[d\d ][hh:mm][:ss]';

var
  Mask: SystemString;
  D: Double;
begin
  D := GetAsFloat;
  if EditMask<>'' then
    Mask := EditMask
  else if FDisplayFormat<>'' then
    Mask := FDisplayFormat
  else
    Mask := DefMask;

  Result := FormatDuration(Mask, D, InputSettings.Settings);
  if Result='' then;
end;

procedure TEzDurationField.SetWideEditText(const Value: WideString);
begin
  if Length(Value) = 0 then
    Clear else
    SetAsFloat(Parseduration(Value, FInputSettings.FSettings));
end;

{$IFNDEF WIDESTRINGSUPPORTED}
function  TEzDurationField.GetAsWideString: SystemWideString;
begin
  Result := GetWideEditText;
end;

procedure TEzDurationField.SetAsWideString(Value: SystemWideString);
begin
  SetWideEditText(Value);
end;
{$ENDIF}

procedure TEzDurationField.SetAsFloat(Value: Double);
begin
  SetData(@Value);
end;

procedure TEzDurationField.SetAsInteger(Value: Longint);
begin
  SetAsFloat(Value);
end;

procedure TEzDurationField.SetAsString(const Value: string);
begin
  if Value = '' then
    Clear else
    SetAsFloat(Parseduration(Value, FInputSettings.FSettings));
end;

procedure TEzDurationField.SetDisplayFormat(const Value: SystemString);
begin
  if FDisplayFormat <> Value then
  begin
    FDisplayFormat := Value;
    PropertyChanged(False);
  end;
end;

procedure TEzDurationField.SetEditMask(const Value: SystemString);
begin
  if FEditMask <> Value then
  begin
    FEditMask := Value;
    PropertyChanged(False);
  end;
end;

procedure TEzDurationField.SetInputSettings(Value: TEzDurationSettingsHelper);
begin
  FInputSettings.Assign(Value);
end;

procedure TEzDurationField.SetOnGetText(const Value: TFieldGetWideTextEvent);
begin

end;

procedure TEzDurationField.SetOnSetText(const Value: TFieldSetWideTextEvent);
begin

end;

procedure TEzDurationField.SetVarValue(const Value: Variant);
begin
  SetAsFloat(Value);
end;


end.
