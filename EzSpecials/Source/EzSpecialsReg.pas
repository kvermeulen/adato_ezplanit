unit EzSpecialsReg;

interface

{$I Ez.inc}

uses Classes;

procedure Register;

implementation

{$R *.res}

uses db, DelayedIdle, BorderLess, EzVirtualDataset, EzDurationField, EzSpeedButton;

procedure Register;
begin
  RegisterComponents
  (
    'EzSpecials', [TEzDelayedIdle, TEzBorderLessDBLookupCombobox, TEzArrayDataset, TEzSpeedButton]
  );

  RegisterClassAlias(TEzDurationField, 'TExtendedTimeField');
  RegisterFields([TEzDurationField]);
end;

end.
