unit EzArrays;

interface

uses sysutils, classes, Windows;

type
  { Exceptions }
  EArrayError = class(Exception);
  EUnsupportedTypeError = class(Exception);
  ELowCapacityError = class(Exception);

  { Forward declarations }
  TEzListNotifier = class;

  TCompareProc = function(item1, item2: Pointer): Integer;

  // Overview:
  // TEzSortOrder indicates the sort order for sorted lists.
  // TEzSortOrder is used by TEzBaseArray to indicate the order in which the
  // data should be sorted. 
  TEzSortOrder = (
    tsNone,       // Do not sort array
    tsAscending,  // Sort array in ascending order
    tsDescending  // Sort array in descending order
  );

  { These flags govern some of the behaviour of array methods }
  TArrayFlags = (afOwnsData, afAutoSize, afCanCompare, afSortUnique);
  TArrayFlagSet = Set of TArrayFlags;

  TDuplicates = (dupIgnore, dupAccept, dupError);

  // Overview:
  // TEzBaseArray implements a (sorted) array.
  //
  // Description:
  // TEzBaseArray is replaced by TEzObjectList and should therefore no longer
  // be used.
  TEzBaseArray = class(TPersistent)
  private
    FMemory: Pointer;           { Pointer to item buffer }
    FCapacity: Integer;         { The allocated size of the array }
    FItemSize: Integer;         { Size of individual item in bytes }
    FCount: Integer;            { Count of items in use }
    FSortOrder: TEzSortOrder;     { True if array is considered sorted }
    FFlags: TArrayFlagSet;      { Ability flags }
    FDuplicates: TDuplicates;   { Signifies if duplicates are stored or not }

  protected
    function  GetItemPtr(index: Integer): Pointer;
    procedure SetCount(NewCount: Integer);
    function  CompareItems(var Item1, Item2): Integer; virtual;
    procedure CopyItems(PSource, PDest: Pointer; numItems: Integer); virtual;
    procedure ReleaseItems(PItems: Pointer; numItems: integer); virtual;
    procedure FreeItems(PItems: Pointer; numItems: Integer); virtual;

    function  ValidIndex(Index: Integer): Boolean;
    function  HasFlag(aFlag: TArrayFlags): Boolean;
    procedure SetFlag(aFlag: TArrayFlags);
    procedure ClearFlag(aFlag: TArrayFlags);
    procedure SetAutoSize(aSize: Boolean);
    procedure BlockCopy(Source: TEzBaseArray; fromIndex, toIndex, numitems: Integer);
    function  GetAutoSize: Boolean;
    function  ValidateBounds(atIndex: Integer; var numItems: Integer): Boolean;
    procedure InternalHandleException;
    procedure SetCapacity(NewCapacity: Integer); virtual;
    procedure Grow; virtual;
  public
    constructor Create(itemcount, iSize: Integer); virtual;
    destructor  Destroy; override;

    procedure Assign(Source: TPersistent); override;
    function  Add(var Value): Integer; virtual;
    procedure Clear; virtual;
    procedure InsertAt(Index: Integer; var Value); virtual;
    procedure PutItem(index: Integer; var Value);
    procedure GetItem(index: Integer; var Value);
    procedure RemoveItem(Index: Integer);
    procedure RemoveRange(atIndex, numItems: Integer);
    procedure Release(Index: Integer); virtual;
    procedure Delete(Index: Integer); virtual;
    procedure Exchange(Index1, Index2: Integer); virtual;
    function  IndexOf(var Item): Integer; virtual;
    function  FindItem(var Index: Integer; var Value): Boolean;
    function  LowerBound(var Item): Integer;
    procedure Sort(Compare: TCompareProc); virtual;

    property  Duplicates: TDuplicates read FDuplicates write FDuplicates default dupIgnore;
    property  SortOrder: TEzSortOrder read FSortOrder write FSortOrder default tsNone;
    property  Capacity: Integer read FCapacity write SetCapacity;
    property  ItemSize: Integer read FItemSize;
    property  AutoSize: Boolean read GetAutoSize write SetAutoSize;
    property  Count: Integer read FCount write SetCount;
    property  List: Pointer read FMemory;
  end;

  // Overview:
  // TEzIntArray implements a (sorted) array of integers.
  //
  // Description:
  // TEzIntArray is replaced by TEzObjectList and should therefore no longer be
  // used.
  TEzIntArray = class(TEzBaseArray)
  protected
    function  GetItem(Index: Integer): Integer;
    procedure SetItem(Index, Value: Integer);
    function  CompareItems(var Item1, Item2): Integer; override;

  public
    constructor Create(InitialSize: Integer = 0); reintroduce; virtual;
    property Items[Index: Integer]: Integer read GetItem write SetItem; default;
  end;

  // Overview:
  // TEzListNotification indicates the type of change that is made to the
  // items in a TEzObjectList.
  TEzListNotification = (
    elnAdding,    // An new item is about to be added.
    elnAdded,     // An new item is added.
    elnChanging,  // An item is about to be replaced by a new item.
    elnExtracting,// An item is about to be extracted.
    elnExtracted, // An item is extracted.
    elnDeleting,  // An item is about to be deleted.
    elnDeleted    // An item is deleted.
  );

  // Overview:
  // TEzObjectList maintains a list of (owned) objects.
  //
  // Description:
  // TEzObjectList is much like a TObjectList. Like TObjectList, TEzObjectList
  // owns the objects stored in the list and therefore frees the associated
  // memory when detroyed.
  // TEzObjectList supports extended change notifications (1-to-many) through
  // TEzListNotifier.
  //
  // SeeAlso:TEzListNotifier;TObjectList;TList
  TEzObjectList = class(TList)
  protected
    FOwnsObjects: Boolean;
    FDataLinks: TList;
    FDestroying: Boolean;
    FNotifyDisable: Integer;

    procedure AddLink(ALink: TEzListNotifier);
    procedure DeleteLink(ALink: TEzListNotifier);

    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
    procedure Notifylinks(AObject: TObject; Action: TEzListNotification); virtual;

    function  GetItem(Index: Integer): TObject;
    function  DoGetItem(Index: Integer): TObject; virtual;
    procedure SetItem(Index: Integer; AObject: TObject);
    procedure DoSetItem(Index: Integer; AObject: TObject); virtual;

  public
    constructor Create(DoesOwnObjects: Boolean); virtual;
    destructor Destroy; override;

    function  Add(AObject: TObject): Integer; virtual;
    procedure Assign(Source: TEzObjectList);

    procedure Delete(Index: Integer); virtual;
    procedure DisableNotifications;
    procedure EnableNotifications;
    function  Extract(Item: TObject): TObject; virtual;
    function  IndexOf(var Index: Integer; AObject: TObject): Boolean; overload; virtual;
    function  IndexOf(AObject: TObject): Integer; overload; virtual;
    function  Remove(AObject: TObject): Integer; virtual;
    procedure Insert(Index: Integer; AObject: TObject); virtual;
    function  First: TObject; virtual;
    function  Last: TObject; virtual;

    property  Destroying: Boolean read FDestroying;
    property  OwnsObjects: Boolean read FOwnsObjects write FOwnsObjects;
    property  Items[Index: Integer]: TObject read GetItem write SetItem; default;
  end;

  TEzListNotifier = class(TPersistent)
  protected
    FObjectList: TEzObjectList;
    procedure Notify(AObject: TObject; Action: TEzListNotification); virtual; abstract;

    procedure SetObjectList(Value: TEzObjectList);
  public
    constructor Create(AList: TEzObjectList);
    destructor Destroy; override;

    property ObjectList: TEzObjectList read FObjectList write SetObjectList;
  end;

  TEzSortedObjectList = class(TEzObjectList)
  private
    FDuplicates: TDuplicates;   { Signifies if duplicates are stored or not }

  protected
    function  CompareItems(Obj1, Obj2: TObject): Integer; virtual;

    procedure DoSetItem(Index: Integer; AObject: TObject); override;

  public

    function  Add(AObject: TObject): Integer; override;
    function  IndexOf(var Index: Integer; AObject: TObject): Boolean; override;

    property  Duplicates: TDuplicates read FDuplicates write FDuplicates default dupIgnore;
  end;

  TEzObjectStack = class(TEzObjectList)
  public
    procedure Push(AObject: TObject);
    function  Pop: TObject;
  end;

implementation

uses EzStrings_1;

procedure ArrayDuplicateError;
begin
  raise EArrayError.Create(sDupeItem);
end;

procedure ArrayIndexError(Index: Integer);
begin
  raise EArrayError.Create(Format(sArrayIndexOutOfRange, [Index]));
end;


  { TEzBaseArray class }

constructor TEzBaseArray.Create(itemcount, iSize: Integer);
begin
  inherited Create;
  FMemory := nil;
  FCapacity := 0;
  FCount := 0;
  FItemSize := iSize;
  FFlags := [afOwnsData, afAutoSize];

  FDuplicates := dupIgnore;
  FSortOrder := tsNone;

  SetCapacity(itemcount);
end;

destructor TEzBaseArray.Destroy;
begin
  if (FMemory <> nil) then
  begin
    Clear;
    FItemSize := 0;
  end;
  inherited Destroy;
end;

procedure TEzBaseArray.Assign(Source: TPersistent);
begin
  if not (Source is TEzBaseArray) then
    inherited
  else
  begin
    Count := TEzBaseArray(Source).Count;
    CopyItems(TEzBaseArray(Source).FMemory, FMemory, TEzBaseArray(Source).Count);
  end;
end;

function TEzBaseArray.Add(var Value): Integer;
begin
  if (SortOrder = tsNone) then
    Result := FCount
  else
    if FindItem(Result, Value) then
      case Duplicates of
        dupIgnore:
        begin
          Result := -1;
          Exit;
        end;
        dupError: ArrayDuplicateError;
      end;
  InsertAt(Result, Value);
end;

procedure TEzBaseArray.SetCount(NewCount: Integer);
begin
  if (NewCount < 0) or (NewCount > MaxListSize) then
    ArrayIndexError(NewCount);
  if (NewCount > FCapacity) then
    SetCapacity(NewCount);
  if (NewCount > FCount) then
    FillMemory(GetItemPtr(FCount), (NewCount - FCount) * FItemSize, 0);
  FCount := NewCount;
end;

procedure TEzBaseArray.Clear;
begin
  if (FCount <> 0) then
    FreeItems(GetItemPtr(0), FCount);
  FCount := 0;
  SetCapacity(0);  { Has same affect as freeing memory }
end;

procedure TEzBaseArray.SetCapacity(NewCapacity: Integer);
begin
  if (NewCapacity < FCount) or (NewCapacity > MaxListSize) then
    ArrayIndexError(NewCapacity);
  if (NewCapacity <> FCapacity) then
  begin
    ReallocMem(FMemory, NewCapacity * FItemSize);
    if Assigned(FMemory) then
      FillChar(Pointer(Integer(FMemory) + FCapacity * FItemSize)^, (NewCapacity - FCapacity) * FItemSize, 0);
    FCapacity := NewCapacity;
  end;
end;

procedure TEzBaseArray.Grow;
var
  Delta: Integer;
begin
  if (FCapacity > 64) then
    Delta := FCapacity div 4
  else if (FCapacity > 8) then
    Delta := 16
  else
    Delta := 4;
  SetCapacity(FCapacity + Delta);
end;

procedure TEzBaseArray.InsertAt(Index: Integer; var Value);
begin
  if (Index < 0) or (Index > FCount) then
    ArrayIndexError(Index);
  { Increase the array size if needed }
  if AutoSize and (FCount = FCapacity) then
    SetCapacity(FCount+1);
  if (Index < FCount) then
  begin
    try
      MoveMemory(GetItemPtr(Index+1), GetItemPtr(Index), (FCount - Index) * FItemSize);
      ReleaseItems(GetItemPtr(Index), 1);
    except
      InternalHandleException;
    end;
  end;
  CopyItems(@Value, GetItemPtr(Index), 1);
  Inc(FCount);
end;

function TEzBaseArray.ValidIndex(Index: Integer): Boolean;
begin
  Result := True;
  if (Index < 0) or (Index > FCount) then
  begin
    ArrayIndexError(Index);
    Result := False;
  end
end;

procedure TEzBaseArray.RemoveItem(Index: Integer);
begin
  Delete(Index);
end;

procedure TEzBaseArray.Release(Index: Integer);
begin
  { We are removing only one item. }
  if ValidIndex(index) then
  begin
    Dec(FCount);
    if (Index < FCount) then
    begin
      try
        MoveMemory(GetItemPtr(Index), GetItemPtr(Index + 1), (FCount - Index) * FItemSize);
      except
      end;
    end;
  end;
end;

procedure TEzBaseArray.Delete(Index: Integer);
begin
  { We are removing only one item. }
  if ValidIndex(index) then
  begin
    FreeItems(GetItemPtr(Index), 1);
    Dec(FCount);
    if (Index < FCount) then
    begin
      try
        MoveMemory(GetItemPtr(Index), GetItemPtr(Index + 1), (FCount - Index) * FItemSize);
      except
      end;
    end;
  end;
end;

procedure TEzBaseArray.RemoveRange(atIndex, numItems: Integer);
begin
  if (numItems = 0) then
    Exit;
  if ValidateBounds(atIndex, numItems) then
  begin
    { Invalidate the items about to be deleted so a derived class can do cleanup on them. }
    FreeItems(GetItemPtr(atIndex), numItems);
    { Move the items above those we delete down, if there are any }
    if ((atIndex+numItems) <= FCount) then
    begin
      MoveMemory(GetItemPtr(atIndex), GetItemPtr(atIndex+numItems),
                (FCount-atIndex-numItems+1)* FItemSize);
    end;
    if AutoSize then
      SetCapacity(FCount - numItems);
  end;
end;

procedure TEzBaseArray.Exchange(Index1, Index2: Integer);
begin
end;

procedure TEzBaseArray.Sort(Compare: TCompareProc);
begin
end;

function TEzBaseArray.CompareItems(var Item1, Item2): Integer;
begin
  Result := 0;
end;

procedure TEzBaseArray.CopyItems(PSource, PDest: Pointer; numItems: Integer);
begin
  if (numItems = 0) then Exit;
  try
    FreeItems(PDest, numItems);
    MoveMemory(PDest, PSource, numItems*FItemSize);
  except
    InternalHandleException;
  end;
end;

procedure TEzBaseArray.FreeItems(PItems: Pointer; numItems: Integer);
begin
  ReleaseItems(PItems, numItems);
end;

procedure TEzBaseArray.ReleaseItems(PItems: Pointer; numItems: integer);
begin
  ZeroMemory(PItems, numItems * FItemSize);
end;

procedure TEzBaseArray.PutItem(index: Integer; var Value);
begin
  if SortOrder <> tsNone then
    Add(Value) else
    // GetItemPtr handles validation of index!
    CopyItems(@Value, GetItemPtr(index), 1);
end;

procedure TEzBaseArray.GetItem(index: Integer; var Value);
begin
  if ValidIndex(index) then
  begin
    try
      CopyItems(GetItemPtr(index), @Value, 1);
    except
      InternalHandleException;
    end;
  end;
end;

function TEzBaseArray.GetItemPtr(index: Integer): Pointer;
begin
  Result := nil;
  if ValidIndex(index) then
    Result := Ptr(LongInt(FMemory) + (index*FItemSize));
end;

function TEzBaseArray.ValidateBounds(atIndex: Integer; var numItems: Integer): Boolean;
begin
  Result := True;
  if (atIndex < 0) or (atIndex > FCount) then
    Result := False;
  if Result then
    if (numItems > Succ(FCount)) or ((FCount-numItems+1) < atIndex) then
      numItems := FCount - atIndex + 1;
end;

function TEzBaseArray.HasFlag(aFlag: TArrayFlags): Boolean;
begin
  Result := aFlag in FFlags;
end;

procedure TEzBaseArray.SetFlag(aFlag: TArrayFlags);
begin
  Include(FFLags, aFlag);
end;

procedure TEzBaseArray.ClearFlag(aFlag: TArrayFlags);
begin
  Exclude(FFLags, aFlag);
end;

procedure TEzBaseArray.SetAutoSize(aSize: Boolean);
begin
  if (aSize = True) then
    SetFlag(afAutoSize)
  else
    ClearFlag(afAutoSize);
end;

function TEzBaseArray.GetAutoSize : Boolean;
begin
  Result := HasFlag(afAutoSize);
end;

function TEzBaseArray.IndexOf(var Item): Integer;
begin
  if (SortOrder = tsNone) then
  begin
    for Result := 0 to Count - 1 do
    begin
      if (CompareItems(GetItemPtr(Result)^, Item) = 0) then
        Exit;
    end;
    Result := -1;
  end
  else
    if not FindItem(Result, Item) then
      Result := -1;
end;

function TEzBaseArray.FindItem(var Index: Integer; var Value): Boolean;
var
  L, H, I, C: Integer;

begin
  if (SortOrder = tsNone) then
  begin
    Index := IndexOf(Value);
    Result := Index<>-1;
    Exit;
  end;

  Result := False;
  L := 0;
  H := Count - 1;
  while (L <= H) do
  begin
    I := (L + H) shr 1;
    C := CompareItems(GetItemPtr(I)^, Value);
    if (C < 0) then
      L := I + 1
    else
    begin
      H := I - 1;
      if (C = 0) then
      begin
        Result := True;
        if (Duplicates <> dupAccept) then
          L := I;
      end;
    end;
  end;
  Index := L;
end;

function TEzBaseArray.LowerBound(var Item): Integer;
begin
  if (SortOrder = tsNone) then
  begin
    Result := 0;
    while (Result < Count) and (CompareItems(GetItemPtr(Result)^, Item) < 0) do
      inc(Result);
  end
  else
    FindItem(Result, Item);
end;

procedure TEzBaseArray.BlockCopy(Source: TEzBaseArray; fromIndex, toIndex, numitems: Integer);
begin
  if (numitems = 0) then Exit;
  if (Source is ClassType) and (ItemSize = Source.ItemSize) then
  begin
    if Source.ValidateBounds(fromIndex, numItems) then
    begin
      try
        CopyItems(Source.GetItemPtr(fromIndex), GetItemPtr(toIndex), numItems);
      except
        InternalHandleException;
      end;
    end;
  end;
end;

procedure TEzBaseArray.InternalHandleException;
begin
  Clear;
  raise EArrayError.Create(sGeneralArrayError);
end;

constructor TEzIntArray.Create(InitialSize: Integer = 0);
begin
  inherited Create(InitialSize, Sizeof(Integer));
  SortOrder := tsAscending;
  Duplicates := dupError;
end;

function TEzIntArray.CompareItems(var Item1, Item2): Integer;
var
  i1: Integer absolute Item1;
  i2: Integer absolute Item2;
begin
  if i1 < i2 then
    Result := -1
  else if i1 > i2 then
    Result := 1
  else
    Result := 0;
end;

function TEzIntArray.GetItem(Index: Integer): Integer;
begin
  inherited GetItem(Index, Result);
end;

procedure TEzIntArray.SetItem(Index, Value: Integer);
begin
  inherited PutItem(Index, Value);
end;

// Overview:
// Creates and initializes a TEzObjectList.
//
// Description:
// Create initializes a new TEzObjectList instance. Create sets FOwnsObjects to
// False.
constructor TEzObjectList.Create(DoesOwnObjects: Boolean);
begin
  inherited Create;
  FNotifyDisable:=0;
  FDestroying := False;
  FOwnsObjects := DoesOwnObjects;
  FDatalinks := TList.Create;
end;

// Overview:
// Destroys a TEzObjectList and frees up any memory held by the object.
//
// Description:
// Destroy frees a TEzObjectList instance. If OwnsObjects is true, all objects
// stored in the list will be freed too.
destructor TEzObjectList.Destroy;
var
  i: Integer;
begin
  FDestroying := True;
  inherited;
  for i:=0 to FDataLinks.Count-1 do
    DeleteLink(FDatalinks[0]);
  FDatalinks.Free;
end;

// Overview:
// Inserts an object at the end of the list.
//
// Description:
// Call Add to insert an object at the end of the list. Add places the object
// after the last item, even if the array contains nil (Delphi) or NULL (C++)
// references, and returns the index of the inserted object. (The first
// object in the list has an index of 0.)
//
// Add increments Count and, if necessary, allocates memory by increasing
// the value of Capacity.
function TEzObjectList.Add(AObject: TObject): Integer;
begin
  Insert(Count, AObject);
  Result := Count;
end;

// Overview:
// Copies the contents of source into this list.
//
// Description:
// Call Assign to assign the elements of another list to this one.
procedure TEzObjectList.Assign(Source: TEzObjectList);
var
  i: Integer;

begin
  Clear;
  Capacity := Source.Capacity;
  for I := 0 to Source.Count - 1 do
    Add(Source[I]);
end;

// Overview:
// Adds a TEzListNotifier object to the list of datalinks.
//
// Description:
// TEzObjectList supports extended change notifications (1-to-many). If you
// want to respond to changes in the list, define your own notifier class
// and add it to the list of datalinks by using this method.
//
// Calling AddLink directly is normally not necessary because
// TEzListNotifier handles this automatically.
//
// SeeAlso: TEzListNotifier
procedure TEzObjectList.AddLink(ALink: TEzListNotifier);
begin
  FDataLinks.Add(ALink);
  ALink.FObjectList := Self;
end;

// Overview:
// Removes the object at the position given by the Index parameter.
//
// Description:
// Call Delete to remove the object at a specific position from the list.
// If OwnsObjects is true, this object will be destroyed as well.
procedure TEzObjectList.Delete(Index: Integer);
begin
  inherited Delete(Index);
end;

// Overview:
// Removes a TEzListNotifier object from the list of datalinks.
//
// Description:
// Use this method to remove a notifier from the list of datalinks.
//
// Calling DeleteLink directly is normally not necessary because
// TEzListNotifier handles this automatically.
//
// SeeAlso: TEzListNotifier
procedure TEzObjectList.DeleteLink(ALink: TEzListNotifier);
begin
  ALink.FObjectList := nil;
  FDataLinks.Remove(ALink);
end;

procedure TEzObjectList.DisableNotifications;
begin
  inc(FNotifyDisable);
end;

procedure TEzObjectList.EnableNotifications;
begin
  if FNotifyDisable>0 then
    dec(FNotifyDisable);
end;

function TEzObjectList.Extract(Item: TObject): TObject;
begin
  Result := TObject(inherited Extract(Item));
end;

function TEzObjectList.IndexOf(var Index: Integer; AObject: TObject): Boolean;
begin
  Index := inherited IndexOf(AObject);
  Result := Index <> -1;
end;

function TEzObjectList.IndexOf(AObject: TObject): Integer;
begin
  if not IndexOf(Result, AObject) then
    Result := -1;
end;

function TEzObjectList.First: TObject;
begin
  Result := TObject(inherited First);
end;

function TEzObjectList.GetItem(Index: Integer): TObject;
begin
  Result := DoGetItem(Index);
end;

function TEzObjectList.DoGetItem(Index: Integer): TObject;
begin
  Result := inherited Items[Index];
end;

procedure TEzObjectList.Insert(Index: Integer; AObject: TObject);
begin
  if FNotifyDisable=0 then
    NotifyLinks(AObject, elnAdding);
  inherited Insert(Index, AObject);
  // Added notification fired by Notify method
end;

function TEzObjectList.Last: TObject;
begin
  Result := TObject(inherited Last);
end;

// Overview:
// Responds when items are added to or removed from the list.
//
// Description:
// Notify overrides the Notify method inhertied from TList.
// Notify is called automatically when the items in the list change.
// Ptr points to the object being added, deleted or changed.
// Action indicates whether item was added, is about to be removed, or is
// about to be deleted.
// Notify calls Notifylinks to notify any links stored in datalinks.
//
// SeeAlso: TEzObjectList.NotifyLinks
procedure TEzObjectList.Notify(Ptr: Pointer; Action: TListNotification);
var
  EzAction: TEzListNotification;

begin
  if FNotifyDisable=0 then
  begin
    EzAction := elnAdded;
    case Action of
      lnExtracted: EzAction := elnExtracted;
      lnDeleted: EzAction := elnDeleted;
    end;

    if not FDestroying then
      Notifylinks(TObject(Ptr), EzAction);
  end;

  if OwnsObjects then
    if Action = lnDeleted then
      TObject(Ptr).Free;
end;

// Overview:
// Notifies all links that items are added to or removed from the list.
//
// Description:
// NotifyLinks is called automatically when the items in the list change.
// AObject points to the object being added, deleted or changed.
// Action indicates whether item was added, is about to be removed, or is
// about to be deleted.
//
// NotifyLinks will call Notify on each link stored in datalinks.
//
// SeeAlso: TEzListNotifier.Notify
procedure TEzObjectList.Notifylinks(AObject: TObject; Action: TEzListNotification);
var
  i: Integer;

begin
  for i:=0 to FDatalinks.Count-1 do
    TEzListNotifier(FDatalinks[i]).Notify(AObject, Action);
end;

// Overview:
// Removes a specified object from the list and (if OwnsObjects is true) frees
// the object.
//
// Description:
// Call Remove to delete a specific object from the list when its index is
// unknown. The value returned is the index of the object in the Items array
// before it was removed. If the specified object is not found on the list,
// Remove returns �1. If OwnsObjects is true, Remove frees the object in
// addition to removing it from the list.
// After an object is deleted, all the objects that follow it are moved up
// in index position and Count is decremented. If an object appears more
// than once on the list, Remove deletes only the first appearance. Hence,
// if OwnsObjects is true, removing an object that appears more than once
// results in empty object references later in the list.
//
// To use an index position (rather than an object reference) to specify
// the object to be removed, call Delete.
//
// To remove an object from the list without freeing it, call Extract.
function TEzObjectList.Remove(AObject: TObject): Integer;
begin
  Result := inherited Remove(AObject);
end;

procedure TEzObjectList.SetItem(Index: Integer; AObject: TObject);
begin
  DoSetItem(Index, AObject);
end;

procedure TEzObjectList.DoSetItem(Index: Integer; AObject: TObject);
begin
  if FNotifyDisable=0 then
    NotifyLinks(AObject, elnChanging);
  inherited Items[Index] := AObject;
end;

constructor TEzListNotifier.Create(AList: TEzObjectList);
begin
  inherited Create;
  FObjectList := nil;
  if AList<>nil then
    AList.AddLink(Self);
end;

destructor TEzListNotifier.Destroy;
begin
  inherited;
  if FObjectList <> nil then
    FObjectList.DeleteLink(Self);
end;

procedure TEzListNotifier.SetObjectList(Value: TEzObjectList);
begin
  if Value<>FObjectList then
  begin
    if FObjectList <> nil then
      FObjectList.DeleteLink(Self);
    FObjectList := Value;
    if FObjectList <> nil then
      FObjectList.AddLink(Self);
  end;
end;

{ TEzSortedObjectList }

function TEzSortedObjectList.Add(AObject: TObject): Integer;
begin
  if IndexOf(Result, AObject) then
    case Duplicates of
      dupIgnore:
      begin
        Result := -1;
        Exit;
      end;
      dupError: ArrayDuplicateError;
    end;
  Insert(Result, AObject);
end;

function TEzSortedObjectList.CompareItems(Obj1, Obj2: TObject): Integer;
begin
  if Integer(Obj1)<Integer(Obj2) then
    Result := -1
  else if Integer(Obj1)>Integer(Obj2) then
    Result := 1
  else
    Result := 0;
end;

procedure TEzSortedObjectList.DoSetItem(Index: Integer; AObject: TObject);
begin
  Delete(Index);
  Add(AObject);
end;

function TEzSortedObjectList.IndexOf(var Index: Integer; AObject: TObject): Boolean;
var
  L, H, I, C: Integer;

begin
  Result := False;
  L := 0;
  H := Count - 1;
  while (L <= H) do
  begin
    I := (L + H) shr 1;
    C := CompareItems(Items[I], AObject);
    if (C < 0) then
      L := I + 1
    else
    begin
      H := I - 1;
      if (C = 0) then
      begin
        Result := True;
        if (Duplicates <> dupAccept) then
          L := I;
      end;
    end;
  end;
  Index := L;
end;

procedure TEzObjectStack.Push(AObject: TObject);
begin
  Add(AObject);
end;

function TEzObjectStack.Pop: TObject;
begin
  Result := Last;
  Delete(Count-1);
end;

end.
