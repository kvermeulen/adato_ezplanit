unit EzStrings_1;

interface

resourcestring
  sDupeItem             = 'Duplicate item in array';
  sArrayIndexOutOfRange = 'Array index out of range : %d';
  sLowCapacityError     = 'The DecisionCube Capacity is low. Please deactivate dimensions or change the data set.';
  sGeneralArrayError    = 'General array error.';
  SUnsupportedFieldType = 'Unsupported field type (%s) in field %s.';
  SPersistentFieldsRequired = 'Virtual dataset can only be used with persistent fields.';
  SIndexOutOfRange = 'Index out of range';
  SUsupportedFieldType = 'Unsupported field type (%s) in field %s';

implementation
 
end.

