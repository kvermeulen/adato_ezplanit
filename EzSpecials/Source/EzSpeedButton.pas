unit EzSpeedButton;

interface

uses Classes, Controls, Buttons, Messages;

type
  TEzSpeedButton = class(TSpeedButton)
  private
    FInternalCaption: string;
    FShowCaption: Boolean;
    FUpdating: Boolean;

  protected
    procedure SetCaption(Value: string);
    procedure SetShowCaption(Value: Boolean);
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;

  public
    constructor Create(AOwner: TComponent); override;

  published
    property Caption: string read FInternalCaption write SetCaption;
    property ShowCaption: Boolean read FShowCaption write SetShowCaption default true;
  end;

implementation

constructor TEzSpeedButton.Create(AOwner: TComponent);
begin
  inherited;
  FShowCaption := True;
  FUpdating := False;
end;

procedure TEzSpeedButton.CMTextChanged(var Message: TMessage);
begin
  inherited;
  if not FUpdating then
    //
    // Caption was set from outside this unit
    //
  begin
    FInternalCaption := inherited Caption;
    if not FShowCaption and (FInternalCaption <> '') then
    begin
      FUpdating := true;
      try
        inherited Caption := '';
      finally
        FUpdating := False;
      end;
    end;
  end;
end;

procedure TEzSpeedButton.SetCaption(Value: string);
begin
  if Value <> FInternalCaption then
  begin
    FInternalCaption := Value;
    if FShowCaption then
    begin
      FUpdating := true;
      try
        inherited Caption := FInternalCaption;
      finally
        FUpdating := False;
      end;
    end;
  end;
end;

procedure TEzSpeedButton.SetShowCaption(Value: Boolean);
begin
  if FShowCaption <> Value then
  begin
    FShowCaption := Value;
    FUpdating := True;
    try
      if not FShowCaption then
        inherited Caption := '' else
        inherited Caption := FInternalCaption;
    finally
      FUpdating := False;
    end;
  end;
end;

end.
