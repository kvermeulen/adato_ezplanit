unit EzMasks;

interface

{$I Ez.inc}

uses
  db, Mask, SysUtils, Math
{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
  ;
{$ENDIF}

type
  {$IFDEF EZ_D2009}
    SystemChar = Char;
    SystemString = String;
    SystemWideString = UnicodeString;
    SystemPChar = PWideChar;
  {$ELSE}
    {$IFDEF EZ_D2005}
      SystemChar = WideChar;
      SystemString = WideString;
      SystemWideString = SystemString;
      SystemPChar = PWideChar;
      TSysCharSet = set of Char;
    {$ELSE}
      SystemChar = Char;
      SystemString = String;
      SystemWideString = SystemString;
      SystemPChar = PChar;
      TSysCharSet = set of Char;
    {$ENDIF}
  {$ENDIF}

  {
    Overview:
      TEzDurationSettings defines a thread-safe duration parsing context.
    Description:
      TEzDurationSettings defines a data structure containing locale information
      used when formatting or parsing  duration values. A variable of type
      TEzDurationSettings defines a thread-safe context that formatting functions
      can use in place of the default global context, which is not thread-safe.

      To create and use the thread-safe environment defined by TEzDurationSettings,
      follow these steps:\par
        \fi-720\li720
        \tab * Define a variable of type TEzDurationSettings.\par
	      \tab * Call GetLocaleFormatSettings to populate the TEzDurationSettings
        variable with locale information.\par
      	\tab * Pass the TEzDurationSettings variable as the last parameter of the
        string formatting routine.
        
        Each routine that accepts a TEzDurationSettings parameter is
        thread-safe, and is an overload of an equivalent function that refers
        to the global formatting variables.
        
    See also:
      FormatDuration
  }
  TEzDurationSettings = record
    DefaultChar,
    
    {
      Overview:
        Holds the chartacter used to indicate a day value.
      Description:
        When parsing data entered by the user this property holds the
        character that indicates a day value was entered. For example if a
        user entered '5d' then the result would be 5 days.

        By default this property is set to 'd'.
    }
    DayChar,
    HourChar,
    MinuteChar,
    SecondsChar,
    MSecondsChar: SystemChar;
    HoursPerDay: Double;
    EnableBCD: Boolean;
  end;

  { !! A description of FormatDuration is included in the implementation section }
  function  FormatDuration(const FormatStr: WideString; Value: Double) : WideString; overload;
  function  FormatDuration(const FormatStr: WideString; Value: Double; const InputChars: TEzDurationSettings) : WideString; overload;
  function  FormatValue(const FormatStr: WideString; const Arg: Variant; ArgType: TFieldType) : WideString;
  procedure GetLocaleDurationSettings(LCID: Integer; var InputCharsSettings: TEzDurationSettings);
  function  TranslateTo(const FormatStr: WideString; sValue: WideString; ArgType: TFieldType) : Variant;
  function  TranslateMask(const Mask: WideString; ArgType: TFieldType) : WideString;
  function  ParseDuration(const Text: SystemString; const InputChars: TEzDurationSettings; const TimeSep, DecimalSep: SystemChar): Double; overload;
  function  ParseDuration(const Text: SystemString; const InputChars: TEzDurationSettings): Double; overload;
  function  ParseDuration(const Text: SystemString): Double; overload;

{$IFDEF EZ_D7}
  function  ParseDuration(Text: SystemString; const InputChars: TEzDurationSettings; const FormatSettings: TFormatSettings): Double; overload;
{$ENDIF}

{$IFNDEF WIDESTRINGSUPPORTED}
  function WideStringReplace(const S, OldPattern, NewPattern: WideString;
    Flags: TReplaceFlags): WideString;
{$ENDIF}

var
  GlobalInputChars: TEzDurationSettings;

implementation

uses Windows
{$IFDEF WIDESTRINGSUPPORTED}
     , WideStrUtils
{$ENDIF}
;

{$IFNDEF WIDESTRINGSUPPORTED}
function WideUpperCase(const S: WideString): WideString;
var
  Len: Integer;
begin
  // CharUpperBuffW is stubbed out on Win9x platofmrs
  if Win32Platform = VER_PLATFORM_WIN32_NT then
  begin
    Len := Length(S);
    SetString(Result, PWideChar(S), Len);
    if Len > 0 then CharUpperBuffW(Pointer(Result), Len);
  end
  else
    Result := AnsiUpperCase(S);
end;

//----------------------------------------------------------
// Wide version of SysUtils.StringReplace
function WideStringReplace(const S, OldPattern, NewPattern: WideString;
  Flags: TReplaceFlags): WideString;
var
  SearchStr, Patt, NewStr: WideString;
  Offset: Integer;
begin
  if rfIgnoreCase in Flags then
  begin
    SearchStr := WideUpperCase(S);
    Patt := WideUpperCase(OldPattern);
  end else
  begin
    SearchStr := S;
    Patt := OldPattern;
  end;
  NewStr := S;
  Result := '';
  while SearchStr <> '' do
  begin
    Offset := Pos(Patt, SearchStr);
    if Offset = 0 then
    begin
      Result := Result + NewStr;
      Break;
    end;
    Result := Result + Copy(NewStr, 1, Offset - 1) + NewPattern;
    NewStr := Copy(NewStr, Offset + Length(OldPattern), MaxInt);
    if not (rfReplaceAll in Flags) then
    begin
      Result := Result + NewStr;
      Break;
    end;
    SearchStr := Copy(SearchStr, Offset + Length(Patt), MaxInt);
  end;
end;
{$ENDIF}

function TranslateTo(const FormatStr: WideString; sValue: WideString; ArgType: TFieldType) : Variant;
const
  Factors: array[0..2] of longword = (24, 1440, 86400);

begin
  Result := Null;
  case ArgType of
    ftInteger:
      Result := StrToInt(sValue);

    ftBoolean:
    begin
      if Pos(';', FormatStr) <> 0 then
      begin
        if (sValue[1] = LowerCase(FormatStr[1])) or (sValue[1] = UpperCase(FormatStr[1])) then
          Result := True
        else
          Result := False;
      end
      else
        if (sValue[1] = 't') or (sValue[1] = 'T') or (sValue[1] = '1') then
          Result := True
        else
          Result := False;
    end;
    ftFloat:
      Result := ParseDuration(sValue);
    ftCurrency:
      Result := StrToCurr(sValue);
    ftDate:
      Result := StrToDate(sValue);
    ftTime:
      Result := TDateTime(StrToTime(sValue));
    ftDateTime:
      Result := StrToDateTime(sValue);
    else
      Result := sValue;
  end;
end;

function TranslateMask(const Mask: WideString; ArgType: TFieldType) : WideString;
begin
  case ArgType of
    ftFloat:
    begin
      Result := WideStringReplace(Mask, '.', {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator, [rfReplaceAll]);
      Result := WideStringReplace(Result, ':', {$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, [rfReplaceAll]);
    end;

    ftCurrency:
      Result := WideStringReplace(Mask, '.', {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator, [rfReplaceAll]);

    ftDate:
      Result := WideStringReplace(Mask, '/', {$IFDEF XE3}FormatSettings.{$ENDIF}DateSeparator, [rfReplaceAll]);

    ftTime:
      Result := WideStringReplace(Mask, ':', {$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, [rfReplaceAll]);

    ftDateTime:
    begin
      Result := WideStringReplace(Mask, '/', {$IFDEF XE3}FormatSettings.{$ENDIF}DateSeparator, [rfReplaceAll]);
      Result := WideStringReplace(Result, ':', {$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator, [rfReplaceAll]);
    end;
  else
    Result := Mask;
  end;
end;

{
  Overview:
    Formats a duration value.
  Description:
    FormatDuration formats the duration value given by Value using the format
    given by FormatStr. FormatDuration recognizes the placeholders listed in
    the table below.

    The first form of FormatDuration is not thread-safe, because it uses
    format information contained in global variables. The second form of
    FormatDuration, which is thread-safe, refers to localization information
    contained in the InputChars parameter. Before calling the thread-safe
    form of FormatDuration, you must populate InputChars with localization
    information. To populate InputChars with a set of default locale values,
    call GetLocaleDurationSettings.

    FormatDuration recognizes the following placeholders:

    \fi-1400\li1400
    \b\ul Specifier \tab Displays \b0\ulnone\par
      \fi-1400\li1400
      \b ! \b0\tab
!!      '!' Character
        Turn on left alignment.\par
      \fi-1400\li1400
      \b 0 \b0\tab
!!      '0' Character
        Placeholder for the default value for the current location in the
        output string. If the value being formatted has a digit in the
        position where the '0' appears in the format string, then that
        digit is copied to the output string. Otherwise, a '0' is stored
        in that position in the output string.\par\tab
	      While formatting the outputstring, we maintain the expected value
        for the current location given the normal display order of:
        days, hours, minutes, seconds and miliseconds. So when the mask
        holds '0 0 0 0 0' then the first 0 is matched against the day value,
        the second 0 against the hour value and so on.\par
      \fi-1400\li1400
      \b 9 or # \b0\tab
        Placeholder for the default value for the current location in the
        output string. If the value being formatted has a digit in the
        position where the '9' or '#' appears in the format string, then
        that digit is copied to the output string. Otherwise, nothing is
        stored in that position in the output string.\par\tab
      	While formatting the outputstring, we maintain the expected value
        for the current location given the normal display order
        of: days, hours, minutes, seconds and miliseconds. So when the
        mask holds '9 9 9 9 9' then the first 9 is matched against the
        day value, the second 9 against the hour value and so on.\par
      \fi-1400\li1400
      \b d or D	\b0\tab
        Displays the number of days. The number of days is calculated as the
        total hours divided by the HoursPerDay value. So when the floating point
        value holds 1.5 (one and a half day equals 36 hours) and HoursPerDay
        equals 8 then the number of days displayed will be 36 / 8 = 4.\par
      \fi-1400\li1400
      \b h or H	\b0\tab
        Displays the number of hours. The number of hours is calculated as the
        total hours minus the number of days already displayed in the output
        string. This way, if there is no placeholder for the day value, the
        number of hours can become larger than 24. So a value of 1.5 and a mask
        of 'dd hh' will be displayed as '01 12' while the same value used with
        mask 'hh' will be displayed as '36'.\par
      \fi-1400\li1400
      \b m or M	\b0\tab
        Displays the number of minutes in the output string.\par
      \fi-1400\li1400
      \b s or S	\b0\tab
        Displays the number of seconds in the output string.\par
      \fi-1400\li1400
      \b z or Z	\b0\tab
        Displays the number of miliseconds in the output string.\par
      \fi-1400\li1400
      \b \\ \b0\tab
        Escape character. Characters preceded by a '\\' character are displayed
        as-is, and do not affect formatting.\par
      \fi-1400\li1400
      \b : \b0\tab
        Displays the time separator character given by the TimeSeparator global
        variable.\par
      \fi-1400\li1400
      \b . \b0\tab
        Displays the decimal separator character given by the DecimalSeparator
        global variable.\par
      \fi-1400\li1400
      \b | \b0\tab
        Separates two masks. The first mask is used to display zero values while
        the second mask is used when displaying non zero values.\par
      \fi-1400\li1400
      \b [ ]	\b0\tab
        Encloses conditional parts of the mask. If a conditional part holds a
        zero value, then this part will not be added to the outputstring. So a
        value of 0.5 and a mask of '[d] hh' will be displayed as '12' while the
        same mask used with a value of 1.5 is displayed as '1 12'.\par

      end of table

  See also:
    TEzDurationField.DisplayFormat
}
function FormatDuration(const FormatStr: WideString; Value: Double) : WideString;
begin
  Result := FormatDuration(FormatStr, Value, GlobalInputChars);
end;

function FormatDuration(const FormatStr: WideString; Value: Double; const InputChars: TEzDurationSettings) : WideString;
type
  TPartType = (ptUnknown, ptDay, ptHour, ptMinute, ptSecond, ptMiliSecond);

var
  i, l, DataStart, DataEnd: Integer;
  Days, Hour, Min, Sec, MSec: Word;
  PartHasData, BreakChar, LeftAligned, IsConditional: Boolean;
  tmp, MaskPart: SystemString;
  HasTypes: set of TPartType;
  ExpectedType: TPartType;

  procedure Reset;
  begin
    tmp := '';
    MaskPart := '';
    ExpectedType := ptUnknown;
    BreakChar := False;
  end;

  function FormatFloatToMask(Value: Integer) : string;
  var
    P0s, P9s, c: Integer;

  begin
    c := Length(MaskPart);
    P0s := Pos('0', MaskPart);
    if P0s = 0 then P0s := c+1;
    P9s := Pos('9', MaskPart);
    if P9s = 0 then P9s := c+1;

    if (P0s < P9s) then
      Result := Format('%-' + IntToStr(c) + '.' + IntToStr(P9s-P0s)  + 'd', [Value])
    else if P0s <> (c+1) then
      Result := Format('%' + IntToStr(c) + '.' + IntToStr(c-P0s+1) + 'd', [Value])
    else if LeftAligned then
      Result := Format('%-' + IntToStr(c) + 'd', [Value])
    else
      Result := Format('%' + IntToStr(c) + 'd', [Value])
  end;

  //
  // Add current part to the result and returns true when
  // the added part contained any data (<>0)
  //
  function StorePart(var ResultMask: WideString): Boolean;
  begin
    Result := False;

    if (ExpectedType = ptDay) or ((ExpectedType = ptUnknown) and not (ptDay in HasTypes)) then
    begin
      Days := Floor((Value*24)/InputChars.HoursPerDay);
      DecodeTime(Value-(Days*InputChars.HoursPerDay)/24, Hour, Min, Sec, MSec);

      Result := Days <> 0;
      Include(HasTypes, ptDay);
      ResultMask := ResultMask + FormatFloatToMask(Days) + tmp;
    end
    else if (ExpectedType = ptHour) or ((ExpectedType = ptUnknown) and not (ptHour in HasTypes)) then
    begin
      if not (ptDay in HasTypes) then inc(Hour, Days*24);
      Result := Hour <> 0;
      Include(HasTypes, ptDay);
      Include(HasTypes, ptHour);
      ResultMask := ResultMask + FormatFloatToMask(Hour) + tmp;
    end
    else if (ExpectedType = ptMinute) or ((ExpectedType = ptUnknown) and not (ptMinute in HasTypes)) then
    begin
      Result := Min <> 0;
      Include(HasTypes, ptDay);
      Include(HasTypes, ptHour);
      Include(HasTypes, ptMinute);
      ResultMask := ResultMask + FormatFloatToMask(Min) + tmp;
    end
    else if (ExpectedType = ptSecond) or ((ExpectedType = ptUnknown) and not (ptSecond in HasTypes)) then
    begin
      Result := Sec <> 0;
      Include(HasTypes, ptDay);
      Include(HasTypes, ptHour);
      Include(HasTypes, ptMinute);
      Include(HasTypes, ptSecond);
      ResultMask := ResultMask + FormatFloatToMask(Sec) + tmp;
    end
    else if (ExpectedType = ptMiliSecond) or ((ExpectedType = ptUnknown) and not (ptMiliSecond in HasTypes)) then
    begin
      Result := MSec <> 0;
      Include(HasTypes, ptDay);
      Include(HasTypes, ptHour);
      Include(HasTypes, ptMinute);
      Include(HasTypes, ptSecond);
      Include(HasTypes, ptMiliSecond);
      ResultMask := ResultMask + FormatFloatToMask(MSec) + tmp;
    end;
    Reset;
  end;

  procedure AddCurrentRange;
  var
    p: Integer;

  begin
    p := length(Result);

    if MaskPart = '' then
    begin
      BreakChar := False;
      if tmp <> '' then
        // tmp may hold some fixed chars
      begin
        Result := Result + tmp;
        tmp := '';

        if (DataStart = -1) and not IsConditional then
          DataStart := p;
      end;
    end
    else if StorePart(Result) or not IsConditional or PartHasData then
    begin
      PartHasData := True;
      if DataStart = -1 then
        DataStart := p;
      DataEnd := length(Result);
    end;
  end;

begin
  if InputChars.EnableBCD and (Value <> 0) then
    // Add 1msec to avoid rounding problems
    Value := Value + 1/24/60/60/1000;

  Days := Floor(Value);
  DecodeTime(Value, Hour, Min, Sec, MSec);

  i:=1;
  PartHasData := False;
  BreakChar := False;
  LeftAligned := True;
  Result := '';
  tmp := '';
  MaskPart := '';

  DataStart := -1;
  DataEnd := -1;

  IsConditional := False;
  l := length(FormatStr);

  HasTypes := [];
  ExpectedType := ptUnknown;

  while i <= l do
  begin
    case FormatStr[i] of
      '!':
        LeftAligned := False;
      '[':
      begin
        AddCurrentRange;
        PartHasData := False;
        IsConditional := True;
      end;
      ']':
      begin
        AddCurrentRange;
        PartHasData := False;
        IsConditional := False;
      end;
      '0':
      begin
        if BreakChar then AddCurrentRange;
        MaskPart := MaskPart + '0';
      end;
      '9','#':
      begin
        if BreakChar then AddCurrentRange;
        MaskPart := MaskPart + '9';
      end;
      'd','D':
      begin
        if BreakChar then AddCurrentRange;
        MaskPart := MaskPart + '0';
        ExpectedType := ptDay;
      end;
      'h','H':
      begin
        if BreakChar then AddCurrentRange;
        MaskPart := MaskPart + '0';
        ExpectedType := ptHour;
      end;
      'm','M':
      begin
        if BreakChar then AddCurrentRange;
        MaskPart := MaskPart + '0';
        ExpectedType := ptMinute;
      end;
      's','S':
      begin
        if BreakChar then AddCurrentRange;
        MaskPart := MaskPart + '0';
        ExpectedType := ptSecond;
      end;
      'z','Z':
      begin
        if BreakChar then AddCurrentRange;
        MaskPart := MaskPart + '0';
        ExpectedType := ptMiliSecond;
      end;

      '\': // Escape char
        if i < l then
        begin
          inc(i);
          if ExpectedType = ptUnknown then
            case FormatStr[i] of
              'd', 'D': ExpectedType := ptDay;
              'h', 'H': ExpectedType := ptHour;
              'm', 'M':
              begin
                if (i < l) and ((FormatStr[i+1]='s') or (FormatStr[i+1]='S')) then
                  ExpectedType := ptMiliSecond else
                  ExpectedType := ptMinute;
              end;
              's', 'S': ExpectedType := ptSecond;
            end;
          tmp := tmp + FormatStr[i];
        end else
          tmp := tmp + '\';

      '|': // Splits zero part mask from positive mask
        if Value = 0 then
          i:=l else
          begin
            Reset;
            Result := '';
          end;
      ':': // Time separator
      begin
        if (ExpectedType=ptUnknown) and not (ptMiliSecond in HasTypes) then
        begin
          if ptSecond in HasTypes then
            ExpectedType := ptMiliSecond
          else if ptMinute in HasTypes then
            ExpectedType := ptSecond
          else if ptHour in HasTypes then
            ExpectedType := ptMinute
          else
            ExpectedType := ptHour;
        end;

        tmp := tmp + {$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator;
        AddCurrentRange;
      end;

      '.': // Decimal separator
      begin
        tmp := tmp + {$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator;
        AddCurrentRange;
      end;

      ';':  // Used by Delphi to separate three parts of EditMask.
            // We don't care so we abort
        i:=l;
      else
      begin
        BreakChar := True;
        tmp := tmp + FormatStr[i];
      end;
    end;
    inc(i);
  end;

  AddCurrentRange;
  if DataEnd <> -1 then
    Result := trim(Copy(Result, DataStart+1, DataEnd-DataStart));
end;

function FormatValue(const FormatStr: WideString; const Arg: Variant; ArgType: TFieldType) : WideString;
var
  dPos: Integer;

begin
  Result := '';
  if varIsNull(Arg) or varIsEmpty(Arg) then Exit;

  case ArgType of
    ftInteger:
      Result := FormatFloat(FormatStr, Integer(Arg)); //Format(FormatStr, [Integer(Arg)]);

    ftBoolean:
    begin
      dPos := Pos(';', FormatStr);
      if dPos <> 0 then
      begin
        if Boolean(Arg) then
          Result := Copy(FormatStr, 1, dPos-1)
        else
          Result := Copy(FormatStr, dPos+1, Length(FormatStr) - dPos);
      end
      else
        if Boolean(Arg) then
          Result := 'true'
        else
          Result := 'false';
    end;

    ftFloat:
      Result := FormatFloat(FormatStr, Double(Arg));
    ftCurrency:
      Result := FormatCurr(FormatStr, Extended(Arg));
    ftDate:
      if TDateTime(Arg) <> 0 then
        Result := FormatDateTime(FormatStr, Trunc(TDateTime(Arg)));
    ftTime:
      if TDateTime(Arg) <> 0 then
        Result := FormatDateTime(FormatStr, Frac(TDateTime(Arg)));
    ftDateTime:
      if TDateTime(Arg) <> 0 then
        Result := FormatDateTime(FormatStr, TDateTime(Arg));
  end;
end;

procedure GetLocaleDurationSettings(LCID: Integer; var InputCharsSettings: TEzDurationSettings);
begin
  with InputCharsSettings do
  begin
    DayChar := 'd';
    HourChar := 'h';
    MinuteChar := 'm';
    SecondsChar := 's';
    MSecondsChar := 'z';
    HoursPerDay := 8;
    DefaultChar := 'd';
    EnableBCD := True;
  end;
end;

function ParseDuration(const Text: SystemString): Double;
begin
  Result := ParseDuration(Text,
                          GlobalInputChars,
                          SystemChar({$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator),
                          SystemChar({$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator));
end;

function  ParseDuration(const Text: SystemString; const InputChars: TEzDurationSettings): Double;
begin
  Result := ParseDuration(  Text,
                            InputChars,
                            SystemChar({$IFDEF XE3}FormatSettings.{$ENDIF}TimeSeparator),
                            SystemChar({$IFDEF XE3}FormatSettings.{$ENDIF}DecimalSeparator));
end;

{$IFDEF EZ_D7}
function ParseDuration(Text: SystemString; const InputChars: TEzDurationSettings; const FormatSettings: TFormatSettings): Double;
begin
  Result := ParseDuration(Text, GlobalInputChars, SystemChar(FormatSettings.TimeSeparator), SystemChar(FormatSettings.DecimalSeparator));
end;
{$ENDIF}

function ParseDuration(const Text: SystemString; const InputChars: TEzDurationSettings; const TimeSep, DecimalSep: SystemChar): Double; overload;
const
  ONE_HOUR = 1/24;
  ONE_MIN = ONE_HOUR/60;
  ONE_SEC = ONE_MIN/60;
  ONE_MSEC = 1/24/60/60/1000;

var
  i, l, s: Integer;
  Days, Hour, Min, Sec, MSec: Integer;
  NonDigit: Boolean;
  AChar, ACharUpper, ACharLower: SystemChar;

  procedure ReadChar(NextChar: SystemChar);
  begin
    AChar := NextChar;
    if (AChar >= 'a') and (AChar <= 'z') then
    begin
      ACharLower := AChar;
      ACharUpper := SystemChar(Ord(AChar) - (Ord('a') - Ord('A')));
    end
    else if (AChar >= 'A') and (AChar <= 'Z') then
    begin
      ACharLower := SystemChar(Ord(AChar) + (Ord('a') - Ord('A')));
      ACharUpper := AChar;
    end
    else
    begin
      ACharUpper := AChar;
      ACharLower := AChar;
    end;
  end;

  function SafeInt(str: string) : Integer;
  begin
    if s = -1 then
      Result := 0 else
      Result := StrToInt(Trim(str));
    // Reset
    s := -1;
  end;

  procedure ZeroPart(var P: Integer);
  begin
    if P = -1 then P := 0;
  end;

  procedure StorePart(CanBeDays: Boolean);
  begin
    if (Days = -1) and (AChar<>':') and (InputChars.DefaultChar = InputChars.DayChar) then
      Days := SafeInt(Copy(Text, s, i-s))
    else if ((InputChars.DefaultChar = InputChars.HourChar) or (AChar=':') or (Days<>-1)) and (Hour = -1) then
    begin
      ZeroPart(Days);
      Hour := SafeInt(Copy(Text, s, i-s))
    end
    else if (Min = -1) and ((InputChars.DefaultChar = InputChars.MinuteChar) or (Hour<>-1)) then
    begin
      ZeroPart(Days);
      ZeroPart(Hour);
      Min := SafeInt(Copy(Text, s, i-s))
    end
    else if (Sec = -1) and ((InputChars.DefaultChar = InputChars.SecondsChar) or (Min<>-1)) then
    begin
      ZeroPart(Days);
      ZeroPart(Hour);
      ZeroPart(Min);
      Sec := SafeInt(Copy(Text, s, i-s))
    end
    else if (MSec = -1) and ((InputChars.DefaultChar = InputChars.MSecondsChar) or (Sec<>-1)) then
    begin
      ZeroPart(Days);
      ZeroPart(Hour);
      ZeroPart(Min);
      ZeroPart(Sec);
      MSec := SafeInt(Copy(Text, s, i-s));
    end;
  end;

begin
  Result := 0;
  i := 1;
  l := length(Text);
  s := -1;
  Days := -1;
  Hour := -1;
  Min := -1;
  Sec := -1;
  MSec := -1;
  NonDigit := False;

  while i <= l do
  begin
    ReadChar(Text[i]);

    if (AChar >= '0') and (AChar <= '9') then
    begin
      if s = -1 then
      begin
        s := i;
        NonDigit := False;
      end
      else if NonDigit then
      begin
        StorePart(True);
        s := i;
        NonDigit := False;
      end;
    end
    else if AChar = TimeSep then
      StorePart(False)

    else if AChar = DecimalSep then
    begin
      inc(i);
      while (i <= l) and
        (
          (Text[i] = ' ') or (Text[i] = #9) or ((Text[i] >= '0') and (Text[i] <= '9'))
        )
      do
        inc(i);

      if (i<=l) then
      begin
        ReadChar(Text[i]);
        if (([ACharUpper, ACharLower] * [InputChars.DayChar, InputChars.HourChar, InputChars.MinuteChar, InputChars.SecondsChar, InputChars.MSecondsChar])=[]) then
          ReadChar(InputChars.DefaultChar);
      end else
        ReadChar(InputChars.DefaultChar);

      if (InputChars.DayChar = ACharUpper) or (InputChars.DayChar = ACharLower) then
        Result := StrToFloat(Copy(Text, s, i-s))
      else if (InputChars.HourChar = ACharUpper) or (InputChars.HourChar = ACharLower) then
        Result := ONE_HOUR * StrToFloat(Copy(Text, s, i-s))
      else if (InputChars.MinuteChar = ACharUpper) or (InputChars.MinuteChar = ACharLower) then
        Result := ONE_MIN * StrToFloat(Copy(Text, s, i-s))
      else if (InputChars.SecondsChar = ACharUpper) or (InputChars.SecondsChar = ACharLower) then
        Result := ONE_SEC * StrToFloat(Copy(Text, s, i-s))
      else if (InputChars.MSecondsChar = ACharUpper) or (InputChars.MSecondsChar = ACharLower) then
        Result := ONE_MSEC * StrToFloat(Copy(Text, s, i-s));

      s := -1;
      NonDigit := False;
    end
    else if (InputChars.DayChar = ACharUpper) or (InputChars.DayChar = ACharLower) then
      Days := SafeInt(Copy(Text, s, i-s))
    else  if (InputChars.HourChar = ACharUpper) or (InputChars.HourChar = ACharLower) then
      Hour := SafeInt(Copy(Text, s, i-s))
    else  if (InputChars.MinuteChar = ACharUpper) or (InputChars.MinuteChar = ACharLower) then
      Min := SafeInt(Copy(Text, s, i-s))
    else  if (InputChars.SecondsChar = ACharUpper) or (InputChars.SecondsChar = ACharLower) then
      Sec := SafeInt(Copy(Text, s, i-s))
    else  if (InputChars.MSecondsChar = ACharUpper) or (InputChars.MSecondsChar = ACharLower) then
      MSec := SafeInt(Copy(Text, s, i-s))
    else
    begin
// KV: 30/7/2003
// Text parameter set to const Text: string and assignment removed.
// Not sure whether this will cause any error.
//      Text[i] := ' ';
      NonDigit := True;
    end;
    inc(i);
  end;

  StorePart(True);

  Result := Result + max(0, Days*InputChars.HoursPerDay/24) +
            max(0, Hour) * ONE_HOUR + max(0, Min) * ONE_MIN +
            max(0, Sec) * ONE_SEC + max(0, MSec-1) * ONE_MSEC;
end;

initialization
  GetLocaleDurationSettings(GetThreadLocale, GlobalInputChars);

end.
