unit EzVirtualDataset;

{$I Ez.inc}
{$R-,T-,H+,X+}

interface

uses Classes, DB, SysUtils, Forms, DbConsts, dbcommon
{$IFDEF EZ_D6}
  , Variants;
{$ELSE}
;
{$ENDIF}

type
  TCustomEzArrayDataset = class;
  TEzArrayDataset = class;
  EEzArrayDatasetError = class(Exception);
  PVariantList = ^TVariantList;
  TVariantList = array[0..0] of OleVariant;

  TDeleteRecordEvent = procedure(Sender: TCustomEzArrayDataset; Index: Integer) of object;
  TGetRecordCountEvent = procedure(Sender: TCustomEzArrayDataset; var Count: Integer) of object;
  TGetFieldValueEvent = procedure(Sender: TCustomEzArrayDataset; Field: TField; Index: Integer; var Value: Variant) of object;
  TPostDataEvent = procedure(Sender: TCustomEzArrayDataset; Index: Integer) of object;
  TLocateEvent = procedure(Sender: TCustomEzArrayDataset; const KeyFields: String; const KeyValues: Variant; Options: TLocateOptions; var Index: Integer) of object;
  TLookupValueEvent = procedure(Sender: TCustomEzArrayDataset; const KeyFields: String; const KeyValues: Variant; const ResultFields: String; var Value: Variant) of object;

{$IFNDEF EZ_D2009}
  TRecordBuffer = PChar;
{$ENDIF}

  PArrayRecInfo = ^TArrayRecInfo;
  TArrayRecInfo = record
    Index: Integer;
    BookmarkFlag: TBookmarkFlag;
  end;

  { TEzBlobStream }
  TEzBlobStream = class(TMemoryStream)
  private
    FField: TBlobField;
    FDataSet: TCustomEzArrayDataset;
    FBuffer: TRecordBuffer;
    FFieldNo: Integer;
    FModified: Boolean;
    FData: Variant;
    FFieldData: Variant;
  protected
    procedure ReadBlobData;
    function Realloc(var NewCapacity: Longint): Pointer; override;
  public
    constructor Create(Field: TBlobField; Mode: TBlobStreamMode);
    destructor Destroy; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    procedure Truncate;
  end;

  TEzVirtualMasterDataLink = class(TMasterDataLink)

  protected
    procedure ActiveChanged; override;

  end;

  TCustomEzArrayDataset = class(TDataSet, IUnknown)
  private
    FCurrent: Integer;
    FFilterBuffer: TRecordBuffer;
    FReadOnly: Boolean;
    FRecBufSize: Integer;
    FMasterDataLink: TEzVirtualMasterDataLink;
//    FModifiedFields: TList;
//    FOldValueBuffer: TRecordBuffer;
    {$IFDEF XE3}
    FReserved: Pointer;
    {$ENDIF}
    FOnDeleteRecord: TDeleteRecordEvent;
    FOnGetFieldValue: TGetFieldValueEvent;
    FOnGetRecordCount: TGetRecordCountEvent;
    FOnPostData: TPostDataEvent;
    FOnLocate: TLocateEvent;
    FOnLookupValue: TLookupValueEvent;

  protected
    // Event firing methods
    procedure DoDeleteRecord(Index: Integer); virtual;
    procedure DoGetFieldValue(Field: TField; Index: Integer; var Value: Variant); virtual;
    procedure DoPostData(Index: Integer); virtual;

    // Internal methods
{$IFDEF XE2}
    procedure DataEvent(Event: TDataEvent; Info: NativeInt); override;
{$ELSE}
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
{$ENDIF}
    function  InternalGetRecord(Buffer: TRecordBuffer; GetMode: TGetMode; DoCheck: Boolean): TGetResult;
    function  GetMasterSource: TDataSource;
    function  GetTopRecNo: Integer;
    function  GetIndex: Integer;
    procedure MasterChanged(Sender: TObject); virtual;
    procedure MasterDisabled(Sender: TObject); virtual;
    procedure SetIndex(Value: Integer);
    procedure SetMasterSource(Value: TDataSource);

    {Standard overrides}
    {$IFDEF XE10_UP}
    function GetActiveRecBuf(var RecBuf: PByte): Boolean;
    {$ELSE}
    function GetActiveRecBuf(var RecBuf: TRecordBuffer): Boolean;
    {$ENDIF}
    function  GetCanModify: Boolean; override;
    function  GetRecNo: Longint; override;
    function  GetRecordCount: Integer; override;
    procedure DoOnNewRecord; override;
    procedure InternalEdit; override;
    procedure SetRecNo(Value: Integer); override;

    { Abstract overrides }
    function  AllocRecordBuffer: TRecordBuffer; override;
    procedure FreeRecordBuffer(var Buffer: TRecordBuffer); override;
{$IFDEF XE3}
    procedure GetBookmarkData(Buffer: TRecordBuffer; Data: TBookmark); overload; override;
{$ENDIF}
    procedure GetBookmarkData(Buffer: TRecordBuffer; Data: Pointer); overload; override;
    function  GetBookmarkFlag(Buffer: TRecordBuffer): TBookmarkFlag; override;
    function  GetRecord(Buffer: TRecordBuffer; GetMode: TGetMode; DoCheck: Boolean): TGetResult; override;
    function  GetRecordSize: Word; override;
    procedure InternalAddRecord(Buffer: Pointer; Append: Boolean); override;
    procedure InternalClose; override; {abstract override}
    procedure InternalDelete; override;
    procedure InternalFirst; override;
{$IFDEF XE3}
    procedure InternalGotoBookmark(Bookmark: TBookmark); overload; override;
{$ENDIF}
    procedure InternalGotoBookmark(Bookmark: Pointer); overload; override;
    procedure InternalHandleException; override;
    procedure InternalInitFieldDefs; override;
    procedure InternalInitRecord(Buffer: TRecordBuffer); override;
    procedure InternalLast; override;
    procedure InternalOpen; override;
    procedure InternalPost; override;
    procedure InternalSetToRecord(Buffer: TRecordBuffer); override;
    function  IsCursorOpen: Boolean; override;
    procedure SetBookmarkFlag(Buffer: TRecordBuffer; Value: TBookmarkFlag); override;
{$IFDEF XE3}
    procedure SetBookmarkData(Buffer: TRecordBuffer; Data: TBookmark); overload; override;
{$ENDIF}
    procedure SetBookmarkData(Buffer: TRecordBuffer; Data: Pointer); overload; override;

{$IFDEF XE3}
    procedure SetFieldData(Field: TField; Buffer: TValueBuffer); overload; override;
    procedure SetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean); overload; override;
{$ENDIF}

{$IFNDEF EZ_D5}
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
{$ELSE}
    procedure SetFieldData(Field: TField; Buffer: Pointer); override;
    procedure SetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean); override;
{$ENDIF}

    // actual impl. of SetFieldData, required to support both Delphi and CPP compile
    procedure InternalSetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean);
    procedure VariantToBuffer(Field: TField;const Data: Variant;Buffer: Pointer; NativeFormat: Boolean); overload;

    {$IFDEF XE3}
    property Reserved: Pointer read FReserved write FReserved;
    {$ENDIF}

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    { Standard public overrides }
    function  BookmarkValid(Bookmark: TBookmark): Boolean; override;
    function  CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer; override;
    function  CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream; override;
    function  GetBlobFieldData(FieldNo: Integer; var Buffer: TBlobByteData): Integer; override;
    function  Locate(const KeyFields: string; const KeyValues: Variant;
              Options: TLocateOptions): Boolean; override;
    function  Lookup(const KeyFields: string; const KeyValues: Variant;
              const ResultFields: string): Variant; override;

{$IFDEF XE10_UP}
    function GetFieldData(Field: TField; var Buffer: TValueBuffer): Boolean; overload; override;
    function GetFieldData(Field: TField; var Buffer: TValueBuffer; NativeFormat: Boolean): Boolean; overload; override;
    function GetFieldData(FieldNo: Integer; var Buffer: TValueBuffer): Boolean; overload; override;
{$ELSE}
  {$IFDEF XE3}
      function GetFieldData(Field: TField; Buffer: TValueBuffer): Boolean; overload; override;
      function GetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean): Boolean; overload; override;
      function GetFieldData(FieldNo: Integer; Buffer: TValueBuffer): Boolean; overload; override;
  {$ENDIF}
{$ENDIF}

{$IFNDEF EZ_D5}
    function GetFieldData(Field: TField; Buffer: Pointer): Boolean; overload; override;
{$ELSE}
    function  GetFieldData(Field: TField; Buffer: Pointer): Boolean; override;
    function  GetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean): Boolean; override;
{$ENDIF}

    {Get/Set properties}
    property Current: Integer read FCurrent;
    property Index: Integer read GetIndex write SetIndex;
    property MasterDataLink: TEzVirtualMasterDataLink read FMasterDataLink;
    property MasterSource: TDataSource read GetMasterSource write SetMasterSource;

    property ReadOnly: Boolean read FReadOnly write FReadOnly default False;
    property TopRecNo: Integer read GetTopRecNo;

    {Event properties}
    property OnDeleteRecord: TDeleteRecordEvent read FOnDeleteRecord write FOnDeleteRecord;
    property OnGetFieldValue: TGetFieldValueEvent read FOnGetFieldValue write FOnGetFieldValue;
    property OnGetRecordCount: TGetRecordCountEvent read FOnGetRecordCount write FOnGetRecordCount;
    property OnLocate: TLocateEvent read FOnLocate write FOnLocate;
    property OnLookupValue: TLookupValueEvent read FOnLookupValue write FOnLookupValue;
    property OnPostData: TPostDataEvent read FOnPostData write FOnPostData;
  end;

  TEzArrayDataset = class(TCustomEzArrayDataset)
  published
    property Active;
    property Filtered;
    property ReadOnly;

    property AfterCancel;
    property AfterClose;
    property AfterDelete;
    property AfterEdit;
    property AfterInsert;
    property AfterOpen;
    property AfterPost;
{$IFDEF EZ_D5}
    property AfterRefresh;
{$ENDIF}
    property AfterScroll;
    property BeforeCancel;
    property BeforeClose;
    property BeforeDelete;
    property BeforeEdit;
    property BeforeInsert;
    property BeforeOpen;
    property BeforePost;
{$IFDEF EZ_D5}
    property BeforeRefresh;
{$ENDIF}
    property BeforeScroll;
    property MasterSource;

    property OnDeleteError;
    property OnDeleteRecord;
    property OnEditError;
    property OnFilterRecord;
    property OnGetFieldValue;
    property OnGetRecordCount;
    property OnNewRecord;
    property OnLookupValue;
    property OnLocate;
    property OnPostData;
    property OnPostError;
  end;

implementation

uses EzStrings_1, ActiveX, Windows, Math,
    System.VarUtils,
    System.WideStrUtils,
    Data.FmtBcd;

procedure EzArrayDatasetError(const Message: string; Dataset: TCustomEzArrayDataset = nil);
begin
  if Assigned(Dataset) then
    raise EEzArrayDatasetError.Create(Format('%s: %s', [Dataset.Name, Message])) else
    raise EEzArrayDatasetError.Create(Message);
end;

procedure EzArrayDatasetErrorFmt(const Message: string; const Args: array of const;
  Dataset: TCustomEzArrayDataset = nil);
begin
  EzArrayDatasetError(Format(Message, Args), Dataset);
end;

function FieldListCheckSum(DataSet: TDataset): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to DataSet.Fields.Count - 1 do
    Result := Result + (Integer(Dataset.Fields[I]) shr (I mod 16));
end;

//=---------------------------------------------------------------------------=
// TEzBlobStream
//=---------------------------------------------------------------------------=
constructor TEzBlobStream.Create(Field: TBlobField; Mode: TBlobStreamMode);
begin
  inherited Create;
  FField := Field;
  FFieldNo := FField.FieldNo - 1;
  FDataSet := FField.DataSet as TCustomEzArrayDataset;
  FFieldData := Null;
  FData := Null;
  if not FDataSet.GetActiveRecBuf(FBuffer) then Exit;
  if Mode <> bmRead then
  begin
    if FField.ReadOnly then
      DatabaseErrorFmt(SFieldReadOnly, [FField.DisplayName], FDataSet);
    if not (FDataSet.State in [dsEdit, dsInsert]) then
      DatabaseError(SNotEditing, FDataSet);
  end;
  if Mode = bmWrite then Truncate
  else ReadBlobData;
end;

destructor TEzBlobStream.Destroy;
begin
  if FModified then
  try
    FDataSet.SetFieldData(FField, @FData);
    FField.Modified := True;
    FDataSet.DataEvent(deFieldChange, Longint(FField));
  except
    Application.HandleException(Self);
  end;
  inherited Destroy;
end;

procedure TEzBlobStream.ReadBlobData;
begin
{$IFNDEF EZ_D5}
  FDataSet.GetFieldData(FField, @FFieldData);
{$ELSE}
  FDataSet.GetFieldData(FField, @FFieldData, True);
{$ENDIF}
  if not VarIsNull(FFieldData) then
  begin
    if (VarType(FFieldData) = varOleStr) or (VarType(FFieldData) = varString) then
    begin
{$IFDEF EZ_D2006}
      if FField.BlobType = ftWideMemo then
        Size := Length(WideString(FFieldData)) * sizeof(widechar)
      else
{$ENDIF}
      begin
        { Convert OleStr into a pascal string (format used by TBlobField) }
        FFieldData := string(FFieldData);
        Size := Length(FFieldData);
      end;
    end else
      Size := VarArrayHighBound(FFieldData, 1) + 1;
    FFieldData := Null;
  end;
end;

function TEzBlobStream.Realloc(var NewCapacity: Longint): Pointer;

  procedure VarAlloc(var V: Variant; Field: TFieldType);
  var
{$IFDEF EZ_D8}
    W: WideString;
{$ENDIF}
    S: string;
  begin
    if Field = ftMemo then
    begin
      if not VarIsNull(V) then S := string(V);
      SetLength(S, NewCapacity);
      V := S;
    end else
{$IFDEF EZ_D2006}
    if Field = ftWideMemo then
    begin
      if not VarIsNull(V) then W := WideString(V);
      SetLength(W, NewCapacity div 2);
      V := W;
    end else
{$ENDIF}
    begin

    if {$IFDEF EZ_D6}VarIsClear(V) or{$ENDIF} VarIsNull(V) then
        V := VarArrayCreate([0, NewCapacity-1], varByte) else
        VarArrayRedim(V, NewCapacity-1);
    end;
  end;

begin
  Result := Memory;
  if NewCapacity <> Capacity then
  begin
    if VarIsArray(FData) then VarArrayUnlock(FData);
    if NewCapacity = 0 then
    begin
      FData := Null;
      Result := nil;
    end else
    begin
      if VarIsNull(FFieldData) then
        VarAlloc(FData, FField.DataType) else
        FData := FFieldData;
      if VarIsArray(FData) then
        Result := VarArrayLock(FData) else
        Result := TVarData(FData).VString;
    end;
  end;
end;

function TEzBlobStream.Write(const Buffer; Count: Longint): Longint;
begin
  Result := inherited Write(Buffer, Count);
  FModified := True;
end;

procedure TEzBlobStream.Truncate;
begin
  Clear;
  FModified := True;
end;

//=---------------------------------------------------------------------------=
// TEzVirtualMasterDataLink
//=---------------------------------------------------------------------------=
procedure TEzVirtualMasterDataLink.ActiveChanged;
begin
  if Dataset = nil then Exit;
  
  // Fake a field.
  if Fields.Count = 0 then
    Fields.Add(TField.Create(Dataset));

  if DataSet.Active and not (csDestroying in DataSet.ComponentState) then
    if Active then
    begin
      if Assigned(OnMasterChange) then OnMasterChange(Self);
    end else
      if Assigned(OnMasterDisable) then OnMasterDisable(Self);
end;

//=---------------------------------------------------------------------------=
// TCustomEzArrayDataset
//=---------------------------------------------------------------------------=
constructor TCustomEzArrayDataset.Create(AOwner: TComponent);
begin
  inherited;
  FReadOnly := False;
//  FModifiedFields := TList.Create;
  FMasterDataLink := TEzVirtualMasterDataLink.Create(Self);
  MasterDataLink.OnMasterChange := MasterChanged;
  MasterDataLink.OnMasterDisable := MasterDisabled;
end;

destructor TCustomEzArrayDataset.Destroy;
begin
  inherited;
//  FModifiedFields.Free;
  FMasterDataLink.Free;
end;

function TCustomEzArrayDataset.AllocRecordBuffer: TRecordBuffer;
begin
  if not (csDestroying in ComponentState) then
  begin
    Result := AllocMem(FRecBufSize);
    Initialize(PVariantList(Result+SizeOf(TArrayRecInfo))^, Fields.Count);
  end else
    Result := nil;
end;

function TCustomEzArrayDataset.BookmarkValid(Bookmark: TBookmark): Boolean;
begin
  if Assigned(Bookmark) and (PInteger(Bookmark)^ >= 0) and (PInteger(Bookmark)^ < RecordCount) then
    Result := True else
    Result := False;
end;

function TCustomEzArrayDataset.CompareBookmarks(Bookmark1, Bookmark2: TBookmark): Integer;
const
  RetCodes: array[Boolean, Boolean] of ShortInt = ((2, -1),(1, 0));

begin
  Result := RetCodes[Bookmark1 = nil, Bookmark2 = nil];
  if Result = 2 then
  begin
    if PInteger(Bookmark1)^ < PInteger(Bookmark2)^ then
      Result := -1
    else if PInteger(Bookmark1)^ > PInteger(Bookmark2)^ then
      Result := 1
    else
      Result := 0;
  end;
end;

function TCustomEzArrayDataset.CreateBlobStream(Field: TField; Mode: TBlobStreamMode): TStream;
begin
  Result := TEzBlobStream.Create(Field as TBlobField, Mode);
end;

{$IFDEF XE2}
procedure TCustomEzArrayDataset.DataEvent(Event: TDataEvent; Info: NativeInt);
{$ELSE}
procedure TCustomEzArrayDataset.DataEvent(Event: TDataEvent; Info: Integer);
{$ENDIF}
begin
  case Event of
    deLayoutChange:
      if Active and Assigned(Reserved) and
        (FieldListCheckSum(Self) <> Integer(Reserved)) then
        Reserved := nil;
  end;
  inherited;
end;

procedure TCustomEzArrayDataset.DoDeleteRecord(Index: Integer);
begin
  if Assigned(FOnDeleteRecord) then FOnDeleteRecord(Self, Index);
end;

procedure TCustomEzArrayDataset.DoGetFieldValue(Field: TField; Index: Integer; var Value: Variant);
begin
  if Assigned(FOnGetFieldValue) then FOnGetFieldValue(Self, Field, Index, Value);
end;

procedure TCustomEzArrayDataset.DoOnNewRecord;
begin
//  FModifiedFields.Clear;
//
//  if FOldValueBuffer = nil then
//    FOldValueBuffer := AllocRecordBuffer else
//    Finalize(PVariantList(FOldValueBuffer+SizeOf(TArrayRecInfo))^, Fields.Count);
//
//  InitRecord(FOldValueBuffer);

  inherited DoOnNewRecord;
end;

procedure TCustomEzArrayDataset.DoPostData(Index: Integer);
begin
  if Assigned(FOnPostData) then FOnPostData(Self, Index);
end;

procedure TCustomEzArrayDataset.FreeRecordBuffer(var Buffer: TRecordBuffer);
begin
  Finalize(PVariantList(Buffer+SizeOf(TArrayRecInfo))^, Fields.Count);
  FreeMem(Buffer);
end;

{$IFDEF XE10_UP}
function TCustomEzArrayDataset.GetActiveRecBuf(var RecBuf: PByte): Boolean;
{$ELSE}
function TCustomEzArrayDataset.GetActiveRecBuf(var RecBuf: TRecordBuffer) : Boolean;
{$ENDIF}
begin
  RecBuf := nil;
  case State of
    dsBlockRead,
    dsBrowse:
      if IsEmpty then
        RecBuf := nil else
        RecBuf := TRecordBuffer(ActiveBuffer);

    dsEdit,
    dsInsert,
    dsNewValue:
      RecBuf := TRecordBuffer(ActiveBuffer);

    dsCalcFields,
    dsInternalCalc:
        RecBuf := PByte(CalcBuffer);

    dsFilter:
      RecBuf := FFilterBuffer;
  end;
  Result := RecBuf <> nil;
end;

function TCustomEzArrayDataset.GetBlobFieldData(FieldNo: Integer; var Buffer: TBlobByteData): Integer;
begin
  Result := inherited GetBlobFieldData(FieldNo, Buffer);
end;

{$IFDEF XE3}
procedure TCustomEzArrayDataset.GetBookmarkData(Buffer: TRecordBuffer; Data: TBookmark);
begin
  PInteger(Data)^ := PArrayRecInfo(Buffer)^.Index;
end;
{$ENDIF}

procedure TCustomEzArrayDataset.GetBookmarkData(Buffer: TRecordBuffer; Data: Pointer);
begin
  PInteger(Data)^ := PArrayRecInfo(Buffer)^.Index;
end;

function TCustomEzArrayDataset.GetBookmarkFlag(Buffer: TRecordBuffer): TBookmarkFlag;
begin
  Result := PArrayRecInfo(Buffer)^.BookmarkFlag;
end;

function TCustomEzArrayDataset.GetCanModify: Boolean;
begin
  Result := not FReadOnly;
end;

{$IFDEF EZ_D5}
function TCustomEzArrayDataset.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
begin
  Result := GetFieldData(Field, Buffer, True);
end;

procedure TCustomEzArrayDataset.VariantToBuffer(
  Field: TField;
  const Data: Variant;
  Buffer: Pointer;
  NativeFormat: Boolean);

  procedure CurrToBuffer(const C: Currency);
  begin
    if NativeFormat then
      DataConvert(Field, @C, Buffer, True) else
      Currency(Buffer^) := C;
  end;

var
  l: Integer;

begin
    case Field.DataType of
      ftGuid, ftFixedChar, ftString:
        begin
          if (VarType(Data) = varString) then
          begin
            l := Min(Length(Data), Field.Size);
            PAnsiChar(Buffer)[l] := #0;
            Move(PChar(string(Data))^, PChar(Buffer)^, l);
          end
          else
          begin
            {$IFDEF EZ_D2009}
              //
              // SysStringLen does not return the proper number of characters.
              // This is probably caused by the conversion from CObject to Variant
              // which contains an implicit cast from WideString to UString.
              //
              l := Min(Length(Data), Field.Size);
            {$ELSE}
              l := Min(SysStringLen(tagVariant(Data).bStrVal), Field.Size);
            {$ENDIF}

            PAnsiChar(Buffer)[l] := #0;
            WideCharToMultiByte(  0,
                                  0,
                                  tagVariant(Data).bStrVal,
                                  l+1,
                                  Buffer,
                                  Field.Size,
                                  nil,
                                  nil);
          end;
        end;
{$IFDEF EZ_D2006}
      ftFixedWideChar,
      ftWideString:
        if tagVariant(Data).bstrVal = nil then
          PWideChar(Buffer)[0] := #0 else
          WStrCopy(Buffer, tagVariant(Data).bstrVal);
{$ELSE}
      ftWideString:
        WideString(Buffer^) := tagVariant(Data).bStrVal;
{$ENDIF}

      ftSmallint:
        if tagVariant(Data).vt = VT_UI1 then
          SmallInt(Buffer^) := Byte(tagVariant(Data).cVal) else
          SmallInt(Buffer^) := tagVariant(Data).iVal;
      ftWord:
        if tagVariant(Data).vt = VT_UI1 then
          Word(Buffer^) := tagVariant(Data).bVal else
          Word(Buffer^) := tagVariant(Data).uiVal;
      ftAutoInc, ftInteger:
        Integer(Buffer^) := Data;
      ftFloat, ftCurrency:
        if tagVariant(Data).vt = VT_R8 then
          Double(Buffer^) := tagVariant(Data).dblVal else
          Double(Buffer^) := Data;
{$IFDEF EZ_D2007}
      ftFMTBCD:
        TBcd(Buffer^) := VarToBcd(Data);
{$ENDIF}
      ftBCD:
        if tagVariant(Data).vt = VT_CY then
          CurrToBuffer(tagVariant(Data).cyVal) else
          CurrToBuffer(Data);
      ftBoolean:
      begin
        VarAsType(Data, VT_BOOL);
        WordBool(Buffer^) := tagVariant(Data).vbool;
      end;
      ftDate, ftTime, ftDateTime:
      begin
        //VarAsType(Data, VT_DATE);
        if NativeFormat then
          DataConvert(Field, @date, Buffer, True) else
          TOleDate(Buffer^) := tagVariant(Data).date;
      end;
//{$IFDEF EZ_D6}
//    ftTimeStamp:
//      TSQLTimeStamp(Buffer^) := VarToSQLTimeStamp(Data);
//{$ENDIF}
//{$IFDEF EZ_D2010}
//    ftTimeStampOffset:
//      TSQLTimeStampOffset(Buffer^) := VarToSQLTimeStampOffset(Data);
//{$ENDIF}
      ftBytes, ftVarBytes:
        if NativeFormat then
          DataConvert(Field, @Data, Buffer, True) else
          OleVariant(Buffer^) := Data;
      ftInterface: IUnknown(Buffer^) := Data;
      ftIDispatch: IDispatch(Buffer^) := Data;
      ftLargeInt:
{$IFDEF XE2}
        if PDecimal(@Data)^.sign > 0 then
          LargeInt(Buffer^):=-1*PDecimal(@Data)^.Lo64 else
          LargeInt(Buffer^):=PDecimal(@Data)^.Lo64;
{$ELSE}
        if Decimal(Data).sign > 0 then
          LargeInt(Buffer^):=-1*Decimal(Data).Lo64 else
          LargeInt(Buffer^):=Decimal(Data).Lo64;
{$ENDIF}

{$IFDEF EZ_D2006}
      ftWideMemo,
{$ENDIF}
      ftBlob..ftTypedBinary, ftVariant:
        OleVariant(Buffer^) := Data;
    else
      DatabaseErrorFmt(SUnsupportedFieldType, [FieldTypeNames[Field.DataType],
        Field.DisplayName]);
    end;
end;

function TCustomEzArrayDataset.GetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean): Boolean;
{$ELSE}
function TCustomEzArrayDataset.GetFieldData(Field: TField; Buffer: Pointer): Boolean;
{$ENDIF}
var
  RecBuf: TRecordBuffer;
  Data: Variant;

  procedure RefreshBuffers;
  begin
    Reserved := Pointer(FieldListCheckSum(Self));
    UpdateCursorPos;
    Resync([]);
  end;

begin
  if not Assigned(Reserved) then RefreshBuffers;

//  if (State = dsOldValue) and (FModifiedFields.IndexOf(Field) <> -1) then
//    //
//    // Requesting the old value of a modified field
//    //
//  begin
//    Result := True;
//    RecBuf := FOldValueBuffer;
//  end else
//    Result := GetActiveRecBuf(RecBuf);

  Result := GetActiveRecBuf(RecBuf);
  if not Result then Exit;

  Data := PVariantList(RecBuf+SizeOf(TArrayRecInfo))^[Field.Index];
  // if data hasn't been loaded yet, then get data from
  // dataset.
  if VarIsEmpty(Data) then
  begin
    DoGetFieldValue(Field, PArrayRecInfo(RecBuf)^.Index, Data);
    if VarIsEmpty(Data) then Data := Null;
    PVariantList(RecBuf+SizeOf(TArrayRecInfo))[Field.Index] := Data;
  end;
  Result := not VarIsNull(Data);
  if Result and (Buffer <> nil) then
    VariantToBuffer(Field, Data, Buffer, NativeFormat);
end;

function TCustomEzArrayDataset.GetFieldData(Field: TField; {$IFDEF XE10_UP}var{$ENDIF} Buffer: TValueBuffer): Boolean;
begin
  Result := GetFieldData(Field, Buffer, True);
end;

function TCustomEzArrayDataset.GetFieldData(FieldNo: Integer; {$IFDEF XE10_UP}var{$ENDIF} Buffer: TValueBuffer): Boolean;
begin
  Result := GetFieldData(FieldByNumber(FieldNo), Buffer);
end;

function TCustomEzArrayDataset.GetFieldData(Field: TField; {$IFDEF XE10_UP}var{$ENDIF} Buffer: TValueBuffer; NativeFormat: Boolean): Boolean;
var
  RecBuf: TRecordBuffer;
  Data: OleVariant;

  procedure CurrToBuffer(const C: Currency);
  var
    LBuff: TValueBuffer;
  begin
    if NativeFormat then
    begin
      SetLength(LBuff, SizeOf(Currency));
      TDBBitConverter.UnsafeFrom<Currency>(C, LBuff);
      DataConvert(Field, LBuff, Buffer, True)
    end
    else
      TDBBitConverter.UnsafeFrom<Currency>(C, Buffer);
  end;

  procedure VarToBuffer;
  var
    TempBuff: TValueBuffer;
    PData: Pointer;

  begin
    case Field.DataType of
      ftGuid, ftFixedChar, ftString:
        begin
          PAnsiChar(Buffer)[Field.Size] := #0;
          WideCharToMultiByte(0, 0, tagVariant(Data).bStrVal, SysStringLen(tagVariant(Data).bStrVal)+1,
            @Buffer[0], Field.Size, nil, nil);
        end;
      ftFixedWideChar, ftWideString:
        begin
          TempBuff := TEncoding.Unicode.GetBytes(tagVariant(Data).bstrVal);
          SetLength(TempBuff, Length(TempBuff) + SizeOf(Char));
          TempBuff[Length(TempBuff) - 2] := 0;
          TempBuff[Length(TempBuff) - 1] := 0;
          Move(TempBuff[0], Buffer[0], Length(TempBuff));
        end;
      ftSmallint:
        if tagVariant(Data).vt = VT_UI1 then
          TDBBitConverter.UnsafeFrom<SmallInt>(Byte(tagVariant(Data).cVal), Buffer)
        else
          TDBBitConverter.UnsafeFrom<SmallInt>(tagVariant(Data).iVal, Buffer);
      ftWord:
        if tagVariant(Data).vt = VT_UI1 then
          TDBBitConverter.UnsafeFrom<Word>(tagVariant(Data).bVal, Buffer)
        else
          TDBBitConverter.UnsafeFrom<Word>(tagVariant(Data).uiVal, Buffer);
      ftAutoInc, ftInteger:
        TDBBitConverter.UnsafeFrom<Integer>(Data, Buffer);
      ftFloat, ftCurrency:
        if tagVariant(Data).vt = VT_R8 then
          TDBBitConverter.UnsafeFrom<Double>(tagVariant(Data).dblVal, Buffer)
        else
          TDBBitConverter.UnsafeFrom<Double>(Data, Buffer);
      ftFMTBCD:
        TDBBitConverter.UnsafeFrom<TBcd>(VarToBcd(Data), Buffer);
      ftBCD:
        if tagVariant(Data).vt = VT_CY then
          CurrToBuffer(tagVariant(Data).cyVal)
        else
          CurrToBuffer(Data);
      ftBoolean:
        TDBBitConverter.UnsafeFrom<WordBool>(tagVariant(Data).vbool, Buffer);
      ftDate, ftTime, ftDateTime:
        if NativeFormat then
        begin
          SetLength(TempBuff, SizeOf(Double));
          TDBBitConverter.UnsafeFrom<Double>(data, TempBuff);
          DataConvert(Field, TempBuff, Buffer, True);
        end
        else
          TDBBitConverter.UnsafeFrom<Double>(tagVariant(Data).date, Buffer);
      ftBytes, ftVarBytes:
        if NativeFormat then
        begin
          PData := VarArrayLock(Data);
          try
            DataConvert(Field, BytesOf(PData, VarArrayHighBound(Data, 1) - VarArrayLowBound(Data, 1) + 1), Buffer, True);
          finally
            VarArrayUnlock(Data);
          end;
        end
        else
        begin
          if VarIsArray(Data) then
            SetLength(Buffer, VarArrayHighBound(Data, 1) + 1);
          TDBBitConverter.UnsafeFromVariant(Data, Buffer);
        end;
      ftInterface:
        begin
          TempBuff := BytesOf(@Data, SizeOf(IUnknown));
          Move(TempBuff[0], Buffer[0], SizeOf(IUnknown));
        end;
      ftIDispatch:
        begin
          TempBuff := BytesOf(@Data, SizeOf(IDispatch));
          Move(TempBuff[0], Buffer[0], SizeOf(IDispatch));
        end;
      ftLargeInt:
      begin
        if PDecimal(@Data)^.sign > 0 then
          TDBBitConverter.UnsafeFrom<Int64>(-1*PDecimal(@Data)^.Lo64, Buffer)
        else
          TDBBitConverter.UnsafeFrom<Int64>(PDecimal(@Data)^.Lo64, Buffer);
      end;
      ftBlob..ftTypedBinary, ftVariant, ftWideMemo:
      begin
        SetLength(TempBuff, SizeOf(Variant));
        PVariant(TempBuff)^ := Data;
        Move(TempBuff[0], Buffer[0], SizeOf(Variant));
      end;
        // TDBBitConverter.UnsafeFromVariant(Data, Buffer);
    else
      DatabaseErrorFmt(SUsupportedFieldType, [FieldTypeNames[Field.DataType],
        Field.DisplayName]);
    end;
  end;

  procedure RefreshBuffers;
  begin
    Reserved := Pointer(FieldListCheckSum(Self));
    UpdateCursorPos;
    Resync([]);
  end;

  function DataToInt64: Int64;
  begin
    if PDecimal(@Data)^.sign > 0 then
      Result := -1 * PDecimal(@Data)^.Lo64
    else
      Result := PDecimal(@Data)^.Lo64;
  end;

var
  v: Variant;

begin
  if not Assigned(Reserved) then RefreshBuffers;

//  if (State = dsOldValue) and (FModifiedFields.IndexOf(Field) <> -1) then
//  //
//  // Requesting the old value of a modified field
//  //
//  begin
//    Result := True;
//    RecBuf := FOldValueBuffer;
//  end
//  else
//    Result := GetActiveRecBuf(RecBuf);

  Result := GetActiveRecBuf(RecBuf);
  if not Result then
    Exit;

  Data := PVariantList(RecBuf + sizeof(TArrayRecInfo))^[Field.Index];

  // if data hasn't been loaded yet, then get data from
  // dataset.
  if VarIsEmpty(Data) then
  begin
    DoGetFieldValue(Field, PArrayRecInfo(RecBuf)^.Index, v);

    if VarIsEmpty(v) then
      Data := Null else
      Data := v;

    // Conversion of Int64(Variant) to OleVariant implicetely converts to Integer.
    // Take extra measures to ensure that Int64 values are stored as Int64.
    if VarType(Data) = varInt64 then
      PVariantList(RecBuf + sizeof(TArrayRecInfo))[Field.Index] := DataToInt64
    else
      PVariantList(RecBuf + sizeof(TArrayRecInfo))[Field.Index] := Data;
  end;

  Result := not VarIsNull(Data);
  if Result and (Buffer <> nil) then
    VarToBuffer;
end;

function TCustomEzArrayDataset.GetIndex: Integer;
begin
  Result := RecNo;
  if Result > -1 then
    dec(Result);
end;

function TCustomEzArrayDataset.GetMasterSource: TDataSource;
begin
  Result := MasterDataLink.DataSource;
end;

function TCustomEzArrayDataset.GetRecNo: Longint;
var
  RecBuf: TRecordBuffer;

begin
  CheckActive;
  Result := -1;
  if GetActiveRecBuf(RecBuf) and (PArrayRecInfo(RecBuf)^.BookmarkFlag = bfCurrent) then
    Result := PArrayRecInfo(RecBuf)^.Index+1;
end;

function TCustomEzArrayDataset.GetRecord(Buffer: TRecordBuffer; GetMode: TGetMode; DoCheck: Boolean): TGetResult;
var
  Accept: Boolean;
  SaveState: TDataSetState;

begin
  if Filtered and Assigned(OnFilterRecord) then
  begin
    FFilterBuffer := Buffer;
    SaveState := SetTempState(dsFilter);
    try
      Accept := True;
      repeat
        Result := InternalGetRecord(Buffer, GetMode, DoCheck);
        if Result = grOK then
        begin
          OnFilterRecord(Self, Accept);
          if not Accept and (GetMode = gmCurrent) then
            Result := grError;
        end;
      until Accept or (Result <> grOK);
    except
      Application.HandleException(Self);
      Result := grError;
    end;
    RestoreState(SaveState);
  end else
    Result := InternalGetRecord(Buffer, GetMode, DoCheck)
end;

function TCustomEzArrayDataset.InternalGetRecord(Buffer: TRecordBuffer; GetMode: TGetMode; DoCheck: Boolean): TGetResult;
var
  iRecCount: Integer;
begin
  try
    Result := grOK;
    case GetMode of
      gmNext:
      begin
        iRecCount := RecordCount;
        if FCurrent < iRecCount then inc(FCurrent);
        if FCurrent >= iRecCount then Result := grEOF;
      end;
      gmPrior:
      begin
        if FCurrent >=0 then dec(FCurrent);
        if FCurrent < 0 then Result := grBOF;
      end;

      gmCurrent:
      begin
        iRecCount := RecordCount;
        if FCurrent < 0 then Result := grBOF else
        if FCurrent >= iRecCount then Result := grEof;
      end;
    end;

    if Result = grOK then
    begin
      with PArrayRecInfo(Buffer)^ do
      begin
        Index := FCurrent;
        BookmarkFlag := bfCurrent;
      end;
      Finalize(PVariantList(Buffer+SizeOf(TArrayRecInfo))^, Fields.Count);
      GetCalcFields(Buffer);
    end;

  except
    if DoCheck then raise;
    Result := grError;
  end;
end;

function TCustomEzArrayDataset.GetRecordCount: Integer;
begin
  Result := -1;
  if Assigned(FOnGetRecordCount) then FOnGetRecordCount(Self, Result);
end;

function TCustomEzArrayDataset.GetRecordSize: Word;
begin
  Result := sizeof(TArrayRecInfo);
end;

function TCustomEzArrayDataset.GetTopRecNo: Integer;
begin
  Result := PArrayRecInfo(Buffers[0])^.Index+1;
end;

procedure TCustomEzArrayDataset.InternalAddRecord(Buffer: Pointer; Append: Boolean);
begin
end;

procedure TCustomEzArrayDataset.InternalClose;
begin
  BindFields(False);
  FieldDefs.Updated := False;

//  if FOldValueBuffer<>nil then
//  try
//    Finalize(PVariantList(FOldValueBuffer+SizeOf(TArrayRecInfo))^, Fields.Count);
//    FreeMem(FOldValueBuffer);
//  finally
//    FOldValueBuffer := nil;
//  end;
end;

procedure TCustomEzArrayDataset.InternalDelete;
var
  RecBuf: TRecordBuffer;

begin
  GetActiveRecBuf(RecBuf);
  DoDeleteRecord(PArrayRecInfo(RecBuf)^.Index);
end;

procedure TCustomEzArrayDataset.InternalEdit;
begin
//  FModifiedFields.Clear;
//
// if FOldValueBuffer = nil then
//    FOldValueBuffer := AllocRecordBuffer else
//    Finalize(PVariantList(FOldValueBuffer+SizeOf(TArrayRecInfo))^, Fields.Count);
end;

procedure TCustomEzArrayDataset.InternalFirst;
begin
  FCurrent := -1;
end;

{$IFDEF XE3}
procedure TCustomEzArrayDataset.InternalGotoBookmark(Bookmark: TBookmark);
begin
  FCurrent := PInteger(Bookmark)^;
end;
{$ENDIF}

procedure TCustomEzArrayDataset.InternalGotoBookmark(Bookmark: Pointer);
begin
  FCurrent := PInteger(Bookmark)^;
end;

procedure TCustomEzArrayDataset.InternalHandleException;
begin
  Application.HandleException(Self);
end;

procedure TCustomEzArrayDataset.InternalInitFieldDefs;
var
  FieldDef: TFieldDef;

  procedure InitFieldDefsFromFields(Fields: TFields; FieldDefs: TFieldDefs);
  var
    i: integer;
    F: TField;
  begin
    for I := 0 to Fields.Count - 1 do
    begin
      F := Fields[I];
      with F do
        if FieldDefs.IndexOf(FieldName) = -1 then
        begin
          FieldDef := FieldDefs.AddFieldDef;
          FieldDef.Name := FieldName;
          FieldDef.DataType := DataType;
          FieldDef.Size := Size;
          if Required then
            FieldDef.Attributes := [faRequired];
          if ReadOnly then
            FieldDef.Attributes := FieldDef.Attributes + [db.faReadonly];
          if (DataType = ftBCD) and (F is TBCDField) then
            FieldDef.Precision := TBCDField(F).Precision;
          if F is TObjectField then
            InitFieldDefsFromFields(TObjectField(F).Fields, FieldDef.ChildDefs);
        end;
    end;
  end;

begin
  FieldDefs.Clear;
  InitFieldDefsFromFields(Fields, FieldDefs);
end;

procedure TCustomEzArrayDataset.InternalInitRecord(Buffer: TRecordBuffer);
var
  I: Integer;
begin
  for I := 0 to Fields.Count - 1 do
    PVariantList(Buffer+SizeOf(TArrayRecInfo))[I] := Null;
end;

procedure TCustomEzArrayDataset.InternalLast;
begin
  FCurrent := RecordCount;
end;

procedure TCustomEzArrayDataset.InternalOpen;
begin
  FCurrent := -1;

  if DefaultFields then
    EzArrayDatasetError(SPersistentFieldsRequired, Self);

  BookmarkSize := sizeof(Integer);

  FieldDefs.Updated := False;
  FieldDefs.Update;
  Reserved := Pointer(FieldListCheckSum(Self));
  BindFields(True);
  FRecBufSize := SizeOf(TArrayRecInfo) + (Fields.Count * SizeOf(Variant));
end;

procedure TCustomEzArrayDataset.InternalPost;
var
  RecBuf: TRecordBuffer;

begin
  UpdateCursorPos;
  GetActiveRecBuf(RecBuf);

  if PArrayRecInfo(RecBuf)^.BookmarkFlag = bfEof then
    DoPostData(-1) else
    DoPostData(PArrayRecInfo(RecBuf)^.Index);
end;

procedure TCustomEzArrayDataset.InternalSetToRecord(Buffer: TRecordBuffer);
begin
  if PArrayRecInfo(Buffer)^.BookmarkFlag in [bfCurrent, bfInserted] then
    FCurrent := PArrayRecInfo(Buffer)^.Index;
end;

function TCustomEzArrayDataset.IsCursorOpen: Boolean;
begin
  Result := FieldDefs.Updated;
end;

function TCustomEzArrayDataset.Locate(const KeyFields: string; const KeyValues: Variant;
              Options: TLocateOptions): Boolean;
var
  P: Integer;

begin
  if Assigned(FOnLocate) then
  begin
    P := -1;
    FOnLocate(Self, KeyFields, KeyValues, Options, P);
    Result := P<>-1;
    if Result and (P<>FCurrent) then
    begin
      DoBeforeScroll;
      FCurrent := P;
      Resync([rmCenter]);
      DoAfterScroll;
    end;
  end
  else
    Result := False;
end;

function TCustomEzArrayDataset.Lookup(const KeyFields: string; const KeyValues: Variant;
              const ResultFields: string): Variant;
begin
  if Assigned(FOnLookupValue) then
  begin
    Result := Null;
    FOnLookupValue(Self, KeyFields, KeyValues, ResultFields, Result);
  end else
    Result := inherited Lookup(KeyFields, KeyValues, ResultFields);
end;

procedure TCustomEzArrayDataset.MasterChanged(Sender: TObject);
begin
  if not Active then Exit;
  FCurrent := -1;
  Resync([]);
end;

procedure TCustomEzArrayDataset.MasterDisabled(Sender: TObject);
begin
  if not Active then Exit;
  // Suggestion from Roman Linde
  // Do not reset cursor position because:
  // Second problem is with "MasterDisabled". Procedure executes when I call
  // "Enable controls" changing active record earlier set with "Locate".
  // I want to locate record with disabled controls and enabling controls should
  // not change active record?
  //  FCurrent := -1;
  Resync([]);
end;

procedure TCustomEzArrayDataset.SetBookmarkFlag(Buffer: TRecordBuffer; Value: TBookmarkFlag);
begin
  PArrayRecInfo(Buffer)^.BookmarkFlag := Value;
end;

{$IFDEF XE3}
procedure TCustomEzArrayDataset.SetBookmarkData(Buffer: TRecordBuffer; Data: TBookmark);
begin
  if PArrayRecInfo(Buffer)^.BookmarkFlag in [bfCurrent, bfInserted] then
    PArrayRecInfo(Buffer)^.Index := PInteger(Data)^ else
    PArrayRecInfo(Buffer)^.Index := -1;
end;
{$ENDIF}

procedure TCustomEzArrayDataset.SetBookmarkData(Buffer: TRecordBuffer; Data: Pointer);
begin
  if PArrayRecInfo(Buffer)^.BookmarkFlag in [bfCurrent, bfInserted] then
    PArrayRecInfo(Buffer)^.Index := PInteger(Data)^ else
    PArrayRecInfo(Buffer)^.Index := -1;
end;

{$IFDEF XE3}
procedure TCustomEzArrayDataset.SetFieldData(Field: TField; Buffer: TValueBuffer);
begin
  InternalSetFieldData(Field, Buffer, True);
end;

procedure TCustomEzArrayDataset.SetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean);
begin
  InternalSetFieldData(Field, Buffer, NativeFormat);
end;
{$ENDIF}

procedure TCustomEzArrayDataset.SetFieldData(Field: TField; Buffer: Pointer);
begin
  InternalSetFieldData(Field, TValueBuffer(Buffer), True);
end;

{$IFDEF EZ_D5}
procedure TCustomEzArrayDataset.SetFieldData(Field: TField; Buffer: Pointer; NativeFormat: Boolean);
begin
  InternalSetFieldData(Field, TValueBuffer(Buffer), NativeFormat);
end;
{$ENDIF}

procedure TCustomEzArrayDataset.SetIndex(Value: Integer);
begin
  if (Value < 0) or (Value >= RecordCount) then
    EzArrayDatasetError(SIndexOutOfRange, Self);
  RecNo := Value+1;
end;

procedure TCustomEzArrayDataset.SetMasterSource(Value: TDataSource);
begin
  if IsLinkedTo(Value) then DatabaseError(SCircularDataLink, Self);
  MasterDataLink.DataSource := Value;
end;

procedure TCustomEzArrayDataset.SetRecNo(Value: Integer);
begin
  Value := min(max(Value, 1), RecordCount);
  if RecNo <> Value then
  begin
    DoBeforeScroll;
    FCurrent := Value-1;
    Resync([rmCenter]);
    DoAfterScroll;
  end;
end;

procedure TCustomEzArrayDataset.InternalSetFieldData(Field: TField; Buffer: TValueBuffer; NativeFormat: Boolean);

  procedure BcdToOleVariant(const Bcd: TBcd; var Data: OleVariant);
  var
    Temp: OleVariant;
  begin
    VarClear(Data);
    Temp := BcdToStr(Bcd);
    // Note: VariantChangeTypeEx does not raise an error on VT_DECIMAL truncation!
    VarResultCheck(VariantChangeTypeEx(TVarData(Data), TVarData(Temp), VAR_LOCALE_USER_DEFAULT,
       0, VT_DECIMAL));
  end;

  procedure BufferToVar(var Data: OleVariant);
  var
    LUnknown: IUnknown;
    LDispatch: IDispatch;
    TempBuff: TValueBuffer;
  begin
    case Field.DataType of
      ftString, ftFixedChar, ftGuid:
        Data := AnsiString(PAnsiChar(Buffer));
      ftWideString, ftFixedWideChar:
        Data := WideString(PWideChar(Buffer));
      ftAutoInc, ftInteger:
        Data := TDBBitConverter.UnsafeInto<LongInt>(Buffer);
      ftSmallInt:
        Data := TDBBitConverter.UnsafeInto<SmallInt>(Buffer);
      ftWord:
        Data := TDBBitConverter.UnsafeInto<Word>(Buffer);
      ftBoolean:
        Data := TDBBitConverter.UnsafeInto<WordBool>(Buffer);
      ftFloat, ftCurrency:
        Data := TDBBitConverter.UnsafeInto<Double>(Buffer);
      ftBlob, ftMemo, ftGraphic, ftVariant, ftWideMemo:
        Data := TDBBitConverter.UnsafeIntoVariant(Buffer);
      ftInterface:
      begin
        Move(Buffer[0], LUnknown, SizeOf(IUnknown));
        Data := LUnknown;
      end;
      ftIDispatch:
      begin
        Move(Buffer[0], LDispatch, SizeOf(IDispatch));
        Data := LDispatch;
      end;
      ftDate, ftTime, ftDateTime:
        if NativeFormat then
        begin
          SetLength(TempBuff, SizeOf(TVarData(Data).VDate));
          DataConvert(Field, Buffer, TempBuff, False);
          TVarData(Data).VDate := TDBBitConverter.UnsafeInto<Double>(TempBuff);
        end
        else
          Data := TDBBitConverter.UnsafeInto<Double>(Buffer);
      ftFMTBCD:
        BcdToOleVariant(TDBBitConverter.UnsafeInto<TBcd>(Buffer), Data);
      ftBCD:
        if NativeFormat then
        begin
          SetLength(TempBuff, SizeOf(TVarData(Data).VCurrency));
          DataConvert(Field, Buffer, TempBuff, False);
          TVarData(Data).VCurrency := TDBBitConverter.UnsafeInto<Currency>(TempBuff);
        end
        else
          Data := TDBBitConverter.UnsafeInto<Currency>(Buffer);
      ftBytes, ftVarBytes:
        if NativeFormat then
        begin
          TempBuff := BytesOf(@Data, SizeOf(Variant));
          DataConvert(Field, Buffer, TempBuff, False);
          Data := TDBBitConverter.UnsafeIntoVariant(TempBuff);
        end
        else
          Data := TDBBitConverter.UnsafeInToBAVariant(Buffer);
      ftLargeInt:
        Data := TDBBitConverter.UnsafeInto<Int64>(Buffer);
      else
        DatabaseErrorFmt(SUsupportedFieldType, [FieldTypeNames[Field.DataType],
          Field.DisplayName]);
    end;
  end;

var
  Data: OleVariant;
  RecBuf: TRecordBuffer;

begin
  with Field do
  begin
    if not (State in dsWriteModes) then DatabaseError(SNotEditing, Self);
    GetActiveRecBuf(RecBuf);

    if FieldNo > 0 then
    begin
      if ReadOnly and not (State in [dsSetKey, dsFilter]) then
        DatabaseErrorFmt(SFieldReadOnly, [DisplayName]);

      Field.Validate(Buffer);

//      if FModifiedFields.IndexOf(Field) = -1 then
//      begin
          // Newer versions of Delphi handle old values in a different way
          // calling Field.Value will actually reset the new field value back to the old value!!
//        // PVariantList(FOldValueBuffer+SizeOf(TArrayRecInfo))[Field.Index] := Field.Value;
//        FModifiedFields.Add(Field);
//      end;
    end;

    if Buffer = nil then
      Data := Null else
      BufferToVar(Data);

    PVariantList(RecBuf+SizeOf(TArrayRecInfo))[Field.Index] := Data;

    if not (State in [dsCalcFields, dsInternalCalc, dsFilter, dsNewValue]) then
      DataEvent(deFieldChange, Longint(Field));
  end;
end;

end.
