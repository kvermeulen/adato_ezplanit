unit BorderLess;

interface

uses Controls, Classes, Forms, DBCtrls, Windows;

type
  TEzBorderLessDBLookupCombobox = class(TDBLookupCombobox)
  private
    FBorderStyle: TBorderStyle;

  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure SetBorderStyle(Value: TBorderStyle);

  public
    constructor Create(AOwner: TComponent); override;

  published
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle default bsSingle;
  end;

implementation

constructor TEzBorderLessDBLookupCombobox.Create(AOwner: TComponent);
begin
  inherited;
  FBorderStyle := bsSingle;
end;

procedure TEzBorderLessDBLookupCombobox.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if BorderStyle = bsNone then
  begin
    Params.Style := Params.Style and not WS_BORDER;
    Params.ExStyle := Params.ExStyle and not WS_EX_CLIENTEDGE;
  end;
end;

procedure TEzBorderLessDBLookupCombobox.SetBorderStyle(Value: TBorderStyle);
begin
  if FBorderStyle <> Value then
  begin
    FBorderStyle := Value;
    RecreateWnd;
  end;
end;

end.
