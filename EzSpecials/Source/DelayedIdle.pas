unit DelayedIdle;

interface

uses Classes, Windows, Forms, SysUtils, Messages, EzArrays;

const
  EXPIRED = DWORD(-1);

type
  TEzDelayedIdle=class;
  TEzDelayedIdleThread=class;

  TEzTriggerEvent = (
      rmAllMessages,  // Reset on all messages!
      rmKeyBoard,     // WM_KEYFIRST till WM_KEYLAST
      rmMouseClicks,  // WM_LBUTTONDBCLK, WM_LBUTTONDOWN, WM_LBUTTONUP,
                      // WM_MBUTTONDBCLK, WM_MBUTTONDOWN, WM_MBUTTONUP,
                      // WM_RBUTTONDBCLK, WM_RBUTTONDOWN, WM_RBUTTONUP,
      rmMouseMove,    // WM_MOUSEMOVE, WM_MOUSEHOVER, WM_MOUSELEAVE
      rmMouseScroll,  // WM_MOUSEWHEEL
      rmUser,         // WM_USER events
      rmScroll,       // WM_VSCROLL, WM_HSCROLL
      rmPaint,        // WM_PAINT event
      rmWindowPos,    // WM_WINDOWPOSCHANGED, WM_WINDOWPOSCHANGING
      rmOtherMessages // All other messages
    );
  TEzTriggerEvents = set of TEzTriggerEvent;

  TEzIdleState = (isInActive, isActive, isWaiting, isHandled);
  TStateChangeEvent = procedure (Sender: TObject; NewState: TEzIdleState) of object;
  TDelayedIdleEvent = procedure (Sender: TObject; var NewState: TEzIdleState) of object;

  TEzIdleMonitor = class
  private
    FIdleClients: TList;
    FMessageQueue: TEzIntArray;
    FOriginalIdle: TIdleEvent;
    FOriginalMessage: TMessageEvent;

  protected
    procedure ApplicationIdle(Sender: TObject; var Done: Boolean); virtual;
    procedure ApplicationMessage(var Msg: TMsg; var Handled: Boolean); virtual;

  public
    constructor Create;
    destructor  Destroy;  override;

    procedure AddClient(Client: TEzDelayedIdleThread); virtual;
    procedure RemoveClient(Client: TEzDelayedIdleThread); virtual;
  end;

  TEzDelayedIdleThread = class(TThread)
  private
    FLastEvent, FCheckInterval: DWORD;
    FDelay: DWORD;
    FState: TEzIdleState;
    FNewState: TEzIdleState;
    FOwner: TEzDelayedIdle;
    FTriggerEvents: TEzTriggerEvents;
    GoSuspended: Boolean;
    FException: Exception;

  protected
    procedure Execute; override;
    procedure SyncException;
    procedure SyncDoDelayIdle;

  public
    constructor Create(AOwner: TEzDelayedIdle);
    function  TimeOut: DWORD;

    property LastEvent: DWORD read FLastEvent;
  end;

  TEzDelayedIdle = class(TComponent)
  private
    FLock: TRTLCriticalSection;
    FIdleThread: TEzDelayedIdleThread;
    FActive: Boolean;
    FFireSynchronized: Boolean;
    FOnIdle: TIdleEvent;
    FOnDelayedIdle: TDelayedIdleEvent;
    FOnStateChanged: TStateChangeEvent;
    FOnWaiting: TNotifyEvent;

  protected
    procedure DoIdle(var Done: Boolean); virtual;
    procedure DoDelayedIdle(var NewState: TEzIdleState); virtual;
    procedure DoStateChanged; virtual;
    procedure DoWaiting; virtual;
    procedure SetActive(Value: Boolean);
    procedure SetDelay(Value: Integer);
    procedure SetTriggerEvents(Value: TEzTriggerEvents);

    function  GetActive: Boolean;
    function  GetDelay: Integer;
    function  GetState: TEzIdleState;
    function  GetTriggerEvents: TEzTriggerEvents;

  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;

    procedure Lock;
    procedure Unlock;
    function  TimeOut: DWORD;
    procedure UpdateState(NewState: TEzIdleState);

    property IdleThread: TEzDelayedIdleThread read FIdleThread;

  published
    property Active: Boolean read GetActive write SetActive;
    property Delay: Integer read GetDelay write SetDelay default 500;
    property FireSynchronized: Boolean read FFireSynchronized write FFireSynchronized;
    property TriggerEvents: TEzTriggerEvents read GetTriggerEvents write SetTriggerEvents default [rmKeyBoard, rmMouseClicks];
    property State: TEzIdleState read GetState;
    property OnIdle: TIdleEvent read FOnIdle write FOnIdle;
    property OnDelayedIdle: TDelayedIdleEvent read FOnDelayedIdle write FOnDelayedIdle;
    property OnStateChanged: TStateChangeEvent read FOnStateChanged write FOnStateChanged;
    property OnWaiting: TNotifyEvent read FOnWaiting write FOnWaiting;
  end;

  function TestTrigger(Triggers: TEzTriggerEvents; Message: Cardinal): Boolean;

var
  EzIdleMonitor: TEzIdleMonitor;

implementation

uses Math;

function TestTrigger(Triggers: TEzTriggerEvents; Message: Cardinal): Boolean;
begin
  if (rmAllMessages in Triggers) or
     ((rmKeyBoard in Triggers) and (Message >= WM_KEYFIRST) and (Message <= WM_KEYLAST)) or
     ((rmMouseClicks in Triggers) and (Message >= WM_LBUTTONDOWN) and (Message <= WM_MBUTTONDBLCLK)) or
     ((rmMouseMove in Triggers) and ((Message = WM_MOUSEMOVE) or (Message = WM_MOUSEHOVER) or (Message = WM_MOUSELEAVE))) or
     ((rmMouseScroll in Triggers) and (Message = WM_MOUSEWHEEL)) or
     ((rmUser in Triggers) and (Message = WM_USER)) or
     ((rmScroll in Triggers) and ((Message = WM_VSCROLL) or (Message = WM_HSCROLL))) or
     ((rmPaint in Triggers) and (Message = WM_PAINT)) or
     ((rmWindowPos in Triggers) and ((Message = WM_WINDOWPOSCHANGED) or (Message = WM_WINDOWPOSCHANGING))) or
     (rmOtherMessages in Triggers)
  then
    Result := True else
    Result := False;
end;

constructor TEzIdleMonitor.Create;
begin
  inherited;
  FIdleClients := TList.Create;
  FMessageQueue := TEzIntArray.Create(20);
  FMessageQueue.Duplicates := dupIgnore;
end;

destructor TEzIdleMonitor.Destroy;
begin
  inherited;
  FIdleClients.Destroy;
  FMessageQueue.Destroy;
end;

procedure TEzIdleMonitor.ApplicationIdle(Sender: TObject; var Done: Boolean);
var
  i, m: Integer;
  IsDone: Boolean;

begin
  IsDone := True;

  for i:=0 to FIdleClients.Count-1 do
    with TEzDelayedIdleThread(FIdleClients[i]) do
    begin
      if FState = isInActive then continue;

      m:=0;
      while m < FMessageQueue.Count do
      begin
        if TestTrigger(FTriggerEvents, Cardinal(FMessageQueue[m])) then
        begin
          FOwner.UpdateState(isWaiting);
          break;
        end;
        inc(m);
      end;
      FOwner.DoIdle(IsDone);
      Done := Done and IsDone;
    end;
  FMessageQueue.Clear;
  if Assigned(FOriginalIdle) then FOriginalIdle(Sender, Done);
end;

procedure TEzIdleMonitor.ApplicationMessage(var Msg: TMsg; var Handled: Boolean);
begin
  FMessageQueue.Add(Msg.message);
  if Assigned(FOriginalMessage) then FOriginalMessage(Msg, Handled);
end;

procedure TEzIdleMonitor.AddClient(Client: TEzDelayedIdleThread);
begin
  if FIdleClients.Count = 0 then
  begin
    FOriginalIdle := Application.OnIdle;
    FOriginalMessage := Application.OnMessage;

    Application.OnIdle := ApplicationIdle;
    Application.OnMessage := ApplicationMessage;
  end;
  FIdleClients.Add(Client);
end;

procedure TEzIdleMonitor.RemoveClient(Client: TEzDelayedIdleThread);
begin
  FIdleClients.Remove(Client);
  if FIdleClients.Count = 0 then
  begin
    Application.OnIdle := FOriginalIdle;
    Application.OnMessage := FOriginalMessage;
  end;
end;

//=----------------------------------------------------------------------------=
constructor TEzDelayedIdleThread.Create(AOwner: TEzDelayedIdle);
begin
  inherited Create(True);
  FOwner := AOwner;
  GoSuspended := False;
  FTriggerEvents := [rmKeyBoard, rmMouseClicks];
  FCheckInterval := 100;
  FDelay := 500;
  FLastEvent := GetTickCount;
  FState := isInActive;
end;

procedure TEzDelayedIdleThread.Execute;
var
  T: DWORD;

begin
  EzIdleMonitor.AddClient(Self);
  try
    while not Terminated do
    begin
      if GoSuspended then
      begin
        GoSuspended := False;
        EzIdleMonitor.RemoveClient(Self);
        Suspended := True;
        EzIdleMonitor.AddClient(Self);
        continue;
      end;

      if (FState = isWaiting) then
      begin
        try
          T := TimeOut;
          if T=EXPIRED then
          begin
            FNewState := isHandled;
            if FOwner.FireSynchronized then
              Synchronize(SyncDoDelayIdle) else
              FOwner.DoDelayedIdle(FNewState);
            FOwner.UpdateState(FNewState);
          end
          else
          begin
            FOwner.DoWaiting;
            Sleep(min(T, FCheckInterval));
          end;
        except
          on E: Exception do
          begin
            FException := E;
            Synchronize(SyncException);
          end;
        end;
      end else
        Sleep(FCheckInterval);
    end;
  finally
    EzIdleMonitor.RemoveClient(Self);
  end;
end;

procedure TEzDelayedIdleThread.SyncException;
begin
  Application.ShowException(FException);
end;

procedure TEzDelayedIdleThread.SyncDoDelayIdle;
begin
  FOwner.DoDelayedIdle(FNewState);
end;

function TEzDelayedIdleThread.TimeOut: DWORD;
var
  T: DWORD;
begin
  T := GetTickCount-LastEvent;
  if FDelay>T then
    Result := FDelay-T else
    Result := EXPIRED;
end;

constructor TEzDelayedIdle.Create(AOwner: TComponent);
begin
  inherited;
  InitializeCriticalSection(FLock);
  FIdleThread := TEzDelayedIdleThread.Create(Self);
  FActive := False;
end;

destructor TEzDelayedIdle.Destroy;
begin
  FIdleThread.Terminate;
  FIdleThread.Destroy;
  Lock;
  try
    inherited;
  finally
    Unlock;
    DeleteCriticalSection(FLock);
  end;
end;

procedure TEzDelayedIdle.DoIdle(var Done: Boolean);
begin
  if Assigned(FOnIdle) then FOnIdle(Self, Done);
end;

procedure TEzDelayedIdle.DoDelayedIdle(var NewState: TEzIdleState);
begin
  if Assigned(FOnDelayedIdle) then FOnDelayedIdle(Self, NewState);
end;

procedure TEzDelayedIdle.DoStateChanged;
begin
  if Assigned(FOnStateChanged) then FOnStateChanged(Self, State);
end;

procedure TEzDelayedIdle.DoWaiting;
begin
  if Assigned(FOnWaiting) then FOnWaiting(Self);
end;

procedure TEzDelayedIdle.SetActive(Value: Boolean);
begin
  if csDesigning in ComponentState then
    FActive := Value
  else if Value <> not FIdleThread.Suspended then
  begin
    if Value then
    begin
      FIdleThread.Suspended := False;
      UpdateState(isActive);
    end
    else
    begin
      FIdleThread.GoSuspended := True;
      UpdateState(isInActive);
    end;
  end;
end;

procedure TEzDelayedIdle.SetDelay(Value: Integer);
begin
  FIdleThread.FDelay := Value;
end;

procedure TEzDelayedIdle.SetTriggerEvents(Value: TEzTriggerEvents);
begin
  FIdleThread.FTriggerEvents := Value;
end;

function TEzDelayedIdle.GetActive: Boolean;
begin
  if csDesigning in ComponentState then
    Result := FActive else
    Result := not FIdleThread.Suspended;
end;

function TEzDelayedIdle.GetDelay: Integer;
begin
  Result := FIdleThread.FDelay;
end;

function TEzDelayedIdle.GetState: TEzIdleState;
begin
  Result := FIdleThread.FState;
end;

function TEzDelayedIdle.GetTriggerEvents: TEzTriggerEvents;
begin
  Result := FIdleThread.FTriggerEvents;
end;

procedure TEzDelayedIdle.Lock;
begin
  EnterCriticalSection(FLock);
end;

function TEzDelayedIdle.TimeOut: DWORD;
begin
  Result := FIdleThread.TimeOut;
end;

procedure TEzDelayedIdle.Unlock;
begin
  LeaveCriticalSection(FLock);
end;

procedure TEzDelayedIdle.UpdateState(NewState: TEzIdleState);
begin
  Lock;
  try
    if FIdleThread.FState <> NewState then
    begin
      FIdleThread.FState := NewState;
      DoStateChanged;
    end;

    if FIdleThread.FState = isWaiting then
      FIdleThread.FLastEvent := GetTickCount;
  finally
    Unlock;
  end;
end;

initialization
  EzIdleMonitor := TEzIdleMonitor.Create;

finalization
begin
  EzIdleMonitor.Destroy;
  EzIdleMonitor := nil;
end;

end.
